/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.python.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.eclipse.comma.python.PythonInterpreter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class InterperterTest {

	@Test
	public void testHelloWorld() {
		String result = PythonInterpreter.execute("print('hello world')");
		assertEquals("hello world", result);
	}
	
	@Test
	public void testSyntaxError() {
		Exception exception = assertThrows(RuntimeException.class, () -> {
			PythonInterpreter.execute("print('hello world");
		});
		assertEquals(exception.getMessage().startsWith("Python execution failed"), true);
	}
	
	@Test
	public void testSnakes() {
		String code = "";
		code += "from snakes.nets import *" + "\n";
		code += "n = PetriNet('First net')" + "\n";
		code += "n.add_place(Place('p', [0]))" + "\n";
		code += "n.add_transition(Transition('t', Expression('x<5')))" + "\n";
		code += "n.add_input('p', 't', Variable('x'))" + "\n";
		code += "n.add_output('p', 't', Expression('x+1'))" + "\n";
		code += "print(n.transition('t').modes())" + "\n";
		
		String result = PythonInterpreter.execute(code);
		assertEquals("[Substitution(x=0)]", result);
	}

}
