## Python builder
Builds a standalone portable Python version (e.g. `.exe` for Windows) using [PyInstaller](https://www.pyinstaller.org/).
Cross-compilation is not supported, e.g. Windows build can only be build on Windows.

### Building
Requires at least Python 3.5

To build run: `python build.py`, build result is written to `../resource`.

### Executable
The executable is created based on the `executor.py`. 
All required dependencies should be imported in this file (in order for PyInstaller to pick them up).
The sole purpose of this script is to execute the Python script passed as an argument.
E.g. on Windows the executable can be called with: 


```
>> executor.exe hello_world.py
hello world
```

### Linux
On Linux the executable is linked to the GLIBC version of the build system. GLIBC is not forward compatible thus it is recommended to build on the oldest system possible.

See: https://github.com/pyinstaller/pyinstaller/wiki/FAQ#gnulinux for more information.

To build on Docker with GLIBC 2.23 (also works on Windows):

```bash
docker run -v $(pwd)/../../:/data ubuntu:16.04 /bin/bash -c "apt-get update && apt-get install software-properties-common build-essential zlib1g-dev -y && add-apt-repository ppa:jblgf0/python && apt-get update && apt-get install binutils python3.9 python3.9-venv python3.9-dev python3.9-tk -y && cd /data/org.eclipse.comma.python/builder && python3.9 build.py"
```