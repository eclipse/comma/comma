#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import sys, platform, venv, os, shutil, subprocess, zipfile, urllib.request, tarfile

if sys.version_info < (3,5):
    print("Sorry, requires at least Python 3.5, got %s" % platform.python_version())
    sys.exit(1)

class _EnvBuilder(venv.EnvBuilder):
    def __init__(self, *args, **kwargs):
        self.context = None
        super().__init__(*args, **kwargs)

    def post_setup(self, context):
        self.context = context

upx_path = 'upx'
shutil.rmtree(upx_path, ignore_errors=True)
upx_lookup = {"Windows": ('4.0.2', 'upx-4.0.2-win64.zip'), 'Linux': ('4.0.2', 'upx-4.0.2-amd64_linux.tar.xz')}
has_upx = platform.system() in upx_lookup
if has_upx:
    print("Downloading upx")
    upx_url = "https://github.com/upx/upx/releases/download/v%s/%s" % upx_lookup[platform.system()]
    archive_path, _ = urllib.request.urlretrieve(upx_url)
    if upx_url.endswith('.zip'):
        with zipfile.ZipFile(archive_path, "r") as f:
            f.extractall(upx_path)
    else:
        with tarfile.open(archive_path) as f:
            f.extractall(upx_path)
    upx_path = os.path.join(upx_path, os.listdir(upx_path)[0])

print("Creating venv...")
builder = _EnvBuilder(with_pip=True)
venv_path = os.path.join(os.path.dirname(__file__), "venv_builder")
shutil.rmtree(venv_path, ignore_errors=True)
builder.create(venv_path)
context = builder.context

print("Installing requirements...")
subprocess.check_call([context.env_exe, '-m' , 'pip', 'install', '-r', 'requirements.txt'])

print ("Building...")
is_mac = platform.system() == 'Darwin'
extra_cmd = []
if has_upx: extra_cmd.extend(['--upx-dir', upx_path])
if is_mac: extra_cmd.extend(['--onefile', '--target-arch', 'universal2'])
subprocess.check_call([context.env_exe, '-m' , 'PyInstaller', *extra_cmd, '--noconfirm', 'python.py'])

print("Creating zip...")
zip_path = 'dist/dist.zip'
zip_file = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED)
if is_mac:
    zip_file.write('dist/python', 'python')
else:
    dist_path = 'dist/python'
    for root, dirs, files in os.walk(dist_path):
        for file in files:
            zip_file.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), dist_path))
zip_file.close()

print('Copying...')
zip_pkg_lookup = {
    'Windows': 'org.eclipse.comma.python.win32.x86_64', 
    'Linux': 'org.eclipse.comma.python.linux.x86_64',
    'Darwin': 'org.eclipse.comma.python.macosx.x86_64'
}
zip_target_dir = os.path.join('..', '..', zip_pkg_lookup[platform.system()])
resource_zip = os.path.join(zip_target_dir, 'dist.zip')
shutil.copyfile(zip_path, resource_zip)
print("Created '%s'" % resource_zip)

resource_zip_dir = os.path.join(zip_target_dir, 'dist')
shutil.rmtree(resource_zip_dir, ignore_errors=True)
with zipfile.ZipFile(resource_zip, 'r') as zipObj:
   zipObj.extractall(resource_zip_dir)
print(f"Extracted to '{resource_zip_dir}'")

print("Done")