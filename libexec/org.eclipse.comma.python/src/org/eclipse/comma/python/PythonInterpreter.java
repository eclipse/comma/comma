/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.python;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;

public class PythonInterpreter {
	
	private static String executable;
	
	public static String execute(File file, String[] arguments) {
		loadExecutableIfNotLoaded();
		
		try {
			var pbArgs = new ArrayList<>(Arrays.asList(executable, file.getAbsolutePath()));
			pbArgs.addAll(Arrays.asList(arguments));
			var pb = new ProcessBuilder(pbArgs);
			var process = pb.start();
			
			var stdout = new StringBuilder();
			var stderr = new StringBuilder();
			startOutConsumerThread(process.getInputStream(), stdout);
			startOutConsumerThread(process.getErrorStream(), stderr);
			
			var exitCode = process.waitFor();
			if (exitCode != 0) {
				throw new Exception(stderr.toString());
			} else {
				return stdout.toString();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException("Python execution failed: " + e.getMessage(), e);
		}
	}
	
	public static String execute(File file) {
		return execute(file, new String[0]);
	}
	
	public static String execute(String script) {
		return execute(script, new String[0]);
	}
	
	public static String execute(String script, String[] arguments) {
		loadExecutableIfNotLoaded();
		
		try {
			File tempFile = File.createTempFile("commasuite", "py");
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		    writer.write(script);
		    writer.close();
			tempFile.deleteOnExit();
			return execute(tempFile, arguments);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	private static void startOutConsumerThread(InputStream stream, StringBuilder builder) {
		new Thread() {
			@Override
			public void run() {
				BufferedReader reader = new BufferedReader(new InputStreamReader(stream)); 
				String line = null;
				try {
					while ((line = reader.readLine()) != null) {
						if (builder.length() != 0) {
							builder.append("\n");
						}
						
						builder.append(line);
					}
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	public static String getExecutablePath() {
		loadExecutableIfNotLoaded();
		return executable;
	}
	
	private static void loadExecutableIfNotLoaded() {
		if (executable != null) return;
		
		String os = System.getProperty("os.name").toLowerCase();
		String pkg = null;
		String exec = null;
		
		if (os.startsWith("windows")) {
			pkg = "org.eclipse.comma.python.win32.x86_64";
			exec = "dist/python.exe";
		} else if (os.startsWith("linux")) {
			pkg = "org.eclipse.comma.python.linux.x86_64";
			exec = "dist/python";
		} else if (os.startsWith("mac")) {
			pkg = "org.eclipse.comma.python.macosx.x86_64";
			exec = "dist/python";
		} else {
			throw new RuntimeException("PythonInterpreter does not support OS: " + os);
		}
		
		try {
			try {
				// Running in Eclipse environment
				var rsc = Platform.getBundle(pkg).getResource(exec);
				executable = FileLocator.toFileURL(rsc).getPath();
				if (os.startsWith("windows")) {
					executable = executable.substring(1);
				}
			} catch (Exception e) {
				var sourceLocation = new File(PythonInterpreter.class.getProtectionDomain().getCodeSource().getLocation().toURI());
				if (sourceLocation.isDirectory()) {
					// Junit via Eclipse
					executable = Paths.get(sourceLocation.getAbsolutePath(), "..", "..", "..", pkg, exec).toAbsolutePath().toString();
				} else if (sourceLocation.getAbsolutePath().toLowerCase().endsWith(".jar")) {
					var execFile = Paths.get(sourceLocation.getParentFile().getAbsolutePath(), "..", "..", pkg, exec).toFile();
					if (execFile.isFile()) {
						// Maven build
						executable = execFile.getAbsolutePath().toString();
					} else {
						// Standalone jar
						var tempDir = Files.createTempDirectory("commapython").toFile();
						tempDir.deleteOnExit();
						var destDir = tempDir.getAbsolutePath();
						var jar = new JarFile(sourceLocation);
						var enumEntries = jar.entries();
						while (enumEntries.hasMoreElements()) {
							var entry = enumEntries.nextElement();
							if (entry.getName().startsWith(pkg)) {
								var file = new File(destDir + java.io.File.separator + entry.getName());
							    if (entry.isDirectory()) {
							        file.mkdir();
							        continue;
							    }
							    var is = jar.getInputStream(entry);
							    var fos = new java.io.FileOutputStream(file);
							    while (is.available() > 0) fos.write(is.read());
							    fos.close();
							    is.close();
							}
						}
						jar.close();
						executable = Paths.get(destDir, pkg, exec).toAbsolutePath().toString();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to load Python interpeter", e);
		}
		
		if (os.startsWith("linux") || os.startsWith("mac")) {
			try {
				new ProcessBuilder().command("chmod", "+x", executable).start().waitFor();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void createStartScript(File file, String name) throws IOException {
		var os = System.getProperty("os.name").toLowerCase();
		var exec = PythonInterpreter.getExecutablePath();
		String script, extension = "";
		if (os.startsWith("windows")) {
			extension = ".bat";
			script = String.format("\"%s\" \"%s\"", exec, file.toString());
        } else {
        	extension = ".sh";
        	script = String.format("#!/bin/bash\n\"%s\" \"%s\"", exec, file.toString());
        }
		
		var target = Paths.get(file.getParent(), name + extension);
		Files.write(target, script.getBytes());
		if (!os.startsWith("windows")) {
			Runtime.getRuntime().exec("chmod u+x " + target.toString());
		}
	}
}