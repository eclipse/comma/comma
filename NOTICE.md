# Notices for Eclipse CommaSuite

This content is produced and maintained by the Eclipse CommaSuite (Eclipse CommaSuite) project.

 * Project home: https://www.eclipse.org/comma
 * Website: https://eclipse.org/comma
 * Issue tracker: https://gitlab.eclipse.org/eclipse/comma/comma/-/issues
 * Downloads: https://download.eclipse.org/comma
 * Archived downloads: https://archive.eclipse.org/comma
 * Forum: https://www.eclipse.org/forums/index.php/f/555/
 * Developers mailing list: https://accounts.eclipse.org/mailing-list/comma-dev
 * GitLab development server: https://gitlab.eclipse.org/eclipse/comma/comma
 * Jenkins build server: https://ci.eclipse.org/comma


## Trademarks

Eclipse CommaSuite and CommaSuite are trademarks of the Eclipse Foundation. Eclipse,
and the Eclipse Logo are registered trademarks of the Eclipse Foundation.
Other names may be trademarks of their respective owners.


## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.


## Declared Project Licenses

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/

SPDX-License-Identifier: EPL-2.0


## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/comma/comma.git

These can also be accessed via a web interface:

 * https://gitlab.eclipse.org/eclipse/comma/comma


## Third-party Content

This software includes third party content listed in [NOTICE-THIRD-PARTY.md](NOTICE-THIRD-PARTY.md)

## Cryptography

Content may contain encryption software. The country in which you are
currently may have restrictions on the import, possession, and use, and/or
re-export to another country, of encryption software. BEFORE using any
encryption software, please check the country's laws, regulations and
policies concerning the import, possession, or use, and re-export of
encryption software, to see if this is permitted.