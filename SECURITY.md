# Security Policy

The Eclipse CommaSuite(TM) project follows the [Eclipse Vulnerability Reporting Policy](https://www.eclipse.org/security/policy.php).
Vulnerabilities are tracked by the Eclipse security team, in cooperation with the CommaSuite project leads.
Fixing vulnerabilities is handled by the CommaSuite project committers, with assistance and guidance of the security team.

## Supported Versions

Fixes to a vulnerability will be included in the next release.

## Reporting a Vulnerability

In case of suspected vulnerabilities please contact the Eclipse Security Team directly via security@eclipse.org. We do not recommend using the CommaSuite public issue tracker.
