# Contributing to Eclipse CommaSuite

This guide provides all necessary information to enable [contributors and committers](https://www.eclipse.org/projects/dev_process/#2_3_1_Contributors_and_Committers) to contribute to Eclipse CommaSuite.

## Eclipse CommaSuite
The domain-specific language of Eclipse CommaSuite allows the specification of the provided and required interfaces of a software component. Each interface is described by means of (1) a signature, i.e., the set of commands, signals and notifications that are offered by a server, (2) a protocol state machine which describes the allowed sequences of interaction events between clients and server, (3) timing constraints on the occurrence of the events, and (4) data constraints on the values communicated.  For a component, constraints can be added on the relations between its interfaces.

For more information, see:
- Project home: https://www.eclipse.org/comma
- Website: https://eclipse.org/comma

## Developer resources
For more information regarding source code management, builds, setting up a
developer environment, coding standards, how to contribute, and more, see:
- https://gitlab.eclipse.org/eclipse/comma/comma/-/blob/main/docs/developing.md

The project maintains the following source code repositories:
- https://gitlab.eclipse.org/eclipse/comma/comma.git

These can also be accessed via a web interface:
- https://gitlab.eclipse.org/eclipse/comma/comma

This project uses GitLab to track ongoing development and issues:
- https://gitlab.eclipse.org/eclipse/comma/comma/-/milestones
- https://gitlab.eclipse.org/eclipse/comma/comma/-/issues

Be sure to search for existing issues before you create another one. Remember
that contributions are always welcome!

To contribute source code (e.g. patches), please use GitLab:
- https://gitlab.eclipse.org/eclipse/comma/comma/-/merge_requests

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team, contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

- http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project’s 'dev' list.
- https://accounts.eclipse.org/mailing-list/comma-dev
