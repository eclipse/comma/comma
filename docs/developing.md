# Developing

## How to contribute
In order to contribute the following steps have to be taken:
1. Create an [issue](https://gitlab.eclipse.org/eclipse/comma/comma/-/issues/new) and describe what you are going to change
    - If no details are available because you have to do refinement add details afterwards
    - The description must not be a technical specification, but it should make clear what changes or maybe why.
    - You can keep the ticket up to date, if things change please update the issue.
    - Add links of documentation, articles, etc.
1. Create a branch which is related to the ticket. The branch should be named: `feature/issue-[ISSUE_NUMBER]` (e.g. `feature/issue-1`).
    - Whenever new packages are added make sure to document them in the [Archive structure section](./docs/developing.md).
    - Until you are finished, make sure the merge request title starts with `WIP:` , this indicates the merge request is still work in progress and should not be merged yet.
    - Make sure every commit starts with the issue number, e.g. `issue-1: my comment`,  alternatively the merge request can be squash merged which combines all commits.
    - Jenkins builds of your merge requests can be found [here](https://ci.eclipse.org/comma/job/Eclipse%20CommaSuite%20CI/view/change-requests/)
1. Once implementation has been finished remove the `WIP:` prefix and ask for review.

## Tips for development
### Setup development environment
See [Setup development environment](setup_dev_env.md)

### Building with Maven
To build CommaSuite with Maven execute the following command in the root: `mvn clean package`.
To also run the tests execute: `mvn clean verify`.
In case you also want to build the dashboards add `-Pdashboard`, e.g. `mvn clean package -Pdashboard`. Note that this requires Node.js to be installed.

### Coverage Report
To run the coverage report execute the command: `mvn clean verify -Pcoverage`.

### License header
The Maven build uses [license-maven-plugin](https://github.com/mycila/license-maven-plugin) to determine if the correct license headers are used for source files. If the header is incorrect the build fails.

To fix and add missing license headers from Eclipse; run `org.eclipse.comma.standard.launch/FixLicenseHeader.launch`.

Handy commands:
- To only run the check execute: `mvn license:check -Dtycho.mode=maven`.
- To automatically add/update execute: `mvn license:format -Dtycho.mode=maven`.

### Eclipse Dash License Tool
The [Eclipse Dash License Tool](https://github.com/eclipse/dash-licenses) checks if all used dependencies are allowed by Eclipse. First download the latest jar  from [here](https://repo.eclipse.org/content/repositories/dash-licenses-snapshots/org/eclipse/dash/org.eclipse.dash.licenses/) and save it as `dash.jar` in the root of this repository. Then execute it: 

```bash
mvn clean package dependency:list > build.log

# Check build.log to make sure if build is OK
cat build.log | grep -Poh "\S+:(system|provided|compile)" | sort | uniq | java -jar dash.jar -

# To run for the dashboards
java -jar dash.jar lib/org.eclipse.comma.monitoring.dashboard/app/package-lock.json
java -jar dash.jar lib/org.eclipse.comma.modelqualitychecks/app/package-lock.json
```

### Third party notice
`NOTICE-THIRD-PARTY.md` contains all third party dependencies. This file is automatically updated by the Jenkins CI pipeline.
To update manually execute the following command in the root of this archive (requires Python 3).

```
mvn clean package dependency:list > build.log
python lib/org.eclipse.comma.license/generate_notice.py build.log lib/org.eclipse.comma.monitoring.dashboard/app/dependencies.json,lib/org.eclipse.comma.modelqualitychecks/app/dependencies.json NOTICE-THIRD-PARTY.md
```

## Archive structure
- `.mvn`: Maven configuration
- `lib`
  - `org.eclipse.comma.actions*`: Grammar definition of actions (e.g. `Command`, `Signal`) + PlantUML generator
  - `org.eclipse.comma.behavior*`: Grammar definition of behavior (e.g. Data/timing constraints, StateMachine) + PlantUML generator
  - `org.eclipse.comma.behavior.component*`: Grammar definition of behavior (`.component` files)
  - `org.eclipse.comma.behavior.interfaces*`: Grammar definition of interfaces (`.interface` files)
  - `org.eclipse.comma.expressions*`: Grammar definition of expressions (e.g. `And`, `Or`, `+`, `/`) + PlantUML generator
  - `org.eclipse.comma.help`: Contains the help which can be shown via Eclipse -> Help -> Help Contents
  - `org.eclipse.comma.icons`: Contains icons used in the IDE (e.g. `.prj` file icon)
  - `org.eclipse.comma.java*`: Library to compile and execute Java code
  - `org.eclipse.comma.lib.feature`: Feature containing all projects under `lib/`
  - `org.eclipse.comma.license`: License features for other features, also contains other utilities related to licensing (e.g. license headers)
  - `org.eclipse.comma.monitoring*`: Generator to generate Java monitoring code from a CommaSuite model.
  - `org.eclipse.comma.monitoring.dashboard`: React application based on Node.js to visualize monitoring results
  - `org.eclipse.comma.parameters*`: Grammar definition of parameters (`.params` files)
  - `org.eclipse.comma.project*`: Grammar definition of project (`.prj` files)
  - `org.eclipse.comma.reachabilitygraph`: Generator to generate a reachability graph from a CommaSuite model (uses `org.eclipse.comma.petrinet`).
  - `org.eclipse.comma.modelqualitychecks*`: Generator to execute model quality checks on a CommaSuite model (uses `org.eclipse.comma.petrinet`).
  - `org.eclipse.comma.simulator*`: Generator to generate a simulator for CommaSuite models (uses `org.eclipse.comma.petrinet`).
  - `org.eclipse.comma.petrinet*`: Library for generating Petrinets from CommaSuite component models
  - `org.eclipse.comma.testapplication`: Generator to generate a testapplication for CommaSuite models (uses `org.eclipse.comma.petrinet`).
  - `org.eclipse.comma.signature*`: Grammar definition of signatures (`.signature` files)
  - `org.eclipse.comma.testcases*`: Generator to generate test cases from a CommaSuite model (uses `org.eclipse.comma.petrinet`).
  - `org.eclipse.comma.traces.events*`: Grammar definition of event traces (`.event` files)
  - `org.eclipse.comma.types*`: Grammar definition of types (e.g. record, vector, int, enum)
- `libexec`
  - `org.eclipse.comma.python*`: Library to execute Python scripts
- `org.eclipse.comma.standard.branding`: Branding feature for `org.eclipse.comma.standard.feature`, contains CommaSuite binary logos and about blurbs.
- `org.eclipse.comma.standard.coverage.report`: Contains the JaCoCo coverage report by running `mvn clean verify -Pcoverage`
- `org.eclipse.comma.standard.examples`: Contains the examples which can be imported via Eclipse -> File -> New -> Example
- `org.eclipse.comma.standard.feature`: Feature containing CommaSuite standard plugins used by update site and product.
- `org.eclipse.comma.standard.launch`: Contains `.launch` files which can be used while developing CommaSuite
- `org.eclipse.comma.standard.product`: Contains CommaSuite standard product configuration
- `org.eclipse.comma.standard.project*`: Registers the `.prj` file extension. This has been separated from `org.eclipse.comma.project` to allow extension of the project grammar by third parties.
- `org.eclipse.comma.standard.site`: Contains CommaSuite standard update site configuration
- `org.eclipse.comma.standard.target`: Contains CommaSuite standard Eclipse platform target configurations.
- `org.eclipse.comma.standard.doc.template`: Plugin which allows to import the `Template.docx` by right clicking on the `.prj` file -> `Import Documentation Template`.
- `docs`: contains documentation for developers
- `pom.xml`: The root pom.xml file
- `typings`: Typing for Python library dependencies

## Creating a new release
See [release process](release_process.md)
