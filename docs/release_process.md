# Release process of the Eclipse Foundation

## Planning a new release
For more information see https://www.eclipse.org/projects/handbook/#release-review

1. Create a new release: login at http://www.eclipse.org/comma ; at the right Committer tools > Create a new release review ; to determine release date:
   - decide on the end of the review period, the  1st or 3rd Wednesday of the month
   - add two days to the date of previous point
   - ensure the release candidate and IP log can be ready 2 weeks before end of review period

2. Schedule the release review - not later than 2 weeks before end of review period:
   - build release candidate
   - submit IP log: on the release page, Committer tools (when logged in) > Generate IP Log ;
     review date is 1 week later
   - create release review: after login, on the release page "schedule a review for this release"
   - request PMC approval: first subscribe to the technology-pmc list to allow posting;
     next use Committer tools > Send Email to the PMC, e.g.
	 "The Eclipse CommaSuite project hereby requests approval for the x.y.z release of the project.
	  See  .../technology.comma/reviews/x.y.x-release-review . "
   - requested EMO (Eclipse Management Organization) to schedule release review and make it public;
     mail to emo.eclipse.org :
	 "The Eclipse CommaSuite project request review of the x.y.z release, the first release of the project.
	  See .../technology.comma/reviews/x.y.x-release-review . "

## Building a new release
To build a new release go to the following url: https://ci.eclipse.org/comma/job/Eclipse_CommaSuite_Create_Release/build?delay=0sec , fill in the version number next to `RELEASE_VERSION` and click "Build". The release version has to match the following format: `v0.0.0`, `v0.0.0.RC0` or `v0.0.0.M0` where `0` can be any and multiple numbers. The job will do the following:
- Upgrade all version numbers in `MANIFEST.MF` and `pom.xml` files and commit this to Git with a tag on it.
- Publish the release to https://download.eclipse.org/comma/
- Add `-SNAPSHOT` to the version number and commit this to Git.

## After a new release
- Create a new release in Gitlab with release notes: https://gitlab.eclipse.org/eclipse/comma/comma/-/releases/new
- Close the milestone: https://gitlab.eclipse.org/eclipse/comma/comma/-/milestones
- Send mail to the developers list
