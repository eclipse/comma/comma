# Setup development environment

This page explains how to setup your CommaSuite developer environment using Oomph.


1.  Make sure that you have a 64-bit version of Java JDK 11 installed. If not download from [Adoptium](https://adoptium.net/?variant=openjdk11&jvmVariant=hotspot) (Version: "Temurin 11 (LTS)") and install.
1.  Download the [Eclipse installer](https://www.eclipse.org/downloads/) and run it. <br/><img src="images/oomph1.png" alt="drawing" width="500"/>
1. In the right top, click on the menu (hamburger) button, select "ADVANCED MODE...".
1.  Select "Eclipse.org" -> "Eclipse Platform", set "Product Version" to "2021-12" and "Java 11+ VM" to the path were Adoptium was installed to in step 1. Click next. <br/><img src="images/oomph2.png" alt="drawing" width="500"/>
1.  Click the green "+" button (right top, right to "type filter text"), in "Catalog" select "Eclipse Projects", for "Resource URIs" fill in `https://gitlab.eclipse.org/eclipse/comma/comma/-/raw/main/comma.setup` and click "OK".
1. Select "Eclipse Projects" -> "\<User\>" -> "Eclipse CommaSuite" and click "Next". <br/><img src="images/oomph4.png" alt="drawing" width="500"/>
1. Click "Show all variables" and change the "Root install folder" and "Installation folder name" as desired. This will be the location where Eclipse is installed  and the Git repository is cloned, respectively. Click "Next".
1. Click "Finish", you might get a pop-up to accept the license, click "Accept Now". <br/><img src="images/oomph5.png" alt="drawing" width="500"/>
1. The Eclipse installation will now start. Once finished Eclipse will start and you can click the "Finish" button in the "Eclipse Installer" window.
    - If you get a pop-up about certificates, accept them.
1. Eclipse will start with a similar window called "Eclipse Updater". This will take some time since it builds CommaSuite. Once done click "Finish".
1. The develop environment is now ready. You can browse the code via: "Window" -> "Show View" -> "Project Explorer". <br/><img src="images/oomph6.png" alt="drawing" width="800"/>.
1. Some tips:
    - To push to the repository you **cannot** use your Eclipse password. You need to create a [personal access token](https://gitlab.eclipse.org/-/profile/personal_access_tokens) with the `write_repository` option enabled. This token can be used as password.
    - Eclipse can be started via the Eclipse executable (e.g. `eclipse.exe` under Windows). This can be found under the installation direction from step 7. If you don't remember this it is probably under the `<USER_DIRECTORY>/comma-main/eclipse` (e.g. `C:\Users\kanterskjhm\eclipse\comma-main\eclipse`).
    - You can launch the CommaSuite IDE by going to the "Project explorer" -> "org.eclipse.comma.standard.launch" -> right click "Comma_Runtime.launch" -> "Run As" -> "Comma_Runtime".
    - To run the tests go to the "Project explorer" -> "org.eclipse.comma.standard.launch" -> "tests" -> right click "Comma_Test_All.launch" -> "Run As" -> "Comma_Test_All".
    - A Maven build can be triggered by going to the "Project explorer" -> "org.eclipse.comma.standard.launch" -> right click "MavenBuild.launch" -> "Run As" -> "MavenBuild". Once the build is finished you can find e.g. the standalone product under `org.eclipse.comma.standard.product\target\products`.
    - To regenerate the help (`org.eclipse.comma.help`) only (Asciidoc, takes +- 20 seconds), go to the "Project explorer" -> "org.eclipse.comma.standard.launch" -> right click "GenerateHelp.launch" -> "Run As"->  "GenerateHelp".