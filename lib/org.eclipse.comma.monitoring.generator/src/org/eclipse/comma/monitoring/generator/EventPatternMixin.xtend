/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import org.eclipse.comma.actions.actions.EVENT_KIND
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.behavior.Port
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.component.component.AnyEvent
import org.eclipse.comma.behavior.component.component.CommandEvent
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.ComponentPart
import org.eclipse.comma.behavior.component.component.NotificationEvent
import org.eclipse.comma.behavior.component.component.SignalEvent
import org.eclipse.xtext.generator.IFileSystemAccess

class EventPatternMixin extends ConstraintsRulesGenerator {
	new(AbstractBehavior behavior, IFileSystemAccess fsa) {
		super(behavior, fsa)
	}
	
	def dispatch CharSequence generateEvent(AnyEvent e){
		if(e.kind == EVENT_KIND::EVENT) return '''new CMessagePattern("*", «e.part === null ? '''componentInstanceName, componentInstanceName''' : '''componentInstanceName + ".«e.part.name»", componentInstanceName + ".«e.part.name»"'''», "«e.port.name»", "«e.port.name»")'''
		if(e.kind == EVENT_KIND::CALL) return '''new CCommandPattern("*", «componentFragment(e.part, e.port, true)», «portFragment(e.port, true)»)«IF e.nameVar !== null».setEventNameVar("«e.nameVar.variable.name»")«ENDIF»'''
		if(e.kind == EVENT_KIND::SIGNAL) return '''new CSignalPattern("*", «componentFragment(e.part, e.port, true)», «portFragment(e.port, true)»)«IF e.nameVar !== null».setEventNameVar("«e.nameVar.variable.name»")«ENDIF»'''
		if(e.kind == EVENT_KIND::NOTIFICATION) return '''new CNotificationPattern("*", «componentFragment(e.part, e.port, false)», «portFragment(e.port, false)»)«IF e.nameVar !== null».setEventNameVar("«e.nameVar.variable.name»")«ENDIF»'''
		''''''
	}
	
	def portFragment(Port port, boolean isCall){
	    if(port instanceof ProvidedPort){
	        if(isCall) {
	           '''"*", "«port.name»"''' 
	        } else {
	           '''"«port.name»", "*"'''
	        }
	    } else {
	       if(isCall) {
	          '''"«port.name»", "*"'''
	       } else {
	          '''"*", "«port.name»"'''
	       }
	    }
	}
	
	def componentFragment(ComponentPart part, Port port, boolean isCall){
	    val componentId =
	       if(part === null) {'''componentInstanceName'''}
	       else {'''componentInstanceName + "." + "«part.name»"'''} 
        if(port instanceof ProvidedPort){
            if(isCall) {
               '''"*", «componentId»''' 
            } else {
               '''«componentId», "*"'''
            }
        } else {
           if(isCall) {
              '''«componentId», "*"'''
           } else {
              '''"*", «componentId»'''
           }
        }
    }
	
	def dispatch CharSequence generateEvent(CommandEvent e)
	'''(CCommandPattern)new CCommandPattern("«e.event.name»", «componentFragment(e.part, e.port, true)», «portFragment(e.port, true)»)«generateParametersFragment(e)»'''

	def dispatch CharSequence generateEvent(CommandReply e)
	'''(CReplyPattern)new CReplyPattern("*", «componentFragment(e.part, e.port, false)», «portFragment(e.port, false)»)«IF e.nameVar !== null».setEventNameVar("«e.nameVar.variable.name»")«ENDIF»«IF e.command !== null».setCommand(«generateCommandInReplyContext(e.command, e.part, e.port)»)«ENDIF»«generateParametersFragment(e)»'''
	
	def dispatch CharSequence generateEvent(NotificationEvent e)
	'''(CNotificationPattern)new CNotificationPattern("«e.event.name»", «componentFragment(e.part, e.port, false)», «portFragment(e.port, false)»)«generateParametersFragment(e)»'''
	
	def dispatch CharSequence generateEvent(SignalEvent e)
	'''(CSignalPattern)new CSignalPattern("«e.event.name»", «componentFragment(e.part, e.port, true)», «portFragment(e.port, true)»)«generateParametersFragment(e)»'''
	
	def CharSequence generateCommandInReplyContext(org.eclipse.comma.actions.actions.CommandEvent e, ComponentPart part, Port port)
	'''(CCommandPattern)new CCommandPattern("«e.event.name»", «componentFragment(part, port, true)», «portFragment(port, true)»)«generateParametersFragment(e)»'''

    override constraintsClassVariables()
    '''
    private String componentInstanceName;
    
    '''
    
    override constraintsClassVariablesInit()
    '''this.componentInstanceName = componentInstanceName;'''
    
    override constraintsClassConstructorParams()
    '''String componentInstanceName'''
    
    override constraintsClassConstructorCallParams()
    '''componentInstanceName'''
}
