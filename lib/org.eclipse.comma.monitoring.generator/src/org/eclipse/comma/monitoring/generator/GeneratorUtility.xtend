/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.behavior.behavior.Port
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.PortReference
import org.eclipse.comma.monitoring.lib.CPortInstance
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.comma.monitoring.lib.CPartDescriptor
import org.eclipse.comma.monitoring.lib.CEntryPoint
import java.util.Map

class GeneratorUtility {
    def static List<List<CPortInstance>> getPortPaths(Component c) {
        val result = new ArrayList<List<CPortInstance>>()
        //iterate over parts
        for(part : c.parts) {
            val partPaths = part.componentType.portPaths
            for(path : partPaths) {
                path.forEach[it.componentInstance = "." + part.name + it.componentInstance]
            }
            result.addAll(partPaths)
        }
        //iterate over all ports
        for(port : c.ports) {
            val connectedPort = port.getConnectedPort(c)
            if(connectedPort === null) {
                val newPath = new ArrayList<CPortInstance>()
                newPath.add(new CPortInstance("", port.name))
                result.add(newPath)
            } else {
                EcoreUtil.resolveAll(connectedPort)
                for(path : result) {
                    if(path.get(0).componentInstance.startsWith("." + connectedPort.part.name) &&
                       path.get(0).port.equals(connectedPort.port.name)) {
                           path.reverse
                           path.add(new CPortInstance("", port.name))
                           path.reverse //TODO shall we use a reverse order in any case?
                    }
                }
            }
        }
        result
    }
    
    def static PortReference getConnectedPort(Port port, Component c){
        for(connection : c.connections) {
            if(connection.firstEnd.part === null && connection.firstEnd.port === port) return connection.secondEnd
            if(connection.secondEnd.part === null && connection.secondEnd.port === port) return connection.firstEnd
        }
        return null
    }
    
    def static List<CPartDescriptor> getPartDescriptors(Component c) {
        val result = new ArrayList<CPartDescriptor>()
        for(part : c.parts){
            result.add(new CPartDescriptor("." + part.name, part.componentType.name))
            val subParts = getPartDescriptors(part.componentType)
            subParts.forEach[it.partId = "." + part.name + it.partId]
            result.addAll(subParts)
        }
        result
    }
    
    def static List<CEntryPoint> getEntryPoints(Map<String, Component> instances, List<String> monitoredInstances) {
        val result = new ArrayList<CEntryPoint>()
        for(instance : monitoredInstances){
            for(port : instances.get(instance).ports) {
                val entry = getEntryPoint(instances, instance, port)
                entry.actualComponentInstance = instance
                entry.actualPort = port.name
                result.add(entry)
            }   
        }
        result
    }
    
    def static CEntryPoint getEntryPoint(Map<String, Component> instances, String instance, Port port){
        if(!instance.contains('.')) return new CEntryPoint(instance, port.name, "", "");//top level instance
        //get parent and parts
        val dotIndex  = instance.lastIndexOf('.')
        val parent = instance.substring(0, dotIndex)
        val part  = instance.substring(dotIndex + 1)
        val parentType = instances.get(parent)
        for(connection : parentType.connections) {
            //check the first end
            if(connection.firstEnd.part !== null) {
                if(connection.firstEnd.part.name.equals(part) && connection.firstEnd.port.name.equals(port.name)) {
                    //first end matches
                    if(connection.secondEnd.part === null) return getEntryPoint(instances, parent, connection.secondEnd.port)
                    else return new CEntryPoint(instance, port.name, "", "")
                }
            }
            //check second end
            if(connection.secondEnd.part !== null) {
                if(connection.secondEnd.part.name.equals(part) && connection.secondEnd.port.name.equals(port.name)) {
                    //second end matches
                    if(connection.firstEnd.part === null) return getEntryPoint(instances, parent, connection.firstEnd.port)
                    else return new CEntryPoint(instance, port.name, "", "")
                }
            }
        }
        new CEntryPoint(instance, port.name, "", "")     
    }
}