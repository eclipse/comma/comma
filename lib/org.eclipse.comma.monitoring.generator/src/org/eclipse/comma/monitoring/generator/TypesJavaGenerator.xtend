/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.io.File
import java.util.List
import org.eclipse.comma.types.types.EnumTypeDecl
import org.eclipse.comma.types.types.MapTypeConstructor
import org.eclipse.comma.types.types.MapTypeDecl
import org.eclipse.comma.types.types.RecordTypeDecl
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.Type
import org.eclipse.comma.types.types.TypeDecl
import org.eclipse.comma.types.types.TypeReference
import org.eclipse.comma.types.types.VectorTypeConstructor
import org.eclipse.comma.types.types.VectorTypeDecl
import static extension org.eclipse.comma.types.utilities.TypeUtilities.*
import static extension org.eclipse.comma.types.utilities.CommaUtilities.*

class TypesJavaGenerator {
	
	final protected static List<String> packageFragments = # ["org", "eclipse", "comma", "monitoring", "generated"]
	final protected static String rootPackage = '''org.eclipse.comma.monitoring.lib'''
	
	protected static def generatedPackage(){
		'''package «packageFragments.join(".")»;'''
	}

	protected static def generatedFileName(String name){
		packageFragments.join(File.separator) + File.separator + name + ".java"
	}

	/*
	 * Default Java values per type
	 */
	def dispatch CharSequence defaultValue(SimpleTypeDecl t){
		if(t.name.equals('int')) return '''0L'''
		if(t.name.equals('bool')) return '''true'''
		if(t.name.equals('string')) return '''""'''
		if(t.name.equals('real')) return '''0.0'''
		if(t.name.equals('bulkdata')) return '''new CBulkdata(0)'''
		if(t.name.equals('id')) return '''""'''
		if(t.base !== null) return defaultValue(t.base)
		return '''null'''
	}
	
	def dispatch CharSequence defaultValue(EnumTypeDecl t)
	'''new «toJavaType(t)»("«t.name»::«t.literals.get(0).name»")''' 
	
	def dispatch CharSequence defaultValue(TypeReference t)
	'''«defaultValue(t.type)»'''
	
	def dispatch CharSequence defaultValue(RecordTypeDecl t) 
	'''new «toJavaType(t)»("«t.fullyQualifiedName.toString("_")»")«FOR f : t.allFields.reverseView».setValueOfField("«f.name»", «defaultValue(f.type)»)«ENDFOR»'''
	
	
	def dispatch CharSequence defaultValue(VectorTypeDecl t) '''new «toJavaType(t)»()'''
	
	def dispatch CharSequence defaultValue(VectorTypeConstructor t) '''new «toJavaType(t)»()'''
	
	def dispatch CharSequence defaultValue(MapTypeDecl t) '''new «toJavaType(t)»()'''
	
	def dispatch CharSequence defaultValue(MapTypeConstructor t) '''new «toJavaType(t)»()'''
	
	
	/*
	 * Mappings of ComMA types to Java types
	 */
	 
	//mapping to Java when the type appears in variable declarations and type conversions
	def dispatch CharSequence toJavaType(EnumTypeDecl t)
 	'''CEnumValue'''	

	def dispatch CharSequence toJavaType(SimpleTypeDecl t){
		if(t.name.equals('int')) return '''long'''
		if(t.name.equals('bool')) return '''boolean'''
		if(t.name.equals('string')) return '''String'''
		if(t.name.equals('real')) return '''double'''
		if(t.name.equals('bulkdata')) return '''CBulkdata''' 
		if(t.name.equals('id')) return '''String'''
		if(t.base !== null) return toJavaType(t.base)
		
		return '''Object'''
	}
		
	def dispatch CharSequence toJavaType(TypeReference t)
	'''«toJavaType(t.type)»'''
	
	def dispatch CharSequence toJavaType(RecordTypeDecl t){
	    '''CRecord'''
	}
	
		
	def dispatch CharSequence toJavaType(VectorTypeDecl t){
		toJavaType(t.constructor)
	}
	
	def dispatch CharSequence toJavaType(VectorTypeConstructor t){
		var result = toJavaReferenceType(t.baseType)
		for(var i = 0; i < t.getDimensions().size; i++){
			result = "CVector<" + result + ">"
		}
		result
	}
	
	def dispatch CharSequence toJavaType(MapTypeDecl t) '''«toJavaType(t.constructor)»'''
	
	def dispatch CharSequence toJavaType(MapTypeConstructor t) '''CMap<«toJavaReferenceType(t.type)», «toJavaReferenceType(t.valueType)»>'''
	
	//mapping to Java when the type must be a reference type
	def dispatch CharSequence toJavaReferenceType(Type t){
		if(t instanceof TypeReference) {
			toJavaReferenceType(t.type)
		} else {
			toJavaType(t)
		}
	}
	
	def dispatch CharSequence toJavaReferenceType(TypeDecl t){
		switch(t.name){
			case "int" : return '''Long'''
			case "bool" : return '''Boolean'''
			case "real" : return '''Double'''
			default : {
				if(t instanceof SimpleTypeDecl){if(t.base !== null) return toJavaReferenceType(t.base)}
				return toJavaType(t)
			}
		}
	}
}
