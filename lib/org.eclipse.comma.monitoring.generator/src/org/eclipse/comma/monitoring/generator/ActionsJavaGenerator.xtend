/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction
import org.eclipse.comma.expressions.expression.ExpressionRecordAccess

abstract class ActionsJavaGenerator extends ExpressionsJavaGenerator {
	
	def dispatch CharSequence generateAction(AssignmentAction a){
		switch(a.assignment.commaScope){
			case GLOBAL : 
				if(a.commaScope == CommaScope::GLOBAL)
					'''«VAR_NAME_PREFIX»«a.assignment.name» = «generateWithCopy(a.exp)»;'''
				else
					'''stateOfDecisionClass.set_«VAR_NAME_PREFIX»«a.assignment.name»(«generateWithCopy(a.exp)»);'''
			case TRANSITION : '''«TVAR_NAME_PREFIX»«a.assignment.name» = «generateWithCopy(a.exp)»;'''
			case QUANTIFIER : '''new Exception("Assignment not allowed for quantifier variable «a.assignment.name»");'''
		}
	}
	
	def dispatch CharSequence generateAction(IfAction ifact)
 	'''
		if(«generateExpression(ifact.guard)»){
			«FOR a : ifact.thenList.actions»
				«generateAction(a)»;
			«ENDFOR»
		}«IF ifact.elseList !== null»else{
				«FOR a : ifact.elseList.actions»
					«generateAction(a)»;
				«ENDFOR»
		}«ENDIF»
	'''
	
	def dispatch CharSequence generateAction(RecordFieldAssignmentAction a){
        val navigationExp = a.fieldAccess as ExpressionRecordAccess //left-hand side
        var record = navigationExp.record
        var field = navigationExp.field
        
        '''«generateExpression(record)».setValueOfField("«field.name»",«generateWithCopy(a.exp)»);'''
	}
}
