/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.actions.actions.AnyEvent
import org.eclipse.comma.actions.actions.CommandEvent
import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.comma.actions.actions.EVENT_KIND
import org.eclipse.comma.actions.actions.EventPattern
import org.eclipse.comma.actions.actions.NotificationEvent
import org.eclipse.comma.actions.actions.ParameterizedEvent
import org.eclipse.comma.actions.actions.SignalEvent
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.behavior.BracketFormula
import org.eclipse.comma.behavior.behavior.ConditionalFollow
import org.eclipse.comma.behavior.behavior.ConditionedAbsenceOfEvent
import org.eclipse.comma.behavior.behavior.ConditionedEvent
import org.eclipse.comma.behavior.behavior.Conjunction
import org.eclipse.comma.behavior.behavior.Connector
import org.eclipse.comma.behavior.behavior.ConnectorOperator
import org.eclipse.comma.behavior.behavior.ConstraintSequence
import org.eclipse.comma.behavior.behavior.DataConstraint
import org.eclipse.comma.behavior.behavior.DataConstraintEvent
import org.eclipse.comma.behavior.behavior.DataConstraintUntilOperator
import org.eclipse.comma.behavior.behavior.Disjunction
import org.eclipse.comma.behavior.behavior.ESDisjunction
import org.eclipse.comma.behavior.behavior.EventInState
import org.eclipse.comma.behavior.behavior.EventInterval
import org.eclipse.comma.behavior.behavior.EventSelector
import org.eclipse.comma.behavior.behavior.GenericConstraint
import org.eclipse.comma.behavior.behavior.GenericConstraintsBlock
import org.eclipse.comma.behavior.behavior.GroupTimeConstraint
import org.eclipse.comma.behavior.behavior.Implication
import org.eclipse.comma.behavior.behavior.NegationFormula
import org.eclipse.comma.behavior.behavior.PeriodicEvent
import org.eclipse.comma.behavior.behavior.Sequence
import org.eclipse.comma.behavior.behavior.SingleTimeConstraint
import org.eclipse.comma.behavior.behavior.TimeConstraint
import org.eclipse.comma.behavior.behavior.TimeInterval
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.expressions.expression.Expression
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.types.NamedElement
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess

import static extension org.eclipse.comma.types.utilities.CommaUtilities.*

abstract class ConstraintsRulesGenerator extends AbstractBehaviorGenerator {
	public static final String OBSERVED_VAR_PREFIX = "observed_"
	protected final AbstractBehavior behavior
	protected final NamedElement rulesContainer
	protected List<Expression> genericRulesConditions	
	final protected IFileSystemAccess fsa;
	
	new (AbstractBehavior behavior, IFileSystemAccess fsa) {
		this.behavior = behavior
		this.rulesContainer = behavior as NamedElement
		this.fsa = fsa
	}
	
	def static rulesClassName(NamedElement rulesContainer)
	'''«rulesContainer.fullyQualifiedName.toString("_")»Rules'''
	
	def ruleClassName(NamedElement rulesContainer, NamedElement rule){
		val ruleNameSuffix = 
			switch(rule){
				TimeConstraint : "TimeRule"
				DataConstraint : "DataRule"
				GenericConstraint : "Formula"
				default : "TimeRule"
			}
		'''«rulesContainer.fullyQualifiedName.toString("_")»«rule.name»«ruleNameSuffix»'''
	}
	
	def constraintsClassProviderContent(AbstractBehavior intdef)
	'''
		«generatedPackage»
		
		import java.util.ArrayList;
		import java.util.List;
		import java.util.regex.Pattern;
		«IF intdef.containsPrecondition»
		import java.util.function.BiFunction;
		import java.io.Serializable;
		import «rootPackage».CState;
		«ENDIF»
		import «rootPackage».constraints.*;
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		
		public class «rulesClassName(intdef as NamedElement)» {
			private List<CRule> rules;
			«constraintsClassVariables»
			public «rulesClassName(intdef as NamedElement)»(«constraintsClassConstructorParams»){
			    «constraintsClassVariablesInit»
				rules = new ArrayList<CRule>();
				«IF intdef.timeConstraintsBlock !== null»
					«FOR r : intdef.timeConstraintsBlock.timeConstraints»
						«IF r instanceof SingleTimeConstraint»
							rules.add(getTimeRule_«r.name»());
						«ELSE»
							rules.add(new «ruleClassName(intdef as NamedElement, r)»(«constraintsClassConstructorCallParams»));
						«ENDIF»
					«ENDFOR»
				«ENDIF»
				«IF intdef.dataConstraintsBlock !== null»
					«FOR r : intdef.dataConstraintsBlock.dataConstraints»
						rules.add(new «ruleClassName(intdef as NamedElement, r)»(«constraintsClassConstructorCallParams»));
					«ENDFOR»
				«ENDIF»
				«IF intdef.genericConstraintsBlock !== null»
					«FOR r : intdef.genericConstraintsBlock.genericConstraints»
						rules.add(new «ruleClassName(intdef as NamedElement, r)»(«constraintsClassConstructorCallParams»));
					«ENDFOR»
				«ENDIF»
			}
			
			public List<CRule> getRules(){
				return rules;
			}
			«IF intdef.timeConstraintsBlock !== null»
				«FOR r : intdef.timeConstraintsBlock.timeConstraints»
					«IF r instanceof SingleTimeConstraint»
						
						«singleTimeConstraintToJavaMethod(r.constraint, r)»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
		}
	'''
	
	def constraintsClassVariables()
	''''''
	
	def constraintsClassVariablesInit()
	''''''
	
	def constraintsClassConstructorParams()
	''''''
	
	def constraintsClassConstructorCallParams()
    ''''''
	
	// Assumption: the pattern is always used in an interface
	// The grammar of other languages ensures this
	def preconditionLambda(EventInState pattern)
	'''
	«var interface = EcoreUtil2::getContainerOfType(pattern, Interface)»
	(BiFunction<CState, List<Object>, Boolean> & Serializable) (env, params) -> {«utilityClassName(interface)» stateOfDecisionClass = («utilityClassName(interface)») env; return «generateExpression(pattern.preCondition)»; }
	'''
	
	def messagePattern(EventInState pattern, String patternVarName)
	'''
	CMessagePattern «patternVarName» = «generateEvent(pattern.event)»;
	'''
	
	def dispatch singleTimeConstraintToJavaMethod(EventInterval r, TimeConstraint rule) '''
		private CRule getTimeRule_«rule.name»() {
		    «messagePattern(r.condition, "firstEvent")»
		    «messagePattern(r.event, "secondEvent")»
			CEventIntervalTimeRule rule = new CEventIntervalTimeRule(firstEvent, secondEvent);
			rule.setName("«rule.name»");
			rule.getEnvironment().setInterval(«interval(r.interval)»);
			return rule;
		}
	'''
	
	def dispatch singleTimeConstraintToJavaMethod(ConditionedAbsenceOfEvent r, TimeConstraint rule) '''
		private CRule getTimeRule_«rule.name»() {
		    «messagePattern(r.condition, "triggeringEvent")»
		    «messagePattern(r.event, "missingEvent")»
			CConditionedAbsenceOfEventTimeRule rule = new CConditionedAbsenceOfEventTimeRule(triggeringEvent, missingEvent);
			rule.setName("«rule.name»");
			rule.getEnvironment().setInterval(«interval(r.interval)»).setIntervalEnd(«IF r.interval.end !== null»«generateExpression(r.interval.end)»«ELSE»-1«ENDIF»);
			return rule;
		}
	'''
	
	def dispatch singleTimeConstraintToJavaMethod(ConditionedEvent r, TimeConstraint rule) '''
		private CRule getTimeRule_«rule.name»() {
		    «messagePattern(r.condition, "trigger")»
		    «messagePattern(r.event, "expected")»
			CConditionedEventTimeRule rule = new CConditionedEventTimeRule(trigger, expected);
			rule.setName("«rule.name»");
			rule.getEnvironment().setInterval(«interval(r.interval)»).setIntervalEnd(«IF r.interval.end !== null»«generateExpression(r.interval.end)»«ELSE»-1«ENDIF»);
			return rule;
		}
	'''
	
	def dispatch singleTimeConstraintToJavaMethod(PeriodicEvent r, TimeConstraint rule) '''
		private CRule getTimeRule_«rule.name»() {
		    «messagePattern(r.condition, "trigger")»
			CMessagePattern periodic = «generateEvent(r.event)»;
			«IF r.stopEvent !== null»
			    «messagePattern(r.stopEvent, "stopPattern")»
				CEventSelector stop = new CEventSelector().setEvent(stopPattern);
			«ELSE»
				CEventSelector stop = new CEventSelector().setEvent(new CMessagePattern()).setNegated();
			«ENDIF»
			CPeriodicTimeRule rule = new CPeriodicTimeRule(trigger, periodic, stop);
			rule.setName("«rule.name»");
			rule.getEnvironment().setVariableValue("period", «generateExpression(r.period)»).setVariableValue("jitter", «generateExpression(r.jitter)»);
			return rule;
		}
	'''
	
	def dataRulesEnvironmentClassContent()
	'''
		«generatedPackage»
		
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		import java.util.HashMap;
		import java.util.Map;
		import java.util.regex.Pattern;
		
		public class «rulesContainer.fullyQualifiedName.toString("_")»DataRulesEnvironment extends CEnvironment {
			private double t;
			private Map<String, Boolean> conditionsCheck = new HashMap<String, Boolean>();
			«FOR v : behavior.dataConstraintsBlock.vars»
				private «toJavaType(v.type)» «VAR_NAME_PREFIX»«v.name»;
			«ENDFOR»
			
			public CEnvironment setVariableValue(String name, Object value) {
				if (name.equals("t")){t = (double) value;}
				«FOR v : behavior.dataConstraintsBlock.vars»
					if(name.equals("«v.name»")) {«VAR_NAME_PREFIX»«v.name» = («toJavaType(v.type)») value;}
				«ENDFOR»
				return this;
			}
				
			«IF quantifiersInMachines.size > 0»
				«FOR quantifier : quantifiersInMachines»
					«generateQuantifierMethod(quantifier, quantifiersInMachines.indexOf(quantifier))»
				«ENDFOR»
			«ENDIF»
			
			public boolean checkCondition(int index, CObservedMessage message) {
				«FOR i : 0..< behavior.dataConstraintsBlock.dataConstraints.size»
					if(index == «i + 1») {
					    conditionsCheck.put("«behavior.dataConstraintsBlock.dataConstraints.get(i).name»", true);
					    return «generateExpression(behavior.dataConstraintsBlock.dataConstraints.get(i).condition)»;
					}
				«ENDFOR»
				return false;
			}
		
			«FOR rule : behavior.dataConstraintsBlock.dataConstraints»
			«IF ! rule.observedValues.empty»
				public Map<String, Object> observedValuesFor«rule.name»(){
					Map<String, Object> map = new HashMap<String, Object>();
					if(!conditionsCheck.containsKey("«rule.name»")){return map;}
					«FOR observedValue : rule.observedValues»
						map.put("«rule.name»_«observedValue.name»", «generateExpression(observedValue.value)»);
					«ENDFOR»
					return map;
				}
			«ENDIF»
			«ENDFOR»
		}
	'''
	
	def dataRuleClassContent(DataConstraint dr)
	'''
		«generatedPackage»
		
		import java.util.Map;
		«IF dr.containsPrecondition»
		import java.util.List;
		import java.util.function.BiFunction;
		import java.io.Serializable;
		import java.util.regex.Pattern;
		import «rootPackage».CState;
		«ENDIF»
		import «rootPackage».constraints.*;
		import «rootPackage».messages.*;
		import «rootPackage».values.*;

		public class «ruleClassName(rulesContainer, dr)» extends CDataRule {
			private «rulesContainer.fullyQualifiedName.toString("_")»DataRulesEnvironment env;
			«constraintsClassVariables»
		
			public «ruleClassName(rulesContainer, dr)»(«constraintsClassConstructorParams») {
				name = "«dr.name»";
				errorMessage = "Data constraint is violated."; 
				env = new «rulesContainer.fullyQualifiedName.toString("_")»DataRulesEnvironment();
				«constraintsClassVariablesInit»
				
				formula = 
						new CConstraintSequence().
						setConditionIndex(«behavior.dataConstraintsBlock.dataConstraints.indexOf(dr) + 1»).
						setSequence(
							new CSequence()
							«dataConstraintToJava(dr)».setEnvironment(env));
			}
			
			public Map<String, Object> observedValues(){
				«IF ! dr.observedValues.empty»
					return env.observedValuesFor«dr.name»();
				«ELSE»
					return null;
				«ENDIF»
			}
		}
	'''
	
	def genericRulesEnvironmentClassContent()
	'''
		«generatedPackage»
		
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		import java.util.regex.Pattern;


		public class «rulesContainer.fullyQualifiedName.toString("_")»GenericRulesEnvironment extends CEnvironment {
			«FOR v : behavior.genericConstraintsBlock.vars»
				private «toJavaType(v.type)» «VAR_NAME_PREFIX»«v.name»;
			«ENDFOR»
			
			public CEnvironment setVariableValue(String name, Object value) {
				«FOR v : behavior.genericConstraintsBlock.vars»
					if(name.equals("«v.name»")) {«VAR_NAME_PREFIX»«v.name» = («toJavaType(v.type)») value;}
				«ENDFOR»
				return this;
			}
			
			«IF quantifiersInMachines.size > 0»
				«FOR quantifier : quantifiersInMachines»
					«generateQuantifierMethod(quantifier, quantifiersInMachines.indexOf(quantifier))»
				«ENDFOR»
			«ENDIF»
		
			public boolean checkCondition(int index, CObservedMessage message) {
			«FOR condition : genericRulesConditions»
				if(index == «genericRulesConditions.indexOf(condition) + 1») {return «generateExpression(condition)»;}
			«ENDFOR»
			return false;
			}
		}
	'''
	
	def genericRuleClassContent(GenericConstraint c)
	'''
		«generatedPackage»
		
		«IF c.containsPrecondition»
		import java.util.List;
		import java.util.function.BiFunction;
		import java.io.Serializable;
		import «rootPackage».CState;
		«ENDIF»		
		import «rootPackage».constraints.*;
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		import java.util.regex.Pattern;


		public class «ruleClassName(rulesContainer, c)» extends CGenericRule {
			private «rulesContainer.fullyQualifiedName.toString("_")»GenericRulesEnvironment env;
			«constraintsClassVariables»
		
			public «ruleClassName(rulesContainer, c)»(«constraintsClassConstructorParams») {
				name = "«c.name»";
				errorMessage = "Generic constraint is violated."; 
				env = new «rulesContainer.fullyQualifiedName.toString("_")»GenericRulesEnvironment();
				«constraintsClassVariablesInit»
				
				formula = 
						«formulaToJava(c.formula, genericRulesConditions)»;
			}
		}
	'''
	
	def dispatch groupTimeConstraintEnvironmentClassContent(ConditionedEvent first, GroupTimeConstraint tc)
	'''
		«generatedPackage»
			
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		
		public class «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment extends CEnvironment {
			private double t, t1, t2, «FOR i : 0..< (tc.followups.length) SEPARATOR ', '»t«3 + i»«ENDFOR»;
			private CInterval i1, «FOR i : 0..< (tc.followups.length) SEPARATOR ', '»i«2 + i»«ENDFOR»;
			
			public «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment() {
				i1 = «interval(first.interval)»;
				«FOR i : 0..< (tc.followups.length)»
					i«2 + i» = «interval(tc.followups.get(i).interval)»;
				«ENDFOR»
			}
											
			public CEnvironment	setVariableValue(String name, Object value) {
				if(name.equals("t")) {t = (double) value;}
				if(name.equals("t1")) {t1 = (double) value;}
				if(name.equals("t2")) {t2 = (double) value;}
				«FOR i : 0..< (tc.followups.length)»
					if(name.equals("t«3 + i»")) {t«3 + i» = (double) value;}
				«ENDFOR»
				
				return this;
			}
											
			public boolean checkCondition(int index, CObservedMessage message) {
				if(index == 1) {return «IF first.interval.end === null»true«ELSE»(t - t1) <= («generateExpression(first.interval.end)»)«ENDIF»;}
				if(index == 2) {return i1.isIn(t2 - t1);}
				«FOR i : 0..< (tc.followups.length)»
					if(index == «4 + (2*i -1)») {return «IF tc.followups.get(i).interval.end === null»true«ELSE»(t - t«3 + (i-1)») <= («generateExpression(tc.followups.get(i).interval.end)»)«ENDIF»;}
					if(index == «4 + 2*i») {return i«2 + i».isIn (t«3 + i» - t«3 + (i-1)»);}
				«ENDFOR»			
				return false;
			}
		}
	'''
	
	def dispatch groupTimeConstraintEnvironmentClassContent(EventInterval first, GroupTimeConstraint tc)
	'''
		«generatedPackage»
				
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		
		public class «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment extends CEnvironment {
			private double t, t1, t2, «FOR i : 0..< (tc.followups.length) SEPARATOR ', '»t«3 + i»«ENDFOR»;
			private CInterval i1, «FOR i : 0..< (tc.followups.length) SEPARATOR ', '»i«2 + i»«ENDFOR»;
										
			public «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment() {
				i1 = «interval(first.interval)»;
				«FOR i : 0..< (tc.followups.length)»
					i«2 + i» = «interval(tc.followups.get(i).interval)»;
				«ENDFOR»
			}
											
			public CEnvironment setVariableValue(String name, Object value) {
				if(name.equals("t")) {t = (double)value;}
				if(name.equals("t1")) {t1 = (double)value;}
				if(name.equals("t2")) {t2 = (double)value;}
				«FOR i : 0..< (tc.followups.length)»
					if(name.equals("t«3 + i»")) {t«3 + i» = (double)value;}
				«ENDFOR»
				
				return this;
			}
											
			public boolean checkCondition(int index, CObservedMessage message) {
				if(index == 2){return i1.isIn(t2 - t1);}
				«FOR i : 0..< (tc.followups.length)»
					if(index == «4 + (2*i -1)»){return «IF tc.followups.get(i).interval.end === null»true«ELSE»(t - t«3 + (i-1)») <= («generateExpression(tc.followups.get(i).interval.end)»)«ENDIF»;}
					if(index == «4 + 2*i»){return i«2 + i».isIn(t«3 + i» - t«3 + (i-1)»);}
				«ENDFOR»			
				return false;
			}
		}
	'''
	
	def dispatch groupTimeConstraintEnvironmentClassContent(ConditionedAbsenceOfEvent first, GroupTimeConstraint tc)
	'''
		«generatedPackage»
					
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		
		public class «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment extends CEnvironment {
			private double t1, t2, t3, t, «FOR i : 0..< (tc.followups.length) SEPARATOR ', '»t«4 + i»«ENDFOR»;
			private CInterval i1, «FOR i : 0..< (tc.followups.length) SEPARATOR ', '»i«2 + i»«ENDFOR»;
									
			public «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment() {
				i1 = «interval(first.interval)»;
				«FOR i : 0..< (tc.followups.length)»
					i«2 + i» = «interval(tc.followups.get(i).interval)»;
				«ENDFOR»
			}
										
			public CEnvironment setVariableValue(String name, Object value) {
				if(name.equals("t1")){t1 = (double) value;}
				if(name.equals("t2")){t2 = (double) value;}
				if(name.equals("t3")){t3 = (double) value;}
				if(name.equals("t")){t = (double) value;}
				«FOR i : 0..< (tc.followups.length)»
					if(name.equals("t«4 + i»")){t«4 + i» = (double)value;}
				«ENDFOR»
				
				return this;
			}
										
			public boolean checkCondition(int index, CObservedMessage message) {
				if(index == 1) {return «IF first.interval.end === null»true«ELSE»(t2 - t1) <= («generateExpression(first.interval.end)»)«ENDIF»;}
				if(index == 2) {return i1.isIn(t3 - t1);}
				if(index == 3) {return «IF tc.followups.get(0).interval.end === null»true«ELSE»(t - t1  - «generateExpression(first.interval.end)») <= («generateExpression(tc.followups.get(0).interval.end)»)«ENDIF»;}
				if(index == 4) {return i2.isIn(t4 - t1 - «generateExpression(first.interval.end)»);}
				«FOR i : 1..< (tc.followups.length)»
					if(index == «4 + (2*i -1)») {return «IF tc.followups.get(i).interval.end === null»true«ELSE»(t - t«4 + (i-1)») <= («generateExpression(tc.followups.get(i).interval.end)»)«ENDIF»;}
					if(index == «4 + 2*i») {return i«2 + i».isIn(t«4 + i» - t«4 + (i-1)»);}
				«ENDFOR»	
				
				return false;
			}
		}
	'''	

	def dispatch groupTimeConstraintClassContent(ConditionedEvent first, GroupTimeConstraint tc)
	'''
		«generatedPackage»
		
		«IF tc.containsPrecondition»
		import java.util.List;
		import java.util.function.BiFunction;
		import java.io.Serializable;
		import «rootPackage».CState;
		«ENDIF»
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		import «rootPackage».constraints.*;

		public class «ruleClassName(rulesContainer, tc)» extends CTimeRule {
		    «constraintsClassVariables»
		        
			public «ruleClassName(rulesContainer, tc)»(«constraintsClassConstructorParams») {
				«rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment env;
				CSequence s;
				CUntil u;
				CEventSelector es;
				CFormula post;
				
				«constraintsClassVariablesInit»
				name = "«tc.name»";
				errorMessage = "Event is not received in the expected interval.";
				env = new «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment();
				«messagePattern(first.condition, "e1")»
				es = (CEventSelector)new CEventSelector().setEvent(e1).setTimeVariable("t1").setEnvironment(env);
				s = new CSequence().addElement(es).setEnvironment(env);
				formula = new CConditionalFollow().setLeft(s);
				//Consequence
				«messagePattern(first.event, "e2")»
				es = (CEventSelector)new CEventSelector().setTimeVariable("t").setEvent(e2).setNegated().setConditionIndex(1).setEnvironment(env);
				u = (CUntil)new CUntil().setBody(es).setEnvironment(env);
				«messagePattern(first.event, "e3")»
				es = (CEventSelector)new CEventSelector().setTimeVariable("t2").setEvent(e3).setConditionIndex(2).setEnvironment(env);
				u.setStop(es);
				s = new CSequence().addElement(u).setEnvironment(env);
				//Followups
				«FOR i : 0..< (tc.followups.length)»
				«messagePattern(tc.followups.get(i).event, "e" + (4 + i))»
				es = (CEventSelector)new CEventSelector().setTimeVariable("t").setEvent(e«4+i»).setNegated().setConditionIndex(«3 + (2*i)»).setEnvironment(env);
				u = (CUntil) new CUntil().setBody(es).setEnvironment(env);
				es = (CEventSelector)new CEventSelector().setTimeVariable("t«3 + i»").setEvent(e«4+i»).setConditionIndex(«4 + 2*i»).setEnvironment(env);
				u.setStop(es);
				s.addElement(u);
				«ENDFOR»
				((CConditionalFollow)formula).setRight(s);
			}
		}
	'''
	
	def dispatch groupTimeConstraintClassContent(EventInterval first, GroupTimeConstraint tc)
	'''
		«generatedPackage»
		
		«IF tc.containsPrecondition»
		import java.util.List;
		import java.util.function.BiFunction;
		import java.io.Serializable;
		import «rootPackage».CState;
		«ENDIF»
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		import «rootPackage».constraints.*;


		public class «ruleClassName(rulesContainer, tc)» extends CTimeRule {
		    «constraintsClassVariables»
		    
			public «ruleClassName(rulesContainer, tc)»(«constraintsClassConstructorParams») {
				«rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment env;
				CSequence s;
				CUntil u;
				CEventSelector es;
										
				«constraintsClassVariablesInit»
				name = "«tc.name»";
				errorMessage = "Event is not received in the expected interval.";
				env = new «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment();
				«messagePattern(first.condition, "e1")»
				es = (CEventSelector)new CEventSelector().setTimeVariable("t1").setEvent(e1).setEnvironment(env);
				s = new CSequence().addElement(es).setEnvironment(env);
				«messagePattern(first.event, "e2")»
				es = (CEventSelector)new CEventSelector().setTimeVariable("t").setEvent(e2).setNegated().setEnvironment(env);
				u = (CUntil)new CUntil().setBody(es).setEnvironment(env);
				es = (CEventSelector)new CEventSelector().setTimeVariable("t2").setEvent(e2).setConditionIndex(2).setEnvironment(env);
				u.setStop(es);
				s.addElement(u);
				formula = new CConditionalFollow().setLeft(s);
				s = new CSequence().setEnvironment(env);
				//Followups
				«FOR i : 0..< (tc.followups.length)»
				«messagePattern(tc.followups.get(i).event, "e" + (3 + i))»
				es = (CEventSelector)new CEventSelector().setTimeVariable("t").setEvent(e«3 + i»).setNegated().setConditionIndex(«3 + (2*i)»).setEnvironment(env);
				u = (CUntil)new CUntil().setBody(es).setEnvironment(env);
				es = (CEventSelector)new CEventSelector().setTimeVariable("t«3 + i»").setEvent(e«3 + i»).setConditionIndex(«4 + 2*i»).setEnvironment(env);
				u.setStop(es);
				s.addElement(u);
				«ENDFOR»
				((CConditionalFollow)formula).setRight(s);
			}
		}
	'''         
	
	def dispatch groupTimeConstraintClassContent(ConditionedAbsenceOfEvent first, GroupTimeConstraint tc)
	'''
		«generatedPackage»
		
		«IF tc.containsPrecondition»
		import java.util.List;
		import java.util.function.BiFunction;
		import java.io.Serializable;
		import «rootPackage».CState;
		«ENDIF»
		import «rootPackage».messages.*;
		import «rootPackage».values.*;
		import «rootPackage».constraints.*;

		public class «ruleClassName(rulesContainer, tc)» extends CTimeRule {
		    «constraintsClassVariables»
		    
			public «ruleClassName(rulesContainer, tc)»(«constraintsClassConstructorParams») {
				«rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment env = new «rulesContainer.fullyQualifiedName.toString("_")»«tc.name»TimeRuleEnvironment();
				name = "«tc.name»";
				errorMessage = "Event is not received in the expected interval.";
				«constraintsClassVariablesInit»
				
				formula = new CImplication(). 
							setLeft(new CNegation().
							            setFormula(new CConditionalFollow(). 
							                           setLeft(new CSequence(). 
							                                      addElement((CEventSelector)new CEventSelector().setTimeVariable("t1").
							                                                 setEvent(«generateEvent(first.condition.event)»).
							                                                 setEnvironment(env)).
							                                      setEnvironment(env)).
							                           setRight(new CSequence().
							                                       addElement((CUntil)new CUntil().
							                                                  setBody((CEventSelector)new CEventSelector().setTimeVariable("t2").setEvent(«generateEvent(first.event.event)»).setNegated().setConditionIndex(1).setEnvironment(env)).
							                                                  setStop((CEventSelector)new CEventSelector().setTimeVariable("t3").setEvent(«generateEvent(first.event.event)»).setConditionIndex(2).setEnvironment(env)).
							                                                  setEnvironment(env)).
							                                       setEnvironment(env)))).
							setRight(new CConditionalFollow(). 
							         setLeft(new CSequence().
							                 addElement((CEventSelector)new CEventSelector().setTimeVariable("t1").setEvent(«generateEvent(first.condition.event)»).setEnvironment(env)).
							                 setEnvironment(env)).
							         setRight(new CSequence(). 
							                  addElement((CUntil)new CUntil(). 
							                              setBody((CEventSelector)new CEventSelector().setTimeVariable("t").setEvent(«generateEvent(tc.followups.get(0).event.event)»).setNegated().setConditionIndex(3).setEnvironment(env)).
							                              setStop((CEventSelector)new CEventSelector().setTimeVariable("t4").setEvent(«generateEvent(tc.followups.get(0).event.event)»).setConditionIndex(4).setEnvironment(env)).
							                              setEnvironment(env)).
							                  //More followups if any
							                  «FOR i : 1..< (tc.followups.length)»
							                  	addElement((CUntil)new CUntil(). 
							                  	            setBody((CEventSelector)new CEventSelector().setTimeVariable("t").setEvent(«generateEvent(tc.followups.get(i).event.event)»).setNegated().setConditionIndex(«4 + (2*i-1)»).setEnvironment(env)).
							                  	            setStop((CEventSelector)new CEventSelector().setTimeVariable("t«4 + i»").setEvent(«generateEvent(tc.followups.get(i).event.event)»).setConditionIndex(«4 + 2*i»).setEnvironment(env)).
							                  	            setEnvironment(env).
							                  «ENDFOR»
							                  setEnvironment(env)));
			}
		}
	'''
	
	/** stepToJava(Step, List<Expression>) */

	def dispatch CharSequence stepToJava(EventSelector s, List<Expression> conditions) 
	'''
		new CEventSelector().setEvent(«generateEvent(s.event.event)»)
			«IF s.counter !== null».setOccurenceVariable("«s.counter.variable.name»")«ENDIF»«IF s.negated !== null».setNegated()«ENDIF»«IF s.condition !== null».setConditionIndex(«conditions.indexOf(s.condition) + 1»)«ENDIF»
			.setTimeVariable("«s.timestamp.variable.name»").setEnvironment(env«IF s.counter !== null».setVariableValue("«s.counter.variable.name»", 0L)«ENDIF»)
	'''

	def dispatch CharSequence stepToJava(Connector c, List<Expression> conditions) 
	'''
	new CUntil()«IF c.conOperator == ConnectorOperator::WU».setWeak()«ENDIF»
		.setBody((CStep) «stepToJava(c.left, conditions)»)
		.setStop((CStep) «stepToJava(c.right, conditions)»)
		.setEnvironment(env)'''

	def dispatch CharSequence stepToJava(ESDisjunction s, List<Expression> conditions) 
	'''
	new CORStep()
		.addSelector((CStep)«stepToJava(s.left, conditions)»)
		.addSelector((CStep)«stepToJava(s.right, conditions)»)'''
	
	/** formulaToJava(Formula, List<Expression>) */
	
	def dispatch CharSequence formulaToJava(NegationFormula f, List<Expression> conditions)
	'''
	new CNegation().setFormula(
		«formulaToJava(f.sub, conditions)»)'''
	
	def dispatch CharSequence formulaToJava(BracketFormula f, List<Expression> conditions) 
	'''
	«formulaToJava(f.sub, conditions)»'''

	def dispatch CharSequence formulaToJava(Sequence f, List<Expression> conditions) 
	'''
		new CSequence()
		«FOR step : f.steps»
			.addElement(
				«stepToJava(step, conditions)»)
		«ENDFOR»
			.setEnvironment(env)«IF f.condition !== null».setConditionIndex(«conditions.indexOf(f.condition) + 1»)«ENDIF»
	'''

	def dispatch CharSequence formulaToJava(Conjunction f, List<Expression> conditions)
	'''
	new CConjunction()
		.setLeft(
			«formulaToJava(f.left, conditions)»)
		.setRight(
			«formulaToJava(f.right, conditions)»)'''

	def dispatch CharSequence formulaToJava(Disjunction f, List<Expression> conditions)
	'''
	new CDisjunction()
		.setLeft(
			«formulaToJava(f.left, conditions)»)
		.setRight(
			«formulaToJava(f.right, conditions)»)'''

	def dispatch CharSequence formulaToJava(Implication f, List<Expression> conditions) 
	'''
	new CImplication()
		.setLeft(
			«formulaToJava(f.left, conditions)»)
		.setRight(
			«formulaToJava(f.right, conditions)»)'''

	def dispatch CharSequence formulaToJava(ConditionalFollow f, List<Expression> conditions) 
	'''
	new CConditionalFollow()
		.setLeft(
			«formulaToJava(f.left, conditions)»)
		.setRight(
			«formulaToJava(f.right, conditions)»)'''

	def dispatch CharSequence formulaToJava(ConstraintSequence f, List<Expression> conditions) 
	'''
	new CConstraintSequence().setConditionIndex(«conditions.indexOf(f.cond) + 1»)
		.setSequence(
			«formulaToJava(f.left, conditions)»)'''
	
	/** generateEvent(CommunicationEvent) */
	
	def dispatch CharSequence generateEvent(AnyEvent e){
	    val messagePattern =
	    switch(e.kind) {
	        case EVENT_KIND::EVENT: '''new CMessagePattern()'''
	        case EVENT_KIND::CALL: '''new CCommandPattern("*")'''
	        case EVENT_KIND::SIGNAL: '''new CSignalPattern("*")'''
	        case EVENT_KIND::NOTIFICATION: '''new CNotificationPattern("*")'''
	        
	    }
	    
	    '''(CMessagePattern)«messagePattern»«IF e.nameVar !== null».setEventNameVar("«e.nameVar.variable.name»")«ENDIF»«generatePreconditionFragment(e)»«generateStatesFragment(e)»'''
	}
	
	def generateParametersFragment(ParameterizedEvent e)
	'''«FOR p : e.parameters».addParameter(«IF p instanceof ExpressionVariable»new CVariable("«p.variable.name»", «toJavaReferenceType(p.variable.type)».class)«ELSE»«generateExpression(p)»«ENDIF»)«ENDFOR»'''
	
	def generateStatesFragment(EventPattern e){
	    if(e.eContainer instanceof EventInState){
	        val parent = e.eContainer as EventInState
	        return '''«FOR s : parent.state».addState("«s.name»")«ENDFOR»'''
	    } else {
	        return ''''''
	    }    
	}
	
	def generatePreconditionFragment(EventPattern e){
        if(e.eContainer instanceof EventInState){
            val parent = e.eContainer as EventInState
            if(parent.preCondition !== null) {
                return '''.setPrecondition(«preconditionLambda(parent)»)'''   
            }
        }
        return ''''''
    }
	
	def generateEventCommonPart(EventPattern e)
	'''«generatePreconditionFragment(e)»«generateParametersFragment(e as ParameterizedEvent)»«generateStatesFragment(e)»'''
	
	def dispatch CharSequence generateEvent(CommandEvent e)
    '''(CCommandPattern)new CCommandPattern("«e.event.name»")«generateEventCommonPart(e)»'''

	def dispatch CharSequence generateEvent(CommandReply e)
	'''(CReplyPattern)new CReplyPattern()«IF e.nameVar !== null».setEventNameVar("«e.nameVar.variable.name»")«ENDIF»«IF e.command !== null».setCommand(«generateEvent(e.command)»)«ENDIF»«generateEventCommonPart(e)»'''
		
	def dispatch CharSequence generateEvent(NotificationEvent e)
	'''(CNotificationPattern)new CNotificationPattern("«e.event.name»")«generateEventCommonPart(e)»'''
	
	def dispatch CharSequence generateEvent(SignalEvent e)
	'''(CSignalPattern)new CSignalPattern("«e.event.name»")«generateEventCommonPart(e)»'''
	
	def dataConstraintToJava(DataConstraint dc)
	'''
		«FOR step : dc.steps»
			«dataSequenceStepToJava(step)»
		«ENDFOR»
	'''
	
	def dispatch dataSequenceStepToJava(DataConstraintEvent s)
	'''
		.addElement(
			new CEventSelector().
			    setEvent(«generateEvent(s.event.event)»).
			    setTimeVariable("t")«IF s.negated !== null».setNegated()«ENDIF».setEnvironment(env)
		)
	'''
	
	def dispatch dataSequenceStepToJava(DataConstraintUntilOperator s)
	'''
		.addElement(
			new CUntil().
			setBody(
				(CStep)new CEventSelector().setEvent(«generateEvent(s.body.event.event)»).
				setTimeVariable("t")«IF s.body.negated !== null».setNegated()«ENDIF».setEnvironment(env)).
			setStop(
				(CStep)new CEventSelector().setEvent(«generateEvent(s.stop.event.event)»).
				setTimeVariable("t")«IF s.stop.negated !== null».setNegated()«ENDIF».setEnvironment(env)
				).setEnvironment(env))
	'''
	
	def interval(TimeInterval i) '''new CInterval()«IF i.begin !== null».setBegin(«generateExpression(i.begin)»)«ENDIF»«IF i.end !== null».setEnd(«generateExpression(i.end)»)«ENDIF»'''
	
	def List<Expression> collectConditions(GenericConstraintsBlock block) {
		var result = new ArrayList<Expression>();
		var TreeIterator<EObject> iter;
		var EObject o;

		iter = block.eAllContents();
		while (iter.hasNext()) {
			o = iter.next();
			if (o instanceof ConstraintSequence) {
				result.add(o.cond);
			}
			if (o instanceof Sequence) {
				if (o.condition !== null) {
					result.add(o.condition);
				}
			}
			if (o instanceof EventSelector) {
				if (o.condition !== null) {
					result.add(o.condition);
				}
			}

		}
		return result
	}
	
	def containsPrecondition(EObject constraint){
	    EcoreUtil2.getAllContentsOfType(constraint, EventInState).exists[preCondition !== null]
	}
	
	def containsPrecondition(AbstractBehavior behavior){
	    if(behavior.timeConstraintsBlock === null) return false
	    behavior.timeConstraintsBlock.timeConstraints.exists[it instanceof SingleTimeConstraint && it.containsPrecondition]
	}
	
	//This method is duplication with the same one in InterfaceDecisionGenerator. Move it to the superclass
    def generateConstraintClasses(){
        //time rules classes
        if(behavior.timeConstraintsBlock !== null){
            for(tr : behavior.timeConstraintsBlock.timeConstraints){
                if(tr instanceof GroupTimeConstraint){
                    fsa.generateFile(generatedFileName(rulesContainer.fullyQualifiedName.toString("_") + tr.name + "TimeRuleEnvironment"), groupTimeConstraintEnvironmentClassContent(tr.first, tr))
                    fsa.generateFile(generatedFileName(ruleClassName(rulesContainer, tr).toString), groupTimeConstraintClassContent(tr.first, tr))
                }
            }
        }
        //data rules classes
        if(behavior.dataConstraintsBlock !== null){
            quantifiersInMachines = getQuantifiersInContainer(behavior.dataConstraintsBlock);
            //data rules environment class
            fsa.generateFile(generatedFileName(rulesContainer.fullyQualifiedName.toString("_") + "DataRulesEnvironment"), dataRulesEnvironmentClassContent)
            //data rule classes
            for(dr : behavior.dataConstraintsBlock.dataConstraints){
                fsa.generateFile(generatedFileName(ruleClassName(rulesContainer, dr).toString), dataRuleClassContent(dr))
            }
        }
        //generic rules classes
        if(behavior.genericConstraintsBlock !== null){
            genericRulesConditions = collectConditions(behavior.genericConstraintsBlock)
            quantifiersInMachines = getQuantifiersInContainer(behavior.genericConstraintsBlock)
            fsa.generateFile(generatedFileName(rulesContainer.fullyQualifiedName.toString("_") + "GenericRulesEnvironment"), genericRulesEnvironmentClassContent)
            //generic rule classes
            for(gc : behavior.genericConstraintsBlock.genericConstraints){
                fsa.generateFile(generatedFileName(ruleClassName(rulesContainer, gc).toString), genericRuleClassContent(gc))
            }
        }
        //finally the rules class provider
        fsa.generateFile(generatedFileName(rulesClassName(rulesContainer).toString), constraintsClassProviderContent(behavior))
    }
    
    static def utilityClassName(Signature s)
    '''«s.fullyQualifiedName.toString("_")»State'''
    
    static def utilityClassName(Interface s)
    '''«s.fullyQualifiedName.toString("_")»State'''
    
    
}
