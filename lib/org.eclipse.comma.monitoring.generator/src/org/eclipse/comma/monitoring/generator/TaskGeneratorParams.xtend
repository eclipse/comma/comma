/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.ArrayList
import java.util.List
import java.util.Map
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.monitoring.lib.CComponentTypeDescriptor

class TaskGeneratorParams {
	public String taskName
	public String taskKind
	public List<String> traceFileNames = new ArrayList<String>
	public List<String> traceFilePaths = new ArrayList<String>
	public Interface interface_
	public Component component
	public List<List<String>> componentInstances = new ArrayList<List<String>>
	public List<Map<String, Component>> allInstances = new ArrayList<Map<String, Component>>	
	public boolean skipTimeConstraints = false
	public boolean skipDataConstraints = false
	public boolean applyReordering = false
	public List<CComponentTypeDescriptor> componentDescriptors = new ArrayList<CComponentTypeDescriptor>
	public int monitorPort
	public int feedbackPort
}
