/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.List
import org.eclipse.comma.actions.actions.EVENT_KIND
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.component.component.AnyEvent
import org.eclipse.comma.behavior.component.component.CommandEvent
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.NotificationEvent
import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint
import org.eclipse.comma.behavior.component.component.SignalEvent
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.types.types.EnumTypeDecl
import org.eclipse.comma.types.types.MapTypeConstructor
import org.eclipse.comma.types.types.MapTypeDecl
import org.eclipse.comma.types.types.RecordTypeDecl
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.TypeReference
import org.eclipse.comma.types.types.VectorTypeConstructor
import org.eclipse.comma.types.types.VectorTypeDecl
import org.eclipse.comma.types.utilities.TypeUtilities
import org.eclipse.xtext.generator.IFileSystemAccess

import static extension org.eclipse.comma.types.utilities.CommaUtilities.*

//Generates records, factory, trace reader and scenario player
class InfrastructureGenerator extends TypesJavaGenerator {
	final protected IFileSystemAccess fsa
	final protected Iterable<RecordTypeDecl> recordTypes
	protected final static String factoryClassName = "CFactoryGenerated"	

	new(IFileSystemAccess fsa, Iterable<RecordTypeDecl> recordTypes){
		this.fsa = fsa
		this.recordTypes = recordTypes
	}

	def generateFactory(Iterable<Interface> interfaces, Iterable<Component> components){
		fsa.generateFile(generatedFileName(factoryClassName), factoryContent(interfaces, components))
	}
	
	def factoryContent(Iterable<Interface> interfaces, Iterable<Component> components)
	'''
		«generatedPackage»
		
		import java.util.ArrayList;
		import java.util.List;
		import java.util.function.Function;
		import java.util.Optional;
		import «rootPackage».*;
		import «rootPackage».messages.*;
		
		
		public class «factoryClassName» extends CFactory {
		
			public CRecordsTracker getRecordsTracker() {
				return new CRecordsInstances();
			}
			
			@Override
			public CInterfaceDecisionClass createDecisionClass(String interfaceName){
				switch(interfaceName){
					«FOR i : interfaces»
						case "«i.fullyQualifiedNameAsString»" : return new «InterfaceDecisionGenerator.decisionClassName(i)»();
					«ENDFOR»
					default: return null;
				}
			}
			
			@Override
			public List<CComponentConstraintMonitor> createComponentConstraintMonitors(CComponentMonitoringContext context) {
				List<CComponentConstraintMonitor> result = new ArrayList<CComponentConstraintMonitor>();
				CFunctionalConstraint c;
				Function<CObservedMessage, Boolean> filter;
				CComponentConstraintMonitor mon;
				
				switch(context.getComponentType()){
					«FOR c : components»
						case "«c.fullyQualifiedNameAsString»" : {
							«IF c.functionalConstraintsBlock !== null»
								«FOR fc : c.functionalConstraintsBlock.functionalConstraints»
									c = new «FunctionalConstraintGenerator.constraintClassName(c, fc)»("«fc.name»", context.getComponentInstanceName());
									filter =
										«generateEventFilter(fc)»;
									mon = new CFunctionalConstraintMonitor(context, c, filter);
									result.add(mon);
								«ENDFOR»
							«ENDIF»
							mon = new CComponentTimeDataMonitor(context.createComponentTimeDataContext(), new «ConstraintsRulesGenerator.rulesClassName(c)»(context.getComponentInstanceName()).getRules());
							result.add(mon);
							return result;
						}
					«ENDFOR»
					default: return null;
				}
			}
		}
	'''
	
	def dispatch generateEventFilter(StateBasedFunctionalConstraint fc)
	'''
		m -> «FOR e : fc.usedEvents»(«filterFragment(e)»)«IF e !== fc.usedEvents.last» ||«ENDIF»
			 «ENDFOR»
	'''
	
	def dispatch generateEventFilter(PredicateFunctionalConstraint fc)
	'''
		m -> true
	'''
	
	def dispatch filterFragment(AnyEvent e){
		var CharSequence eventTypeFragment = ''''''
		switch (e.kind) {
			case EVENT_KIND::CALL: eventTypeFragment = ''' && (m instanceof CObservedCommand)'''
			case EVENT_KIND::SIGNAL: eventTypeFragment = ''' && (m instanceof CObservedSignal)'''
			case EVENT_KIND::NOTIFICATION: eventTypeFragment = ''' && (m instanceof CObservedNotification)'''
			default: eventTypeFragment = ''''''
		}
		'''((m.getSourcePort().equals("«e.port.name»") && (context.getComponentInstanceName()«IF e.part !== null» + ".«e.part.name»"«ENDIF»).equals(m.getSource())) || (m.getDestinationPort().equals("«e.port.name»") && (context.getComponentInstanceName()«IF e.part !== null» + ".«e.part.name»"«ENDIF»).equals(m.getDestination())))«eventTypeFragment»'''
	}
	
	def dispatch filterFragment(CommandReply e)
	'''m.«IF e.port instanceof ProvidedPort»getSourcePort()«ELSE»getDestinationPort()«ENDIF».equals("«e.port.name»") && m.«IF e.port instanceof ProvidedPort»getSource()«ELSE»getDestination()«ENDIF».equals(context.getComponentInstanceName()«IF e.part !== null» + ".«e.part.name»"«ENDIF») &&«IF e.command !== null» m.getName().equals("«e.command.event.name»") &&«ENDIF» (m instanceof CObservedReply)'''
		
	def dispatch filterFragment(CommandEvent e)
	'''m.«IF e.port instanceof ProvidedPort»getDestinationPort()«ELSE»getSourcePort()«ENDIF».equals("«e.port.name»") && m.«IF e.port instanceof ProvidedPort»getDestination()«ELSE»getSource()«ENDIF».equals(context.getComponentInstanceName()«IF e.part !== null» + ".«e.part.name»"«ENDIF») && m.getName().equals("«e.event.name»")'''
	
	def dispatch filterFragment(SignalEvent e)
	'''m.«IF e.port instanceof ProvidedPort»getDestinationPort()«ELSE»getSourcePort()«ENDIF».equals("«e.port.name»") && m.«IF e.port instanceof ProvidedPort»getDestination()«ELSE»getSource()«ENDIF».equals(context.getComponentInstanceName()«IF e.part !== null» + ".«e.part.name»"«ENDIF») && m.getName().equals("«e.event.name»")'''
	
	def dispatch filterFragment(NotificationEvent e)
	'''m.«IF e.port instanceof ProvidedPort»getSourcePort()«ELSE»getDestinationPort()«ENDIF».equals("«e.port.name»") && m.«IF e.port instanceof ProvidedPort»getSource()«ELSE»getDestination()«ENDIF».equals(context.getComponentInstanceName()«IF e.part !== null» + ".«e.part.name»"«ENDIF») && m.getName().equals("«e.event.name»")'''

	def generateRecordsTracker() {
			fsa.generateFile(generatedFileName("CRecordsInstances"), createRecordsTracker)
	}


	def generateScenarioPlayer(List<String> taskNames){
		fsa.generateFile(generatedFileName("ScenarioPlayer"), scenarioPlayerContent(taskNames))
	}

	def createRecordsTracker() {
	 '''
		«generatedPackage»
		/*
		 * Copyright (c) 2021 Contributors to the Eclipse Foundation
		 *
		 * This program and the accompanying materials are made
		 * available under the terms of the Eclipse Public License 2.0
		 * which is available at https://www.eclipse.org/legal/epl-2.0/
		 *
		 * SPDX-License-Identifier: EPL-2.0
		 */				
		import java.util.LinkedHashMap;
		import java.util.HashMap;
		import java.util.Map;		
		import «rootPackage».values.*;
		import «rootPackage».CRecordsTracker;

		public class CRecordsInstances implements CRecordsTracker {
			private Map<String,Map<String,String>> records;
			«IF(!recordTypes.empty)»
			public CRecordsInstances () {
			    records = new HashMap<String,Map<String,String>>();
 				«FOR rt : recordTypes»
 				Map<String,String> «rt.fullyQualifiedNameAsString.removeDot»_fields = new  LinkedHashMap<String,String>();
 				«FOR field : TypeUtilities::getAllFields(rt)»
 				«rt.fullyQualifiedNameAsString.removeDot»_fields.put("«field.name»","«toType(field.type)»");
 				«ENDFOR»
			records.put("«rt.fullyQualifiedNameAsString.removeDot»",«rt.fullyQualifiedNameAsString.removeDot»_fields);
			
 				«ENDFOR»
			}

			@Override
			public Map<String,String> getFieldsOfRecord(String recordName) {
				if(!records.containsKey(recordName)) {
					throw new IllegalArgumentException("The record "+recordName+ " is not used by the interface model.");
				}
				return records.get(recordName);
			}
			«ELSE»
			public CRecordsInstances () {
				//no records defined in the interface model
			}

			@Override
			public Map<String,String> getFieldsOfRecord(String recordName) {
				throw new IllegalArgumentException("unexpected record value "+recordName+". Model does not use record types.");
			}
			«ENDIF»
		}
	'''
	}
	
	def removeDot(String string) {
		return string.replace(".","_")
	}
	
	def scenarioPlayerContent(List<String> taskNames)
	'''
		«generatedPackage»
		
		import java.util.ArrayList;
		import java.util.List;
		
		import «rootPackage».CTask;
		import «rootPackage».CTaskResults;
		import «rootPackage».ExportToJson;
		import «rootPackage».ExportToDashboard;
		
		public class ScenarioPlayer {
			private List<CTask> tasks = new ArrayList<CTask>();
			
			public void init() {
				«FOR task : taskNames»
					CTask «task» = new CTask_«task»();
					tasks.add(«task»);
				«ENDFOR»
			}
			
			public void run() {
				for(CTask task : tasks) {
					task.run();
				}
				List<CTaskResults> results = new ArrayList<CTaskResults>();
				for(CTask task : tasks) {
					results.add(task.getResults());
				}
				System.out.println("All monitoring tasks executed");
				ExportToJson exportJson = new ExportToJson();
				exportJson.export(results);
				ExportToDashboard exportDashboard = new ExportToDashboard();
				exportDashboard.export(results);
			}
			
			public static void main(String[] args) {
				ScenarioPlayer player = new ScenarioPlayer();
				player.init();
				player.run();
			}
		}
	'''
	
	def static dispatch CharSequence toType(TypeReference t)
    '''«toType(t.type)»'''
    
    def static dispatch CharSequence toType(SimpleTypeDecl t)
    '''«IF t.base !== null»«t.base.name»«ELSE»«t.name»«ENDIF»'''
    
    def static dispatch CharSequence toType(EnumTypeDecl t)
    '''enum'''
    
    def static dispatch CharSequence toType(RecordTypeDecl t) 
    '''record'''
    
    def static dispatch CharSequence toType(VectorTypeConstructor t) 
    '''vector'''
    
    def static dispatch CharSequence toType(VectorTypeDecl t) 
    '''vector'''
    
    def static dispatch CharSequence toType(MapTypeConstructor t) 
    '''map'''
    
    def static dispatch CharSequence toType(MapTypeDecl t) 
    '''map'''
}
