/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.VariableDeclBlock
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.behavior.EventInState
import org.eclipse.comma.behavior.behavior.StateMachine
import org.eclipse.comma.behavior.behavior.Transition
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.expressions.expression.ExpressionQuantifier
import org.eclipse.comma.expressions.expression.MapRWContext
import org.eclipse.comma.expressions.expression.QUANTIFIER
import org.eclipse.comma.expressions.utilities.ExpressionsUtilities
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2

abstract class AbstractBehaviorGenerator extends ActionsJavaGenerator {
	protected List<ExpressionQuantifier> quantifiersInMachines
	
	//Some variables are defined and kept in State or Env class
	//We call them global
	//Examples: global variables in interface specs
	//          variables in constraint blocks
	//          variables that are iterators in quantifiers
	//
	//Other variables are defined locally
	//Examples: variables in triggers of transitions
	//          
	//Global variables used in expressions in the following context, are just referred by NAME
	//      - init section of interface spec
	//      - init section in functional constraints blocks
	//      - conditions used in data and generic constraints
	//      - conditions in quantifiers
	//
	//Global variables used in expressions in the following contexts are accessed via the State class
	//      - Transitions in interface and component state machines
	//      - Transitions in component functional constraints
	//
	//Local variables are always referred to by names
	//
	//Quantifiers are mapped to methods in a State or Env class
	//
	//Quantifiers are called by a simple name in the context of the defining class if they are used in
	//      - init section of interface and component specs
	//      - init section of functional constraint block
	//      - conditions data and generic blocks
	//      - conditions in quantifiers
	//
	//Quantifiers are called via the State class if used in:
	//      - Transitions in any state based behavior
	
	
	override CommaScope getCommaScope(EObject o) {
		var EObject container = o.eContainer();
	
		while (! isVariableScope(container)) 
			container = container.eContainer();
		if(container instanceof VariableDeclBlock) return CommaScope::GLOBAL
		if(container instanceof ExpressionQuantifier || container instanceof MapRWContext) return CommaScope::QUANTIFIER	
		return CommaScope::TRANSITION
	}
	
	def boolean isVariableScope(EObject o){
		(o instanceof VariableDeclBlock) ||
		(o instanceof Transition) ||
		(o instanceof ExpressionQuantifier) ||
		(o instanceof MapRWContext) || 
		(o instanceof EventInState) ||
		(o instanceof ActionWithVars)//TODO better naming of scopes
	}
	
	def List<ExpressionQuantifier> getQuantifiersInContainer(EObject cont){
		EcoreUtil2::getAllContentsOfType(cont, ExpressionQuantifier)
	}
	
	def List<ExpressionQuantifier> getQuantifiersInStateMachines(AbstractBehavior behavior, List<StateMachine> machines){
		var List<ExpressionQuantifier> result = new ArrayList<ExpressionQuantifier>();
		
		for(m : machines){
			result.addAll(getQuantifiersInContainer(m))
		}
		
		for(ia : behavior.initActions){
			result.addAll(getQuantifiersInContainer(ia))
		}
		
		return result
	}
	
	
	def filterQuantifierVariables(ExpressionQuantifier quantifier){
		return 
		ExpressionsUtilities::getReferredVariablesInQuantifier(quantifier).filter(p | !(p.eContainer instanceof VariableDeclBlock))
	}
	
	def CharSequence generateQuantifierMethod(ExpressionQuantifier quantifier, int index)
	{
		var parameters = filterQuantifierVariables(quantifier)
		val returnType = quantifierType(quantifier)
				
		'''
			public «returnType» evalQuantifier«index»(«FOR p : parameters SEPARATOR ', '»«toJavaType(p.type)» «IF p.getCommaScope == CommaScope::TRANSITION»«TVAR_NAME_PREFIX»«ELSE»«QVAR_NAME_PREFIX»«ENDIF»«p.name»«ENDFOR») {
				«IF quantifier.quantifier == QUANTIFIER::EXISTS»
					«generateQuantifierBodyForExists(quantifier)»
				«ELSEIF quantifier.quantifier == QUANTIFIER::DELETE»
					«generateQuantifierBodyForDelete(quantifier)»
				«ELSE»
					«generateQuantifierBodyForForAll(quantifier)»
				«ENDIF»
			}	
		'''
	}
	
	def quantifierType(ExpressionQuantifier exp){
		if(exp.quantifier == QUANTIFIER::DELETE){
			'''CVector<«toJavaReferenceType(exp.iterator.type)»>'''
		}
		else{
			'''boolean'''
		}
	}
	
	def CharSequence generateQuantifierBodyForExists(ExpressionQuantifier quantifier)
		'''
		CVector<«toJavaReferenceType(quantifier.iterator.type)»> commaCollection = «generateExpression(quantifier.collection)»;
		for(«toJavaType(quantifier.iterator.type)» «QVAR_NAME_PREFIX»«quantifier.iterator.name» : commaCollection.getElements()){
			if(«generateExpression(quantifier.condition)») return true;
		}
		return false;
	'''
		
	def CharSequence generateQuantifierBodyForForAll(ExpressionQuantifier quantifier)
		'''
		CVector<«toJavaReferenceType(quantifier.iterator.type)»> commaCollection = «generateExpression(quantifier.collection)»;
		for(«toJavaType(quantifier.iterator.type)» «QVAR_NAME_PREFIX»«quantifier.iterator.name» : commaCollection.getElements()){
			if(! «generateExpression(quantifier.condition)») return false;
		}
		return true;
	'''
		
	def CharSequence generateQuantifierBodyForDelete(ExpressionQuantifier quantifier)
		'''
		CVector<«toJavaReferenceType(quantifier.iterator.type)»> result = new CVector<«toJavaReferenceType(quantifier.iterator.type)»>();
		CVector<«toJavaReferenceType(quantifier.iterator.type)»> commaCollection = «generateExpression(quantifier.collection)»;
		for(«toJavaType(quantifier.iterator.type)» «QVAR_NAME_PREFIX»«quantifier.iterator.name» : commaCollection.getElements()){
			if(!«generateExpression(quantifier.condition)»){
				result.getElements().add(«QVAR_NAME_PREFIX»«quantifier.iterator.name»);
			}
		}
		return result;
	'''
	
	def Signature getInterface(TriggeredTransition t) {
		if(t !== null) t.trigger.eContainer as Signature else null
	}
	
	def dispatch CharSequence generateExpression(ExpressionQuantifier expr){
		var parameters = filterQuantifierVariables(expr)
		'''«IF getCommaScope(expr) != CommaScope::TRANSITION»this.«ELSE»stateOfDecisionClass.«ENDIF»evalQuantifier«quantifiersInMachines.indexOf(expr)»(«FOR p : parameters SEPARATOR ', '»«IF getCommaScope(p) == CommaScope::QUANTIFIER»«QVAR_NAME_PREFIX»«ELSE»«TVAR_NAME_PREFIX»«ENDIF»«p.name»«ENDFOR»)'''
    }
	
}
