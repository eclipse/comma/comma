/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import org.eclipse.comma.expressions.expression.Expression
import org.eclipse.comma.expressions.expression.ExpressionAddition
import org.eclipse.comma.expressions.expression.ExpressionAnd
import org.eclipse.comma.expressions.expression.ExpressionAny
import org.eclipse.comma.expressions.expression.ExpressionBracket
import org.eclipse.comma.expressions.expression.ExpressionBulkData
import org.eclipse.comma.expressions.expression.ExpressionConstantBool
import org.eclipse.comma.expressions.expression.ExpressionConstantInt
import org.eclipse.comma.expressions.expression.ExpressionConstantReal
import org.eclipse.comma.expressions.expression.ExpressionConstantString
import org.eclipse.comma.expressions.expression.ExpressionDivision
import org.eclipse.comma.expressions.expression.ExpressionEnumLiteral
import org.eclipse.comma.expressions.expression.ExpressionEqual
import org.eclipse.comma.expressions.expression.ExpressionFunctionCall
import org.eclipse.comma.expressions.expression.ExpressionGeq
import org.eclipse.comma.expressions.expression.ExpressionGreater
import org.eclipse.comma.expressions.expression.ExpressionLeq
import org.eclipse.comma.expressions.expression.ExpressionLess
import org.eclipse.comma.expressions.expression.ExpressionMap
import org.eclipse.comma.expressions.expression.ExpressionMapRW
import org.eclipse.comma.expressions.expression.ExpressionMaximum
import org.eclipse.comma.expressions.expression.ExpressionMinimum
import org.eclipse.comma.expressions.expression.ExpressionMinus
import org.eclipse.comma.expressions.expression.ExpressionModulo
import org.eclipse.comma.expressions.expression.ExpressionMultiply
import org.eclipse.comma.expressions.expression.ExpressionNEqual
import org.eclipse.comma.expressions.expression.ExpressionNot
import org.eclipse.comma.expressions.expression.ExpressionOr
import org.eclipse.comma.expressions.expression.ExpressionPlus
import org.eclipse.comma.expressions.expression.ExpressionPower
import org.eclipse.comma.expressions.expression.ExpressionRecord
import org.eclipse.comma.expressions.expression.ExpressionRecordAccess
import org.eclipse.comma.expressions.expression.ExpressionSubtraction
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.ExpressionVector
import org.eclipse.comma.expressions.validation.ExpressionValidator
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.utilities.TypeUtilities
import org.eclipse.emf.ecore.EObject
import org.eclipse.comma.signature.interfaceSignature.Signature

import static extension org.eclipse.comma.types.utilities.CommaUtilities.*

abstract class ExpressionsJavaGenerator extends TypesJavaGenerator {
	protected static final String VAR_NAME_PREFIX = "commaVar_" //prefix for global variables
	protected static final String TVAR_NAME_PREFIX = "commaTVar_" //prefix for transition parameters
	protected static final String QVAR_NAME_PREFIX = "commaQVar_" //prefix for quantifier iterator variables
	
	def dispatch CharSequence generateExpression(ExpressionAddition expr) 
 	'''(«generateExpression(expr.left)» + «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionAnd expr) 
    '''(«generateExpression(expr.left)» && «generateExpression(expr.right)»)'''
    
    def dispatch CharSequence generateExpression(ExpressionBracket expr) 
    '''(«generateExpression(expr.sub)») '''

    def dispatch CharSequence generateExpression(ExpressionConstantBool expr) 
    '''«expr.value»'''
    
    def dispatch CharSequence generateExpression(ExpressionConstantReal expr) 
    '''«expr.value»'''

    def dispatch CharSequence generateExpression(ExpressionConstantInt expr) 
    '''«expr.value»L'''
    
    def dispatch CharSequence generateExpression(ExpressionConstantString expr) 
    '''"«expr.value.replace('\\', '\\\\')»"'''
    
    def dispatch CharSequence generateExpression(ExpressionAny expr) 
    '''(new CAny())'''

	def dispatch CharSequence generateExpression(ExpressionDivision expr) 
    '''(«generateExpression(expr.left)» / «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionEqual expr){
    	var validator = new ExpressionValidator()
		var type = validator.typeOf(expr.left)
		if(type instanceof SimpleTypeDecl){
			if(!(type.name.equals("string") || type.name.equals("id")) && (type.base !== null ? !type.base.name.equals("string") : true) )
				return '''(«generateExpression(expr.left)» == «generateExpression(expr.right)»)'''
		}
		'''«generateExpression(expr.left)».equals(«generateExpression(expr.right)»)'''
    }
    

    def dispatch CharSequence generateExpression(ExpressionGeq expr) 
    '''(«generateExpression(expr.left)» >= «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionGreater expr) 
    '''(«generateExpression(expr.left)» > «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionLeq expr) 
    '''(«generateExpression(expr.left)» <= «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionLess expr) 
    '''(«generateExpression(expr.left)» < «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionMaximum expr) 
    '''Math.max(«generateExpression(expr.left)», «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionMinimum expr) 
    '''Math.min(«generateExpression(expr.left)», «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionMinus expr) 
    '''(-«generateExpression(expr.sub)»)'''

    def dispatch CharSequence generateExpression(ExpressionModulo expr) 
    '''(«generateExpression(expr.left)» % «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionMultiply expr) 
    '''(«generateExpression(expr.left)» * «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionNEqual expr) {
    	var validator = new ExpressionValidator()
		var type = validator.typeOf(expr.left)
		if(type instanceof SimpleTypeDecl){
			if(!(type.name.equals("string") || type.name.equals("id")) && (type.base !== null ? !type.base.name.equals("string") : true) )
				return '''(«generateExpression(expr.left)» != «generateExpression(expr.right)»)'''
		}
		'''!«generateExpression(expr.left)».equals(«generateExpression(expr.right)»)'''
    }

    def dispatch CharSequence generateExpression(ExpressionNot expr) 
    '''!(«generateExpression(expr.sub)»)'''

    def dispatch CharSequence generateExpression(ExpressionOr expr) 
    '''(«generateExpression(expr.left)» || «generateExpression(expr.right)»)'''
    
    def dispatch CharSequence generateExpression(ExpressionPlus expr) 
    '''«generateExpression(expr.sub)»'''

    def dispatch CharSequence generateExpression(ExpressionPower expr) 
    '''Math.pow(«generateExpression(expr.left)», «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionSubtraction expr) 
    '''(«generateExpression(expr.left)» - «generateExpression(expr.right)»)'''
    
    def dispatch CharSequence generateExpression(ExpressionVariable expr){
    	generateVariableReference(expr)
    }
    
    /*
     * All variables appear in the generated Java code with their name preceded by a prefix
     * derived from the variable scope
     */
    def CharSequence generateVariableReference(ExpressionVariable expr){
    	switch(expr.variable.commaScope){
    		case GLOBAL: if(expr.commaScope == CommaScope::GLOBAL || expr.commaScope == CommaScope::QUANTIFIER) 
    						'''«VAR_NAME_PREFIX»«expr.variable.name»'''
    					 else
    						'''stateOfDecisionClass.get_«VAR_NAME_PREFIX»«expr.variable.name»()'''
    		case TRANSITION : '''«TVAR_NAME_PREFIX»«expr.variable.name»'''
    		case QUANTIFIER : '''«QVAR_NAME_PREFIX»«expr.variable.name»'''
    	}
    }
    
    def dispatch CharSequence generateExpression(ExpressionEnumLiteral expr) {
      var typePrefix = expr.type.fullyQualifiedName.toString(".")
      if(expr.type.eContainer instanceof Signature && expr.type.fullyQualifiedName.segments.length == 2) {
         typePrefix = expr.type.name
      }
      '''(new CEnumValue("«typePrefix»::«expr.literal.name»"))'''
    }

    def dispatch CharSequence generateExpression(ExpressionBulkData expr)
    '''(new CBulkdata(«expr.size»))'''
    
    def dispatch CharSequence generateExpression(ExpressionRecord expr)
    '''(new CRecord("«expr.type.fullyQualifiedName.toString("_")»"))«FOR f : expr.fields.reverseView».setValueOfField("«f.recordField.name»", «generateWithCopy(f.exp)»)«ENDFOR»'''
    
    def dispatch CharSequence generateExpression(ExpressionRecordAccess expr)
    '''((«toJavaType(expr.field.type)»)«generateExpression(expr.record)».getValueOfField("«expr.field.name»"))'''
    
    def dispatch CharSequence generateExpression(ExpressionVector expr)
    '''new «toJavaReferenceType(expr.typeAnnotation.type)»()«FOR e : expr.elements».add(«generateWithCopy(e)»)«ENDFOR»'''
    
    def dispatch CharSequence generateExpression(ExpressionFunctionCall expr)
    '''
		«IF expr.functionName.equals('isEmpty')»«generateExpression(expr.args.get(0))».isEmpty()«ENDIF»
		«IF expr.functionName.equals('size')»«generateExpression(expr.args.get(0))».size()«ENDIF»
		«IF expr.functionName.equals('contains')»«generateExpression(expr.args.get(0))».contains(«generateExpression(expr.args.get(1))»)«ENDIF»
		«IF expr.functionName.equals('add')»«generateExpression(expr.args.get(0))».add(«generateWithCopy(expr.args.get(1))»)«ENDIF»
		«IF expr.functionName.equals('asReal')»((double)«generateExpression(expr.args.get(0))»)«ENDIF»
		«IF expr.functionName.equals('abs')»Math.abs(«generateExpression(expr.args.get(0))»)«ENDIF»
		«IF expr.functionName.equals('length')»«generateExpression(expr.args.get(0))».getSize()«ENDIF»
		«IF expr.functionName.equals('hasKey')»«generateExpression(expr.args.get(0))».hasKey(«generateExpression(expr.args.get(1))»)«ENDIF»
		«IF expr.functionName.equals('deleteKey')»«generateExpression(expr.args.get(0))».deleteKey(«generateExpression(expr.args.get(1))»)«ENDIF»
		«IF expr.functionName.equals('matches')»(Pattern.compile(«generateExpression(expr.args.get(1))»).matcher(«generateExpression(expr.args.get(0))»).matches())«ENDIF»
	'''
    
    def dispatch CharSequence generateExpression(ExpressionMap expr)
	'''new «toJavaReferenceType(expr.typeAnnotation.type)»()«FOR p : expr.pairs».put(«generateExpression(p.key)», «generateWithCopy(p.value)»)«ENDFOR»'''
	
	def dispatch CharSequence generateExpression(ExpressionMapRW expr)
	'''«IF expr.value !== null»«generateMapUpdate(expr)»«ELSE»«generateMapAccess(expr)»«ENDIF»'''
	
	def CharSequence generateMapUpdate(ExpressionMapRW expr) {
	    if(expr.context !== null) {
	        '''«generateExpression(expr.map)».update(«generateExpression(expr.context.collection)», («toJavaReferenceType(expr.context.iterator.type)» «QVAR_NAME_PREFIX»«expr.context.iterator.name») -> «generateExpression(expr.key)», («toJavaReferenceType(expr.context.iterator.type)» «QVAR_NAME_PREFIX»«expr.context.iterator.name») -> «generateExpression(expr.value)»)'''
	    }
	    else {
	        '''«generateExpression(expr.map)».update(«generateExpression(expr.key)», «generateWithCopy(expr.value)»)'''  
	    }
	}
	
	def CharSequence generateMapAccess(ExpressionMapRW expr){
		var validator = new ExpressionValidator()
		var mapType = validator.typeOf(expr.map)
		if(expr.context !== null) {
		    '''«generateExpression(expr.map)».readSafe(«generateExpression(expr.context.collection)», («toJavaReferenceType(expr.context.iterator.type)» «QVAR_NAME_PREFIX»«expr.context.iterator.name») -> «generateExpression(expr.key)», «defaultValue(TypeUtilities::getValueType(mapType))»)'''
		}
		else {
		    '''«generateExpression(expr.map)».readSafe(«generateExpression(expr.key)», «defaultValue(TypeUtilities::getValueType(mapType))»)'''   
		}
	}
	
	def CharSequence generateWithCopy(Expression expr) {
		var validator = new ExpressionValidator()
		val type = validator.typeOf(expr)
		if(TypeUtilities::isStructuredType(type)){
			if( expr instanceof ExpressionVariable || 
				expr instanceof ExpressionRecordAccess || 
				(expr instanceof ExpressionMapRW &&   (expr as ExpressionMapRW).value === null )) {
				return '''(«toJavaReferenceType(type)»)Utils.deepCopy(«generateExpression(expr)»)'''
			}
		}
		'''«generateExpression(expr)»'''
	}
	
    //utility function do determine the scope of an expression
	//concrete languages that use Expressions sub-language are expected to override the definition
	def abstract CommaScope getCommaScope(EObject o) 
	
	//TODO consider if deep copy is needed for quantifiers, especially for delete
}
