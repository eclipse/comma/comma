/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.xtext.generator.IFileSystemAccess

class ComponentTimeDataConstraintsGenerator extends EventPatternMixin {
	final protected Component component
	
	new(Component component, IFileSystemAccess fsa) {
		super(component, fsa)
		this.component = component
	}
}
