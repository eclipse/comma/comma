/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

/*
 * Type that indicates the scope of a ComMA variable
 * GLOBAL: variables defined in state machines and data/generic constraints variable blocks
 * TRANSITION: variables that are parameters of events
 * QUANTIFIER: variables that are iterators in quantifiers
 */
enum CommaScope {
	GLOBAL,
	TRANSITION,
	QUANTIFIER
}
