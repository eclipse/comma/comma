/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.stream.Collectors
import org.eclipse.comma.types.utilities.CommaUtilities
import org.eclipse.xtext.generator.IFileSystemAccess

class TaskClassGenerator extends TypesJavaGenerator {
	IFileSystemAccess fsa
	TaskGeneratorParams params

	new(IFileSystemAccess fsa, TaskGeneratorParams params){
		this.fsa = fsa
		this.params = params
	}
	def generateTask(){
		fsa.generateFile(generatedFileName(taskClassName(params.taskName)), taskContent)
	}

	def taskContent() {
		val sortedFileNames = params.traceFileNames.stream().sorted().collect(Collectors.toList());
	'''
			«generatedPackage»

			import java.util.Arrays;
			import java.util.HashSet;
			«IF !params.traceFilePaths.empty && params.traceFilePaths.get(0).fileExtension.equals("runtime")»
					import java.io.BufferedReader;
					import java.io.IOException;
					import java.io.InputStreamReader;
					import java.net.ServerSocket;
					import java.net.Socket;
					import com.google.gson.JsonStreamParser;
			«ENDIF»
			import «rootPackage».CFactory;
			import «rootPackage».CInterfaceMonitoringContext;
			import «rootPackage».CComponentMonitoringTaskContext;
			import «rootPackage».CComponentTypeDescriptor;
			import «rootPackage».CTask;
			import «rootPackage».CPartDescriptor;
			import «rootPackage».CEntryPoint;
			import «rootPackage».CPortInstance;
			import «rootPackage».CTracePlayer;
			import «rootPackage».utils.CMonitorFeedbackSender;

			public class «taskClassName(params.taskName)» extends CTask{
				CTracePlayer player;
				«IF params.taskKind.equals("interface")»
					CInterfaceMonitoringContext context;
				«ELSE»
					CComponentMonitoringTaskContext context;
					CComponentTypeDescriptor componentDescriptor;
				«ENDIF»

				public «taskClassName(params.taskName)»() {
					super("«params.taskName»", "«params.taskKind»", "«params.taskKind.equals("interface") ? params.interface_.name : params.component.name»");
					CFactory factory = new CFactoryGenerated();

					«FOR i : 0..< sortedFileNames.size»
						«val realIndex = params.traceFileNames.indexOf(sortedFileNames.get(i))»
						«IF params.taskKind.equals("interface")»
							context = new CInterfaceMonitoringContext("«CommaUtilities.getFullyQualifiedName(params.interface_)»", «params.interface_.singleton», factory, "comma-gen/«params.taskName»/«sortedFileNames.get(i)»/");
						«ELSE»
							context = new CComponentMonitoringTaskContext("«CommaUtilities.getFullyQualifiedName(params.component)»", Arrays.asList(«FOR ci : params.componentInstances.get(i) SEPARATOR ', '»"«ci»"«ENDFOR»), factory, "comma-gen/«params.taskName»/«sortedFileNames.get(i)»/");
							«FOR cd : params.componentDescriptors»
							componentDescriptor = new CComponentTypeDescriptor();
							componentDescriptor.componentType = "«cd.componentType»";
							componentDescriptor.singletonPorts = new HashSet<>(Arrays.asList(«FOR p : cd.singletonPorts SEPARATOR ', '»"«p»"«ENDFOR»));
							«IF cd.partDescriptors !== null»
							componentDescriptor.partDescriptors = Arrays.asList(«FOR pd : cd.partDescriptors SEPARATOR ', '»new CPartDescriptor("«pd.partId»", "«pd.type»")«ENDFOR»);
							«ENDIF»
							context.addComponentTypeDescriptor(componentDescriptor);
							«ENDFOR»
							context.setEntryPoints(Arrays.asList(«FOR ep : GeneratorUtility.getEntryPoints(params.allInstances.get(i), params.componentInstances.get(i)) SEPARATOR ', '»new CEntryPoint("«ep.receivingComponentInstance»", "«ep.receivingPort»", "«ep.actualComponentInstance»", "«ep.actualPort»")«ENDFOR»));
							context.setPortPaths(Arrays.asList(
							«FOR path : GeneratorUtility.getPortPaths(params.component) SEPARATOR ','»
							     Arrays.asList(«FOR p : path SEPARATOR ', '»new CPortInstance("«p.componentInstance»", "«p.port»")«ENDFOR»)
						    «ENDFOR»
							));
						«ENDIF»
						context.skipTimeConstraints(«params.skipTimeConstraints»);
						context.skipDataConstraints(«params.skipDataConstraints»);
						context.applyReordering(«params.applyReordering»);
						«IF !params.traceFilePaths.get(realIndex).fileExtension.equals("runtime")»
							player = new CTracePlayer(context, "«params.traceFilePaths.get(realIndex)»",null);
							addTracePlayer(player);
						«ENDIF»
					«ENDFOR»
				}
				
				«IF !params.traceFilePaths.empty && params.traceFilePaths.get(0).fileExtension.equals("runtime")»
				public void run() {
				    ServerSocket commaServer = null;
					//run the server
					try {
					    CMonitorFeedbackSender feedback = new CMonitorFeedbackSender(«params.feedbackPort»);
					    feedback.start();
				        commaServer = new ServerSocket(«params.monitorPort»);
						System.out.println("Comma Server started, Listening for a connection...");
				        
				        // processing the incoming trace
				        // Call accept() to receive the next connection
				        Socket socket = commaServer.accept();
				        System.out.println("A trace is streamed to Comma Server");
				        
				        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				        JsonStreamParser  streamReader = new JsonStreamParser(in);
				        if(streamReader.hasNext()) {
				            player = new CTracePlayer(context, "«params.traceFilePaths.get(0)»", streamReader, feedback);
				            addTracePlayer(player);
				            player.run();
				            socket.close();	
				        }
				        feedback.stop();
				    } catch (IOException e) {
				        e.printStackTrace();
				    }
				    
				    //closing Comma Server, after processing the trace
				    try {
				        System.out.println("Closing Comma Server");
				        commaServer.close();
				    } catch(IOException e){
				        e.printStackTrace();
				    }
				}
				«ENDIF»
			}
		'''
	}
	
	def fileExtension(String fileName){
		if (fileName.contains("."))
     		return fileName.substring(fileName.lastIndexOf(".")+1)
	}

	def taskClassName(String taskName){
		"CTask_" + taskName
	}
}
