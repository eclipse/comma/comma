/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.Map
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.EventPattern
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction
import org.eclipse.comma.actions.actions.Reply
import org.eclipse.comma.behavior.behavior.Clause
import org.eclipse.comma.behavior.behavior.NonTriggeredTransition
import org.eclipse.comma.behavior.behavior.Port
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.behavior.behavior.Transition
import org.eclipse.comma.behavior.component.component.CONNECTION_QUANTIFIER
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.CommandReplyWithVars
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.ConnectionVariable
import org.eclipse.comma.behavior.component.component.EventCall
import org.eclipse.comma.behavior.component.component.EventWithVars
import org.eclipse.comma.behavior.component.component.ExpressionConnectionState
import org.eclipse.comma.behavior.component.component.ExpressionInterfaceState
import org.eclipse.comma.behavior.component.component.FunctionalConstraint
import org.eclipse.comma.behavior.component.component.PortSelector
import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint
import org.eclipse.comma.behavior.component.component.TriggeredTransition
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.expressions.expression.Expression
import org.eclipse.comma.expressions.expression.ExpressionQuantifier
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.Variable
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess

import static org.eclipse.comma.behavior.component.utilities.ComponentUtilities.*

import static extension org.eclipse.comma.types.utilities.CommaUtilities.*
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.behavior.component.utilities.TransitionDecomposer
import org.eclipse.comma.behavior.component.utilities.ClauseFragments
import org.eclipse.comma.behavior.component.utilities.IfHelperAction
import org.eclipse.comma.behavior.component.utilities.FunctionFragmentCall
import org.eclipse.comma.behavior.component.utilities.StateTransitionAction

class FunctionalConstraintGenerator extends EventPatternMixin {
	Iterable<Interface> interfaces
	
	//two variables for the context of the generator
	var inEventReception = false
	var eventReceptionPort = ""
	var eventReceptionConnection = ""
	
	Map<Variable, String> triggerVariables
	Map<Transition, Map<Clause, ClauseFragments>> clauseFragments
	
	new(Iterable<Interface> interfaces, IFileSystemAccess fsa) {
		super(null, fsa)
		this.interfaces = interfaces
	}
	
	def populateClauseFragments(StateBasedFunctionalConstraint  fc) {
	    val result = new HashMap<Transition, Map<Clause, ClauseFragments>>
	    triggerVariables = new HashMap<Variable, String>
	    for(s : fc.states) {
	        var subStateCounter = 1
	        var functionCounter = 1
	        for(t : s.transitions) {
	            val clausesMap = new HashMap<Clause, ClauseFragments>
	            for(cl : t.clauses) {
	                val fragments = TransitionDecomposer.decomposeTransition(fc, t, cl)
	                for(f : fragments.subTransitionFragments) f.subState = s.name + "_" + (subStateCounter++)
	                for(f : fragments.functionFragments) f.methodName = "f" + s.name + "_" + (functionCounter++)
	                fragments.triggerVariables.entrySet.forEach[triggerVariables.put(it.key, "")]
                    clausesMap.put(cl, fragments)
                }
                result.put(t, clausesMap)
            }
        }
        var counter = 1
        for(v : triggerVariables.keySet) {
            triggerVariables.put(v, v.name + "_" + counter++)
        }
	    result
	}
	
	def generateUtilityClass(Component c, FunctionalConstraint fc){
		quantifiersInMachines = new ArrayList<ExpressionQuantifier>
		quantifiersInMachines.addAll(getQuantifiersInContainer(fc))
		if(fc instanceof StateBasedFunctionalConstraint) {
		  clauseFragments = populateClauseFragments(fc)   
		}
		fsa.generateFile(generatedFileName(utilityClassName(c, fc).toString), utilityClassContent(fc, c))
	}
	
	def generateConstraintClass(Component c, FunctionalConstraint fc){
		quantifiersInMachines = new ArrayList<ExpressionQuantifier>
		quantifiersInMachines.addAll(getQuantifiersInContainer(fc))
		fsa.generateFile(generatedFileName(constraintClassName(c, fc).toString), constraintClassContent(fc, c))
	}
	
	def constraintClassImportPart()
	'''
	«generatedPackage»
	
	import java.util.Arrays;
	import java.util.ArrayList;
	import java.util.HashSet;
	import java.util.List;
	import java.util.regex.Pattern;
	import «rootPackage».*;
	import «rootPackage».messages.*;
	import «rootPackage».values.*;
	import «rootPackage».utils.Utils;
	'''

	def dispatch constraintClassContent(PredicateFunctionalConstraint fc, Component c) {
		'''
			«constraintClassImportPart»
			
			public class «constraintClassName(c, fc)» extends CFunctionalConstraint {
				private «utilityClassName(c, fc)» stateOfDecisionClass;
				
				public «constraintClassName(c, fc)»(String name, String componentInstanceName) {
					super(name, componentInstanceName);
					stateOfDecisionClass = new «utilityClassName(c, fc)»();
				}
				
				@Override
				public void setState(CFunctionalConstraintState state) {
					stateOfDecisionClass = («utilityClassName(c, fc)») state;
				}
				
				@Override
				public CFunctionalConstraintState getInitialState() {
					return stateOfDecisionClass;
				}
				
				@Override
				public CFunctionalConstraintResult consume(CObservedMessage message) {
				    CFunctionalConstraintResult result = new CFunctionalConstraintResult();
					«{inEventReception = true eventReceptionPort = '''determineContextPort(message)''' eventReceptionConnection = '''determineContextConnection(message)''' ""}»
					if((«generateExpression(fc.expression)») &&
						«{inEventReception = false ""}»
						  («generateExpression(fc.expression)»)) {
						  	result.states.add(stateOfDecisionClass);
					}
					return result;
				}
			}
		'''
	}
	
	def dispatch constraintClassContent(StateBasedFunctionalConstraint fc, Component c) {
		val states = getStateNames(fc)
	
	'''
			«constraintClassImportPart»
					
			public class «constraintClassName(c, fc)» extends CFunctionalConstraint {
				private «utilityClassName(c, fc)» lastReceivedState;
				private «utilityClassName(c, fc)» stateOfDecisionClass;
							
				public «constraintClassName(c, fc)»(String name, String componentInstanceName) {
					super(name, componentInstanceName);
					stateOfDecisionClass = new «utilityClassName(c, fc)»();
				}
							
				@Override
				public void setState(CFunctionalConstraintState state) {
					lastReceivedState = («utilityClassName(c, fc)») state;
				}
							
				@Override
				public CFunctionalConstraintState getInitialState() {
					return stateOfDecisionClass;
				}
				
				@Override
				public CFunctionalConstraintResult consume(CObservedMessage message) {
				    stateOfDecisionClass = («utilityClassName(c, fc)») Utils.deepCopy(lastReceivedState);
					switch(lastReceivedState.getState()) {
					«FOR s : states»
						case "«s»" : return state«s»(message);
					«ENDFOR»
					default: return null;
					}
				}
				
				«FOR s : fc.states»
					«generateStateMethod(s, utilityClassName(c, fc))»
					
					«generateFragmentMethods(s)»
					
					«generateFunctionMethods(s)»
				«ENDFOR»
			}
		'''
	}
	
	def bindParameter(Variable v, int index)
	'''stateOfDecisionClass.set_«VAR_NAME_PREFIX»«triggerVariables.get(v)»(message.getParameters().get(«index»));'''
	
	def bindParameter(Variable v, String value)
	'''stateOfDecisionClass.set_«VAR_NAME_PREFIX»«triggerVariables.get(v)»(«value»);'''
		
	def determineContextPort(PortSelector ev) {
	    if(ev.part !== null) {
	        ev.part.name + "." + ev.port.name
	    } else {
	        ev.port.name
	    }
	}
	
	def determineContextConnection(PortSelector ev) {
	    '''«IF ev.part !== null»(componentInstanceName + "." + "«ev.part.name»")«ELSE»componentInstanceName«ENDIF».equals(message.getSource()) ? message.getDestination() : message.getSource()'''
	}

    def generateFunctionMethods(State s) 
    '''
    «FOR t : s.transitions»
    «FOR c : t.clauses»
    «FOR fragment : clauseFragments.get(t).get(c).functionFragments»
    private CFunctionalConstraintResult «fragment.methodName»() {
        CFunctionalConstraintResult result = new CFunctionalConstraintResult();
        
        «FOR a : fragment.getActions»
        «n_generateAction(a)»
        «ENDFOR»
        
        return result;
    }
    «ENDFOR»
    «ENDFOR»
    «ENDFOR»
    '''
		
	def generateFragmentMethods(State s) {
	    '''
	    «FOR t : s.transitions»
	    «FOR c : t.clauses»
	    «FOR fragment : clauseFragments.get(t).get(c).subTransitionFragments»
	    private CFunctionalConstraintResult state«fragment.subState»(CObservedMessage message) {
	       CMessagePattern e;
	       CFunctionalConstraintResult result = new CFunctionalConstraintResult();
	       «val first = fragment.actions.get(0)»
	       «headOfFragmentMethod(first as EObject)» 
	       if(e.match(message)) {
	           «IF first instanceof ActionWithVars»
	           «IF first instanceof EventWithVars»
	           «val event = first.event»
	           «var counter = 0»
	           «FOR i : 0..< event.parameters.size»
	           «IF event.parameters.get(i).direction !== DIRECTION::OUT»
	           «bindParameter(first.parameters.get(counter), counter++)»
	           «ENDIF»
	           «ENDFOR»
	           «ELSE»
	           «FOR i : 0..< first.parameters.size»
	           «bindParameter(first.parameters.get(i), i)»
	           «ENDFOR»
	           «ENDIF»
	           «bindActionWithVars(first)»
	           «IF first.condition !== null»
	           «{inEventReception = true eventReceptionPort = '''"«determineContextPort(first as PortSelector)»"''' eventReceptionConnection = '''«determineContextConnection(first as PortSelector)»''' ""}»
	           if(«generateExpression(first.condition)») {
	               «{inEventReception = false ""}»
	               «FOR i : 1..< fragment.actions.size»
	               «n_generateAction(fragment.actions.get(i))»
	               «ENDFOR»
	           }
	           «ELSE»
	           «FOR i : 1..< fragment.actions.size»
	           «n_generateAction(fragment.actions.get(i))»
	           «ENDFOR»
	           «ENDIF»
	           «ELSE»
	           «FOR i : 1..< fragment.actions.size»
	           «n_generateAction(fragment.actions.get(i))»
	           «ENDFOR»
	           «ENDIF»
	       }
	       
	       return result;                  
	    }
	    «ENDFOR»
	    «ENDFOR»
	    «ENDFOR»
	    '''
	}
	
	def bindActionWithVars(ActionWithVars a) {
	    val idVarDef = (a as ConnectionVariable).idVarDef
	    if(idVarDef !== null)
	       '''
	       «bindParameter(idVarDef, "message.getDestination().equals(" + componentIdExp(a as PortSelector) + ") ? message.getSource() : message.getDestination()")»
	       '''
	    else
	       ''''''
	}
	
	def dispatch headOfFragmentMethod(CommandReplyWithVars e) {
	   val event = makeEvent(null, getCommand(e), e, new ArrayList<Expression>)
	   
	   '''
	   e = «generateEvent(event)»;
	   ''' 
	}
	
	def dispatch headOfFragmentMethod(CommandReply e) {
       val event = makeEvent(null, getCommand(e), e, new ArrayList<Expression>)
       
       '''
       e = «generateEvent(event)»;
       «IF !e.parameters.empty»
       e«FOR p : e.parameters».addParameter(«generateExpression(p)»)«ENDFOR»;
       «ENDIF»
       «IF e.idVar !== null»
       if(message.getDestination().equals(«componentIdExp(e)»)) {e.setSource(«generateExpression(e.idVar)»);}
       else {e.setDestination(«generateExpression(e.idVar)»);}
       «ENDIF»
       ''' 
    }
	
	def dispatch headOfFragmentMethod(EventCall e) {
       val event = makeEvent(e.event, null, e, new ArrayList<Expression>)
       
       '''
       e = «generateEvent(event)»;
       «IF !e.parameters.empty»
       e«FOR p : e.parameters».addParameter(«generateExpression(p)»)«ENDFOR»;
       «ENDIF»
       «IF e.idVar !== null»
       if(message.getDestination().equals(«componentIdExp(e)»)) {e.setSource(«generateExpression(e.idVar)»);}
       else {e.setDestination(«generateExpression(e.idVar)»);}
       «ENDIF»
       ''' 
    }
    
    def dispatch headOfFragmentMethod(EventWithVars e) {
       val event = makeEvent(e.event, null, e, new ArrayList<Expression>)
       
       '''
       e = «generateEvent(event)»;
       ''' 
    }
   
	def generateStateMethod(State s, String stateClassName)
	'''
	private CFunctionalConstraintResult state«s.name»(CObservedMessage message){
	    CFunctionalConstraintResult result = new CFunctionalConstraintResult();
	    «IF !s.transitions.filter(TriggeredTransition).empty»
	    CMessagePattern e;
	    «ENDIF»
	    
	    «FOR i : 0..< s.transitions.size»
	    «transitionInStateMethod(s.transitions.get(i), stateClassName, i)»
	    «ENDFOR»
	    return result;
	}
	'''
	
	def dispatch transitionInStateMethod(NonTriggeredTransition t, String stateClassName, int tIndex)
	'''
	«IF tIndex > 0»
	stateOfDecisionClass = («stateClassName») Utils.deepCopy(lastReceivedState);
	«ENDIF»
	«IF t.guard!== null»
	if(«generateExpression(t.guard)») {
	    «FOR c : t.clauses»
	    «IF t.clauses.indexOf(c) > 0»
	    stateOfDecisionClass = («stateClassName») Utils.deepCopy(lastReceivedState);
	    «ENDIF»
	    «FOR a : clauseFragments.get(t).get(c).rootFragment.actions»
	    «n_generateAction(a)»
	    «ENDFOR»
	    
	    «ENDFOR»
	}
	«ELSE»
	«FOR c : t.clauses»
	«IF t.clauses.indexOf(c) > 0»
	stateOfDecisionClass = («stateClassName») Utils.deepCopy(lastReceivedState);
	«ENDIF»
	«FOR a : clauseFragments.get(t).get(c).rootFragment.actions»
	«n_generateAction(a)»
	«ENDFOR»
	
	«ENDFOR»
	«ENDIF»
	'''
	
	def dispatch CharSequence n_generateAction(AssignmentAction a)
	'''
	«generateAction(a)»
	'''
	
	def dispatch CharSequence n_generateAction(IfAction ifact) '''
    if(«generateExpression(ifact.guard)»){
        «FOR a : ifact.thenList.actions»
        «generateAction(a)»
        «ENDFOR»
    «IF ifact.elseList !== null »
        } else {
            «FOR a : ifact.elseList.actions»
            «generateAction(a)»
            «ENDFOR»
    «ENDIF»
    }
    '''
    
    def dispatch CharSequence n_generateAction(IfHelperAction ifact) '''
    if(«generateExpression(ifact.condition)»){
       «FOR a : ifact.thenActions»
       «n_generateAction(a)»
       «ENDFOR»
    «IF ifact.elseActions !== null »
       } else {
           «FOR a : ifact.elseActions»
           «n_generateAction(a)»
           «ENDFOR»
    «ENDIF»
    }
    '''
    
    def dispatch CharSequence n_generateAction(RecordFieldAssignmentAction a) {
        generateAction(a)
    }
	
	def dispatch transitionInStateMethod(TriggeredTransition t, String stateClassName, int tIndex) {
	val event = pattern(t)
	'''
	e = «generateEvent(event)»;
	if(e.match(message)) {
	    «IF tIndex > 0»
	    stateOfDecisionClass = («stateClassName») Utils.deepCopy(lastReceivedState);
	    «ENDIF»
	    «IF t.trigger !== null && t.trigger instanceof Command»
	    «var counter = 0»
	    «val command = t.trigger as Command»
	    «FOR i : 0..< command.parameters.size»
	    «IF command.parameters.get(i).direction !== DIRECTION::OUT»
	    «bindParameter(t.parameters.get(counter), counter++)»
	    «ENDIF»
	    «ENDFOR»
	    «ELSE»
	    «FOR i : 0..< t.parameters.size»
	    «bindParameter(t.parameters.get(i), i)»
	    «ENDFOR»
	    «ENDIF»
	    «IF t.idVarDef !== null»
	    «bindParameter(t.idVarDef, "message.getDestination().equals(" + componentIdExp(t) + ") ? message.getSource() : message.getDestination()")»
	    «ENDIF»
	    «IF t.clauses.size > 1»
	    «stateClassName» tempDecisionClass = («stateClassName») Utils.deepCopy(stateOfDecisionClass);
	    «ENDIF»
	    «IF t.guard !== null»
	    «{inEventReception = true eventReceptionPort = '''"«determineContextPort(t)»"''' eventReceptionConnection = '''«determineContextConnection(t)»''' ""}»
	    if(«generateExpression(t.guard)») {
	        «{inEventReception = false ""}»
	        «FOR c : t.clauses»
	        «IF t.clauses.indexOf(c) > 0»
	        stateOfDecisionClass = («stateClassName») Utils.deepCopy(tempDecisionClass);
	        «ENDIF»
	        «FOR a : clauseFragments.get(t).get(c).rootFragment.actions»
	        «n_generateAction(a)»
	        «ENDFOR»
	        
	        «ENDFOR»
	    }
	    «ELSE»
	    «FOR c : t.clauses»
	    «IF t.clauses.indexOf(c) > 0»
	    stateOfDecisionClass = («stateClassName») Utils.deepCopy(tempDecisionClass);
	    «ENDIF»
	    «FOR a : clauseFragments.get(t).get(c).rootFragment.actions»
	    «n_generateAction(a)»
	    «ENDFOR»
	    
	    «ENDFOR»
	    «ENDIF»
	}
	'''
	}
	
	def dispatch CharSequence n_generateAction(StateTransitionAction a) 
	'''
	«IF a.inNonTriggeredRoot»
	result.states.addAll(state«a.nextState»(message).states);
	«ELSE»
	stateOfDecisionClass.setState("«a.nextState»");
	result.states.add(stateOfDecisionClass);
	«ENDIF»
	'''
	
	def dispatch CharSequence n_generateAction(FunctionFragmentCall a)
	'''
	result.states.addAll(«a.functionFragment.methodName»().states);
	'''

	def EventPattern pattern(TriggeredTransition t) 
	{
	    makeEvent(t.trigger, t.replyTo, t, new ArrayList<Expression>)
	}
	
	def utilityClassImportPart()
	'''
	«generatedPackage»
	
	import java.util.Arrays;
	import java.util.HashMap;
	import java.util.Map;
	import java.util.HashSet;
	import java.util.Set;
	import java.util.regex.Pattern;
	import «rootPackage».CFunctionalConstraintState;
	import «rootPackage».CPathDescription;
	'''
	
	def dispatch utilityClassContent(StateBasedFunctionalConstraint fc, Component c)
	'''
		«utilityClassImportPart»
		import «rootPackage».values.*;
		
		public class «utilityClassName(c, fc)» extends CFunctionalConstraintState {
			/* current state */
			private String state;
		
			/* global variables */
			«FOR v : fc.vars»
			private «toJavaType(v.type)» «VAR_NAME_PREFIX»«v.name» = «defaultValue(v.type)»;
			«ENDFOR»
			
			/* trigger variables */
			«FOR v : triggerVariables.keySet»
			private «toJavaType(v.type)» «VAR_NAME_PREFIX»«triggerVariables.get(v)»;
			«ENDFOR»
			                
			/* getter and setter for current state */
			public String getState() {
				return state;
			}
						
			public void setState(String s) {
				state = s;
			}
					
			/* getters and setters for global variables */
			«FOR v : fc.vars»
				public «toJavaType(v.type)» get_«VAR_NAME_PREFIX»«v.name»() {
					return «VAR_NAME_PREFIX»«v.name»;
				}
				
				public void set_«VAR_NAME_PREFIX»«v.name»(Object i) {
					«VAR_NAME_PREFIX»«v.name» = («toJavaType(v.type)») i;
				}
					
			«ENDFOR»
			/* getters and setters for trigger variables */
			«FOR v : triggerVariables.keySet»
			public «toJavaType(v.type)» get_«VAR_NAME_PREFIX»«triggerVariables.get(v)»() {
			    return «VAR_NAME_PREFIX»«triggerVariables.get(v)»;
			}
			
			public void set_«VAR_NAME_PREFIX»«triggerVariables.get(v)»(Object i) {
			    «VAR_NAME_PREFIX»«triggerVariables.get(v)» = («toJavaType(v.type)») i;
			}
			
			«ENDFOR»
			«IF quantifiersInMachines.size > 0»
				/* methods that implement quantifiers */
				«FOR quantifier : quantifiersInMachines»
					«generateQuantifierMethod(quantifier, quantifiersInMachines.indexOf(quantifier))»
				«ENDFOR»
				
			«ENDIF»
			public «utilityClassName(c, fc)»() {
				/* initialization of current state */
				state = "«getInitialState(fc).name»";
				«IF !fc.initActions.empty»
					
					/* initialization of variables in init section */
					«FOR a : fc.initActions» 
						«generateAction(a)»;
					«ENDFOR»
				«ENDIF»
				
				/* initialization of initial port states */
				initialPortStates = new HashMap<String, Set<String>>();
				«FOR p : c.ports»
					initialPortStates.put("«p.name»", new HashSet<>(Arrays.asList(«FOR s : p.initialStatesForPort SEPARATOR ', '»"«s.name»"«ENDFOR»)));
				«ENDFOR»
				«FOR connection : c.connections»
				«IF connection.firstEnd.part !== null && connection.secondEnd.part !== null»
				    initialPortStates.put("«connection.firstEnd.part.name».«connection.firstEnd.port.name»", new HashSet<>(Arrays.asList(«FOR s : connection.firstEnd.port.initialStatesForPort SEPARATOR ', '»"«s.name»"«ENDFOR»)));
				    initialPortStates.put("«connection.secondEnd.part.name».«connection.secondEnd.port.name»", new HashSet<>(Arrays.asList(«FOR s : connection.secondEnd.port.initialStatesForPort SEPARATOR ', '»"«s.name»"«ENDFOR»)));
				«ENDIF»
				«ENDFOR»
			}
		}
	'''
	
	def dispatch utilityClassContent(PredicateFunctionalConstraint fc, Component c)
	'''
		«utilityClassImportPart»

		public class «utilityClassName(c, fc)» extends CFunctionalConstraintState {
		
			«IF quantifiersInMachines.size > 0»
				/* methods that implement quantifiers */
				«FOR quantifier : quantifiersInMachines»
					«generateQuantifierMethod(quantifier, quantifiersInMachines.indexOf(quantifier))»
				«ENDFOR»
				
			«ENDIF»
			public «utilityClassName(c, fc)»() {
				/* initialization of initial port states */
				initialPortStates = new HashMap<String, Set<String>>();
				«FOR p : c.ports»
					initialPortStates.put("«p.name»", new HashSet<>(Arrays.asList(«FOR s : p.initialStatesForPort SEPARATOR ', '»"«s.name»"«ENDFOR»)));
				«ENDFOR»
				«val visitedPorts = new HashSet<String>»
				«FOR connection : c.connections»
				«IF connection.firstEnd.part !== null && connection.secondEnd.part !== null»
				    «val first = connection.firstEnd.part.name + "." + connection.firstEnd.port.name»
				    «val second = connection.secondEnd.part.name + "." + connection.secondEnd.port.name»
				    «IF !visitedPorts.contains(first)»
				    initialPortStates.put("«first»", new HashSet<>(Arrays.asList(«FOR s : connection.firstEnd.port.initialStatesForPort SEPARATOR ', '»"«s.name»"«ENDFOR»)));
				    «ENDIF»
				    «IF !visitedPorts.contains(second)»
				    initialPortStates.put("«second»", new HashSet<>(Arrays.asList(«FOR s : connection.secondEnd.port.initialStatesForPort SEPARATOR ', '»"«s.name»"«ENDFOR»)));
				    «ENDIF»
				    «{visitedPorts.add(first)
				      visitedPorts.add(second)
				      ""
				    }»
				«ENDIF»
				«ENDFOR»
			}
		}
	'''
	
	static def utilityClassName(Component c, FunctionalConstraint fc){
		c.fullyQualifiedName.toString("_") + fc.name + "State"	
	}
	
	static def constraintClassName(Component c, FunctionalConstraint fc){
	   c.fullyQualifiedName.toString("_") + fc.name + "FunctionalConstraint"	
	}
	
	def initialStatesForPort(Port port){
		interfaces.findFirst[name.equals(port.interface.name)].machines.map[states.filter[initial]].flatten
	}
	
	def getStateNames(StateBasedFunctionalConstraint fc){
		val result = new ArrayList<String>
		for(s : fc.states){
			result.add(s.name)
			for(t : s.transitions) {
			    for(c : t.clauses) {
			        clauseFragments.get(t).get(c).subTransitionFragments.forEach[result.add(subState)]
			    }
			}
		}
		result
	}
	
	override CharSequence generateVariableReference(ExpressionVariable expr){
        val parent = expr.variable.eContainer 
        if(parent instanceof TriggeredTransition || parent instanceof ActionWithVars) {
            if(EcoreUtil2.getContainerOfType(expr, ExpressionQuantifier) !== null) {
                '''«TVAR_NAME_PREFIX»«expr.variable.name»'''
            } else {
                '''stateOfDecisionClass.get_«VAR_NAME_PREFIX»«triggerVariables.get(expr.variable)»()'''
            }
        } else {
            super.generateVariableReference(expr)
        }
    }
	
	def dispatch CharSequence generateExpression(ExpressionInterfaceState expr){		
		'''(portsContext.portInState("«expr.portName»", "«expr.state.name»", «IF inEventReception»«eventReceptionPort», «eventReceptionConnection»«ELSE»null, null«ENDIF»))'''
	}
	
	def portName(PortSelector s)
	'''«IF s.part !== null»«s.part.name».«ENDIF»«s.port.name»'''
	
	def dispatch CharSequence generateExpression(ExpressionConnectionState expr){
		if(expr.idVar !== null){
			'''(portsContext.connectionAtPortInState(stateOfDecisionClass.get_«VAR_NAME_PREFIX»«expr.idVar.variable.name»(), "«expr.portName»", new HashSet<>(Arrays.asList(«FOR s : expr.states SEPARATOR ', '»"«s.name»"«ENDFOR»)), «IF inEventReception»«eventReceptionPort», «eventReceptionConnection»«ELSE»null, null«ENDIF»))'''
		}else if(expr.multiplicity !== null){
			'''(portsContext.someAtPortInState("«expr.portName»", new HashSet<>(Arrays.asList(«FOR s : expr.states SEPARATOR ', '»"«s.name»"«ENDFOR»)), «IF inEventReception»«eventReceptionPort», «eventReceptionConnection»«ELSE»null, null«ENDIF», «expr.multiplicity.lower», «IF expr.multiplicity.upperInf !== null»-1«ELSE»«expr.multiplicity.upper»«ENDIF»))'''
		}else '''(portsContext.«IF expr.quantifier == CONNECTION_QUANTIFIER::ALL»allAtPortInState«ELSE»someAtPortInState«ENDIF»("«expr.portName»", new HashSet<>(Arrays.asList(«FOR s : expr.states SEPARATOR ', '»"«s.name»"«ENDFOR»)), «IF inEventReception»«eventReceptionPort», «eventReceptionConnection»«ELSE»null, null«ENDIF»«IF expr.quantifier == CONNECTION_QUANTIFIER::ONE», 1, 1«ENDIF»«IF expr.quantifier == CONNECTION_QUANTIFIER::SOME», 1, -1«ENDIF»))'''
	}
	
	def componentIdExp(PortSelector e)
	'''componentInstanceName«IF e.part !== null» + ".«e.part.name»"«ENDIF»'''
	
	def getCommand(Reply r) {
	    if(r.command !== null) {
	        return r.command.event as Command
	    }
	    val transition = EcoreUtil2.getContainerOfType(r, TriggeredTransition)
	    if(transition?.trigger !== null) {
	        if(transition.trigger instanceof Command)
	           return transition.trigger as Command
	    }
	    return null
	}

	override dispatch CharSequence generateExpression(ExpressionQuantifier expr){
        var parameters = filterQuantifierVariables(expr)
        '''«IF getCommaScope(expr) != CommaScope::TRANSITION»this.«ELSE»stateOfDecisionClass.«ENDIF»evalQuantifier«quantifiersInMachines.indexOf(expr)»(«FOR p : parameters SEPARATOR ', '»«IF getCommaScope(p) == CommaScope::QUANTIFIER»«QVAR_NAME_PREFIX»«p.name»«ELSE»stateOfDecisionClass.get_«VAR_NAME_PREFIX»«triggerVariables.get(p)»()«ENDIF»«ENDFOR»)'''
    }
}
