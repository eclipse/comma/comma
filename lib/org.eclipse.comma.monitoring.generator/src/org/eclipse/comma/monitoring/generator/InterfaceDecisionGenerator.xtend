/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.generator

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.comma.actions.actions.CommandReplyWithVars
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.ParallelComposition
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.behavior.Clause
import org.eclipse.comma.behavior.behavior.NonTriggeredTransition
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.behavior.behavior.StateMachine
import org.eclipse.comma.behavior.behavior.Transition
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.Variable
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.signature.interfaceSignature.InterfaceEvent
import org.eclipse.comma.signature.interfaceSignature.Notification
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.signature.utilities.InterfaceUtilities
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess

import static extension org.eclipse.comma.actions.utilities.ActionsUtilities.*
import static extension org.eclipse.comma.types.utilities.CommaUtilities.*
import org.eclipse.emf.ecore.EObject
import org.eclipse.comma.actions.actions.PCFragmentDefinition
import org.eclipse.comma.behavior.component.utilities.ClauseFragments
import org.eclipse.comma.behavior.component.utilities.TransitionDecomposer
import org.eclipse.comma.behavior.component.utilities.TransitionFragment
import org.eclipse.comma.behavior.component.utilities.IfHelperAction
import org.eclipse.comma.behavior.component.utilities.StateTransitionAction
import org.eclipse.comma.behavior.component.utilities.FunctionFragmentCall
import org.eclipse.comma.behavior.component.utilities.BehaviorFragment
import org.eclipse.comma.expressions.expression.ExpressionQuantifier

class InterfaceDecisionGenerator extends ConstraintsRulesGenerator {
	protected Signature signature
	
	protected Map<InterfaceEvent, StateMachine> event2Machine
	protected Map<Clause, String> clause2Method
	protected Map<TriggeredTransition, String> transition2Method
	Map<StateMachine, List<InterfaceEvent>> eventPartitions
	Map<Transition, Map<Clause, ClauseFragments>> clauseFragments
	Map<Variable, String> triggerVariables
	
	new(Signature signature, Interface behavior, IFileSystemAccess fsa) {
		super(behavior, fsa)
		this.signature = signature;
	}
	
	def generateUtilityClass(){
		fsa.generateFile(generatedFileName(utilityClassName(signature).toString), generateUtilityClassContent(behavior))
	}
	
	def generateDecisionClass(){
		fsa.generateFile(generatedFileName(decisionClassName(signature).toString), generateDecisionClassContent())
	}
	
	def protected generateUtilityClassContent(AbstractBehavior intdef) {
		val machines = intdef.machines
		triggerVariables = new HashMap<Variable, String>
		clauseFragments = populateClauseFragments(intdef)
		quantifiersInMachines = getQuantifiersInStateMachines(intdef, machines);
		'''
			«generatedPackage»
			
			import java.util.LinkedHashMap;
			import java.util.HashSet;
			import java.util.regex.Pattern;
			import «rootPackage».CState;
			import «rootPackage».utils.Utils;
			import «rootPackage».values.*;
			
			public class «utilityClassName(signature)» extends CState {
				/* global variables from the spec */
				«FOR v : intdef.vars»
					private «toJavaType(v.type)» «VAR_NAME_PREFIX»«v.name» = «defaultValue(v.type)»;
				«ENDFOR»
				
				/* trigger variables */
				«FOR v : triggerVariables.keySet»
				    private «toJavaType(v.type)» «VAR_NAME_PREFIX»«triggerVariables.get(v)»;
				«ENDFOR»
			
				/* variables for current state in machines */
				«FOR sm : machines»
					private String «sm.name»;
				«ENDFOR»
			
				/* getters and setters for global variables */
				«FOR v : intdef.vars»
					public «toJavaType(v.type)» get_«VAR_NAME_PREFIX»«v.name»() {
						return «VAR_NAME_PREFIX»«v.name»;
					}
					
					public void set_«VAR_NAME_PREFIX»«v.name»(«toJavaType(v.type)» p) {
						«VAR_NAME_PREFIX»«v.name» = p;
					}
					
				«ENDFOR»
				/* getters and setters for trigger variables */
				«FOR v : triggerVariables.keySet»
				    public «toJavaType(v.type)» get_«VAR_NAME_PREFIX»«triggerVariables.get(v)»() {
				        return «VAR_NAME_PREFIX»«triggerVariables.get(v)»;
				    }
				    
				    public void set_«VAR_NAME_PREFIX»«triggerVariables.get(v)»(«toJavaType(v.type)» p) {
				        «VAR_NAME_PREFIX»«triggerVariables.get(v)» = p;
				    }
				    
				«ENDFOR»
				/* getters and setter for variables that hold current state */
				«FOR sm : machines»
					public String get_«sm.name»() {
						return «sm.name»;
					}
						
					public void set_«sm.name»(String p) {
						if(!«sm.name».equals(p)){
							activeStates.remove(«sm.name»);
							activeStates.add(p);
						}
						«sm.name» = p;
					}
					
				«ENDFOR»
				«IF quantifiersInMachines.size > 0»
					/* methods that implement quantifiers */
					«FOR quantifier : quantifiersInMachines»
						«generateQuantifierMethod(quantifier, quantifiersInMachines.indexOf(quantifier))»
					«ENDFOR»
					
				«ENDIF»
				/* constructor that performs initialization */
				public «utilityClassName(signature)»() {
					/* initialization of the set with active states of the state machines */
					activeStates = new HashSet<String>();
					«FOR s : machines.map[states].flatten.filter(s | s.initial)»
						activeStates.add("«s.name»");
					«ENDFOR»

					/* initialization of global variables in model init section */
					«FOR a : intdef.initActions» 
						«generateAction(a)»
					«ENDFOR»
			
					/* initilization of current state variables */
					«FOR sm : machines»
						«sm.name» = "«sm.states.filter(s | s.initial).get(0).name»";
					«ENDFOR»
					
					/* initialization of the last execution state */
					executionState = "";
					
					/* initialization of coverage info */
					clauseMap = new LinkedHashMap<String, Integer>();
					stateMap = new LinkedHashMap<String, Integer>();
					
					«FOR m : machines»
						«FOR s : m.states»
							stateMap.put("«s.name»", «IF s.initial»1«ELSE»0«ENDIF»);
							«val allTransitionInState = StateMachineUtilities::transitionsForState(s)»
							«FOR ti : 0..< allTransitionInState.size»
								«FOR ci : 0..< allTransitionInState.get(ti).clauses.size»
									clauseMap.put("«s.name» transition «ti + 1»«getEventName(allTransitionInState.get(ti), allTransitionInState.get(ti).clauses.get(ci))» clause «ci + 1»", 0);
								«ENDFOR»
							«ENDFOR»
						«ENDFOR»
					«ENDFOR»
				}
			
				/* print method */
				public String toString() {
					String result = "Values of global variables and current machine states:" + "\n\n";
					«IF ! intdef.vars.isEmpty»
						«FOR v : intdef.vars» 
							result = result + "«v.name» = " + «VAR_NAME_PREFIX»«v.name» + "\n";
						«ENDFOR»
					«ELSE»
						result = result + "No global variables" + "\n";
					«ENDIF»
					«FOR m : machines» 
						result = result + "Machine «m.name» in state " + «m.name» + "\n";
					«ENDFOR»
					
					return result;
				}
			}
		'''
	}
	
	def populateClauseFragments(AbstractBehavior behavior) {
	    val result = new HashMap<Transition, Map<Clause, ClauseFragments>>
	    var counter = 1
	    val (StateMachine, Transition) => void lambda = [m, t |
	        val clausesMap = new HashMap<Clause, ClauseFragments>
            for(c : t.clauses) {
                val fragments = TransitionDecomposer.decomposeTransition(m, t, c)
                fragments.triggerVariables.entrySet.forEach[triggerVariables.put(it.key, "")]
                clausesMap.put(c, fragments)
            }
            result.put(t, clausesMap)
	    ]
	    
	    for(m : behavior.machines) {
	        //transitions in 'all states' blocks
	        m.inAllStates.forEach[transitions.forEach[lambda.apply(m, it)]]
	        //transitions in states
	        m.states.forEach[transitions.forEach[lambda.apply(m, it)]]
	    }
	    for(t : result.keySet)
	       for(c : result.get(t).keySet)
	           for(f : result.get(t).get(c).subTransitionFragments)
	               f.subState = "_s" + (counter++)
	    counter = 1
	    for(v : triggerVariables.keySet) {
	        triggerVariables.put(v, v.name + "_" + counter++)
	    }
	    result
	}
	
	def protected generateDecisionClassContent(){
		eventPartitions = StateMachineUtilities::getEventPartitions(behavior, signature)
		event2Machine = new HashMap<InterfaceEvent, StateMachine>
		clause2Method = new HashMap<Clause, String>
		transition2Method = new HashMap<TriggeredTransition, String>
		
		val events = InterfaceUtilities::getAllInterfaceEvents(signature)

		for (ev : events) {
			for (sm : eventPartitions.keySet) {
				if (eventPartitions.get(sm).contains(ev)) {
					event2Machine.put(ev, sm)
				}
			}
		}
		val List<StateMachine> machines = behavior.machines
				
		quantifiersInMachines = getQuantifiersInStateMachines(behavior, machines);
		
		'''
			«generatedPackage»
			
			import java.util.ArrayList;
			import java.util.List;
			import java.util.function.BiFunction;
			import java.util.regex.Pattern;
			import «rootPackage».*;
			import «rootPackage».values.*;
			import «rootPackage».messages.*;
			import «rootPackage».utils.Utils;
			import «rootPackage».constraints.CRule;
			
			
			public class «decisionClassName(signature)» extends CInterfaceDecisionClass {
				private «utilityClassName(signature)» stateOfDecisionClass;
				private «utilityClassName(signature)» lastReceivedState;
				private «rulesClassName(signature)» rulesProvider;
				
				public «decisionClassName(signature)»() {
					stateOfDecisionClass = new «utilityClassName(signature)»();
					lastReceivedState = new «utilityClassName(signature)»();
					rulesProvider = new «rulesClassName(signature)»();
				}
			
				public void setState(CState state) {
					lastReceivedState = («utilityClassName(signature)») state;
					stateOfDecisionClass = («utilityClassName(signature)») Utils.deepCopy(lastReceivedState);
				}
				
				public CState getState() {
				    return stateOfDecisionClass;
				}
			
				public CInterfaceDecisionResult consume(CObservedMessage message) {
				    if(message instanceof CObservedReply) {
				        if(stateOfDecisionClass.getSubState() == null) {
				            return noTransitions;
				        }
				        return handleReply((CObservedReply) message);
				    }
				    
					switch(message.getName()) {
					«FOR e : events»
					«var j = 0»
					case "«e.name»" : return «makeEventMethodName(e)»(«IF !(e instanceof Notification)»«FOR p : e.parameters SEPARATOR ', '»«IF p.direction != DIRECTION::OUT»(«toJavaType(p.type)») message.getParameters().get(«j++»)«ELSE»«defaultValue(p.type)»«ENDIF»«ENDFOR»«ELSE»message«ENDIF»);
					«ENDFOR»
					default: return unknownEvent;
					}
				}
			
				«FOR e : events»
				«generateEventRootMethod(e)»
					
				«ENDFOR»
				«generateClauseMethods(machines)»
				«generateTriggeredMethodsInStates(machines, events)»
				«generateNonTriggeredMethodsInStates(machines, events)»
				«generateReplyHandlerMethod»
				
				@Override
				public List<CRule> getRules() {
				    return rulesProvider.getRules();
				}
			}
		'''
	}
	
	def generateReplyHandlerMethod(){
	   val transitions = subTransitionsForReplies
	'''
    private CInterfaceDecisionResult handleReply(CObservedReply message) {
        «IF transitions.empty»
        return noTransitions;
        «ELSE»
        List<CExecutionContext> result = new ArrayList<CExecutionContext>();
        String subState = stateOfDecisionClass.getSubState();
        if(subState != null) {
           switch(subState) {
               «FOR f : subTransitionsForReplies»
               case "«f.subState»": «f.methodName»(stateOfDecisionClass.get_«f.machineName»(), message, result); break;
               «ENDFOR»
               default: return noTransitions;
           }
        } else {
           return noTransitions;
        }
        return checkTransitionsResult(result);
        «ENDIF»
    }
    '''   
	}
	
	def makeEventMethodName(InterfaceEvent e)
	'''_«e.name»'''
	
	def CharSequence generateClauseMethods(List<StateMachine> machines)
	'''
		«FOR m : machines»
			«generateClauseMethods(m)»
		«ENDFOR»
	'''
	
	def CharSequence generateClauseMethods(StateMachine m)
	'''
		«FOR i : 0..< m.inAllStates.size»
			«generateClauseMethods(m, "block" + (i+1), m.inAllStates.get(i).transitions)»
		«ENDFOR»
		«FOR i : 0..< m.states.size»
			«generateClauseMethods(m, m.states.get(i).name, m.states.get(i).transitions)»
		«ENDFOR»
	'''
	
	def CharSequence generateClauseMethods(StateMachine m, String nameFragment, List<Transition> transitions)
	'''
		«FOR i : 0..< transitions.size»
			«FOR j : 0..< transitions.get(i).clauses.size»
				«val methodName = m.name + "_" + nameFragment + "_t" + (i + 1) + "_c" + (j + 1)»
				«val fragments = clauseFragments.get(transitions.get(i)).get(transitions.get(i).clauses.get(j))»
				«{fragments.setMethodNames(methodName)}»
				«FOR fragment : fragments.all»
				«generateClauseMethod(m, transitions.get(i).clauses.get(j), fragment)»
				
				«ENDFOR»
			«ENDFOR»
			«IF transitions.get(i) instanceof TriggeredTransition»
				«generateTriggeredTransitionMethod(m, m.name + "_" + nameFragment + "_t" + (i + 1), transitions.get(i) as TriggeredTransition)»
				
			«ENDIF»
		«ENDFOR»
	'''
	
	def CharSequence generateTriggeredTransitionMethod(StateMachine m, String methodName,
		TriggeredTransition transition) {
		transition2Method.put(transition, methodName)
		'''
			private void «methodName»(String stateName, int tIndex, List<CExecutionContext> result«FOR p : transition.parameters», «toJavaType(p.type)» «TVAR_NAME_PREFIX»«p.name»«ENDFOR») {
				«IF transition.guard !== null»
				«FOR p : transition.parameters»
				stateOfDecisionClass.set_«VAR_NAME_PREFIX»«triggerVariables.get(p)»(«TVAR_NAME_PREFIX»«p.name»);
                «ENDFOR»
				if(«generateExpression(transition.guard)»){
				    «FOR c : transition.clauses»
					«val rootFragment = clauseFragments.get(transition).get(c).rootFragment»
					«rootFragment.getMethodName»(stateName, tIndex, result«FOR p : transition.parameters», «TVAR_NAME_PREFIX»«p.name»«ENDFOR»);
					«ENDFOR»
				}
				«ELSE»
				«FOR c : transition.clauses»
				«val rootFragment = clauseFragments.get(transition).get(c).rootFragment»
				«rootFragment.getMethodName»(stateName, tIndex, result«FOR p : transition.parameters», «TVAR_NAME_PREFIX»«p.name»«ENDFOR»);
				«ENDFOR»
				«ENDIF»
			}
		'''
	}
	
	def CharSequence generateClauseMethod(StateMachine m, Clause clause, BehaviorFragment fragment) {
	    var result = ''''''
		if(fragment instanceof TransitionFragment) {
		    if(fragment.isRoot) {
		        val transition = fragment.transition
		        val eventName = getEventName(transition, clause)
		        val parametersList = if (transition instanceof TriggeredTransition)
                '''«FOR p : transition.parameters», «toJavaType(p.type)» «TVAR_NAME_PREFIX»«p.name»«ENDFOR»'''
                else ''''''
                result =
                '''
                private void «fragment.getMethodName»(String stateName, int tIndex, «IF transition instanceof NonTriggeredTransition»CObservedMessage message, «ENDIF»List<CExecutionContext> result«parametersList») {
                    String clauseKey = stateName + " transition " + (tIndex + 1) + "«eventName»" + " clause «transition.clauses.indexOf(clause) + 1»";
                    CExecutionContext context = new CExecutionContext();
                    stateOfDecisionClass = («utilityClassName(signature)») Utils.deepCopy(lastReceivedState);
                    «IF transition instanceof TriggeredTransition»
                    «FOR p : transition.parameters»
                    stateOfDecisionClass.set_«VAR_NAME_PREFIX»«triggerVariables.get(p)»(«TVAR_NAME_PREFIX»«p.name»);
                    «ENDFOR»
                    «ENDIF»
                    stateOfDecisionClass.takeSnapshot();
                    enactClause(stateOfDecisionClass, clauseKey);
                    CMessagePattern mp;
                    CParallelCompositionPattern pc;
                    BiFunction<CState, List<Object>, Boolean> _condition;
                    «FOR a : fragment.actions»
                    «generateActionInContext(a, null)»
                    «ENDFOR»
                }
                ''' 
		    } else { //transition fragment that is not root
		        result =
		        '''
		        private void «fragment.getMethodName»(String stateName, CObservedMessage message, «IF clause.eContainer instanceof NonTriggeredTransition»CExecutionContext execContext, «ENDIF»List<CExecutionContext> result) {
		            «IF clause.eContainer instanceof NonTriggeredTransition»
		            CExecutionContext context;
		            if(execContext == null) {
		                context = new CExecutionContext();
		                stateOfDecisionClass = («utilityClassName(signature)») Utils.deepCopy(lastReceivedState);
		                stateOfDecisionClass.takeSnapshot(); //TODO reconsider this!
		            } else {
		                context = execContext;
		            }
		            «ELSE»
		            CExecutionContext context = new CExecutionContext();
		            stateOfDecisionClass = («utilityClassName(signature)») Utils.deepCopy(lastReceivedState);
		            stateOfDecisionClass.takeSnapshot(); //TODO reconsider this!
		            «ENDIF»
		            CMessagePattern mp;
		            CParallelCompositionPattern pc;
		            BiFunction<CState, List<Object>, Boolean> _condition;
		            «FOR a : fragment.actions»
		            «generateActionInContext(a, null)»
		            «ENDFOR»
		        }
		        '''
		    }
		} else { //Function fragment
		    result =
                '''
                private void «fragment.getMethodName»(String stateName, CObservedMessage message, CExecutionContext context, List<CExecutionContext> result) {
                    CMessagePattern mp;
                    CParallelCompositionPattern pc;
                    BiFunction<CState, List<Object>, Boolean> _condition;
                    «FOR a : fragment.actions»
                    «generateActionInContext(a, null)»
                    «ENDFOR»
                }
                '''
		}
		result
	}
	
	def CharSequence generateTriggeredMethodsInStates(List<StateMachine> machines, List<InterfaceEvent> events) '''
		«FOR sm : machines»
			«FOR state : sm.states»
				«FOR ev : events.filter(e | !(e instanceof Notification))»
					«val transitions = getTriggeredTransitionsInState(ev, state)»
					«IF ! transitions.empty»
						«generateTriggeredTransitionMethodInState(ev, transitions, state)»
						
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
		«ENDFOR»
	'''
	
	def CharSequence generateNonTriggeredMethodsInStates(List<StateMachine> machines, List<InterfaceEvent> events) ''' 
		«FOR sm : machines»
			«FOR state : sm.states»
				«FOR n : events.filter(Notification)»
					«val transitions = getNonTriggeredTransitionsInState(n, state)»
					«val fragments = getSubTransitions(n, state)»
					«IF ! transitions.empty || ! fragments.empty»
						«generateNonTriggeredTransitionMethodInState(n, transitions, fragments, state)»
						
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
		«ENDFOR»
	'''
	
	def getSubTransitions(Notification n, State s) {
	    val List<TransitionFragment> result = new ArrayList<TransitionFragment>
	    for(t : StateMachineUtilities::transitionsForState(s)) {
	        for(c : t.clauses) {
	            for(f : clauseFragments.get(t).get(c).subTransitionFragments) {
	                val first = f.actions.get(0)
	                if(first instanceof EventWithVars) {
	                    if(first.event == n) {
	                        result.add(f)
	                    }
	                }
	            }
	        }
	    }
	    result
	}
	
	def getSubTransitionsForReplies() {
	    val List<TransitionFragment> result = new ArrayList<TransitionFragment>
	    for(m : clauseFragments.values) {
	        for(f : m.values) {
	            for(subTrans : f.subTransitionFragments) {
	                val first = subTrans.actions.get(0)
	                if(first instanceof CommandReplyWithVars) {
	                    result.add(subTrans)
	                }
	            }
	        }
	    }
	    result
	}
	
	def generateEventRootMethod(InterfaceEvent event){
		val stateMachine = event2Machine.get(event)
		'''
			private CInterfaceDecisionResult «makeEventMethodName(event)»(«IF !(event instanceof Notification)»«FOR p : event.parameters SEPARATOR ', '»«toJavaType(p.type)» «TVAR_NAME_PREFIX»«p.name»«ENDFOR»«ELSE»CObservedMessage message«ENDIF»){
				switch(stateOfDecisionClass.get_«stateMachine.name»()) {
				«FOR s : stateMachine.states»
					«IF !emptyTransitionsInState(event, s)»
					case "«s.name»": return «s.name»_«event.name»(«IF !(event instanceof Notification)»«FOR p : event.parameters SEPARATOR ', '»«TVAR_NAME_PREFIX»«p.name»«ENDFOR»«ELSE»message«ENDIF»);
				«ENDIF»
				«ENDFOR»
				default: return noTransitions;
				}
			}
		'''
	}
	
	def CharSequence generateTriggeredTransitionMethodInState(InterfaceEvent event, List<TriggeredTransition> transitions, State s) {
		'''
			private CInterfaceDecisionResult «s.name»_«event.name»(«FOR p : event.parameters SEPARATOR ', '»«toJavaType(p.type)» «VAR_NAME_PREFIX»«p.name»«ENDFOR») {
				List<CExecutionContext> result = new ArrayList<CExecutionContext>();
				if(stateOfDecisionClass.getSubState() != null) {
				   return noTransitions;
				}
				«FOR t : transitions»
				«val int transitionIndex = StateMachineUtilities::transitionsForState(s).indexOf(t)»
				«IF transitions.indexOf(t) > 0»
				stateOfDecisionClass = («utilityClassName(signature)») Utils.deepCopy(lastReceivedState);
				«ENDIF»
				«transition2Method.get(t)»("«s.name»", «transitionIndex», result«FOR p : event.parameters», «VAR_NAME_PREFIX»«p.name»«ENDFOR»);
				«ENDFOR»
				return checkTransitionsResult(result);
			}
		'''
	}
	
	def CharSequence generateNonTriggeredTransitionBody(NonTriggeredTransition t, State s, InterfaceEvent ev, int transitionIndex) {
		'''
		«FOR c : t.clauses»
		«val rootFragment = clauseFragments.get(t).get(c).rootFragment»
		«IF rootFragment.headNotifications.contains(ev)»
		«rootFragment.getMethodName»("«s.name»", «transitionIndex», message, result);
		«ENDIF»
		«ENDFOR»
		'''
	}
	
	def CharSequence generateNonTriggeredTransitionMethodInState(InterfaceEvent event, List<NonTriggeredTransition> transitions, List<TransitionFragment> fragments, State s) {
		'''
			private CInterfaceDecisionResult «s.name»_«event.name»(CObservedMessage message) {
				List<CExecutionContext> result = new ArrayList<CExecutionContext>();
				String subState = stateOfDecisionClass.getSubState();
				if(subState != null) {
				    switch(subState) {
				        «FOR f : fragments»
				        «val parentTransition = EcoreUtil2::getContainerOfType(f.actions.get(0) as EObject, NonTriggeredTransition)»
				        case "«f.subState»": «f.methodName»("«s.name»", message, «IF parentTransition !== null»null, «ENDIF»result); break;
				        «ENDFOR»
				        default: return noTransitions;
				    }
				} else {
				    «IF transitions.empty»
				    return noTransitions;
				    «ELSE»
				    «FOR t : transitions»
				    «val int transitionIndex = StateMachineUtilities::transitionsForState(s).indexOf(t)»
				    //begin transition
				    «IF transitions.indexOf(t) > 0»
				    stateOfDecisionClass = («utilityClassName(signature)») Utils.deepCopy(lastReceivedState);
				    «ENDIF»
				    «IF t.guard !== null»
				    if(«generateExpression(t.guard)») {
				        «generateNonTriggeredTransitionBody(t, s, event, transitionIndex)»
				    }
				    «ELSE»
				    «generateNonTriggeredTransitionBody(t, s, event, transitionIndex)»
				    «ENDIF»
				    //end of transition
				    «ENDFOR»
				    «ENDIF»
				}
				return checkTransitionsResult(result);
			}
		'''
	}
	
	def boolean emptyTransitionsInState(InterfaceEvent event, State s) {
		if(event instanceof Notification) {
		    val allTransitions = StateMachineUtilities::transitionsForState(s)
		    for(t : allTransitions) {
		        for(c : t.clauses) {
		            val fragments = clauseFragments.get(t).get(c)
		            if(!fragments.subTransitionFragments.filter[headNotifications.contains(event)].empty) {
                        return false
                    }
                    if(t instanceof NonTriggeredTransition && fragments.rootFragment.headNotifications.contains(event)) {
                        return false
                    }
		        }
		    }
		    return true
		}
		else {
		    StateMachineUtilities::getTriggeredTransitions(s.eContainer as StateMachine, s).filter(t | t.trigger == event).empty
		} 
	}
	
	def List<TriggeredTransition> getTriggeredTransitionsInState(InterfaceEvent event, State s) {
		var result = new ArrayList<TriggeredTransition>()
		val allTriggeredTransitions = StateMachineUtilities::getTriggeredTransitions(s.eContainer as StateMachine, s)
		result.addAll(allTriggeredTransitions.filter(t | t.trigger == event))
		result
    }
	
	def List<NonTriggeredTransition> getNonTriggeredTransitionsInState(Notification n, State s) {
		val transitions = StateMachineUtilities::getNonTriggeredTransitions(s.eContainer as StateMachine, s)
		val result = new ArrayList<NonTriggeredTransition>
	    transitions.filter(t | t.clauses.exists(c | clauseFragments.get(t).get(c).rootFragment.headNotifications.contains(n))).forEach[result.add(it as NonTriggeredTransition)]
		result
	}
	
	def dispatch CharSequence generateActionInContext(AssignmentAction a, InterfaceEvent ev) {
		generateAction(a)
	}

	def dispatch CharSequence generateActionInContext(IfAction ifact, InterfaceEvent ev) '''
	if(«generateExpression(ifact.guard)»){
		«FOR a : ifact.thenList.actions»
		«generateActionInContext(a, ev)»
		«ENDFOR»
	«IF ifact.elseList !== null »
		} else {
			«FOR a : ifact.elseList.actions»
			«generateActionInContext(a, ev)»
			«ENDFOR»
	«ENDIF»
	}'''
	
	def dispatch CharSequence generateActionInContext(IfHelperAction ifact, InterfaceEvent ev) '''
	if(«generateExpression(ifact.condition)»){
	   «FOR a : ifact.thenActions»
	   «generateActionInContext(a, ev)»
	   «ENDFOR»
	«IF ifact.elseActions !== null »
	   } else {
	       «FOR a : ifact.elseActions»
	       «generateActionInContext(a, ev)»
	       «ENDFOR»
	«ENDIF»
	}'''
	
	def dispatch CharSequence generateActionInContext(FunctionFragmentCall fc, InterfaceEvent ev)
	'''
	«fc.functionFragment.methodName»(stateName, message, context, result);
	'''

	def dispatch CharSequence generateActionInContext(RecordFieldAssignmentAction a, InterfaceEvent ev) {
		generateAction(a)
	}

	def dispatch CharSequence generateActionInContext(CommandReply a, InterfaceEvent ev) {
		'''
		«messagePatternFromAction(a, ev)»
		context.addToExpectedMessages(new CMessageMultiplicityPattern().setMessagePattern(mp).setInterfaceState((CState)Utils.deepCopy(stateOfDecisionClass)));
		'''
	}

	// Assumption: EventCall is not outgoing call (this is not supported yet)
	def dispatch CharSequence generateActionInContext(EventCall a, InterfaceEvent ev)
	'''
	«messagePatternFromAction(a, ev)»
	context.addToExpectedMessages(new CMessageMultiplicityPattern(mp, «a.normalizedMultiplicity.lower», «a.normalizedMultiplicity.upper»).setInterfaceState((CState)Utils.deepCopy(stateOfDecisionClass)));
	'''
	
	def dispatch CharSequence generateActionInContext(ParallelComposition pc, InterfaceEvent ev) '''
	pc = new CParallelCompositionPattern();
	«FOR c : pc.flatten»
		«messagePatternFromAction(c, ev)»
		pc.addElement(new CMessageMultiplicityPattern(mp, «IF c instanceof EventCall»«c.normalizedMultiplicity.lower», «c.normalizedMultiplicity.upper»«ELSE»1, 1«ENDIF»));
	«ENDFOR»
	context.addToExpectedMessages(pc.setInterfaceState((CState)Utils.deepCopy(stateOfDecisionClass)));'''
	
	def dispatch messagePatternFromAction(CommandReplyWithVars a, InterfaceEvent ev){
        val parentTransition = EcoreUtil2::getContainerOfType(a, TriggeredTransition)
        val commandName = parentTransition.trigger.name
        
        '''
        mp = new CReplyPattern(); «IF commandName !== null»mp.setName("«commandName»");«ENDIF»
        «IF a.condition !== null»
        _condition = (env, params) -> {«utilityClassName(signature)» stateOfDecisionClass = («utilityClassName(signature)») env; return «generateExpression(a.condition)»;};
        mp.setPrecondition(_condition);
        «ENDIF»
        «IF a.parameters.size() > 0»
        mp«FOR p : a.parameters».addParameter(new CVariable("«p.name»", «toJavaReferenceType(p.type)».class))«ENDFOR»;
        «ENDIF»
        '''
    }
	
	def dispatch messagePatternFromAction(EventWithVars a, InterfaceEvent ev)
    '''
    mp = new CNotificationPattern("«a.event.name»");
    «IF a.condition !== null»
    _condition = (env, params) -> {«utilityClassName(signature)» stateOfDecisionClass = («utilityClassName(signature)») env; return «generateExpression(a.condition)»;};
    mp.setPrecondition(_condition);
    «ENDIF»
    «FOR p : a.parameters»
    mp.addParameter(new CVariable("«p.name»", «toJavaReferenceType(p.type)».class));
    «ENDFOR»
    '''
	
	//state is always updated, enactment of transition and state is in the end of the big transition
	def dispatch CharSequence generateActionInContext(StateTransitionAction a, InterfaceEvent ev) '''
	«IF a.toSubState»
	«IF a.inNonTriggeredRoot»
	if(context.expectedMessagesEmpty()) {
	    «a.transitionFragment.methodName»(stateName, message, context, result);
	} else {
	    stateOfDecisionClass.setSubState("«a.nextState»");
	    updateState(stateOfDecisionClass, stateName, context);
	    result.add(context);
	}
	«ELSE»
	stateOfDecisionClass.setSubState("«a.nextState»");
	updateState(stateOfDecisionClass, stateName, context);
	result.add(context);
	«ENDIF»
	«ELSE»
	stateOfDecisionClass.setSubState(null);
	stateOfDecisionClass.set_«a.machineName»(«IF a.nextState !== null»"«a.nextState»"«ELSE»stateName«ENDIF»);
	updateState(stateOfDecisionClass, stateName, «IF a.nextState !== null»"«a.nextState»"«ELSE»stateName«ENDIF», context);
	result.add(context);
	«ENDIF»
	'''
	
	def dispatch messagePatternFromAction(EventCall a, InterfaceEvent ev)
	'''
	mp = new CNotificationPattern(); mp.setName("«a.event.name»");
	«FOR p : a.parameters»
	mp.addParameter(«generateExpression(p)»);
	«ENDFOR»
	'''
	
	def dispatch generateActionInContext(EventWithVars a, InterfaceEvent ev)
	'''
	if(!message.getName().equals("«a.event.name»")) {
	    return;
	}
	«FOR i : 0..< a.parameters.size»
	stateOfDecisionClass.set_«VAR_NAME_PREFIX»«triggerVariables.get(a.parameters.get(i))»((«toJavaType(a.parameters.get(i).type)») message.getParameters().get(«i»));
	«ENDFOR»
	«IF a.condition !== null»
	if(!«generateExpression(a.condition)») {
	    return;
	}
	«ENDIF»
	mp = new CNotificationPattern(); mp.setName("«a.event.name»");
	«FOR p : a.parameters»
	mp.addParameter(new CAny());
	«ENDFOR»
	context.addToExpectedMessages(new CMessageMultiplicityPattern(mp, 1, 1).setInterfaceState((CState)Utils.deepCopy(stateOfDecisionClass)));
	'''
	
	def dispatch generateActionInContext(CommandReplyWithVars a, InterfaceEvent ev) {
	    val parentTransition = EcoreUtil2::getContainerOfType(a, TriggeredTransition)
	    val commandName = parentTransition.trigger.name
	    
	    '''
	    if(!message.getName().equals("«commandName»")) {
	        return;
	    }
	    «FOR i : 0..< a.parameters.size»
	    stateOfDecisionClass.set_«VAR_NAME_PREFIX»«triggerVariables.get(a.parameters.get(i))»((«toJavaType(a.parameters.get(i).type)») message.getParameters().get(«i»));
	    «ENDFOR»
	    «IF a.condition !== null»
	    if(!«generateExpression(a.condition)») {
	        return;
	    }
	    «ENDIF»
	    mp = new CReplyPattern(); mp.setName("«commandName»");
	    «IF a.parameters.size() > 0»
	    mp«FOR p : a.parameters».addParameter(new CAny())«ENDFOR»;
	    «ENDIF»
	    context.addToExpectedMessages(new CMessageMultiplicityPattern().setMessagePattern(mp).setInterfaceState((CState)Utils.deepCopy(stateOfDecisionClass)));
	    '''
	    
	}
	
	def dispatch messagePatternFromAction(CommandReply a, InterfaceEvent ev){
		var String commandName = ""
		if (ev !== null && ev instanceof Command) {
			commandName = ev.name
		} else if (a.command !== null) {
			commandName = a.command.event.name
		} else {
			val parentTransition = EcoreUtil2::getContainerOfType(a, TriggeredTransition)
			if(parentTransition !== null) commandName = parentTransition.trigger.name
		}
		'''
		mp = new CReplyPattern(); «IF commandName !== null»mp.setName("«commandName»");«ENDIF»
		«IF a.parameters.size() > 0»
		mp«FOR p : a.parameters».addParameter(«generateExpression(p)»)«ENDFOR»;
		«ENDIF»
		'''
	}
	
	def CharSequence getEventName(Transition t, Clause c) {
		if (t instanceof TriggeredTransition) {
			''' «t.trigger.name»'''
		} else {
			val eventCalls = clauseFragments.get(t).get(c).rootFragment.headNotifications
			if (eventCalls.empty) '''''' else ''' «eventCalls.get(0).name»'''
		}
	}
	
	static def decisionClassName(Signature s)
	'''«s.fullyQualifiedName.toString("_")»DecisionClass'''
	
	static def decisionClassName(Interface i)
    '''«i.fullyQualifiedName.toString("_")»DecisionClass'''
    
    override CharSequence generateVariableReference(ExpressionVariable expr){
        val parent = expr.variable.eContainer 
        if(parent instanceof TriggeredTransition) {
            if(EcoreUtil2.getContainerOfType(expr, ExpressionQuantifier) !== null) {
                '''«TVAR_NAME_PREFIX»«expr.variable.name»'''
            } else {
                '''stateOfDecisionClass.get_«VAR_NAME_PREFIX»«triggerVariables.get(expr.variable)»()'''
            }
        } else if(parent instanceof ActionWithVars) {
            val parentOfParent = parent.eContainer
            if(parentOfParent instanceof ParallelComposition || parentOfParent instanceof PCFragmentDefinition) {
                if(EcoreUtil2.getContainerOfType(expr, ExpressionQuantifier) !== null) {
                    '''«TVAR_NAME_PREFIX»«expr.variable.name»'''
                } else {
                    val index = parent.parameters.indexOf(expr.variable)
                    '''((«toJavaReferenceType(expr.variable.type)»)params.get(«index»))'''   
                }
            } else if(EcoreUtil2.getContainerOfType(expr, ExpressionQuantifier) !== null){
                '''«TVAR_NAME_PREFIX»«expr.variable.name»'''
            } else {
                '''stateOfDecisionClass.get_«VAR_NAME_PREFIX»«triggerVariables.get(expr.variable)»()'''
            }
        } else {
            super.generateVariableReference(expr)
        }
    }

	override dispatch CharSequence generateExpression(ExpressionQuantifier expr){
        var parameters = filterQuantifierVariables(expr)
        '''«IF getCommaScope(expr) != CommaScope::TRANSITION»this.«ELSE»stateOfDecisionClass.«ENDIF»evalQuantifier«quantifiersInMachines.indexOf(expr)»(«FOR p : parameters SEPARATOR ', '»«IF getCommaScope(p) == CommaScope::QUANTIFIER»«QVAR_NAME_PREFIX»«p.name»«ELSE»«IF p.eContainer.eContainer instanceof ParallelComposition»((«toJavaReferenceType(p.type)»)params.get(«(p.eContainer as ActionWithVars).parameters.indexOf(p)»))«ELSE»stateOfDecisionClass.get_«VAR_NAME_PREFIX»«triggerVariables.get(p)»()«ENDIF»«ENDIF»«ENDFOR»)'''
    }
}
