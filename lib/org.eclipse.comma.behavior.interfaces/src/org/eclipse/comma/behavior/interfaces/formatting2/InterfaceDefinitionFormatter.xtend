/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.formatting2

import com.google.inject.Inject
import org.eclipse.comma.behavior.formatting2.BehaviorFormatter
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.services.InterfaceDefinitionGrammarAccess
import org.eclipse.comma.types.types.Import
import org.eclipse.xtext.formatting2.IFormattableDocument

class InterfaceDefinitionFormatter extends BehaviorFormatter {	

	@Inject extension InterfaceDefinitionGrammarAccess

	def dispatch void format(InterfaceDefinition interfaceDefinition, extension IFormattableDocument document) {
		
		for (Import _import : interfaceDefinition.getImports()) {
			_import.format;
		}
		
		interfaceDefinition.getInterface.format;		
	}

	def dispatch void format(Interface _interface, extension IFormattableDocument document) {		
		val rFinder = _interface.regionFor
		
		rFinder.keyword(interfaceAccess.interfaceKeyword_1).prepend(emptyLine)
		rFinder.keyword(interfaceAccess.interfaceKeyword_1).append(oneSpace)
		
		rFinder.keyword(interfaceAccess.initKeyword_5_0).prepend(emptyLine)	
		
		val kwVariables = rFinder.keyword(interfaceAccess.variablesKeyword_4_0)
		kwVariables.prepend(emptyLine)
		
		val machine = _interface.machines.get(0)
		if(kwVariables !== null && machine !== null) {
			document.set(kwVariables.previousHiddenRegion, machine.regionForEObject.previousHiddenRegion, indent);			
		}		
		
		_interface.formatAbstractBehavior(document)
	}
}
