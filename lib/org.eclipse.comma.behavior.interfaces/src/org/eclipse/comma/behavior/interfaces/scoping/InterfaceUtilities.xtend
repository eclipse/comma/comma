/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.scoping

import java.util.List
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.expressions.expression.ExpressionPackage
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.utilities.CommaUtilities
import org.eclipse.xtext.scoping.IScopeProvider

class InterfaceUtilities {
    
	def static Signature getSignature(Interface context, IScopeProvider scopeProvider) {
		val List<Signature> signatures = getSignatures(context, scopeProvider)
		
		return signatures.findFirst[CommaUtilities::getFullyQualifiedName(it).equals(CommaUtilities::getFullyQualifiedName(context))]
	} 
	
	static def List<Signature> getSignatures(Interface context, IScopeProvider scopeProvider) {
		CommaUtilities::resolveProxy(context, scopeProvider.
			getScope(context, ExpressionPackage.Literals.INTERFACE_AWARE_TYPE__INTERFACE).allElements)
	}  
}