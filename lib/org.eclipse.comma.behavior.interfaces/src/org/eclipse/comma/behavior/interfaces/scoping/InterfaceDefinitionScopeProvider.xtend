/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.scoping

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.InterfaceEventInstance
import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScope

import static org.eclipse.xtext.scoping.Scopes.*

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class InterfaceDefinitionScopeProvider extends AbstractInterfaceDefinitionScopeProvider {
    override getScope(EObject context, EReference reference){
        if(context instanceof TriggeredTransition && reference == BehaviorPackage.Literals.TRIGGERED_TRANSITION__TRIGGER) {
            return scope_InterfaceEvent(context)
        }
        if(context instanceof InterfaceEventInstance && reference == ActionsPackage.Literals.INTERFACE_EVENT_INSTANCE__EVENT) {
            return scope_InterfaceEvent(context)
        }
        return super.getScope(context, reference);
    }
    
    def scope_InterfaceEvent(EObject context){ 
        val interface = EcoreUtil2.getContainerOfType(context, Interface)
        if(interface === null) return IScope.NULLSCOPE
        val sig = InterfaceUtilities::getSignature(interface, super.delegate)
        if(sig !== null){
            return scopeFor(org.eclipse.comma.signature.utilities.InterfaceUtilities::getAllInterfaceEvents(sig))
        }
        return IScope.NULLSCOPE
    }
    
    override List<Signature> findVisibleInterfaces(EObject context){
        val result = new ArrayList<Signature>()
        val interface = EcoreUtil2.getContainerOfType(context, Interface)
        if(interface === null) return result
        val sig = InterfaceUtilities::getSignature(interface, super.delegate)
        if(sig !== null){
            result.add(sig)
        }
        return result
    }
}
