/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class InterfaceDefinitionStandaloneSetup extends InterfaceDefinitionStandaloneSetupGenerated {

	def static void doSetup() {
		new InterfaceDefinitionStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
