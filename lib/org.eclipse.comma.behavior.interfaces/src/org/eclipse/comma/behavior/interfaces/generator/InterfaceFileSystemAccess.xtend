/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.generator

import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2

class InterfaceFileSystemAccess {

	final static public String UML = "uml/"

	def static getUMLFileSystemAccess(Resource interfaceResource, IFileSystemAccess2 fsa) {
		new CommaFileSystemAccess(UML, fsa).addFolder(interfaceResource.getURI().lastSegment)
	}
	
	def static getUMLFileSystemAccess(String folder, IFileSystemAccess2 fsa) {
		new CommaFileSystemAccess(UML, fsa).addFolder(folder)
	}

}
