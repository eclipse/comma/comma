/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.validation

import com.google.inject.Inject
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set
import org.eclipse.comma.actions.actions.ActionList
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.ParallelComposition
import org.eclipse.comma.actions.actions.Reply
import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.comma.behavior.behavior.Transition
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.scoping.InterfaceUtilities
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.comma.expressions.expression.ExpressionPackage
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.signature.interfaceSignature.InterfaceEvent
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignatureDefinition
import org.eclipse.comma.signature.interfaceSignature.Notification
import org.eclipse.comma.types.types.FileImport
import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.validation.Check
import org.eclipse.comma.actions.actions.EventWithVars

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class InterfaceDefinitionValidator extends AbstractInterfaceDefinitionValidator {

	@Inject IScopeProvider scopeProvider
	@Inject extension IQualifiedNameProvider

	public static final String INTERFACE_UNUSED_SIGNAL = "interface_unused_signal"
	public static final String INTERFACE_UNUSED_COMMAND = "interface_unused_command"
	public static final String INTERFACE_UNUSED_NOTIFICATION = "interface_unused_nontification"
	public static final String REPLY_NOT_POSSIBLE = "trigger_remove"
	
	/*
	 * Constraints:
	 * - the interface name and the name of the imported signature are the same
	 */
	@Check
	def checkInterfaceNameEqualSignatureName(Interface i) {
        val signatures = 
            InterfaceUtilities.getSignatures(i, scopeProvider)
                .filter[fullyQualifiedName.equals(i.fullyQualifiedName)].toSet
        if(signatures.empty){
            error("There is no visible signature with name equal to the interface name", TypesPackage.Literals.NAMED_ELEMENT__NAME)   
        } else if(signatures.size > 1) {
            error("There is more than one visible signature with name equal to the interface name", TypesPackage.Literals.NAMED_ELEMENT__NAME)  
        }
	}
	
	/*
	 * Constraints:
	 * - an interface model imports one interface signature
	 */
	@Check
	override checkImportForValidity(FileImport imp) {
		if (! EcoreUtil2.isValidUri(imp, URI.createURI(imp.importURI)))
			error("Invalid resource", imp, TypesPackage.eINSTANCE.fileImport_ImportURI)
		else {
			val Resource r = EcoreUtil2.getResource(imp.eResource, imp.importURI)
			val root = r.allContents.head
			if (! (root instanceof InterfaceSignatureDefinition))
				error("The imported resource is not an interface signature definition.", imp,
					TypesPackage.eINSTANCE.fileImport_ImportURI)
		}
	}
	
	/*
	 * Constraints:
	 * - warning is given about interface events not used in the state machines
	 */
	@Check
	def checkUnusedInterfaceEvents(Interface cInterface) {
		val signature = InterfaceUtilities::getSignature(cInterface, scopeProvider)
		if(signature === null){
			return
		}
		val Set<InterfaceEvent> usedEvents = new HashSet<InterfaceEvent>()
		
		EcoreUtil2.getAllContentsOfType(cInterface, TriggeredTransition).forEach[usedEvents.add(it.trigger)]
		EcoreUtil2.getAllContentsOfType(cInterface, EventCall).forEach[
			if (it.event !== null && (it.event instanceof Notification)) {
				usedEvents.add(it.event)
			}]
		EcoreUtil2.getAllContentsOfType(cInterface, EventWithVars).forEach[
            if (it.event !== null && (it.event instanceof Notification)) {
                usedEvents.add(it.event)
            }]
		signature.commands.forEach[
			if (!usedEvents.contains(it)) {
				warning("Unused command " + it.name, cInterface, TypesPackage.Literals.NAMED_ELEMENT__NAME,
						INTERFACE_UNUSED_COMMAND, it.name)}]
		signature.signals.forEach[
			if (!usedEvents.contains(it)) {
				warning("Unused signal " + it.name, cInterface, TypesPackage.Literals.NAMED_ELEMENT__NAME,
					INTERFACE_UNUSED_SIGNAL, it.name)}]
		signature.notifications.forEach[
			if (!usedEvents.contains(it)) {
				warning("Unused notification " + it.name, cInterface, TypesPackage.Literals.NAMED_ELEMENT__NAME,
					INTERFACE_UNUSED_NOTIFICATION, it.name)}]
	}
	
	/*
	 * Constraints:
	 * - the triggers of transitions can only be signals and commands
	 */
	@Check
	def checkTrigger(TriggeredTransition t) {
		if (t.trigger !== null && (t.trigger instanceof Notification)) {
			error("Trigger must be a command or a signal", BehaviorPackage.Literals.TRIGGERED_TRANSITION__TRIGGER)
		}
	}
	
	/*
	 * Constraints:
	 * - Only notifications can be sent within the transition bodies, that is,
	 *   sending commands and signals is not allowed.
	 * Note: replies to commands can also be sent but they are treated in other constraints 
	 */
	@Check
	def checkOutgoingEvents(EventCall e) {
		if (e.event !== null && !(e.event instanceof Notification)) {
			error("Only notifications are allowed as outgoing events", ActionsPackage.Literals.INTERFACE_EVENT_INSTANCE__EVENT)
		}
	}
	
	@Check
    def checkOutgoingEvents_1(EventWithVars e) {
        if (e.event !== null && !(e.event instanceof Notification)) {
            error("Only notifications are allowed as outgoing events", ActionsPackage.Literals.EVENT_WITH_VARS__EVENT)
        }
    }
	
	
	/*
	 * Constraints:
	 * - if the trigger of a transition is a command then all clauses have to send a reply;
	 *   Furthermore, all execution paths in a clause have to contain a reply
	 * Note: it is difficult to ensure by static checking that exactly one reply is sent.
	 * During the monitoring a runtime check is performed as well and a runtime error is 
	 * generated if a clause sends zero or more than one reply.
	 */
	@Check
	def checkTransitionForReplies(TriggeredTransition t) {
		if (t.trigger === null || !(t.trigger instanceof Command)) {return}
		//all clauses have to have a reply. If a reply is present, all paths should have a reply
		t.clauses.forEach[
			if(!EcoreUtil2::getAllContentsOfType(it, Reply).empty) {
				if (!hasReply(it.actions))
						error("The body of this transition has a path that is missing a reply.", it,
							BehaviorPackage.Literals.CLAUSE__ACTIONS)
			} else{
				error("The body of this transition is missing a reply.", it,
							BehaviorPackage.Literals.CLAUSE__ACTIONS)
			}
		]
	}
		
	/*
	 * Checks if the possible paths in a list of actions has a reply
	 */
	def boolean hasReply(ActionList actionList) {
		if (actionList === null) {
			return false
		}
		for(action : actionList.actions){
			if(action instanceof Reply){
				return true
			} else
			if(action instanceof ParallelComposition){
				if(! action.components.filter(Reply).empty)
					return true
			} else
			if (action instanceof IfAction) {
				if (hasReply(action.thenList) && hasReply(action.elseList)) {
					return true
				}
			}
		}
		return false
	}
	
	/*
	 * Constraints:
	 * - out parameters of commands are write-only, that is, they can only be used
	 *   in the left-hand side of assignments and as reply parameters
	 */
	@Check
	def checkOutVariableUsage(ExpressionVariable exp){
		val v = exp.variable //the variable definition
		val container = v.eContainer //the context of the var definition
		if(container instanceof TriggeredTransition){
			val index = container.parameters.indexOf(v)
			if(index < container.trigger.parameters.size  && container.trigger.parameters.get(index).direction == DIRECTION::OUT){
				// we are dealing with an out parameter. 
				//The allowed usage is only as a parameter in reply
				if(!(exp.eContainer instanceof CommandReply)){
					error("Out parameters are write-only.", ExpressionPackage.Literals.EXPRESSION_VARIABLE__VARIABLE)
				}
			}
		}
	}
	
	/*
	 * Constraints:
	 * - depending on the context of use, replies obey different constraints.
	 *   In any case, the number and types of parameters in replies must 
	 *   conform to the command definition. In addition, replies used 
	 *   in state machines have specific constraints. Consult the two methods
	 *   checkReplyInConstraint and checkReplyInTransition for details.
	 */
	@Check
	def checkReply(Reply r) {
		val context = StateMachineUtilities::getTransitionContainer(r);
		if (context === null){
			checkReplyInConstraint(r as CommandReply)
		}
		else{
			checkReplyInTransition(context, r)
		}
	}
	
	/*
	 * Constraints:
	 * - when used in time and data rules, reply may omit parameters and 
	 *   may skip the indication to which command it is given.
	 *   If the command is given and the parameters are used then their number
	 *   and type must conform to the command definition. 
	 */
	def checkReplyInConstraint(CommandReply r){
		if((r.parameters.size() > 0) && (r.command !== null) && (r.command.event !== null)){
			if(! (r.command.event instanceof Command)) return
			val command = r.command.event as Command
			checkReplyAgainstCommand(command, r)
		}
	}
	
	/*
	 * Constraints:
	 * If a reply is used in a transition:
	 * - the transition has a trigger that is a command
	 * - the reply cannot give an indication for its command
	 *   (this can always be deduced from the context)
	 * - the number and type of the parameters conform to the command definition
	 */
	def checkReplyInTransition(Transition context, Reply r) {
		if(context instanceof TriggeredTransition){
			//trigger is present. Check if it is a command
			if( ! (context.trigger instanceof Command) ){
				error('Reply cannot be used for triggers that are notifications or signals.', 
					r.eContainer, r.eContainingFeature, (r.eContainer.eGet(r.eContainingFeature) as EList<EObject>).indexOf(r), REPLY_NOT_POSSIBLE, null)
				return
			}
			// reply is used for a command trigger. Check parameter number and types
			if(r instanceof CommandReply && (r as CommandReply).command !== null){
				error('Reply cannot indicate a command in this transition.', ActionsPackage.Literals.REPLY__COMMAND)
				return
			}
			val trigger = (context.trigger as Command)
			checkReplyAgainstCommand(trigger, r)
		}
		else{ //non-triggered transition, reply cannot be used
			error('Reply cannot be used in a non-triggered transition', 
				r.eContainer, r.eContainingFeature, (r.eContainer.eGet(r.eContainingFeature) as EList<EObject>).indexOf(r), REPLY_NOT_POSSIBLE, null)
		}
	}
	
	/*
	 * Constraints:
	 * - the behavior of events is specified in a single state machine.
	 *   This is ensured by the static check that the sets of events for each state
	 *   machine are disjoint.
	 */
	@Check
	def checkDisjointEvents(Interface behaviorModel){
		val List<Set<InterfaceEvent>> setsOfTriggers = new ArrayList<Set<InterfaceEvent>>()
		for(sm : behaviorModel.machines){
			val eventsForMachine = new HashSet<InterfaceEvent>(StateMachineUtilities::getAllTriggers(sm))
			eventsForMachine.addAll(StateMachineUtilities::getAllNotifications(sm))		
			for(set : setsOfTriggers){
				val intersection = new HashSet<InterfaceEvent>(eventsForMachine)
				intersection.retainAll(set)
				if(intersection.size > 0){
					error("Some events are used in more than one machine", TypesPackage.Literals.NAMED_ELEMENT__NAME)
					return
				}
			}
			setsOfTriggers.add(eventsForMachine)
		}
	}
	
	@Check
    def checkTypesForDuplications(InterfaceDefinition intDef){
        val multiMap = intDef.importedTypes
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported type " + entry.key.toString, intDef.interface, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
    }
}
