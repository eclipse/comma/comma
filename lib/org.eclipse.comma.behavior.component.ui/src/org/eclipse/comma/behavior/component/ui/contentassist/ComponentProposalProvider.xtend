/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.ui.contentassist

import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class ComponentProposalProvider extends AbstractComponentProposalProvider {

	override complete_Component(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_Component(model, ruleCall, context, acceptor)
		acceptComponentWithPorts(model, ruleCall, context, acceptor)
	}

	def acceptComponentWithPorts(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
			
		val interfaces = scopeProvider.getScope(model, BehaviorPackage.Literals.PORT__INTERFACE)		
		val prop = '''
			component NewComponent 
			
			«IF !interfaces.allElements.empty»	
			«FOR cInterface : interfaces.allElements»
			provided port «cInterface.name.toString» «cInterface.name.toString»Port
			«ENDFOR»
			«ELSE»
			provided port iA iAPort
			«ENDIF»
				
			functional constraints
			
			// state-based functional constraints
			FC1 {
			    use events
			    «IF !interfaces.allElements.empty»
			    «FOR cInterface : interfaces.allElements»
			    any «cInterface.name.toString»Port::event
			    «ENDFOR»
			    «ELSE»
			    any iAPort::event
			    «ENDIF»
			    
			    initial state initialState {
			    
			    }
			}
			«IF !interfaces.allElements.empty»
			
			// predicate functional constraint
			FC2
			always some at «interfaces.allElements.get(0).name.toString»Port in someState
			«ENDIF»
		'''
		acceptor.accept(createTemplate("Component", prop, "Component Definition with ports and functional constraints", 0, context))
	}
	
}
