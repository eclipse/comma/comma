/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.tests

import com.google.inject.Inject
import org.eclipse.comma.behavior.component.component.ComponentModel
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(ComponentInjectorProvider)
class ComponentParsingTest {
@Inject
ParseHelper<ComponentModel> parseHelper

	@Test
	def void loadModel() {
		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA
				notification a::nA
				signal a::sA
				signal b::sB

				variables
				bool b

				initial state Idle {
					transition trigger: b::sB
					next state: Idle

					transition trigger: a::cA guard: a in S1 do:
					b := a in S2
					a::reply to command cA
					next state: Active
				}

				state Active {
					transition trigger: b::sB do:
					   a::nA
					next state: Active

					transition trigger: a::sA
					next state: Idle
				}
			}

			timing constraints

			TR1 signal b::sB -[..180.0 ms]-> notification a::nA
		''')
		Assertions.assertNotNull(result)
		Assertions.assertTrue(result.eResource.errors.isEmpty)
	}
}
