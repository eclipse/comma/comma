/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.tests

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.component.component.ComponentModel
import org.eclipse.comma.behavior.component.test.generator.ImportLoader
import org.eclipse.comma.behavior.component.test.generator.MultiLangInjectorProvider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*
import static org.eclipse.comma.behavior.component.component.ComponentPackage.Literals.*
import static org.eclipse.comma.types.types.TypesPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)

class ComponentValidtionTest {
	@Inject extension ParseHelper<ComponentModel> parseHelper

	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	@Test
	def void wrongImportTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"
			import "iA.signature"

			component C

			provided port iA a
			provided port iB b
			''', set)
		assertError(result, IMPORT, null, "The imported resource has to be interface or component model.")
	}
	
	@Test
	def void duplicateStateFCTest(){ //FC stands for Functional Constraint
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}

				state Idle {
					transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}
			}
			''', set)
		assertError(result, STATE, null, "Duplicate state name")
	}

	@Test
	def void duplicateFCTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}
			}

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do: 
					   a::reply to command cA
					next state: Idle
				}
			}
			''', set)
		assertError(result, FUNCTIONAL_CONSTRAINT, null, "Duplicate constraint name")
	}

	@Test
	def void duplicatePortsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB a

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}
			}
			''', set)
		assertError(result, PORT, null, "Duplicate port name")
	}

	@Test
	def void noInitialStateFCTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}
			}
			''', set)
		assertError(result, FUNCTIONAL_CONSTRAINT, null, "Missing initial state")
	}

	@Test
	def void multipleInitialStatesFCTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}

				initial state Idle1 {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle1
				}
			}
			''', set)
		assertError(result, FUNCTIONAL_CONSTRAINT, null, "More than one initial state")
	}

	@Test
	def void subsumedEventTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				any a::event
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}
			}
			''', set)
		assertWarning(result, FUNCTIONAL_CONSTRAINT, null, "The event is duplicated or subsumed by another event")
	}

	@Test
	def void undeclaredEventTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					   a::nA
					next state: Idle
				}
			}
			''', set)
		assertError(result, EVENT_CALL , null, "Event is not declared in used events list")
	}

	@Test
	def void unusedVariableTest(){
		val set = resourceSetProvider.get()
		ImportLoader.loadInterfaces(set)

		val result = parseHelper.parse('''
			import "iA.interface"
			import "iB.interface"

			component C

			provided port iA a
			provided port iB b

			functional constraints

			abInteraction {
				use events
				command a::cA
				a::reply to command cA

				variables
				int i

				initial state Idle {
				    transition trigger: a::cA do:
					   a::reply to command cA
					next state: Idle
				}
			}
			''', set)
		assertWarning(result, FUNCTIONAL_CONSTRAINT, null, "Unused variable")
	}
}
