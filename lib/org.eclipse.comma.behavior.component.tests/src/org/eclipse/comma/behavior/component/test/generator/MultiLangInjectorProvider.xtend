/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.test.generator

import org.eclipse.comma.behavior.component.tests.ComponentInjectorProvider
import org.eclipse.comma.behavior.interfaces.tests.InterfaceDefinitionInjectorProvider

class MultiLangInjectorProvider extends ComponentInjectorProvider{
	override protected internalCreateInjector() {
		new ComponentInjectorProvider().getInjector
		new InterfaceDefinitionInjectorProvider().getInjector
		return super.internalCreateInjector()
	}
}