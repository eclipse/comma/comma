/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.test.generator

import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.util.StringInputStream

class ImportLoader {
	
	def static loadInterfaces(ResourceSet rs){
		loadSignatureA(rs)
		loadSignatureB(rs)
		loadInterfaceA(rs)
		loadInterfaceB(rs)
	}
	
	def static loadSignatureA(ResourceSet rs){
		val res = rs.createResource(URI.createURI("iA.signature"))
		res.load(new StringInputStream("
			signature iA

			commands
			void cA

			signals
			sA

			notifications
			nA
		"), emptyMap)
	}
	
	def static loadInterfaceA(ResourceSet rs){
		val res = rs.createResource(URI.createURI("iA.interface"))
		res.load(new StringInputStream("
			import 'iA.signature'

			interface iA

			machine StateMachine {
			
				initial state S1 {
			
					transition trigger: cA
						do: reply
					next state: S2
				}
			
				state S2 {
			
					transition trigger: sA
					next state: S1
			
					transition
						do: nA
					next state: S2
				}
			}
		"), emptyMap)
	}
	
	def static loadSignatureB(ResourceSet rs){
		val res = rs.createResource(URI.createURI("iB.signature"))
		res.load(new StringInputStream("
			signature iB

			signals
			sB
		"), emptyMap)
	}
	
	def static loadInterfaceB(ResourceSet rs){
		val res = rs.createResource(URI.createURI("iB.interface"))
		res.load(new StringInputStream('''
			import "iB.signature"
			
			interface iB
			
			machine StateMachine {
				
				initial state S {
					
					transition trigger: sB
						next state: S
					
				}
			}
		'''), emptyMap)
	}
}
