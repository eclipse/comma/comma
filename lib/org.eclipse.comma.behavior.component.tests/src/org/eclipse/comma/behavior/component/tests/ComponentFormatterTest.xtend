/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(ComponentInjectorProvider)
class ComponentFormatterTest {

	@Inject extension FormatterTestHelper

	@Test def void formatComponentGeneric() {
			assertFormatted[
			expectation = '''
				import "Install.interface"

				component Office

				provided port Install I
				required port Install ISensor
				required port Light L

				timing constraints
				OnForwardS command I::On(*) - [ 1.0 ms .. 10.0 ms ] -> command SensorI::On(*)
			'''

			toBeFormatted = '''
				import   "Install.interface"	component  Office	provided  port  Install  I	required  port  Install  ISensor
					required  port  Light  L	timing
				constraints 	OnForwardS	 command  I  :: On(*)  -  [  1.0 ms .. 10.0 ms ] -> command SensorI::On(*)
			'''
		]
	}
}
