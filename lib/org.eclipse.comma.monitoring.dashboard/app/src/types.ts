/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Task from './model/Task';
import Trace from './model/Trace';
import Connection from './model/Connection';
import Statistic from './model/Statistic';

type Kind = 'interface' | 'component';

type Status = 'failed' | 'warning' | 'successful';

type StatisticKind = 'data' | 'time';

interface StatisticData {
    name: string;
    mean: number;
    min: number;
    max: number;
    median: number;
    standardDeviation: number;
    variance: number;
    values: number[];
    valuesPerVariable: {[s: string]: number[]} | null;
    lowerBound: number | null;
    upperBound: number | null;
}

interface StatusEntry {
    name: string;
    status: Status;
}

interface Warning {
    ruleName: string,
    errorMessage: string,
    umlBase64: string,
}

interface FunctionalConstraintError {
    constraintName: string,
    error: {
        errorDescription: string,
        contextError: string[],
        umlBase64: string,
    },
}

interface MonitoringError {
    errorDescription: string,
    contextError: string[],
    umlBase64: string,
}

// eslint-disable-next-line
interface JsonData {[s: string]: any}

interface CoverageInfo {
    interfaceName: string;
    transitionCoverage: {[s: string]: number};
    stateCoverage: {[s: string]: number};
    transitionCoveragePercentage: number;
    stateCoveragePercentage: number;
}

interface CoverageSummary {
    name: string;
    statePercentage: number;
    transitionPercentage: number;
}

type DialogType = 'monitoringError' | 'warnings' | 'coverage' | 'componentWarnings' | 'componentFunctionalConstraintErrors' | 'statistic';
type DialogObject = Task | Connection | Trace | Statistic;

export type {
    Kind, Status, Warning, JsonData, MonitoringError, CoverageInfo, CoverageSummary, DialogType, StatusEntry,
    FunctionalConstraintError, StatisticKind, DialogObject, StatisticData,
}
