/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import AppBar from '@material-ui/core/AppBar';

class Header extends React.Component {
    render(): React.ReactNode {
        return (
            <AppBar position="relative">
                <h2 style={{marginLeft: '10px'}}>CommaSuite monitoring results</h2>
            </AppBar>
        );
    }
}

export default Header;
