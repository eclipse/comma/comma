/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Box from './Box';
import StatusTable from './StatusTable';
import CoverageTable from './CoverageTable';
import Trace from '../model/Trace';
import Statistic from '../model/Statistic';
import Task from '../model/Task';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

interface IProps {
    selectedTask: Task | null;
    selectedTrace: Trace | null;
    flex: number;
    onSelectTrace: (trace: Trace) => void;
    onCoverageDetails: (task: Task, connectionName: string) => void;
    onSelectStatistic: (statistic: Statistic) => void;
}

class TaskView extends React.Component<IProps> {
    renderStatistics(): React.ReactNode {
        const task = this.props.selectedTask;
        const styleName = {
            paddingRight: '0px',
            wordBreak: 'break-all' as const,
        };

        return (
            <div>
                <h3>Statistics</h3>
                {task?.statistics.length ?
                (
                    <TableContainer component={Paper}>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell style={styleName}>Name</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {task?.statistics.map((statistic, i) => (
                                <TableRow key={i}>
                                    <TableCell style={styleName}>
                                        <label
                                            onClick={() => this.props.onSelectStatistic(statistic)}
                                            style={{color: '#007bff', textDecoration: 'underline', cursor: 'pointer'}}>
                                            {statistic.name}
                                        </label>
                                    </TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                ) : (
                    "No statistics available"
                )}
            </div>
        )
    }

    renderTask(): React.ReactNode {
        const task = this.props.selectedTask;
        if (task === null) return null;

        const traces = task.traces;
        const entries = traces.map((t) => {
            return {
                status: t.getStatus(),
                name: `${t.file}`,
            }
        });

        let selectedEntry = null;
        if (this.props.selectedTrace !== null) {
            selectedEntry = entries[traces.indexOf(this.props.selectedTrace)]
        }

        return (
            <div>
                <h3>Coverage</h3>
                <CoverageTable
                    name="Interface"
                    onSelectCoverageSummary={(r) => this.props.onCoverageDetails(task, r.name)}
                    coverageSummaries={task.getCoverageSummaries()}
                />
                {this.renderStatistics()}
                <h3>Traces</h3>
                <StatusTable
                    entries={entries}
                    selectedEntry={selectedEntry}
                    onClick={(row) => this.props.onSelectTrace(traces[entries.indexOf(row)])}
                />
            </div>
        )
    }

    render(): React.ReactNode {
        return (
            <Box title="Task" flex={this.props.flex}>
                {this.props.selectedTask  ? this.renderTask() : <p>Select a task</p>}
            </Box>
        );
    }
}

export default TaskView;
