/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import {default as MaterialDialog} from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Connection from '../model/Connection';
import { DialogType, CoverageInfo, DialogObject } from '../types';
import Task from '../model/Task';
import Trace from '../model/Trace';
import StatisticView from './StatisticView';
import Statistic from '../model/Statistic';

const styleCode = {
    'userSelect': 'text' as const, 'backgroundColor': '#E5E5E5', 'borderRadius': '5px', 'border': '1px solid #888888',
    'color': 'black', 'fontFamily': 'Courier New', 'width': '100%', 'textAlign': 'left' as const, 'padding': '5px'
};

const h4Style = {
    marginBlockEnd: '0',
    marginBottom: '0',
}

interface IProps {
    open: boolean;
    onClose: () => void;
    type?: DialogType;
    object?: DialogObject;
    coverageInfoTaskName?: string;
}

const typeTitleLookup = {
    'monitoringError': 'Monitoring error of',
    'warnings': 'Warning(s) of',
    'statistic': 'Statistic',
    'coverage': 'Coverage of',
    'componentWarnings': 'Component warning(s) of',
    'componentFunctionalConstraintErrors': 'Component functional constraint error(s) of'
};

class Dialog extends React.Component<IProps> {
    renderStatistic(): React.ReactNode {
        if (!(this.props.object instanceof Statistic)) {
            throw new Error('Not supported');
        }

        return <StatisticView statistic={this.props.object}/>
    }

    renderContentMonitoringOrFunctionalConstraintError(): React.ReactNode {
        const errors: {context: string[], umlBase64: string, description: string, name?: string}[] = [];
        if (this.props.type === 'monitoringError' && this.props.object instanceof Connection) {
            errors.push({
                description: this.props.object.monitoringError.errorDescription,
                context: this.props.object.monitoringError.contextError,
                umlBase64: this.props.object.monitoringError.umlBase64,
            });
        } else if (this.props.type === 'componentFunctionalConstraintErrors' && this.props.object instanceof Trace) {
            this.props.object.componentFunctionalConstraintErrors?.forEach((e) => {
                errors.push({
                    description: e.error.errorDescription,
                    context: e.error.contextError,
                    umlBase64: e.error.umlBase64,
                    name: e.constraintName,
                })
            });
        } else {
            throw new Error('Not supported')
        }

        return (
            <div>
            {errors.map((error, i) => {
                return (
                    <div key={i}>
                        {error.name && <h4 style={{...h4Style, marginBlockStart: '0'}}>Constraint error on: {error.name}</h4>}
                        <h4 style={{...h4Style, marginBlockStart: '0'}}>Error description</h4>
                        <div style={styleCode}>{error.description}</div>
                        <h4 style={h4Style}>Error context</h4>
                        <div style={styleCode}>{error.context.map((l, i) => <div key={i}>{l}</div>)}</div>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <img src={`data:image/png;base64, ${error.umlBase64}`} alt=""/>
                        </div>
                    </div>
                )
            })}
            </div>
        );
    }

    renderContentWarnings(): React.ReactNode {
        let warnings;
        if (this.props.object instanceof Connection) {
            warnings = this.props.object.warnings;
        } else if (this.props.object instanceof Trace && this.props.object.componentWarnings) {
            warnings = this.props.object.componentWarnings;
        } else {
            throw new Error('Unsupported');
        }

        return (
            <div>
            {warnings.map((constraintError, i) => {
                return (
                    <div key={i}>
                        <h4 style={{...h4Style, marginBlockStart: i !== 0 ? '10' : '0'}}>Constraint warning on: {constraintError.ruleName}</h4>
                        <div style={styleCode}>{constraintError.errorMessage}</div>
                        <div style={{width: '100%', textAlign: 'center'}}>
                            <img src={`data:image/png;base64, ${constraintError.umlBase64}`} alt=""/>
                        </div>
                    </div>
                )
            })}
            </div>
        )
    }

    renderContentCoverage(): React.ReactNode {
        if (this.props.object === null) {
            throw new Error('Not supported');
        }

        let coverage: CoverageInfo ;
        if (this.props.object instanceof Task && this.props.coverageInfoTaskName) {
            coverage = this.props.object.getCoverageInfo(this.props.coverageInfoTaskName)
        } else {
            return;
        }

        return (
            <div>
                <h4 style={{...h4Style, marginBlockStart: '0'}}>Transtions  ({coverage.transitionCoveragePercentage}%)</h4>
                {Object.entries(coverage.transitionCoverage).map((value, i) => (
                    <div key={i}>{value[0]} <b>:</b> {value[1]} time(s)</div>
                ))}

                <h4 style={{...h4Style}}>States  ({coverage.stateCoveragePercentage}%)</h4>
                {Object.entries(coverage.stateCoverage).map((value, i) => (
                    <div key={i}>{value[0]} <b>:</b> {value[1]} time(s)</div>
                ))}
            </div>
        )
    }

    render(): React.ReactNode {
        const style = {height:  this.props.type === 'statistic' ? '95vh' : 'auto'};
        return (
            <MaterialDialog fullWidth maxWidth={'md'} open={this.props.open} onClose={() => this.props.onClose()} PaperProps={{style}}>
                <DialogTitle>
                    {this.props.type && typeTitleLookup[this.props.type]} {this.props.object && (this.props.coverageInfoTaskName || this.props.object.name)}
                </DialogTitle>
                <DialogContent>
                    {(this.props.type === 'monitoringError' || this.props.type === 'componentFunctionalConstraintErrors') &&
                        this.renderContentMonitoringOrFunctionalConstraintError()}
                    {(this.props.type === 'warnings' || this.props.type === 'componentWarnings') &&
                        this.renderContentWarnings()}
                    {this.props.type === 'coverage' && this.renderContentCoverage()}
                    {this.props.type === 'statistic' && this.renderStatistic()}
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.onClose()} color="primary">Close</Button>
                </DialogActions>
            </MaterialDialog>
        )
    }
}

export default Dialog;
