/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import CancelIcon from '@material-ui/icons/Cancel';
import { Status } from '../types';

interface IProps {
    size?: string;
    status: Status;
}

class StatusIcon extends React.Component<IProps> {
    render(): React.ReactNode {
        const lookup = {
            failed: <CancelIcon style={{color: 'red', height: this.props.size, width: this.props.size}}/>,
            warning: <ErrorIcon style={{color: '#ffea00', height: this.props.size, width: this.props.size}}/>,
            successful: <CheckCircleIcon style={{color: 'green', height: this.props.size, width: this.props.size}}/>,
        }

        return lookup[this.props.status]
    }
}

export default StatusIcon;
