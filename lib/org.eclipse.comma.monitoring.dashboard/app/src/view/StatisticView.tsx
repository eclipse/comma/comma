/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Statistic from '../model/Statistic';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Plot from 'react-plotly.js';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { StatisticData } from '../types';

interface IProps {
    statistic: Statistic;
}

interface IState {
    selected: 'Overview' | 'Stats' | string;
    contentSize?: {height: number; width: number};
    popperOpen: boolean;
}

 class TasksView extends React.Component<IProps, IState> {
    private contentRef: React.RefObject<HTMLDivElement>;
    private anchorRef: React.RefObject<HTMLDivElement>;

    constructor(props: IProps) {
        super(props);
        this.state = {selected: 'Overview', popperOpen: false};
        this.contentRef = React.createRef();
        this.anchorRef = React.createRef();
    }

    componentDidMount(): void {
        this.measureContent();
        window.addEventListener('resize', this.measureContent.bind(this))
    }

    measureContent(): void {
        if (this.contentRef.current) {
            this.setState({contentSize: {height: this.contentRef.current.offsetHeight, width: this.contentRef.current.offsetWidth}});
        }
    }

    renderSelectors(): React.ReactNode {
        const options = ['Overview', ...this.props.statistic.data.map((d) => d.name), 'Stats'];
        return (
            <Grid container direction="column" alignItems="center" style={{height: '40px'}}>
                <Grid item xs={12}>
                    <ButtonGroup variant="contained" color="primary" ref={this.anchorRef} aria-label="split button">
                        <Button onClick={() => this.setState({popperOpen: !this.state.popperOpen})}>{this.state.selected}</Button>
                        <Button
                            color="primary"
                            size="small"
                            aria-controls={this.state.popperOpen ? 'split-button-menu' : undefined}
                            aria-expanded={this.state.popperOpen ? 'true' : undefined}
                            onClick={() => this.setState({popperOpen: !this.state.popperOpen})}
                        >
                            <ArrowDropDownIcon />
                        </Button>
                    </ButtonGroup>
                    <Popper open={this.state.popperOpen} anchorEl={this.anchorRef.current} role={undefined} transition disablePortal style={{zIndex: 99999}}>
                    {({ TransitionProps, placement }) => (
                        <Grow {...TransitionProps} style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}>
                            <Paper>
                                <ClickAwayListener onClickAway={() => this.setState({popperOpen: false})}>
                                    <MenuList id="split-button-menu">
                                        {options.map((option, index) => (
                                            <MenuItem
                                                key={option}
                                                selected={option === this.state.selected}
                                                onClick={() => this.setState({selected: options[index], popperOpen: false})}
                                            >
                                                {option}
                                            </MenuItem>
                                        ))}
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                    </Popper>
                </Grid>
            </Grid>
        );
    }

    renderOverview(): React.ReactNode {
        if (!this.state.contentSize) return null;
        return (
            <Plot
                data={[{
                    values: this.props.statistic.data.map((d) => d.values.length),
                    labels: this.props.statistic.data.map((d) => d.name),
                    type: 'pie',
                }]}
                config={{displayModeBar: false}}
                layout={{...this.state.contentSize, title: 'Constraint invocations'}}
            />
        )
    }

    renderData(): React.ReactNode {
        const stat = this.props.statistic.data.find((d) => d.name === this.state.selected);
        if (!stat || stat.values.length == 0) return null;

        if (isNaN(stat.values[0])) {
            return this.renderDataTable(stat);
        } else {
            return this.renderDataHistogram(stat);
        }
    }

    renderDataTable(stat: StatisticData): React.ReactNode {
        if (!stat.valuesPerVariable) return;
        const variables = Object.keys(stat.valuesPerVariable);
        const instances = stat.values.length / variables.length
        const styleCell = {paddingRight: '0px', wordBreak: 'break-all' as const};
        return (
            <TableContainer>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell style={styleCell}>Instance</TableCell>
                            <TableCell style={styleCell}>Variable</TableCell>
                            <TableCell style={styleCell}>Value</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Array(instances * variables.length).fill(0).map((_, i) => (
                            <TableRow key={i}>
                                {i % variables.length === 0 && <TableCell rowSpan={variables.length} style={styleCell}>{Math.floor(i / variables.length) + 1}</TableCell>}
                                <TableCell style={styleCell}>
                                    {variables[(i % variables.length)]}
                                </TableCell>
                                <TableCell style={styleCell}>
                                    {stat.valuesPerVariable && stat.valuesPerVariable[variables[(i % variables.length)]][Math.floor(i / variables.length)]}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }

    renderDataHistogram(stat: StatisticData): React.ReactNode {
        if (!this.state.contentSize) return null;

        const rangeDivider = stat.valuesPerVariable ? Object.keys(stat.valuesPerVariable).length : 1;

        const range = [0.5, (stat.values.length / rangeDivider) + 0.5];
        const line = {
            x: range,
            mode: 'text+lines',
            line: {color: 'black'},
            textposition:'top right',
        };

        // eslint-disable-next-line
        const data: any[] = [{...line, text: ['Mean'], y: [stat.mean, stat.mean], hoverinfo: 'y', showlegend: false}];

        if (stat.valuesPerVariable) { // kind is 'data'
            Object.entries(stat.valuesPerVariable).forEach((entry) => {
                data.splice(0, 0, {y: entry[1], x: entry[1].map((v, i) => i + 1), type: 'bar', hoverinfo: 'y', name: entry[0]});
            });
        } else {
            data.splice(0, 0, {y: stat.values, x: stat.values.map((v, i) => i + 1), type: 'bar', hoverinfo: 'y'});
        }

        if (stat.upperBound !== null) {
            data.push({...line, text: ['Upper bound'], y: [stat.upperBound, stat.upperBound], hoverinfo: 'y', showlegend: false});
        }
        if (stat.lowerBound !== null) {
            data.push({...line, text: ['Lower bound'], y: [stat.lowerBound, stat.lowerBound], hoverinfo: 'y', showlegend: false});
        }

        return (
            <Plot
                data={data}
                config={{displayModeBar: false}}
                layout={{
                    showlegend: this.props.statistic.kind === 'data',
                    ...this.state.contentSize,
                    title: `${stat.name} interval values histogram`,
                    yaxis: {title: this.props.statistic.kind === 'time' ? 'Values in milliseconds' : ''},
                    xaxis: {title: 'Number of instances', range}
                }}
            />
        )
    }

    renderStats(): React.ReactNode {
        const stat = this.props.statistic.data;
        const rows = [
            ['Times invoked', ...stat.map((s) => s.values.length)],
            ['Mean value', ...stat.map((s) => s.mean)],
            ['Variance', ...stat.map((s) => s.variance)],
            ['Std deviation', ...stat.map((s) => s.standardDeviation)],
            ['Min', ...stat.map((s) => s.min)],
            this.props.statistic.kind === 'time' ? ['Lower bound', ...stat.map((s) => s.lowerBound != null ? s.lowerBound : '-')] : [],
            ['Median', ...stat.map((s) => s.median)],
            ['Max', ...stat.map((s) => s.max)],
            this.props.statistic.kind === 'time' ? ['Upper bound', ...stat.map((s) => s.upperBound != null ? s.upperBound : '-')] : [],
        ];

        const styleName = {
            paddingTop: '5px',
            paddingBottom: '5px',
        };

        return (
            <TableContainer>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            {['', ...stat.map((d) => d.name)].map((column, i) => (<TableCell key={i}>{column}</TableCell>))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((columns, i) => (
                            <TableRow key={i}>
                                {columns.map((c, ii) => (
                                    <TableCell style={styleName} key={ii}>{typeof c === 'string' ? c : (isNaN(c) ? '-' : c)}</TableCell>
                                ))}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }

    render(): React.ReactNode {
        const isOverviewOrStat = this.state.selected === 'Stats' || this.state.selected === 'Overview';
        return (
             <div style={{display: 'flex', width: '100%', flexDirection: 'column', alignItems: 'center', height: '100%'}}>
                {this.renderSelectors()}
                <div ref={this.contentRef} style={{width: '100%', height: '100%', overflowY: this.state.selected === 'Stats' ? 'auto' : 'hidden'}}>
                    {this.state.selected === 'Overview' && this.renderOverview()}
                    {!isOverviewOrStat && this.renderData()}
                    {this.state.selected === 'Stats' && this.renderStats()}
                </div>
            </div>
        )
    }
 }

 export default TasksView;
