/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Connection from '../model/Connection';
import Trace from '../model/Trace';
import { DialogType, Status } from '../types';
import Box from './Box';
import StatusIcon from './StatusIcon';

interface IProps {
    selectedTrace: Trace | null;
    flex: number;
    onSelect: (object: Connection | Trace, type: DialogType) => void;
}

class TraceView extends React.Component<IProps> {
    renderStatus(status: Status): React.ReactNode {
        return (
            <div style={{display: 'flex'}}>
                Status: <StatusIcon size='20px' status={status}/>
                <label style={{marginLeft: '5px'}}>({status})</label>
            </div>
        );
    }

    renderTrace(): React.ReactNode {
        if (!this.props.selectedTrace) return;

        const details = (c: Connection | Trace, type: DialogType) => {
            return (
                <label>
                    (<label
                        onClick={() => this.props.onSelect(c, type)}
                        style={{color: '#007bff', textDecoration: 'underline', cursor: 'pointer'}}>
                        details
                    </label>)
                </label>
            );
        }

        const trace = this.props.selectedTrace;
        return (
            <div>
                {trace.traceError &&
                    <div>
                        <div><b>Trace format error</b></div>
                        <div>{trace.traceErrorMessage}</div>
                    </div>
                }
                {(trace.componentWarnings && trace.componentFunctionalConstraintErrors)  &&
                    <div style={{ marginTop: trace.traceError ? '20px' : '0px'}}>
                        <div><b>Component warnings</b></div>
                        {this.renderStatus(trace.getStatusComponentWarnings())}
                        <label style={{marginRight: '5px'}}>Constraint warnings: {trace.componentWarnings.length}</label>
                        {trace.componentWarnings.length > 0 && details(trace, 'componentWarnings')}

                        <div style={{marginTop: '20px'}}><b>Component functional constraint errors</b></div>
                        {this.renderStatus(trace.getStatusComponentFunctionalConstraintErrors())}
                        <label style={{marginRight: '5px'}}>Functional constraint errors: {trace.componentFunctionalConstraintErrors.length}</label>
                        {trace.componentFunctionalConstraintErrors.length > 0 && details(trace, 'componentFunctionalConstraintErrors')}
                    </div>
                }
                {trace.connections.map((c, i) => {
                    return (
                        <div key={i} style={{ marginTop: i !== 0 || trace?.kind === 'component' || trace.traceError ? '20px' : '0px'}}>
                            <div><b>Connection: {c.name}</b></div>
                            { c.kind === 'component' &&
                                <div>
                                    <div>Component instance: {c.instanceName}</div>
                                    <div>Port: {c.portName}</div>
                                </div>
                            }
                            {this.renderStatus(c.getStatus())}
                            {c.warnings.length !== 0 && <div>
                                Constraint warnings: {c.warnings.length} {details(c, 'warnings')}
                            </div>}
                            {c.monitoringError && <div>
                                Monitoring error: {c.monitoringError.errorDescription} {details(c, 'monitoringError')}
                            </div>}
                        </div>
                    );
                })}
            </div>
        )
    }

    render(): React.ReactNode {
        return (
            <Box title="Trace" flex={this.props.flex}>
                {this.props.selectedTrace  ? this.renderTrace() : <p>Select a trace</p>}
            </Box>
        );
    }
}

export default TraceView;
