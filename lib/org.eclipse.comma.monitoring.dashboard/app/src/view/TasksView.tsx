/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Box from './Box';
import StatusTable from './StatusTable';
import React from 'react';
import Task from '../model/Task';

interface IProps {
    tasks: Task[];
    selectedTask: Task | null;
    flex: number;
    onSelectTask: (task: Task) => void;
}

class TasksView extends React.Component<IProps> {
    render(): React.ReactNode {
        const entries = this.props.tasks.map(t => {
            return {
                status: t.getStatus(),
                name: `${t.name} (${t.kind} ${t.modelName})`,
            }
        });

        let selectedEntry = null;
        if (this.props.selectedTask) {
            selectedEntry = entries[this.props.tasks.indexOf(this.props.selectedTask)]
        }

        return (
            <Box title="Tasks" flex={this.props.flex}>
                <div>
                    <StatusTable
                        entries={entries}
                        selectedEntry={selectedEntry}
                        onClick={(row) => this.props.onSelectTask(this.props.tasks[entries.indexOf(row)])}
                    />
                </div>
            </Box>
        );
    }
}

export default TasksView;
