/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';

const styleRoot = {
    backgroundColor: "white",
    boxShadow: "0px 0px 1px 0px rgba(0,0,0,0.20)",
    borderRadius: "5px",
    display: "flex",
    flexDirection: "column" as const,
    height: "0%"
};

const styleTitle = {
    backgroundColor: "#C0C0C0",
    color: "white",
    boxShadow: "0px 2px 2px 0px rgba(0,0,0,0.33)",
    paddingLeft: "10px",
    paddingTop: '3px',
    paddingBottom: '3px',
    borderRadius: "2px",
    fontSize: 'x-large',
}

const styleContainer = {
    padding: '5px'
}

interface IProps {
    title: string;
    flex: number;
}

class Box extends React.Component<IProps> {
    render(): React.ReactNode {
        return (
            <div style={{...styleRoot, flex: this.props.flex}}>
                <div style={styleTitle}>
                    {this.props.title}
                </div>
                <div style={styleContainer}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Box;
