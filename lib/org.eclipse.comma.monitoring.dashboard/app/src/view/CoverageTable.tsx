/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Paper from '@material-ui/core/Paper';
import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { CoverageSummary } from '../types';

const styleName = {
    paddingRight: '0px',
    wordBreak: 'break-all' as const,
};

const styleStateTransition = {
    paddingRight: '5px',
    paddingLeft: '0px',
    width: 'auto'
};

interface IProps {
    name: string;
    coverageSummaries: CoverageSummary[];
    onSelectCoverageSummary: (coverageSummary: CoverageSummary) => void;
}

class CoverageTable extends React.Component<IProps> {
    render(): React.ReactNode {
        return (
            <TableContainer component={Paper}>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell style={styleName}>{this.props.name}</TableCell>
                            <TableCell style={styleStateTransition}>State</TableCell>
                            <TableCell style={styleStateTransition}>Transition</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {this.props.coverageSummaries.map((c, i) => (
                        <TableRow key={i}>
                            <TableCell style={styleName}>
                                <label
                                    onClick={() => this.props.onSelectCoverageSummary(c)}
                                    style={{color: '#007bff', textDecoration: 'underline', cursor: 'pointer'}}>
                                    {c.name}
                                </label>
                            </TableCell>
                            <TableCell style={styleStateTransition}>{c.statePercentage}%</TableCell>
                            <TableCell style={styleStateTransition}>{c.transitionPercentage}%</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
}

export default CoverageTable;
