/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Paper from '@material-ui/core/Paper';
import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import StatusIcon from './StatusIcon';
import { StatusEntry } from '../types';

const colorSelect = '#ececec';
const colorHighlight = '#dfdfdf';

const statusCellStyle = {
    width: '1px'
}

interface IProps {
    entries: StatusEntry[];
    selectedEntry: StatusEntry | null;
    onClick: (entry: StatusEntry) => void;
}

interface IState {
    highlight: number | null;
}

class StatusTable extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {highlight: null};
    }

    render(): React.ReactNode {
        return (
            <TableContainer component={Paper}>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell style={statusCellStyle}></TableCell>
                            <TableCell>Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {this.props.entries.map((entry, i: number) => (
                        <TableRow
                            key={i}
                            onClick={() => this.props.onClick(entry)}
                            onMouseOver={() => this.setState({highlight: i})}
                            onMouseLeave={() => this.setState({highlight: null})}
                            style={{backgroundColor: i === this.state.highlight ? colorHighlight : this.props.selectedEntry === entry ? colorSelect : 'white'}}
                        >
                            <TableCell style={statusCellStyle}><StatusIcon status={entry.status}/></TableCell>
                            <TableCell style={{wordBreak: 'break-all'}}>{entry.name}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
}

export default StatusTable;
