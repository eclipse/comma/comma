/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Header from './view/Header';
import TraceView from './view/TraceView';
import TasksView from './view/TasksView';
import TaskView from './view/TaskView';
import Task from './model/Task';
import Dialog from './view/Dialog';
import CssBaseline from '@material-ui/core/CssBaseline';
import { DialogType, JsonData, DialogObject } from './types';
import Trace from './model/Trace';

const space = <div style={{width: '10px'}}/>

// eslint-disable-next-line
interface IProps {}

interface IState {
    tasks: Task[];
    dialog: {open: boolean, object?: DialogObject, coverageInfoTaskName?: string, type?: DialogType};
    selectedTask: Task | null;
    selectedTrace: Trace | null;
}

class App extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = { tasks: [], dialog: {open: false}, selectedTask: null, selectedTrace: null};
    }

    componentDidMount(): void {
        let tasks : JsonData[];
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // eslint-disable-next-line
            tasks = require('./testdata.js').default;
        } else {
            // eslint-disable-next-line
            // @ts-ignore
            tasks = window.monitoringTasks;
        }

        this.setState({tasks: tasks.map(t => new Task(t))});
    }

    render(): React.ReactNode {
        return (
            <div className="App">
                <CssBaseline />
                <Header/>
                <Dialog
                    object={this.state.dialog.object}
                    coverageInfoTaskName={this.state.dialog.coverageInfoTaskName}
                    type={this.state.dialog.type}
                    open={this.state.dialog.open}
                    onClose={() => this.setState({dialog: {...this.state.dialog, open: false}})}
                />
                <div style={{display: 'flex', justifyContent: 'space-between', padding: '20px'}}>
                    <TasksView
                        flex={1}
                        selectedTask={this.state.selectedTask}
                        tasks={this.state.tasks}
                        onSelectTask={(selectedTask) => this.setState({selectedTask, selectedTrace: null})}
                    />
                    {space}
                    <TaskView
                        flex={1}
                        selectedTrace={this.state.selectedTrace}
                        selectedTask={this.state.selectedTask}
                        onSelectTrace={(selectedTrace) => this.setState({selectedTrace})}
                        onSelectStatistic={(statistic) => this.setState({dialog: {object: statistic, type: 'statistic', open: true}})}
                        onCoverageDetails={(task, coverageInfoTaskName) => this.setState({dialog: {object: task, coverageInfoTaskName, type: 'coverage', open: true}})}
                    />
                    {space}
                    <TraceView
                        selectedTrace={this.state.selectedTrace}
                        onSelect={(object, type) => this.setState({dialog: {object, type, open: true}})}
                        flex={2}
                    />
                </div>
            </div>
        );
    }
}

export default App;
