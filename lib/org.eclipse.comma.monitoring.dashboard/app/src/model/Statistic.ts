/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import { JsonData, StatisticKind, StatisticData } from '../types';

const calculator = {
    mean: (arr: number[]): number => {
        return arr.reduce((acc,v) => acc + v) / arr.length;
    },
    standardDeviation: (arr: number[]): number => {
        return Math.sqrt(calculator.variance(arr));
    },
    variance: (arr: number[]): number => {
        const mean = calculator.mean(arr);
		return calculator.mean(arr.map((num) => Math.pow(num - mean, 2)));
    },
    median: (arr: number[]): number => {
        const mid = Math.floor(arr.length / 2), nums = [...arr].sort((a, b) => a - b);
        return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
    },
};

class Statistic {
    public kind: StatisticKind;
    public name: string;
    public data: StatisticData[];

    constructor(data: JsonData) {
        this.kind = data.file.startsWith("statisticsData") ? 'data' : 'time';
        this.name = data.file.replace(".statistics", "").replace("statisticsData", "").replace("statisticsTime", "") + ` (${this.kind})`;

        const tempData: {[s: string]: StatisticData} = {};
        for (const line of data.content) {
            const [fullName, value, lowerBound, upperBound] = line.split(',');
            const name = this.kind === 'data' ?  fullName.split("_")[0] : fullName;
            const variable = this.kind === 'data' ? fullName.split(/_(.+)/)[1] : null;

            if (!(name in tempData)) {
                tempData[name] = {
                    name, mean: 0, min: 0, max: 0, median: 0, variance: 0, standardDeviation: 0, values: [],
                    valuesPerVariable: this.kind === 'data' ? {} : null,
                    upperBound: upperBound != null && parseFloat(upperBound) != -1 ? parseFloat(upperBound) : null,
                    lowerBound: lowerBound != null && parseFloat(lowerBound) != -1 ? parseFloat(lowerBound) : null,
                };
            }

            tempData[name].values.push(parseFloat(value));
            const valuesPerVariable = tempData[name].valuesPerVariable;
            if (valuesPerVariable) {
                if (!(variable in valuesPerVariable)) valuesPerVariable[variable] = [];
                valuesPerVariable[variable].push(value);
            }
        }

        // Calculate various values
        Object.values(tempData).forEach((d) => {
            d.mean = calculator.mean(d.values);
            d.min = Math.min(...d.values);
            d.max = Math.max(...d.values);
            d.median = calculator.median(d.values);
            d.variance = calculator.variance(d.values);
            d.standardDeviation = calculator.standardDeviation(d.values);
        });

        this.data = Object.values(tempData);
    }
}

export default Statistic;