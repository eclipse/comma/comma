/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import { Warning, JsonData, Kind, MonitoringError, Status } from '../types';

class Connection {
    public name: string;
    public kind: Kind;
    public portName: string | null;
    public instanceName: string | null;
    public warnings: Warning[];
    public monitoringError: MonitoringError;

    constructor(data: JsonData, kind: Kind, portName: string | null, instanceName: string | null) {
        this.name = data.connectionName;
        this.kind = kind;
        this.portName = portName;
        this.instanceName = instanceName;
        this.warnings = data.warnings;
        this.monitoringError = data.monitoringErrors.length === 1 ? data.monitoringErrors[0] : null;
    }

    public getStatus(): Status {
        if (this.monitoringError != null) return 'failed';
        if (this.warnings.length !== 0) return 'warning';
        else return 'successful';
    }
}

export default Connection;