/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Trace from './Trace';
import Statistic from './Statistic';
import { CoverageInfo, CoverageSummary, JsonData, Kind, Status } from '../types';

class Task {
    public name: string;
    public kind: Kind;
    public modelName: string;
    public traces: Trace[];
    public statistics: Statistic[];
    private coverageInfos: CoverageInfo[];

    constructor(data: JsonData) {
        this.name = data.taskName;
        this.kind = data.taskKind;
        this.modelName = data.modelName;
        this.traces = data.traceResults.map((t: JsonData) => new Trace(t, this.kind));
        this.statistics = data.statistics?.map((d: JsonData) => new Statistic(d)) || [];
        this.coverageInfos = data.coverageInfo;
    }

    getCoverageInfo(interfaceName: string) : CoverageInfo {
        const coverageInfo = this.coverageInfos.find((c) => c.interfaceName === interfaceName);
        if (!coverageInfo) throw new Error(`No coverage info for ${interfaceName}`);
        return coverageInfo;
    }

    getCoverageSummaries(): CoverageSummary[] {
        return this.coverageInfos.map(info => {
            return {
                name: info.interfaceName,
                statePercentage: info.stateCoveragePercentage,
                transitionPercentage: info.transitionCoveragePercentage,
            };
        });
    }

    getStatus(): Status {
        const statuses = this.traces.map(t => t.getStatus());
        if (statuses.includes('failed')) return 'failed';
        else if (statuses.includes('warning')) return 'warning';
        else return 'successful';
    }
}

export default Task;