/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Connection from './Connection';
import { Warning, JsonData, Kind, Status, FunctionalConstraintError} from '../types';

class Trace {
    public name: string;
    public file: string;
    public kind: Kind;
    public connections: Connection[];
    public componentWarnings?: Warning[];
    public componentFunctionalConstraintErrors?: FunctionalConstraintError[];
    public traceError: boolean;
    public traceErrorMessage: string;

    constructor(data: JsonData, kind: Kind) {
        this.file = data.traceFile;
        this.traceError = data.traceError;
        this.traceErrorMessage = data.traceErrorMessage;
        this.name = this.file.split('\\').slice(-1)[0];
        this.kind = kind;
        this.connections = [];

        if (this.kind === 'interface') {
            data.monitorResults.forEach((d: JsonData) => {
                this.connections.push(new Connection(d, this.kind, null, null));
            });
        } else {
            this.componentWarnings = [];
            this.componentFunctionalConstraintErrors = [];

            for (const monitorResult of data.monitorResults) {
                this.componentWarnings.push(...monitorResult.warnings);
                this.componentFunctionalConstraintErrors.push(...monitorResult.functionalConstraintResults.filter((f: JsonData) => f.error));

                const instanceName = monitorResult.componentInstanceName;
                const portResults: JsonData = monitorResult.portResults;
                for (const [portName, portData] of Object.entries(portResults)) {
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    portData.forEach((p: any) => {
                        this.connections.push(new Connection(p, this.kind, portName, instanceName));
                    });
                    
                }
            }
        }
    }

    getStatusComponentWarnings(): Status {
        if (!this.componentWarnings || this.componentWarnings.length === 0) return 'successful';
        else return 'warning';
    }

    getStatusComponentFunctionalConstraintErrors(): Status {
        if (!this.componentFunctionalConstraintErrors || this.componentFunctionalConstraintErrors.length === 0) return 'successful';
        else return 'failed';
    }

    getStatus(): Status {
        const statuses = this.connections.map(c => c.getStatus());
        statuses.push(this.getStatusComponentWarnings());
        statuses.push(this.getStatusComponentFunctionalConstraintErrors());
        statuses.push(this.traceError ? 'failed' : 'successful');
        if (statuses.includes('failed')) return 'failed';
        else if (statuses.includes('warning')) return 'warning';
        else return 'successful';
    }
}

export default Trace;