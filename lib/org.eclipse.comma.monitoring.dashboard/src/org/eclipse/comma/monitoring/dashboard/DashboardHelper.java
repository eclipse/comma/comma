/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.dashboard;

import java.io.IOException;
import java.io.InputStream;

public class DashboardHelper {
	public static byte[] getHTML() {
		InputStream in = DashboardHelper.class.getClassLoader().getResourceAsStream("monitoring_dashboard.html");
		if (in != null) {
			try {
				return in.readAllBytes();
			} catch (IOException e) {
			}
		}
		
		return null;
	}
}
