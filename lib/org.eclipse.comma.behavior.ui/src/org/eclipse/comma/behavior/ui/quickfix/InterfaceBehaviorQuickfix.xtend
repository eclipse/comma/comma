/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.ui.quickfix

import org.eclipse.comma.behavior.behavior.StateMachine
import org.eclipse.comma.behavior.validation.StateMachineValidator
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue

class InterfaceBehaviorQuickfix extends StateMachineQuickfix {

	@Fix(StateMachineValidator.STATEMACHINE_MISSING_INITIAL_STATE)
	def addInitialState(Issue issue, IssueResolutionAcceptor acceptor) {

		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				val sm = element as StateMachine
				var offset = if (!sm.states.empty)
						NodeModelUtils.findActualNodeFor(sm.states.get(0)).offset - 1
					else
						NodeModelUtils.findActualNodeFor(sm).endOffset - 1
				
				val initialState = 
					'''
					
						initial state NewState {
							transition
								next state: NewState«""»
						}

					'''
				context.xtextDocument.replace(offset, 0, initialState)
			}
		}
		acceptor.accept(issue, 'Add initial state', 'Add initial state.', 'upcase.png', modification)
	}
}