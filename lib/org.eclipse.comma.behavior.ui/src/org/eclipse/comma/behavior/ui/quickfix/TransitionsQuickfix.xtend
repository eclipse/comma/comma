/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.ui.quickfix

import org.eclipse.comma.actions.ui.quickfix.ActionsQuickfixProvider
import org.eclipse.comma.behavior.validation.BehaviorValidator
import org.eclipse.comma.behavior.validation.StateMachineValidator
import org.eclipse.comma.behavior.validation.TimeConstraintsValidator
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue

class TransitionsQuickfix extends ActionsQuickfixProvider {

	@Fix(BehaviorValidator.STATEMACHINE_DUPLICATE_INTERFACE)
	@Fix(BehaviorValidator.STATEMACHINE_UNUSED_INTERFACE)
	@Fix(BehaviorValidator.STATEMACHINE_DUPLICATE_STATE)
	@Fix(BehaviorValidator.STATEMACHINE_DUPLICATE_VAR)
	@Fix(BehaviorValidator.STATEMACHINE_UNITIALIZED_VAR)
	@Fix(BehaviorValidator.STATEMACHINE_UNUSED_VAR)
	@Fix(StateMachineValidator.STATEMACHINE_DUPLICATE_INITIAL_STATE)
	@Fix(TimeConstraintsValidator.TIME_CONSTRAINT_DUPLICATE)
	def removeElement(Issue issue, IssueResolutionAcceptor acceptor) {
		val element = issue.data.get(0)
		acceptor.accept(issue, 'Remove ' + element, 'Remove ' + element, 'upcase.png') [ context |
			context.xtextDocument.replace(issue.offset, issue.length, "")
		]
	}

}
