/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.ui.quickfix

import org.eclipse.comma.behavior.behavior.Clause
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.behavior.validation.BehaviorValidator
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue

class StateMachineQuickfix extends TimeConstraintsQuickfix {

	@Fix(BehaviorValidator.CLAUSE_MISSING_NEXT_STATE)
	def addNextState(Issue issue, IssueResolutionAcceptor acceptor) {
		

		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {				
				if (element instanceof Clause) {
					val actions = NodeModelUtils.findActualNodeFor(element.actions)
					context.xtextDocument.replace(actions.offset + actions.length + 1, 0,
						"next state: " + (element.eContainer.eContainer as State).name)
				}
			}
		}
		acceptor.accept(issue, 'Add next state', 'Add next state.', 'upcase.png', modification)
	}
	

}
