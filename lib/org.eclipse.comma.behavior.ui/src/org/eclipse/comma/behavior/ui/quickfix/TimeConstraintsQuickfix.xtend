/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.ui.quickfix

import org.eclipse.comma.behavior.behavior.ConditionedAbsenceOfEvent
import org.eclipse.comma.behavior.validation.TimeConstraintsValidator
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue

class TimeConstraintsQuickfix extends TransitionsQuickfix {

@Fix(TimeConstraintsValidator.GROUP_CONSTRAINT_MISING_END)
	def changeToEndBoundary(Issue issue, IssueResolutionAcceptor acceptor) {
		
		val modification = new ISemanticModification() {			
			override apply(EObject element, IModificationContext context) throws Exception {
				val expr = NodeModelUtils.findActualNodeFor((element as ConditionedAbsenceOfEvent).interval.begin).text
				context.xtextDocument.replace(issue.offset, issue.length, "[ .. "+ expr +" ms ]")				
			}			
		}
		acceptor.accept(issue, 'Change to end boundary', 'Change to end boundary', 'upcase.png', modification)
	}
}
