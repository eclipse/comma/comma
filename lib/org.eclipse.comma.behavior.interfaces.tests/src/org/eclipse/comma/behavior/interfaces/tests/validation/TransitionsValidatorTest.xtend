/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.comma.behavior.validation.TransitionsValidator
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)

class TransitionsValidatorTest {
	@Inject extension ParseHelper<InterfaceDefinition> parseHelper
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	@Test
	def missingNextStateTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {
					transition do:
						newPerson(*)
				}
			}
			''', set)
		assertError(result, TRANSITION, TransitionsValidator.CLAUSE_MISSING_NEXT_STATE, "Missing next state")
	}

	@Test
	def wrongNumberOrParamsInTriggerTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: c
						do: reply(*)
					next state: Initial
				}
			}
			''', set)
		assertError(result, TRIGGERED_TRANSITION, null, "Wrong number of parameters in the trigger.")
	}

	@Test
	def wrongTypeOfParamInTriggerTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: c(real i)
						do: reply(*)
					next state: Initial
				}
			}
			''', set)
		assertError(result, TRIGGERED_TRANSITION, null, "The type of parameter does not match the type in the trigger signature.")
	}

	@Test
	def duplicateParamsInTriggerTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int j, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}
			''', set)
		assertError(result, VARIABLE, null, "Duplicate parameter name")
	}

	@Test
	def erroneousGuardTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l) guard: 1
						do: reply(*, *, *)
					next state: Initial
				}
			}
			''', set)
		assertError(result, TRANSITION, null, "Guard expression has to be of type boolean.")
	}

	@Test
	def duplicateClausesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
						next state: Initial
					OR
						do: reply(*, *, *)
						next state: Initial
				}
			}
			''', set)
		assertWarning(result, TRANSITION, null, "Duplicate clause")
	}

	/*
	 * Tests for validation rules in Interface language
	 */

	@Test
	def wrongTriggerTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {
					transition trigger: newPerson(Person p) next state: S
				}
			}
			''', set)
		assertError(result, TRANSITION, null, "Trigger must be a command or a signal")
	}

	@Test
	def missingReplyTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {
					transition trigger: c(int i) next state: S
				}
			}
			''', set)
		assertError(result, CLAUSE, null, "The body of this transition is missing a reply.")
	}

	@Test
	def pathWithoutReplyTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA
			machine M1 {
				initial state S {
					transition trigger: c(int i)
						do: if true then reply(*) else i := 1 fi
					next state: S
				}
			}
			''', set)
		assertError(result, CLAUSE, null, "The body of this transition has a path that is missing a reply.")
	}

	@Test
	def silentTransitionTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA
			variables
			int i
			machine M1 {
				initial state S {
					transition do: if true then i := 1 fi
					next state: S
				}
			}
			''', set)
		assertError(result, NON_TRIGGERED_TRANSITION, null, "The clause has an execution path without any event")
	}
}
