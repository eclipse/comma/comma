/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.actions.validation.ActionsValidator
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)

class InOutParametersValidatorTest {
	@Inject extension ParseHelper<InterfaceDefinition> parseHelper

	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider
	var ImportedInterfaces interfaces = new ImportedInterfaces

	@Test
	def checkCorrectInterface(){
		val set = resourceSetProvider.get()
		interfaces.importInOutTestSignature(set)
		val model = parse('''
			import 'iA.signature'

			interface iA

			machine iABehavior {
				initial state S1 {
					transition trigger: C1(int i, int j, int k)
					do: reply(*,*)
					next state: S1
				}
			}
		''', set)
		assertNoErrors(model)
	}

	@Test
	def checkWrongNumberOfValues(){
		val set = resourceSetProvider.get()
		interfaces.importInOutTestSignature(set)
		val model = parse('''
			import 'iA.signature'

			interface iA

			machine iABehavior {
				initial state S1 {
					transition trigger: C1(int i, int j, int k)
					do: reply(*,*)
					next state: S1

					transition trigger: C2(bool f) do: reply(*)
					next state: S1
				}
			}
		''', set)
		assertError(model, COMMAND_REPLY, ActionsValidator.REPLY_WRONG_NUMBER_PARAMS, 'Wrong number of values in reply.')
	}

	@Test
	def checkWrongValueTypes(){
		val set = resourceSetProvider.get()
		interfaces.importInOutTestSignature(set)
		val model = parse('''
			import 'iA.signature'

			interface iA

			machine iABehavior {
				initial state S1 {
					transition trigger: C1(int i, int j, int k)
					do: reply(*, 2.0)
					next state: S1

					transition trigger: C2(bool f)
					do: do: reply(*,*)
					next state: S1
				}
			}
		''', set)
		assertError(model, COMMAND_REPLY, ActionsValidator.REPLY_INVALID_TYPE, 'The type of the value must match the return type of the command or inout/out parameter type.')
	}

	@Test
	def checkWrongOutParamUsage(){
		val set = resourceSetProvider.get()
		interfaces.importInOutTestSignature(set)
		val model = parse('''
			import 'iA.signature'

			interface iA

			machine iABehavior {
				initial state S1 {
					transition trigger: C1(int i, int j, int k)
					do: j := k
					    reply(*, *)
					next state: S1

					transition trigger: C2(bool f)
					next state: S2
				}

				state S2 {
					transition do: reply(*,*) to command C2 next state: S1
				}
			}
		''', set)
		assertError(model, EXPRESSION_VARIABLE, null, 'Out parameters are write-only.')
	}
}