/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.generator

class ExpectedJavaFiles {
	
	def static expectedjavaGeneratedModelTesterjava() '''
public class ModelTester{
		
	public enum state {
		Off,
		SwitchingOn,
		On,
		TakingPicture,
		BatteryLow
	};

	//initialize current state with the initial state.
public state currentState = state.Off;
	private enum runTime {
		ERROR, WARNING, CORRECT
	};
	
	private runTime runtime;
	private int eventNumber = 0;
	
	private Map<String, ArrayList<String>> commandAndReply = new HashMap<String, ArrayList<String>>();
	private String event;
	private String currentReply;
	private String notification;
	private boolean isLastNotification;
	private boolean isNotify;
	private state expectedState;
	private boolean notificationReceived;
	private String notification;
	private org.eclipse.comma.types.types.impl.TypeReferenceImpl@306851c7 count ;
	
	public String generateEvent() throws InterruptedException {
	
			System.out.println();
			System.out.println("state " + currentState.toString());
			if (currentState == state.Off) {
					return off();			
			} 
			if (currentState == state.SwitchingOn) {
					return switchingon();			
			} 
			if (currentState == state.On) {
					return on();			
			} 
			if (currentState == state.TakingPicture) {
					return takingpicture();			
			} 
			if (currentState == state.BatteryLow) {
					return batterylow();			
			} 
			return "";
	
		}
	public String off() throws InterruptedException {
							
		double randomProbability = Math.random();
		String event = "";
		
		if(randomProbability < 0.2){
			event = "PowerOn";
			expectedState = SwitchingOn;
		}
		return event;
	}
	public String switchingon() throws InterruptedException {
							
		double randomProbability = Math.random();
		String event = "";
		
		if(randomProbability < 0.2){
			event = "PowerOff";
			expectedState = Off;
		}
		return event;
	}
	public String on() throws InterruptedException {
							
		double randomProbability = Math.random();
		String event = "";
		
		if(randomProbability < 0.2){
			event = "Click";
			expectedState = TakingPicture;
		}
		if(randomProbability < 0.2 && ( count > 0)){
			event = "GetPictureNumber";
			expectedState = On;
		}
		if(randomProbability < 0.2){
			event = "PowerOff";
			expectedState = Off;
		}
		return event;
	}
	public String takingpicture() throws InterruptedException {
							
		double randomProbability = Math.random();
		String event = "";
		
		if(randomProbability < 0.2){
			event = "PowerOff";
			expectedState = Off;
		}
		return event;
	}
	public String batterylow() throws InterruptedException {
							
		double randomProbability = Math.random();
		String event = "";
		
		return event;
	}
}
	
	public void notificationReceived(String notify) throws ClassNotFoundException, IOException, InterruptedException {	
	
		notificationReceived = true;
		notification = notify;
		oldState = currentState;
		
		
		if (receivedNotification.contains("CameraStatus")) {
			currentState = state.On;
		}
		
		
		if (receivedNotification.contains("CameraStatus")) {
			currentState = state.Off;
		}
		
		
		if (receivedNotification.contains("LowBattery")) {
			currentState = state.BatteryLow;
		}
		
		
		if (receivedNotification.contains("LowBattery")) {
			currentState = state.BatteryLow;
		}
		
		
		if (receivedNotification.contains("PictureTaken")) {
			currentState = state.On;
		}
		
		
		if (receivedNotification.contains("LowBattery")) {
			currentState = state.BatteryLow;
		}
		
		
		if (receivedNotification.contains("LowBattery")) {
			currentState = state.BatteryLow;
		}
		
		
		if (receivedNotification.contains("EmptyBattery")) {
			currentState = state.Off;
		}
		
		
	}

	public void printInfo() {
		if (notificationReceived) {
			System.out.println("Notification = " + notification);
			notificationReceived = false;
			System.out.println();
			System.out.println("state " + oldState.toString());
		}else {
			System.out.println();
			System.out.println("state " + oldState.toString());
		}
		
		
		if (!notificationReceived) {
			// System.out.println();
			System.out.println("event number = " + eventNumber);

		}

		if (currentEvent.contains("SIGNAL")) {
			System.out.println("Signal = " + currentEvent);
		} else {
			System.out.println("Command = " + currentEvent);
			System.out.println("Reply = " + currentReply);
		}
		
		

	}

	public void receive(String vendingReply) throws ClassNotFoundException, IOException, InterruptedException {
		
		defineState(vendingReply);
		currentReply = vendingReply;
		printInfo();
		oldState = currentState;
		Thread.sleep(3);

		if (modelChecker() == runTime.CORRECT && parametersCheckAndUpdate() == runTime.CORRECT) {
		
			
			//System.out.println("!currentReply.contains(\"inventoryInfo NOTIFY\") || currentEvent.contains(\"SIGNAL\") "+(!currentReply.contains("inventoryInfo NOTIFY") || currentEvent.contains("SIGNAL")));
			if ( (!currentReply.contains("inventoryInfo NOTIFY") || currentEvent.contains("SIGNAL")) ) {
				simulate();
			}

		} else {
			System.out.println(currentReply);
			System.out.println("ERROR another reply was expected.");
		}
		//System.out.println("Nothing happens");
	}
	}
	private void defineState(String vendingReply) throws InterruptedException {
	
			if(currentEvent.contains("switchOff")) {
				currentState = state.HIBERNATE;
			}
			if (vendingReply.contains("switchOnResult::SWITCH_ON_OK")) {
	
				currentState = state.STANDBY;
	
			}
		}
		
		public void simulate() throws ClassNotFoundException, IOException, InterruptedException {
				String generatedEvent = generateEvent();
				currentEvent = generatedEvent;
				
				if (eventNumber < 1000) {
					
					eventNumber++;
					//System.out.println(currentEvent);
					vendingConnection.send(generatedEvent);
		
				} else {
					vendingConnection.send("leave");
				}
			}
'''
	
}
