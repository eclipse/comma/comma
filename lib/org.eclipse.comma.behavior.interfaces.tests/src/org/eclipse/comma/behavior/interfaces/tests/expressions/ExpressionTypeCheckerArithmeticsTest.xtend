/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))

class ExpressionTypeCheckerArithmeticsTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctArithmeticsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			real r
			string s
			bool b

			init

			i := 1 + 2
			i := i - 1
			i := 2 * 3
			i := -1
			i := +1
			r := 2.0
			r := -2.0
			r := +32.0
			r := 1.0 + r
			r := 1.0 -r
			r := 1.0 * 2.0
			i := 1 / 2
			i := 1 mod 2
			r := 1.2 / 2.3
			i := 1 ^ 2
			r := 1.0 ^ 2.0
			s := ""
			s := s + "1"
			b := true
			b := false
			b := not b
			b := true and false
			b := false or true
			i := 1 min 2
			i := 1 max 2
			r := 1.2 min 2.0
			r := 1.2 max 3.0
			b := 1 < 2
			b := 1 <= 2
			b := 1 == 2
			b := 1 != 2
			b := 1 > 3
			b := 1 >= 3
			b := 1.0 < 2.0
			b := 1.0 <= 2.0
			b := 1.0 == 2.0
			b := 1 != 2.0
			b := 1.0 > 3.0
			b := 1.0 >= 3.0
			b := true == 3
			b := (not b)

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertNoIssues(result)
	}

	@Test
	def void erroneousANDTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			b := true and 1

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_AND, null, "Type mismatch: expected type bool")
	}

	@Test
	def void erroneousORTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			b := 2 or true

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_OR, null, "Type mismatch: expected type bool")
	}

	@Test
	def void erroneousNOTTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			b := not 1

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, EXPRESSION_NOT, null, "Type mismatch: expected type bool")
	}

	@Test
	def void erroneousLESSTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			b := 1 < 2.3

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, ASSIGNMENT_ACTION, null, "Arguments must be of compatible types")
	}

	@Test
	def void erroneousLESS1Test(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			b := true < false

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, EXPRESSION_LESS, null, "Type mismatch: expected type int or real")
	}

	@Test
	def void erroneousADDest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			i := 1 + 2.0

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, ASSIGNMENT_ACTION, null, "Arguments must be of compatible types")
	}

	@Test
	def void erroneousADDSUBTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			b := true + false
			b := true - false

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_SUBTRACTION, null, "Type mismatch: expected type int or real")
	}

	@Test
	def void erroneousMINUSTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			i := - "a"

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_MINUS, null, "Expected type int or real")
	}

	@Test
	def void erroneousMODULOTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			bool b

			init

			i := 1.0 mod 2.0

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_MODULO, null, "Type mismatch: expected type int")
	}
}