/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceDefinitionInjectorProvider)
class FormatterTest {

	@Inject extension FormatterTestHelper

	/**
	 * This example tests if the formatted document equals the unformatted document.
	 * This is the most convenient way to test a formatter.
	 */
	@Test def void stateMachineEmpty() {
		assertFormatted[
			toBeFormatted = '''
				import ""

				interface Interface

				machine statemachine {

					initial state initialState {
					}
				}
			'''
		]
	}

	/**
 	* This example tests whether a messy document is being formatted properly.
 	* In contrast to the first example, this approach also allows to test formatting strategies that are input-aware.
 	* Example: "Change newLines between tokens to be one at minimum, two at maximum."
 	* Here, it depends on the formatters input document whether there will be one or two newLines on the output.
 	*/

	@Test def void stateMachineVariables() {

		assertFormatted[
			expectation = '''
				import ""

				interface Interface

					variables
					int number
					bool notificationPending
					Person newStudent

				machine statemachine {

					initial state initialState {
					}
				}
			'''
			toBeFormatted = '''
				import ""

				interface Interface


					variables
					int   number		bool    notificationPending		Person   newStudent
				machine statemachine {	initial
					 state initialState {		}	}
			'''
		]
	}


//	@Test def void stateMachineRecordExample() {
//		assertFormatted[
//			toBeFormatted = '''
//				import "IUniversity.signature"
//
//				interface IUniversity
//
//					variables
//					int number
//					bool notificationPending
//					Person newStudent
//
//					init
//					number := 0
//					notificationPending := false
//
//				machine main {
//
//					// In this state the user can initiate the enrollment process. Other commands are not allowed
//					initial state Process {
//
//						transition trigger: startEnrollment
//							do:
//							reply
//							next state: Enrolling
//					}
//
//					state Enrolling {
//
//						// A person is added and enrolled in an university
//						// The confirmation is given by notifying the client for a new student
//						// The new student has the same name and age as the person and is assigned with a student number
//						// It is not possible to add a new person before receiving teh notification
//						transition trigger: add( Person p)
//							guard: NOT notificationPending
//								do: notificationPending := true
//								newStudent := p
//								reply
//								next state: Enrolling
//
//						transition
//							guard: notificationPending
//								do: number := number + 1
//								newStudent(Student {name = newStudent.name, age = newStudent.age, number = number})
//								notificationPending := false
//								next state: Enrolling
//
//						transition trigger: endEnrollment
//							guard: NOT notificationPending
//								do:
//								reply
//								next state: Process
//					}
//				}
//			'''
//		]
//	}
}
