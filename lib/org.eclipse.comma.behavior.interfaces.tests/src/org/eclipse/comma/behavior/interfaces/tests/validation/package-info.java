/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation;

/*
 * Class ImportedInterfaces includes all interfaces that are needed and used from all the classes
 * 
 * The test classes correspond to the validator classes
Only exception is the class ValidationTest which was the initial original test class 
and it is needed for the other classes to run. 
It has some general methods that test if everything is working as expected.

Some methods of the validator contain many checks and might produce several errors/warnings with different messages 
         
The big majority of the messages are tested in a separate method.

every method in the Tests has the name of the corresponding validator method plus the suffix Test

eg: checkEventCall(){} from the validator is tested in checkEventCallTest(){

Note: if the method in the validator contains many messages then 
there might be many test methods (one for every message).
Then the name of the test methods is the name of the validator method that is tested
plus the suffix Test plus a suffix that corresponds to the tested message.

eg: the method checkReply in the CommandsValidator.xtend

it is tested through several methods in the CommandsValidatorTests.xtend 
These methods are:

 	checkReplyTestNoTrigger() {}
 	checkReplyTestNotificationsOrSignals(){}
 	checkReplyTestParameters(){}
 	checkReplyTestReturn(){}
 	checkReplyTestReturnMatch(){}
 * 
 */
