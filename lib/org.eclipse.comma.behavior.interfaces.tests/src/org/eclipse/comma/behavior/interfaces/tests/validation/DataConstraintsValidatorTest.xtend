/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class DataConstraintsValidatorTest {
	@Inject extension ParseHelper<InterfaceDefinition> parseHelper

	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	@Test
	def void varAndRulesDuplicatesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			data constraints
			variables
			int X
			int X

			DR1 reply(X) to command c where X > 0
			DR1 reply(X) to command c where X > 0
			''', set)
		assertError(result, DATA_CONSTRAINT, null, "Duplicate data constraint")
		assertError(result, VARIABLE, null, "Duplicate variable")
	}

	@Test
	def void erroneousUseOfVarsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			data constraints
			variables
			int X
			int Y

			DR1 reply(X) to command c; reply(X) where Y > 0
			''', set)
		assertError(result, EXPRESSION_VARIABLE, null, "The variable is already bound to a value.")
		assertError(result, EXPRESSION_VARIABLE, null, "The variable is not previously bound to a value.")
	}

	@Test
	def void unusedVarTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			data constraints
			variables
			int X
			int Y

			DR1 reply(X) to command c where X > 0
			''', set)
		assertWarning(result, DATA_CONSTRAINTS_BLOCK, null, "Variable not used.")
	}

	@Test
	def void erroneousTypeInConditionTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR1 reply(X) to command c where X
			''', set)
		assertError(result, DATA_CONSTRAINT, null, "Condition has to be of boolean type.")
	}

	@Test
	def void useOFVarInNegationTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR1 no [reply(X) to command c] where X > 0
			''', set)
		assertError(result, EXPRESSION_VARIABLE, null, "Variables cannot be used in negated events.")
	}
}
