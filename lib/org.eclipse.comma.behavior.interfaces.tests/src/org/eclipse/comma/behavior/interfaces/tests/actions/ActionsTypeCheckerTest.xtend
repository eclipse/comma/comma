/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.actions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.actions.validation.ActionsValidator
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))
class ActionsTypeCheckerTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			Person p
			real r

			init
			i := 1
			p := Person{name = "John Smith", age = 20, adr = Address{street = "", number = 23}}
			p.adr.number := 12

			machine StateMachine {

				initial state Initial {

					transition trigger: c(int i)
						do:
							if i > 1 then
								reply(2)
							else
								reply(*)
							fi
						next state: Initial

				}
			}
			''', URI.createURI("iA.interface"), set)
		assertNoErrors(result)
	}

	@Test
	def void erroneousAssignmentsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i
			Person p
			real r

			init
			i := 1.0
			p := Person{name = "John Smith", age = 20, adr = Address{street = "", number = 23}}
			p.adr.number := ""

			machine StateMachine {

				initial state Initial {

					transition trigger: c(int i)
						do:
							if i > 1 then
								reply(2)
							else
								reply(1)
							fi
						next state: Initial

				}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, ASSIGNMENT_ACTION, null, "Type mismatch: actual type does not match the expected type")
		assertError(result, RECORD_FIELD_ASSIGNMENT_ACTION, null, "Type mismatch: actual type does not match the expected type")
	}

	@Test
	def void erroneousIfStatementTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {

					transition trigger: c(int i)
						do:
							if "" then
								reply(2)
							else
								reply(1)
							fi
						next state: Initial

				}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, IF_ACTION, null, "Type mismatch: the type of the condition must be boolean")
	}

	@Test
	def void erroneousReturnStatementTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {

					transition trigger: c(int i)
						do:
							if i > 1 then
								reply(3.5)
							else
								reply(1)
							fi
						next state: Initial

				}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, COMMAND_REPLY, ActionsValidator.REPLY_INVALID_TYPE, "The type of the value must match the return type of the command or inout/out parameter type.")
	}

	@Test
	def void erroneousReturnParamsStatementTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {

					transition trigger: b(int j, int k, int l)
						do:
							if i > 1 then
								reply(1, 2, 3)
							else
								reply(1, 2)
							fi
						next state: Initial

				}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, COMMAND_REPLY, ActionsValidator.REPLY_WRONG_NUMBER_PARAMS, "Wrong number of values in reply.")
	}

	//usage of *
	@Test
	def void erroneousAnyValueTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA


			machine StateMachine {

				initial state Initial {

					transition trigger: c(int i)
						do:
							i := *
							if i > 1 then
								reply(*)
							else
								reply(i)
							fi
							newPerson(Person{name = *, age = 23, adr = Address{street = *, number = *}})
						next state: Initial

				}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, ASSIGNMENT_ACTION, null, "Expression * cannot be used in this context")
	}
}