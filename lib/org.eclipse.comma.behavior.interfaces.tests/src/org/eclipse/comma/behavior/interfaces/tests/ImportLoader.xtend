/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.util.StringInputStream

class ImportLoader {
	
	def static importInOutTestSignature(ResourceSet set){
		val res = set.createResource(URI.createURI("iA.signature"))
		res.load(new StringInputStream("
			signature iA

			commands
			void C1(int i, inout int j, out int k)
			int C2(out bool flag)
		"), emptyMap)
	}
	
	def static cameraInterfaceIf(ResourceSet set) {
		val res = set.createResource(URI.createURI("Camera_interface.signature"));
		res.load(new StringInputStream('''
				/* The example ICamera interface defines signatures of commands and notifications supported by a digital camera.
								 */
								
								signature ICamera 
									types
									/* Users can define their own enumeration types. The language provides a set of predefined primitive types:
									 * int, bool, string, real, void.
									 * User defined enumeration type Status defines two literals that indicate the success or failure of an operation. 
									 */
									 enum Status {OnOK OnFailed}  
									 
									commands 
										/*Turns on the camera. Returns a value of type Status.*/
										Status PowerOn
										
										/*Turns off the camera. */
										void   PowerOff
										
										/*Takes a picture */
										void   Click
										
										/*Returns the number of the last taken picture. */
										int    GetPictureNumber
										 
									notifications
										/*Indicates change of status of the camera. */
										CameraStatus(Status s)
										
										/*Indicates that the camera is running on a low battery. */		
										LowBattery
										
										/*Indicates that the battery is depleted and the camera will turn off. */
										EmptyBattery
										
										/*Indicates that a picture has been successfully taken. */
										PictureTaken
				
			'''
		), emptyMap);
	}	
	
	def static recordsUniversityIf(ResourceSet set) {
		val res = set.createResource(URI.createURI("IUniversity.signature"));
		res.load(new StringInputStream('''
				signature IUniversity
					types
					/* Simple interface that illustrates the usage of records.
					 * Record types can be defined at the global scope (type Person) of in an interface (Student)
					 */
					record Person {
						string name,
						int age
					}
					record Student {
						string name,
						int age,
						int number
					}
				
					commands
					void add(Person p)
					void startEnrollment
					void endEnrollment
				
					notifications
					newStudent(Student s)
								
				'''
		), emptyMap);
	}
	
	def static IworkerIf(ResourceSet set) {
		val res = set.createResource(URI.createURI("interface.signature"));
		res.load(new StringInputStream(
			'''
				signature IWorker
					types
					enum Status {STARTED STOPPED}
					enum Activity {WORKING SLEEPING}
					
					commands
					Status start
					
					signals
					sleep
					wakeup
					stop
					
					notifications
					status(Activity a)
					startedWorking
				
			'''
		), emptyMap);
	}

	def static Iworker2If(ResourceSet set) {
		val res = set.createResource(URI.createURI("interface2.signature"));
		res.load(new StringInputStream(
			'''
				signature IWorker2
					types
					enum Status2 {
						STARTED
						STOPPED
					}
					enum Activity2 {
						WORKING
						SLEEPING
					}
				
					commands
					Status2 start2
				
					signals
					sleep2(Activity2 a)
					wakeup2
					stop2
				
					notifications
					status2(Activity2 a)
					startedWorking2
			'''
		), emptyMap);
	}

	def static IworkerReqIf(ResourceSet set) {
		val res = set.createResource(URI.createURI("interfaceReq.signature"));
		res.load(new StringInputStream(
			'''
				signature IWorkerReq
					types
					enum StatusReq {
						STARTED
						STOPPED
					}
					enum ActivityReq {
						WORKING
						SLEEPING
					}
				
					commands
					StatusReq startReq
				
					signals
					sleepReq(ActivityReq a)
					wakeupReq
					stopReq
				
					notifications
					statusReq(ActivityReq aReq,ActivityReq bReq,ActivityReq cReq)
					startedWorkingReq
			'''
		), emptyMap);
	}
	
	def static expressionTypeCheckerSignature(ResourceSet set){
		val res = set.createResource(URI.createURI("iA.signature"));
		res.load(new StringInputStream(
			'''
			signature iA
			
			types
			
			type wstring
			type long based on int
			type ulong based on int
			
			record Person {string name, int age}
			
			record Student extends Person {int sNumber}
			
			vector Students = Student[]
			
			commands
			void c
			'''
		), emptyMap);
	}
	
	def static actionsSignature(ResourceSet set){
		val res = set.createResource(URI.createURI("iA.signature"));
		res.load(new StringInputStream(
			'''
			signature iA
			
			types
			
			record Address{  
				string street  
				int number
			}
			
			record Person {string name, int age, Address adr}
			
			commands
			int c(int i)
			int b(int j, inout int k, out int l)
			
			signals
			s(bool b)
			
			notifications
			newPerson(Person p)
			'''
		), emptyMap);
	}
	
}
