/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.util.StringInputStream

/*
 * This class contains all imported interfaces that are used from the ValidatorTest classes
 */
@InjectWith(MultiLangInjectorProvider)
class ImportedInterfaces {
	
	def importInOutTestSignature(ResourceSet set){
		val res = set.createResource(URI.createURI("iA.signature"))
		res.load(new StringInputStream("
			signature iA

			commands
			void C1(int i, inout int j, out int k)
			int C2(out bool flag)
		"), emptyMap)
	}
	
	def importICamera(ResourceSet set){
		//val normalizedCamera = set.getURIConverter().normalize(URI.createURI("imports/Camera_interface.if"))	
		val res = set.createResource(URI.createURI("imports/Camera_interface.signature"));
		res.load(new StringInputStream("signature ICamera  
			types
				/* Users can define their own enumeration types. The language provides a set of predefined primitive types:
				 * int, bool, string, real, void.

				 * User defined enumeration type Status defines two literals that indicate the success or failure of an operation. 
	
				 */

					enum Status {OnOK OnFailed}  

				commands 
	
					/*Turns on the camera. Returns a value of type Status.*/
	
					Status PowerOn

					/*Turns off the camera. */
	
					void   PowerOff

					/*Takes a picture */
	
					void   Click
	
					
					/*Returns the number of the last taken picture. */	
					int    GetPictureNumber
	
				notifications
	
					/*Indicates change of status of the camera. */
	
					CameraStatus(Status s)	
					
					/*Indicates that the camera is running on a low battery. */		
	
					LowBattery	
					

					/*Indicates that the battery is depleted and the camera will turn off. */
	
					EmptyBattery

					/*Indicates that a picture has been successfully taken. */

					PictureTaken

			"), emptyMap);
	}
	def importIVendingMachine(ResourceSet set){
			
		val res = set.createResource(URI.createURI("imports/IVendingMachine.signature"));
		res.load(new StringInputStream("			
			signature IVendingMachine 
				
			types

			record Product{
				string name,
				int cost
				
			}
			
			enum coinStatus {ACCEPTED NOT_ACCEPTED}
			enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }
				
				commands
					void somethingReturned(int x)
					coinStatus coinThrowedIn
					int returnMoney
					result orderProduct(Product p)
					
				signals
				
					endProcess
				
				notifications
			
					creditsShown
											
			"), emptyMap);
	}
	def importIVendingMachineSecondInterface(ResourceSet set){
		
		val res = set.createResource(URI.createURI("imports/IVendingMachineSecondInterface.signature"));
			res.load(new StringInputStream("signature IVendingMachineSecondInterface
	
				commands
				
					void coinThrowedIn
					void something
					
				signals
				
					endProcess2
									
			"), emptyMap);
	}
	
	def importCheckDisjointProvidedInterfacesTest(ResourceSet set){
		
		val res = set.createResource(URI.createURI("imports/CheckDisjointProvidedInterfacesTest.signature"));
			res.load(new StringInputStream("
			interface CheckDisjointProvidedInterfacesTest signature{

			types

			enum coinStatus { ACCEPTED NOT_ACCEPTED}
		
			commands
			
				coinStatus coinThrowedIn
									
			}	"),emptyMap);
	}
	def importCheckDuplicateMachineNamesTest(ResourceSet set){
		
		val res = set.createResource(URI.createURI("imports/CheckDuplicateMachineNamesTest.signature"));
			res.load(new StringInputStream("
			signature CheckDuplicateMachineNamesTest

			types

			enum coinStatus { ACCEPTED NOT_ACCEPTED}

			commands
	
			coinStatus coinThrowedIn
			"),emptyMap);
	}
	def importInterfaceDefinitionTests(ResourceSet set){
		
		val res = set.createResource(URI.createURI("imports/InterfaceDefinitionTests.signature"));
			res.load(new StringInputStream("

			signature InterfaceDefinitionTests 
			
				commands
				
					coinStatus coinThrowedIn
					int returnMoney
					result orderProduct(Product p)
													
			"),emptyMap);
	}
	def importInterface2DefinitionTests(ResourceSet set){
		val vending6 = set.getURIConverter().normalize(URI.createURI("imports/Interface2DefinitionTests.signature"))	
		val res8 = set.createResource(vending6);
			res8.load(new StringInputStream("signature Interface2DefinitionTests signature

			commands
					
		"),emptyMap);
	}
	def importIVendingMachineTest(ResourceSet set){
		
		val res = set.createResource(URI.createURI("IVendingMachineTest.signature"));
			res.load(new StringInputStream("
			
			signature IVendingMachine 
			
				types

				record Product{
					productName name,
					int cost
					
				}
				enum productName {WATER
								  COLA 
								  JUICE
				}
				enum coinStatus {ACCEPTED 
								 NOT_ACCEPTED
				}
				enum result {productDelivered_NoChange 
					         productDelivered_ThereIsChange
					         productDelivered_NoChange_ZeroTotalSupplies
					         productDelivered_ThereIsChange_ZeroTotalSupplies
					         notEnough_Money 
					         notEnough_Supplies 
				}
				
				commands
				
					void loadProducts(int totalSupplies)
					coinStatus coinThrowedIn
					int returnMoney
					result orderProduct(Product p)
					void A(int X,int Y)
					
				signals
				
					endProcess
				
				notifications
			
					totalSuppliesZero
											
			"),emptyMap);
			
	}
	def importCheckEventCallTest(ResourceSet set){
	
		val res = set.createResource(URI.createURI("imports/CheckEventCallTest.signature"));
		res.load(new StringInputStream("
			
		signature CheckEventCallTest
			types
			record Product{
				string name,
				int cost
				
			}
			enum coinStatus {ACCEPTED NOT_ACCEPTED}
		
			enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }
		
			
			commands
			
				coinStatus coinThrowedIn
				int returnMoney
				result orderProduct(Product p)
				checkEventCallTestExpressionType(bool p)
				void method
			signals
				methodSignal
				returnRandom
				endProcess
			
			notifications
				method1
				creditsShown
									
		"), emptyMap);
	}
	def importCheckEventCallTestRequired(ResourceSet set){
		
		val res = set.createResource(URI.createURI("imports/CheckEventCallTestRequired.signature"));
		res.load(new StringInputStream("signature CheckEventCallTestRequired

			commands
				
				void coinThrowedIn
				void something
				int returnMoney	
				
			notifications
				
				int returnMoney				
		"),emptyMap);
	}
	def importCheckEventCallTestRequired2(ResourceSet set){

		val res = set.createResource(URI.createURI("imports/CheckEventCallTestRequired2.signature"));
		res.load(new StringInputStream("signature CheckEventCallTestRequired2

				commands
					
					void coinThrowedIn
					void something
					void method
					
				signals
					newSignal
					methodSignal
						
				notifications
					method1
					int returnKati			
			"),emptyMap);
	}
	def importInterfaceEnumTests(ResourceSet set){
			
		val res = set.createResource(URI.createURI("imports/InterfaceEnumTests.signature"));
		res.load(new StringInputStream("signature InterfaceEnumTests
				types

			record Product{
				string name,
				int cost
				
			}
			

				enum coinStatus {ACCEPTED NOT_ACCEPTED}
				enum result {productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies}
			
				
				commands
				
					coinStatus coinThrowedIn
					int returnMoney
					result orderProduct(Product p)
					
				signals
				
					endProcess
				
				notifications
			
					creditsShown
											
			"),emptyMap);
	}
	def importInterface2EnumTests(ResourceSet set){
	
		val res = set.createResource(URI.createURI("imports/Interface2EnumTests.signature"));
		res.load(new StringInputStream("signature Interface2EnumTests

			types
				enum coinStatus {ACCEPTED NOT_ACCEPTED}
				
			commands
			
				void coinThrowedIn
				void something
				
			signals
			
				endProcess2
								
			"),emptyMap);
	}
	def importInterfaceForExpressionsValidatorRecordTests(ResourceSet set){
		
		val res = set.createResource(URI.createURI("imports/InterfaceForExpressionsValidatorRecordTests.signature"));
		res.load(new StringInputStream("signature InterfaceForExpressionsValidatorRecordTests
				types
			enum coinStatus {ACCEPTED NOT_ACCEPTED}
			enum result {productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies}
			

				record Product{
				string name,
				int cost
				
				}
				
				commands
				
					coinStatus coinThrowedIn
					int returnMoney
					result orderProduct(Product p)
					Product returnProduct
				signals
				
					endProcess
				
				notifications
			
					creditsShown
											
			"), emptyMap);
			
	}
	def importInterface2ForExpressionsValidatorRecordTests(ResourceSet set){
		val res = set.createResource(URI.createURI("imports/Interface2ForExpressionsValidatorRecordTests.signature"));
		res.load(new StringInputStream("signature Interface2ForExpressionsValidatorRecordTests

					types
						record Product{
							string name,
							int cost
						}
					
					commands
					
						void coinThrowedIn
						void something
						
					signals
					
						endProcess2
										
				"), emptyMap);
	}
	def importCheckUnusedInterfaceTestRequired(ResourceSet set){
			val res = set.createResource(URI.createURI("imports/CheckUnusedInterfaceTestRequired.signature"));
			res.load(new StringInputStream("signature CheckUnusedInterfaceTestRequired
				
				commands
				
					void someCommand
													
			"), emptyMap);
	}
	def importCheckRepliesForTransitionTest(ResourceSet set){
		val res = set.createResource(URI.createURI("imports/CheckRepliesForTransitionTest.signature"));
			res.load(new StringInputStream("
				signature CheckRepliesForTransitionTest
					types
					record Product{
					string name,
					int cost
					
				}
				
				enum coinStatus {ACCEPTED NOT_ACCEPTED}
				enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }
				
				
					
					commands
					
						coinStatus coinThrowedIn
						int returnMoney
						result orderProduct(Product p)
						
					signals
						
						endProcess
					
					notifications
						
						creditsShown
												
					"), emptyMap);
	}
	def importCheckTransitionTriggerTestDefinition(ResourceSet set){	
		val res = set.createResource(URI.createURI("imports/CheckTransitionTriggerTestDefinition.signature"));
			res.load(new StringInputStream("
				signature CheckTransitionTriggerTestDefinition
					types
					record Product{
					string name,
					int cost
					
				}
				
				enum coinStatus {ACCEPTED NOT_ACCEPTED}
				enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }
				
				
					
					commands
					
						
						int returnMoney
						result orderProduct(Product p)
						
					signals
					
						
						endProcess
					
					notifications
						
						creditsShown
												
				"), emptyMap);
	}
	def importCheckTransitionTriggerTestInterfaceNotAmong(ResourceSet set){
		val res = set.createResource(URI.createURI("imports/CheckTransitionTriggerTestInterfaceNotAmong.signature"));
			res.load(new StringInputStream("
				
				signature CheckTransitionTriggerTestInterfaceNotAmong
					types

				record Product{
					string name,
					int cost
					
				}
				
				enum coinStatus {ACCEPTED NOT_ACCEPTED}
				enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }

					
					commands
					
						coinStatus coinThrowedIn
						int returnMoney
						result orderProduct(Product p)
						
					signals
						
						endProcess
					
					notifications
						
						creditsShown
												
					"), emptyMap);
	}
	def importCheckTransitionTriggerTestTriggerIsNotification(ResourceSet set){
		val res = set.createResource(URI.createURI("imports/CheckTransitionTriggerTestTriggerIsNotification.signatures"));
			res.load(new StringInputStream("

					
					signature CheckTransitionTriggerTestTriggerIsNotification
					types
					record Product{
						string name,
						int cost
						
					}
					
					enum coinStatus {ACCEPTED NOT_ACCEPTED}
					enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }
					
						
						commands
						
							
							int returnMoney
							result orderProduct(Product p)
							
						signals
							
							endProcess
						
						notifications
							coinStatus coinThrowedIn
							creditsShown
													
				"), emptyMap);
	}
	def importCheckTriggerSignatureTestDuplicateParameter(ResourceSet set){	
		val res = set.createResource(URI.createURI("imports/CheckTriggerSignatureTestDuplicateParameter.signature"));
			res.load(new StringInputStream("

				
				signature CheckTriggerSignatureTestDuplicateParameter
				types
				record Product{
					string name,
					int cost
					
				}
				
				enum coinStatus {ACCEPTED NOT_ACCEPTED}
				enum result{productDeliveredNoChange productDeliveredThereIsChange notEnoughMoney notEnoughSupplies  }
				
					
					commands
						
						int returnMoney
						result orderProduct(Product p,Product d)
						
					signals
					
						endProcess
					
					notifications
				
						creditsShown
												
					"), emptyMap);
	}
	def importIVendingMachineCorrectEdition(ResourceSet set){	
		val res = set.createResource(URI.createURI("imports/IVendingMachine.signature"));
			res.load(new StringInputStream("
			
			signature IVendingMachine
			types
			record Product{
					productName name,
					int cost
				}
				enum productName {WATER
								  COLA 
								  JUICE
				}
				enum coinStatus {ACCEPTED 
								 NOT_ACCEPTED
				}
				enum result {productDelivered_NoChange 
					         productDelivered_ThereIsChange
					         productDelivered_NoChange_ZeroTotalSupplies
					         productDelivered_ThereIsChange_ZeroTotalSupplies
					         notEnough_Money 
					         notEnough_Supplies 
				}
				
				commands
				
					void loadProducts(int totalSupplies)
					coinStatus coinThrowedIn
					int returnMoney
					result orderProduct(Product p)
					
				signals
				
					endProcess
				
				notifications
			
					totalSuppliesZero
											
			
		"),emptyMap);
	
		}

}
