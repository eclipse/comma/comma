/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import org.eclipse.comma.behavior.interfaces.tests.InterfaceDefinitionInjectorProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Disabled

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceDefinitionInjectorProvider)
class ExpressionCaseFormatterTest {

	@Inject extension FormatterTestHelper
	
	@Disabled
	@Test def void formatExpressions() {
		assertFormatted[
			expectation = '''
				import "ExpressionInterface.signature"

				interface Interface

					variables
					bool a
					real b
					string s
					int i
					Address adr
					People pp
					Person[] ppp
					Status enumVar

					init
					a := true
					a := false
					a := a AND true
					a := a OR false
					a := 1 == 3
					a := 2 != 2
					a := Status::On == Status::Off
					a := Status::On != Status::Off
					a := true == true
					a := false != true
					enumVar := Status::On
					a := enumVar == Status::Off
					b := 0.1
					a := b == b
					a := b != 0.1
					s := "A"
					a := "A" == "A"
					a := s != "A"
					a := 1 < 2
					a := b < 1.0
					a := 1 > 2
					a := 2.0 > 1.0
					a := 1 <= 2
					a := b <= 1.0
					a := 1 >= 2
					a := 2.0 >= 1.0
					i := 1
					i := -1
					i := 1 + 2
					i := i - 3
					b := 12.9 + 3.0
					b := b - b - 12.3
					i := 1 * 2
					b := 1.0 * 2.0
					i := 1 / 2
					b := 1.0 / 2.0
					i := 1 max 2
					i := 1 min 2
					b := 1.0 max 3.0
					b := 1.0 min 3.9
					i := 1 mod 2
					i := 2 ^ 3
					b := 2.0 ^ 3.0
					a := NOT true
					a := NOT a
					a := NOT (1 > 2)
					i := -1
					i := -i
					i := +i
					b := -0.1
					b := +1.1
					s := adr.city
					s := Address{street = "Baker", city = "London"}.street
					s := Person{adr = Address{street = "Baker", city = "London"}, name = "Holmes"}.name
					s := Person{adr = Address{street = "Baker", city = "London"}, name = "Holmes"}.adr.city
					adr := Address {street = "Baker", city = "London"}
					s := Person{adr = adr, name = "Holmes"}.name
					s := Person{adr = adr, name = "Holmes"}.adr.city
					pp := <People> [Person {adr = adr, name = "Holmes"}]
					ppp := <Person[]> [Person {adr = adr, name = "Holmes"}] // TODO check that any type cannot be used for declaring whatever, and * can be used only in events
					a := isEmpty (pp )
					a := contains (pp, s )
					pp := add (pp, Person {adr = adr, name = "Holmes"} )
					b := asReal (1 )
					b := abs (1.0 )
					i := abs (-1 )
					a := exists (Person p in pp : p.name == "Holmes")
					a := forAll (Person p in ppp : p.adr.street == "Baker")

				machine statemachine {

					initial state eventState2 {

						transition trigger: ping
							do:
							reply
							next state: eventState2
					}
				}
			'''
	toBeFormatted = '''
				import "ExpressionInterface.signature"

							interface Interface			variables			bool a
								real b			string s			int i
								Address adr
								People pp							Person[] ppp
								Status enumVar
																init
								a := 	true							a := false
								a := 	a	AND 	true								a := a OR false								a := 1 == 3
								a 	:=	 2 	!=	 2
								a 		:= 	Status::On	 == 	Status::Off								a := Status::On != Status::Off
								a := true == true								a := false != true
								enumVar  :=   Status::On								a := enumVar == Status::Off
								b  := 0.1							a := b == b
								a  := b != 0.1
								s  := "A"
								a  := "A" == "A"								a := s != "A"
								a   :=   1  <  2								a := b < 1.0
								a  :=  1  >  2					a := 	2.0 > 1.0
								a  :=  1   <=  2								a := b 	<= 	1.0
								a  :=  1  >=  2								a := 2.0 	>=	  1.0
								i  :=   1								i := -1
								i :=  1   +   2								i  :=  i -  3
								b :=   12.9 + 3.0								b := b - b - 12.3
								i :=   1 * 2								b  :=  1.0  *  2.0
								i := 1 / 2								b  :=  1.0  /  2.0
								i :=  1 max 2 								i := 1 min 2
								b :=  1.0 max 3.0								b := 1.0 min 3.9
								i := 1 mod 2 								i := 2 ^ 3
								b := 2.0 ^ 3.0								a := NOT true
								a := NOT a								a := NOT (1 > 2)
								i := -1								i := -i
								i := + i								b := -0.1
								b := + 1.1 								s := adr.city
								s := Address{street = "Baker", city = "London"}.street
								s := Person{adr = Address{street = "Baker", city = "London"}, name = "Holmes"}.name
								s := Person{adr = Address{street = "Baker", city = "London"}, name = "Holmes"}.adr.city
								adr := Address{street = "Baker", city = "London"}								s := Person{adr = adr, name = "Holmes"}.name
								s := Person{adr = adr, name = "Holmes"}.adr.city 								pp := <People>[Person{adr = adr, name = "Holmes"}]
								ppp :=   <Person[]>[Person{adr = adr, name = "Holmes"}]								//TODO check that any type cannot be used for declaring whatever, and * can be used only in events
								a :=   isEmpty(pp)
								a  :=  contains(pp, s)
								pp  :=  add(pp, Person{adr = adr, name = "Holmes"})
								b :=   asReal(1)								b := abs(1.0)
								i :=  abs(-1)								a := exists(Person p in pp : p.name == "Holmes")
								a :=  forAll(Person p in ppp : p.adr.street == "Baker")
								machine statemachine {
								initial  state  eventState2 {									transition  trigger: ping
									 do:  reply		 							next state: eventState2							}


							}
				'''
		]
	}


}
