/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.generator

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.generator.plantuml.BehaviorUmlGenerator
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))
class CameraGeneratorTest {

	@Inject ParseHelper<InterfaceDefinition> parseHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	InMemoryFileSystemAccess fsa;

	@BeforeEach
	def setup() {
		val set = resourceSetProvider.get()
		ImportLoader.cameraInterfaceIf(set)
		val model = parseHelper.parse('''
			/*This model defines the behavior of the interfaces defined in the imported model. */
			import "Camera_interface.signature"

			interface ICamera
				/* Interface behavior is defined by one or more state machines.
				 * In this example only one machine is used named 'camera'.
				 * The machine defines the behavior of the interfaces listed as provided (after the keyword 'provides').
				 */
					variables
						/* State machines may use variables. Variable count contains the number of taken pictures. */
						int count

					init
					count := 0

				machine camera {
					/*Exactly one state must be defined as initial. In the beginning the camera is in state Off. */
					initial state Off {

						/* Transitions can be initiated by commands defined in the provided interfaces.
						 * Transitions have a possibly empty body and a next state.
						 * Non-determinism is supported by giving several alternative bodies/next state for a command separated with
						 * OR keyword.
						 * In the example, the command PowerOn may succeed or fail. This is indicated by the returned value.
						 */
						transition trigger: PowerOn
							do: reply(Status::OnOK)            next state: SwitchingOn
							OR
							do: reply(Status::OnFailed)        next state: Off
					}

					/* In this state the camera is powering up. This may succeed or fail. The user is notified for the outcome.*/
					state SwitchingOn {
						transition trigger: PowerOff do: reply next state: Off

						/*Transitions may also happen without an explicit trigger. After certain period of time the camera is
						 * switched on and a notification is sent. The camera then enters into state On.
						 */
						transition do: CameraStatus(Status::OnOK)
						                                                next state: On

						/*Alternatively, the process of switching on may fail. A notification is sent and the camera
						 * goes back to the initial state Off.
						 */
						transition do: CameraStatus(Status::OnFailed)
						                                                next state: Off

						transition do:  LowBattery             next state: BatteryLow
					}

					/*In state On the camera is ready for taking pictures. */
					state On {
						/*In this state the command Click can be issued. In the previous states there were no transitions defined for this command.
						 * According to the semantics of the state machine language, if in a given state a command is not handled by a transition, the command
						 * is not allowed in this state.
						 */
						transition trigger: Click do: reply    next state: TakingPicture

						/*Similarly to many state machine notations, a transition may be guarded. The transition will be activated only if the
						 * guard expression is evaluated to true.
						 * In the example transition, the GetPictureNumber command can be executed only if there are pictures already taken,
						 * that is, the value of count is more than 0.
						 */
						transition trigger: GetPictureNumber guard: count > 0
							do: reply(count)                            next state: On
						transition trigger: PowerOff do: reply next state: Off
						   transition do:  LowBattery             next state: BatteryLow
					}

					/*In state TakingPicture the camera is busy with capturing and storing an image.
					 *After certain time (not specified here) the process is finished and a notification is sent (see the first transition)
					 */
					state TakingPicture {
						transition do: count := count+1 PictureTaken
						                                                next state: On
						transition trigger: PowerOff do: reply next state: Off
						transition do:  LowBattery             next state: BatteryLow
					}

					/*In state BatteryLow the camera is periodically sending notifications for the battery status.
					 */
					state BatteryLow {
						transition do:  LowBattery             next state: BatteryLow
						transition do: EmptyBattery            next state: Off
					}
				}

				/*The language allows definition of time rules. Time rules specify sequence of events and allowed time intervals between them. */
				timing constraints

				/*This rule defines the allowed time interval between two events.
				 * The first event is the call of operation PowerOff when the camera is in state SwitchingOn.
				 * The second event is the reply of the command. The interval states that the reply should be
				 * observed no later than 32 milliseconds after the call.
				 */
				TR1 in state SwitchingOn command PowerOff  - [ .. 32.0 ms ]
				    -> reply

				TR2 in state On command Click - [ 10.0 ms .. 100.0 ms ]
				    -> notification PictureTaken

				/*This rule illustrates the possibility to specify time interval between two events only if both events are observed.
				 * The command PowerOn may produce two outcomes. If the outcome is success then this must happen between 1 and 3 milliseconds
				 * after receiving the command.
				 */
				TR3 in state Off command PowerOn and reply(Status::OnOK)
				    -> [ 1.0 ms .. 3.0 ms ] between events

				/*In contrast to the previous rule, this rule handles the case of failure of the command PowerOn.
				 * The reply that indicates the failure may take a little longer, up to 4 milliseconds.
				 */
				TR4 in state Off command PowerOn and reply(Status::OnFailed)
				   -> [ 1.0 ms .. 4.0 ms ] between events

				/*This rule is an example of a periodic event. If the event LowBattery occurs then
				 * the notification LowBattery is sent periodically until a stopping event occurs. The stopping event here is
				 * the depletion of the battery (notification EmptyBattery).
				 */
				TR5 notification LowBattery
				    then notification LowBattery with period 750.0 ms jitter 50.0 ms
				    until notification EmptyBattery



		''', URI.createURI("CameraSpec.interface"), set)

		fsa = new InMemoryFileSystemAccess()


		val underTest = new BehaviorUmlGenerator(model.interface ,fsa)
		underTest.doGenerate
	}

	@Test
	def generateFiles() {
		Assertions.assertEquals(9, fsa.textFiles.size, "Statemachine generator did not create the expected amount of files")
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_TR1plantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_TR1.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_TR1plantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_TR2plantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_TR2.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_TR2plantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_TR3plantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_TR3.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_TR3plantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_TR4plantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_TR4.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_TR4plantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_TR5plantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_TR5.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_TR5plantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_camera_completeplantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_camera_complete.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_camera_completeplantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_camera_noactionsplantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_camera_noactions.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_camera_noactionsplantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_camera_noactions_noselftransplantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT +
			"CameraSpec_camera_noactions_noselftrans.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_camera_noactions_noselftransplantuml.toString)
	}

	@Test
	def generateumlCameraSpecsmCameraSpec_camera_noselftransplantuml() {
		val file = IFileSystemAccess::DEFAULT_OUTPUT + "CameraSpec_camera_noselftrans.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(file))

		Assertions.assertEquals(fsa.textFiles.get(file).toString,
			ExpectedPlantUmlFiles.cameraSpecsmCameraSpec_camera_noselftransplantuml.toString)
	}

}
