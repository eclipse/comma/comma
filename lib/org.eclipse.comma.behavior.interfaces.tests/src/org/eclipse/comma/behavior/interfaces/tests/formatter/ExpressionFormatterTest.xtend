/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.formatter

import com.google.inject.Inject
import org.eclipse.comma.behavior.interfaces.tests.InterfaceDefinitionInjectorProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceDefinitionInjectorProvider)
class ExpressionFormatterTest {

	@Inject extension FormatterTestHelper

	@Test def void formatExpressions() {
		assertFormatted[
			expectation = '''
				import "Interface.signature"

				interface IWorker

					variables
					Record rArg1
					bool s

				machine spec {

					in all states except Stopped {
					}

					initial state Stopped {

						transition trigger: start
							do: if ((1 > 1) AND (1 < 1)) then
								reply(Status::STARTED)
							else
								reply(Status::STARTED)
							fi
							if ((1 > 1) OR (1 < 1)) then
								reply(Status::STARTED)
							else
								reply(Status::STARTED)
							fi
							next state: Stopped
					}

					state Idle {

						transition trigger: sum2(Record a, int b)
							do: rArg1 := a
							rArg1.key := a.key
							b := b / b
							b := b * b
							b := b + b
							b := b - b
							s := b <= b
							s := b >= b
							b := b max b
							b := b min b
							b := b mod b
							next state: Stopped
					}
				}
			'''
			toBeFormatted = '''
				import "Interface.signature" 	interface IWorker	variables	Record rArg1 bool s
												machine spec {		in all	 states	 except 	Stopped	 {			}
														initial state Stopped {	transition trigger: start
																do:   if  ((1   >
																	1  )AND  (1   < 1)

																) then
																	reply(Status::STARTED)
																else
																	reply(Status::STARTED)
																fi
																if ((1   >  1 ) OR  (1   <  1)  ) then	reply(Status::STARTED)else	reply(Status::STARTED)			fi			next state: Stopped	}


															state Idle {	transition trigger: sum2(Record a, int b)							do: rArg1 := a
															rArg1.key := a.key	b := b / b			b := b * b		b := b + b	b := b - b	s := b <= b	s := b >= b
															b := b max b	b := b min b	b := b mod b  	next state: Stopped
													}
												}
			'''
		]
	}
}
