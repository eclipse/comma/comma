/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.generator.plantuml.BehaviorUmlGenerator
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class ExpressionUMLTest {

	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def logicalAndValidator(){
		val set = resourceSetProvider.get()
		ImportedInterfaces.importIWorker(set)

		val result = parseHelper.parse('''
			import "Interface.signature"
			interface IWorker
			machine spec {
				initial state Stopped {
					transition trigger: start
						do: if true AND 1 then
							reply(Status::STARTED)
						else
							reply(Status::STARTED)
						fi
						next state: Stopped
				}
			}

		''', URI.createURI("behavior.interface"), set);
		val fsa = new InMemoryFileSystemAccess()
		val genUML = new BehaviorUmlGenerator(result.interface, fsa)
		genUML.doGenerate()

		val complete = IFileSystemAccess::DEFAULT_OUTPUT + "behavior_spec_complete.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(complete))

		val noactions = IFileSystemAccess::DEFAULT_OUTPUT + "behavior_spec_noactions.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(noactions))

		val noselftrans = IFileSystemAccess::DEFAULT_OUTPUT + "behavior_spec_noselftrans.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(noselftrans))

		val noactions_noselftrans = IFileSystemAccess::DEFAULT_OUTPUT + "behavior_spec_noactions_noselftrans.plantuml";
		Assertions.assertTrue(fsa.textFiles.containsKey(noactions_noselftrans))
	}

}
