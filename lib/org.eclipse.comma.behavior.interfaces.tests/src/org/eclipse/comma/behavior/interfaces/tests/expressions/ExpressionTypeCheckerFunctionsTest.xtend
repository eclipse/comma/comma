/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))
class ExpressionTypeCheckerFunctionsTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctFunctionsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			bool b
			int[] i
			int j
			real r
			map<int, int> m

			init
			b := isEmpty(<int[]>[])
			b := contains(<int[]>[1, 2], 2)
			i := add(i, 2)
			j := abs(-2)
			r := asReal(j)
			j := length(Bulkdata<3000>)
			i := delete(int j in i : j == 2)
			b := exists(int j in i : j == 2)
			b := forAll(int j in i : j >= 3)
			m := <map<int, int>>{1 -> 1}
			b := hasKey(m, 1)
			m := deleteKey(m, 1)

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertNoErrors(result)
	}

	@Test
	def void unknownFunctionTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			bool b

			init
			b := remove(<int[]>[])

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Unknown function")
	}

	@Test
	def void erroneousQuantifierTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			bool b

			init
			b := exists(int i in true : i > 2)
			b := forAll(int i in <int[]>[1, 2] : "true")

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_QUANTIFIER, null, "Expression must be of type vector")
			assertError(result, EXPRESSION_QUANTIFIER, null, "Condition expression must be of type boolean")
	}

	@Test
	def void erroneousFunctionTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			bool b
			int[] i
			real r
			int j
			map<int, int> m

			init
			b := isEmpty(1, 2)
			b := isEmpty("")
			b := contains(1)
			b := contains("", "")
			i := add(i)
			i := add(1, 1)
			i := add(i, "")
			r := asReal()
			r := asReal(true)
			r := abs()
			r := abs(true)
			j := length()
			j := length(i)
			b := hasKey(<map<int, int>>{})
			m := deleteKey(1, 1)
			b := hasKey(<map<int, int>>{}, 3.0)


			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function isEmpty expects one argument")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function isEmpty expects argument of type vector")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function contains expects two arguments")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function contains expects first argument of type vector")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function add expects two arguments")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function add expects first argument of type vector")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Second argument does not conform to the base type of the vector")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function asReal expects one argument")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function asReal expects an argument of type int")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function abs expects one argument")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function abs expects an argument of numeric type")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function length expects one argument")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Function length expects an argument of type bulkdata")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "This function expects two arguments")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "This function expects first argument of type map")
			assertError(result, EXPRESSION_FUNCTION_CALL, null, "Second argument does not conform to the key type of the map")
	}
}