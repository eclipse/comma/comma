/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.comma.behavior.interfaces.validation.InterfaceDefinitionValidator
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*
import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)

class EventsValidatorTest {

	@Inject extension ParseHelper<InterfaceDefinition> parseHelper

	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	@Test
	def void duplicateStatesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			timing constraints
			TR1 in state Initial, Initial command b -[.. 0.2 ms]-> reply
			''', set)
		assertWarning(result, EVENT_IN_STATE, null, "Duplicated state")
	}

	@Test
	def void erroneousEventCallTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {
					transition do: s(true)
					next state: S
				}
			}
			''', set)
		assertError(result, EVENT_CALL, null, "Only notifications are allowed as outgoing events")
	}

	@Test
	def void erroneousReplyTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {
					transition trigger: s(bool b) do: reply
					next state: S

					transition trigger: c(int i) do: reply(*) to command c
					next state: S

					transition do: reply(*) newPerson(*)
					next state: S
				}
			}
			''', set)
		assertError(result, ACTION_LIST, InterfaceDefinitionValidator.REPLY_NOT_POSSIBLE, "Reply cannot be used for triggers that are notifications or signals.")
		assertError(result, COMMAND_REPLY, null, "Reply cannot indicate a command in this transition.")
		assertError(result, ACTION_LIST, InterfaceDefinitionValidator.REPLY_NOT_POSSIBLE, "Reply cannot be used in a non-triggered transition")
	}
}
