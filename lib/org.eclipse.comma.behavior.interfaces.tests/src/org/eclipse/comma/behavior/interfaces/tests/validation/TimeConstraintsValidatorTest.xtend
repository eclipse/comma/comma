/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.comma.behavior.validation.TimeConstraintsValidator
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class TimeConstraintsValidatorTest {
	@Inject extension ParseHelper<InterfaceDefinition> parseHelper

	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	@Test
	def erroneousPeriodJitterTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"
			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}
			timing constraints

			TR1 command b then notification newPerson with period 2.0 + 1.0 ms jitter 5 ms
			''', set)
		assertError(result, PERIODIC_EVENT, null, "The value of period must be a positive real constant.")
		assertError(result, PERIODIC_EVENT, null, "The value of jitter must be a positive real constant.")
	}

	@Test
	def boundsTimeIntervalTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"
			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			timing constraints
			TR1 command b -[12.0 ms .. 10.0 ms]-> reply
			''', set)
		assertError(result, TIME_INTERVAL, null, "Lower bound should be less than the upper bound.")
	}

	@Test
	def erroneousBoundsTimeIntervalTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"
			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			timing constraints
			TR1 command b -[12.0 + 1.0 ms .. 10 ms]-> reply
			''', set)
		assertError(result, TIME_INTERVAL, null, "The begin of the interval must be a positive real constant.")
		assertError(result, TIME_INTERVAL, null, "The end of the interval must be a positive real constant.")
	}

	@Test
	def erroneousIntervalInGroupTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"
			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			timing constraints
			group TR1
			command c -> absent reply in [2.0 ms .. 10.0 ms]
			-[.. 23.0 ms]-> notification newPerson
			end group
			''', set)
		assertError(result, CONDITIONED_ABSENCE_OF_EVENT, TimeConstraintsValidator.GROUP_CONSTRAINT_MISING_END, "The interval used in this scenario must be in the form [.. RealConst ms].")
	}

	@Test
	def duplicateConstraintNamesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)
		val result = parseHelper.parse('''
			import "iA.signature"
			interface iA

			machine StateMachine {
				initial state Initial {
					transition trigger: b(int j, int k, int l)
						do: reply(*, *, *)
					next state: Initial
				}
			}

			timing constraints
			TR1 command b -[ .. 10 ms]-> reply
			TR1 command b -[1.0 ms .. 10 ms]-> reply
			''', set)
		assertError(result, SINGLE_TIME_CONSTRAINT, TimeConstraintsValidator.TIME_CONSTRAINT_DUPLICATE, "Duplicate time constraints name")
	}
}
