/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.actions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))

class ActionsEventsTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {
					transition do:
						newPerson(*)*
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR reply(X) to command c where X > 0
			''', URI.createURI("iA.interface"), set)
		assertNoErrors(result)
	}

	@Test
	def void erroneousParamsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {
					transition do:
						newPerson(*, 1)*
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR reply(X) to command c where X > 0
			''', URI.createURI("iA.interface"), set)
		assertError(result, INTERFACE_EVENT_INSTANCE, null, "The number of parameters does not match.")
	}

	@Test
	def void erroneousParamTypeTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {
					transition do:
						newPerson(1)*
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR reply(X) to command c where X > 0
			''', URI.createURI("iA.interface"), set)
		assertError(result, INTERFACE_EVENT_INSTANCE, null, "The type of the expression must match the type in the signature.")
	}

	@Test
	def void erroneousMultiplicityTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {
					transition do:
						newPerson(*)[*]
						newPerson(*)[2-1]
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR reply(X) to command c where X > 0
			''', URI.createURI("iA.interface"), set)
		assertError(result, MULTIPLICITY, null, "Upper bound cannot be 0")
		assertError(result, MULTIPLICITY, null, "Lower bound cannot be greater than the upper bound")
	}

	@Test
	def void erroneousVariableUseTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine StateMachine {

				initial state Initial {
					transition do:
						newPerson(*)*
					next state: Initial
				}
			}

			data constraints
			variables
			int X

			DR reply(X + 1) to command c where X > 0
			''', URI.createURI("iA.interface"), set)
		assertError(result, EVENT_PATTERN, null, "Parameter cannot be an expression containing variables.")
	}
}