/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))
class ExpressionTypeCheckerVectorsMapsTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctVectorsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				int[] i
				int[2] j
				int[][] k
				int[2][] l
				int[][2] m
				Person[] p
				Students s
			init
				i := <int[]>[]
				i := <int[]>[1, 2, 3]
				j := <int[2]>[1, 2]
				k := <int[][]>[<int[]>[1], i]
				l := <int[2][]>[i, i]
				m := <int[][2]>[j]
				p := <Student[]>[Student{name = "", age = 18, sNumber = 234}]
				s := <Students>[Student{name = "", age = 18, sNumber = 234}]

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertNoErrors(result)
	}

	@Test
	def void erroneousVectorAssigmentTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int[] i

			init
			i := <real[]>[1.0]
			i := <int[][]>[]
			i := <int[2]>[1, 2]

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, ASSIGNMENT_ACTION, null, "Type mismatch: actual type does not match the expected type")
	}

	@Test
	def void erroneousVectorTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int[2] j

			init

			j := <int>[]
			j := <int[2]>[1]
			j := <int[2]>[1, 2.0]

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, TYPE_ANNOTATION, null, "The type must be a vector type")
			assertError(result, EXPRESSION_VECTOR, null, "Expected size of the vector is 2")
			assertError(result, EXPRESSION_VECTOR, null, "The element does not conform to the base type")
	}

	@Test
	def void erroneousMapExprTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			map<int, int> m

			init
			m := <int>{}

			machine main {
				initial state Idle {}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, TYPE_ANNOTATION, null, "The type must be a map type")
	}

	@Test
	def void erroneousMapUpdateTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			map<int, int> m

			init
			m := m[1 -> 2.0]

			machine main {
				initial state Idle {}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_MAP_RW, null, "Type of expression does not conform to map value type")
	}

	@Test
	def void erroneousMapReadTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			map<int, int> m

			init
			m := m[1 -> m[true]]

			machine main {
				initial state Idle {}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, EXPRESSION_MAP_RW, null, "Type of expression does not conform to map key type")
	}
}