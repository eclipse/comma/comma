/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.comma.behavior.validation.StateMachineValidator
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class StateMachineValidatorTest {

	@Inject extension ParseHelper<InterfaceDefinition> parseHelper

	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	/*
	 * Tests for validation rules in Behavior language
	 */

	@Test
	def void noInitialStateTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				state S {}
			}
			''', set)
		assertError(result, STATE_MACHINE, StateMachineValidator.STATEMACHINE_MISSING_INITIAL_STATE, "The state machine does not have an initial state.")
	}

	@Test
	def void manyInitialStatesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S1 {}

				initial state S2 {}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, STATE_MACHINE, StateMachineValidator.STATEMACHINE_DUPLICATE_INITIAL_STATE, "The state machine has more than one initial state.")
	}

	@Test
	def void unreachableStateTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S1 {}
				state S2 {}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, STATE, null, "Unreachable state")
	}

	@Test
	def void warningDuplicateTransitionsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				in all states {
					transition trigger: c(int i) do: reply(*) next state: S1
				}

				initial state S1 {
					transition trigger: c(int i) do: reply(*) next state: S1
					transition trigger: c(int i) do: reply(*) next state: S1
				}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, STATE, null, "Duplicate of a local transition")
		assertWarning(result, STATE, null, "Duplicate of a transition from in_all_states block")
	}

	@Test
	def void warningDuplicateTransitionsWithClausePermutationTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S1 {
					transition trigger: c(int i)
						do: reply(1) next state: S1
					OR
						do: reply(2) next state: S1

					transition trigger: c(int i)
						do: reply(2) next state: S1
					OR
						do: reply(1) next state: S1
				}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, STATE, null, "Duplicate of a local transition")
	}

	@Test
	def void overlappingClausesInSameStateTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S1 {
					transition trigger: c(int i)
						do: reply(1) next state: S1
					OR
						do: reply(2) next state: S1

					transition trigger: c(int i)
						do: reply(2) next state: S1
				}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, TRANSITION, null, "Clause duplicates another clause in a different transition in the same state")
	}

	@Test
	def void overlappingClausesFromInAllStatesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				in all states {
					transition trigger: c(int i)
						do: reply(2) next state: S1
				}

				initial state S1 {
					transition trigger: c(int i)
						do: reply(1) next state: S1
					OR
						do: reply(2) next state: S1
				}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, TRANSITION, null, "Clause duplicates another clause in a transition from in_all_states block")
	}

	@Test
	def void overlappingClausesFromDifferentInAllStatesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				in all states {
					transition trigger: c(int i)
						do: reply(2) next state: S1
				}

				in all states {
					transition trigger: c(int i)
						do: reply(1) next state: S1
				OR
						do: reply(2) next state: S1
				}

				initial state S1 {}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, STATE, null, "Duplicate clauses in transitions from two different in_all_states blocks")
	}

	@Test
	def void overlappingClausesInAllStatesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				in all states {
					transition trigger: c(int i)
						do: reply(2) next state: S1

					transition trigger: c(int i)
						do: reply(1) next state: S1
				OR
						do: reply(2) next state: S1
				}

				initial state S1 {}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, TRANSITION, null, "Clause duplicates another clause in a different transition in the same block")
	}
}
