/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))
class ExpressionTypeCheckerRecordsTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctRecordsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				Person p
				Student s
				int i
			init
				p := Person {name = "", age = 10}
				s := Student {name = "", age = 10, sNumber = 123}
				p:= s
				i := p.age
				p.age := i


			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertNoErrors(result)
	}

	@Test
	def void erroneousRecordAssignmentTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				Person p
				Student s
			init
				s := p

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, ASSIGNMENT_ACTION, null, "Type mismatch: actual type does not match the expected type")
	}

	@Test
	def void erroneousRecordFieldsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				Person p
				Student s
			init
				s := Student{name = "", age = 23}

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, EXPRESSION_RECORD, null, "Wrong number of fields")
	}

	@Test
	def void erroneousFieldNameTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				Person p
				Student s
			init
				s := Student{nam = "", age = 23, sNumber = 12}

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, FIELD, "org.eclipse.xtext.diagnostics.Diagnostic.Linking", "Couldn't resolve reference to RecordField 'nam'.")
	}

	@Test
	def void duplicateFieldNameTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				Person p
				Student s
			init
				s := Student{name = "", name = "", sNumber = 12}

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, EXPRESSION_RECORD, null, "Wrong field name")
	}

	@Test
	def void erroneousFieldValueTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				Person p
				Student s
			init
				s := Student{name = "", age = 23, sNumber = 12.0}

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, EXPRESSION_RECORD, null, "Type mismatch")
	}

	@Test
	def void erroneousAccessTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
				int i
				Student s
			init
				i := i.name

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);
			assertError(result, EXPRESSION_RECORD_ACCESS, "org.eclipse.xtext.diagnostics.Diagnostic.Linking", "Couldn't resolve reference to RecordField 'name'.")
	}
}