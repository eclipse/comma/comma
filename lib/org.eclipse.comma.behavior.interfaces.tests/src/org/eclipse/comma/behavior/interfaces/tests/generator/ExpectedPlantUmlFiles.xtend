/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.generator

class ExpectedPlantUmlFiles {
	
def static cameraSpecsmCameraSpec_camera_noselftransplantuml() '''
@startuml

[*] -> Off: /-
Off --> SwitchingOn: ICamera::PowerOn/reply(Status::OnOK) 
SwitchingOn --> Off: ICamera::PowerOff/reply 
SwitchingOn --> On: /ICamera::CameraStatus(Status::OnOK) 
SwitchingOn --> Off: /ICamera::CameraStatus(Status::OnFailed) 
SwitchingOn --> BatteryLow: /ICamera::LowBattery 
On --> TakingPicture: ICamera::Click/reply 
On --> Off: ICamera::PowerOff/reply 
On --> BatteryLow: /ICamera::LowBattery 
TakingPicture --> On: /count := count + 1; ICamera::PictureTaken 
TakingPicture --> Off: ICamera::PowerOff/reply 
TakingPicture --> BatteryLow: /ICamera::LowBattery 
BatteryLow --> Off: /ICamera::EmptyBattery 

@enduml
'''

def static cameraSpecsmCameraSpec_camera_noactions_noselftransplantuml() '''
@startuml

[*] -> Off
Off --> SwitchingOn: ICamera::PowerOn/reply(Status::OnOK)
SwitchingOn --> Off: ICamera::PowerOff/reply
SwitchingOn --> On: /ICamera::CameraStatus(Status::OnOK)
SwitchingOn --> Off: /ICamera::CameraStatus(Status::OnFailed)
SwitchingOn --> BatteryLow: /ICamera::LowBattery
On --> TakingPicture: ICamera::Click/reply
On --> Off: ICamera::PowerOff/reply
On --> BatteryLow: /ICamera::LowBattery
TakingPicture --> On: /ICamera::PictureTaken
TakingPicture --> Off: ICamera::PowerOff/reply
TakingPicture --> BatteryLow: /ICamera::LowBattery
BatteryLow --> Off: /ICamera::EmptyBattery

@enduml
'''

def static cameraSpecsmCameraSpec_camera_noactionsplantuml() '''
@startuml

[*] -> Off
Off --> SwitchingOn: ICamera::PowerOn/reply(Status::OnOK)
Off --> Off: ICamera::PowerOn/reply(Status::OnFailed)
SwitchingOn --> Off: ICamera::PowerOff/reply
SwitchingOn --> On: /ICamera::CameraStatus(Status::OnOK)
SwitchingOn --> Off: /ICamera::CameraStatus(Status::OnFailed)
SwitchingOn --> BatteryLow: /ICamera::LowBattery
On --> TakingPicture: ICamera::Click/reply
On --> On: ICamera::GetPictureNumber[count > 0]/reply(count)
On --> Off: ICamera::PowerOff/reply
On --> BatteryLow: /ICamera::LowBattery
TakingPicture --> On: /ICamera::PictureTaken
TakingPicture --> Off: ICamera::PowerOff/reply
TakingPicture --> BatteryLow: /ICamera::LowBattery
BatteryLow --> BatteryLow: /ICamera::LowBattery
BatteryLow --> Off: /ICamera::EmptyBattery

@enduml
'''

def static cameraSpecsmCameraSpec_camera_completeplantuml() '''
@startuml

[*] -> Off: /-
Off --> SwitchingOn: ICamera::PowerOn/reply(Status::OnOK) 
Off --> Off: ICamera::PowerOn/reply(Status::OnFailed) 
SwitchingOn --> Off: ICamera::PowerOff/reply 
SwitchingOn --> On: /ICamera::CameraStatus(Status::OnOK) 
SwitchingOn --> Off: /ICamera::CameraStatus(Status::OnFailed) 
SwitchingOn --> BatteryLow: /ICamera::LowBattery 
On --> TakingPicture: ICamera::Click/reply 
On --> On: ICamera::GetPictureNumber[count > 0]/reply(count) 
On --> Off: ICamera::PowerOff/reply 
On --> BatteryLow: /ICamera::LowBattery 
TakingPicture --> On: /count := count + 1; ICamera::PictureTaken 
TakingPicture --> Off: ICamera::PowerOff/reply 
TakingPicture --> BatteryLow: /ICamera::LowBattery 
BatteryLow --> BatteryLow: /ICamera::LowBattery 
BatteryLow --> Off: /ICamera::EmptyBattery 

@enduml
'''

def static cameraSpecsmCameraSpec_TR5plantuml() '''
@startuml
title Timing Rule TR5
Client //- Server: notification ICamera_LowBattery
loop period 750.0 ms and jitter 50.0 ms
Client //- Server: notification ICamera_LowBattery
end
...until...
Client //- Server: notification ICamera_EmptyBattery
@enduml
'''

def static cameraSpecsmCameraSpec_TR4plantuml() '''
@startuml
title Timing Rule TR4
Client -> Server: command ICamera_PowerOn
alt the following event is observed
...within [1.0 ms .. 4.0 ms ] ...
Server --> Client: reply (Status::OnFailed)
else event is not observed -> rule is not triggered
...other events...
end
@enduml
'''

def static cameraSpecsmCameraSpec_TR3plantuml() '''
@startuml
title Timing Rule TR3
Client -> Server: command ICamera_PowerOn
alt the following event is observed
...within [1.0 ms .. 3.0 ms ] ...
Server --> Client: reply (Status::OnOK)
else event is not observed -> rule is not triggered
...other events...
end
@enduml
'''

def static cameraSpecsmCameraSpec_TR2plantuml() '''
@startuml
title Timing Rule TR2
Client -> Server: command ICamera_Click
... within [10.0 ms .. 100.0 ms ] ...
Client //- Server: notification ICamera_PictureTaken
@enduml
'''

def static cameraSpecsmCameraSpec_TR1plantuml() '''
@startuml
title Timing Rule TR1
Client -> Server: command ICamera_PowerOff
... within [ .. 32.0 ms ] ...
Server --> Client: reply 
@enduml
'''


}
