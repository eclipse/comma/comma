/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.util.StringInputStream

/*
 * This class contains all imported interfaces that are used from the ValidatorTest classes
 */
@InjectWith(MultiLangInjectorProvider)
class ImportedInterfaces {

	def static importIWorker(ResourceSet set) {
		val res = set.createResource(URI.createURI("Interface.signature"));
		res.load(new StringInputStream('''
			signature IWorker
				types
				enum Status {
					STARTED
					STOPPED
				}
				enum Activity {
					WORKING
					SLEEPING
				}
				
				record Record {
					string key, 
					int value
				}
			
				commands
				Status start
			
				signals
				sleep
				wakeup
				stop
				sum(int a, int b)
				sum2(Record a, int b)
			
				notifications
				status(Activity a)
				startedWorking
			
		'''), emptyMap);
	}

}
