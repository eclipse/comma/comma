/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.expressions

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(typeof(MultiLangInjectorProvider))
class ExpressionTypeCheckerPrimitiveTypesTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject
	ParseHelper<InterfaceDefinition> parseHelper

	@Test
	def void correctSimpleTypesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			wstring s
			wstring t
			long l
			ulong ul
			int i
			bulkdata bd

			init

			s := t
			l := 1
			i := l
			l := i
			ul := l
			l := ul
			bd := Bulkdata<>
			bd := Bulkdata<2>

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertNoErrors(result)
	}

	@Test
	def void erroneousTypeWithNoBaseTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			wstring s

			init

			s := "t"

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, ASSIGNMENT_ACTION, null, "Type mismatch: actual type does not match the expected type")
	}

	@Test
	def void erroneousTypeWithBaseTest(){
		val set = resourceSetProvider.get()
		ImportLoader.expressionTypeCheckerSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			long l

			init

			l := 2.0

			machine main {
				initial state Idle {
					transition trigger: c
						do: reply
						next state: Idle
				}
			}
			''', URI.createURI("Statemachine.interface"), set);

			assertError(result, ASSIGNMENT_ACTION, null, "Type mismatch: actual type does not match the expected type")
	}
}