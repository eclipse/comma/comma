/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.formatter

import com.google.inject.Inject
import org.eclipse.comma.behavior.interfaces.tests.InterfaceDefinitionInjectorProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceDefinitionInjectorProvider)
class StatemachineFormatterTest {

	@Inject extension FormatterTestHelper

	//Remove this when the normal tests are adapted
	//The following is put just to avoid an empty class
	@Test def void dummyTest(){
		assertFormatted[
			expectation = '''
				import "Interface.signature"

				interface IComputer

				machine main {

					initial state Idle {

						transition trigger: interfaceEvent
							guard: true
								do:
								reply(*)
								next state: NextState
					}
				}
			'''

			toBeFormatted = '''
				import "Interface.signature"
				interface IComputer
				machine main{
					initial state Idle {
						transition trigger: interfaceEvent
										guard: true
										do:
										reply(*)
						next state: NextState
					}
				}
			'''

		]
	}

	//TODO adapt the next two after the new formatter is done
////	@Test def void formatStatemachineGeneric() {
////		assertFormatted[
////			expectation = '''
////				import "Interface.signature"
////
////				interface IComputer
////
////					variables
////					int arg1
////					int arg2
////
////				machine computer {
////
////					in all states {
////
////						transition trigger: interfaceEvent
////							guard: true
////								do:
////								reply(*)
////								next state: NextState
////					}
////
////					initial state Idle {
////
////						transition trigger: sum(int a, int b)
////							do: arg1 := a
////							arg2 := b
////							next state: Computing
////					}
////
////					state Computing {
////
////				// * is returned on purpose in order to use a data rule to express the constraint that result is the sum of the arguments
////						transition
////								do: error
////								next state: Idle
////							OR
////								do: calculating
////								next state: Computing
////							OR
////								do: result(*)
////								next state: Idle
////					}
////				}
////
////				machine log {
////
////					initial state Init {
////
////						transition trigger: length
////							do:
////							reply(*)
////							next state: Init
////
////						transition trigger: empty
////							next state: Emptying
////					}
////
////					state Emptying {
////
////						transition
////							do: loggedError
////							next state: Emptying
////
////						transition
////							do: logEmpty
////							next state: Init
////					}
////				}
////
////				data constraints
////				variables
////				int X
////				int Y
////				int Z // The result is the sum of the arguments
////
////				DR1 signal sum(X, Y);no [signal sum] until notification result(Z) where (X + Y) == Z
////				//if after emptying the log and observing at least one error then the first length shall return a positive value
////				DR2 notification logEmpty;no [signal empty] until notification error;
////				no [signal empty] until reply(X) to command length where X >= 1
////				//If the log is emptied and no errors are observed the first length returns 0
////				DR3 notification logEmpty;no [notification error] until reply(Y) to command length where Y == 0
////
////				generic constraints
////
////				variables
////				int X
////				int Y
////				int Z
////				int T
////				real t1
////				real t2
////				real t3
////				real t4
////				real t5
////				real t6
////				real t7
////				real t8 //After log is reported empty the number of observed errors is the same as the number of the reported errors when empty is requested
////				// GR1  <    t3  , !notification error > until < t4, signal empty >; < t5, Y, notification loggedError > or < t6, !notification loggedError > until < t7, notification logEmpty > where X == Y
////				//After reporting the length of the log and some errors are observed and empty is not observed until the next length, the two reported values are OK
////				GR2 < t3, !signal empty > until < t4, reply(T) to command length > where T == (X + Z)
////				//If length is reported and then no errors and empty happened, the length should return the same result
////				GR3 < t7, notification logEmpty > until < t8, reply(T) to command length > where (T == Z)
////
////			'''
////			toBeFormatted = '''
////				import "Interface.signature" interface IComputer 	variables	int arg1
////					int arg2 machine computer { in all states { transition trigger: interfaceEvent		guard: true		do:
////				reply(*)		next state: NextState }		initial state Idle {	transition trigger: sum(int a, int b)
////					do: arg1 := a		arg2 := b	next state: Computing	}state Computing {
////				// * is returned on purpose in order to use a data rule to express the constraint that result is the sum of the arguments
////						transition		do: error	next state: Idle	OR		do: calculating	next state: Computing		OR
////					do: result (*)		next state: Idle	}}
////				machine log {	initial state Init {	transition trigger: length	do:	reply(*)	next state: Init
////					transition trigger: empty	next state: Emptying  }	state Emptying {	transition
////					do: loggedError		next state: Emptying	transition	do: logEmpty	next state: Init
////					}	}		data constraints	variables	int X		int Y		int Z		//The result is the sum of the arguments
////				DR1 signal sum (X, Y) ;no [signal sum] until notification result (Z) where (X + Y) == Z
////				//if after emptying the log and observing at least one error then the first length shall return a positive value
////				DR2 notification logEmpty ;no [signal empty] until notification error;
////				no [signal empty] until reply (X) to command length where X >= 1
////				//If the log is emptied and no errors are observed the first length returns 0
////				DR3 notification logEmpty ;no [notification error] until reply (Y) to command length where Y == 0
////				generic constraints	variables int X		int Y	int Z		int T	real t1		real t2		real t3
////				real t4		real t5		real t6		real t7		real t8 	//After log is reported empty the number of observed errors is the same as the number of the reported errors when empty is requested
////				 //    GR1  <    t3  , !notification error > until < t4, signal empty >; < t5, Y, notification loggedError > or < t6, !notification loggedError > until < t7, notification logEmpty > where X == Y
////				//After reporting the length of the log and some errors are observed and empty is not observed until the next length, the two reported values are OK
////				GR2   <  t3   , !    signal empty     >  until  <  t4  ,  reply   ( T )   to  command   length  >  where   T ==    (  X   +   Z )
////				//If length is reported and then no errors and empty happened, the length should return the same result
////				GR3  < t7, notification logEmpty > until < t8, reply (T) to command length > where (T == Z)
////
////			'''
////		]
//	}
//
//	@Test def void formatStatemachineTiming() {
//		assertFormatted[
//			expectation = '''
//				import "IMachine.signature"
//
//				interface IMachine
//
//					variables
//					bool rejectForced
//					bool pongPending
//					bool failurePending
//					SoftwareInfo current
//
//					init
//					rejectForced := false
//					pongPending := false
//					failurePending := false
//					current := SoftwareInfo {versionNumber = "1.0.0", date = "21/12/2017"}
//
//				machine Main {
//
//					initial state Off {
//
//						transition trigger: start
//								do:
//								reply(Status::SUCCESS)
//								next state: On
//							OR
//								do:
//								reply(Status::	FAIL)
//								next state: Off
//
//						transition trigger: getStatus
//							do:
//							reply(WorkingStatus::OFF)
//							next state: Off
//
//						transition trigger: getTestStatus
//							do:
//							reply(InternalStatus::OFF)
//							next state: Off
//					}
//
//					state On {
//
//						transition trigger: stop
//							do:
//							reply
//							next state: Off
//
//						transition trigger: registerPerson(Person p)
//							do: if rejectForced then
//								rejectForced := false
//								reply(RegistrationStatus::NOT_REGISTERED)
//							else
//								reply(RegistrationStatus::REGISTERED)
//							fi
//							next state: On
//
//						transition trigger: registerProduct(Product p)
//							guard: rejectForced
//								do: rejectForced := false
//								reply(RegistrationStatus::NOT_REGISTERED)
//								next state: On
//
//						transition trigger: registerProduct(Product p)
//							guard: NOT rejectForced
//								do:
//								reply(RegistrationStatus::REGISTERED)
//								next state: On
//
//						transition trigger: registerService(Service s)
//							guard: rejectForced
//								do: rejectForced := false
//								reply(RegistrationStatus::NOT_REGISTERED)
//								next state: On
//
//						transition trigger: registerService(Service s)
//							guard: NOT rejectForced
//								do:
//								reply(RegistrationStatus::REGISTERED)
//								next state: On
//
//						transition trigger: getStatus
//							do:
//							reply(WorkingStatus::ON)
//							next state: On
//
//						transition trigger: getTestStatus
//							do:
//							reply(InternalStatus::ON)
//							next state: On
//
//						transition trigger: ping(int cookie)
//							guard: NOT pongPending
//								do: pongPending := true
//								next state: On
//
//						transition
//							guard: pongPending
//								do: pong(*)
//								pongPending := false
//								next state: On
//
//						transition trigger: Inject(Stimuli s)
//							guard: s == Stimuli::SIM_ERROR
//								do: failurePending := true
//								reply(Status  ::  SUCCESS)
//								next state: On
//
//						transition trigger: Inject(Stimuli s)
//							guard: s == Stimuli::SIM_REJECT
//								do: if NOT rejectForced then
//									rejectForced := true
//									reply(Status::SUCCESS)
//								else
//									reply(Status::FAIL)
//								fi
//								next state: On
//
//						transition
//							guard: failurePending
//								do: failurePending := false
//								failure(*)
//								next state: Error
//					}
//
//					state Error {
//
//						transition trigger: Inject(Stimuli s)
//							guard: s == Stimuli::SIM_ERROR
//								do:
//								reply(Status::FAIL)
//								next state: Error
//
//						transition trigger: Inject(Stimuli s)
//							guard: s == Stimuli::SIM_REJECT
//								do:
//								reply(Status::FAIL)
//								next state: Error
//
//						transition trigger: getStatus
//							do:
//							reply(WorkingStatus::ON)
//							next state: Error
//
//						transition trigger: getTestStatus
//							do:
//							reply(InternalStatus::ERROR)
//							next state: Error
//
//						transition trigger: stop
//							do:
//							reply
//							next state: Off
//
//						transition trigger: ping(int cookie)
//							guard: NOT pongPending
//								do: pongPending := true
//								next state: Error
//
//						transition
//							guard: pongPending
//								do: pong(*)
//								pongPending := false
//								next state: Error
//					}
//				}
//
//				machine Service {
//
//					initial state Idle {
//
//						transition trigger: getSoftwareInfo
//							do:
//							reply(*)
//							next state: Idle
//
//						transition trigger: updateSoftware(SoftwareInfo newSoftware)
//								do: current := newSoftware
//								reply(Status::SUCCESS)
//								next state: Idle
//							OR
//								do:
//								reply(Status::FAIL)
//								next state: Idle
//					}
//				}
//
//				timing constraints
//
//				/*
//				 * ping command is periodically sent after successful start
//				 * until stop
//				 */
//				TR1 reply(Status::SUCCESS) to command start then signal ping with period 200.0 ms jitter 20.0 ms until command stop
//
//				/*
//				 * The time for sending pong after ping is 30 msecs
//				 */
//				TR2 signal ping - [ .. 30.0 ms ] -> notification pong
//
//				data constraints
//				variables
//				int X
//				int Y
//				SoftwareInfo s1
//				SoftwareInfo s2
//
//				/*
//				 * This constraint captures the cookie answer logic.
//				 * If cookie < 20 then cookie is incremented
//				 * If cookie = 20 then 0				 */
//				DR1
//				signal ping(X);any event until notification pong(Y) where ((X != 20) OR (Y == 0)) AND ((X == 20) OR (Y == (X + 1)))
//				/*
//				 * Getting two times info about the installed software without a change between the calls
//				 * shall produce the same results				 */
//				DR2
//				reply(s1) to command getSoftwareInfo;no [command updateSoftware] until reply(s2) to command getSoftwareInfo where (s1.date == s2.date) AND
//				(s1.versionNumber == s2.versionNumber)
//
//				/*
//				 * Getting two times info about the installed software without a successful change between the calls
//				 * shall produce the same results				 */
//				DR3
//				reply(s1) to command getSoftwareInfo;no [reply(Status::SUCCESS) to command updateSoftware] until reply(s2) to command getSoftwareInfo where (s1.date == s2.date) AND
//				(s1.versionNumber == s2.versionNumber)
//
//			'''
//			toBeFormatted = '''
//				import "IMachine.signature"
//
//				interface IMachine
//
//					variables
//					bool rejectForced
//					bool pongPending
//					bool failurePending
//					SoftwareInfo current
//
//					init
//					rejectForced := false
//					pongPending := false
//					failurePending := false
//					current := SoftwareInfo {versionNumber = "1.0.0", date = "21/12/2017"}
//
//				machine Main{
//					initial state Off {
//						transition trigger: start
//							do: reply(Status::SUCCESS) next state: On
//						OR
//							do:	 reply	(Status::	FAIL) next 	state: Off
//
//						transition trigger: getStatus do: reply(WorkingStatus::OFF) next state: Off
//
//						transition trigger: getTestStatus do: reply(InternalStatus::OFF) next state: Off
//					}
//
//
//					state On {						transition trigger: stop do: reply next state: Off
//
//					transition trigger: registerPerson(Person p) do:
//							if rejectForced then
//								rejectForced := false
//								reply(RegistrationStatus::NOT_REGISTERED)
//						else
//								reply(RegistrationStatus::REGISTERED)
//						fi							next state: On
//						transition trigger: registerProduct(Product p)
//							guard: rejectForced do:								rejectForced := false
//								reply(RegistrationStatus::NOT_REGISTERED)
//							next state: On
//
//						transition trigger: registerProduct(Product p)
//							guard: NOT rejectForced do:
//								reply(RegistrationStatus::REGISTERED)
//							next state: On
//
//						transition trigger: registerService(Service s)
//							guard: rejectForced do:
//								rejectForced := false
//								reply(RegistrationStatus::NOT_REGISTERED)
//							next state: On
//
//						transition trigger: registerService(Service s)
//							guard: NOT rejectForced do:
//								reply(RegistrationStatus::REGISTERED)
//							next state: On
//
//						transition trigger: getStatus do:
//							reply(WorkingStatus::ON)
//							next state: On
//
//						transition trigger: getTestStatus do: reply(InternalStatus::ON) next state: On
//
//						transition trigger: ping(int cookie) guard: NOT pongPending do:
//							pongPending := true
//							next state: On
//
//						transition guard: pongPending do:
//							pong(*)
//							pongPending := false
//							next state: On
//
//						transition trigger: Inject(Stimuli s) guard: s == Stimuli::SIM_ERROR do:
//							failurePending := true
//							reply(Status  ::  SUCCESS)							next state: On
//
//						transition trigger: Inject(Stimuli s) guard: s == Stimuli::SIM_REJECT do:
//							if NOT rejectForced then
//								rejectForced := true
//								reply(Status::SUCCESS)							else
//								reply(Status::FAIL)							fi
//							next state: On
//						transition guard: failurePending do:
//							failurePending := false							failure(*)						next state: Error
//					}
//
//					state Error {
//						transition trigger: Inject(Stimuli s) guard: s == Stimuli::SIM_ERROR do:
//							reply(Status::FAIL)							next state: Error
//
//						transition trigger: Inject(Stimuli s) guard: s == Stimuli::SIM_REJECT do:
//							reply(Status::FAIL)							next state: Error
//						transition trigger: getStatus do:
//							reply(WorkingStatus::ON)							next state: Error
//						transition trigger: getTestStatus do: reply(InternalStatus::ERROR) next state: Error
//
//						transition trigger: stop do: reply next state: Off
//						transition trigger: ping(int cookie) guard: NOT pongPending do:
//							pongPending := true							next state: Error
//						transition guard: pongPending do:
//							pong(*)
//							pongPending := false
//							next state: Error
//
//					}
//
//				}
//
//				machine Service {
//
//					initial state Idle {						transition trigger: getSoftwareInfo do: reply(*)
//						next state: Idle
//						transition trigger: updateSoftware(SoftwareInfo newSoftware)
//							do:							current := newSoftware							reply(Status::SUCCESS)							next state: Idle						OR					do:
//							reply(Status::FAIL)				next state: Idle
//					}
//				}
//
//				timing constraints
//
//				/*
//				 * ping command is periodically sent after successful start
//				 * until stop
//				 */
//
//				 TR1  				 reply(Status::SUCCESS) to command start
//				 then
//				 signal   ping    with   period 200.0 ms jitter 20.0 ms
//				 until
//				 command stop
//
//				 /*
//				  * The time for sending pong after ping is 30 msecs
//				  */
//				  TR2				  signal ping   -   [   ..30.0    ms   ]  ->   notification  pong
//
//				data constraints
//				  variables				int X				int Y
//				  SoftwareInfo s1
//				SoftwareInfo s2
//
//				/*
//				 * This constraint captures the cookie answer logic.
//				 * If cookie < 20 then cookie is incremented
//				 * If cookie = 20 then 0				 */
//				DR1
//				signal ping(X);				any event
//				until				notification pong(Y)
//				where				((X != 20) OR (Y == 0))
//				AND
//				((X == 20) OR (Y == (X + 1)) )
//				/*
//				 * Getting two times info about the installed software without a change between the calls
//				 * shall produce the same results				 */
//				DR2
//				reply(s1) to command getSoftwareInfo;
//				no [command updateSoftware]				until
//				reply(s2) to command getSoftwareInfo				where
//				(s1.date == s2.date) AND (s1.versionNumber == s2.versionNumber)
//
//				/*
//				 * Getting two times info about the installed software without a successful change between the calls
//				 * shall produce the same results				 */
//				DR3
//				reply(s1) to command getSoftwareInfo;				no [reply(Status::SUCCESS) to command updateSoftware]
//				until				reply(s2) to command getSoftwareInfo				where				(s1.date == s2.date) AND (s1.versionNumber == s2.versionNumber)
//
//			'''
//		]
//	}
}
