/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.tests.validation

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.tests.ImportLoader
import org.eclipse.comma.behavior.interfaces.tests.MultiLangInjectorProvider
import org.eclipse.comma.behavior.interfaces.validation.InterfaceDefinitionValidator
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

import static org.eclipse.comma.actions.actions.ActionsPackage.Literals.*
import static org.eclipse.comma.behavior.behavior.BehaviorPackage.Literals.*
import static org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinitionPackage.Literals.*
import static org.eclipse.comma.expressions.expression.ExpressionPackage.Literals.*

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class InterfaceBehaviorValidatorTest {

	@Inject extension ParseHelper<InterfaceDefinition> parseHelper
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	/*
	 * Tests for validation rules in Behavior language
	 */

	@Test
	def void warningUninitializedVarsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA
			variables
			int i

			machine M {
				initial state S {}
			}
			''', URI.createURI("iA.interface"), set)
		assertWarning(result, VARIABLE, null, "Uninitialized variable.")
	}

	@Test
	def void duplicateSMNamesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M {
				initial state S {}
			}

			machine M {
				initial state S1 {}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, STATE_MACHINE, null, "Duplicate state machine name")
	}

	@Test
	def void duplicateStateNamesTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {}
			}

			machine M2 {
				initial state S {}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, STATE, null, "Duplicate state name")
	}

	@Test
	def void duplicateFragmentsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			event fragments
			A = reply
			A = reply

			machine M1 {
				initial state S {}
			}
			''', URI.createURI("iA.interface"), set)
		assertError(result, PC_FRAGMENT_DEFINITION, null, "Duplicate event fragment name")
	}

	@Test
	def void warningUnusedVarsTest(){
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			variables
			int i

			machine M1 {
				initial state S {}
			}
			''', set)
		assertWarning(result, INTERFACE, null, "Unused variable.")
	}

	/*
	 * Tests for validation rules in Interface Behavior language
	 */

	@Test
	def warningUnusedEventsTest() {
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {}
			}
			''', set)
		assertWarning(result, INTERFACE, InterfaceDefinitionValidator.INTERFACE_UNUSED_NOTIFICATION, "Unused notification newPerson")
		assertWarning(result, INTERFACE, InterfaceDefinitionValidator.INTERFACE_UNUSED_COMMAND, "Unused command b")
		assertWarning(result, INTERFACE, InterfaceDefinitionValidator.INTERFACE_UNUSED_SIGNAL, "Unused signal s")
	}

	@Test
	def nonDisjointTriggerSetsTest() {
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA

			machine M1 {
				initial state S {
					transition trigger: s(bool b) next state: S
				}
			}

			machine M2 {
				initial state T {
					transition trigger: s(bool b) next state: T
				}
			}
			''', set)
		assertError(result, INTERFACE, null, "Some events are used in more than one machine")
	}

	@Test
	def erroneousInterfaceNameTest() {
		val set = resourceSetProvider.get()
		ImportLoader.actionsSignature(set)

		val result = parseHelper.parse('''
			import "iA.signature"

			interface iA1

			machine M1 {
				initial state S {
					transition trigger: s(bool b) next state: S
				}
			}
			''', set)
		assertError(result, INTERFACE, null, "There is no visible signature with name equal to the interface name")
	}
}
