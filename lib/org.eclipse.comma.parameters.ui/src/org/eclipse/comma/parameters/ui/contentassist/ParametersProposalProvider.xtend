/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/*
 * generated by Xtext 2.25.0
 */
package org.eclipse.comma.parameters.ui.contentassist

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.jface.viewers.StyledString
import org.eclipse.comma.behavior.ui.contentassist.HtmlConfigurableCompletionProposal
import org.eclipse.swt.graphics.Image
import org.eclipse.core.runtime.Platform
import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.parameters.ParametersUtils
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.types.utilities.TypeUtilities

/**
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#content-assist
 * on how to customize the content assistant.
 */
class ParametersProposalProvider extends AbstractParametersProposalProvider {

	protected Image templateIcon;

	final int TEMPLATE_DEFAULT_PRIORITY = 600;

	new() {
		templateIcon = ImageDescriptor.createFromURL(Platform.getBundle("org.eclipse.comma.icons").getResource("icons/template.png"))
			.createImage();
	}
    
	override complete_TriggerParams(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.complete_Parameters(model, ruleCall, context, acceptor)
		val Parameters parameters = model as Parameters
		if (parameters.interface === null || parameters.interface.name === null) return;

		val itf = parameters.interface
		var String[] existingTriggerParams = #[]
		if (parameters.triggerParams !== null) {
			existingTriggerParams = parameters.triggerParams.map[t | t.event.name]
		}

		var proposal = ""
		val transitionStates = ParametersUtils.getTransitionStates(itf)
		for (entry : transitionStates.entrySet) {
			val params = entry.key.parameters.filter[p | p.direction != DIRECTION.OUT]
			if (!existingTriggerParams.contains(entry.key.name) && !params.empty) {
				proposal += '''
				trigger: «entry.key.name»
				«FOR state : entry.value»
				state: «state» params: ( «params.map[p | p.name.toUpperCase].join(", ")» )
				«ENDFOR»

				'''
			}
		}
		acceptor.accept(createProposal("Autocomplete missing triggers", proposal, "", context, TEMPLATE_DEFAULT_PRIORITY+200, -1, -1));
	}
	
	override complete_NotificationParams(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
	   super.complete_Parameters(model, ruleCall, context, acceptor)
       val Parameters parameters = model as Parameters
       if (parameters.interface === null || parameters.interface.name === null) return;

       val itf = parameters.interface
       var String[] existingNotificationParams = #[]
       if (parameters.notificationParams !== null) {
           existingNotificationParams = parameters.notificationParams.map[t | t.event.name]
       }

        var proposal = ""
        val notificationStates = ParametersUtils.getUncoveredEventStates(ParametersUtils.getUncoveredNotifications(itf, parameters))
        for (entry : notificationStates.entrySet) {
            if (!existingNotificationParams.contains(entry.key.name)) {
                proposal += '''
                notification: «entry.key.name»
                «FOR state : entry.value»
                state: «state» params: ( «entry.key.parameters.map[p | p.name.toUpperCase].join(", ")» )
                «ENDFOR»

                '''
            }
        }
        acceptor.accept(createProposal("Autocomplete missing notifications", proposal, "", context, TEMPLATE_DEFAULT_PRIORITY+100, -1, -1));   
	}

    override complete_ReplyParams(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
       super.complete_Parameters(model, ruleCall, context, acceptor)
       val Parameters parameters = model as Parameters
       if (parameters.interface === null || parameters.interface.name === null) return;

       val itf = parameters.interface
       var String[] existingReplyParams = #[]
       if (parameters.replyParams !== null) {
           existingReplyParams = parameters.replyParams.map[t | t.event.name]
       }

        var proposal = ""
        val replyStates = ParametersUtils.getUncoveredEventStates(ParametersUtils.getUncoveredReplies(itf, parameters))
        for (entry : replyStates.entrySet) {
            val params = entry.key.parameters.filter[p | p.direction !== DIRECTION.IN]
            if (!existingReplyParams.contains(entry.key.name)) {
                proposal += '''
                reply to: «entry.key.name»
                «FOR state : entry.value»
                state: «state» params: ( «params.map[p | p.name.toUpperCase].join(", ")»«IF !TypeUtilities.isVoid((entry.key as Command).type)»«IF !params.empty», «ENDIF»RETURN_VALUE«ENDIF» )
                «ENDFOR»

                '''
            }
        }
        acceptor.accept(createProposal("Autocomplete missing replies", proposal, "", context, TEMPLATE_DEFAULT_PRIORITY, -1, -1));   
    }

	private def createProposal(String name, String content, String additionalInfo, ContentAssistContext context,
		int priority, int selection, int length) {

		var finalAdditionalInfo = content
		val proposal = createHtmlCompletionProposal(content, new StyledString(name), templateIcon,
			TEMPLATE_DEFAULT_PRIORITY, context);

		if (proposal instanceof ConfigurableCompletionProposal) {
			while (finalAdditionalInfo.startsWith("\n") || finalAdditionalInfo.startsWith("\r")) {
				finalAdditionalInfo = finalAdditionalInfo.substring(1);
			}
			finalAdditionalInfo = "<html><body bgcolor=\"#FFFFE1\"><style> body { font-size:9pt; font-family:'Segoe UI' }</style><pre>" +
				finalAdditionalInfo + "</pre>";
			if (additionalInfo !== null) {
				finalAdditionalInfo = finalAdditionalInfo + "<p>" + additionalInfo + "</p>";
			}
			finalAdditionalInfo = finalAdditionalInfo + "</body></html>"
			proposal.additionalProposalInfo = finalAdditionalInfo
			proposal.proposalContextResource = context.resource
			proposal.priority = priority
			proposal.selectionStart = context.offset + selection
			proposal.selectionLength = length
		}
		proposal
	}

	private def createHtmlCompletionProposal(String proposal, StyledString displayString, Image image, int priority,
		ContentAssistContext context) {
		if (isValidProposal(proposal, context.getPrefix(), context)) {
			return doCreateHtmlCompletionProposal(proposal, displayString, image, priority, context);
		}
		return null;
	}

	private def doCreateHtmlCompletionProposal(String proposal, StyledString displayString, Image image, int priority,
		ContentAssistContext context) {
		val replacementOffset = context.getReplaceRegion().getOffset();
		val replacementLength = context.getReplaceRegion().getLength();
		val result = new HtmlConfigurableCompletionProposal(proposal, replacementOffset, replacementLength,
			proposal.length(), image, displayString, null, null);

		result.priority = priority
		result.matcher = context.matcher
		result.replaceContextLength = context.replaceContextLength
		result;
	}

}
