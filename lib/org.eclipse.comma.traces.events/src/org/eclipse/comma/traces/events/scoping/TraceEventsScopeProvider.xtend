/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.traces.events.scoping

import org.eclipse.comma.traces.events.traceEvents.Command
import org.eclipse.comma.traces.events.traceEvents.Event
import org.eclipse.comma.traces.events.traceEvents.Notification
import org.eclipse.comma.traces.events.traceEvents.Reply
import org.eclipse.comma.traces.events.traceEvents.Signal
import org.eclipse.comma.traces.events.traceEvents.TraceEventsPackage
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference

import static org.eclipse.xtext.scoping.Scopes.*

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class TraceEventsScopeProvider extends AbstractTraceEventsScopeProvider {
	override getScope(EObject context, EReference reference){
		if(context instanceof Command && reference == TraceEventsPackage.Literals.EVENT__COMMAND) 
			return scopeFor( (context as Event).interface.commands )
			
		if(context instanceof Signal && reference == TraceEventsPackage.Literals.EVENT__COMMAND) 
			return scopeFor( (context as Event).interface.signals )
		
		if(context instanceof Notification && reference == TraceEventsPackage.Literals.EVENT__COMMAND) 
			return scopeFor( (context as Event).interface.notifications )
			
		if(context instanceof Reply && reference == TraceEventsPackage.Literals.EVENT__COMMAND) 
			return scopeFor( (context as Event).interface.commands )
			
		return super.getScope(context, reference);
	}
}
