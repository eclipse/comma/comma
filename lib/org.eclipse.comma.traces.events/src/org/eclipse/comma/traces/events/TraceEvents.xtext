/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
grammar org.eclipse.comma.traces.events.TraceEvents hidden(WS, ML_COMMENT, SL_COMMENT)

generate traceEvents "https://www.eclipse.org/comma/traces/events/TraceEvents"

import "http://www.eclipse.org/emf/2002/Ecore" as ecore
import "https://www.eclipse.org/comma/signature/InterfaceSignature" as signature
import "https://www.eclipse.org/comma/behavior/component/Component" as component

TraceEvents: {TraceEvents}
	NL*
	imports += Import*
	('connections' NL+
	connections += Connection+)?
	('components' NL+
	components += Component+)?
	('errors' NL+)?
	('events'
	(NL NL+ events += Event)* NL*)?
;

Import:
	'import' importURI = STRING NL+
;

Connection:
	'('source = QN ',' 
	   sourcePort = ID ',' 
	   interface = QN ','
	   target = QN ','
	   targetPort = ID ')' NL+
;

Component:
	componentType = QN componentId = QN NL+
;

Event:
	Command | Reply | Signal | Notification
;

Command:
	(id = ID NL+)?
	'Command'
	EventData
	'End'
;

Reply:
	(id = ID NL+)?
	'Reply'
	EventData
	'End'
;

Notification:
	(id = ID NL+)?
	'Notification' 
	 EventData
	'End'
;

Signal:
	(id = ID NL+)?
	'Signal' 
	 EventData
	'End'
;


fragment EventData returns Event :
	(timeStamp = DATE_TIME | epochTime = FLOAT)
	 timeDelta = FLOAT
	 source = QN
	 sourcePort = ID
	 target = QN
	 targetPort = ID
	 interface = [signature::Signature | QN]
	 command = [signature::InterfaceEvent | ID] NL
	 parameters += Expression*
;

Expression: {Expression}
	tokens += (QN | STRING | FLOAT | NUMBER | NEG_NUMBER)+ NL
;

Dummy:
    component = [component::Component | QN]
;

QN: ID ('.'ID)*;

terminal NL:
	('\r')?'\n'
;

terminal FLOAT:
	(NUMBER | NEG_NUMBER) '.' NUMBER (('E' | 'e') (NUMBER | NEG_NUMBER))?
;

terminal DATE_TIME:
	NUMBER NEG_NUMBER NEG_NUMBER NEG_NUMBER ':' NUMBER ':' FLOAT
;

terminal NUMBER: ('0'..'9')+;

terminal NEG_NUMBER:
	'-' NUMBER
;

terminal ID: '^'?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

terminal STRING:
			'"' ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|'"') )* '"' |
			"'" ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|"'") )* "'"
		;
terminal ML_COMMENT : '/*' -> '*/';
terminal SL_COMMENT : '//' !('\n'|'\r')* ('\r'? '\n')?;

terminal WS         : (' '|'\t')+;

terminal ANY_OTHER: .;