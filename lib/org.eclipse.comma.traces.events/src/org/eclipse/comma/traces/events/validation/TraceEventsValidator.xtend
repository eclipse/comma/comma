/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.traces.events.validation

import com.google.inject.Inject
import java.util.Set
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.ComponentModel
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignatureDefinition
import org.eclipse.comma.traces.events.traceEvents.Event
import org.eclipse.comma.traces.events.traceEvents.Import
import org.eclipse.comma.traces.events.traceEvents.Notification
import org.eclipse.comma.traces.events.traceEvents.Reply
import org.eclipse.comma.traces.events.traceEvents.Signal
import org.eclipse.comma.traces.events.traceEvents.TraceEvents
import org.eclipse.comma.traces.events.traceEvents.TraceEventsPackage
import org.eclipse.comma.types.utilities.TypeUtilities
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.validation.Check

import static extension org.eclipse.comma.types.utilities.CommaUtilities.*
import com.google.common.collect.HashMultimap

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class TraceEventsValidator extends AbstractTraceEventsValidator {
    
    @Inject protected IScopeProvider scopeProvider
	
	@Check
	def checkImportForValidity(Import imp){
		if(! EcoreUtil2.isValidUri(imp, URI.createURI(imp.importURI)))
			error("Invalid resource", imp, TraceEventsPackage.eINSTANCE.import_ImportURI)
		else{
			val Resource r = EcoreUtil2.getResource(imp.eResource, imp.importURI)
			val head = r.allContents.head
			if(! (head instanceof InterfaceSignatureDefinition) && !(head instanceof ComponentModel))
				error("The imported resource is not an interface signature nor a component definition", imp, TraceEventsPackage.eINSTANCE.import_ImportURI)
		}
	}
	
	@Check
	def checkUniqueComponentIds(TraceEvents traces){
	    val multiMap = HashMultimap.create()
        for (ci : traces.components)
            multiMap.put(ci.componentId, ci)
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                for (d : duplicates)
                    error("Duplicate component instance name", d, TraceEventsPackage.Literals.COMPONENT__COMPONENT_ID)
            }
        }
	}
	
	@Check
	def checkNumberOfParameters(Event e){
		if(e === null) return
		var expectedNrParams = 0
		if(e instanceof org.eclipse.comma.traces.events.traceEvents.Command) {expectedNrParams = e.command.parameters.filter(p | p.direction != DIRECTION::OUT).size}
		else if(e instanceof Signal || e instanceof Notification){
			expectedNrParams = e.command.parameters.size
		}
		else if(e instanceof Reply){
			expectedNrParams = e.command.parameters.filter(p | p.direction != DIRECTION::IN).size
			if( !(e.command instanceof Command)) return
			val retType = (e.command as Command).type
			if(! TypeUtilities::isVoid(retType)) expectedNrParams++
		}
		if(expectedNrParams != e.parameters.size){
			error("Wrong number of parameters. Expected " + expectedNrParams + " parameters.", TraceEventsPackage.Literals.EVENT__COMMAND)
		}
	}
	
	@Check
	def checkCorrectUseOfComponents(TraceEvents trace){
	    if(!trace.components.empty) {
	        val Set<Component> componentTypes = trace.resolveProxy(scopeProvider.getScope(trace, TraceEventsPackage.Literals.DUMMY__COMPONENT).getAllElements).toSet
	        var unknownTypeFound = false
	        for(cInstance : trace.components){
	            if(!componentTypes.exists[fullyQualifiedNameAsString.equals(cInstance.componentType)]){
	                unknownTypeFound = true
	                error("Unknown component type", cInstance, TraceEventsPackage.Literals.COMPONENT__COMPONENT_TYPE)
	            }
	        }
	        if(unknownTypeFound) {return}
	        for(ev : trace.events){
	            checkEvent(ev, componentTypes)
	        }
	    }
	}
	
	def checkEvent(Event ev, Set<Component> componentTypes) {
	    val root = ev.eContainer as TraceEvents
	    var targetPort = true
	    var EAttribute attrToHighlight
	    
	    //first check if the event source is a component instance
	    var cInstance = root.components.findFirst[componentId.equals(ev.source)]
	    if(cInstance !== null){
	        attrToHighlight = TraceEventsPackage.Literals.EVENT__SOURCE_PORT
	        targetPort = false
	    } else {
	        //event source is not a component instance. try with the target
	        cInstance = root.components.findFirst[componentId.equals(ev.target)]
	        if(cInstance === null) {return}
            attrToHighlight = TraceEventsPackage.Literals.EVENT__TARGET_PORT
	    }
	    //check if the port is defined in the component model
	    val cType = cInstance.componentType
	    val componentModel = componentTypes.findFirst[fullyQualifiedNameAsString.equals(cType)]
	    val portName = targetPort ? ev.targetPort : ev.sourcePort
	    val portDef = componentModel.ports.findFirst[name.equals(portName)]
	    if(portDef === null) {
            //unknown event source port
            error("Port is not defined in the component model", ev, attrToHighlight)
            return
        }
        val providedPort = portDef instanceof ProvidedPort
        //check if the event is allowed for the combination (targetPort, kind of port)
        if(targetPort.xor(providedPort)){
            if(ev instanceof Command || ev instanceof Signal){
                error("Only reply or notification is allowed for this port and this direction", ev, attrToHighlight)
            }
        } else {
            if(ev instanceof Reply || ev instanceof Notification){
                error("Only command or signal is allowed for this port and this direction", ev, attrToHighlight)
            }
        }
	}
}
