/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.traces.events.utilities

import org.eclipse.comma.traces.events.traceEvents.TraceEvents
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.ByteArrayInputStream
import com.google.gson.JsonStreamParser

class EventsUtilities {
	var static String components
	
	static def TraceEvents readHeader(URI traceUri){
		val resourceSet = new ResourceSetImpl
		val inputStream = resourceSet.URIConverter.createInputStream(traceUri)
		val reader = new BufferedReader(new InputStreamReader(inputStream))

		var String line
		var eventsReached = false
		
		var result = ""
		
		
	   while((line = reader.readLine) !== null && !eventsReached){
				line = line.trim
				if(line.startsWith("events") || line.startsWith("errors")){
					eventsReached = true
				}else{
					result += line + System.lineSeparator
				}
			}
		getTraceEventsFrom(result)
	}
	
	def static TraceEvents readHeaderFromJson(URI traceUri){
		val resourceSet = new ResourceSetImpl
		val inputStream = resourceSet.URIConverter.createInputStream(traceUri)
		val parser = new JsonStreamParser(new InputStreamReader(inputStream, "UTF-8"))
		
		
		components = "components\n"
		if(parser.hasNext) {
				val componentInstances = parser.next()
				if(componentInstances.asJsonObject.has("components")) {
				 	componentInstances.asJsonObject.get("components").asJsonArray.forEach[instance |
				 	  	components += instance.asJsonObject.get("type").asString + " " + instance.asJsonObject.get("value").asString + System.lineSeparator
				 	]
				 } 
			}
		getTraceEventsFrom(components)
	}
	
	def static TraceEvents getTraceEventsFrom(String inputType) {
		val resourceSet = new ResourceSetImpl
		val uri = URI.createURI("dummy.events")
		var dummyres = resourceSet.getResource(uri, false)
		if(dummyres === null){
			dummyres = resourceSet.createResource(uri)
		}
		val input = new ByteArrayInputStream(inputType.getBytes())
		dummyres.load(input, resourceSet.getLoadOptions())
		if(dummyres === null || dummyres.getContents.empty){
			return null
		}
		return dummyres.getContents().get(0) as TraceEvents
	}
	
}