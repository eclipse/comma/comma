/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.modelqualitychecks;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.comma.behavior.behavior.State;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.petrinet.PetrinetBuilder;
import org.eclipse.comma.petrinet.PetrinetBuilder.Mode;
import org.eclipse.xtext.scoping.IScopeProvider;

public class ModelQualityChecksGenerator {
	public static String generate(Interface itf, Parameters params, IScopeProvider scopeProvider, int maxDepth, List<List<State>> homeStates, String plantumlJar) throws IOException, URISyntaxException {
		var code = "SELF_CONTAINED = True\n" + PetrinetBuilder.getModelCode() + PetrinetBuilder.getReachabilityGraphCode();
		code += "## verification_report.py\n" + getResourceText("/verification_report.py") + "\n\n";
		code += PetrinetBuilder.forInterface(itf, params, scopeProvider, Mode.NOT_INTERACTIVE) + "\n\n";
		
		if (homeStates == null || homeStates.isEmpty()) {
			var initialStates = itf.getMachines().stream().map(m -> m.getStates().stream()
					.filter(s -> s.isInitial()).findFirst().get()).collect(Collectors.toList());
			homeStates = new ArrayList<List<State>>();
			homeStates.add(initialStates);
		}
		var homeStatesStr = homeStates.stream().map(l -> l.stream().map(s -> String.format("'%s'", s.getName()))
				.collect(Collectors.joining(","))).map(s -> String.format("[%s]", s)).collect(Collectors.joining(", "));
		
        var javaExecutable = Paths.get(System.getProperty("java.home"), "bin", "java");
        
        var itfOS = new ByteArrayOutputStream();
        itf.eResource().save(itfOS, new HashMap<>());
		var itfBase64 = Base64.getEncoder().encodeToString(itfOS.toByteArray());

		code += "## Parameters\n";
		code += String.format("MAX_DEPTH = %d\n", maxDepth);
		code += String.format("HOME_STATES = [%s]\n", homeStatesStr);
		code += String.format("JAVA = r'%s'\n", javaExecutable);
		code += String.format("PLANTUML = r'%s'\n", plantumlJar);
		code += String.format("INTERFACE = r'%s'\n", itfBase64);
		code += "OUTPUT_FILE = 'report.json'\n";
		code += "\n";

		code += "## generator_mqc.py\n" + getResourceText("/generator_mqc.py") + "\n\n";
		return code;
	}
	
	public static InputStream getDashboardHTML() throws IOException {
		return ModelQualityChecksGenerator.class.getResourceAsStream("/dashboard.html");
	}
	
	private static String getResourceText(String resource) {
		var stream = ModelQualityChecksGenerator.class.getResourceAsStream(resource);
		return new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));
	}
}
