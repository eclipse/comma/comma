/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

interface IProps {
  interface: string;
}

const keywords = ['state', 'transition', 'interface', 'next', 'state:', 'state', 'reply',
    'import', 'intial', 'init', 'machine', 'trigger:', 'do:', 'OR', 'reply', 'variables', 'initial']

class InterfaceView extends React.Component<IProps> {
    styledInterface(): string {
        let cText = this.props.interface;
        cText = cText.replace(/(".+")/g, `<span style="color: blue;">$1</span>`);
        cText = cText.replace(/(\/\/.*)/g, `<span style="color: green;">$1</span>`);
        cText = cText.replace(/((\/\*)(.|\n)+?(\*\/))/g, `<span style="color: green;">$1</span>`);

        const breaks = ['\n', '\t', ' ', '(', ')'];
        let hText = '';
        let current = '';
        let inSpan = false;
        for (let i = 0; i < cText.length; i++) {
            const char = cText[i];
            current += char;
            if (current == '<span') {
                inSpan = true;
            } else if (current.endsWith('</span>')) {
                inSpan = false;
                hText += current;
                current = '';
            } else if (!inSpan) {
                if (breaks.includes(char)) {
                    const withoutBreak = current.substring(0, current.length - 1);
                    if (keywords.includes(withoutBreak)) {
                        hText += `<span style="color: purple; font-weight:bold">${withoutBreak}</span>${char}`;
                    } else {
                        hText += current;
                    }
                    current = '';
                }
            }
        }
        hText += current;
        return hText;
    }

    render(): React.ReactNode {
        return (
            <Box justifyContent="center" m={2} boxShadow={2} p={2} >
                <Typography style={{ whiteSpace: 'pre-wrap' }}>
                    <div dangerouslySetInnerHTML={{__html: this.styledInterface()}}/>
                </Typography>
            </Box>
        );
    }
}
export default InterfaceView;
