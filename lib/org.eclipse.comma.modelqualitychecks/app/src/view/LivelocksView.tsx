/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Box from '@material-ui/core/Box';
import VerificationReport from '../VerificationReport';
import { renderInfo } from './base';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';

interface IProps {
    report: VerificationReport;
}

const sccHelp = "SCC shows the states which are strongly connected with each other, meaning that every state can reach every other state in the SCC." + 
    " If the interface has only 1 SCC then it will not have any livelocks.";

class LivelocksView extends React.Component<IProps> {
    render(): React.ReactNode {
        return (
        <div>
            <Box boxShadow={2} m={2}>
                <div style={{padding: '10px'}}>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <div>Strongly Connected Components (SCC):</div>
                        <Tooltip title={sccHelp}><InfoIcon/></Tooltip>
                    </div>
                    <ol>
                    {
                        this.props.report.scc.map((d, i) => (
                            <li key={i}>{d.message}</li>
                        ))
                    }
                    </ol>
                </div>
            </Box>
            { this.props.report.livelocks.length == 0 &&
                <div>{renderInfo({message: `No livelock states found`}, 0)}</div>
            }
            {
                Array.from(new Set(this.props.report.livelocks.map((l) => l.state))).map((state, index) => (
                    <Box key={index} boxShadow={2} m={2}>
                        <Box display="flex" justifyContent="center">{state} is a livelock</Box>
                    </Box>
                ))
            }
            {this.props.report.livelocks.map((d, i) => renderInfo(d, i))}
        </div>
        );
    }
}

export default LivelocksView;
