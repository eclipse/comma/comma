/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Box from '@material-ui/core/Box';
import VerificationReport from '../VerificationReport';
import { renderInfo } from './base';

interface IProps {
    report: VerificationReport;
}

class GeneralizedConfusionsView extends React.Component<IProps> {
    render(): React.ReactNode {
        if (this.props.report.generalizedConfusions.length == 0) {
            return <div>{renderInfo({message: `No generalized confusions found`}, 0)}</div>
        }

        return (
        <div>
            {this.props.report.generalizedConfusions.map((d, i) => {
                const additional = (
                    <div>
                        <Box display="flex" justifyContent="center"><b>Confusion Event:</b>{d.confusionEvent}</Box>
                        <Box display="flex" justifyContent="center"><b>Path to Confusion:</b>{d.eventsToConfusion.map(s => s + " ")}</Box>
                        <Box display="flex" justifyContent="center"><b>States to Confusion:</b>{d.statesToConfusion.map(s => s + " ")}</Box>
                    </div>
                )
                return renderInfo(d, i, {beforeImage: additional})
            })}
        </div>
        );
    }
}

export default GeneralizedConfusionsView;
