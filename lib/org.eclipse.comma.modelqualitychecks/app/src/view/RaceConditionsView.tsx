/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Box from '@material-ui/core/Box';
import VerificationReport from '../VerificationReport';
import { renderInfo } from './base';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

interface IProps {
    report: VerificationReport;
    side: 'server' | 'client';
}

interface IState {
    error: boolean,
    warning: boolean,
}

class RaceConditionsView extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {error: true, warning: false}
    }

    render(): React.ReactNode {
        const raceConditions = this.props.side == 'client' ? this.props.report.clientRaceConditions : this.props.report.serverRaceConditions;

        if (raceConditions.length == 0) {
            return <div>{renderInfo({message: `No ${this.props.side} races found`}, 0)}</div>
        }
        
        return (
        <div>
            <Box display="flex" justifyContent="center" m={2} boxShadow={2} padding='10px'>
                <ButtonGroup color="primary" aria-label="outlined primary button group">
                    <Button 
                        variant={this.state.error ? 'contained' : 'outlined'}
                        onClick={() => this.setState({error: !this.state.error})}
                    >
                        Errors ({raceConditions.filter((r) => r.category === 'error').length})
                    </Button>
                    <Button 
                        variant={this.state.warning ? 'contained' : 'outlined'}
                        onClick={() => this.setState({warning: !this.state.warning})}
                    >
                        Warnings ({raceConditions.filter((r) => r.category === 'warning').length})
                    </Button>
                </ButtonGroup>
            </Box>

            {raceConditions.filter((r) => this.state[r.category]).map((d, i) => {
                const additional = {
                    beforeMessage: (<Box display="flex" justifyContent="center"><b>{d.category.toUpperCase()}</b></Box>),
                    beforeImage: ( <Box display="flex" justifyContent="center">{d.resolution}</Box>)
                }
                return renderInfo(d, i, additional)
            })}
        </div>
        );
    }
}

export default RaceConditionsView;
