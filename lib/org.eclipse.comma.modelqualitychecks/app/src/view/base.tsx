/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import Box from '@material-ui/core/Box';
import React from 'react';

interface AdditionalRenderInfo {beforeMessage?: React.ReactNode, beforeImage?: React.ReactNode}
export function renderInfo(info: {message: string; image?: string}, key: number, additional: AdditionalRenderInfo = {}): React.ReactNode {
    return (
    <Box key={key} boxShadow={2} m={2}>
        {additional.beforeMessage}
        <Box display="flex" justifyContent="center">{info.message}</Box>
        {additional.beforeImage}
        {info.image && 
            <Box display="flex" justifyContent="center">
                <img style={{ width:'100%'}} src={"data:image/png;base64, " + info.image} alt="Diagram" />
            </Box>
        }
    </Box>
    );
}