/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import { renderInfo } from './base';

interface IProps {
    data: {message: string; image?: string}[];
    name: string;
}

class MessageImageView extends React.Component<IProps> {
    render(): React.ReactNode {
        if (this.props.data.length == 0) {
            return <div>{renderInfo({message: `No ${this.props.name} found`}, 0)}</div>
        } else {
            return (
                <div>
                    {this.props.data.map((d, i) => renderInfo(d, i))}
                </div>
            );
        }
    }
}

export default MessageImageView;
