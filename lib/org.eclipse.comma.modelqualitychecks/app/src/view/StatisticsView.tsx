/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import Box from '@material-ui/core/Box';
import VerificationReport from '../VerificationReport';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import InfoIcon from '@material-ui/icons/Info';
import Tooltip from '@material-ui/core/Tooltip';

interface IProps {
  report: VerificationReport;
}

const info = {
    nstates: 'Number of states in the CommaSuite model',
    dstates: 'States that could cause a deadlock',
    ctransition: '% of transitions covered in the CommaSuite model',
    cstates: '% of states covered in the CommaSuite model',
    rcoverage: 'Full if the reachability graph covers the whole Petri Net, partial if not',
}

class StatisticsView extends React.Component<IProps> {
    stringifyPercentage(percentage: number): string {
        return `${Math.round(percentage * 10000) / 100}%`;
    }
    
    render(): React.ReactNode {
        const deadlockStates = this.props.report.deadlocks.length == 0 ? "None" : 
            Array.from(new Set(this.props.report.deadlocks.map((d) => d.state).filter((s) => s))).join(', ');

        return (
            <div>
                { this.props.report.statistics.reachabilityGraphCoverage === 'partial' &&
                    <Box m={2} boxShadow={2} p={2} style={{backgroundColor: 'rgba(255,229,100,0.2)', borderLeft:'5px solid #ffe564'}}>
                        ⚠️ Reachability graph coverage is partial, no checks have been executed. 
                        Increase the max-depth or limit the state space by adding guards to counter variables.
                    </Box>
                }
                <Box justifyContent="center" m={2} boxShadow={2} p={2} >
                    <TableContainer>
                        <Table aria-label="simple table"  >
                            <TableBody>
                                <TableRow>
                                    <TableCell>Reachability graph coverage</TableCell>
                                    <TableCell width="50%">{this.props.report.statistics.reachabilityGraphCoverage}</TableCell>
                                    <TableCell ><Tooltip title={info.rcoverage}><InfoIcon/></Tooltip></TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell >Model transition coverage</TableCell>
                                    <TableCell>{this.stringifyPercentage(this.props.report.statistics.transitionCoverage)}</TableCell>
                                    <TableCell ><Tooltip title={info.ctransition}><InfoIcon/></Tooltip></TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell >Model state coverage</TableCell>
                                    <TableCell>{this.stringifyPercentage(this.props.report.statistics.stateCoverage)}</TableCell>
                                    <TableCell ><Tooltip title={info.cstates}><InfoIcon/></Tooltip></TableCell>
                                </TableRow>
                                { Object.keys(this.props.report.statistics.stateCoverageMachine).length > 1 && 
                                    Object.entries(this.props.report.statistics.stateCoverageMachine).map((e, i) => {
                                    return (
                                        <TableRow key={i}>
                                            <TableCell><i style={{marginLeft: '10px'}}>Machine {e[0]}</i></TableCell>
                                            <TableCell><i>{this.stringifyPercentage(e[1])}</i></TableCell>
                                            <TableCell/>
                                        </TableRow>
                                    )
                                })}
                                <TableRow>
                                    <TableCell >Number of states</TableCell>
                                    <TableCell>{this.props.report.statistics.numOfStates}</TableCell>
                                    <TableCell ><Tooltip title={info.nstates}><InfoIcon/></Tooltip></TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell >Deadlock states</TableCell>
                                    <TableCell>{deadlockStates}</TableCell>
                                    <TableCell ><Tooltip title={info.dstates}><InfoIcon/></Tooltip></TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
            </div>
        );
    }
}
export default StatisticsView;
