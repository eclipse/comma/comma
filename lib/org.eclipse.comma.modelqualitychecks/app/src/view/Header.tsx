/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import DescriptionIcon from '@material-ui/icons/Description';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';

interface IProps {
    menuOpen: boolean;
    interfaceShown: boolean;
    classes: ClassNameMap<"appBar" | "appBarShift">
    toggleMenu: () => void;
    toggleInterface: () => void;
    title: string;
}

class Header extends React.Component<IProps> {
    render(): React.ReactNode {
        return (
            <AppBar
                position="relative"
                className={clsx(this.props.classes.appBar, {[this.props.classes.appBarShift]: this.props.menuOpen})}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        onClick={this.props.toggleMenu}
                        edge="start"
                        style={{display: this.props.menuOpen ? 'none' : ''}}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" style={{flexGrow: 1}}>
                        {this.props.title}
                    </Typography>
                    <IconButton
                        color="inherit"
                        onClick={this.props.toggleInterface}
                    >
                        {this.props.interfaceShown ? <DescriptionIcon /> : <DescriptionOutlinedIcon />}
                    </IconButton>
                </Toolbar>
            </AppBar>
        );
    }
}

export default Header;
