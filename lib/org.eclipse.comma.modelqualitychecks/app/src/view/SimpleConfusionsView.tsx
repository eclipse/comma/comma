/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import VerificationReport from '../VerificationReport';
import { renderInfo } from './base';

interface IProps {
    report: VerificationReport;
}

class SimpleConfusionsView extends React.Component<IProps> {
    render(): React.ReactNode {
        if (this.props.report.simpleConfusions.length == 0) {
            return <div>{renderInfo({message: `No simple confusions found`}, 0)}</div>
        }

        return (
        <div>
            {this.props.report.simpleConfusions.map((d, i) => {
                const additional = (
                    <div style={{paddingLeft: '10px', paddingRight: '10px'}}>
                        <p style={{margin: 0, wordBreak: 'break-all'}}><b>1:</b> {d.reason1}</p>
                        <p style={{margin: 0, wordBreak: 'break-all'}}><b>2:</b> {d.reason2}</p>
                    </div>
                )
                return renderInfo(d, i, {beforeImage: additional})
            })}
        </div>
        );
    }
}

export default SimpleConfusionsView;
