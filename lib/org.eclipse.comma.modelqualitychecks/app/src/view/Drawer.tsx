/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
 import React from 'react';
 import IconButton from '@material-ui/core/IconButton';
 import {Drawer as MDrawer, ListSubheader} from '@material-ui/core';
 import { ClassNameMap } from '@material-ui/core/styles/withStyles';
 import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
 import Divider from '@material-ui/core/Divider';
 import List from '@material-ui/core/List';
 import ListItem from '@material-ui/core/ListItem';
 import ListItemText from '@material-ui/core/ListItemText';
 import { ViewID } from '../types';
import VerificationReport from '../VerificationReport';

interface IProps {
    setView: (view: ViewID) => void;
    menuOpen: boolean;
    toggleMenu: () => void;
    report: VerificationReport;
    classes: ClassNameMap<'drawer' | 'drawerPaper' | 'drawerHeader'>
}

class Drawer extends React.Component<IProps> {
    render(): React.ReactNode {
        return (
            <MDrawer
                className={this.props.classes.drawer}
                variant="persistent"
                anchor="left"
                open={this.props.menuOpen}
                classes={{paper: this.props.classes.drawerPaper}}
            >
                <div className={this.props.classes.drawerHeader}>
                    <IconButton onClick={this.props.toggleMenu}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider/>
                <List>
                    <ListItem button onClick={() => this.props.setView('statistics')}>
                        <ListItemText primary='Statistics' />
                    </ListItem>
                </List>
                { this.props.report.statistics.reachabilityGraphCoverage == 'full' &&
                <div>
                    <Divider />
                    <ListSubheader>{'State machine'}</ListSubheader>
                    <List>
                        <ListItem button onClick={() => this.props.setView('deadlocks')}>
                            <ListItemText primary={`Deadlock states (${this.props.report.deadlocks.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('livelocks')}>
                            <ListItemText primary={`Livelock states (${this.props.report.livelocks.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('unreachable')}>
                            <ListItemText primary={`Unreachable states (${this.props.report.unreachableStates.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('sinks')}>
                            <ListItemText primary={`Sink states (${this.props.report.sinkStates.length})`} />
                        </ListItem>
                    </List>
                    <Divider />
                    <ListSubheader>{'Client-server communication'}</ListSubheader>
                    <List>
                        <ListItem button onClick={() => this.props.setView('choice')}>
                            <ListItemText primary={`Choice states (${this.props.report.choiceProperties.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('serverrace')}>
                            <ListItemText primary={`Server races (${this.props.report.serverRaceConditions.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('clientrace')}>
                            <ListItemText primary={`Client races (${this.props.report.clientRaceConditions.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('simpleconfusion')}>
                            <ListItemText primary={`Simple confusions (${this.props.report.simpleConfusions.length})`} />
                        </ListItem>
                        <ListItem button onClick={() => this.props.setView('generalizedconfusion')}>
                            <ListItemText primary={`Generalized confusions (${this.props.report.generalizedConfusions.length})`} />
                        </ListItem>
                        {this.props.report.weakTermination &&
                            <ListItem button onClick={() => this.props.setView('weaktermination')}>
                                <ListItemText primary={`Violation of weak termination (${this.props.report.weakTermination.length})`} />
                            </ListItem>
                        }
                    </List>
                </div>
                }
          </MDrawer>
        );
    }
}
 
export default Drawer;
