/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import SplitPane from 'react-split-pane';
import { Theme, withStyles } from "@material-ui/core/styles";
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import clsx from 'clsx';

import './split-pane.css';
import VerificationReport from './VerificationReport';
import { ReportJSON, ViewID } from './types';

import Drawer from './view/Drawer';
import Header from './view/Header';
import StatisticsView from './view/StatisticsView';


import InterfaceView from './view/InterfaceView';
import MessageImageView from './view/MessageImageView';
import LivelocksView from './view/LivelocksView';
import RaceConditionsView from './view/RaceConditionsView';
import GeneralizedConfusionsView from './view/GeneralizedConfusionsView';
import SimpleConfusionsView from './view/SimpleConfusionsView';

interface IProps {
    classes: ClassNameMap<'content' | 'contentShift' | 'appBar' | 'appBarShift' | 'drawer' | 'drawerPaper' | 'drawerHeader'>
 }

interface IState {
    view: ViewID;
    report: VerificationReport | null;
    menuOpen: boolean;
    interfaceShown: boolean;
}

const drawerWidth = 240;

const styles = (theme: Theme) => ({
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: drawerWidth,
    },
    content: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    drawer: {
        width: drawerWidth,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
});


const viewLookup = {
    statistics: {
        render: (report: VerificationReport) => <StatisticsView report={report}/>, 
        title: 'Statistics'
    },
    deadlocks: {
        render: (report: VerificationReport) => <MessageImageView name='deadlock states' data={report.deadlocks} />, 
        title: 'Deadlock states'
    },
    livelocks: {
        render: (report: VerificationReport) => <LivelocksView report={report} />, 
        title: 'Livelock states'
    },
    unreachable: {
        render: (report: VerificationReport) => <MessageImageView name='unreachable states' data={report.unreachableStates} />, 
        title: 'Unreachable states'
    },
    sinks: {
        render: (report: VerificationReport) => <MessageImageView name='sink states' data={report.sinkStates} />, 
        title: 'Sink states'
    },
    choice: {
        render: (report: VerificationReport) => <MessageImageView name='choice states' data={report.choiceProperties} />, 
        title: 'Choice states'
    },
    clientrace: {
        render: (report: VerificationReport) => <RaceConditionsView report={report} side='client' />, 
        title: 'Client races'
    },
    serverrace: {
        render: (report: VerificationReport) => <RaceConditionsView report={report} side='server' />, 
        title: 'Server races'
    },
    simpleconfusion: {
        render: (report: VerificationReport) => <SimpleConfusionsView report={report} />, 
        title: 'Simple confusions'
    },
    generalizedconfusion: {
        render: (report: VerificationReport) => <GeneralizedConfusionsView report={report} />, 
        title: 'Generalized confusions'
    },
    weaktermination: {
        render: (report: VerificationReport) => report.weakTermination && <MessageImageView name='deadlock states' data={report.weakTermination} />, 
        title: 'Violation of weak termination'
    },
}

class App extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            interfaceShown: true,
            view: 'statistics',
            report: null,
            menuOpen: true,
        };
    }

    componentDidMount(): void {
        let report: ReportJSON;

        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            report = require('./testdata.js').default;
        } else {
            // eslint-disable-next-line
            // @ts-ignore
            report = window.report;
        }

        this.setState({report: new VerificationReport(report)});
    }

    render(): React.ReactNode {
        if (this.state.report == null) return null;
        const classes = this.props.classes;
        const toggleMenu = () => this.setState({menuOpen: !this.state.menuOpen});

        return (
            <div className="App">
                <CssBaseline />
                <div style={{display: 'flex', flexDirection: 'column', height: '100vh'}}>
                    <Header
                        interfaceShown={this.state.interfaceShown}
                        toggleInterface={() => this.setState({interfaceShown: !this.state.interfaceShown})}
                        menuOpen={this.state.menuOpen}
                        toggleMenu={toggleMenu}
                        classes={classes}
                        title={viewLookup[this.state.view].title}
                    />

                    <Drawer
                        report={this.state.report}
                        setView={(view: ViewID) => this.setState({view})}
                        menuOpen={this.state.menuOpen}
                        toggleMenu={toggleMenu}
                        classes={classes}
                    />

                    <SplitPane
                        className={clsx(this.props.classes.content, {[this.props.classes.contentShift]: this.state.menuOpen})}
                        size={this.state.interfaceShown ? '40%' : '100%'}
                        split="vertical"
                        paneStyle={{'overflowY': 'scroll'}}
                        style={{display: 'flex', flex: '1', overflowY: 'hidden', position: 'relative'}}
                    >
                        <div>{viewLookup[this.state.view].render(this.state.report)}</div>
                        <div><InterfaceView interface={this.state.report.interface} /></div>
                    </SplitPane>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(App);
