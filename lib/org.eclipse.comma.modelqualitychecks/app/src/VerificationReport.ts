/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import * as types from './types';

class VerificationReport {
    public readonly createdAt: Date;
    public readonly statistics: types.ReportStatistics;
    public readonly interface: string;
    public readonly deadlocks: types.ReportMessageImageState[];
    public readonly livelocks: types.ReportMessageImageState[];
    public readonly unreachableStates: types.ReportMessage[];
    public readonly sinkStates: types.ReportMessageImage[];
    public readonly choiceProperties : types.ReportMessage[];
    public readonly scc : types.ReportMessage[];
    public readonly serverRaceConditions: types.ReportRace[];
    public readonly clientRaceConditions: types.ReportRace[];
    public readonly simpleConfusions: types.ReportSimpleConfusion[];
    public readonly generalizedConfusions: types.ReportGeneralizedConfusion[];
    public readonly weakTermination: types.ReportMessageImage[] | null;

    constructor(data: types.ReportJSON) {
        this.createdAt = new Date(data.meta.createdAt);
        this.statistics = data.statistics;
        this.interface = atob(data.interface.content);
        this.deadlocks = data.deadlocks;
        this.unreachableStates = data.unreachableStates;
        this.sinkStates = data.sinkStates
        this.choiceProperties = data.choiceProperties;
        this.scc = data.SCC;
        this.serverRaceConditions = data.serverRaceConditions;
        this.clientRaceConditions = data.clientRaceConditions;
        this.simpleConfusions = data.simpleConfusions;
        this.generalizedConfusions = data.generalizedConfusions;
        this.livelocks = data.livelocks;
        this.weakTermination = data.weakTermination;
    }
}

export default VerificationReport;