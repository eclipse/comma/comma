/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
export type ReportMessageImageState = ReportMessageImage & {state: string | null}

export type ReportMessageImage = ReportMessage & {image: string}

export interface ReportMessage {message: string}

export interface ReportRace {
    category: 'warning' | 'error',
    message : string,
    image : string,
    resolution : string,
}

export interface ReportSimpleConfusion {
    message: string,
    image: string,
    reason1: string, 
    reason2: string
}

export interface ReportGeneralizedConfusion {
    message: string,
    image: string,
    confusionEvent: string, 
    eventsToConfusion: string[],
    statesToConfusion: string[],
}

export interface ReportStatistics {
    reachabilityGraphCoverage: 'full' | 'partial',
    transitionCoverage: number,
    stateCoverage: number,
    stateCoverageMachine: {[s: string]: number}
    numOfStates: number,
}

export interface ReportJSON {
    meta: {createdAt: string},
    statistics: ReportStatistics, 
    interface: {content: string}
    deadlocks: ReportMessageImageState[],
    livelocks: ReportMessageImageState[],
    unreachableStates: ReportMessage[],
    sinkStates: ReportMessageImage[],
    choiceProperties: ReportMessage[],
    SCC: ReportMessage[],
    serverRaceConditions: ReportRace[],
    clientRaceConditions: ReportRace[],
    simpleConfusions: ReportSimpleConfusion[],
    generalizedConfusions: ReportGeneralizedConfusion[],
    weakTermination: ReportMessageImage[] | null,
}

export type ViewID = 'statistics' | 'deadlocks' | 'livelocks' | 'unreachable' | 'sinks' | 
    'choice' | 'serverrace' | 'simpleconfusion' | 'generalizedconfusion' | 'clientrace' | 'weaktermination';