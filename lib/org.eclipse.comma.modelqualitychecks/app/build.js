/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/**
 * This script inlines the bundle.js in index.html and saves it as the dashboard.html.
 */

const fs = require('fs')
const index = fs.readFileSync('public/index.html', 'utf8');
const bundle = fs.readFileSync('public/bundle.js', 'utf8');

let content = index.split("<script src=\"bundle.js\"></script>");
content = content[0] + `<script>${bundle}</script>` + content[1]

const placeholder = new TextEncoder().encode("\"%REPORT%\"");
const contentBytes = Buffer.from(content, 'utf8');
const placeHolderStart = contentBytes.indexOf(placeholder);
const placeHolderEnd = placeHolderStart + placeholder.length;
content = `<!-- REPORT_PLACEHOLDER = ${placeHolderStart}:${placeHolderEnd} -->\n` + content;

fs.writeFileSync('../resource/dashboard.html', content);
console.log('Saved to ../resource/dashboard.html');
