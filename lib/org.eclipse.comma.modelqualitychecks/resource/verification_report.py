#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import dataclasses, itertools, inspect, re
from typing import Callable
@dataclasses.dataclass
class SimpleConfusion: node: 'Node'; paths: List['Path']
@dataclasses.dataclass
class GeneralizedConfusion: node: 'Node'; path: 'Path'; event_edge: 'Edge'; conflict: 'Path'
@dataclasses.dataclass
class RaceCondition: 
    node: 'Node'; direction: str; client_path: List['Edge']; server_path: List['Edge']; category: str
    problem_type: str; problem_event: Optional['Event']; problem_node: Optional['Node']; problem_nodes: Optional[List['Node']]

class VerificationReport:
    inverse_direction_lookup = {'client': 'server', 'server': 'client'}
    plantuml_lookup = {
        'server': ['server -> middleware', 'middleware -> client'], 
        'client': ['client -> middleware', 'middleware -> server'],
        f'client_{EventType.Command}': 'client -> middleware', f'server_{EventType.Command}': 'middleware -> server',
        f'client_{EventType.Signal}': 'client -> middleware', f'server_{EventType.Signal}': 'middleware -> server',
        f'client_{EventType.Notification}': 'middleware -> client', f'server_{EventType.Notification}': 'server -> middleware',
        f'client_{EventType.Reply}': 'middleware -> client', f'server_{EventType.Reply}': 'server -> middleware',
    }

    def __init__(self, net: PetriNet, rg: 'ReachabilityGraph', home_states: List[List[str]]) -> None:
        self.net = net
        self.rg = rg
        self.home_states = home_states
        self.unreachable_states: List[str] = []
        self.sinks: List['Node'] = []
        self.deadlocks: List['Node'] = []
        self.choice_violations: List['Node'] = []
        self.livelocks: List['Node'] = []
        self.race_conditions: List[RaceCondition] = []
        self.scc: List[List[str]] = []
        self.simple_confusions: List[SimpleConfusion] = []
        self.generalized_confusions: List[GeneralizedConfusion] = []
        self.__calculate_coverage()
        if self.rg.depth != self.rg.max_depth:
            self.__find_unreachable_states()
            self.__find_deadlocks()
            self.__find_sinks()
            self.__find_livelocks()
            self.__find_choice_violations()
            self.__find_scc()
            self.__find_simple_confusion()
            self.__find_generalized_confusion()
            self.__find_race_conditions()

    def __calculate_coverage(self) -> None:
        all_transitions = set(self.net._trans.keys())
        covered_transitions = set(e.transition for e in self.rg.edges)

        self.covered_states_per_machine: Dict[str, Set[str]] = {}
        for node in self.rg.state_nodes:
            assert node.states
            for machine, state in node.states.items():
                if not machine in self.covered_states_per_machine: self.covered_states_per_machine[machine] = set()
                self.covered_states_per_machine[machine].add(state)

        self.all_states_per_machine: Dict[str, Set[str]] = {}
        for place in self.net.place():
            if 'state' in place.meta:
                if not place.meta['machine'] in self.all_states_per_machine: 
                    self.all_states_per_machine[place.meta['machine']] = set()
                self.all_states_per_machine[place.meta['machine']].add(place.meta['state'])

        coverage_machine = {}
        for machine, states in self.all_states_per_machine.items():
            coverage_machine[machine] = len(self.covered_states_per_machine[machine]) / len(states)

        self.coverage: Dict[str, Any] = {
            'state': sum([len(c) for c in self.covered_states_per_machine.values()]) / sum([len(c) for c in self.all_states_per_machine.values()]),
            'transition': len(covered_transitions) / len(all_transitions),
            'state_machine': coverage_machine,
        }

    def __find_unreachable_states(self) -> None:
        '''
        Unreachable states are states of the Comma model that have a place in the PetriNet
        but don't have a corresponding node in the reachability graph.
        (since the reachability graph only contains reachable nodes). 
        '''
        self.unreachable_states: List[str] = []
        multimachine = len(self.all_states_per_machine) != 1
        for machine, states in self.all_states_per_machine.items():
            for state in states:
                if not state in self.covered_states_per_machine[machine]:
                    txt = f"{machine}_{state}" if multimachine else state
                    self.unreachable_states.append(txt)

    def __find_deadlocks(self) -> None:
        'Deadlock states are all nodes that have incoming edge(s) but no outgoing edge.'
        self.deadlocks = list(dict([(n.states_values_str(), n) for n in self.rg.state_nodes if len(n.outgoing) == 0 and len(n.incoming) != 0]).values())

    def __find_sinks(self) -> None:
        'Sink states are states in the Comma model that cannot be left anymore after entering them and are not a deadlock.'
        sinks: Dict[str, Node] = {}
        all_states = list(set([n.states_values_str() for n in self.rg.state_nodes]))
        for n in self.rg.state_nodes:
            if not n.states_values_str() in sinks and len(n.outgoing) > 0 and not any(n.find_paths('states', [s for s in all_states if s != n.states_values_str()])):
                sinks[n.states_values_str()] = n
        self.sinks = list(sinks.values())

    def __find_livelocks(self) -> None:
        'Livelock states are states from which non of the home state(s) can be reached.'
        livelocks: Dict[str, Node] = {}
        for n in self.rg.state_nodes:
            if not n.states_values_str() in livelocks and not any(n.find_paths('states', self.home_states)):
                livelocks[n.states_values_str()] = n
        self.livelocks = list(livelocks.values())

    def __find_choice_violations(self) -> None:
        'States from which both a non-triggered and triggered transition can be done'
        nodes: Dict[str, Node] = {}
        for node in self.rg.state_nodes:
            if node.states_values_str() in nodes: continue
            triggerd_transitions = [e for e in node.outgoing if e.has_event and e.direction == 'client']
            if len(triggerd_transitions) != 0 and len(triggerd_transitions) != len(node.outgoing):
                nodes[node.states_values_str()] = node
        self.choice_violations = list(nodes.values())

    def __find_scc(self) -> None:
        '''Find strongly connected components using Tarjan's algorithm'''
        self.scc: List[List[str]]  = []
        for l in [l for l in self.__scc() if any(n.is_state for n in l)]:
            states = sorted(list(set([n.states_values_str() for n in l if n.is_state])))
            if not states in self.scc:
                self.scc.append(states)

    def __scc(self) -> List[List['Node']]:
        nodes: Dict[str, Dict[str, Any]] = dict((n.id, {"visited": False, "id": -1, "low": -1, "node": n}) for n in self.rg.nodes.values())
        for node in nodes.values():
            if not node['visited']:
                self.__scc_dfs(node, nodes, [])

        result: Dict[int, List[Node]] = {}
        for v in nodes.values():
            if not v['low'] in result: result[v['low']] = []
            result[v['low']].append(v['node'])

        return list(result.values())
    
    def __scc_dfs(self, node: Dict[str, Any], nodes: Dict[str, Dict[str, Any]], stack: List[str]) -> None:
        stack.append(node['node'].id)
        node['visited'] = True
        node['id'] = node['low'] = len([n for n in nodes.values() if n['visited']])

        for neighbour in [e.target.id for e in node['node'].outgoing]:
            if not nodes[neighbour]['visited']: self.__scc_dfs(nodes[neighbour], nodes, stack)
            if neighbour in stack: node["low"] = min(node["low"], nodes[neighbour]["low"])
        
        if node["id"] == node["low"]:
            while True:
                n = stack.pop()
                nodes[n]['low'] = node['id']
                if n == node['node'].id: break

    def __find_simple_confusion(self) -> None:
        ''' 
        A simple confusion happens when the same event from a state node with different events in its path 
        lead to different state nodes. See SimpleConfusionTest.xtend for examples
        '''
        def key(x: Path) -> str:
            events = x.events()
            return str(events[0]) if len (events) > 0 else ''

        def first_confusion(paths: List[Path]) -> Optional[List[Path]]:
            for path1 in paths:
                is_command = path1.events()[0].kind == EventType.Command
                for path2 in paths:
                    if path1 == path2: continue
                    sequence = self.__common_sequence(path1, path2)
                    prefix_match = len(sequence.edges) == len(path1.edges)
                    if is_command and path1.first_event_edge().target != path2.first_event_edge().target:
                        if not prefix_match: return [path1, path2]
                    if prefix_match and not (path1.target() == path2.target() and len(sequence.events()) == len(path2.events())):
                        return [path1, path2]
        
        confusions: Dict[str, SimpleConfusion] = {}
        for node in self.rg.state_nodes:
            for event, paths in itertools.groupby(sorted(node.paths, key=key), key=key):
                if event == '': continue
                ckey = f"{node.states_values_str()}_{event}"
                if ckey in confusions: continue
                paths = list(paths)
                if len(paths) > 1:
                    confusion = first_confusion(paths)
                    if confusion != None:
                        confusions[ckey] = SimpleConfusion(node, confusion)

        self.simple_confusions = list(confusions.values())

    def __common_sequence(self, a: 'Path', b: 'Path') -> 'Path':
        last_match = None
        a_events = a.event_edges()
        b_events = b.event_edges()
        for i in range(len(a_events)):
            if not len(b_events) > i or b_events[i].get_event() != a_events[i].get_event(): break
            last_match = a_events[i]
        return Path([]) if last_match == None else a.slice_till_next_event_edge(last_match)

    def __find_generalized_confusion(self) -> None:
        ''' 
        A generalized confusion happens when the same signal or notification can be executed in another path of the source node before seeing
        an event in the other direction (notification for server, signal for client)
        '''
        notification_signal = [EventType.Signal, EventType.Notification]
        confusions: Dict[str, GeneralizedConfusion] = {}
        for node in self.rg.nodes.values():
            if len(node.paths) < 2: continue
            client_paths = [p for p in node.find_paths('first_client_event') if p.first_event_edge().get_event().kind in notification_signal]
            server_paths = [p for p in node.find_paths('first_server_event') if p.first_event_edge().get_event().kind in notification_signal]

            for path in node.paths:
                if len(path.event_edges()) == 0: continue 
                first_event_edge = path.first_event_edge()
                if not first_event_edge.get_event().kind in notification_signal: continue
                key = f"{node.friendly_name()}_{first_event_edge.get_event().method}"
                if key in confusions: continue
                event = first_event_edge.get_event()
                other_paths = server_paths if first_event_edge.direction == 'client' else client_paths
                for other_path in other_paths:
                    if other_path.first_event_edge().get_event() == first_event_edge.get_event(): continue
                    conflict = next((e for e in other_path.edges if e.has_event and e.get_event() == event), None)
                    if conflict != None:
                        conflict_path = other_path.slice_till_next_event_edge(conflict)
                        confusions[key] = GeneralizedConfusion(node, path.slice_till_next_event_edge(first_event_edge), first_event_edge, conflict_path)
                        break
        self.generalized_confusions = list(confusions.values())

    def __find_race_conditions_compute(self, client_edge: 'Edge', server_edge: 'Edge'):
        @dataclasses.dataclass 
        class DItem:  
            edge: Optional[Edge]; path: List['Edge']; queue: List[Event]; side: str
            def copy(self, edge: Optional[Edge]=None): 
                return DItem(self.edge if edge == None else edge, self.path.copy(), self.queue.copy(), self.side)
        class ItemState(str, enum.Enum): ACTIVE = 'active'; DELETE = 'delete'; INVALID = 'invalid'; COMPLETE = 'complete'
        @dataclasses.dataclass 
        class Item: 
            client: DItem; server: DItem; side: str; state: ItemState 
            problem_event: Optional[Event] = None; problem_node: Optional[Node] = None; problem_type: Optional[str] = None

        def problem_node(ditem: DItem) -> Node:
            return next((e.target for e in reversed(ditem.path) if e.target.is_state or e.has_event), ditem.path[0].source)

        def is_parameters_problem(ditem: DItem, ditem_other: DItem) -> bool:
            return (ditem.side == 'server' and ditem_other.queue[0].kind == EventType.Command and
                any(e for e in ditem.path[-1].target.outgoing if e.direction == 'client' and e.get_event().method == ditem_other.queue[0].method))

        def max_repeat(event: Event, client_node: 'Node') -> int:
            comp: Callable[['Edge'], bool] = lambda edge: not edge.has_event or (edge.get_event() == event)
            paths = [[e] for e in client_node.outgoing.copy() if comp(e)]
            for path in paths:
                while True:
                    next_edges = [e for e in path[-1].target.outgoing if comp(e) and not e in path]
                    if len(next_edges) == 0: break
                    for next_edge in next_edges[1:]: paths.append([*path, next_edge])
                    path.append(next_edges[0])
            return max([len([e for e in p if e.has_event]) for p in paths], default=1) + 1

        assert client_edge.has_event and not server_edge.has_event
        items = [Item(DItem(client_edge, [], [], 'client'), DItem(server_edge, [], [], 'server'), 'client', ItemState.ACTIVE)]
        for item in items:
            if item.state != ItemState.ACTIVE: continue
            while True:
                ditem = item.client if item.side == 'client' else item.server
                ditem_other = item.server if item.side == 'client' else item.client
                if ditem.edge == None:
                    if len(ditem_other.queue) != 0:
                        item.state = ItemState.INVALID
                        item.problem_event = ditem_other.queue[0]
                        item.problem_node = problem_node(ditem)
                        if is_parameters_problem(ditem, ditem_other):
                             item.problem_type = 'parameters'
                             item.side = 'client'
                        else:
                            item.problem_type = 'event'
                    else:
                        item.state = ItemState.DELETE
                    break

                reply_missing = False
                if ditem.edge.direction != None and ditem.edge.direction != item.side:
                    if len(ditem_other.queue) == 0:
                        item.side = self.inverse_direction_lookup[item.side]
                        continue
                    
                    event = ditem_other.queue[0]
                    if ditem.edge.get_event().kind == EventType.Reply:
                        assert item.side == 'client'
                        event = next((e for e in ditem_other.queue if e.kind == EventType.Reply), None)
                        if event == None: reply_missing = True

                    if event != None and event != ditem.edge.get_event():
                        item.problem_event = event
                        item.problem_node = problem_node(ditem)
                        item.problem_type = 'event'
                        item.state = ItemState.INVALID
                        break

                    if event != None: 
                        ditem_other.queue.pop(ditem_other.queue.index(event))

                if not reply_missing:
                    ditem.path.append(ditem.edge)
                    if ditem.edge.has_event and ditem.edge.direction == ditem.side: 
                        ditem.queue.append(ditem.edge.get_event())

                if ditem.edge.target.is_state:
                    if len(item.client.queue) == 0 and len(item.server.queue) == 0:
                        item.state = ItemState.COMPLETE
                        break
                    elif ditem.side == 'client':
                        next_edges = [e for e in ditem.edge.target.outgoing if e.direction == None]
                        if len(ditem_other.queue) == 0:
                            item.side = self.inverse_direction_lookup[ditem.side]
                    else: # ditem.side == 'server'
                        if len(ditem_other.queue) != 0:
                            next_edges = [e for e in ditem.edge.target.outgoing  if e.direction == 'client' and e.get_event() == item.client.queue[0]]
                        else:
                            next_edges = []                        
                else:
                    next_edges = ditem.edge.target.outgoing
                    if ditem.side == 'server':
                        loop_edge = next((e for e in next_edges if e.source == e.target), None)
                        if loop_edge != None:
                            # Don't loop endlessly for loop edges (e.g. Notification1*), figure out max needed repetition based on the client side.
                            assert len(next_edges) == 2 and loop_edge.get_event().kind == EventType.Notification and loop_edge.has_event
                            repeat = max_repeat(loop_edge.get_event(), ditem_other.path[-1].target)
                            ditem.path += ([loop_edge] * repeat)
                            ditem.queue += ([loop_edge.get_event()] * repeat)
                            next_edges = [e for e in next_edges if e != loop_edge]

                if len(next_edges) == 0:
                    # Don't report a problem when the reply is missing, in this case we are at the client side
                    # and the server could not handle the command. A problem will already show up on the server side
                    if len(ditem_other.queue) != 0 and not reply_missing:
                        problem_node_ = ditem.edge.target if reply_missing else problem_node(ditem)   
                        if is_parameters_problem(ditem, ditem_other):
                            items.append(Item(
                                item.client.copy(), item.server.copy(), 'client', ItemState.INVALID, ditem_other.queue[0], problem_node_, 'parameters'))
                        else:
                            items.append(Item(
                                item.client.copy(), item.server.copy(), ditem.side, ItemState.INVALID, ditem_other.queue[0], problem_node_, 'event'))
                    ditem.edge = None
                    item.side = self.inverse_direction_lookup[ditem.side]
                else:
                    ditem.edge = next_edges[0]
                    for edge in next_edges[1:]:
                        ditem_client = item.client.copy(edge if ditem.side == 'client' else None)
                        ditem_server = item.server.copy(edge if ditem.side == 'server' else None)
                        items.append(Item(ditem_client, ditem_server, item.side, item.state))

        return [i for i in items if i.state != ItemState.DELETE]

    def __find_race_conditions(self) -> None:
        '''
        A race condition happens when the client or server end up in a state where it cannot handle the events of the
        server/client.
        '''
        self.race_conditions = []
        seen_problems: Set[str] = set()

        for node in self.rg.state_nodes:
            edges: Dict[str, List[Edge]] = {'client': [], 'server': []}
            for edge in node.outgoing: edges['client' if edge.direction == 'client' else 'server'].append(edge)
            for client_edge in edges['client']:
                for server_edge in edges['server']:
                    results = self.__find_race_conditions_compute(client_edge, server_edge)
                    for side in ['client', 'server']:
                        problems = [r for r in results if r.state == 'invalid' and r.side == side]
                        if len(problems) != 0 and not any(r for r in results if r.state != 'invalid'):
                             # Cases where the client or server cannot handle events from the other party
                            r = sorted(problems, key=lambda x: len((x.client if side == 'client' else x.server).path), reverse=True)[0]
                            assert r.problem_event != None and r.problem_node != None and r.problem_type != None
                            key = f"{node.friendly_name()}_{r.problem_event.method}_{r.problem_node.friendly_name()}_{side}"
                            if key in seen_problems: continue
                            seen_problems.add(key)
                            category = 'error' if r.problem_type == 'event' else 'warning'
                            self.race_conditions.append(RaceCondition(node, r.side, r.client.path, r.server.path, category, r.problem_type, 
                                r.problem_event, r.problem_node, None))

                    for r in [r for r in results if r.state == 'complete' and r.client.path[-1].target != r.server.path[-1].target]:
                        # Cases where client and server can accept all events but end up in other states (not a full diamond)
                        self.race_conditions.append(RaceCondition(node, 'client', r.client.path, r.server.path, 'warning', 
                            'state', None, None, [r.client.path[-1].target, r.server.path[-1].target]))
                        self.race_conditions.append(RaceCondition(node, 'server', r.client.path, r.server.path, 'warning', 
                            'state', None, None, [r.client.path[-1].target, r.server.path[-1].target]))

    def plantuml_event(self, event: 'Event') -> str:
        if event.kind == EventType.Reply: name = f"reply({(', '.join([str(p.value) for p in event.parameters]))})"
        else: name = event.__str__(include_prefix=False, include_port=False)
        split_size = 25
        chunks = [name[i:i+split_size] for i in range(0, len(name), split_size)]
        return "\\n".join(chunks)
        
    def plantuml_trace_generalized_confusion(self, confusion: GeneralizedConfusion) -> str:
        assert confusion.event_edge.direction != None
        direction = confusion.event_edge.direction
        inverse_direction = 'client' if direction == 'server' else 'server'
        result = self.plantuml_trace_start(confusion.conflict.edges[0].source, True)
        result += self.plantuml_trace_events({inverse_direction: confusion.path.edges, direction: confusion.conflict.edges}, True)
        result += "@enduml"
        return result

    def plantuml_trace_weak_termination(self, path: 'Path') -> str:
        result = "@startuml\nparticipant client\nparticipant middleware\nparticipant server\n\n"
        result += f"note over client: {path.source().states['client']}\n"
        result += f"note over server: {path.source().states['server']}\n"
        events = {
            'client': [e for e in path.edges if e.transition.name.startswith('client_')],
            'server': [e for e in path.edges if e.transition.name.startswith('server_')]
        }
        def state_cb(node: Node, side: str) -> Optional[str]: 
            return node.states[side] if side in node.states else None

        result += self.plantuml_trace_events(events, True, state_cb)
        result += "\n@enduml"
        return result

    def plantuml_trace_simple_confusion(self, confusion: SimpleConfusion) -> str:
        result = self.plantuml_trace_start(confusion.node, True)
        sorted_paths = sorted(confusion.paths, key= lambda x: len(x.events()))
        result += self.plantuml_trace_events({'client': sorted_paths[0].edges, 'server': sorted_paths[1].edges}, True)
        result += "@enduml"
        return result

    def plantuml_trace_race_condition(self, race: RaceCondition) -> str:
        result = self.plantuml_trace_start(race.node, True)
        result += self.plantuml_trace_events({'client': race.client_path, 'server': race.server_path}, True)

        if race.problem_type == 'event' or race.problem_type == 'parameters':
            assert race.problem_event != None
            direction = 'server' if race.problem_type == 'parameters' else race.direction
            inverse_direction = self.inverse_direction_lookup[direction]
            result += f"{self.plantuml_lookup[inverse_direction][1]}: {self.plantuml_event(race.problem_event)}\n"
            result += f"destroy {direction}"

        result += "\n@enduml"
        return result

    def plantuml_trace_events(self, cs_edges: Dict[str, List['Edge']], include_states:bool, 
        state_cb: Optional[Callable[['Node', str], Optional[str]]]=None) -> str:
        result = ""
        cs_edges = {'client': cs_edges['client'].copy(), 'server': cs_edges['server'].copy()}
        direction = next((e.direction for e in cs_edges['client'] if e.direction != None), 'client')
        client = False
        clientBlocked = False
        while not (len(cs_edges['client']) == 0 and len(cs_edges['server']) == 0):
            if len(cs_edges[direction]) == 0: direction = self.inverse_direction_lookup[direction]
            if direction == 'client': client = True
            seen_directions: Set[Any] = set()
            while len(cs_edges[direction]) != 0:
                edge = cs_edges[direction][0]
                if ((direction == 'client' and 'client' in seen_directions and edge.direction == 'server') or
                    (not client and edge.direction == 'client') or
                    (direction == 'server' and 'client' in seen_directions and edge.direction == 'client')):
                    direction = self.inverse_direction_lookup[direction]
                    break

                cs_edges[direction].pop(0)
                seen_directions.add(edge.direction)
                if edge.has_event:
                    edge_direction = f"{direction}_{edge.get_event().kind}"
                    if (direction == 'client' and not clientBlocked) or (direction == 'server'):
                        result += f"{self.plantuml_lookup[edge_direction]}: {self.plantuml_event(edge.get_event())}\n"
                    if client and edge_direction.endswith('Command'): clientBlocked = True
                    if edge_direction.startswith('server') and edge_direction.endswith('Reply'): clientBlocked = False
                if include_states and ((direction == 'client' and not clientBlocked) or (direction == 'server')):
                    if state_cb != None and state_cb(edge.target, direction) != None:
                        result += f"note over {direction}: {state_cb(edge.target, direction)}\n"
                    elif edge.target.is_state:
                        result += f"note over {direction}: {edge.target.friendly_name()}\n"
        return result

    def plantuml_trace_start(self, node: 'Node', include_states: bool) -> str:
        result = ""
        if include_states: 
            result += f"note over client: {self.rg.initial.states_values_str()}\n"
            result += f"note over server: {self.rg.initial.states_values_str()}\n"
        if self.rg.initial != node:
            path = next(self.rg.initial.find_paths('node', node), None)
            assert path != None, f'No path found from initial to {node.id}'
            result += self.plantuml_trace_events({'client': path.edges, 'server': path.edges}, include_states)
        return inspect.cleandoc(f"""
        @startuml
        participant client
        participant middleware
        participant server

        """) + '\n\n' + result
        
    def plantuml_trace_node(self, node: 'Node', message: str) -> str:
        return self.plantuml_trace_start(node, True) + '\n' + inspect.cleandoc(f"""
        note over client, server
        {message}
        end note
        @enduml
        """)

    def to_json(self) -> Dict[str, Any]:
        serverRaceConditions: List[Dict[str, Any]] = []
        clientRaceConditions: List[Dict[str, Any]] = []
        
        def event_str(event: Event):
            if event.kind == EventType.Reply:
                return f"{event.method}_reply({(', '.join([str(p.value) for p in event.parameters]))})"
            else:
                return event.__str__(include_prefix=False, include_port=False)

        for race in self.race_conditions:
            message = f"Race condition for {race.direction} in state {race.node.friendly_name()}"
            if race.problem_type == 'event':
                assert race.problem_event != None and race.problem_node != None
                resolution = f"Allow {event_str(race.problem_event)} in {race.problem_node.friendly_name()}"
            elif race.problem_type == 'parameters':
                assert race.problem_event != None and race.problem_node != None
                parameters = ', '.join([str(p.value) for p in race.problem_event.parameters])                
                resolution = f"Extend the .params file with parameter '{parameters}' for command {race.problem_event.method} in {race.problem_node.friendly_name()}"
            else:
                assert race.problem_nodes
                resolution = f"Resulting states are different: {', '.join([n.friendly_name() for n in race.problem_nodes])}"
            result = {"message": message, "image": self.plantuml_trace_race_condition(race), "resolution": resolution, "category": race.category}
            serverRaceConditions.append(result) if race.direction == 'server' else clientRaceConditions.append(result)

        return {
            "statistics": {
                "reachabilityGraphCoverage": ("full" if self.rg.max_depth == None or self.rg.depth < self.rg.max_depth else "partial"),
                "transitionCoverage": self.coverage['transition'],
                "stateCoverage": self.coverage['state'],
                "stateCoverageMachine": self.coverage['state_machine'],
                "numOfStates": len(set(n.states_values_str() for n in self.rg.state_nodes)),
            },
            "deadlocks": [{
                "message": f"Deadlock found in {n.friendly_name()}",
                "state": n.states_values_str() if n.is_state else None,
                "image": self.plantuml_trace_node(n, f"Deadlock found in {n.friendly_name()}"),
                } for n in self.deadlocks],
            "livelocks": [{
                "message": f"State {n.states_values_str()} does not have a path to any of the home states",
                "state": n.states_values_str(),
                "image": self.plantuml_trace_node(n, f"No path from {n.states_values_str()} to home state was found"),
                } for n in self.livelocks],
            "unreachableStates": [{"message": s} for s in self.unreachable_states],
            "sinkStates": [{
                "message": f"State {n.states_values_str()} is a sink",
                "image": self.plantuml_trace_node(n, f"State {n.states_values_str()} is a sink"),
                } for n in self.sinks],
            "choiceProperties": [{"message": f"State {s} has non-triggered and triggered transitions"} for s in sorted([n.states_values_str() for n in self.choice_violations])],
            "SCC": [{"message": ', '.join(sorted(s))} for s in self.scc],
            "serverRaceConditions": serverRaceConditions,
            "clientRaceConditions": clientRaceConditions,
            "simpleConfusions": [{
                "message": f"State {c.node.friendly_name()} has non-deterministic events leading to different states",
                "image": self.plantuml_trace_simple_confusion(c),
                "reason1": ', '.join([event_str(e) for e in c.paths[0].events()]),
                "reason2": ', '.join([event_str(e) for e in c.paths[1].events()])
                } for c in self.simple_confusions],
            "generalizedConfusions": [{
                "message": f"Confusion detected in state: {c.node.friendly_name()}",
                "image": self.plantuml_trace_generalized_confusion(c),
                "confusionEvent": c.event_edge.get_event().__str__(include_prefix=False, include_port=False),
                "eventsToConfusion": [event_str(e) for e in c.conflict.events()],
                "statesToConfusion": [e.target.states_values_str() for e in c.conflict.edges if e.target.is_state]
                } for c in self.generalized_confusions],
        }
