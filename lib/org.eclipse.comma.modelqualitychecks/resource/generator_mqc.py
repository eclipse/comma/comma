#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

if __name__ == "__main__":
    import os, argparse, re, tempfile, subprocess, base64, datetime, json
    assert len(nets.values()) == 1

    def write(file: str, content: Any, mode: str):
        with open(file, mode) as f: f.write(content)

    def read(file: str, mode: str) -> Any:
        with open(file, mode) as f: return f.read()

    def modify_report_for_dashboard(report: Dict[str, Any]) -> Dict[str, Any]:
        report['meta'] = {'createdAt': datetime.datetime.now().isoformat()}
        report['interface'] = {'content': INTERFACE}

        # Render images
        to_render: List[Dict[str, str]] = []
        for items in [v for v in report.values() if isinstance(v, list)]:
            to_render.extend([i for i in items if 'image' in i])
        with tempfile.TemporaryDirectory() as tmp:
            for index, item in enumerate(to_render):
                write(os.path.join(tmp, f"{index}.txt"), item['image'], 'w')
            subprocess.run([JAVA, '-jar', PLANTUML, tmp])
            for index, item in enumerate(to_render):
                item['image'] = base64.b64encode(read(os.path.join(tmp, f"{index}.png"), 'rb')).decode('utf8')

        return report

    def add_report_to_dashboard(dashboard: bytes, report: Dict[str, Any]):
        newline = '\n'.encode()
        report_bytes = json.dumps(report).encode('utf8')
        report_header_end = dashboard.index(newline)
        report_header = dashboard[:report_header_end].decode('utf8')
        report_start, report_end = [int(g) for g in re.match(r'<!-- REPORT_PLACEHOLDER = (\d+):(\d+) -->', report_header).groups()]
        new_report_header = f"<!-- REPORT_PLACEHOLDER = {report_start}:{report_start + len(report_bytes)} -->\n".encode('utf8')
        dashboard = dashboard[report_header_end + 1:]
        dashboard_before_report = dashboard[:report_start]
        dashboard_after_report = dashboard[report_end:]
        dashboard = new_report_header + dashboard_before_report + report_bytes + dashboard_after_report
        return dashboard

    parser = argparse.ArgumentParser()
    parser.add_argument('--output', default='file', choices=['file', 'print'])
    args = parser.parse_args()

    net = list(nets.values())[0]()
    rg = ReachabilityGraph(net, True, MAX_DEPTH)
    vr = VerificationReport(net, rg, HOME_STATES)
    vr_json = vr.to_json()

    if args.output == 'file':
        directory = os.path.dirname(__file__)
        target = os.path.join(directory, OUTPUT_FILE)
        write(target, json.dumps(vr_json, indent=4), 'w')

        # Dashboard
        dashboard_file = os.path.join(directory, 'dashboard.html')
        dashboard_report = modify_report_for_dashboard(vr_json)
        dashboard = read(dashboard_file, 'rb')
        dashboard = add_report_to_dashboard(dashboard, dashboard_report)
        write(dashboard_file, dashboard, 'wb')
    else:
        print(json.dumps(vr_json, indent=4))
