#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import enum, json, re
from typing import Dict, Any, List, Optional, Union, Tuple
from dataclasses import dataclass, field


class EventType(enum.Enum):
    Command = 0
    Reply = 1
    Notification = 2
    Signal = 3
    Any = 4


class PortDirection(enum.Enum):
    Unknown = 0
    Provided = 1
    Required = 2


class ANY(object):
    def __eq__(self, obj: Any):
        return True

    def __repr__(self):
        return "ANY"


class Parameters:
    def __init__(self, parameters: List[Any]):
        self.value = parameters

    def __repr__(self):
        return f"[{','.join([str(v) for v in self.value])}]"

    def __hash__(self):
        return id(self)

    def __getitem__(self, item: int):
        return self.value[item]

    def v(self, variables: List[str]):
        assert len(variables) == len(self.value)
        return Variables(dict(zip(variables, self.value)))
    
    def vdict(self, variables: List[str]):
        assert len(variables) == len(self.value)
        return dict(zip(variables, self.value))

class Variables:
    def __init__(self, variables: Dict[str, object]):
        for key in variables.keys():
            setattr(self, key, variables[key])

    def copy(self):
        return Variables(json.loads(json.dumps(vars(self))))

    def gl(self, l: Optional['Variables'] = None):
        if l == None: l = Variables({})
        return VariablesGL(self, l)

    def vars(self) -> Dict[str, Any]:
        v = vars(self)
        return dict([(k, v[k]) for k in v.keys()])

    def __repr__(self):
        return "{%s}" % ','.join([f"{k}:{v}" for k, v in self.vars().items()])

    def __hash__(self):
        return id(self)
    
    def add(self, variables: Dict[str, object]):
        for key in variables.keys():
            setattr(self, key, variables[key])
        return self
        
    def match(self, s : str, pattern : str):
        return re.match(s, pattern) != None

class VariablesGL:
    g: Variables
    l: Variables
    r: Optional[int]

    def __init__(self, g: Variables, l: Variables):
        self.g = g
        self.l = l

    def copy(self):
        return VariablesGL(self.g.copy(), self.l.copy())

    def e(self, expr: str):
        l = self.l.copy()
        g = self.g.copy()
        exec(expr, {}, {'g': g, 'l': l})
        return VariablesGL(g, l)

    def er(self, expr: str):
        v = VariablesGL(self.g, self.l)
        if hasattr(self, 'r'): v.r = self.r
        exec("v.r %s" % expr, {}, {'v': v})
        return v

    def __repr__(self):
        v = vars(self)
        return "{%s}" % ','.join(["%s:%s" % (k, v[k]) for k in v.keys()])

    def __hash__(self):
        return hash((self.g, self.l))

    def __eq__(self, obj: object) -> bool:
        return self.l == obj.l and self.g == obj.g if isinstance(obj, VariablesGL) else False
    
    def add(self, variables: Dict[str, object]):
        self.l.add(variables)
        return self

@dataclass
class Parameter:
    type: Optional[Union[str, Dict[str, object]]]
    expression: Optional[str]
    value: object = None

    @staticmethod
    def from_json(jsn: Dict[str, object]) -> 'Parameter':
        value = jsn['value']
        return Parameter(None, None, value)

    def to_json(self):
        assert self.type != None, "Cannot convert parameter to_json, type info is missing"
        def recurse(t: Union[str, Dict[str, object]], value: object, first: bool = False) -> Dict[str, object]:
            if type(t) is dict:
                if t['type'] == 'vector':
                    if t['dimensions'] == 1:
                        typeElem = t['typeElem']['type'] if type(t['typeElem']) is dict else t['typeElem'] # type: ignore
                        if isinstance(value, ANY):
                           [recurse(t['typeElem'], v) for v in value] # type: ignore
                        if typeElem == 'record':
                            return {'type': 'vector', 'typeElem': typeElem, 'value': [{'type': 'record', 'record': t['typeElem']['record'], 'value': v} for v in value]}   
                        else:
                            return {'type': 'vector', 'typeElem': typeElem, 'value': value}
                    else:
                        if isinstance(value, ANY):
                            value = [recurse({**t, 'dimensions': t['dimensions'] - 1}, v) for v in value] # type: ignore
                        return {'type': 'vector', 'typeElem': 'vector', 'value': [recurse({**t, 'dimensions': t['dimensions'] - 1}, v) for v in value]}
                elif t['type'] == 'map':
                    typeValue = t['typeValue']['type'] if type(t['typeValue']) is dict else t['typeValue'] # type: ignore
                    if isinstance(value, ANY):
                        value = dict([(k, recurse(t['typeValue'], v)) for k, v in value.items()]) # type: ignore
                    return {'type': 'map', 'typeKey': t['typeKey'], 'typeValue': typeValue, 'value': value}
                else:
                    if isinstance(value, ANY):
                        value = dict([(f['name'], recurse(f['type'], value[f['name']])) for f in t['fields']]) # type: ignore
                    return {'type': 'record', 'record': t['record'], 'value': value}
            elif first:
                return {'type': t, 'value': value}
            else:
                return value # type: ignore

        return recurse(self.type, self.value, True)
    
    def eval(self, mode: Optional[Dict[str, Any]]=None) -> 'Parameter':
        assert self.expression != None and self.type != None, "Cannot convert parameter to_json, type info is missing"
        if self.type == "enum" and (":" in self.expression) and not self.expression.startswith('"'):
            self.expression = '"' + self.expression + '"'
        value = eval(self.expression, {'ANY': ANY}, mode)
        return Parameter(self.type, self.expression, value)

@dataclass
class Event:
    kind: EventType
    interface: str
    port: str
    component: str
    method: str
    parameters: List[Parameter]
    portDirection: PortDirection
    breakpoint: bool

    def equals(self, other: 'Event', skip_parameters: bool):
        if self.kind == EventType.Any:
            return self.interface == other.interface and self.port == other.port and (self.component == '' or self.component == other.component)
        if skip_parameters:
            return self.kind == other.kind and (self.method == '' or self.method == other.method) and self.interface == other.interface and self.port == other.port
        else:
            return self == other
            
    def has_breakpoint(self):
        return self.breakpoint
        
    def is_server_event(self):
        return self.kind == EventType.Reply or self.kind == EventType.Notification

    def is_client_event(self):
        return self.kind == EventType.Command or self.kind == EventType.Signal

    def is_provided_port(self):
        return self.portDirection == PortDirection.Provided

    def is_required_port(self):
        return self.portDirection == PortDirection.Required

    def __eq__(self, other: Any):
        if (isinstance(other, Event) and self.kind == other.kind and self.method == other.method and
                self.interface == other.interface and len(self.parameters) == len(other.parameters)):
            for idx, parameter in enumerate(self.parameters):
                p1 = parameter.value
                p2 = other.parameters[idx].value
                assert (p1 != None or isinstance(p1, ANY)) and (p2 != None or isinstance(p2, ANY)), "Parameters not evaluated"
                if p1 != p2:
                    return False
            return True
        return False

    def __str__(self, skip_parameters:bool=False, include_prefix:bool=True, include_port:bool=True) -> str:
        name = ''
        if include_prefix: name = f"{self.kind.name[0]}: " + name
        if include_port:
            name += f"{self.component}."    
            name += f"{self.port}."
        name += self.method
        if skip_parameters:
            return name + "()"
        else:
            return name + f"({', '.join([str(p.value) for p in self.parameters])})"

    def to_json(self):
        return {'kind': self.kind.name, 'method': self.method, 'parameters': [p.to_json() for p in self.parameters], 
            'interface': self.interface, 'port': self.port, 'component': self.component}

    @staticmethod
    def from_json(jsn: Dict[str, Any]) -> 'Event':
        try:
            return Event(EventType[jsn['kind']], jsn['interface'], jsn['port'], jsn['component'], jsn['method'], [Parameter.from_json(p) for p in jsn['parameters']], jsn['direction'], jsn['breakpoint'])
        except:
            return Event(EventType[jsn['kind']], jsn['interface'], jsn['port'], jsn['component'], jsn['method'], [Parameter.from_json(p) for p in jsn['parameters']], "", "")

    def eval(self, mode: Dict[str, Any]) -> 'Event':
        return Event(self.kind, self.interface, self.port, self.component, self.method, [p.eval(mode) for p in self.parameters], self.portDirection, self.breakpoint)
    
    def connectionKey(self):
        return (self.port, self.component)

@dataclass
class ConstraintState:
    state: str
    index: Optional[Tuple[int, int]] = None
    variables: Variables = None
    
class Constraint:
    def take(self, event: Event, port_machine_state: Dict[str, Dict[str, str]]) -> Optional['Constraint']: ...
    def get_state(self) -> List[ConstraintState]: ...
    def set_state(self, state: List[ConstraintState]): ...
    
    def connectionAtPort(self, component: str, port: str, states: set, port_machine_state: Dict[tuple, Dict[str, str]]):
        return len(set(port_machine_state[(port, component)].values()).intersection(states)) > 0
        
    def allAtPort(self, port: str, states: set, port_machine_state: Dict[tuple, Dict[str, str]]):
        for (connection, machine_state) in port_machine_state.items():
            if connection[0] == port:
                found = False
                for (machine, state) in machine_state.items():
                    if state in states:
                        found = True
                if not found:
                    return False
        return True
        
    def someAtPort(self, port: str, states: set, port_machine_state: Dict[tuple, Dict[str, str]]):
        for (connection, machine_state) in port_machine_state.items():
            if connection[0] == port:
                for (machine, state) in machine_state.items():
                    if state in states:
                        return True
        return False
        
    def oneAtPort(self, port: str, states: set, port_machine_state: Dict[tuple, Dict[str, str]]):
        i = 0
        for (connection, machine_state) in port_machine_state.items():
            if connection[0] == port:
                for (machine, state) in machine_state.items():
                    if state in states:
                        i = i + 1
        if i == 1:
            return True
        return False
        
    def intervalAtPort(self, l: int, u : int, port: str, states: set, port_machine_state: Dict[tuple, Dict[str, str]]):
        i = 0
        for (connection, machine_state) in port_machine_state.items():
            if connection[0] == port:
                for (machine, state) in machine_state.items():
                    if state in states:
                        i = i + 1
        if (i <= u and l <= i) or (l <= i and u == -1):
            return True
        return False
        
    def portInState(self, port: str, state: str, port_machine_state: Dict[tuple, Dict[str, str]]):
        return self.allAtPort(port, {state}, port_machine_state)
