#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

from typing import Tuple, Dict, List, Set, Any, Generator
import collections
from snakes.data import Substitution
from snakes.nets import PetriNet, Marking, Place, Transition
if not 'SELF_CONTAINED' in globals():
    from model import Event


class Node:
    def __init__(self, id: str, places: List[Place]) -> None:
        self.id = id
        self.incoming: List['Edge'] = []
        self.outgoing: List['Edge'] = []
        machine_places = [p for p in places if 'machine' in p.meta]
        self.states: Dict[str, str] = dict([(p.meta['machine'], p.meta['state']) for p in machine_places if 'state' in p.meta])
        self.is_state = all(p.meta['type'] == 'state' for p in machine_places)
        self.place_types: List[str] = sorted(list(set([p.meta['type'] for p in places])))
        self.paths: List['Path'] = []
        self.next_state_nodes: Set['Node'] = set()
        self.previous_state_nodes: Set['Node'] = set()

    def to_json(self) -> Dict[str, Any]: 
        return {'place_types': self.place_types, 'friendly_name': self.friendly_name()}

    def states_values_str(self) -> str:
        assert self.states != None 
        return '_'.join(sorted(self.states.values()))
    
    def has_states(self, states: List[str]) -> bool:
        return all(s in self.states.values() for s in states)

    def friendly_name(self) -> str:
        if self.is_state:
            return '_'.join(sorted(self.states.values()))
        elif len(self.previous_state_nodes) != 0:
            states: Dict[str, str] = {}
            for n in self.previous_state_nodes: states.update(n.states)
            return f"{'_'.join(sorted(states.values()))}_transient"
        else:
            return self.id

    def __find_paths_non_state_node(self, state_node: 'Node', yielded: List['Path'], node: 'Node', prepend: List['Path'] = []) -> Generator['Path', None, None]:
        found = False
        for path in state_node.paths:
            edges: List['Edge'] = []
            for edge in path.edges:
                edges.append(edge)
                if edge.target == node:
                    p = Path.combine([*prepend, Path(edges)])  
                    yielded.append(p)
                    found = True
                    yield p
        if not found:
            paths: List[List[Edge]] = [[]]
            for path in paths:
                edges = state_node.outgoing if len(path) == 0 else path[-1].target.outgoing
                edges = [e for e in edges if not e in path]
                for edge in edges:
                    if edge.target == node: 
                        p = Path.combine([*prepend, Path([*path, edge])])  
                        yielded.append(p)
                        yield p
                    else: 
                        paths.append([*path, edge])

    def find_paths(self, stop: str, obj: Any=None) -> Generator['Path', None, None]:
        event_lookup = {'first_client_event': 'client', 'first_server_event': 'server'}
        lpaths = collections.deque([[p] for p in self.paths])
        seen_nodes: Set[Node] = set()
        yielded: List[Path] = []
        skipped: List[Path] = []

        def _yield(path: Path) -> Generator[Path, None, None]:
            yielded.append(path)
            yield path

        if stop == 'node' and not obj.is_state and self in obj.previous_state_nodes:
            yield from self.__find_paths_non_state_node(self, yielded, obj)
            return
        
        while len(lpaths) != 0:
            lpath = lpaths.popleft()
            target = lpath[-1].target()
            if stop == 'node' and (target == obj if obj.is_state else target in obj.previous_state_nodes): 
                if not obj.is_state:
                    yield from self.__find_paths_non_state_node(target, yielded, obj, lpath)
                else:
                    yield from _yield(Path.combine(lpath))
            if stop == 'states' and any(target.has_states(s) if type(s) is list else target.states_values_str() == s for s in obj): 
                yield from _yield(Path.combine(lpath))
            if stop == 'first_client_event' or stop == 'first_server_event':
                event = next((e for e in lpath[-1].edges if e.direction == event_lookup[stop]), None)
                if event != None:
                    yield from _yield(Path.combine([*lpath[:-1], lpath[-1].slice_till_next_event_edge(event)]))
                    continue

            if not target in seen_nodes:
                seen_nodes.add(target)
                for p in target.paths:
                    lpaths.append([*lpath, p])
            else:
                skipped.append(Path.combine(lpath))

        for path in skipped:
            for y in yielded:
                for i in range(len(y.edges)):
                    if y.edges[i].source == path.target():
                        p = Path(path.edges + y.edges[i:])
                        yield p
                        break


class Edge:
    def __init__(self, source: Node, target: Node, transition: Transition, mode: Substitution) -> None:
        self.source = source
        self.target = target
        self.transition = transition
        self.event = transition.meta['event'].eval(mode) if transition.meta['type'] == 'event' else None
        self.has_event = self.event != None
        self.direction = None
        if self.event != None: 
            self.direction = 'client' if self.event.kind in [EventType.Signal, EventType.Command] else 'server'

    def get_event(self) -> 'Event':
        if self.event == None: assert False, "Edge has no event"
        return self.event

    def to_json(self) -> Dict[str, Any]:
        result: Dict[str, Any] = {'source': self.source.id, 'target': self.target.id, 'transition': self.transition.name}
        if self.event != None: result['event'] = self.event.to_json()
        return result


class Path(object):
    def __init__(self, edges: List[Edge] = []) -> None:
        self.edges = edges

    def event_edges(self) -> List[Edge]:
        return [e for e in self.edges if e.has_event]

    def events(self) -> List[Event]:
        return [e.get_event() for e in self.edges if e.has_event]

    def last_event_edge(self):
        return next(e for e in reversed(self.edges) if e.has_event)

    def first_event_edge(self):
        return next(e for e in self.edges if e.has_event)

    def slice_till_next_event_edge(self, edge: Edge) -> 'Path':
        idx = self.edges.index(edge)
        edges = self.edges[:idx + 1]
        for i in range(idx + 1, len(self.edges)):
            if not self.edges[i].has_event: edges.append(self.edges[i])
            else: break

        return Path(edges)       

    def target(self) -> Node:
        return self.edges[-1].target

    def source(self) -> Node:
        return self.edges[0].source

    @staticmethod
    def combine(paths: List['Path']) -> 'Path':
        edges: List[Edge] = []
        for p in paths: edges.extend(p.edges)
        return Path(edges)


class ReachabilityGraph:
    def __init__(self, net: PetriNet, extensive_lookups: bool, max_depth: int) -> None:
        self.net = net 
        self.max_depth = max_depth
        self.nodes: Dict[str, Node] = {}
        self.edges: List[Edge] = []
        self.__compute_nodes_edges()
        self.__compute_lookups(extensive_lookups)

    def __compute_lookups(self, extensive: bool):
        self.state_nodes = [n for n in self.nodes.values() if n.is_state]
        for edge in self.edges:
            edge.source.outgoing.append(edge)
            edge.target.incoming.append(edge)
        
        if extensive:
            # Compute node paths for state nodes to next state node
            for node in self.state_nodes:
                paths: List[List['Edge']] = [[]]
                seen_edges: Set['Edge'] = set()
                cnt = 0
                while cnt < len(paths):
                    path = paths[cnt]
                    edges = node.outgoing if len(path) == 0 else path[-1].target.outgoing
                    for edge in edges:
                        if edge in seen_edges or edge in path: continue
                        if edge.target.is_state:
                            for i in range(len(path)):
                                path[i].source.paths.append(Path([*path[i:], edge]))
                        else:
                            edge.target.previous_state_nodes.add(node)
                            paths.append([*path, edge])
                    cnt += 1

            # Compute next_state_nodes
            seen_nodes: Set[Node] = set()
            stack: List[Tuple[Node, Node]] = [(n, n) for n in self.state_nodes]
            while len(stack) != 0:
                state_node, node = stack.pop(0)
                for edge in node.incoming:
                    next_node = edge.source
                    if next_node in seen_nodes or next_node.is_state: continue
                    seen_nodes.add(next_node)
                    next_node.next_state_nodes.add(state_node)
                    stack.insert(0, (state_node, next_node))

    def __serialize_marking(self, marking: Marking) -> str:
        m = [e for e in marking.items() if not e[0].startswith("P_")]
        return '__'.join(["%s_%s" % (e[0], e[1]) for e in m])

    def __add_node(self, id: str, marking: Marking) -> Node:
        if id not in self.nodes:
            places = [self.net._place[p] for p in marking.keys()]
            self.nodes[id] = Node(id, places)
        return self.nodes[id]

    def __compute_nodes_edges(self) -> None:
        initial_marking = self.net.get_marking()
        intial_id = self.__serialize_marking(initial_marking)
        self.initial = self.__add_node(intial_id, initial_marking)
        self.depth: int = 0 
        seen_edge_keys: Set[str] = set()
        stack: List[Tuple[Node, Marking, int]] = [(self.initial, initial_marking, 0)]

        while len(stack) != 0:
            (source_node, source_marking, depth) = stack.pop(0)
            self.net.set_marking(source_marking)
            marking_changed = False
            for transition in [t for t in self.net.transition() if len(t.modes()) > 0]:
                if marking_changed: self.net.set_marking(source_marking)
                for mode in transition.modes():
                    if marking_changed: self.net.set_marking(source_marking)
                    transition.fire(mode)
                    marking_changed = True
                    target_marking = self.net.get_marking()
                    target_id = self.__serialize_marking(target_marking)
                    edge_key = f"{source_node.id}_{transition.name}_{target_id}"

                    if not edge_key in seen_edge_keys:
                        seen_edge_keys.add(edge_key)
                        target_node = self.__add_node(target_id, target_marking)
                        self.edges.append(Edge(source_node, target_node, transition, mode))
                        new_depth = depth + 1
                        if new_depth <= self.max_depth:
                            if new_depth > self.depth: self.depth = new_depth
                            stack.insert(0, (target_node, target_marking, new_depth))

    def to_json(self) -> Dict[str, Any]:
        return {
           "graph": {
                'metadata': {'max-depth': self.max_depth, 'depth': self.depth, 'initial': self.initial.id},
                'nodes': dict((n.id, n.to_json()) for n in self.nodes.values()),
                'edges': [e.to_json() for e in self.edges],
            }
        }
