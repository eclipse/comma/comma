#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

from dataclasses import dataclass
from typing import Optional, Dict, Callable, List, TypeVar, Set, Generator, Type, Any, Tuple
from snakes.nets import PetriNet, Marking, Place
if not 'SELF_CONTAINED' in globals():
    from model import Event, Constraint


@dataclass
class WalkerStep:
    marking: Marking
    constraints: List['Constraint']
    event: 'Event'
    breakStep: bool 

WalkerState = Tuple[Dict[str, Marking], List[Dict[str, Any]]]

class Walker():
    def __init__(self, nets: Dict[tuple, Callable[[], PetriNet]], constraints: List[Type['Constraint']], log: Callable[[str], None]) -> None:
        self.nets = dict([(n[0], n[1]()) for n in nets.items()])
        self.constraints = [c() for c in constraints]
        self.log = log
        self.all_states = set(self.__flatten([[self.__place_to_state_key(p, connection) for p in net.place() if p.meta['type'] == 'state'] 
            for (connection, net) in self.nets.items()]))
        self.all_events = set(self.__flatten([[self.__event_to_event_key(t.meta['event']) for t in net.transition() if t.meta['type'] == 'event'] 
            for net in self.nets.values()]))
        self.all_clauses = set(self.__flatten([[self.__place_to_clause_key(p, connection) for p in net.place() if p.meta['type'] == 'clause' and 'sourceline' in p.meta] 
            for (connection, net) in self.nets.items()]))
        self.seen_events: Set[str] = set()
        self.seen_states: Set[str] = set()
        self.seen_clauses: Set[str] = set()
        self.states = dict([(p, self.__net_states(self.nets[p])) for p in self.nets.keys()])
        for connection, net in self.nets.items():
            self.__update_visited_states_and_events(connection, net.get_marking())

    T = TypeVar('T')
    def __flatten(self, list: List[List[T]]) -> List[T]:
        return [x for xs in list for x in xs]

    def __place_to_state_key(self, place: Place, connection: str) -> str:
        return f"{place.meta['interface']}.{connection[0]}.{connection[1]}.{place.meta['machine']}.{place.meta['state']}"
    
    def __place_to_clause_key(self, place: Place, connection: str) -> str:
        return f"{place.meta['interface']}.{connection[0]}.{connection[1]}.{place.name}.{place.meta['sourceline']}"

    def __event_to_event_key(self, event: 'Event') -> str:
        return f"{event.interface}.{event.port}.{event.component}.{event.method}"

    def __update_visited_states_and_events(self, connection: tuple, marking: Marking, event: Optional['Event'] = None):
        places = [self.nets[connection]._place[p] for p in marking.keys() if 'state' in self.nets[connection]._place[p].meta]
        self.seen_states.update([self.__place_to_state_key(p, connection) for p in places])
        if event != None:
            self.seen_events.add(self.__event_to_event_key(event))

    def __net_states(self, net: PetriNet) -> Dict[str, str]: # key = machine, value = state of machine
        return dict([(p.meta['machine'], p.meta['state']) for p in [net._place[pp] for pp in net.get_marking().keys()] if 'state' in p.meta])

    def take_step(self, step: WalkerStep):
        before_states = self.states
        net = self.nets[step.event.connectionKey()]
        net.set_marking(step.marking)
        for c1, c2 in [(c1, c2) for c1, c2 in zip(self.constraints, step.constraints) if c1 != c2]:
            self.log(f"Constraint changed: '{c1}' -> '{c2}'")
        self.constraints = step.constraints
        self.__update_visited_states_and_events(step.event.connectionKey(), step.marking, step.event)
        if step.event != None:
            if "_join" in step.clause:
                step.clause = step.clause[0: (step.clause.rfind("_join"))]
            if "_split" in step.clause:
                step.clause = step.clause[0: (step.clause.rfind("_split"))]
            if not step.clause.endswith("_0"):
                step.clause = step.clause[0: (step.clause.rfind("_")+1)] + "0"
            self.seen_clauses.add(step.event.interface + "." + step.event.port + "." + step.event.component + "." + step.clause + "." + net._place[step.clause].meta['sourceline'])
        self.states = dict([(p, self.__net_states(self.nets[p])) for p in self.nets.keys()])
        for (connection, states) in self.states.items():
            for (machine, state) in states.items():
                if state != before_states[connection][machine]:
                    self.log(f"State of connection ({connection[1]}, {connection[0]}), machine '{machine}' changed from '{before_states[connection][machine]}' to '{state}'")

    def next_steps(self, connection: str) -> List[WalkerStep]:
        net = self.nets[connection]
        def recurse(marking: Marking, constraints: List[Constraint], possible_step: Optional[WalkerStep]) -> Generator[WalkerStep, None, None]:
            net.set_marking(marking)
            transitions = [t for t in net.transition() if len(t.modes()) > 0]
            if len(transitions) == 0 and possible_step != None:
                yield possible_step

            for transition in transitions:
                is_event = transition.meta['type'] == 'event'
                transition_machine = transition.meta['machine']
                if is_event and possible_step != None:
                    yield possible_step
                else:
                    net.set_marking(marking)
                    for mode in transition.modes():
                        net.set_marking(marking)
                        transition.fire(mode)
                        new_marking = net.get_marking()
                        new_constraints = constraints
                        step: Optional[WalkerStep] = possible_step
                        if is_event:
                            evt: Event = transition.meta['event']
                            assert evt != None
                            evt = evt.eval(mode._dict)
                            port_machine_state = {**self.states, connection: self.__net_states(net)}
                            new_constraints = [c for c in [c.take(evt, port_machine_state) for c in constraints] if c != None]
                            if len(new_constraints) != len(constraints): continue # Constraint failed
                            step = WalkerStep(new_marking, new_constraints, evt, False)
                            # step.machine = transition_machine
                            
                        is_state = any(s.startswith("S_") and (s.count('_') == 1 or transition_machine in s) for s in new_marking.keys())
                        if is_state and step != None:
                            st:WalkerStep = WalkerStep(new_marking, new_constraints, step.event, False)
                            st.clause = [x for x in list(marking.keys()) if x.startswith("C_")][0]
                            yield st # WalkerStep(new_marking, new_constraints, step.event)
                        else:
                            if any(k.startswith("C_") and k.endswith("_0") for k in new_marking.keys()):
                                if step != None:
                                    step.clause = [x for x in list(new_marking.keys()) if x.startswith("C_")][0]
                            yield from recurse(new_marking, new_constraints, step)

        marking = net.get_marking()
        result = [x for x in recurse(marking, self.constraints, None)]
        net.set_marking(marking)
        withoutDuplicates = []
        for i in result:
            isInDuplicates = False
            for j in withoutDuplicates:
                if str(i.event) == str(j.event):
                    isInDuplicates = True
            if not isInDuplicates:
                withoutDuplicates.append(i)
        result = withoutDuplicates
        return result
    
    def get_state(self) -> WalkerState:
        return (dict([(name, net.get_marking()) for name, net in self.nets.items()]), [c.get_state() for c in self.constraints])
    
    def set_state(self, state: WalkerState):
        for name, marking in state[0].items():
            self.nets[name].set_marking(marking)
        for idx, s in enumerate(state[1]):
            self.constraints[idx].set_state(s)
