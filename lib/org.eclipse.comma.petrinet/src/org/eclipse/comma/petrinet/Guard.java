/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.List;
import java.util.function.Function;

import org.eclipse.comma.behavior.behavior.NonTriggeredTransition;
import org.eclipse.comma.expressions.expression.Expression;

class Guard {
	private Expression expression = null;
	private boolean inverse = false;
	private GuardContext context = null;
	
	enum PRepeatGuardType {MIN, MAX}
	private PRepeatGuardType repeatGuardType = null;
	private Long repeatGuardValue = null;
	
	Guard(Expression expression, GuardContext context) { this (expression, context, false); }
	
	Guard(Expression expression, GuardContext context, boolean inverse) {
		assert expression != null;
		this.expression = expression;
		this.inverse = inverse;
		this.context = context;
	}
	
	Guard(PRepeatGuardType repeatGuardType, Long repeatGuardValue) {
		this.repeatGuardType = repeatGuardType;
		this.repeatGuardValue = repeatGuardValue;
	}
	
	String toPython(Function<String, String> variablePrefix, List<String> parameters, boolean inActionWithVars) {
		if (expression != null) {
//			Function<String, String> variablePrefix2 = (String variable) -> {
//				var prefix = "gl.";
//				if (parameters != null) {
//					prefix = String.format("g.gl(p.v([%s])).", parameters.stream().map(p -> String.format("\"%s\"", p)).collect(Collectors.joining(",")));
//				}				
//				return prefix + variablePrefix.apply(variable);
//			};
			String expr;
			if(expression.eContainer() instanceof NonTriggeredTransition) {
				expr = PythonHelper.expression(expression, (String variable) -> { return "";});
			} else {
				expr = PythonHelper.expression(expression, variablePrefix);
			}
			if (inverse) expr = String.format("not (%s)", expr);

			switch(context) {
			case ACTION_WITH_VARS: return String.format("(lambda gl, p:(%s))(gl, p)", expr);
			case TRANSITION: return String.format("(lambda g, p:(%s))(g, p)", expr);
			case IF_THEN_ELSE: return String.format("(lambda gl:(%s))(gl)", expr);
			}
			return expr;
		} else {
			String operation = repeatGuardType == PRepeatGuardType.MAX ? "<" : ">=";
			return String.format("gl.r %s %d", operation, repeatGuardValue);
		}
	}
}
