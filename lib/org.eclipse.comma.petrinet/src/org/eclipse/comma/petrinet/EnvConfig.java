/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import org.eclipse.comma.behavior.behavior.Port;
import org.eclipse.comma.parameters.parameters.Parameters;

public class EnvConfig {
	public final String component;
	public final Port port;
	public final Parameters param;
	
	public EnvConfig(String component, Port port, Parameters param) {
		this.component = component;
		this.port = port;
		this.param = param;
	}
}
