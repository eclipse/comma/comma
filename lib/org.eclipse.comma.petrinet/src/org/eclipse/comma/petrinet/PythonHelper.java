/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.comma.actions.actions.Action;
import org.eclipse.comma.actions.actions.ActionWithVars;
import org.eclipse.comma.actions.actions.AssignmentAction;
import org.eclipse.comma.actions.actions.CommandReply;
import org.eclipse.comma.actions.actions.CommandReplyWithVars;
import org.eclipse.comma.actions.actions.EventCall;
import org.eclipse.comma.actions.actions.EventWithVars;
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction;
import org.eclipse.comma.behavior.behavior.State;
import org.eclipse.comma.behavior.behavior.StateMachine;
import org.eclipse.comma.behavior.behavior.TriggeredTransition;
import org.eclipse.comma.behavior.component.component.ExpressionConnectionState;
import org.eclipse.comma.behavior.component.component.ExpressionInterfaceState;
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.expressions.expression.Expression;
import org.eclipse.comma.expressions.expression.ExpressionAddition;
import org.eclipse.comma.expressions.expression.ExpressionAnd;
import org.eclipse.comma.expressions.expression.ExpressionAny;
import org.eclipse.comma.expressions.expression.ExpressionBracket;
import org.eclipse.comma.expressions.expression.ExpressionBulkData;
import org.eclipse.comma.expressions.expression.ExpressionConstantBool;
import org.eclipse.comma.expressions.expression.ExpressionConstantInt;
import org.eclipse.comma.expressions.expression.ExpressionConstantReal;
import org.eclipse.comma.expressions.expression.ExpressionConstantString;
import org.eclipse.comma.expressions.expression.ExpressionDivision;
import org.eclipse.comma.expressions.expression.ExpressionEnumLiteral;
import org.eclipse.comma.expressions.expression.ExpressionEqual;
import org.eclipse.comma.expressions.expression.ExpressionFunctionCall;
import org.eclipse.comma.expressions.expression.ExpressionGeq;
import org.eclipse.comma.expressions.expression.ExpressionGreater;
import org.eclipse.comma.expressions.expression.ExpressionLeq;
import org.eclipse.comma.expressions.expression.ExpressionLess;
import org.eclipse.comma.expressions.expression.ExpressionMap;
import org.eclipse.comma.expressions.expression.ExpressionMapRW;
import org.eclipse.comma.expressions.expression.ExpressionMaximum;
import org.eclipse.comma.expressions.expression.ExpressionMinimum;
import org.eclipse.comma.expressions.expression.ExpressionMinus;
import org.eclipse.comma.expressions.expression.ExpressionModulo;
import org.eclipse.comma.expressions.expression.ExpressionMultiply;
import org.eclipse.comma.expressions.expression.ExpressionNEqual;
import org.eclipse.comma.expressions.expression.ExpressionNot;
import org.eclipse.comma.expressions.expression.ExpressionOr;
import org.eclipse.comma.expressions.expression.ExpressionPlus;
import org.eclipse.comma.expressions.expression.ExpressionPower;
import org.eclipse.comma.expressions.expression.ExpressionQuantifier;
import org.eclipse.comma.expressions.expression.ExpressionRecord;
import org.eclipse.comma.expressions.expression.ExpressionRecordAccess;
import org.eclipse.comma.expressions.expression.ExpressionSubtraction;
import org.eclipse.comma.expressions.expression.ExpressionVariable;
import org.eclipse.comma.expressions.expression.ExpressionVector;
import org.eclipse.comma.expressions.expression.MapRWContext;
import org.eclipse.comma.expressions.expression.QUANTIFIER;
import org.eclipse.comma.expressions.expression.Variable;
import org.eclipse.comma.expressions.validation.ExpressionValidator;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.signature.interfaceSignature.Command;
import org.eclipse.comma.signature.interfaceSignature.DIRECTION;
import org.eclipse.comma.types.types.EnumTypeDecl;
import org.eclipse.comma.types.types.MapTypeConstructor;
import org.eclipse.comma.types.types.MapTypeDecl;
import org.eclipse.comma.types.types.RecordTypeDecl;
import org.eclipse.comma.types.types.SimpleTypeDecl;
import org.eclipse.comma.types.types.Type;
import org.eclipse.comma.types.types.TypeObject;
import org.eclipse.comma.types.types.TypeReference;
import org.eclipse.comma.types.types.VectorTypeConstructor;
import org.eclipse.comma.types.types.VectorTypeDecl;
import org.eclipse.comma.types.utilities.TypeUtilities;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;

class PythonHelper {	
	static String defaultValue(TypeObject type) {
		if (type instanceof VectorTypeConstructor) {
			return "[]";
		}
		if(type instanceof MapTypeConstructor) {
			return "{}";
		}
		if (type instanceof SimpleTypeDecl) {
			SimpleTypeDecl t = (SimpleTypeDecl) type;
			if (t.getBase() != null) return defaultValue(t.getBase());
			else if (t.getName().equals("int")) return "0";
			else if (t.getName().equals("real")) return "0.0";
			else if (t.getName().equals("bool")) return "True";
			else if (t.getName().equals("string")) return "\"\"";
			else return "\"\""; // Custom types without base (e.g. type DateTime)
		} else if (type instanceof VectorTypeDecl) {
			return "[]";
		} else if (type instanceof EnumTypeDecl) {
			EnumTypeDecl t = (EnumTypeDecl) type;
			return String.format("\"%s:%s\"", t.getName(), t.getLiterals().get(0).getName());
		} else if (type instanceof MapTypeDecl) {
			return "{}";
		} else if (type instanceof RecordTypeDecl) {
			String value = 	TypeUtilities.getAllFields((RecordTypeDecl) type).stream()
				.map(f -> String.format("\"%s\": %s", f.getName(), defaultValue(TypeUtilities.getTypeObject(f.getType()))))
				.collect(Collectors.joining(", "));
			return String.format("{%s}", value);
		} 
		
		throw new RuntimeException("Not supported");
	}

	static String expression(Expression expression, Function<String, String> variablePrefix) {
		return expression(expression, variablePrefix, null);
	}

	static String expression(Expression expression, Function<String, String> variablePrefix, Function<Expression, String> handleUnsupported) {
		if (expression instanceof ExpressionConstantInt) {
			return Long.toString(((ExpressionConstantInt) expression).getValue());
		} else if (expression instanceof ExpressionConstantString) {
			return String.format("\"%s\"", ((ExpressionConstantString) expression).getValue());
		} else if (expression instanceof ExpressionNot) {
			return String.format("not (%s)", expression(((ExpressionNot) expression).getSub(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionConstantReal) {
			return Double.toString(((ExpressionConstantReal) expression).getValue());
		} else if (expression instanceof ExpressionConstantBool) {
			return ((ExpressionConstantBool) expression).isValue() ? "True" : "False";
		} else if (expression instanceof ExpressionAny) {
			return "'ANY()'";
		} else if (expression instanceof ExpressionAddition) {
			ExpressionAddition e = (ExpressionAddition) expression;
			return String.format("(%s) + (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionSubtraction) {
			ExpressionSubtraction e = (ExpressionSubtraction) expression;
			return String.format("(%s) - (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionMultiply) {
			ExpressionMultiply e = (ExpressionMultiply) expression;
			return String.format("(%s) * (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionDivision) {
			ExpressionDivision e = (ExpressionDivision) expression;
			return String.format("(%s) / (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionModulo) {
			ExpressionModulo e = (ExpressionModulo) expression;
			return String.format("(%s) % (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported),
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionMinimum) {
			ExpressionMinimum e = (ExpressionMinimum) expression;
			return String.format("min((%s), (%s))", expression(e.getLeft(), variablePrefix, handleUnsupported),
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionMaximum) {
			ExpressionMaximum e = (ExpressionMaximum) expression;
			return String.format("max((%s), (%s))", expression(e.getLeft(), variablePrefix, handleUnsupported),
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionPower) {
			ExpressionPower e = (ExpressionPower) expression;
			return String.format("pow((%s), (%s))", expression(e.getLeft(), variablePrefix, handleUnsupported),
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionVariable) {
			ExpressionVariable v = (ExpressionVariable) expression;
			var variableDef = v.getVariable();
			var varName = variableDef.getName();
			var prefix = variablePrefix != null ? variablePrefix.apply(varName) : "gl.";
			if(variableDef.eContainer() instanceof Interface) { //global var in an interface
				if(EcoreUtil2.getContainerOfType((EObject)v, StateMachine.class) == null) { //var used in init section
					return "v_" + varName;
				}
				if(prefix.endsWith("g.")) {
					return prefix + varName;
				} else {
					return prefix + "g." + varName;
				}
			} else if(variableDef.eContainer() instanceof ExpressionQuantifier ||
					  variableDef.eContainer() instanceof MapRWContext) { //var defined in iterator
				return varName;
			} else if(variableDef.eContainer() instanceof Parameters) { //var defined in param file
				return "p_" + varName;
			} else if(EcoreUtil2.getContainerOfType((EObject)variableDef, StateBasedFunctionalConstraint.class) != null) { //var defined in component constraint
				return "v." + varName;
			} else {
				if(prefix.startsWith("p[")) {
					return prefix;
				}
				if(prefix.endsWith(".l.")) {
					return prefix + v.getVariable().getName();
				} else {
					return prefix + "l." + v.getVariable().getName();
				}
			}
		} else if (expression instanceof ExpressionGreater) {
			ExpressionGreater e = (ExpressionGreater) expression;
			return String.format("(%s) > (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionLess) {
			ExpressionLess e = (ExpressionLess) expression;
			return String.format("(%s) < (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionLeq) {
			ExpressionLeq e = (ExpressionLeq) expression;
			return String.format("(%s) <= (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionGeq) {
			ExpressionGeq e = (ExpressionGeq) expression;
			return String.format("(%s) >= (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionEqual) {
			ExpressionEqual e = (ExpressionEqual) expression;
			return String.format("(%s) == (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionNEqual) {
			ExpressionNEqual e = (ExpressionNEqual) expression;
			return String.format("(%s) != (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionAnd) {
			ExpressionAnd e = (ExpressionAnd) expression;
			return String.format("(%s) and (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionOr) {
			ExpressionOr e = (ExpressionOr) expression;
			return String.format("(%s) or (%s)", expression(e.getLeft(), variablePrefix, handleUnsupported), 
					expression(e.getRight(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionEnumLiteral) {
			ExpressionEnumLiteral e = (ExpressionEnumLiteral) expression;
			return String.format("\"%s:%s\"", e.getType().getName(), e.getLiteral().getName());
		} else if (expression instanceof ExpressionVector) {
			ExpressionVector e = (ExpressionVector) expression;
			return String.format("[%s]", e.getElements().stream().map(ee -> 
				expression (ee, variablePrefix, handleUnsupported)).collect(Collectors.joining(", ")));
		} else if (expression instanceof ExpressionMinus) {
			ExpressionMinus e = (ExpressionMinus) expression;
			return String.format("(%s) * -1", expression(e.getSub(), variablePrefix, handleUnsupported));
		} else if (expression instanceof ExpressionPlus) {
			ExpressionPlus e = (ExpressionPlus) expression;
			return expression(e.getSub(), variablePrefix, handleUnsupported);
		} else if (expression instanceof ExpressionBracket) {
			ExpressionBracket e = (ExpressionBracket) expression;
			return expression(e.getSub(), variablePrefix, handleUnsupported);
		} else if (expression instanceof ExpressionFunctionCall) {
			ExpressionFunctionCall e = (ExpressionFunctionCall) expression;
			if (e.getFunctionName().equals("add")) {
				return String.format("%s + [%s]", 
						expression(e.getArgs().get(0), variablePrefix, handleUnsupported), 
						expression(e.getArgs().get(1), variablePrefix, handleUnsupported));
			} else if (e.getFunctionName().equals("size")) {
				return String.format("len(%s)", expression(e.getArgs().get(0), variablePrefix, handleUnsupported));
			} else if (e.getFunctionName().equals("isEmpty")) {
				return String.format("len(%s) == 0", expression(e.getArgs().get(0), variablePrefix, handleUnsupported));
			} else if (e.getFunctionName().equals("contains")) {
				return String.format("%s in %s", 
						expression(e.getArgs().get(1), variablePrefix, handleUnsupported),
						expression(e.getArgs().get(0), variablePrefix, handleUnsupported));
			} else if (e.getFunctionName().equals("abs")) {
				return String.format("abs(%s)", expression(e.getArgs().get(0), variablePrefix, handleUnsupported));
			} else if (e.getFunctionName().equals("asReal")) {
				return String.format("float(%s)", expression(e.getArgs().get(0), variablePrefix, handleUnsupported));
			} else if (e.getFunctionName().equals("hasKey")) {
				String map = expression(e.getArgs().get(0), variablePrefix, handleUnsupported);
				String key = expression(e.getArgs().get(1), variablePrefix, handleUnsupported);
				return String.format("(str(%s) in %s)", key, map);
			} else if (e.getFunctionName().equals("deleteKey")) {
				String map = expression(e.getArgs().get(0), variablePrefix, handleUnsupported);
				String key = expression(e.getArgs().get(1), variablePrefix, handleUnsupported);
				ExpressionValidator validator = new ExpressionValidator();
				TypeObject keyType = validator.typeOf(e.getArgs().get(1));
				if(TypeUtilities.isVectorType(keyType)) {
					return String.format("{_k: _v for _k, _v in %s.items() if not(_k in [str(__k) for __k in %s])}", map, key);
				} else {
					return String.format("{_k: _v for _k, _v in %s.items() if _k != str(%s)}", map, key);
				}
			} else if (e.getFunctionName().equals("matches")) {
				String arg = expression(e.getArgs().get(0), variablePrefix, handleUnsupported);
				String regex = expression(e.getArgs().get(1), variablePrefix, handleUnsupported);
				if(EcoreUtil2.getContainerOfType(e, Interface.class) != null) {
					var prefix = (variablePrefix != null ? variablePrefix.apply("") : "gl.") + "g";
					return String.format("%s.match(%s, %s)", prefix, regex, arg);
				} else {
					return String.format("re.match(%s, %s)", regex, arg);
				}
				
			}
		} else if (expression instanceof ExpressionQuantifier) {
			ExpressionQuantifier e = (ExpressionQuantifier) expression;
			String collection = expression(e.getCollection(), variablePrefix, handleUnsupported);
			String it = e.getIterator().getName();
			String condition = expression(e.getCondition(), variablePrefix, handleUnsupported);
			if (e.getQuantifier() == QUANTIFIER.EXISTS) {
				return String.format("len([%s for %s in %s if %s]) != 0", it, it, collection, condition);
			} else if (e.getQuantifier() == QUANTIFIER.DELETE) {
				return String.format("[%s for %s in %s if not (%s)]", it, it, collection, condition);
			} else if (e.getQuantifier() == QUANTIFIER.FORALL) {
				return String.format("len([%s for %s in %s if %s]) == len(%s)", it, it, collection, condition, collection);
			}
		} else if (expression instanceof ExpressionMap) {
			ExpressionMap e = (ExpressionMap) expression;
			return String.format("{%s}", e.getPairs().stream().map(p -> {
				String key = expression(p.getKey(), variablePrefix, handleUnsupported);
				String value = expression(p.getValue(), variablePrefix, handleUnsupported);
				return String.format("%s: %s", key, value);
			}).collect(Collectors.joining(", ")));
		} else if (expression instanceof ExpressionMapRW) {
			ExpressionMapRW e = (ExpressionMapRW) expression;
			String map = expression(e.getMap(), variablePrefix, handleUnsupported);
			String key = expression(e.getKey(), variablePrefix, handleUnsupported);
			if (e.getValue() == null) {
				if(e.getContext() == null) {
					return String.format("%s[str(%s)]", map, key);
				} else {
					String collection = expression(e.getContext().getCollection(), variablePrefix, handleUnsupported);
					return String.format("[%s[str(%s)] for %s in %s]", map, key, e.getContext().getIterator().getName(), collection);
				}
			} else {
				String value = expression(e.getValue(), variablePrefix, handleUnsupported);
				if(e.getContext() == null) {
					return String.format("{**%s, **{str(%s): %s}}", map, key, value);
				} else {
					String collection = expression(e.getContext().getCollection(), variablePrefix, handleUnsupported);
					return String.format("{**%s, **{str(%s): %s for %s in %s}}", map, key, value, e.getContext().getIterator().getName(), collection);
				}
			}
		} else if (expression instanceof ExpressionRecord) {
			ExpressionRecord e = (ExpressionRecord) expression;
			return String.format("{%s}", e.getFields().stream().map(p -> {
				String key = p.getRecordField().getName();
				String value = expression(p.getExp(), variablePrefix, handleUnsupported);
				return String.format("\"%s\": %s", key, value);
			}).collect(Collectors.joining(", ")));
		} else if (expression instanceof ExpressionRecordAccess) {
			ExpressionRecordAccess e = (ExpressionRecordAccess) expression;
			String map = expression(e.getRecord(), variablePrefix, handleUnsupported);
			return String.format("%s[\"%s\"]", map, e.getField().getName());
		} else if (expression instanceof ExpressionBulkData) {
			return "[]";
		} else if(expression instanceof ExpressionConnectionState) {
			ExpressionConnectionState e = (ExpressionConnectionState) expression;
			String statesString = "";
			for(State s : e.getStates()) {
				statesString += "'" + s.getName() + "', ";
			}
			if(e.getIdVar() != null) {
				return "self.connectionAtPort(v." + e.getIdVar().getVariable().getName() + ", '" + e.getPort().getName() + "', {" + statesString + "}" + ", port_machine_state)";
			}
			if(e.getMultiplicity() != null) {
				long lower = e.getMultiplicity().getLower();
				long upper = -1;
				if(e.getMultiplicity().getUpperInf() == null) {
					upper = e.getMultiplicity().getUpper();
				}
				return "self.intervalAtPort(" + lower + ", " + upper + ", '" + e.getPort().getName() + "', {" + statesString + "}" + ", port_machine_state)";
			}
			if(e.getQuantifier() != null) {
				String prefix = e.getQuantifier().getLiteral();
				return "self." + prefix + "AtPort('" + e.getPort().getName() + "', {" + statesString + "}" + ", port_machine_state)";
			}
		} else if(expression instanceof ExpressionInterfaceState) {
			ExpressionInterfaceState e = (ExpressionInterfaceState) expression;
			return "self.allAtPort('" + e.getPort().getName() + "', {'" + e.getState().getName() + "'}" + ", port_machine_state)"; 
		} else if (handleUnsupported != null) {
			return handleUnsupported.apply(expression);
		}
		
		throw new RuntimeException("Not supported");
	}
	
	static String type(Object type) {
		if (type instanceof VectorTypeConstructor) {
			var v = (VectorTypeConstructor) type;
			return String.format("{'type':'vector','dimensions':%d,'typeElem':%s}", v.getDimensions().size(), type(v.getType()));
		} else if (type instanceof MapTypeConstructor) {
			var m = (MapTypeConstructor) type;
			return String.format("{'type':'map','typeKey':%s,'typeValue':%s}", type(m.getType()), type(m.getValueType()));
		} else if (type instanceof RecordTypeDecl) {
			var r = (RecordTypeDecl) type;
			var fields = r.getFields().stream()
					.map(f -> String.format("{'name':'%s','type':%s}", f.getName(), type(f.getType())))
					.collect(Collectors.joining(","));
			return String.format("{'type':'record','record':'%s','fields':[%s]}", r.getName(), fields);
		} else if (type instanceof SimpleTypeDecl) {
			return String.format("'%s'", ((SimpleTypeDecl) type).getName());
		} else if (type instanceof MapTypeDecl) {
			return type(((MapTypeDecl) type).getConstructor());
		} else if (type instanceof TypeReference) {
			return type(((TypeReference) type).getType());
		} else if (type instanceof EnumTypeDecl) {
			return "'enum'";
		} else if (type instanceof VectorTypeDecl) {
			return type(((VectorTypeDecl)type).getConstructor());
		}
		
		throw new RuntimeException("Not supported");
	}
	
	static String parameter(Type type, Object expression, int index, Function<String, String> variablePrefix) {
		String expressionStr;
		if (expression instanceof Variable) {
			expressionStr = String.format("p[%d]", index);
		} else if (expression instanceof ExpressionAny &&
				EcoreUtil2.getContainerOfType((EObject)expression, StateBasedFunctionalConstraint.class) == null) {
			expressionStr = String.format("p[%d]", index);
		} else {
			expressionStr = expression((Expression) expression, variablePrefix);
		}
		
		if (variablePrefix != null && EcoreUtil2.getContainerOfType((EObject)expression, StateBasedFunctionalConstraint.class) != null) {
			if (!expressionStr.startsWith("\"") && !expressionStr.endsWith("\"")) {
				return String.format("Parameter(%s, str(%s))", type(type), expressionStr);
			}
		}
		return String.format("Parameter(%s, '%s')", type(type), expressionStr);
	}
	
	static List<String> parameters(Object event, TriggeredTransition trigger, Function<String, String> variablePrefix) {
		if (event instanceof TriggeredTransition) {
			var e = (TriggeredTransition) event;
			var counter = new AtomicInteger(0);
			return e.getTrigger().getParameters().stream().filter(p -> p.getDirection() != DIRECTION.OUT).map(p -> {
				var index = e.getTrigger().getParameters().indexOf(p);
				var parameter = e.getParameters().get(index);
				return parameter(p.getType(), parameter, counter.getAndIncrement(), variablePrefix);//Index according to the number of the parameters in the params file
			}).collect(Collectors.toList());
		} else if (event instanceof CommandReply) {
			var e = (CommandReply) event;
			var cmd = (Command) trigger.getTrigger();
			var triggerParameters = cmd.getParameters().stream().filter(p -> p.getDirection() != DIRECTION.IN).collect(Collectors.toList());
			var types = triggerParameters.stream().map(p -> p.getType()).collect(Collectors.toList());
			types.add(cmd.getType());
			return  e.getParameters().stream().map(expr -> {
				var index = e.getParameters().indexOf(expr);
				var type = types.get(index);
				return parameter(type, expr, index, variablePrefix);
			}).collect(Collectors.toList());
		} else if (event instanceof CommandReplyWithVars) {
			var e = (ActionWithVars) event;
			var tr = EcoreUtil2.getContainerOfType((EObject) event, TriggeredTransition.class);
			var cmd = (Command) tr.getTrigger();
			var triggerParameters = cmd.getParameters().stream().filter(p -> p.getDirection() != DIRECTION.IN).collect(Collectors.toList());
			var types = triggerParameters.stream().map(p -> p.getType()).collect(Collectors.toList());
			types.add(cmd.getType());
			return  e.getParameters().stream().map(expr -> {
				var index = e.getParameters().indexOf(expr);
				var type = types.get(index);
				return parameter(type, expr, index, variablePrefix);
			}).collect(Collectors.toList());
		} else if (event instanceof EventCall) {
			var e = (EventCall) event;
			return e.getEvent().getParameters().stream().map(p -> {
				var index = e.getEvent().getParameters().indexOf(p);
				return parameter(p.getType(), e.getParameters().get(index), index, variablePrefix);
			}).collect(Collectors.toList());
		} else if (event instanceof EventWithVars) {
			var e = (EventWithVars) event;
			return e.getParameters().stream().map(p -> {
				var index = e.getParameters().indexOf(p);
				return parameter(p.getType(), e.getParameters().get(index), index, variablePrefix);
			}).collect(Collectors.toList());
		}
		
		throw new RuntimeException("Not supported");
	}

	static String action(Action action, Function<String, String> variablePrefix, boolean init, boolean isPN) {
		// TODO this prefix is still experimental! Test for records
		Function<String, String> vp = (String variable) -> {
			return "";
		};
		
		if (action instanceof AssignmentAction) {
			AssignmentAction a = (AssignmentAction) action;
			String variable = String.format("%s%s", variablePrefix.apply(a.getAssignment().getName()), a.getAssignment().getName());
			String exp = expression(a.getExp(), vp);
			if (init && exp.startsWith("\"") && exp.endsWith("\"")) {
				return String.format("%s = '%s'", variable, exp);
			} else {
				if(isPN) { 
					return String.format("%s = (lambda g, l: (%s))(g, l)", variable, exp);
				} else {
					return String.format("%s = %s", variable, exp);
				}
			}
		} else if (action instanceof RecordFieldAssignmentAction) {
			RecordFieldAssignmentAction a = (RecordFieldAssignmentAction) action;
			ExpressionRecordAccess access = (ExpressionRecordAccess) a.getFieldAccess();
			String record = expression(access.getRecord(), variablePrefix);
			String field = access.getField().getName();
			String value = expression(a.getExp(), variablePrefix);
			if (init && value.startsWith("\"") && value.endsWith("\"")) {
				return String.format("%s[\"%s\"] = '%s'", record, field, value);
			} else {
				if(isPN) { 
					return String.format("%s[\"%s\"] = (lambda g, l: (%s))(g, l)", record, field, value);
				} else {
					return String.format("%s[\"%s\"] = %s", record, field, value);
				}
			}
		}
		
		throw new RuntimeException("Not supported");
	}
}
