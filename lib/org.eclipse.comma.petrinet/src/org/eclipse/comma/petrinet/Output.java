/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.comma.actions.actions.Action;
import org.eclipse.comma.actions.actions.ActionWithVars;
import org.eclipse.comma.behavior.behavior.TriggeredTransition;
import org.eclipse.comma.signature.interfaceSignature.DIRECTION;

class Output {
	
	enum RepeatAction { INIT, INCREASE, SET_1 }

	final Place place;
	final PTransition transition;
	
	private List<Action> actions = null;
	RepeatAction repeatAction = null;
	
	Output(Place place, PTransition transition) {
		this.place = place;
		this.transition = transition;
	}
	
	Output(Place place, PTransition transition, List<Action> actions) {
		this(place, transition);
		this.actions = actions;
	}
		
	String toPython(Function<String, String> variablePrefix) {
		String label = "";
		String exec = "";
		if (actions != null && !actions.isEmpty()) {
			exec += actions.stream().map(a -> {
				return String.format(".e('%s')", PythonHelper.action(a, variablePrefix, false, true).replace("\"", "\\\""));
			}).collect(Collectors.joining());
		}
		
		if (repeatAction != null) {
			if (repeatAction == RepeatAction.INIT) exec += ".er('= 0')";
			else if (repeatAction == RepeatAction.INCREASE) exec += ".er('+= 1')";
			else if (repeatAction == RepeatAction.SET_1) exec += ".er('= 1')";
		}
		
		if (place.type == Place.PPlaceType.PARAMETERS && exec.equals("")) {
			label = "Variable('p')";
		} else if (place.type == Place.PPlaceType.TRANSITION && exec.equals("")) {
			var locals = "";
			if (transition.event instanceof TriggeredTransition) {
				var e = (TriggeredTransition) transition.event;
				var params = IntStream.range(0, e.getParameters().size()).mapToObj(i -> {
					var tP = e.getTrigger().getParameters().get(i);
					var p = e.getParameters().get(i);
					return tP.getDirection() == DIRECTION.OUT ? null : String.format("\"%s\"", p.getName());
				}).filter(p -> p != null).collect(Collectors.joining(","));
				locals = String.format("p.v([%s])", params);
			}
			label = String.format("Expression('g.gl(%s)')", locals);
		} else if (place.type == Place.PPlaceType.VARIABLES) {
			label = String.format("Expression(\"gl%s.g\")", exec);
		} else if (place.type == Place.PPlaceType.STATE) {
			assert actions == null || actions.isEmpty() : "Should not happen";
			label = String.format("Expression(\"''\")", exec);
		} else if (place.type == Place.PPlaceType.CLAUSE) {
			if (!exec.equals("")) {
				label = String.format("Expression(\"gl%s\")", exec);
			} else if( transition.event != null && transition.event instanceof ActionWithVars) {
				var e = (ActionWithVars) transition.event; 
				var params = e.getParameters().stream().map(i -> String.format("\"%s\"", i.getName())).collect(Collectors.joining(","));
				var	locals = String.format("p.vdict([%s])", params);
				label = String.format("Expression('gl.add(%s)')", locals);
			} else {
				label = "Variable('gl')";
			}
		} else {
			assert false : "Should not happen";
		}
		
		return String.format("n.add_output('%s', '%s', %s)\n", 
				place.name, transition.getPythonName(variablePrefix), label);
	}
}
