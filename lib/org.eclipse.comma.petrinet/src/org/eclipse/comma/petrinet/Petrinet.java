/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.comma.actions.actions.Action;
import org.eclipse.comma.actions.actions.ActionWithVars;
import org.eclipse.comma.actions.actions.AssignmentAction;
import org.eclipse.comma.actions.actions.CommandReply;
import org.eclipse.comma.actions.actions.CommandReplyWithVars;
import org.eclipse.comma.actions.actions.EventCall;
import org.eclipse.comma.actions.actions.EventWithVars;
import org.eclipse.comma.actions.actions.IfAction;
import org.eclipse.comma.actions.actions.Multiplicity;
import org.eclipse.comma.actions.actions.PCElement;
import org.eclipse.comma.actions.actions.PCFragmentReference;
import org.eclipse.comma.actions.actions.ParallelComposition;
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction;
import org.eclipse.comma.behavior.behavior.Clause;
import org.eclipse.comma.behavior.behavior.NonTriggeredTransition;
import org.eclipse.comma.behavior.behavior.State;
import org.eclipse.comma.behavior.behavior.StateMachine;
import org.eclipse.comma.behavior.behavior.Transition;
import org.eclipse.comma.behavior.behavior.TriggeredTransition;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.expressions.expression.Expression;
import org.eclipse.comma.expressions.expression.ExpressionVariable;
import org.eclipse.comma.expressions.expression.Variable;
import org.eclipse.comma.parameters.parameters.NotificationParams;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.parameters.parameters.Params;
import org.eclipse.comma.parameters.parameters.ReplyParams;
import org.eclipse.comma.parameters.parameters.StateOtherParams;
import org.eclipse.comma.parameters.parameters.StateParams;
import org.eclipse.comma.parameters.parameters.StateReplyParams;
import org.eclipse.comma.parameters.parameters.TriggerParams;
import org.eclipse.comma.petrinet.Guard.PRepeatGuardType;
import org.eclipse.comma.petrinet.Output.RepeatAction;
import org.eclipse.comma.petrinet.PetrinetBuilder.PortDirection;
import org.eclipse.comma.petrinet.Place.PPlaceType;
import org.eclipse.comma.signature.interfaceSignature.DIRECTION;
import org.eclipse.comma.types.utilities.TypeUtilities;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;

public class Petrinet {
	
	public enum ParameterPlaceMode { 
		TRIGGERED_EMPTY_NONTRIGGERED_NONE, TRIGGERED_FILLED_NONTRIGGERED_EMPTY, TRIGGERED_FILLED_NONTRIGGERED_NONE 
	}
	
	private final Interface itf;
	private final String port;
	private final String component;
	private final ParameterPlaceMode mode;
	private final Parameters params;
	private final PortDirection portDirection;
	private Map<String, Place> places = new LinkedHashMap<>();
	private ArrayList<PTransition> transitions = new ArrayList<>();
	private ArrayList<Input> inputs = new ArrayList<>();
	private ArrayList<Output> outputs = new ArrayList<>();
	private List<String> globalVariables;
	
	Petrinet(Interface itf, Parameters params, ParameterPlaceMode mode, String port, String component, PortDirection portDirection) {
		this.itf = itf;
		this.params = params;
		this.mode = mode;
		this.port = port;
		this.component = component;
		this.portDirection = portDirection;
		this.build();
	}
	
	private Petrinet build() {
		PTransition.resetIDCounter();
		
		globalVariables = itf.getVars().stream().map(v -> v.getName()).collect(Collectors.toList());

		for (StateMachine machine : itf.getMachines()) {
			for (State state : machine.getStates()) {
				Place source = add(Place.forState(machine, state));				
				List<Transition> transitions = new ArrayList<>(state.getTransitions());
				machine.getInAllStates().stream().filter(s -> !s.getExcludedStates().contains(state))
					.forEach(s -> transitions.addAll(s.getTransitions()));
				
				for (Transition transition : transitions) {
					var retVal = RetVal.NONE;
					if (transition instanceof TriggeredTransition) {
						retVal = needsTransitionToBeSkipped(source, (TriggeredTransition)transition);
						if (retVal == RetVal.SKIP) { 
							continue;
						}
					} else {
						retVal = needsTransitionToBeSkipped(source, (NonTriggeredTransition)transition);
						if (retVal == RetVal.SKIP) { 
							continue;
						}
					}
					var trigger = transition instanceof TriggeredTransition ? (TriggeredTransition) transition : null;
					Place intermediate = add(Place.forTransition(machine, state, transition));
					Guard guard = transition.getGuard() != null ? new Guard(transition.getGuard(), GuardContext.TRANSITION) : null;
					add(new PTransition(transition, trigger, guard, null, this.component, retVal == RetVal.BREAK), source, intermediate, null);
					for (Clause clause : transition.getClauses()) {
						retVal = needsClauseToBeSkipped(source, clause);
						if (retVal == RetVal.SKIP) {
							continue;
						}
						Place clausePlace = add(Place.forClause(intermediate, clause));
						add(new PTransition(transition, this.component, retVal == RetVal.BREAK), intermediate, clausePlace, null);
						State targetState = clause.getTarget() == null ? state : clause.getTarget();
						Place target = add(Place.forState(machine, targetState));
						if (clause.getActions() == null) {
							add(new PTransition(transition, this.component, retVal == RetVal.BREAK), clausePlace, target, null);
						} else {
							addClause(transition, clause.getActions().getActions(), clausePlace, target, trigger, retVal == RetVal.BREAK);
						}
					}
				}
			}
		}
		
		// Set initial token
		places.values().stream().filter(p -> p.isInitial()).forEach(p -> p.addToken(Token.empty()));
		
		// Add parameter places + tokens
		for (var transition : transitions) {
			if (transition.event != null) {
				var source = getSource(transition);
				if (transition.event instanceof ActionWithVars && mode != ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_EMPTY) {
					var place = Place.forParameters(transition, source.state);
					if (!places.containsKey(place.name)) {
						add(place);
						if(transition.event instanceof EventWithVars) {
							for (var t : getTokens(source,  (EventWithVars)transition.event)) {
								place.addToken(t);
							}
						} else {
							for (var t : getTokens(source,  (CommandReplyWithVars)transition.event)) {
								place.addToken(t);
							}
						}
						
					}
					inputs.add(new Input(place, transition));
					outputs.add(new Output(place, transition, null));
				} else if (transition.event instanceof CommandReply && mode != ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_EMPTY) {
					if (getTokens(source,  (CommandReply)transition.event).size() > 0) {
						var place = Place.forParameters(transition, source.state);
						if (!places.containsKey(place.name)) {
							add(place);
							for (var t : getTokens(source,  (CommandReply)transition.event)) {
								place.addToken(t);
							}
						}
						inputs.add(new Input(place, transition));
						outputs.add(new Output(place, transition, null));
					}
				} else if (transition.event instanceof EventCall && mode != ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_EMPTY) {
					if (getTokens(source,  (EventCall)transition.event).size() > 0){
						var place = Place.forParameters(transition, source.state);
						if (!places.containsKey(place.name)) {
							add(place);
							for (var t : getTokens(source,  (EventCall)transition.event)) {
								place.addToken(t);
							}
						}
						inputs.add(new Input(place, transition));
						outputs.add(new Output(place, transition, null));
					}
				}
				
				var isTriggered = transition.event instanceof TriggeredTransition;
				var addTokens = isTriggered && mode != ParameterPlaceMode.TRIGGERED_EMPTY_NONTRIGGERED_NONE;
				if (isTriggered || mode == ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_EMPTY) {
					var place = addTokens ? Place.forParameters(transition, source.state) : Place.forParameters(transition);
					if (!places.containsKey(place.name)) {
						add(place);
						if (addTokens) {
							for (var t : getTokens(source, (TriggeredTransition) transition.event)) {
								place.addToken(t);
							}
						}
					}
					
					inputs.add(new Input(place, transition));
					if (addTokens) {
						outputs.add(new Output(place, transition, null));
					}
				}
			}
		}
		
		// Add variable places
		var variablesPlace = add(Place.forVariables());
		variablesPlace.addToken(Token.forVariables(globalVariables));
		outputs.stream().filter(o -> o.place.type == PPlaceType.STATE).collect(Collectors.toList())
			.forEach(o -> outputs.add(new Output(variablesPlace, o.transition)));
		inputs.stream().filter(i -> i.place.type == PPlaceType.STATE).collect(Collectors.toList())
			.forEach(i -> inputs.add(new Input(variablesPlace, i.transition)));

		return this;
	}
	
	private Place getSource(PTransition transition) {
		List<Input> inputs = this.inputs.stream().filter(i -> i.transition == transition && i.place.type != PPlaceType.VARIABLES).collect(Collectors.toList());
		assert inputs.size() == 1;
		return inputs.get(0).place;
	}

	enum RetVal {
		NONE,
		SKIP,
		BREAK
	}

	private RetVal needsTransitionToBeSkipped(Place place, TriggeredTransition e) {
		if (e == null) return RetVal.NONE;
		if (params != null) {
			for (TriggerParams triggerParams : params.getTriggerParams()) {
				if (triggerParams.getEvent() == e.getTrigger()) {
					for (StateParams stateParams : triggerParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							if (skipValue(stateParams)) {
								return RetVal.SKIP;
							}
							if (stateParams.getBreak() != null) {
								return RetVal.BREAK;
							}
						}
					}
				}
			}
		}
		return RetVal.NONE;
	}

	private RetVal needsTransitionToBeSkipped(Place place, NonTriggeredTransition e) {
		if (e == null || e.getClauses().size() > 1) return RetVal.NONE;
		RetVal rVal = RetVal.NONE;
		for (Clause c : e.getClauses()) {
			if (c.getActions() == null) return RetVal.NONE;
			for (Action a : c.getActions().getActions()) {
				if (a instanceof EventWithVars) {
					var tmp = needsActionToBeSkipped(place, (EventWithVars)a);
					rVal = (rVal != RetVal.NONE) ? rVal : tmp;
				} else if (a instanceof EventCall) {
					var tmp = needsActionToBeSkipped(place, (EventCall)a);
					rVal = (rVal != RetVal.NONE) ? rVal : tmp;
				}
			}
		}
		return rVal;
	}

	private RetVal needsClauseToBeSkipped(Place place, Clause c) {
		if (c == null || c.getActions() == null) return RetVal.NONE;
		RetVal rVal = RetVal.NONE;
		for (Action a : c.getActions().getActions()) {
			RetVal tmp = RetVal.NONE;
			if (a instanceof CommandReply) {
				tmp = needsActionToBeSkipped(place, (CommandReply)a);
			} else if (a instanceof EventWithVars) {
				tmp = needsActionToBeSkipped(place, (EventWithVars)a);
			} else if (a instanceof EventCall) {
				tmp = needsActionToBeSkipped(place, (EventCall)a);
			}
			rVal = (rVal != RetVal.NONE) ? rVal : tmp;
		}
		return rVal;
	}

	private RetVal needsActionToBeSkipped(Place place, EventCall n) {
		RetVal rVal = RetVal.NONE;
		if (params != null) {
			RetVal tmp = RetVal.NONE;
			tmp = needsTriggerToBeSkipped(place, n);
			rVal = (rVal != RetVal.NONE) ? rVal : tmp;
			tmp = needsNotificationToBeSkipped(place, n);
			rVal = (rVal != RetVal.NONE) ? rVal : tmp;
		}
		return rVal;
	}

	private RetVal needsActionToBeSkipped(Place place, EventWithVars n) {
		RetVal rVal = RetVal.NONE;
		if (params != null) {
			RetVal tmp = RetVal.NONE;
			tmp = needsTriggerToBeSkipped(place, n);
			rVal = (rVal != RetVal.NONE) ? rVal : tmp;
			tmp = needsNotificationToBeSkipped(place, n);
			rVal = (rVal != RetVal.NONE) ? rVal : tmp;
		}
		return rVal;
	}

	private RetVal needsTriggerToBeSkipped(Place place, EventCall n) {
		RetVal rVal = RetVal.NONE;
		for (TriggerParams tParams : params.getTriggerParams()) {
			if (tParams.getEvent() == n.getEvent()) {
				for (StateParams stateParams : tParams.getStateParams()) {
					var tmp = skipStateOrParameter(place, n, stateParams);
					rVal = (rVal != RetVal.NONE) ? rVal : tmp;
				}
			}
		}
		return rVal;
	}

	private RetVal needsTriggerToBeSkipped(Place place, EventWithVars n) {
		RetVal rVal = RetVal.NONE;
		for (TriggerParams tParams : params.getTriggerParams()) {
			if (tParams.getEvent() == n.getEvent()) {
				for (StateParams stateParams : tParams.getStateParams()) {
					var tmp = skipStateOrParameter(place, n, stateParams);
					rVal = (rVal != RetVal.NONE) ? rVal : tmp;
				}
			}
		}
		return rVal;
	}

	private RetVal needsNotificationToBeSkipped(Place place, EventCall n) {
		RetVal rVal = RetVal.NONE;
		for (NotificationParams nParams : params.getNotificationParams()) {
			if (nParams.getEvent() == n.getEvent()) {
				for (StateParams stateParams : nParams.getStateParams()) {
					var tmp = skipStateOrParameter(place, n, stateParams);
					rVal = (rVal != RetVal.NONE) ? rVal : tmp;
				}
			}
		}
		return rVal;
	}

	private RetVal needsNotificationToBeSkipped(Place place, EventWithVars n) {
		RetVal rVal = RetVal.NONE;
		for (NotificationParams nParams : params.getNotificationParams()) {
			if (nParams.getEvent() == n.getEvent()) {
				for (StateParams stateParams : nParams.getStateParams()) {
					var tmp = skipStateOrParameter(place, n, stateParams);
					rVal = (rVal != RetVal.NONE) ? rVal : tmp;
				}
			}
		}
		return rVal;
	}

	private RetVal skipStateOrParameter(Place place, EventCall n, StateParams stateParams) {
		Function<String, String> variablePrefix = (String variable) -> {
			return "" + "";
		};
		if (stateParams.getState() == place.state) {
			if (skipValue(stateParams)) {
				return RetVal.SKIP;
			}
			if (stateParams.getBreak() != null) {
				return RetVal.BREAK;
			}			for (var p : n.getParameters()) {
				var expStr1 = PythonHelper.expression(p, variablePrefix);
				for (Params param : stateParams.getParams()) {
					for (Expression exp : param.getValue()) {
						var expStr2 = PythonHelper.expression(exp, variablePrefix);
						if (expStr1.equals(expStr2)) {
							if (param.getSkip() != null) {
								return RetVal.SKIP;
							}
						}
					}
				}
			}
		}
		return RetVal.NONE;
	}

	private RetVal skipStateOrParameter(Place place, EventWithVars n, StateParams stateParams) {
		if (stateParams.getState() == place.state) {
			if (skipValue(stateParams)) {
				return RetVal.SKIP;
			}
			if (stateParams.getBreak() != null) {
				return RetVal.BREAK;
			}
		}
		return RetVal.NONE;
	}

	private RetVal needsActionToBeSkipped(Place place, CommandReply e) {
		Function<String, String> variablePrefix = (String variable) -> {
			return "" + "";
		};
		if (params != null) {
			for (ReplyParams rParams : params.getReplyParams()) {
				for (var p : e.getParameters()) {
					var expStr1 = PythonHelper.expression(p, variablePrefix);
					for (StateParams stateParams : rParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							for (Params param : stateParams.getParams()) {
								for (Expression exp : param.getValue()) {
									var expStr2 = PythonHelper.expression(exp, variablePrefix);
									if (expStr1.equals(expStr2)) {
										if (param.getSkip() != null) {
											return RetVal.SKIP;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return RetVal.NONE;
	}

	private List<Token> getTokens(Place place, EventWithVars n) {
		List<Token> result = new ArrayList<Token>();	
		
		if (params != null) {
			for (NotificationParams nParams : params.getNotificationParams()) {
				if (nParams.getEvent() == n.getEvent()) {
					for (StateParams stateParams : nParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							for (Params p : stateParams.getParams()) {
								if (p.getSkip() == null) {
									result.add(Token.forParameters(p.getValue()));
								}
							}
						}
					}
				}
			}
		}
		
		if (result.isEmpty()) {
			result.add(Token.forParameters(new ArrayList<>()));
		}
		
		return result;
	}
	
	private List<Token> getTokens(Place place, EventCall n){
		List<Token> result = new ArrayList<Token>();
		if (params != null) {
			for (NotificationParams nParams : params.getNotificationParams()) {
				if (nParams.getEvent() == n.getEvent()) {
					for (StateParams stateParams : nParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							for (Params p : stateParams.getParams()) {
								if (p.getSkip() == null) {
									result.add(Token.forParameters(p.getValue()));
								}
							}
						}
					}
				}
			}
		}
		
//		if (result.isEmpty()) {
//			result.add(Token.forParameters(new ArrayList<>()));
//		}
		return result;
	}
	
	private List<Token> getTokens(Place place, CommandReply r) {
		List<Token> result = new ArrayList<Token>();	
		var command = EcoreUtil2.getContainerOfType((EObject)r, TriggeredTransition.class).getTrigger();
		if (params != null) {
			for (ReplyParams rParams : params.getReplyParams()) {
				if (rParams.getEvent() == command) {
					for (StateParams stateParams : rParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							for (Params p : stateParams.getParams()) {
								if (p.getSkip() == null) {
									result.add(Token.forParameters(p.getValue()));
								}
							}
						}
					}
				}
			}
		}
		
//		if (result.isEmpty()) {
//			result.add(Token.forParameters(new ArrayList<>()));
//		}
		
		return result;
	}
	
	private List<Token> getTokens(Place place, CommandReplyWithVars r) {
		List<Token> result = new ArrayList<Token>();	
		var command = EcoreUtil2.getContainerOfType((EObject)r, TriggeredTransition.class).getTrigger();
		if (params != null) {
			for (ReplyParams rParams : params.getReplyParams()) {
				if (rParams.getEvent() == command) {
					for (StateParams stateParams : rParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							for (Params p : stateParams.getParams()) {
								if (p.getSkip() == null) {
									result.add(Token.forParameters(p.getValue()));
								}
							}
						}
					}
				}
			}
		}
		
		if (result.isEmpty()) {
			result.add(Token.forParameters(new ArrayList<>()));
		}
		
		return result;
	}

	
	private List<Token> getTokens(Place place, TriggeredTransition event) {
		List<Token> result = new ArrayList<Token>();	
		
		if (params != null) {
			for (TriggerParams triggerParams : params.getTriggerParams()) {
				if (triggerParams.getEvent() == event.getTrigger()) {
					for (StateParams stateParams : triggerParams.getStateParams()) {
						if (stateParams.getState() == place.state) {
							for (Params p : stateParams.getParams()) {
								if (p.getSkip() == null) {
									result.add(Token.forParameters(p.getValue()));
								}
							}
						}
					}
				}
			}
		}
		
		if (result.isEmpty() && event.getTrigger().getParameters().stream().anyMatch(s -> s.getDirection() != DIRECTION.OUT)) {
			throw new RuntimeException(String.format("No parameters provided for: '%s'", event.getTrigger().getName()));
		}
		
		if (result.isEmpty()) {
			result.add(Token.forParameters(new ArrayList<>()));
		}
		
		return result;
	}
	
	private void addClause(Transition transition, List<Action> actions,  Place start, Place end, TriggeredTransition trigger, boolean breakpoint) {
		List<Action> outputActions = new ArrayList<>();
		boolean skipLastTranstion = false;
		
		for (Action action : actions) {
			if (action instanceof AssignmentAction || action instanceof RecordFieldAssignmentAction) {
				outputActions.add(action);
			} else if (action instanceof IfAction) {
				IfAction a = (IfAction) action;
				
				// Split/join
				Place splitPlace = add(Place.forClause(start, String.format("%d_split", actions.indexOf(action))));
				add(new PTransition(transition, this.component, breakpoint), start, splitPlace, outputActions);				
				outputActions = new ArrayList<Action>();
				start = add(Place.forClause(start, String.format("%d_join", actions.indexOf(action))));
				
				// If
				Place ifPlace = add(Place.forClauseNest(splitPlace, "if_0"));
				add(new PTransition(transition, new Guard(a.getGuard(), GuardContext.IF_THEN_ELSE), this.component, breakpoint), splitPlace, ifPlace, outputActions);
				addClause(transition, a.getThenList().getActions(), ifPlace, start, trigger, breakpoint);
				 
				// Else
				Place elsePlace = add(Place.forClauseNest(splitPlace, "else_0"));
				add(new PTransition(transition, new Guard(a.getGuard(), GuardContext.IF_THEN_ELSE, true), this.component,  breakpoint), splitPlace, elsePlace, outputActions);
				addClause(transition, a.getElseList() == null ? new ArrayList<Action>() : a.getElseList().getActions(), 
						elsePlace, start, trigger, breakpoint);
				
				outputActions = new ArrayList<Action>();
			} else if(action instanceof ActionWithVars) { //was EventWIthVars
				var ev = (ActionWithVars) action;
				if (!outputActions.isEmpty()) {					
					Place next = add(Place.forClause(start, Integer.toString(actions.indexOf(action))));
					add(new PTransition(transition, this.component, breakpoint), start, next, outputActions);
					start = next;
					outputActions = new ArrayList<Action>();
				}
				Place actionEnd;
				if (actions.indexOf(action) == actions.size() - 1) {
					actionEnd = end;
					skipLastTranstion = true;
				} else {
					actionEnd = add(Place.forClause(start, Integer.toString(actions.indexOf(action) + 1)));
				}
				Guard guard = ev.getCondition() != null ? new Guard(ev.getCondition(), GuardContext.ACTION_WITH_VARS) : null;
				//add(new PTransition(trigger, guard), source, intermediate, null);
				add(new PTransition(transition, action, guard, null, this.component, breakpoint), start, actionEnd, null);
				start = actionEnd;
				
			} else if (action instanceof CommandReply || action instanceof EventCall) {
				if (!outputActions.isEmpty()) {					
					Place next = add(Place.forClause(start, Integer.toString(actions.indexOf(action))));
					add(new PTransition(transition, this.component, breakpoint), start, next, outputActions);
					start = next;
					outputActions = new ArrayList<Action>();
				}
				
				Place actionEnd;
				if (actions.indexOf(action) == actions.size() - 1) {
					actionEnd = end;
					skipLastTranstion = true;
				} else {
					actionEnd = add(Place.forClause(start, Integer.toString(actions.indexOf(action) + 1)));
				}
				
				addMultiplicityAndOccurence(transition, action, start, actionEnd, trigger, breakpoint);
				start = actionEnd;
			} else if (action instanceof ParallelComposition) { // Any order
				ParallelComposition a = (ParallelComposition) action;
				
				Place anyOrderPlace = add(Place.forClause(start, String.format("%d_anyorder", actions.indexOf(action))));
				add(new PTransition(transition, this.component, breakpoint), start, anyOrderPlace, outputActions);
				outputActions = new ArrayList<Action>();
				PTransition anyOrderTransition = add(new PTransition(transition, this.component, breakpoint), anyOrderPlace, null, null);
				
				Place joinPlace = add(Place.forClause(start, String.format("%d_join", actions.indexOf(action))));
				PTransition joinTransition = add(new PTransition(transition, this.component, breakpoint), null, joinPlace, null);
				
				List<Action> pcActions = new ArrayList<>();
				class RecursiveHelper {
					final Consumer<PCElement> addRecursive = (PCElement element) -> {
						if (element instanceof PCFragmentReference) {
							((PCFragmentReference) element).getFragment().getComponents().forEach(e -> this.addRecursive.accept(e));
						} else {
							pcActions.add((Action) element);
						}
					};
				}
				a.getComponents().forEach(e -> new RecursiveHelper().addRecursive.accept(e));

				for (Action pcAction : pcActions) {
					Place elementStart = add(Place.forClause(start, 
							String.format("%d_anyorder_%d_start", actions.indexOf(action), pcActions.indexOf(pcAction))));
					Place elementEnd = add(Place.forClause(start, 
							String.format("%d_anyorder_%d_end", actions.indexOf(action), pcActions.indexOf(pcAction))));
					
					outputs.add(new Output(elementStart, anyOrderTransition, null));
					inputs.add(new Input(elementEnd, joinTransition));
					
					addClause(transition, Arrays.asList(pcAction), elementStart, elementEnd, trigger, breakpoint);
				}
				
				start = joinPlace;
			} else {
				assert false : "Not supported";
			}
		}
		
		if (!skipLastTranstion) {
			if (!outputActions.isEmpty() && end.state != null) {
				Place next = add(Place.forClause(start, Integer.toString(actions.size())));
				add(new PTransition(transition, this.component, breakpoint), start, next, outputActions);
				start = next;
				outputActions = new ArrayList<Action>();
			}
			
			add(new PTransition(transition, this.component, breakpoint), start, end, outputActions);
		}
	}
	
	private void addMultiplicityAndOccurence(Transition transition, Action action, Place start, Place end, TriggeredTransition trigger, boolean breakpoint) {
		if (!(action instanceof EventCall) || (((EventCall) action).getMultiplicity() == null &&
				((EventCall) action).getOccurence() == null)) {
			add(new PTransition(transition, action, null, trigger, this.component, breakpoint), start, end, null);
			return;
		}
		
		EventCall event = (EventCall) action;
		Multiplicity multiplicity = event.getMultiplicity();
		String occurence = event.getOccurence();
		assert multiplicity != null || occurence != null;
		
		long min, max; // -1 = inifinity
		if (occurence != null) {
			if (occurence.equals("?")) {min = 0; max = 1;}
			else if (occurence.equals("+")) {min = 1; max = -1;}
			else if (occurence.equals("*")) {min = 0; max = -1;}
			else throw new RuntimeException("Not supported");
		} else {
			min = multiplicity.getLower();
			max = multiplicity.getUpperInf() != null ? -1 : multiplicity.getUpper();
		}
		
		
		List<Output> startOutputs = this.outputs.stream().filter(o -> o.place == start).collect(Collectors.toList());
		if (startOutputs.size() != 1) throw new RuntimeException("Not allowed");
		if (!(min == 0 && max == -1)) {
			startOutputs.get(0).repeatAction = RepeatAction.INIT;
		}
		
		Guard minGuard = min != 0 ? new Guard(PRepeatGuardType.MIN, min) : null;
		add(new PTransition(transition, minGuard, this.component, breakpoint), start, end, null);

		if (max != 0) {
			Guard maxGuard = max != -1 ? new Guard(PRepeatGuardType.MAX, max) : null;
			add(new PTransition(transition, event, maxGuard, null, this.component, breakpoint), start, start, null);
			if (!(min == 0 && max == -1)) {
				outputs.get(outputs.size() - 1).repeatAction = max == -1 ? RepeatAction.SET_1 : RepeatAction.INCREASE;
			}
		}
	}
	
	private PTransition add(PTransition transition, Place from, Place to, List<Action> outputActions) {
		this.transitions.add(transition);
		if (from != null) inputs.add(new Input(from, transition));
		if (to != null) outputs.add(new Output(to, transition, outputActions));
		return transition;
	}
	
	private Place add(Place place) {
		if (places.containsKey(place.name)) {
			return places.get(place.name);
		} else {
			places.put(place.name, place);
			return place;
		}
	}
	
	public String toPython(String netFunctionName) {
		var builder = new StringBuilder();
		
		builder.append("def " + netFunctionName + "():\n"
				+ "    n = PetriNet(\"N\")\n"
				+ "\n"
				+ "    def add_place(place: Place, meta: Dict[str, Any]):\n"
				+ "        n.add_place(place)\n"
				+ "        place.meta = meta\n"
				+ "\n"
				+ "    def add_transition(transition: Transition, meta: Dict[str, Any]):\n"
				+ "        n.add_transition(transition)\n"
				+ "        transition.meta = meta\n"
				+ "\n");
		
		Function<String, String> variablePrefix = (String variable) ->  globalVariables.contains(variable) ? "g." : "l.";
		
		var vars = new ArrayList<Variable>();
		if (params != null) {
			for (var tp : params.getTriggerParams()) {
				vars.addAll(getImportedOtherVariables(tp.getStateParams()));
			}
			for (var tp : params.getNotificationParams()) {
				vars.addAll(getImportedOtherVariables(tp.getStateParams()));
			}
			for (var tp : params.getReplyParams()) {
				vars.addAll(getImportedReplyVariables(tp.getStateParams()));
			}
		}
		
		builder.append("    # Variables\n");
		itf.getVars().forEach(v -> 
			builder.append(String.format("    v_%s = %s\n", v.getName(), PythonHelper.defaultValue(TypeUtilities.getTypeObject(v.getType())))));
		if (params != null) {
			List<Variable> variables = new ArrayList<Variable>();
			variables.addAll(params.getVars());
			variables.addAll(vars);
			variables = variables.stream().distinct().collect(Collectors.toList());
			variables.forEach(v -> 
				builder.append(String.format("    p_%s = %s\n", v.getName(), PythonHelper.defaultValue(TypeUtilities.getTypeObject(v.getType())))));
		}
		
		builder.append("\n    # Init\n");
		itf.getInitActions().forEach(a -> builder.append("    " + PythonHelper.action(a, (String variable) -> "v_", false, false) + "\n"));
		if (params != null) {
			List<Action> actions = new ArrayList<Action>();
			actions.addAll(params.getInitActions());
			for (var v : vars) {
				var con = v.eContainer();
				if (con instanceof Parameters) {
					for (var a : ((Parameters) con).getInitActions()) {
						actions.add(a);
					}
				}
			}
			actions = actions.stream().distinct().collect(Collectors.toList());
			actions.forEach(a -> builder.append("    " + PythonHelper.action(a, (String variable) -> "p_", false, false) + "\n"));
		}
		
		builder.append("\n    # Places\n");
		places.values().forEach(p -> builder.append("    " + p.toPython(itf)));
		
		builder.append("\n    # Transitions\n");
		transitions.forEach(p -> builder.append("    " + p.toPython(variablePrefix, itf, port, portDirection)));
		
		builder.append("\n    # Inputs \n");
		inputs.forEach(p -> builder.append("    " + p.toPython(variablePrefix)));
		
		builder.append("\n    # Outputs\n");
		outputs.forEach(p -> builder.append("    " + p.toPython(variablePrefix)));
		builder.append("    return n\n");
		
		return builder.toString();
	}
	
	private List<Variable> getImportedOtherVariables(EList<StateOtherParams> sps) {
		var vars = new ArrayList<Variable>();
		for (var sp : sps) {
			for (var p : sp.getParams()) {
				for (var e : p.getValue()) {
					if (e instanceof ExpressionVariable) {
						vars.add(((ExpressionVariable) e).getVariable());
					}
				}
			}
		}
		return vars;
	}
	
	private List<Variable> getImportedReplyVariables(EList<StateReplyParams> sps) {
		var vars = new ArrayList<Variable>();
		for (var sp : sps) {
			for (var p : sp.getParams()) {
				for (var e : p.getValue()) {
					if (e instanceof ExpressionVariable) {
						vars.add(((ExpressionVariable) e).getVariable());
					}
				}
			}
		}
		return vars;
	}
	
	private boolean skipValue(StateParams sp) {
		if (sp instanceof StateOtherParams) {
			var sop = (StateOtherParams) sp;
			return sop.getSkip() != null;
		}
		return false;
	}
}

