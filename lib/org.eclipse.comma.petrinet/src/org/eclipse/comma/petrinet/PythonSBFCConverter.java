/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.comma.actions.actions.ActionWithVars;
import org.eclipse.comma.actions.actions.AssignmentAction;
import org.eclipse.comma.actions.actions.EVENT_KIND;
import org.eclipse.comma.actions.actions.EventPattern;
import org.eclipse.comma.actions.actions.IfAction;
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction;
import org.eclipse.comma.actions.actions.Reply;
import org.eclipse.comma.behavior.behavior.Clause;
import org.eclipse.comma.behavior.behavior.NonTriggeredTransition;
import org.eclipse.comma.behavior.behavior.State;
import org.eclipse.comma.behavior.component.component.AnyEvent;
import org.eclipse.comma.behavior.component.component.CommandEvent;
import org.eclipse.comma.behavior.component.component.CommandReply;
import org.eclipse.comma.behavior.component.component.CommandReplyWithVars;
import org.eclipse.comma.behavior.component.component.EventCall;
import org.eclipse.comma.behavior.component.component.EventWithVars;
import org.eclipse.comma.behavior.component.component.NotificationEvent;
import org.eclipse.comma.behavior.component.component.PortSelector;
import org.eclipse.comma.behavior.component.component.SignalEvent;
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint;
import org.eclipse.comma.behavior.component.component.TriggeredTransition;
import org.eclipse.comma.behavior.component.component.impl.CommandEventImpl;
import org.eclipse.comma.behavior.component.component.impl.CommandReplyImpl;
import org.eclipse.comma.behavior.component.component.impl.NotificationEventImpl;
import org.eclipse.comma.behavior.component.component.impl.SignalEventImpl;
import org.eclipse.comma.behavior.component.utilities.ClauseFragments;
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities;
import org.eclipse.comma.behavior.component.utilities.FunctionFragment;
import org.eclipse.comma.behavior.component.utilities.FunctionFragmentCall;
import org.eclipse.comma.behavior.component.utilities.IfHelperAction;
import org.eclipse.comma.behavior.component.utilities.StateTransitionAction;
import org.eclipse.comma.behavior.component.utilities.TransitionDecomposer;
import org.eclipse.comma.behavior.component.utilities.TransitionFragment;
import org.eclipse.comma.expressions.expression.Expression;
import org.eclipse.comma.expressions.expression.ExpressionVariable;
import org.eclipse.comma.expressions.expression.Variable;
import org.eclipse.comma.signature.interfaceSignature.Command;
import org.eclipse.comma.signature.interfaceSignature.DIRECTION;
import org.eclipse.comma.signature.interfaceSignature.Notification;
import org.eclipse.comma.signature.interfaceSignature.Parameter;
import org.eclipse.comma.signature.interfaceSignature.impl.SignalImpl;
import org.eclipse.comma.types.types.Type;
import org.eclipse.comma.types.types.TypeObject;
import org.eclipse.comma.types.types.TypeReference;
import org.eclipse.xtext.EcoreUtil2;

public class PythonSBFCConverter extends PythonConstraintConverter {
	private static StateTransitionAction stateTrans;
	private static List<String> functions = new ArrayList<String>();
	public static String convert(StateBasedFunctionalConstraint constraint) {
		var sb = new StringBuilder();
		var className = constraint.getName() + "Constraint";
		functions = new ArrayList<String>();
		sb.append("@dataclass\n");
		sb.append("class " + className + "(Constraint):\n");
		
		// Constraints States
		var initial = constraint.getStates().stream().filter(s -> s.isInitial()).findFirst().get();
		sb.append(indent1 + "states: List[ConstraintState]\n\n");
			
		// Use events
		sb.append(indent1 + "use_event = [\n");
		for (var event : constraint.getUsedEvents()) {
			sb.append(indent2 + convertEventPattern(event, new ArrayList<Expression>()) + ",\n");
		}
		sb.append(indent1 + "]\n\n");
		
		// __init__
		//if (!constraint.getVars().isEmpty()) { COMMENTED: we always generate init method
			sb.append(indent1 + "def __init__(self, states=None, init=True):\n");
			sb.append(indent2 + "if init:\n");
			sb.append(indent3 + "v = Variables({})\n");
			//initialize with default value 
			for (var variable : constraint.getVars()){
				var type = variable.getType();
				String varName = String.format("%s%s", "v.", variable.getName());
				if (type instanceof TypeReference) {
					sb.append(String.format("%s%s = %s\n", indent3, varName, PythonHelper.defaultValue(variable.getType().getType())));
				} else {
					sb.append(String.format("%s%s = %s\n", indent3, varName, PythonHelper.defaultValue((TypeObject)variable.getType())));
				}
			}
			//init
			for (var action : constraint.getInitActions()) {
				sb.append(String.format("%s%s\n", indent3, PythonHelper.action(action, (v) -> "v.", true, false)));
			}
			sb.append(indent3 + String.format("initState = [ConstraintState('%s', None, v)]\n", initial.getName()));
			sb.append(indent3 + "self.states = initState\n");
			sb.append(indent2 + "else:\n");
			sb.append(indent3 + "self.states = states\n");
			sb.append("\n");
		//}

		// __str__
		sb.append(indent1 + "def __str__(self) -> str:\n");
		sb.append(indent2 + "return f\"{__class__.__name__} {self.states}\"\n\n");
		
		// Fragments
		sb.append(indent1  + "def take(self, event: Event, port_machine_state: Dict[str, Dict[str, str]]):\n");
		sb.append(indent2 + "if len([e for e in self.use_event if e.equals(event, True)]) == 0:\n");
		sb.append(indent3 + "return self\n\n");
		sb.append(indent2 + "states = []\n");
		sb.append(indent2 + "for cstate in self.states:\n");
		sb.append(indent3 + "index = cstate.index\n");
		sb.append(indent3 + "s = cstate.state\n");
		sb.append(indent3 + "v = cstate.variables\n");
		
		for (var state : constraint.getStates()) {
			
			sb.append(indent3 + "if s == '" + state.getName() + "':\n");
			for(var transition : state.getTransitions()) {
				var transitionIndex = state.getTransitions().indexOf(transition);
				sb.append(indent4 + "# Transition " + transitionIndex + "\n");
				//decompose each clause to fragments
				var firstClause = true;
				for (var clause: transition.getClauses()) {
					var fragments = TransitionDecomposer.decomposeTransition(constraint, transition, clause);
					stateTrans = null;
					var rootTransition = fragments.getRootFragment().getTransition();
					if (rootTransition instanceof TriggeredTransition) {
						if (firstClause) {
							sb.append(convertFragments(fragments, className, clause, state, transitionIndex, true));
							firstClause = false;
						} else {
							sb.append(convertFragments(fragments, className, clause, state, transitionIndex, false));
						}
					} else if (rootTransition instanceof NonTriggeredTransition) {
						for (var subClause: rootTransition.getClauses()) {
							var subFragments = TransitionDecomposer.decomposeTransition(constraint, rootTransition, subClause);
							sb.append(convertFragments(subFragments, className, clause, state, transitionIndex, true));
						}
					}
				}
			}
			
			if (state.getTransitions().isEmpty()) {
				sb.append(indent4 + "pass\n");
			}
		}
		sb.append(indent2 + "if len(states):\n");
		sb.append(String.format("%sreturn %s(states, False)\n", indent3, className));
		sb.append("\n" + indent2 + "return None\n\n");
		//Convert FunctionFragment
		for (int i = 0; i < functions.size(); i++) {
			sb.append(indent1 + "def function_" + i +"(self, v):\n");
			sb.append(functions.get(i)+"\n");
		}
		sb.append(indent1 + "def get_state(self) -> List[ConstraintState]:\n");
		sb.append(indent2 + "return self.states\n\n");
		sb.append(indent1 + "def set_state(self, state: List[ConstraintState]):\n");
		sb.append(indent2 + "self.states = state\n");

		return sb.toString();
	}
	
	private static String convertFragments(ClauseFragments cf, String className, Clause clause, State state, int transitionIndex, boolean firstClause) {
		var sb = new StringBuilder();
		//assign index to the transition fragment
		var actionIndex = 1;
		cf.getRootFragment().setMethodName("None");
		for (var trans: cf.getSubTransitionFragments()) {
			trans.setMethodName("("+ transitionIndex +","+ actionIndex + ")");
			actionIndex++;
		}
		//convert triggered transition to transition
		if (firstClause) {
			if (cf.getRootFragment().getTransition() instanceof TriggeredTransition) {
				sb.append(convertTransition(cf.getRootFragment(), className, state));
				firstClause = false;
			}
		}
		if (firstClause) {
			for (var subTrans: cf.getSubTransitionFragments()) {
				sb.append(convertActions(subTrans.getActions(), className, state, "None", indent5));
			}
		} else {
			for (var subTrans: cf.getSubTransitionFragments()) {
				sb.append(convertActions(subTrans.getActions(), className, state, subTrans.getMethodName(), indent5));
			}
		}
		
		return sb.toString();
	}
	private static String convertStateTransition(Object action, String className, State state, String indent) {
		var sb = new StringBuilder();
		if (action instanceof StateTransitionAction) {
			var isLastAction = false;
			StateTransitionAction stateAction = (StateTransitionAction) action;
			var nextState = stateAction.getNextState();
			var nextIndex = "";
			if (stateAction.getTransitionFragment() != null) {
				nextIndex = stateAction.getTransitionFragment().getMethodName();
			} else {
				nextIndex = "None";
			}
			if (nextState != "") {
				isLastAction = true;
			}
			if (isLastAction) {
				sb.append(String.format("%sstates.append(%s('%s', %s, v))\n", indent, "ConstraintState", nextState, "None"));
			} else {
				sb.append(String.format("%sstates.append(%s('%s', %s, v))\n", indent, "ConstraintState", state.getName(), nextIndex));
			}
		}
		return sb.toString();
	}
	private static String convertIfAction(Object action, String className, State state, String nextState, String index, String indent) {
		var sb = new StringBuilder();
		if (action instanceof IfHelperAction) {
			IfHelperAction ifaction = (IfHelperAction) action;
			Expression condition = ifaction.getCondition();
			sb.append(String.format("%sif %s:\n", indent, PythonHelper.expression(condition, (String v) -> "v.", (e) -> handleUnsupportedExpression(e))));
			if (ifaction.getThenActions() != null) {
				sb.append(convertActions(ifaction.getThenActions(), className, state, index, indent+indent1));
			}
			if (ifaction.getElseActions() != null) {
				sb.append(String.format("%selse:\n", indent));
				sb.append(convertActions(ifaction.getElseActions(), className, state, index, indent+indent1));
			}
		} else if (action instanceof IfAction) {
			IfAction ifaction = (IfAction) action;
			Expression condition = ifaction.getGuard();
			sb.append(String.format("%sif %s:\n", indent, PythonHelper.expression(condition, (String v) -> "v.", (e) -> handleUnsupportedExpression(e))));
			if (ifaction.getThenList() != null) {
				List<Object> actions = new ArrayList<Object>(ifaction.getThenList().getActions());
				sb.append(convertActions(actions, className, state, index, indent+indent1));
			}
			if (ifaction.getElseList() != null) {
				sb.append(String.format("%selse:\n", indent));
				List<Object> actions = new ArrayList<Object>(ifaction.getElseList().getActions());
				sb.append(convertActions(actions, className, state, index, indent+indent1));
			}
		}
		
		return sb.toString();
	}
	
	private static String convertTransition(TransitionFragment tf, String className, State state) {
		var sb = new StringBuilder();
		var transition = tf.getTransition();
		if (transition instanceof TriggeredTransition) {
			TriggeredTransition triggertran = ((TriggeredTransition) transition);
			EventPattern eventPattern = ComponentUtilities.makeEvent(triggertran.getTrigger(), triggertran.getReplyTo(), triggertran, new ArrayList<Expression>());
			List<Expression> patternParameters = getEventPatternParameters(eventPattern);
			List<Variable> patternVariables = new ArrayList<Variable>();
			Expression condition = null;
			Variable idVarDef = null;

			if (triggertran.getTrigger() != null) {
				patternVariables = getTriggeredTransitionParameters((TriggeredTransition) transition).stream().map(p -> {
					var idx = getTriggeredTransitionParameters((TriggeredTransition) transition).indexOf(p);
					var parameter = triggertran.getParameters().get(idx);
					return parameter;
				}).collect(Collectors.toList());
			} else {
				patternVariables = triggertran.getParameters();
			}
			
			condition = transition.getGuard();
			idVarDef = ((TriggeredTransition) transition).getIdVarDef();
			
			sb.append(convertEventAction(eventPattern, patternParameters, patternVariables, idVarDef, null, condition, className, "None"));
			//actions belongs to the triggered transition
			sb.append(convertActions(tf.getActions(), className, state, tf.getMethodName(), condition != null ? indent6 : indent5));
			if(idVarDef != null) {
				throw new RuntimeException("Connection variables not supported");
			}
		}
		return sb.toString();
	}
	
	private static String convertAssignment(Object action, String indent) {
		var sb = new StringBuilder();
		if(action instanceof AssignmentAction) {
			String assignment = String.format("%s%s\n", indent, PythonHelper.action((AssignmentAction) action, (String v) -> "v.", true, false));
			sb.append(assignment);
		} else if(action instanceof RecordFieldAssignmentAction){
			String assignment = String.format("%s%s\n", indent, PythonHelper.action((RecordFieldAssignmentAction) action, (String v) -> "v.", true, false));
			sb.append(assignment);
		}
		return sb.toString();
	}
	
	private static String convertActions(List<Object> actions, String className, State state, String index, String indent) {
		var sb = new StringBuilder();
		String reply = "";
		List<String> assignments = new ArrayList<String>();
		var nextState = state.getName();
		Expression condition = null;
		var functionCallString = new ArrayList<String>();
		var states = new ArrayList<StateTransitionAction>();
		var extraFrontIndent = false;
		for(var action : actions) {
			EventPattern eventPattern;
			List<Expression> patternParameters = new ArrayList<Expression>();
			List<Variable> patternVariables = new ArrayList<Variable>();
			Variable idVarDef = null;
			ExpressionVariable idVar = null;
			if(action instanceof TriggeredTransition) {
				var transition = ((TriggeredTransition) action);
				eventPattern = ComponentUtilities.makeEvent(transition.getTrigger(), transition.getReplyTo(), transition, new ArrayList<Expression>());
				patternParameters = getEventPatternParameters(eventPattern);
				patternVariables = getTriggeredTransitionParameters(transition).stream().map(p -> {
					var idx = getTriggeredTransitionParameters((TriggeredTransition) transition).indexOf(p);
					var parameter = transition.getParameters().get(idx);
					return parameter;
				}).collect(Collectors.toList());
				condition = transition.getGuard();
				idVarDef = transition.getIdVarDef();
				sb.append(convertEventAction(eventPattern, patternParameters, patternVariables, idVarDef, idVar, condition, className, index));
			} else if(action instanceof EventCall) {
				var event = ((EventCall) action).getEvent();
				eventPattern = ComponentUtilities.makeEvent(event, null, (EventCall) action, new ArrayList<Expression>());
				patternParameters = ((EventCall)action).getParameters();
				idVar = ((EventCall) action).getIdVar();
				sb.append(convertEventAction(eventPattern, patternParameters, patternVariables, idVarDef, idVar, condition, className, index));
			} else if(action instanceof EventWithVars) {
				var event = ((EventWithVars) action).getEvent();
				eventPattern = ComponentUtilities.makeEvent(event, null, (EventWithVars) action, new ArrayList<Expression>());
				patternVariables = event.getParameters().stream().filter(p -> p.getDirection() != DIRECTION.OUT).map(p -> {
					var idx = event.getParameters().indexOf(p);
					var parameter = ((EventWithVars) action).getParameters().get(idx);
					return parameter;
				}).collect(Collectors.toList());
				idVarDef = ((EventWithVars) action).getIdVarDef();
				condition = ((EventWithVars) action).getCondition();
				extraFrontIndent = (condition != null);
				sb.append(convertEventAction(eventPattern, patternParameters, patternVariables, idVarDef, null, condition, className, index));
				
			} else if(action instanceof Reply) {
				var command = getCommand((Reply) action);
				eventPattern = ComponentUtilities.makeEvent(null, command, (PortSelector) action, new ArrayList<Expression>());
				if(action instanceof CommandReply) {
					patternParameters = ((CommandReply) action).getParameters();
					idVar = ((CommandReply) action).getIdVar();
				} else {
					patternVariables = ((ActionWithVars) action).getParameters();
					condition = ((ActionWithVars) action).getCondition();
					extraFrontIndent = (condition != null);
					idVarDef = ((CommandReplyWithVars) action).getIdVarDef();
				}
				reply = convertEventAction(eventPattern, patternParameters, patternVariables, idVarDef, idVar, condition, className, index);
			} else if(action instanceof AssignmentAction) {
				String assignment = convertAssignment((AssignmentAction) action, (extraFrontIndent ? indent1 : "") + indent);
				assignments.add(assignment);
			} else if(action instanceof RecordFieldAssignmentAction){
				String assignment = convertAssignment((RecordFieldAssignmentAction) action, (extraFrontIndent ? indent1 : "") + indent);
				assignments.add(assignment);
			} else if(action instanceof StateTransitionAction) {
				StateTransitionAction stateAction = (StateTransitionAction) action;
				states.add(stateAction);
				nextState = stateAction.getNextState();
			} else if(action instanceof FunctionFragmentCall) {
				var function = ((FunctionFragmentCall) action).getFunctionFragment();
				var idx = 0;
				var funcString = convertFunction(function, className, state);
				if (functions.contains(funcString)) {
					idx = functions.indexOf(funcString);
				} else {
					idx = functions.size();
					functions.add(funcString);
				}
				functionCallString.add(String.format("%sself.function_%s(v)\n", condition != null ? indent+indent1 : indent, idx));
				states.add(stateTrans);
			} else if (action instanceof IfAction) {
				assignments.add(convertIfAction(action, className, state, nextState, index, (extraFrontIndent ? indent1 : "") + indent));
			} else if (action instanceof IfHelperAction) {
				condition = ((IfHelperAction) action).getCondition();
				assignments.add(convertIfAction(action, className, state, nextState, index, (extraFrontIndent ? indent1 : "") + indent));
		    } else {
				throw new RuntimeException("Action not supported");
			} 
		}
		
		if (assignments.size() > 0) {
			sb.append(reply);
			for(String assignment: assignments) {
				sb.append(assignment);
			}
		} else {
			sb.append(reply);
		}
		
		for(int i = 0; i < functionCallString.size(); i++) {
			sb.append(functionCallString.get(i));
		}
		
		if (states.size() > 0) {
			sb.append(convertStateTransition(states.get(0), className, state, condition != null ? indent+indent1 : indent));
		}
		
		return sb.toString();
	}
	
	private static String convertFunction(FunctionFragment function, String className, State state) {
		var sb = new StringBuilder();
		for(var action: function.getActions()) {
			if(action instanceof FunctionFragmentCall) {
				var funcFragment = ((FunctionFragmentCall) action).getFunctionFragment();
				sb.append(convertFunction(funcFragment, className, state));
			} else if (action instanceof AssignmentAction || action instanceof RecordFieldAssignmentAction){
				sb.append(convertAssignment(action, indent2));
			} else if (action instanceof IfHelperAction) {
				sb.append(convertIfAction(action, className, state, "", function.getMethodName(), indent2));
			} else if (action instanceof IfAction) {
				sb.append(convertIfAction(action, className, state, "", function.getMethodName(), indent2));
			} else if (action instanceof StateTransitionAction) {
				stateTrans = (StateTransitionAction) action;
			}
		}
		return sb.toString();
	}
	
	private static String convertEventAction(EventPattern eventPattern, List<Expression> patternParameters, List<Variable> patternVariables, Variable idVarDef, ExpressionVariable idVar, Expression condition, String className, String index) {
		var sb = new StringBuilder();
		var eventPatternStr = convertEventPattern(eventPattern, patternParameters);
		var skipParameters = patternParameters.isEmpty() ? "True" : "False";
		if(idVar != null) {
			sb.append(String.format("%sif index == %s and %s.equals(event, %s) and (event.component == v.%s):\n", indent4, index, eventPatternStr, skipParameters, idVar.getVariable().getName()));
		} else {
			sb.append(String.format("%sif index == %s and %s.equals(event, %s):\n", indent4, index, eventPatternStr, skipParameters));
		}
		sb.append(indent5 + "v = cstate.variables.copy()\n");
		for (var v : patternVariables) {
			sb.append(String.format("%sv.%s = event.parameters[%d].value\n", indent5, v.getName(), patternVariables.indexOf(v)));
		}
		if(idVarDef != null) {
			sb.append(String.format("%sv.%s = event.component\n", indent5, idVarDef.getName()));
		}
		var hasCondition = condition != null;
		if (hasCondition) {
			sb.append(String.format("%sif %s:\n", indent5, PythonHelper.expression(condition, (String v) -> "v.", (e) -> handleUnsupportedExpression(e))));
		}
		return sb.toString();
	}
	
	private static String convertEventPattern(EventPattern eventPattern, List<Expression> parameterExpressions) {
		String type;
		String port;
		List<Type> parameterTypes;
		String name;
		String itf;
		
		if (eventPattern instanceof CommandEvent) {
			var e = (CommandEvent) eventPattern;
			port = e.getPort().getName();
			itf = e.getPort().getInterface().getName();
			type = "Command";
			name = ((Command) e.getEvent()).getName();
			parameterTypes = e.getEvent().getParameters().stream().filter(p -> p.getDirection() != DIRECTION.OUT)
					.map(p -> p.getType()).collect(Collectors.toList());
		} else if (eventPattern instanceof CommandReply) {
			var e = (CommandReply) eventPattern;
			port = e.getPort().getName();
			itf = e.getPort().getInterface().getName();
			type = "Reply";
			if(e.getCommand() != null) {
				name = e.getCommand().getEvent().getName();
				parameterTypes = e.getCommand().getEvent().getParameters().stream().filter(p -> p.getDirection() != DIRECTION.IN)
						.map(p -> p.getType()).collect(Collectors.toList());
				parameterTypes.add(((Command) e.getCommand().getEvent()).getType());
			} else {
				name = "";
				parameterTypes = new ArrayList<Type>();	
			}
		} else if (eventPattern instanceof NotificationEvent) {
			var e = (NotificationEvent) eventPattern;
			port = e.getPort().getName();
			itf = e.getPort().getInterface().getName();
			type = "Notification";
			name = ((Notification) e.getEvent()).getName();
			parameterTypes = e.getEvent().getParameters().stream().map(p -> p.getType()).collect(Collectors.toList());
		} else if (eventPattern instanceof SignalEvent) {
			var e = (SignalEvent) eventPattern;
			port = e.getPort().getName();
			itf = e.getPort().getInterface().getName();
			type = "Signal";
			name = ((SignalImpl) e.getEvent()).getName();
			parameterTypes = e.getEvent().getParameters().stream().map(p -> p.getType()).collect(Collectors.toList());
		} else if(eventPattern instanceof AnyEvent) {
			var e = (AnyEvent) eventPattern;
			type = "Any";
			if(e.getKind() == EVENT_KIND.CALL) type = "Command";
			if(e.getKind() == EVENT_KIND.SIGNAL) type = "Signal";
			if(e.getKind() == EVENT_KIND.NOTIFICATION) type = "Notification";
			port = e.getPort().getName();
			itf = e.getPort().getInterface().getName();
			name = "";
			parameterTypes = new ArrayList<Type>();
		} else {
			throw new RuntimeException("Not supported"); 
		}
		
		String parametersStr = parameterExpressions.stream().map(expression -> {
			var parameterType = parameterTypes.get(parameterExpressions.indexOf(expression));
			Function<String, String> variablePrefix = (String variable) -> { 
				return "str(" + variable + ")"; 
			};
			return PythonHelper.parameter(parameterType, expression, -1, variablePrefix) + ".eval()";
		}).collect(Collectors.joining(", "));
		
		return String.format("Event(EventType.%s, '%s', '%s', '', '%s', [%s], PortDirection.Unknown, False)", type, itf, port, name, parametersStr);
	}
	
	private static Command getCommand(Reply r) {
	    if(r.getCommand() != null) {
	        return (Command)r.getCommand().getEvent();
	    }
	    var transition = EcoreUtil2.getContainerOfType(r, TriggeredTransition.class);
	    if(transition.getTrigger() != null) {
	        if(transition.getTrigger() instanceof Command)
	           return (Command)transition.getTrigger();
	    }
	    return null;
	}
	
	private static List<Expression> getEventPatternParameters(EventPattern eventPattern) {
		if (eventPattern instanceof CommandEventImpl) {
			return ((CommandEventImpl) eventPattern).getParameters();
		} else if (eventPattern instanceof CommandReplyImpl) {
			return ((CommandReplyImpl) eventPattern).getParameters();
		} else if (eventPattern instanceof NotificationEventImpl) {
			return ((NotificationEventImpl) eventPattern).getParameters();
		} else if (eventPattern instanceof SignalEventImpl) {
			return ((SignalEventImpl) eventPattern).getParameters();
		} else {
			throw new RuntimeException("Not supported");
		}
	}
	
	private static List<Parameter> getTriggeredTransitionParameters(TriggeredTransition transition) {
		List<Parameter> params = new ArrayList<Parameter>();
		if (transition.getTrigger() != null ) {
			params = transition.getTrigger().getParameters().stream().filter(p -> p.getDirection() != DIRECTION.OUT).collect(Collectors.toList());
		} else if (transition.getReplyTo() != null) {
			params = transition.getReplyTo().getParameters().stream().filter(p -> p.getDirection() != DIRECTION.IN).collect(Collectors.toList());
		}
		return params;
	}
}
