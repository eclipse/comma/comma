/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint;

public class PythonPFCConverter extends PythonConstraintConverter {
	public static String convert(PredicateFunctionalConstraint constraint) {
		var sb = new StringBuilder();
		var className = constraint.getName() + "Constraint";
		
		sb.append("@dataclass\n");
		sb.append("class " + className + ":\n");
		sb.append(indent1  + "def take(self, event: Event, port_machine_state: Dict[str, Dict[str, str]]):\n");
		
		var expression = PythonHelper.expression(constraint.getExpression(), 
				(String e) -> {throw new RuntimeException("Should not be needed");}, 
				(e) -> handleUnsupportedExpression(e));
		
		sb.append(indent2 + String.format("if %s:\n", expression));
		sb.append(indent3 + "return self\n");
		sb.append(indent2 + "else:\n");
		sb.append(indent3 + "return None\n\n");
		sb.append(indent1 + "def get_state(self) -> Dict[str, Any]: return {}\n\n");
		sb.append(indent1 + "def set_state(self, state: Dict[str, Any]): pass\n");

		return sb.toString();
	}
}
