/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.function.Function;

class Input {

	final Place place;
	final PTransition transition;
	
	Input(Place place, PTransition transition) {
		this.place = place;
		this.transition = transition;
	}
	
	String toPython(Function<String, String> variablePrefix) {
		String label = "";
		if (place.type == Place.PPlaceType.STATE) label = "Variable('t')";
		else if (place.type == Place.PPlaceType.VARIABLES) label = "Variable('g')";
		else if (place.type == Place.PPlaceType.PARAMETERS) label = "Variable('p')";
		else if (place.type == Place.PPlaceType.CLAUSE || place.type == Place.PPlaceType.TRANSITION) 
			label = "Variable('gl')";
		else throw new RuntimeException("Should not happen");
		
		return String.format("n.add_input('%s', '%s', %s)\n", 
				place.name, transition.getPythonName(variablePrefix), label);
	}
}
