/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.comma.expressions.expression.Expression;

class Token {
	
	private final List<String> variables;
	private final List<Expression> parameters;

	private Token(List<String> variables, List<Expression> parameters) {
		this.variables = variables;
		this.parameters = parameters;
	}
	
	static Token empty() {
		return new Token(null, null);
	}
	
	static Token forParameters(List<Expression> parameters) {
		return new Token(null, parameters);
	}
	
	static Token forVariables(List<String> variables) {
		return new Token(variables, null);
	}
	
	String toPython() {
		if (this.parameters != null) {
			var str = this.parameters.stream()
					.map(s -> PythonHelper.expression(s, (String variable) -> "p_"))
					.collect(Collectors.joining(","));
			return String.format("Parameters([%s])", str);
		} else if (this.variables != null) {
			var str = this.variables.stream().map(s -> String.format("'%s': %s%s", s, "v_", s))
					.collect(Collectors.joining(","));
			return String.format("Variables({%s})", str);
		} else {
			return "''";
		}
	}
}
