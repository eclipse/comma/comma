/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.comma.behavior.behavior.Clause;
import org.eclipse.comma.behavior.behavior.InAllStatesBlock;
import org.eclipse.comma.behavior.behavior.State;
import org.eclipse.comma.behavior.behavior.StateMachine;
import org.eclipse.comma.behavior.behavior.Transition;
import org.eclipse.comma.behavior.behavior.TriggeredTransition;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;

class Place {
	
	enum PPlaceType { PARAMETERS, STATE, TRANSITION, CLAUSE, VARIABLES }

	final String name;
	final PPlaceType type;
	
	final State state;
	final StateMachine machine;
	String sourceLine;
	private final List<Token> tokens = new ArrayList<Token>();

	private Place(String name, PPlaceType type, StateMachine machine, State state) {
		this(name, type, machine, state, null);
	}

	private Place(String name, PPlaceType type, StateMachine machine, State state, String sourceLine) {
		this.name = name;
		this.type = type;
		this.state = state;
		this.machine = machine;
		this.sourceLine = sourceLine;
		assert (this.type == PPlaceType.VARIABLES || this.type == PPlaceType.PARAMETERS) || this.machine != null;
	}
	
	private static String name(String prefix, StateMachine machine, String name) {
		String machineName = "";
		// If an interface has multiple machines, add machine name to the place
		if (((Interface) machine.eContainer()).getMachines().size() > 1) {
			machineName = "_" + machine.getName();
		}
		
		return String.format("%s%s_%s", prefix, machineName, name);
	}
	
	static Place forState(StateMachine machine, State state) {
		return new Place(name("S", machine, state.getName()), PPlaceType.STATE, machine, state);
	}
	
	static Place forVariables() {
		return new Place("V", PPlaceType.VARIABLES, null, null);
	}
	
	static Place forTransition(StateMachine machine, State state, Transition transition) {
		String name = name("T", machine, state.getName());
		
		String transitionName = "NonTriggered";
		if (transition instanceof TriggeredTransition) {
			transitionName = ((TriggeredTransition) transition).getTrigger().getName() + "()";
		}
		
		String key = Integer.toString(state.getTransitions().indexOf(transition));
		if (key.equals("-1")) { // Transition is from in all states
			InAllStatesBlock allState = machine.getInAllStates().stream()
					.filter(s -> s.getTransitions().contains(transition)).findFirst().get();
			key = String.format("ias%d.%d", machine.getInAllStates().indexOf(allState), 
					allState.getTransitions().indexOf(transition));
		}

		name = String.format("%s_%s_%s", name, key, transitionName);
		return new Place(name, PPlaceType.TRANSITION, machine, state);
	}
	
	static Place forClause(Place transitionPlace, Clause clause) {
		int index = ((Transition) clause.eContainer()).getClauses().indexOf(clause);
		String name = String.format("C%s_%d_0", transitionPlace.name.substring(1), index);
		INode node = NodeModelUtils.getNode(clause);
		return new Place(name, PPlaceType.CLAUSE, transitionPlace.machine, transitionPlace.state, "" + node.getStartLine());
	}
	
	static Place forParameters(PTransition transition) {
		String name = String.format("P_%s", transition.eventName(true));
		return new Place(name, PPlaceType.PARAMETERS, null, null);
	}
	
	static Place forParameters(PTransition transition, State state) {
		String name = String.format("P_%s_%s", state.getName(), transition.eventName(true));
		return new Place(name, PPlaceType.PARAMETERS, null, null);
	}
	
	static Place forClause(Place precedingPlace, String key) {
		Pattern p = Pattern.compile("^(.*)_\\d+(_join|_split|_quit)?$");
		Matcher m = p.matcher(precedingPlace.name);
		if (precedingPlace.type != PPlaceType.CLAUSE || !m.find()) {
			throw new RuntimeException("Not allowed");
		}
		String name = String.format("%s_%s", m.group(1), key);
		return new Place(name, PPlaceType.CLAUSE, precedingPlace.machine, precedingPlace.state);
	}
	
	static Place forClauseNest(Place precedingPlace, String key) {
		assert precedingPlace.type == PPlaceType.CLAUSE : "Not allowed";
		String name = String.format("%s_%s", precedingPlace.name, key);
		return new Place(name, PPlaceType.CLAUSE, precedingPlace.machine, precedingPlace.state);
	}
	
	boolean isInitial() {
		return this.type == PPlaceType.STATE && this.state.isInitial();
	}

	void addToken(Token token) {
		this.tokens.add(token);
	}
	
	String toPython(Interface itf) {
		String token = "";
		if (!this.tokens.isEmpty()) {
			token = String.format(", [%s]", this.tokens.stream()
					.map(t -> t.toPython()).collect(Collectors.joining(", ")));
		}
		
		String meta = String.format("'type': '%s', 'interface': '%s'", this.type.toString().toLowerCase(), itf.getName());
		if (this.machine != null) {
			meta += String.format(", 'machine': '%s'", this.machine.getName());
		}
		if (this.state != null) {
			meta += String.format(", 'state': '%s'", this.state.getName());
		}
		if (this.sourceLine != null) {
			meta += String.format(", 'sourceline': '%s'", this.sourceLine);
		}
		return String.format("add_place(Place('%s'%s), {%s})\n", name, token, meta);
	}
}
