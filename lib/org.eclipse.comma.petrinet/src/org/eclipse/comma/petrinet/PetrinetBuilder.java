/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.comma.behavior.behavior.RequiredPort;
import org.eclipse.comma.behavior.component.component.Component;
import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint;
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint;
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.behavior.interfaces.scoping.InterfaceUtilities;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.petrinet.Petrinet.ParameterPlaceMode;
import org.eclipse.xtext.scoping.IScopeProvider;

public class PetrinetBuilder {
	public enum Mode {
		INTERACTIVE, NOT_INTERACTIVE
	}
	public enum PortDirection {
		NONE, REQUIRED, PROVIDED
	}
	
	public static String forComponent(Component component, List<EnvConfig> environment, IScopeProvider scopeProvider, Mode mode) {
	    // Nets
		var allInterfaces = ComponentUtilities.getAllInterfaces(component.eContainer(), scopeProvider);
		var nets = new ArrayList<String>();
		var netsCode = new ArrayList<String>();
		for(var connection : environment) {
			var itf = allInterfaces.stream().filter(i -> InterfaceUtilities.getSignature(i, scopeProvider) == connection.port.getInterface()).findFirst().get();
			ParameterPlaceMode parameterPlaceMode = null;
			if (mode == Mode.INTERACTIVE) {
				parameterPlaceMode = connection.port instanceof RequiredPort ? ParameterPlaceMode.TRIGGERED_EMPTY_NONTRIGGERED_NONE :
					ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_EMPTY;
			} else {
				parameterPlaceMode = ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_NONE;
			}
			var portDirection = connection.port instanceof RequiredPort ? PortDirection.REQUIRED : PortDirection.PROVIDED;
			var net = new Petrinet(itf, connection.param, parameterPlaceMode, connection.port.getName(), connection.component, portDirection);
			netsCode.add(net.toPython("net_" + connection.component + "_" + connection.port.getName()));
			nets.add("('" + connection.port.getName() + "', '" + connection.component + "'): net_" + connection.component + "_" + connection.port.getName());
		}
		
		// Component constraints
		var constraints = new ArrayList<String>();
		var constrainsCode = new ArrayList<String>();
		if (component.getFunctionalConstraintsBlock() != null) {
			for (var constraint : component.getFunctionalConstraintsBlock().getFunctionalConstraints()) {
				if (constraint instanceof StateBasedFunctionalConstraint) {
					var c = (StateBasedFunctionalConstraint) constraint;
					constrainsCode.add(PythonSBFCConverter.convert(c));
					constraints.add(c.getName());
				} else if (constraint instanceof PredicateFunctionalConstraint) {
					var c = (PredicateFunctionalConstraint) constraint;
					constrainsCode.add(PythonPFCConverter.convert(c));
				} else {
					throw new RuntimeException("Not supported");
				}
			}
		}
		
		return build(netsCode, nets, constrainsCode, constraints);
	}
	
	public static String forInterface(Interface itf, Parameters params, IScopeProvider scopeProvider, Mode mode) {
		// Nets
		var nets = new ArrayList<String>();
		var netsCode = new ArrayList<String>();
		ParameterPlaceMode parameterPlaceMode = null;
		if (mode == Mode.INTERACTIVE) {
			parameterPlaceMode = ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_EMPTY;
		} else {
			parameterPlaceMode = ParameterPlaceMode.TRIGGERED_FILLED_NONTRIGGERED_NONE;
		}
		var net = new Petrinet(itf, params, parameterPlaceMode, itf.getName(), "c", PortDirection.NONE);
		netsCode.add(net.toPython("net_" + itf.getName()));
		nets.add("('" + itf.getName() + "', 'c') : net_" + itf.getName());
		return build(netsCode, nets, new ArrayList<String>(), new ArrayList<String>());
	}
	
	private static String build(List<String> netsCode, List<String> nets, List<String> constraintsCode, List<String> constraints) {
		var sb = new StringBuilder();
		
		sb.append("## net\n");
		sb.append("from snakes.nets import Transition, Place, Expression, Variable, PetriNet\n");
		sb.append("from typing import Tuple\n\n");
		
		netsCode.forEach(c -> sb.append(c + "\n\n"));
		
		constraintsCode.forEach(c -> sb.append(c + "\n\n"));
		
		sb.append("nets = {\n");
		nets.forEach(c -> sb.append(String.format("    %s,\n", c)));
		sb.append("}\n");
		
		sb.append("constraints = [\n");
		constraints.forEach(c -> sb.append(String.format("    %sConstraint,\n", c)));
		sb.append("]\n");

		return sb.toString();
	}
	
	public static String getModelCode() {
		return "## model.py\n" + getResourceText("/model.py") + "\n\n";
	}
	
	public static String getWalkerCode() {
		return "## walker.py\n" + getResourceText("/walker.py") + "\n\n";
	}
	
	public static String getReachabilityGraphCode() {
		return "## reachability_graph.py\n" + getResourceText("/reachability_graph.py") + "\n\n";
	}
	
	private static String getResourceText(String resource) {
		var stream = PetrinetBuilder.class.getResourceAsStream(resource);
		return new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));
	}
}
