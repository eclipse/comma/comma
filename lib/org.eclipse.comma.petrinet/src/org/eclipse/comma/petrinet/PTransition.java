/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.comma.actions.actions.ActionWithVars;
import org.eclipse.comma.actions.actions.CommandReply;
import org.eclipse.comma.actions.actions.CommandReplyWithVars;
import org.eclipse.comma.actions.actions.EventCall;
import org.eclipse.comma.actions.actions.EventWithVars;
import org.eclipse.comma.behavior.behavior.StateMachine;
import org.eclipse.comma.behavior.behavior.Transition;
import org.eclipse.comma.behavior.behavior.TriggeredTransition;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.petrinet.PetrinetBuilder.PortDirection;
import org.eclipse.comma.signature.interfaceSignature.DIRECTION;
import org.eclipse.comma.signature.interfaceSignature.Parameter;
import org.eclipse.comma.signature.interfaceSignature.Signal;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;

class PTransition {

	private static int nextID = 0;
	static void resetIDCounter() { nextID = 0; }
	final Transition transition;
	final String name;
	final Object event;
	final TriggeredTransition trigger;
	final boolean breakpoint;
	final String component;
	private final Guard guard;
	
	PTransition(Transition t, String component, boolean breakpoint) { this(t, null, null, null, component, breakpoint); }
	
	//PTransition(Object event) { this(event, null, null); }
	
	//PTransition(Object event, TriggeredTransition trigger) { this(event, null, trigger); }

	PTransition(Transition t, Guard guard, String component, boolean breakpoint) { this(t, null, guard, null, component, breakpoint); }
	
	//PTransition(Object event, Guard guard) { this(event, guard, null); }

	PTransition(Transition t, Object event, Guard guard, TriggeredTransition trigger, String component, boolean breakpoint) {
		this.name = "T" + nextID;
		this.transition = t;
		this.event = event;
		this.guard = guard;
		this.trigger = trigger;
		this.breakpoint = breakpoint;
		this.component = component;
		nextID++;
	}
	
	public String eventName(boolean postfixReply) {
		if (event instanceof CommandReply) {
			return trigger.getTrigger().getName() + (postfixReply ? "_reply" : "");
		} else if (event instanceof CommandReplyWithVars) {
			var t = EcoreUtil2.getContainerOfType((EObject)event, TriggeredTransition.class);
			return t.getTrigger().getName() + (postfixReply ? "_reply" : "");
		} else if (event instanceof EventCall) {
			EventCall e = (EventCall) event;
			return e.getEvent().getName();
		} else if(event instanceof EventWithVars) {
			EventWithVars e = (EventWithVars) event;
			return e.getEvent().getName();
		} else if (event instanceof TriggeredTransition) {
			TriggeredTransition t = (TriggeredTransition) event;
			return t.getTrigger().getName();
		}
		 
		throw new RuntimeException("Not supported");
	}

	String getPythonName(Function<String, String> variablePrefix) {
		String name = this.name;
		if (this.event != null) {
			name += String.format("_event_%s", eventName(true));
		}
		return name;
	}
	
	String toPython(Function<String, String> variablePrefix, Interface itf, String port, PortDirection portDirection) {
		String expr = "";
		boolean inActionWithVars = false;
		if (guard != null) {
			List<String> parameters = null;
			Function<String, String> vp = null;
			if (event instanceof TriggeredTransition) {
				var trigger = ((TriggeredTransition) event).getTrigger();
				var params = new ArrayList<String>();
				for(Parameter p : trigger.getParameters()) {
					if(p.getDirection() != DIRECTION.OUT) {
						params.add(((TriggeredTransition) event).getParameters().get(trigger.getParameters().indexOf(p)).getName());
					}
				}
				parameters = params;
				vp = (String variable) -> {
					for(String v : params) {
						if(v.equals(variable)) return "p[" + params.indexOf(v) + "]";
					}
					return "";
				};
			} else if (event instanceof ActionWithVars) {
				inActionWithVars = true;
				var params = ((ActionWithVars) event).getParameters().stream().map(p -> p.getName()).collect(Collectors.toList());
				parameters = params;
				vp = (String variable) -> {
					for(String v : params) {
						if(v.equals(variable)) return "p[" + params.indexOf(v) + "]";
					}
					return "gl.";
				};
			}
			
			expr = String.format(", Expression('%s')", guard.toPython(vp, parameters, inActionWithVars));
		}
		
		String eventMeta = "";
		if (this.event != null) {
			Function<String, String> variablePrefix2 = (String variable) -> {
				var prefix = event instanceof TriggeredTransition ? "" : "gl.";			
				return prefix + variablePrefix.apply(variable);
			};
			
			String name = eventName(false);
			String kind;
			String parameters = PythonHelper.parameters(this.event, trigger, variablePrefix2).stream().collect(Collectors.joining(","));
			if (event instanceof CommandReply) {
				kind = "Reply";
			} else if (event instanceof CommandReplyWithVars) {
				kind = "Reply";
			} else if (event instanceof EventCall) {
				kind = "Notification";
			} else if (event instanceof EventWithVars) {
				kind = "Notification";
			} else {
				kind = ((TriggeredTransition) event).getTrigger() instanceof Signal ? "Signal" : "Command";
			}
			String direction;
			if (portDirection == PortDirection.REQUIRED) {
				direction = "Required";
			} else if (portDirection == PortDirection.PROVIDED) {
				direction = "Provided";
			} else {
				direction = "Unknown";
			} 
			var breakpointStr = (breakpoint) ? "True" : "False";
			
			eventMeta = String.format(",'event': Event(EventType.%s, '%s', '%s', '%s', '%s', [%s], PortDirection.%s, %s)", kind, itf.getName(), port, this.component, name, parameters, direction, breakpointStr);
		}
		
		String typeMeta = "";
		String machineName = EcoreUtil2.getContainerOfType(transition, StateMachine.class).getName();
		if(this.event != null) {
			typeMeta = String.format("'type': '%s', 'machine': '%s'", "event", machineName);
		} else {
			typeMeta = String.format("'type': '%s', 'machine': '%s'", "none", machineName);
		}
		String meta = String.format("{%s%s}", typeMeta, eventMeta);
		return String.format("add_transition(Transition('%s'%s), %s)\n", getPythonName(variablePrefix), expr, meta);
	}
}

