/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet;

import org.eclipse.comma.behavior.component.component.ExpressionInterfaceState;
import org.eclipse.comma.behavior.component.component.FunctionalConstraint;
import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint;
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint;
import org.eclipse.comma.expressions.expression.Expression;

public class PythonConstraintConverter  {
	final static String indent1 = "    ";
	final static String indent2 = indent1 + indent1;
	final static String indent3 = indent1 + indent1 + indent1;
	final static String indent4 = indent1 + indent1 + indent1 + indent1;
	final static String indent5 = indent1 + indent1 + indent1 + indent1 + indent1;
	final static String indent6 = indent1 + indent1 + indent1 + indent1 + indent1 + indent1;

	public static String convert(FunctionalConstraint constraint) {
		if (constraint instanceof StateBasedFunctionalConstraint) {
			return PythonSBFCConverter.convert((StateBasedFunctionalConstraint) constraint);
		} else if (constraint instanceof PredicateFunctionalConstraint) {
			return PythonPFCConverter.convert((PredicateFunctionalConstraint) constraint);
		} else {
			throw new RuntimeException("Not supported");
		}
	}
	
	static String handleUnsupportedExpression(Expression expression) {
		if (expression instanceof ExpressionInterfaceState) {
			var e = (ExpressionInterfaceState) expression;
			if (e.getPart() != null) throw new RuntimeException("Unsupported");
			return String.format("'%s' in port_machine_state['%s'].values()", e.getState().getName(), e.getPort().getName(), e.getState().getName(), e.getPort().getName());
		}

		throw new RuntimeException("Not supported");
	}
}
