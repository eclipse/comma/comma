/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.testcases.tests

import org.eclipse.comma.behavior.interfaces.tests.InterfaceDefinitionInjectorProvider
import org.eclipse.comma.behavior.component.tests.ComponentInjectorProvider
import org.eclipse.comma.parameters.tests.ParametersInjectorProvider

class MultiLangInjectorProvider extends InterfaceDefinitionInjectorProvider {
    override protected internalCreateInjector() {
        new ComponentInjectorProvider().getInjector
        new ParametersInjectorProvider().getInjector
        return super.internalCreateInjector()
    }
}