/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.testcases.tests

import com.google.inject.Inject
import com.google.inject.Provider
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Stream
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.util.StringInputStream
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.DynamicTest.dynamicTest
import org.eclipse.comma.testcases.TestCasesGenerator
import org.junit.jupiter.api.BeforeAll
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class Tests {
    @Inject extension Provider<ResourceSet> resourceSet
    @Inject IScopeProvider scopeProvider
    
    static val templateFile = Files.createTempFile("template", "j2");
    static val gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping.create;
    static val resourcePath = "test_resources"

    @BeforeAll
    static def void beforeAll() {
        var template = TestCasesGenerator.getTemplate(null, "java")
        Files.write(templateFile, template.content.bytes);
    }
    
    def void test(String path) {
        var set = resourceSet.get()
        var Parameters parameters = null
        var testResourcePath = resourcePath + "/" + path
        var Interface itf = null
        var String expectedRg = null
        var String expectedS = null
        var String expectedC = null
        for (file : Paths.get(testResourcePath).toFile.listFiles) {
            var ext = file.name.substring(file.name.lastIndexOf("."));
            var contents = Files.readString(Paths.get(testResourcePath + "/" + file.name)).replaceAll("\r\n", "\n").trim()
            if (#[".interface", ".params", ".signature", ".types"].contains(ext)) {
                set.createResource(URI.createURI(file.name))
                    .load(new StringInputStream(contents), emptyMap)
                if (ext.equals(".params")) {
                    parameters = set.getResource(URI.createURI(file.name), true).contents.head as Parameters
                } else if (ext.equals(".interface")) {
                    itf = (set.getResource(URI.createURI(file.name), true).contents.head as InterfaceDefinition).interface
                }
            } 
            else if (file.name.equals("reachability_graph.json")) { expectedRg = contents }
            else if (file.name.equals("scenarios.json")) { expectedS = contents }
            else if (file.name.equals("code.txt")) { expectedC = contents }
        }
        

        
        var code = TestCasesGenerator.generate(itf, parameters, scopeProvider, 1000, templateFile.toString.replace("\\", "\\\\"), "", "testCase")
        var result = gson.fromJson(PythonInterpreter.execute(code, #["--output", "print"]), JsonObject);

        Assertions.assertEquals(expectedRg, gson.toJson(result.getAsJsonObject("reachability_graph")));
        Assertions.assertEquals(expectedS, gson.toJson(result.getAsJsonArray("scenarios")));
        Assertions.assertEquals(expectedC, result.get("code").asString);
    }

    @TestFactory
    def Stream<DynamicTest> testFromResources() {
        return Paths.get(resourcePath).toFile.listFiles.map[f | dynamicTest(f.name, [| test(f.name)])].stream
    }
}
