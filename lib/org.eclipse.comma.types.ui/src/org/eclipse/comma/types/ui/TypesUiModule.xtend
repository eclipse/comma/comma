/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.ui

import org.eclipse.jface.text.hyperlink.IHyperlinkDetector
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor
import org.eclipse.comma.types.ui.contentassist.CommaHyperLinkDetector

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class TypesUiModule extends AbstractTypesUiModule {
	
	override Class<? extends IHyperlinkDetector> bindIHyperlinkDetector() {
		return CommaHyperLinkDetector
	}
}
