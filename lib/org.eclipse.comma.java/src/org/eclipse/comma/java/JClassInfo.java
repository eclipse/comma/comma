/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;

/**
 * JClassInfo holds all information of a concrete java class needed by
 * {@link JArtifactBuilder}
 * 
 * @see JClassInfos
 */
public interface JClassInfo {

	/**
	 * Returns the location of a concrete java class (e.g
	 * /my-prj/target/my/App.class)
	 * 
	 * @return location of the class file.
	 */
	File getLocation();

	/**
	 * Returns the full qualified name of a class (e.g java.io.File)
	 * 
	 * @return full qualified name of the class.
	 */
	String getName();
}
