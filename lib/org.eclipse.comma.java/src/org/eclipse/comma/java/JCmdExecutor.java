/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

/**
 * The JCmdExecutor runs a {@link JCmd} and returns a {@link JProcessMonitor} upon execution.
 * <p>Some examples:
 * <code>
 * <pre>
 - JProcessMonitor monitor = new JCmdExecutor(cmd).execute();
 - JProcessMonitor monitor = new JCmdExecutor(cmd).withWorkingDirectory(workingDirectory).execute();
 - JProcessMonitor monitor = new JCmdExecutor(cmd).withOnStdOut((line) -> System.out.println(line)).execute();
 * </pre>
 * </code>
 * </p>
 * 
 * @see #run(JCmd)
 * @see #run(JCmd, File)
 * @see JCmdBuilder
 */
public class JCmdExecutor {
	
	private final ProcessBuilder processBuilder;
	private Consumer<String> onStdOut;
	private Consumer<String> onStdErr;
	
	/**
	 * Creates a new instance of the JCmdExecutor
	 * 
	 * @param cmd which we are going to execute.
	 */
	public JCmdExecutor(JCmd cmd) {
		processBuilder = new ProcessBuilder(cmd.toArray());
	}
	
	/**
	 * Set the directory in which the command will be executed
	 * 
	 * @param directory Desired working directory
	 * @return updated receiver's instance.
	 */
	public JCmdExecutor withWorkingDirectory(File directory) {
		processBuilder.directory(directory);
		return this;
	}
	
	/**
	 * Sets the onStdOut handler, this consumer will be called each time the 
	 * process writes something to the stdout during execution.
	 * 
	 * @param onStdOut consumer
	 * @return updated receiver's instance.
	 */
	public JCmdExecutor withOnStdOut(Consumer<String> onStdOut) {
		this.onStdOut = onStdOut;
		return this;
	}
	
	/**
	 * Sets the onStdErr handler, this consumer will be called each time the 
	 * process writes something to the stderr during execution.
	 * 
	 * @param onStdErr consumer
	 * @return updated receiver's instance.
	 */
	public JCmdExecutor withOnStdErr(Consumer<String> onStdErr) {
		this.onStdErr = onStdErr;
		return this;
	}
	
	/**
	 * Start execution of the command
	 * 
	 * @return {@link JProcessMonitor} the process monitor.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public JProcessMonitor execute() throws IOException, InterruptedException {
		return new JProcessMonitor(this.processBuilder, onStdOut, onStdErr);
	}
}
