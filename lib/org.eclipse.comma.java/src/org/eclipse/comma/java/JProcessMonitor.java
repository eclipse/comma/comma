/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

/**
 * JProcessMonitor provides a wrapper around the Process class. Its aim is to:
 * - Easily catch output written by the process (both stdout and stderr)
 * - Determining the execution time of the process
 * - Ensure that the input and error stream of the process get read to prevent the process from getting blocked
 * 
 * A JProcessMonitor instance is returned from the {@link JCmdExecutor} execute method.
 * <p>Example usage:
 * <code>
 * <pre>
 JProcessMonitor monitor = new JCmdExecutor(cmd).execute();
 monitor.process.waitFor(); // Blocks till process finishes
 String stdOut = monitor.getStdOut(); // Retrieve stdOut of the process
 String stdErr = monitor.getStdErr(); // Retrieve stdErr of the process
 double executionTime = monitor.getExecutionTime(); // Get execution time in seconds
 * </pre>
 * </code>
 * </p>
 * 
 */
public class JProcessMonitor {
	public final Process process;
	private final StringBuilder stdOut = new StringBuilder();
	private final StringBuilder stdErr = new StringBuilder();
	private final long startTime;
	private Long endTime = null;
	
	/**
	 * Creates a new instance of the JProcessMonitor
	 * 
	 * @param processBuilder processBuilder of the process
	 * @param onStdOut stdOut consumer, null is allowed
	 * @param onStdErr stdOut consumer, null is allowed
	 * @throws IOException 
	 */
	public JProcessMonitor(ProcessBuilder processBuilder, Consumer<String> onStdOut, Consumer<String> onStdErr) throws IOException {
		this.startTime = System.nanoTime();
		this.process = processBuilder.start();
		this.startMeasureEndTimeThread();
		// It is important to read the process output even when no onStdOut or onStdErr is given.
		// otherwise processes freeze when the stdout or stderr buffer is full.
		startOutConsumerThread(process.getInputStream(), onStdOut, stdOut);
		startOutConsumerThread(process.getErrorStream(), onStdErr, stdErr);
	}
	
	/**
	 * Retrieves all current stdOut output of the process
	 * 
	 * @return string containing the stdOut
	 */
	public String getStdOut() {
		return this.stdOut.toString();
	}
	
	/**
	 * Retrieves all current stdErr output of the process
	 * 
	 * @return string containing the stdErr
	 */
	public String getStdErr() {
		return this.stdErr.toString();
	}
	
	/**
	 * Retrieves the current execution time in seconds
	 * 
	 * @return execution time in seconds
	 */
	public double getExecutionTime() {
		var time = endTime != null ? endTime : System.nanoTime();
		return (time - startTime) / 1000000000.0;
	}
	
	private void startMeasureEndTimeThread() {
		new Thread() {
			@Override
			public void run() {
				try {
					process.waitFor();
					endTime = System.nanoTime();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}.start();
	}
	
	private void startOutConsumerThread(InputStream stream, Consumer<String> handler, StringBuilder sb) {
		new Thread() {
			@Override
			public void run() {
				try {
					byte[] buffer = new byte[1024];
					int charsRead;
					while ((charsRead = stream.read(buffer)) != -1) {
						var text = new String(buffer, 0, charsRead);
						sb.append(text);
						if (handler != null) {
							handler.accept(text);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}
