/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 * <h1>Introduction</h1>
 * <p>
 * The <code>org.eclipse.comma.java</code> provides you a set of functions which
 * could categories in
 * <ol>
 * <li>Compiling: compile an arbitrary number of java code</li>
 * <li>Creating Artifact: package an arbitrary number of classes into an certain
 * jar file</li>
 * <li>Executing Artifact: to run a this compiled jar file.</li>
 * <li>Utils: set of utility functions.
 * </ol>
 * 
 * You can define more details in the follow up descriptions
 * </p>
 * 
 * <h1>Compiling</h1>
 * <p>
 * To compile a java code you have normally the following requirements
 * <ul>
 * <li>a folder containing our sources (e.g /my-proj/java/src/**.java)</li>
 * <li>an arbitrary number of dependencies (e.g /my-proj/lib/**.jar)</li>
 * </ul>
 * To run a compilation you jave to use {@link org.eclipse.comma.java.JCompiler
 * JCompiler} class like
 * 
 * <pre>
 * <code>
 // fetch all *.java files under the /my-proj/java/src folder 
 Set&lt;JSourceInfo&gt;javaSourceInfos = JSourceInfos.getJavaSourcesFromSrcDirectory("/my-proj/java/src");

 // fetch all *.* files under the /my-proj/java/lib folder 
 Set&ltJDependencyInfo&gt; dependencyInfos = JDependencyInfos.getDependenciesFromDirectory("/my-proj/java/lib");

 // build and compile all 
 List&ltJClassInfo&gt; classInfos = new  JCompiler()
    .withSourceInfos(javaSourceInfos)
    .withDependencyInfos(dependencyInfos)
    .compile();
 * </code>
 * </pre>
 * </p>
 * <p>
 * Look for {@link org.eclipse.comma.java.JSourceInfos} and
 * {@link org.eclipse.comma.java.JDependencyInfos} for more information.
 * </p>
 * 
 * <h1>Creating Artifact</h1>
 * <p>
 * To package your java application we can assume the following requirements:
 * <ul>
 * <li>an arbitrary number of class files created by the compiler</li>
 * <li>an folder with arbitrary number of resource files (e.g.
 * "my-proj/java/resources")</li>
 * <li>an arbitrary number of dependencies (e.g /my-proj/lib/**.jar) we already
 * used for compiling</li>
 * </ul>
 * To build the artifact the code could look like:
 * 
 * <pre>
 * <code>
 // we assume this exists from compilation, see above Compiling
 Set&ltJClassInfo&gt; dependencyInfos;
 Set&ltJDependencyInfo&gt; dependencyInfos;
 
 // fetch all *.* files under the /my-proj/java/resource folder 
 Set&JSourceInfo&gt; resourceInfos = JSourceInfos.getResourcesFromSrcDirectory("/my-proj/java/resource");
 
 // create JArtifactBuilder and build JArtifact
 JArtifactInfo artifactInfo = new JArtifactInfoBuilder()
    .withArchiveFile(new File("C:/my-prj/target/my.jar"))
    .withResources(resourceInfos)
    .withClassInfos(classInfos)
    .withDependencyInfos(dependencyInfos)
    .withMainClass("org.sample.app.App")
    .build();		 
 * </code>
 * </pre>
 * </p>
 * <p>
 * Look for {@link org.eclipse.comma.java.JSourceInfos} and
 * {@link org.eclipse.comma.java.JDependencyInfos} for more information.
 * </p>
 * 
 * <h1>Executing Artifact</h1>
 * <p>
 * After compiling and creating our artifact we are going to run this artifiact.
 * Also hier we assume the following requirements:
 * <ul>
 * 	<li>Have artifact instance of {@link org.eclipse.comma.java.JArtifiact}
 * </ul>
 * Now we have to do the following steps
 * <ul>
 * 	<li>create {@link org.eclipse.comma.java.JCmd JCmd} with options and arguments we want to use</li>
 *  <li>execute {@link org.eclipse.comma.java.JCmd JCmd} via {@link org.eclipse.comma.java.JCmdExecutor JCmdExecutor} 
 * </ul>
 * </p>
 *  <p>An typical code snapshot would look like:
 * <pre>
 * <code>
 // we assume this exists from packing, see above Creating Artifact
 JArtifactInfo artifactInfo = ;

 // create the cmd with options and arguments
 JCmd cmd = new JCmdBuilder()
    .withArtifactInfo(jArtifactInfo)
    .withOptions("-Dmy.test.prop1=props-worked")
    .withArguments("arg1", "arg2")
    .build();

 // execute the command - this blocks until the application is terminated.
 java.lang.Process p = new JCmdExecutor().run(cmd);

 // read stdout/stderr to avoid buffer overflow (only if needed)
 BufferedReader errorStream = new BufferedReader(new InputStreamReader(p.getErrorStream()));
 BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));
 String line;
 while(p.isAlive()) {
   System.out.println("### Error stream");
   while((line = errorStream.readLine())!=null) {
     System.out.println(line);
   }

   System.out.println("### Input stream");
   while((line = inputStream.readLine())!=null) {
    System.out.println(line);
   }	
 }
 
 // or simple wait until the application terminates
 p.waitFor
 
 * </code>
 * </pre>  
 *  </p>
 * <p>
 * Look for {@link org.eclipse.comma.java.JCmdBuilder} for more information.
 * </p>
 *  
 *  
 * <h1>Utils</h1>
 * <p>
 * You found an set of useful functions under  {@link org.eclipse.comma.java.JSourceInfos} and
 * {@link org.eclipse.comma.java.JDependencyInfos}.
 * </p>
 */
package org.eclipse.comma.java;