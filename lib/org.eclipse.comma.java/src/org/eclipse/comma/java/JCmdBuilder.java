/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.eclipse.comma.java.impl.JCmdImpl;

/**
 * JCmdBuider is used to create {@link JCmd} instance which can be executed via
 * {@link JCmdExecutor}.
 * <p>
 * The java executable itself can be added manually by invoking
 * {@link JCmdBuilder#withJavaExec(String)} or the builder tries to determine a
 * proper java executable based on the system property
 * <ul>
 * <li><code>java.home</code></li> or 
 * <li><code>JAVA_HOME</code></li>
 * </ul>.
 * </p>
 * 
 * @see JCmd
 * @see JCmdExecutor
 * @see JArtifact
 */
public class JCmdBuilder {

	private final List<String> options;
	private final List<String> arguments;
	private String javaExec;
	private JArtifactInfo artifactInfo;

	/**
	 * For unit testing purpose only.
	 * 
	 * @param options   instance used to store the options
	 * @param arguments instance used store the arguments.
	 */
	protected JCmdBuilder(List<String> options, List<String> arguments) {
		this.options = Objects.requireNonNull(options, "options must not be null");
		this.arguments = Objects.requireNonNull(arguments, "arguments must not be null");
	}

	public JCmdBuilder(int optionsSize, int argumentsSize) {
		this(new ArrayList<>(optionsSize), new ArrayList<>(argumentsSize));
	}

	public JCmdBuilder() {
		this(5, 3);
	}

	public JCmdBuilder withJavaExec(String javaExec) {
		this.javaExec = javaExec;
		return this;
	}

	public JCmdBuilder withArtifactInfo(JArtifactInfo artifactInfo) {
		this.artifactInfo = Objects.requireNonNull(artifactInfo, "artifactInfo must not be null");
		return this;
	}

	public JCmdBuilder withOptions(Collection<String> options) {
		this.options.addAll(options);
		return this;
	}

	public JCmdBuilder withOptions(String... options) {
		for (String o : options) {
			this.options.add(Objects.requireNonNull(o, "option must not be null"));
		}
		return this;
	}

	public JCmdBuilder withArguments(Collection<String> arguments) {
		this.arguments.addAll(arguments);
		return this;
	}

	public JCmdBuilder withArguments(String... arguments) {
		for (String a : arguments) {
			this.arguments.add(Objects.requireNonNull(a, "argument must not be null"));
		}
		return this;
	}

	public JCmd build() {
		List<String> items = new ArrayList<>(4 + options.size() + arguments.size());

		items.add(getJavaExec());
		items.add("-classpath");
		items.add(getClasspathItems());
		items.addAll(options);
		items.add(artifactInfo.getMainClass());
		items.addAll(arguments);

		return new JCmdImpl(items);
	}

	private String getJavaExec() {
		return (javaExec != null ? javaExec : findJavaExec());
	}

	private static String findJavaExec() {
		String javaHome = System.getProperty("java.home");
		if (javaHome == null || javaHome.isEmpty()) {
			javaHome = System.getenv("JAVA_HOME");
		}
		if (javaHome == null) {
			throw new IllegalArgumentException("find java home dir failed");
		}

		return new File(javaHome, "bin/java").getAbsolutePath();
	}

	private String getClasspathItems() {
		final String jarFile = artifactInfo.getLocation().getAbsolutePath();
		final Collection<JDependencyInfo> dependencyInfos = artifactInfo.getDependencyInfos();
		var classpath = new StringBuilder();
		classpath.append(jarFile);
		if (!artifactInfo.getIncludesDependencies() && !dependencyInfos.isEmpty()) {
			classpath.append(JDependencyInfos.getClasspathSeparator());
			classpath.append(JDependencyInfos.toClasspath(dependencyInfos));
		}
		return classpath.toString();
	}
}
