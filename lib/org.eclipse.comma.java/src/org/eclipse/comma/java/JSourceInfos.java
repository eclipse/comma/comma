/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.comma.java.impl.JSourceInfoImpl;

/**
 * JSourceInfos provides a set of utility functions to make life more easier. We
 * can categories the methods like
 * <p>
 * Java Source files (*.java)
 * <ul>
 * <li>{@link #getJavaSourcesFromSrcDirectory(File)}</li>
 * <li>{@link #getJavaSourcesFromSrcDirectory(String)}</li>
 * <li>{@link #getJavaSourcesFromActiveProjectWithSrcDirectory(String)}</li>
 * <li>{@link #isJavaSource(File)}</li>
 * <li>{@link #isJavaSource(String)}</li>
 * </ul>
 * </p>
 * <p>
 * Resource files (every file which is not a java file)
 * <ul>
 * <li>{@link #getResourcesFromSrcDirectory(File)}</li>
 * <li>{@link #getResourcesFromSrcDirectory(String)}</li>
 * <li>{@link #getResourcesFromActiveProjectWithSrcDirectory(String)}</li>
 * <li>{@link #isResource(File)}</li>
 * <li>{@link #isResource(String)}</li>
 * </ul>
 * </p>
 */
public class JSourceInfos {

	/**
	 * Returns a set of java sources ({@link #isJavaSource(File)}) by invoking the
	 * receiver's {@link #getJavaSourcesFromSrcDirectory(File)}.
	 * 
	 * @param directory we are looking for *.java files.
	 * @return all found *.java files.
	 * @throws IOException if given <code>directory</code> or one of it;s
	 *                     subdirectory could not be accessed.
	 * @see JSourceInfo
	 * @see #isJavaSource(File)
	 */
	public static Set<JSourceInfo> getJavaSourcesFromSrcDirectory(String directory) throws IOException {
		return getJavaSourcesFromSrcDirectory(new File(directory));
	}

	/**
	 * Returns a set of java sources ({@link #isJavaSource(File)}) which are found
	 * under the given <code>directory</code> and it's sub directories.
	 * 
	 * @param directory we are looking for *.java files.
	 * @return all found *.java files.
	 * @throws IOException if given <code>directory</code> could not be accessed.
	 * @see JSourceInfo
	 * @see #isJavaSource(File)
	 */
	public static Set<JSourceInfo> getJavaSourcesFromSrcDirectory(File directory) throws IOException {
		try (Stream<Path> files = Files.walk(Paths.get(directory.getAbsolutePath()))) {

			return files.filter(p -> !p.toFile().equals(directory) && isJavaSource(p.toFile()))
					.map(p -> new JSourceInfoImpl(p.toFile(), getName(directory, p))).collect(Collectors.toSet());
		}

	}

	/**
	 * Returns a set of resources ({@link #isResource(File)}) by invoking the
	 * receiver's {@link #getResourcesFromSrcDirectory(File)}.
	 * 
	 * @param directory we are looking for resource files.
	 * @return all found resources.
	 * @throws IOException if given <code>directory</code> or one of it;s sub
	 *                     directory could not be accessed.
	 * @see JSourceInfo
	 * @see #isResource(File)
	 */
	public static Set<JSourceInfo> getResourcesFromSrcDirectory(String directory) throws IOException {
		return getJavaSourcesFromSrcDirectory(new File(directory));
	}

	/**
	 * Returns a set of resources ({@link #isResource(File)}) which are found under
	 * the given <code>directory</code> and it's sub directories.
	 * 
	 * @param directory we are looking for resource files.
	 * @return all found resources
	 * @throws IOException if given <code>directory</code> or one of it;s sub
	 *                     directory could not be accessed.
	 * @see JSourceInfo
	 * @see #isResource(File)
	 */
	public static Set<JSourceInfo> getResourcesFromSrcDirectory(File directory) throws IOException {
		try (Stream<Path> files = Files.walk(Paths.get(directory.getAbsolutePath()))) {

			return files.filter(p -> !p.toFile().equals(directory) && isResource(p.toFile()))
					.map(p -> new JSourceInfoImpl(p.toFile(), getName(directory, p))).collect(Collectors.toSet());
		}

	}

	/**
	 * Returns <code>true</code> if the given <code>file</code> is a file with the
	 * extension <i>.java</i>, otherwise returns <code>false</code>.
	 * 
	 * @param file we want to check.
	 * @return <code>true</code> if the file is a java file.
	 */
	public static boolean isJavaSource(File file) {
		return file != null && file.getAbsolutePath().endsWith(".java") && file.isFile();
	}

	/**
	 * Returns <code>true</code> if the given <code>sourceInfo</code>'s location is
	 * a java file, otherwise returns <code>false</code>.
	 * 
	 * @param sourceInfo we want to check
	 * @return <code>true</code> if the sourceInfo's location is a java file.
	 */
	public static boolean isJavaSource(JSourceInfo sourceInfo) {
		return sourceInfo != null && isJavaSource(sourceInfo.getLocation());
	}

	/**
	 * Returns <code>true</code> if the given <code>file</code> is a resource file,
	 * otherwise returns <code>false</code>.
	 * <p>
	 * A resource file is each file which is not a java source
	 * ({@link #isJavaSource(File)}==false)
	 * 
	 * @param file we want to check.
	 * @return <code>true</code> if the file is a resource file.
	 */
	public static boolean isResource(File file) {
		return file != null && !isJavaSource(file) && file.isFile();
	}

	/**
	 * Returns <code>true</code> if the given <code>sourceInfo</code>'s location is
	 * a resource file, otherwise returns <code>false</code>.
	 * <p>
	 * A resource file is each file which is not a java source
	 * ({@link #isJavaSource(File)}==false)
	 * 
	 * @param sourceInfo we want to check.
	 * @return <code>true</code> if the file is a resource file.
	 */
	public static boolean isResource(JSourceInfo sourceInfo) {
		return sourceInfo != null && !isJavaSource(sourceInfo);
	}

	static String getName(File directory, Path classFile) {
		String prefix = directory.getAbsolutePath();
		String className = classFile.toFile().getAbsolutePath();
		if (className.startsWith(prefix)) {
			className = className.substring(prefix.length());
		}

		className = className.replace('/', '.').replace('\\', '.').replaceFirst(".java$", "");
		if (className.indexOf('.') == 0) {
			className = className.substring(1);
		}

		return className;
	}

	static JarEntry toJarEntry(JSourceInfo jSrc) {
		String jarFileName = jSrc.getName();
		if (isJavaSource(jSrc)) {
			jarFileName = jSrc.getName().replaceAll("\\.", "/") + ".class";
		}
		return new JarEntry(jarFileName);
	}

}
