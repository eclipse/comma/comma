/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipException;

import org.eclipse.comma.java.impl.JArtifactInfoImpl;

/**
 * The JArtifactBuilder is used to create {@link JArtifact}.
 * <p>
 * To create an artifact you need the following part
 * <ul>
 * <li>archive filen name - see
 * {@link JArtifactInfoBuilder#withArchiveFile(File) withArchiveFile}</li>
 * <li>java classes - see
 * {@link JArtifactInfoBuilder#withClassInfos(JClassInfo...)
 * withClassInfos}</li>
 * <li>dependencies - see
 * {@link JArtifactInfoBuilder#withDependencyInfos(JDependencyInfo...)
 * withDependencyInfos}</li>
 * <li>resources - see {@link JArtifactInfoBuilder#withResources(JSourceInfo...)
 * withResources}</li>
 * <li>manifest - see {@link JArtifactInfoBuilder#withManifest(Manifest)
 * withManifest}</li>
 * <li>main class - see {@link JArtifactInfoBuilder#withMainClass(String)
 * withMainClass}</li>
 * </ul>
 * </p>
 * <p>
 * Manifest is optional. If no manifest is available it will be created
 * automatically and contains the main class and version 1.0.
 * </p>
 * 
 * <p>
 * We are not added the dependency jars into the created archive file.
 * </p>
 * 
 * @see JCompiler
 * @see JArtifactInfo
 * @see JCmdBuilder
 * @see #build()
 */
public class JArtifactInfoBuilder {

	private File archiveFile;
	private List<JClassInfo> classInfos = new ArrayList<>();
	private List<JDependencyInfo> dependencyInfos = new ArrayList<>();
	private List<JSourceInfo> resources = new ArrayList<>();
	private String mainClass;
	private Manifest manifest;
	private boolean includeDependencies = false;

	/**
	 * Sets the archive file name. This file will be overridden during the {@link #build()} process.
	 * 
	 * @param archiveFile where all resources and classes are added.
	 * @return updated receiver's instance.
	 * @throws NullPointerException if the given <code>archiveFile</code> argument is <code>null</code>
	 * @see #build()
	 */
	public JArtifactInfoBuilder withArchiveFile(File archiveFile) {
		this.archiveFile = Objects.requireNonNull(archiveFile, "archiveFile must not be null");
		// fixme: further valiation directory, file exists.
		return this;
	}
	
	/**
	 * Enables the include dependencies option.
	 * This will make the resulting .jar file include the provided dependencyInfos.
	 * 
	 * @return updated receiver's instance.
	 * @see #build()
	 */
	public JArtifactInfoBuilder withIncludeDependencies() {
		this.includeDependencies = true;
		return this;
	}

	/**
	 * Adds the given <code>classInfos</code> to the builder which should be
	 * packaged into the archive file and returns the updated instance.
	 * 
	 * @param classInfos classes which should be packaged in the archive file
	 * @return updated receiver's instance.
	 * @throws NullPointerException if one of the classInfos is <code>null</code>.
	 * @see #withArchiveFile(File)
	 * @see JClassInfo
	 * @see #build()
	 */
	public JArtifactInfoBuilder withClassInfos(JClassInfo... classInfos) {
		for(JClassInfo cInfo : classInfos) {
			this.classInfos.add(Objects.requireNonNull(cInfo, "classInfo must not be null"));			
		}
		return this;
	}

	/**
	 * Adds the given <code>classInfos</code> by invoking receiver's {@link #withClassInfos(JClassInfo...)}.
	 * 
	 * @param classInfos classes which should be packaged in the archive file
	 * @return updated receiver's instance.
	 * @throws NullPointerException if one of the classInfos is <code>null</code>.
	 * @see #withArchiveFile(File)
	 * @see #build()
	 */
	public JArtifactInfoBuilder withClassInfos(Collection<JClassInfo> classInfos) {
		classInfos.forEach(ci -> withClassInfos(ci));
		return this;
	}

	/**
	 * Adds the given <code>dependencyInfos</code> to your artifact and returns the
	 * updated receiver's instance.
	 * 
	 * @param dependencyInfos which are needed to run the artifact's java application
	 * @return  updated instance of the receiver.
	 * @see JArtifactInfo
	 * @see JCmdExecutor
	 * @see #build()
	 */
	public JArtifactInfoBuilder withDependencyInfos(JDependencyInfo... dependencyInfos) {
		for(JDependencyInfo dInfos : dependencyInfos) {
			this.dependencyInfos.add(Objects.requireNonNull(dInfos, "dependencyInfo must not be null"));			
		}
		return this;
	}

	/**
	 * Adds the given <code>dependencyInfos</code by invoking the receiver's
	 * {@link #withDependencyInfos(JDependencyInfo...)}.
	 * 
	 * @param dependencyInfos which are needed to run the artifact's java
	 *                        application
	 * @return updated instance of the receiver.
	 * @see JArtifactInfo
	 * @see JCmdExecutor
	 * @see #build()
	 */
	public JArtifactInfoBuilder withDependencyInfos(Collection<JDependencyInfo> dependencyInfos) {
		dependencyInfos.forEach(di -> withDependencyInfos(di));
		return this;
	}

	/**
	 * Adds the given <code>resources</code> to the builder instance and returns the
	 * updated instance.
	 * 
	 * @param resources which should be packaged in the archive file.
	 * @return updated receiver's instance.
	 * @throws IllegalArgumentException if one of the given <code>resources</code>
	 *                                  is not an resource.
	 * @see #withArchiveFile(File)
	 * @see JSourceInfos#isResource(JSourceInfo)
	 * @see #build()
	 */
	public JArtifactInfoBuilder withResources(JSourceInfo... resources) {
		for(JSourceInfo rInfo : resources) {
			if (!JSourceInfos.isResource(rInfo)) {
				throw new IllegalArgumentException("Resource '" + rInfo + "' is not a resource");
			}
			this.resources.add(rInfo);
		}
		return this;
	}

	/**
	 * Adds the given <code>resources</code> by invoking receiver's {@link #withResources(JSourceInfo...)}.
	 * 
	 * @param resources which should be packaged in the archive file.
	 * @return updated receiver's instance.
	 * @see #withResources(JSourceInfo...)
	 * @see #build()
	 */
	public JArtifactInfoBuilder withResources(Collection<JSourceInfo> resources) {
		resources.forEach(r -> withResources(r));
		return this;
	}

	/**
	 * Adds the given <code>mainClass</code> which contains the
	 * <code>static void main(String[])</code> method.
	 * <p>
	 * <b>HINT:</b> This value will override the main class value in the manifest.
	 * </p>
	 * 
	 * @param mainClass of the artificat.
	 * @return updated receiver's instance.
	 * @see #withManifest(Manifest)
	 * @see #build()
	 */
	public JArtifactInfoBuilder withMainClass(String mainClass) {
		this.mainClass = mainClass;
		return this;
	}

	/**
	 * Adds manifest which should be added to the archive file.
	 * <p>
	 * If no manifest is defined a default one will be created.
	 * </p>
	 * <p>
	 * The main class value in the manifest will be override by the receiver's
	 * {@link #withMainClass(String)}.
	 * </p>
	 * 
	 * @param manifest we want to use for our achive
	 * @return updated receiver's instance
	 * @see #withArchiveFile(File)
	 * @see #build()
	 */
	public JArtifactInfoBuilder withManifest(Manifest manifest) {
		this.manifest = manifest;
		return this;
	}

	/**
	 * Creates an {@link JArtifactInfo} based on the given
	 * <ul>
	 * <li>{@link #withArchiveFile(File)}</li>
	 * <li>{@link #withClassInfos(JClassInfo...)}</li>
	 * <li>{@link #withDependencyInfos(JDependencyInfo...)}</li>
	 * <li>{@link #withResources(JSourceInfo...)}</li>
	 * <li>{@link #withMainClass(String)}</li>
	 * <li>{@link #withResources(JSourceInfo...)}</li>
	 * </ul>
	 * 
	 * @return new instance of {@link JArtifactInfo}
	 * @throws IOException if the archive could not be build.
	 */
	public JArtifactInfo build() throws IOException {
		// fixme: validate input
		var bytes = makeArchive();
		return new JArtifactInfoImpl(bytes, archiveFile, this.dependencyInfos, getMainClass(), this.includeDependencies);
	}

	private byte[] makeArchive() throws IOException {
		if (archiveFile != null) {
			archiveFile.delete();
			archiveFile.getParentFile().mkdirs();
		}

		prepareManifest();
		
		var stream = new ByteArrayOutputStream();		
		try (JarOutputStream out = new JarOutputStream(stream, manifest)) {

			for (JClassInfo classInfo : classInfos) {
				var jarEntry = JClassInfos.toJarEntry(classInfo);
				try (FileInputStream in = new FileInputStream(classInfo.getLocation())) {
					addJarEntryToJar(out, jarEntry, in, false);
				}
			}
			
			for (var resource : resources) {
				var jarEntry = JSourceInfos.toJarEntry(resource);
				try (FileInputStream in = new FileInputStream(resource.getLocation())) {
					addJarEntryToJar(out, jarEntry, in, false);
				}
			}
			
			if (includeDependencies) {
				var ignoreFiles = Arrays.asList("META-INF/ECLIPSE_.SF");
				
				for (var dependencyInfo : dependencyInfos) {
					for (var entry : JDependencyInfos.toJarEntries(dependencyInfo).entrySet()) {
						// - Ignore errors for duplicate zip entries
						// - Ignore some specific files of dependencies, e.g. ECLIPSE_.SF prevents .jar from starting		
						if (!ignoreFiles.contains(entry.getKey().getName())) {
							addJarEntryToJar(out, entry.getKey(), entry.getValue(), true);
						}
					}
				}
			}
		}
		
		if (archiveFile != null) {
			try(FileOutputStream outputStream = new FileOutputStream(archiveFile)) {
				stream.writeTo(outputStream);
			}
		}
		
		return stream.toByteArray();
	}
	
	private void addJarEntryToJar(JarOutputStream out, JarEntry jarEntry, InputStream in, boolean ignoreErrors) throws IOException {
		try {
			out.putNextEntry(jarEntry);

			byte buffer[] = new byte[10240];
			int nRead;
			while ((nRead = in.read(buffer)) > 0) {
				out.write(buffer, 0, nRead);
			}
			in.close();

			out.closeEntry();
		} catch (ZipException exception) {
			if (!ignoreErrors) {
				throw exception;
			}
		}
	}
	
	private void prepareManifest() {
		if (manifest == null) {
			manifest = new Manifest();
		}
		if (!manifest.getMainAttributes().containsKey(Attributes.Name.MANIFEST_VERSION)) {
			manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		}
		if (mainClass != null) {
			manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, mainClass);
		}
	}

	private String getMainClass() {
		return (String) manifest.getMainAttributes().get(Attributes.Name.MAIN_CLASS);
	}

}
