/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.util.List;

/**
 * The JArtifactInfo holds the information needed to run the java application.
 * <p>We have three categories of information
 * <ul>
 * 	<li>location - {@link #getLocation()}
 *  <li>dependency infos - {@link #getDependencyInfos()}</li>
 *  <li>main class - {@link #getMainClass()}</li>
 * </ul>  
 * </p>
 * 
 * @see JArtifactInfoBuilder
 */
public interface JArtifactInfo {

	/**
	 * Returns the location of the main jar file.
	 * 
	 * @return location of the artifacts jar file
	 */
	File getLocation();

	/**
	 * Returns all dependencies needed to run the java application.
	 * 
	 * @return list receiver's dependencies
	 * @see JDependencyInfo
	 */
	List<JDependencyInfo> getDependencyInfos();

	/**
	 * Returns the main class needed to run the java application provided by the
	 * artifact.
	 * <p>
	 * @Override
	is is the class which contains the <code>main(String[])</code> method
	 * </p>
	 * 
	 * @return the main class of the artifact.
	 */
	String getMainClass();
	
	/**
	 * Returns a boolean indicating whether this artifact has the dependencies included.
	 * 
	 * @return the includes dependencies boolean.
	 */
	boolean getIncludesDependencies();
	
	/**
	 * Returns the bytes of the jar file.
	 * 
	 * @return bytes of the artifacts jar file
	 */
	byte[] getBytes();
}
