/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;

/**
 * JSourceInfo represents all entities which are relevant for the artifact like
 * java sources, images, configuration files, etc..
 */
public interface JSourceInfo {

	/**
	 * Returns the location of the source (java source, image, etc.)
	 * 
	 * @return location of the receiver.
	 */
	File getLocation();

	/**
	 * Returns the full qualified name of the receiver. This named must be unique
	 * with a archiv (jar) file. If it is a java source than the name is the full
	 * qualified class name, otherwise it will be the package name + the file name
	 * where it is located.
	 * 
	 * <p>For instance <code>java.io.File</code> or <code>java.lang.String</code>
	 * </p>
	 * 
	 * @return receiver's full qualified name.
	 */
	String getName();
}
