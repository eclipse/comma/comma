/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.eclipse.comma.java.impl.JClassInfoImpl;

/**
 * JCompiler provides an API to compile a set of java codes with a set of
 * dependencies.
 * <p>
 * The compiler tool is determined by the <code>javax.tools</code> API, that
 * means the application running the JCompiler must be started with an
 * <b>jdk</b>.
 * </p>
 * 
 * @see JSourceInfo
 * @see JDependencyInfo
 * @see #compile()
 */
public class JCompiler {

	private final List<JSourceInfo> sourceInfos;
	private final List<JDependencyInfo> dependencyInfos;
	private final List<String>options;
	private File targetDir;

	
	/**
	 * Used for unit testing purpose only
	 * 
	 * @param sourceInfos used to store the {@link JSourceInfo} instances.
	 * @param dependencyInfos used to store the {@link JDependencyInfo} instances.
	 */
	protected JCompiler(List<JSourceInfo> sourceInfos, List<JDependencyInfo> dependencyInfos, List<String>options) {
		this.sourceInfos = Objects.requireNonNull(sourceInfos, "sourceInfos must not be null");
		this.dependencyInfos = Objects.requireNonNull(dependencyInfos, "dependencyInfos must not be null");	
		this.options = Objects.requireNonNull(options, "options must not be null");	
	}
	
	public JCompiler(int sourceInfosSize, int dependencyInfosSize, int optionsSize) {
		this(new ArrayList<>(sourceInfosSize), new ArrayList<>(dependencyInfosSize), new ArrayList<>(optionsSize));
	}

	public JCompiler() {
		this(10, 5, 5);
	}
	
	/**
	 * Adds the given <code>sourceInfos</code> which should be compiled and returns
	 * the updated receiver instance.
	 * 
	 * @param sourceInfos which should be compiled.
	 * @return updated instance of JCompiler.
	 * @throws IllegalArgumentException will be raised if one of the entries in
	 *                                  <code>sourceInfos</code> is not a java
	 *                                  source
	 * @see JSourceInfos#isJavaSource(File)
	 * @see JCompiler#compile()
	 */
	public JCompiler withSourceInfos(JSourceInfo... sourceInfos) {
		for(JSourceInfo sInfo : sourceInfos)  {
			if(!JSourceInfos.isJavaSource(sInfo)) {
				throw new IllegalArgumentException("adding sourceInfo '"+  sInfo.getLocation() + "' failed - is not a java source");
			}
			this.sourceInfos.add(sInfo);			
		}
		return this;
	}

	/**
	 * Adds the given <code>sourceInfos</code> by invoking receiver's {@link JCompiler#withOptions(String...)}.
	 * 
	 * @param sourceInfos which should be compiled.
	 * @return updated instance of the receiver.
	 * @throws IllegalArgumentException will be raised if one of the entries in
	 *                                  <code>sourceInfos</code> is not a java
	 *                                  source
	 * @see JCompiler#compile()
	 */
	public JCompiler withSourceInfos(Collection<JSourceInfo> sourceInfos) {
		sourceInfos.forEach(s -> withSourceInfos(s));
		return this;
	}

	/**
	 * Adds the given <code>dependencyInfos</code> which should be considered by the
	 * compiler and returns the updated receiver instance.
	 * 
	 * @param dependencyInfos which should be considered by the compiler
	 * @return updated instance of the receiver.
	 * @throws IllegalArgumentException if the given dependency is not a file
	 * @see JCompiler#compile()
	 */
	public JCompiler withDependencyInfos(JDependencyInfo... dependencyInfos) {
		for(JDependencyInfo dInfo : dependencyInfos) {
			Objects.requireNonNull(dInfo, "dependencyInfo must not be null");
			this.dependencyInfos.add(dInfo);			
		}
		return this;
	}
	
	/**
	 * Adds the given <code>depencdyInfos</code> by invoking the receiver's
	 * {@link JCompiler#withDependencyInfos(JDependencyInfo...) and returns the
	 * updated receiver instance.
	 * 
	 * @param dependencyInfos which should be considered by the compiler
	 * @return updated instance of the receiver.
	 * @throws IllegalArgumentException if the given dependency is not a file
	 * @see JCompiler#compile()
	 */
	public JCompiler withDependencyInfos(Collection<JDependencyInfo> dependencyInfos) {
		dependencyInfos.forEach(d -> withDependencyInfos(d));
		return this;
	}

	/**
	 * Adds the given <code>options</code> used by the compiler (e.g.: -g, -nowarn, ...).
	 * 
	 * @param options which should be used by the compiler.
	 * @return updated instance of the receiver.
	 * @throws NullPointerException if one of the options is <code>null</code>.
	 * @see JCompiler#compile()
	 */
	public JCompiler withOptions(String... options) {
		for(String o : options) {
			this.options.add(Objects.requireNonNull(o, "option must not be null"));
		}
		return this;
	}

	/**
	 * Adds the given <code>options</code> by invoking the receiver's {@link JCompiler#withOptions(String...)}.
	 * 
	 * @param options which should be used by the compiler.
	 * @return updated instance of the receiver.
	 * @throws NullPointerException if one of the options is <code>null</code>.
	 * @see JCompiler#compile()
	 */
	public JCompiler withOptions(Collection<String> options) {
		options.forEach(o -> withOptions(o));
		return this;
	}

	/**
	 * Compiles all add sources () by considering all added dependencies and options.
	 * 
	 * @return list of all compiled classes ({@link JClassInfo}).
	 * @throws IOException if the compilation failed.
	 * @see #withSourceInfos(JSourceInfo...)
	 * @see #withDependencyInfos(Collection)
	 * @see #withOptions(String...)
	 */
	public List<JClassInfo> compile() throws IOException {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		if(compiler == null) {
			throw new IOException("ToolProvider.getSystemJavaCompiler() returns null - Could not get java compiler, please make sure your application is running with a jdk and not jre.");
		}
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);

		createTargetDir();
		
		List<File> files = sourceInfos.stream()
									.filter(s -> JSourceInfos.isJavaSource(s))
									.map(s -> s.getLocation())
									.collect(Collectors.toList());
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(files);

		List<String>options = createOptions();
		
		boolean success = compiler.getTask(null, fileManager, null, options, null, compilationUnits).call();
		if (!success) {
			throw new IllegalArgumentException("compile failed");
		}

		List<JClassInfo>classInfos = new ArrayList<>(files.size());
		for(JavaFileObject jfo : fileManager.list(StandardLocation.CLASS_OUTPUT, "", Collections.singleton(JavaFileObject.Kind.CLASS), true)) {
			classInfos.add(new JClassInfoImpl(new File(jfo.getName()), JClassInfos.getName(targetDir, jfo.getName())));
		}
		
		return classInfos;
	}
	
	/**
	 * Create temporary directory with the prefix 'comma-jcompiler-target-'. It will
	 * be used to store the *.class files. If the application is terminated properly
	 * the temporary directory is deleted.
	 * 
	 * @throws IOException if the temporary directory could not be created.
	 */
	private void createTargetDir() throws IOException {
		targetDir = Files.createTempDirectory("comma-jcompiler-target-").toFile();
	}

	/**
	 * Setup the options by added the
	 * <ul> 
	 * 	<li>compiler's target directory '-d',</li>
	 * 	<li>classpath + dependencies</li>
	 *  <li>all other available options</li>
	 * </ul>
	 * and returns the result.
	 *  
	 * @return the list of ready to use options for the compiler
	 */
	private List<String>createOptions() {
		List<String>tOptions = new ArrayList<>(dependencyInfos.size() + options.size() + 4);
		tOptions.add("-d");
		tOptions.add(targetDir.getAbsolutePath());
		if(!dependencyInfos.isEmpty()) {
			tOptions.add("-classpath");		
			tOptions.add(JDependencyInfos.toClasspath(dependencyInfos));
		}
		tOptions.addAll(this.options);
		return tOptions;
	}
}
