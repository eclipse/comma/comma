/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.impl;

import java.io.File;
import java.util.Objects;

import org.eclipse.comma.java.JClassInfo;

public class JClassInfoImpl implements JClassInfo {
	private final File location;
	private final String name;

	public JClassInfoImpl(File location, String name) {
		this.location = Objects.requireNonNull(location, "location must not be null");
		this.name = Objects.requireNonNull(name, "name must not be null");
	}

	public File getLocation() {
		return location;
	}

	public String getName() {
		return name;
	}
	// fixme isEqual hashCode
}
