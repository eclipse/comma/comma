/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.impl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.comma.java.JCmd;

public class JCmdImpl implements JCmd {

	private final List<String> items;

	public JCmdImpl(List<String> items) {
		this.items = Objects.requireNonNull(items, "items must not be null");
	}

	@Override
	public List<String> getItems() {
		return this.items;
	}

	@Override
	public String toString() {
		return items.stream().collect(Collectors.joining(" "));
	}

	@Override
	public String[] toArray() {
		return items.toArray(new String[items.size()]);
	}
}
