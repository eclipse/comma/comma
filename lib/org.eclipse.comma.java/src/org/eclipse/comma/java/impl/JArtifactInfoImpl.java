/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.impl;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.eclipse.comma.java.JArtifactInfo;
import org.eclipse.comma.java.JDependencyInfo;

public class JArtifactInfoImpl implements JArtifactInfo {
	
	private final byte[] bytes;
	private final File location;
	private final List<JDependencyInfo>dependencyInfos;
	private final String mainClass;
	private final boolean includesDependencies;
	
	public JArtifactInfoImpl(byte[] bytes, File location, List<JDependencyInfo> dependencyInfos, String mainClass, boolean includesDependencies) {
		this.bytes = bytes;
		this.location = location;
		this.dependencyInfos = (dependencyInfos != null ? dependencyInfos : Collections.emptyList());
		this.mainClass = Objects.requireNonNull(mainClass, "mainClass must not be null");
		this.includesDependencies = includesDependencies;
	}
	
	@Override
	public boolean getIncludesDependencies() {
		return includesDependencies;
	}

	@Override
	public File getLocation() {
		return location;
	}

	@Override
	public List<JDependencyInfo> getDependencyInfos() {
		return dependencyInfos;
	}

	@Override
	public String getMainClass() {
		return mainClass;
	}
	
	@Override
	public byte[] getBytes() {
		return this.bytes;
	}
}
