/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.impl;

import java.io.File;
import java.util.Objects;

import org.eclipse.comma.java.JDependencyInfo;

public class JDependencyInfoImpl implements JDependencyInfo {

	private final File location;

	public JDependencyInfoImpl(File location) {
		this.location = Objects.requireNonNull(location, "location must not be null");
	}

	@Override
	public File getLocation() {
		return location;
	}
	// fixme isEqual hashCode

}
