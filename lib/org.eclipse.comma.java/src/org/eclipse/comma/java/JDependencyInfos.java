/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.comma.java.impl.JDependencyInfoImpl;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.URIUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleWiring;

/**
 * The JDependencyInfos provide a set of methods to collect and create
 * {@link JDependenyInfo}s.
 * <p>
 * The collecting takes place by searching file system's directories for files
 * with
 * <ul>
 * <li>{@link #getDependenciesFromDirectory(File)}</li>
 * <li>{@link #getDependenciesFromDirectory(String)}</li>
 * </ul>
 * </p>
 * <p>
 * or by searching eclipse bundles which provide files
 * <ul>
 * <li>{@link #getDependencyFromBundle(Bundle, org.eclipse.core.runtime.Path)}</li>
 * <li>{@link #getDependencyFromBundle(String, String)}</li>
 * </ul>
 * </p>
 */
public class JDependencyInfos {
	/**
	 * Returns a set of all dependency files by
	 * invoking the receiver's {@link #getDependenciesFromDirectory(File)} and
	 * returns the result.
	 * 
	 * @param directory we are looking for dependencies
	 * @return set of dependency files
	 * @throws IOException
	 * @see JDependencyFile
	 */
	public static Set<JDependencyInfo> getDependenciesFromDirectory(String directory) throws IOException {
		return getDependenciesFromDirectory(new File(directory));
	}

	/**
	 * Returns a set of all dependency files found
	 * under the given <code>directory</code>.
	 * 
	 * @param directory we are looking for dependencies
	 * @return set of dependency files
	 * @throws IOException
	 * @see JDependencyFile
	 */
	public static Set<JDependencyInfo> getDependenciesFromDirectory(File directory) throws IOException {
		try (Stream<Path> files = Files.walk(Paths.get(directory.getAbsolutePath()))) {
			return files.filter(f -> f.toFile().isFile()).map(p -> new JDependencyInfoImpl(p.toFile()))
					.collect(Collectors.toSet());
		}
	}
	
	/**
	 * Returns the {@link JDepedencyInfo} related to the given
	 * <code>bundleId</code>.
	 * <p>
	 * The bundle/plugin must be available in the Eclipse environment.
	 * </p>
	 * 
	 * @param bundleId of the bundle we are looking for
	 * @return {@link JDepedencyInfo} instance
	 * @throws IOException
	 */
	public static JDependencyInfo getDependencyFromBundleId(String bundleId) throws IOException {
		return create(Platform.getBundle(bundleId));
	}
	
	/**
	 * Returns the {@link JDepedencyInfo} related to the given
	 * <code>bundleId</code> with all its dependencies.
	 * <p>
	 * The bundle/plugin must be available in the Eclipse environment.
	 * </p>
	 * 
	 * @param bundleId of the bundle we are looking for
	 * @return set of {@link JDepedencyInfo}
	 * @throws IOException
	 */
	public static Set<JDependencyInfo> getDependencyFromBundleIdWithTransitiveDependencies(String bundleId) throws IOException {
		var startBundle = Platform.getBundle(bundleId);
		var bundles = new HashSet<Bundle>();
		
		final var reference = new AtomicReference<Consumer<Bundle>>();
		Consumer<Bundle> walkRecursive = (bundle) -> {
			if (!bundles.contains(bundle)) {
				bundles.add(bundle);
				var wiring = bundle.adapt(BundleWiring.class);
				for (var wire : wiring.getRequiredWires(null)) {
					reference.get().accept(wire.getProviderWiring().getBundle());
				}
			}
		};

		reference.set(walkRecursive);
		walkRecursive.accept(startBundle);
		
		var dependencyInfos = new HashSet<JDependencyInfo>();
		for (var bundle : bundles) dependencyInfos.add(create(bundle));
		return dependencyInfos;
	}

	/**
	 * Returns {@link JDependencyInfo} found in the given eclipse plugin
	 * <code>bundle</code> with the file name at <code>path</code> by invoking the
	 * receiver's
	 * {@link #getDependencyFromBundle(Bundle, org.eclipse.core.runtime.Path)}
	 * 
	 * @param bundle we are looking for
	 * @param path   of the file within the bundle
	 * @return the found {@link JDependencyInfo}
	 * @throws IOException
	 */
	public static JDependencyInfo getDependencyFromBundle(String bundle, String path) throws IOException {
		return getDependencyFromBundle(Platform.getBundle(bundle), new org.eclipse.core.runtime.Path(path));
	}

	/**
	 * Returns {@link JDependencyInfo} found in under the given eclipse plugin
	 * <code>bundle</code> with the file name at <code>path</code>.
	 * <p>
	 * This function hides the tasks of extracting the file at <code>path</code>
	 * from the given <code>bundle</code> to a temporary file in the underlying file
	 * system. Because of this you can treat {@link JDependencyInfo}'s location like
	 * any other file in the file sytem.
	 * </p>
	 * 
	 * @param bundle we are looking for
	 * @param path   of the file within the bundle
	 * @return the found {@link JDependencyInfo}
	 * @throws IOException
	 */
	public static JDependencyInfo getDependencyFromBundle(Bundle bundle, org.eclipse.core.runtime.Path path)
			throws IOException {
		URL jarUrl = FileLocator.find(bundle, path);
		if (jarUrl == null) {
			throw new IOException("No dependency found in bundle '" + bundle + "' at '" + path + "'");
		}
		URL jarFileUrl = FileLocator.toFileURL(jarUrl);
		try {
			// HINT: this copies the plugin jarFile to a file system based file.
			return create(URIUtil.toFile(URIUtil.toURI(jarFileUrl)));
		} catch (URISyntaxException e) {
			throw new IOException(e);
		}
	}

	/**
	 * Creates a new instance of {@link JDependencyInfo} based on the given
	 * <code>jarFile</code>.
	 * 
	 * @param jarFile which is used as dependency.
	 * @return instance of {@link JDependencyInfo}
	 */
	public static JDependencyInfo create(File jarFile) {
		return new JDependencyInfoImpl(jarFile);
	}
	
	/**
	 * Creates a new instance of {@link JDependencyInfo} based on the given
	 * <code>bundle</code>.
	 * 
	 * @param Bundle which is used as dependency.
	 * @return instance of {@link JDependencyInfo}
	 * @throws IOException 
	 */
	public static JDependencyInfo create(Bundle bundle) throws IOException {
		File bundleFile = FileLocator.getBundleFile(bundle);
		if (isEclipseProject(bundleFile)) {
			return create(new File(bundleFile, "target/classes"));
		}
		return create(bundleFile);
	}

	/**
	 * Returns a list of classpath entries used by {@link JCompiler}.
	 * 
	 * @param dependencyInfos collection of dependency infos.
	 * @return list of all classpath entries.
	 * @see JCompiler
	 */
	public static List<String> toClasspathList(Collection<JDependencyInfo> dependencyInfos) {
		return dependencyInfos.stream().map(d -> d.getLocation().getAbsolutePath()).collect(Collectors.toList());
	}

	/**
	 * Returns well format class path entry based on the given
	 * <code>depencyInfos</code> collection.
	 * 
	 * @param dependencyInfos we want to create the class path entry.
	 * @return well format class path entry
	 */
	public static String toClasspath(Collection<JDependencyInfo> dependencyInfos) {
		return JDependencyInfos.toClasspathList(dependencyInfos).stream()
				.collect(Collectors.joining(getClasspathSeparator()));

	}

	/**
	 * Returns the proper class path separator (':' or ';')
	 * 
	 * @return proper class path separator
	 */
	static CharSequence getClasspathSeparator() {
		String val = System.getProperty("path.separator");
		if (val == null) {
			throw new IllegalArgumentException("system property 'path.seaprator' must not be null");
		}
		return val;
	}

	/**
	 * Returns <code>true</code> if the given <code>bundelFile</code> meets the following criterias:
	 * <ul>
	 * <li>is a directory</li>
	 * <li>contains a sub directory 'bin'</li>
	 * <li>contains the file '.project'</li>
	 * </ul>
	 * ohterwise returns <code>false</code>.
	 * 
	 * @param bundleFile we want to check.
	 * @return <code>true if the bundleFile is an eclipse project</code>.
	 */
	static boolean isEclipseProject(File bundleFile) {
		if (!bundleFile.isDirectory()) {
			return false;
		}

		if (!new File(bundleFile, "target/classes").exists()) {
			return false;
		}

		File pFile = new File(bundleFile, ".project");
		return pFile.exists() && pFile.isFile();
	}
	
	/**
	 * Returns jar entries based on the given {@link JDependencyInfo dependencyInfo} argument.
	 * If the given {@link JDependencyInfo dependencyInfo} argument is a .jar all entries of that .jar
	 * will be returned.
	 * 
	 * @param dependencyInfo the dependency the jar entry is based on
	 * @return map with jar entries and their input streams
	 * @throws IOException 
	 * @see JDependencyInfo
	 */
	public static Map<JarEntry, InputStream> toJarEntries(JDependencyInfo dependencyInfo) throws IOException {
		var entries = new HashMap<JarEntry, InputStream>();
		var classesPattern = Pattern.compile("\\/target(\\/xtend)?\\/classes(\\d+)?\\/(.*)");
		var classesMatcher = classesPattern.matcher(dependencyInfo.getLocation().getAbsolutePath().replace('\\', '/'));
		if (dependencyInfo.getLocation().isDirectory()) {
			for (var dependency : getDependenciesFromDirectory(dependencyInfo.getLocation())) {
				entries.putAll(toJarEntries(dependency));
			}
		} else if (dependencyInfo.getLocation().getName().toLowerCase().endsWith(".jar")) {
			var jar = new JarFile(dependencyInfo.getLocation());
			entries.putAll(entriesFromJarRecursive(jar));
		} else if (classesMatcher.find()) {
			var fis = new FileInputStream(dependencyInfo.getLocation());
			entries.put(new JarEntry(classesMatcher.group(3)), fis);
		}

		return entries;
	}
	
	private static HashMap<JarEntry, InputStream> entriesFromJarRecursive(JarFile jar) throws IOException {
		var entries = new HashMap<JarEntry, InputStream>();
		var enumerator = jar.entries();
		while (enumerator.hasMoreElements()) {
			var entry = enumerator.nextElement();
			if (entry.getName().toLowerCase().endsWith(".jar")) {
				var file = File.createTempFile(entry.getName(), "");
				Files.copy(jar.getInputStream(entry), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
				entries.putAll(entriesFromJarRecursive(new JarFile(file)));
			} else {
				entries.put(entry, jar.getInputStream(entry));
			}
		}
		return entries;
	}
}
