/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.util.List;

/**
 * The JCmd represent a java command like you normally invoke on a command line
 * to execute a java application.
 * <p>We assuem a java command consists of the following items:
 * <ol>
 * 	<li>the java executable path (e.g. /opt/java/openjdk-11/bin/java)</li>
 *  <li>the -classpath followed by classpath value (e.g. -classpath my.jar:lib1.jar:lib2.jar)
 *  <li>an arbitrary number of options (e.g. -Dmy.key1=1234 or -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044)</li>
 *  <li>the main class which contains the <code>void main(String[])</code> function (e.g. my.App)
 *  <li>an arbitrary number of arguments which should be passed to the application
 * </ol>
 * </p>
 * <p>For instance the complete cmd above look like
 * <code>/opt/java/openjdk-11/bin/java -classpath my.jar:lib1.jar:lib2.jar -Dmy.key1=1234 -Dmy.key2=4321 my.App arg1 arg2 
 * </code>
 * </p>
 * 
 * @see JCmdBuilder
 * @see JCmdExecutor
 */
public interface JCmd {

	/**
	 * Return the items of the command in the property order.
	 *  
	 * @return cmd's items.
	 */
	List<String> getItems();

	/**
	 * Returns a ready to use string representation of the cmd.
	 * 
	 * @return java command.
	 */
	String toString();

	/**
	 * Returns string array of the cmd's items in a proper order.
	 * 
	 * @return items as array.
	 */
	String[] toArray();
}
