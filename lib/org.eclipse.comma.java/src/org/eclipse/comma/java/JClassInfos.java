/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java;

import java.io.File;
import java.util.jar.JarEntry;

/**
 * JClassInfos provides a set of util functions for JClassInfo related
 * operations.
 */
public class JClassInfos {

	/**
	 * Returns the full qualified file name of {@link JClassInfo} based on the
	 * {@link JClassInfo}'s {@link JClassInfo#getName() name}. 
	 * <p>For instance if name is java.io.File the result
	 * will be java/io/File.class
	 * </p>
	 * 
	 * @param classInfo the class we want the jar file name
	 * @return file name of the given class within an jar.
	 * @see JClassInfo
	 */
	public static String getJarFileName(JClassInfo classInfo) {
		return classInfo.getName().replaceAll("\\.", "/") + ".class";
	}

	/**
	 * Returns jar entry based on the given  {@link JClassInfo classInfo} argument.
	 * 
	 * @param classInfo the class the jar entry is based on
	 * @return jar entry of the given class info.
	 * @see JClassInfo
	 */
	public static JarEntry toJarEntry(JClassInfo classInfo) {
		JarEntry jarEntry =new JarEntry(getJarFileName(classInfo));
		jarEntry.setTime(classInfo.getLocation().lastModified());
		// fixme: further entries
		return jarEntry;
	}

	public static String getName(File directory, String classFile) {
		String prefix = directory.getAbsolutePath();
		String className = classFile;
		if(className.startsWith(prefix)) {
			className = className.substring(prefix.length());
		}
		
		className = className.replace('/', '.').replace('\\', '.').replaceFirst(".class$", "");
		if(className.indexOf('.') == 0) {
			className = className.substring(1);
		}
		
		return className;
	}

}
