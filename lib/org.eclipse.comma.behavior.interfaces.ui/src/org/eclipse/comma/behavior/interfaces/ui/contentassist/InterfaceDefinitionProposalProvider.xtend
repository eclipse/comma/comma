/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.ui.contentassist

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.core.runtime.Platform

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class InterfaceDefinitionProposalProvider extends AbstractInterfaceDefinitionProposalProvider {
	
	new() {
		templateIcon = ImageDescriptor.createFromURL(Platform.getBundle("org.eclipse.comma.icons").getResource("icons/template.png")).
			createImage();
	} 

	
	override complete_Interface(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.complete_Interface(model, ruleCall, context, acceptor)
			
			val signature = StateMachineUtilities.getSignatureForMachine(model, scopeProvider)
			val name = if(signature === null) "Example" else signature.name 
			val prop = '''
			interface «name» version "0.1"
			
			machine StateMachine {
				
				initial state initialState {
					
				}
			}
		'''
		acceptor.accept(createTemplate("Interface Definition " + name, prop, "Interface Definition and machine", 0, context, 10, name.length))
	}

}
