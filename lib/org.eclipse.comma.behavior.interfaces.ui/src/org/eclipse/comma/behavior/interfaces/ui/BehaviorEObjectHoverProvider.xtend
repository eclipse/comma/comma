/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.ui

import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider
import org.eclipse.comma.expressions.expression.Variable

class BehaviorEObjectHoverProvider extends DefaultEObjectHoverProvider{
	
    protected override String getFirstLine(EObject o) {
    	if (o instanceof Variable){
    		val parent = o.eContainer
    		if(parent instanceof TriggeredTransition){
    			val event = parent.trigger
    			if(event instanceof Command){
    				val index = parent.parameters.indexOf(o)
    				if(index < event.parameters.size){
    					val param = event.parameters.get(index)
    					return "<b>" + param.direction.getName.toLowerCase + "</b>" + " Variable " + "<b>" + o.name + "</b>"
    				}
    			}
    		}
    	}
    	return super.getFirstLine(o);
    }
	
}