/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.ui.quickfix

import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue
import org.eclipse.comma.behavior.interfaces.validation.InterfaceDefinitionValidator

class CommandsQuickfix {

	@Fix(InterfaceDefinitionValidator.REPLY_NOT_POSSIBLE)
	def removeCommandReplyParam(Issue issue, IssueResolutionAcceptor acceptor) {
		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				if (element instanceof CommandReply) {
					val node = NodeModelUtils.findActualNodeFor(element)
					context.xtextDocument.replace(node.offset, node.length, "reply")
				}
			}
		}
		acceptor.accept(issue, 'Remove Type', 'Remove Type.', 'upcase.png', modification)
	}

	@Fix(InterfaceDefinitionValidator.REPLY_NOT_POSSIBLE)
	def removeReply(Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, 'Remove reply', 'Remove reply.', 'upcase.png') [ context |
			context.xtextDocument.replace(issue.offset, issue.length, "")
		]
	}
	
	@Fix(InterfaceDefinitionValidator.REPLY_WRONG_NUMBER_PARAMS)
	def removeCommandReplyParams(Issue issue, IssueResolutionAcceptor acceptor) {
		
		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				if (element instanceof CommandReply) {
					val params = element.parameters;
					val first = NodeModelUtils.findActualNodeFor(params.get(0))
					val last = NodeModelUtils.findActualNodeFor(params.get(params.length - 1))					
					val startOffset = first.offset + first.length
					val endOffset = last.offset + last.length
					context.xtextDocument.replace(startOffset, endOffset - startOffset, "")
				}
			}
		}		
		acceptor.accept(issue, 'Remove Type', 'Remove Type.', 'upcase.png', modification)
	}
	
	def removeAction(Issue issue, IssueResolutionAcceptor acceptor) {
		
	}

}
