/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.interfaces.ui.quickfix

import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.validation.InterfaceDefinitionValidator
import org.eclipse.comma.behavior.ui.quickfix.StateMachineQuickfixProvider
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.comma.signature.interfaceSignature.Notification
import org.eclipse.comma.signature.interfaceSignature.Parameter
import org.eclipse.comma.types.types.TypeReference
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.ui.editor.quickfix.Fix
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor
import org.eclipse.xtext.validation.Issue
import org.eclipse.xtext.scoping.IScopeProvider
import com.google.inject.Inject

/**
 * Custom quickfixes.
 * 
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#quick-fixes
 */
class InterfaceDefinitionQuickfixProvider extends StateMachineQuickfixProvider {
	@Inject IScopeProvider scopeProvider
	
	@Fix(InterfaceDefinitionValidator.INTERFACE_UNUSED_SIGNAL)
	def addSignalWithEvent(Issue issue, IssueResolutionAcceptor acceptor) {
		val triggerName = issue.data.get(0)
		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				val sm = EcoreUtil2.getContainerOfType(element, Interface).machines.get(0)
				val signature = StateMachineUtilities.getSignatureForMachine(sm, scopeProvider)

				var notification = signature.notifications.findFirst[it.parameters.size == 0]
				if (notification === null) {
					notification = signature.notifications.get(0)
				}
				val reply = if(notification === null) "Notification" else (notification.name + getParams(notification))

				val offset = NodeModelUtils.findActualNodeFor(sm).endOffset - 1
				val fix = '''
					
						state eventState«sm.states.length + 1» {
							transition trigger: «triggerName»
								do: «reply»
								next state: eventState«sm.states.length + 1»
						}
				'''
				context.xtextDocument.replace(offset, 0, fix)
			}

		}
		acceptor.accept(issue, 'Add state and transition with trigger ' + triggerName, 'Add state with trigger',
			'upcase.png', modification)
	}

	@Fix(InterfaceDefinitionValidator.INTERFACE_UNUSED_NOTIFICATION)
	def addNotificationWithEvent(Issue issue, IssueResolutionAcceptor acceptor) {
		val triggerName = issue.data.get(0)
		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				val sm = EcoreUtil2.getContainerOfType(element, Interface).machines.get(0)
				val signature = StateMachineUtilities.getSignatureForMachine(sm, scopeProvider)
				val notification = signature.notifications.findFirst[it.name.equals(triggerName)]
				
				var signal = signature.signals.findFirst[it.parameters.size == 0]
				if (signal === null) {
					signal = signature.signals.get(0)
				}
				val signalName = if(signal === null) "Signal" else signal.name

				val offset = NodeModelUtils.findActualNodeFor(sm).endOffset - 1
				val fix = '''
					
						state eventState«sm.states.length + 1» {
							transition trigger: «signalName»
								do: «notification.name + getParams(notification)»
								next state: eventState«sm.states.length + 1»
						}
				'''
				context.xtextDocument.replace(offset, 0, fix)
			}
		}
		acceptor.accept(issue, 'Add state and transition with notification ' + triggerName,
			'Add state with notification', 'upcase.png', modification)
	}
	
	def getParams(Notification notification) {
		var params = ""
		if (!notification.parameters.empty) {
			params += "("
			for (val it = notification.parameters.iterator; it.hasNext();) {
				params += paramToString(it.next)
				if (it.hasNext) {
					params += ", "
				}
			}
			params += ")"
		}
		params

	}

	def paramToString(Parameter parameter) {
		parameter.type.type.name		
	}

	@Fix(InterfaceDefinitionValidator.INTERFACE_UNUSED_COMMAND)
	def addCommandWithEvent(Issue issue, IssueResolutionAcceptor acceptor) {
		val triggerName = issue.data.get(0)
		val modification = new ISemanticModification() {
			override apply(EObject element, IModificationContext context) throws Exception {
				val sm = EcoreUtil2.getContainerOfType(element, Interface).machines.get(0)
				val signature = StateMachineUtilities.getSignatureForMachine(sm, scopeProvider)
				val command = signature.commands.findFirst[it.name.equals(triggerName)]

				val reply = if (command.type instanceof TypeReference &&
						(command.type as TypeReference).type.name.equals("void"))
						"reply"
					else
						"reply(*)"

				val offset = NodeModelUtils.findActualNodeFor(sm).endOffset - 1
				val fix = '''
					
						state eventState«sm.states.length + 1» {
							transition trigger: «triggerName»
								do: «reply»
								next state: eventState«sm.states.length + 1»
						}
				'''
				context.xtextDocument.replace(offset, 0, fix)
			}
		}

		acceptor.accept(issue, 'Add state and transition with trigger ' + triggerName, 'Add state with trigger',
			'upcase.png', modification)
	}
}
