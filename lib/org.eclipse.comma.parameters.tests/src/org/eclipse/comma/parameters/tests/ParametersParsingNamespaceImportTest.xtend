/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/*
 * generated by Xtext 2.12.0
 */
package org.eclipse.comma.parameters.tests

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.expressions.expression.ExpressionEnumLiteral
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.util.StringInputStream
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.comma.parameters.parameters.ParametersPackage

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class ParametersParsingNamespaceImportTest {
	@Inject extension Provider<ResourceSet> resourceSet
	@Inject ParseHelper<Parameters> parseHelper
	@Inject ValidationTestHelper validationTestHelper
	ResourceSet set;

	def create(String content) {
		val uri = URI.createURI("IUser.params")
		set.createResource(uri).load(new StringInputStream(content), emptyMap)
        return set.getResource(uri, false)
	}
	
	@BeforeEach
    def void setup() {
        set = resourceSet.get()
        
        set.createResource(URI.createURI("IUser.types")).load(new StringInputStream('''
            namespace user.typ
            
            enum ProductName {
            	WATER
            	COLA
            	JUICE
            }
            
            enum CoinResult {
                NOT_ACCEPTED
                NOT_OPERATIONAL
            }
        '''), emptyMap)

        set.createResource(URI.createURI("IUser.signature")).load(new StringInputStream('''
            namespace user
            
            import user.typ.*

            signature IUser
            
            commands
            CoinResult   InsertCoin(out int credit)
            int          ReturnMoney
            OrderResult  OrderProduct(ProductName prodName)
        '''), emptyMap)
        
        set.createResource(URI.createURI("IUser.interface")).load(new StringInputStream('''
            namespace user
            
            import user.typ.*
            
            interface IUser version "0.1"
            
            	variables
            	int credit
            	pricesMap prices
            
            	init
            	credit := 0
            	prices := <pricesMap>{ProductName::COLA->3, ProductName::JUICE->2, ProductName::WATER->1}
            
            machine userMachine {
            
            	initial state AcceptUserCommands {
            
            		transition trigger: InsertCoin(int val)
            				do: credit := credit + 1
            				reply(credit,CoinResult::ACCEPTED)
            				next state: AcceptUserCommands
            			OR
            				do:
            				reply(credit,CoinResult::NOT_ACCEPTED)
            				next state: AcceptUserCommands
            			OR
            				do:
            				reply(*,CoinResult::NOT_OPERATIONAL) // unspecified credit result
            				next state: AcceptUserCommands
            
            		transition trigger: ReturnMoney
            			guard: credit > 0
            				do:
            				reply(credit)
            				credit := 0
            				next state: AcceptUserCommands
            
            		transition trigger: OrderProduct(ProductName prod)
            			guard: credit > 0
            				do:
            				reply(OrderResult::NOT_OPERATIONAL)
            				next state: AcceptUserCommands
            
            		transition trigger: OrderProduct(ProductName prod)
            			guard: credit > 0 AND credit < prices[prod]
            				do:
            				reply(OrderResult::NOT_ENOUGH_MONEY)
            				next state: AcceptUserCommands
            
            		transition trigger: OrderProduct(ProductName prod)
            			guard: credit >= prices[prod] // assuming products are not for free ...
            				do: credit := credit - prices[prod]
            				reply(OrderResult::DELIVERED)
            				next state: AcceptUserCommands
            			OR
            				do:
            				reply(OrderResult::NO_PRODUCTS_AVAILABLE)
            				next state: AcceptUserCommands
            
            	}
            }
        '''), emptyMap)
	}
	
  	@Test
	def void testValid() {
		val params = create('''
			import user.typ.*
			import user.*
			
			interface: IUser

			trigger: OrderProduct   
			state: AcceptUserCommands params: (ProductName::WATER) 
			 
			trigger: InsertCoin   
			state: AcceptUserCommands params: ()
			 
			trigger: ReturnMoney    
			state: AcceptUserCommands break
			
			reply to: InsertCoin
			state: AcceptUserCommands params: (1,CoinResult::NOT_ACCEPTED)
        '''.toString.replace("\r\n", "\n")).contents.head as Parameters
        
        Assertions.assertNotNull(params)
		validationTestHelper.assertNoIssues(params)
		val triggerParams = params.triggerParams.get(0)
		Assertions.assertEquals("OrderProduct", triggerParams.event.name);
		val stateParams = triggerParams.stateParams.get(0)
		Assertions.assertEquals("AcceptUserCommands", stateParams.state.name);
		Assertions.assertEquals(1, stateParams.params.length);
		val eventParam = stateParams.params.get(0).value
		Assertions.assertEquals(1, eventParam.length);
		val enumValue = eventParam.get(0) as ExpressionEnumLiteral
		Assertions.assertEquals("WATER", enumValue.literal.name);
	}
	
	@Test
	def void testInvalidTriggerName() {
		val params = parseHelper.parse('''
			import user.typ.*
			import user.*
			
			interface: IUser

			trigger: OrderProductWrongName   
			state: AcceptUserCommands params: (ProductName::WATER) 
			 
			trigger: InsertCoin   
			state: AcceptUserCommands params: ()
			 
			trigger: ReturnMoney    
			state: AcceptUserCommands break
			
			reply to: InsertCoin
			state: AcceptUserCommands params: (1,CoinResult::NOT_OPERATIONAL)
        '''.toString.replace("\r\n", "\n"), set)
        
        validationTestHelper.assertError(params, ParametersPackage.Literals.TRIGGER_PARAMS, "org.eclipse.xtext.diagnostics.Diagnostic.Linking", 60, 21, 
        	'''Couldn't resolve reference to InterfaceEvent 'OrderProductWrongName'.''')
	}
}
