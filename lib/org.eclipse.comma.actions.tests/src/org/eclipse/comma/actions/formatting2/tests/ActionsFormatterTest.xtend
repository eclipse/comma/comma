/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.actions.formatting2.tests

import com.google.inject.Inject
import org.eclipse.comma.actions.tests.ActionsInjectorProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(ActionsInjectorProvider)
class ActionsFormatterTest {

	@Inject extension FormatterTestHelper

	@Test
	def void testFormatCommandReplyWithReplySurroundedBy2Blanks() {
		// HINT: Surrounded blanks are not truncated.
		assertFormatted[
			expectation = '''  reply  '''
			toBeFormatted = '''  reply  '''
		]
	}

	@Test
	def void testFormatCommandReplyWithToSurroundedBy2Blanks() {
		assertFormatted[
			expectation = '''reply to command IMyInterface1'''
			toBeFormatted ='''reply  to  command IMyInterface1'''
		]
	}

	@Test
	def void testFormatCommandEventWithCommandSurroundedBy2Blanks() {
		// HINT: The trailing blanks are not truncated.
		assertFormatted[
			expectation = '''reply to command IMyInterface1  '''
			toBeFormatted = '''reply to  command  IMyInterface1  '''
		]

	}

	@Test
	def void testFormatIfActionWithElseAnd2BlanksSeparatorInOneLine() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					reply(1)
				else
					reply(2)
				fi'''
			toBeFormatted = '''if(1>0)  then  reply(1)  else  reply(2)  fi'''
		]
	}

	@Test
	def void testFormatIfActionWith2BlanksSeparatorAndInOneLine() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					reply(1)
				fi'''
			toBeFormatted ='''if(1>0)  then  reply(1)fi'''
		]
	}

	@Test
	def void testFormatIfActionWithElseAndKeywordsAndActionListInSeparatedLine() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					reply(1)
				else
					reply(2)
				fi'''
			toBeFormatted ='''if(1>0)
				then
				reply(1)
				else
				reply(2)
				fi'''
		]
	}

	@Test
	def void testFormatIfActionWithKeywordsAndActionListInSeparatedLine() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					reply(1)
				fi'''
			toBeFormatted ='''
			if(1>0)
				then
				reply(1)
				fi'''
		]

	}

	@Test
	def void testFormatIfActionWithElseAnd2ActionsInOneRowInIfPart() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					v := 1
					reply(1)
				else
					reply(2)
				fi'''
			toBeFormatted ='''
				if(1>0)  then
					v := 1 reply(1)
				else
				reply(2)
				fi'''
		]
	}


	@Test
	def void testFormatIfActionWithElseAnd2ActionsInOneRowInElsePart() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					reply(1)
				else
					v := 1
					reply(2)
				fi'''
			toBeFormatted ='''
				if(1>0)  then
					reply(1)
				else
					v:=1 reply(2)
				fi'''
		]
	}

	@Test
	def void testFormatAssignmentActionWith1BlanksSurrounded() {
		assertFormatted[
			expectation = '''
				v := 3
				'''
			toBeFormatted ='''
				v := 3
				'''
		]
	}

	@Test
	def void testFormatAssignmentActionWith2BlanksSurrounded() {
		assertFormatted[
			expectation = '''
				v := 3
				'''
			toBeFormatted ='''
				v  :=  3
				'''
		]
	}

	@Test
	def void testFormatAssignmentActionWithNonBlanksSurrounded() {
		assertFormatted[
			expectation = '''
				v := 3
				'''
			toBeFormatted ='''
				v:=3
				'''
		]
	}

	@Test
	def void testFormatRecordFieldAssignmentActionWith2BlanksSurrounded() {
		assertFormatted[
			expectation = '''
				r.v := 3
				'''
			toBeFormatted ='''
				r.v  :=  3
				'''
		]
	}

	@Test
	def void testFormatRecordFieldAssignmentActionWithNonBlanksSurrounded() {
		assertFormatted[
			expectation = '''
				r.v := 3
				'''
			toBeFormatted = '''
				r.v:=3
				'''
		]
	}

	@Test
	def void testFormatIfActionListWithOneEntry() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					v := 3
				fi
				'''
			toBeFormatted ='''
				if (1>0) then v:=3 fi
				'''
		]

	}

	@Test
	def void testFormatIfActionListWith2Entries() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					v := 3
					reply(1)
				fi
				'''
			toBeFormatted ='''
				if (1>0) then v:=3 reply(1) fi
				'''
		]

	}

	@Test
	def void testFormatIfActionListWithEntryBlankEntry() {
		assertFormatted[
			expectation ='''
				if (1 > 0) then
					v := 3
					reply(1)
				fi
				'''
			toBeFormatted = '''
				if (1>0) then
					v:=3

					reply(1)
				fi
				'''
		]
	}

	@Test
	def void testFormatElseListWithOneEntry() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					v := 3
				else
					v := 4
				fi
				'''
			toBeFormatted ='''
				if (1>0) then v:=3  else v:=4 fi
				'''
		]

	}


	@Test
	def void testFormatElseListWith2Entry() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					v := 3
				else
					v := 4
					reply(1)
				fi
				'''
			toBeFormatted ='''
				if (1>0) then v:=3 else  v:=4 reply(1) fi
				'''
		]

	}

	@Test
	def void testFormatElseListWithEntryBlankEntry() {
		assertFormatted[
			expectation = '''
				if (1 > 0) then
					v := 3
				else
					v := 4
					reply(1)
				fi
				'''
			toBeFormatted ='''
				if (1>0) then v:=3
				else
					v:=4

					reply(1)
				fi
				'''
		]
	}
}
