/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.validation

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import com.google.inject.Inject
import java.util.HashSet
import java.util.LinkedHashSet
import java.util.Set
import org.eclipse.comma.types.scoping.TypesImportUriGlobalScopeProvider
import org.eclipse.comma.types.types.EnumTypeDecl
import org.eclipse.comma.types.types.FileImport
import org.eclipse.comma.types.types.MapTypeConstructor
import org.eclipse.comma.types.types.ModelContainer
import org.eclipse.comma.types.types.NamedElement
import org.eclipse.comma.types.types.NamespaceImport
import org.eclipse.comma.types.types.RecordField
import org.eclipse.comma.types.types.RecordTypeDecl
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.Type
import org.eclipse.comma.types.types.TypesModel
import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.comma.types.utilities.CommaUtilities
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EValidator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static extension org.eclipse.comma.types.utilities.TypeUtilities.*

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class TypesValidator extends AbstractTypesValidator {
	
	//Generic method to detect name duplications in a collection of named elements
	protected def checkForNameDuplications(Iterable<? extends NamedElement> elements, String desc, String code, String... issueData) {
		val multiMap = HashMultimap.create()
		
		for (e : elements)
			multiMap.put(e.name, e)
  		for (entry : multiMap.asMap.entrySet) {
  			val duplicates = entry.value    
  			if (duplicates.size > 1) {
  				for (d : duplicates)
  					error("Duplicate " + desc + " name", d, TypesPackage.Literals.NAMED_ELEMENT__NAME, code, issueData)
  			}
  		}
  	}				
  	
  	//Used because the resource with the predefined types is not loaded by the resource description provider
  	//In unit test they are properly loaded
  	protected def placePredefinedTypes(Multimap<QualifiedName, IEObjectDescription> map){
  	    val intType = map.get(QualifiedName.EMPTY.append("int"))
  	    if(intType.exists[EObjectURI.toString.contains(#["org", "eclipse", "comma", "types", "types.types"].join("/"))]){
  	        return
  	    }
  		map.put(QualifiedName.EMPTY.append("int"), null)
  		map.put(QualifiedName.EMPTY.append("bool"), null)
  		map.put(QualifiedName.EMPTY.append("real"), null)
  		map.put(QualifiedName.EMPTY.append("string"), null)
  	    map.put(QualifiedName.EMPTY.append("void"), null)
  	    map.put(QualifiedName.EMPTY.append("any"), null)
  	    map.put(QualifiedName.EMPTY.append("bulkdata"), null)
  	    map.put(QualifiedName.EMPTY.append("id"), null)
  	}
  	
  	/*
  	 * Constraints:
  	 * - namespace and file import mechanisms are not mixed
  	 */
  	@Check
    def checkFileImportTransitively(ModelContainer root) {
        if(root.imports.exists[it instanceof FileImport]) {
            val knownURIs = new LinkedHashSet<URI>(5)
            knownURIs.add(root.eResource.URI)
            if(! usesOnlyFileImports(root, knownURIs)) {
               error("Combining namespace and file imports locally and transitively is not allowed. Check all import statements.", TypesPackage.Literals.MODEL_CONTAINER__IMPORTS)
            }
        }
    }
  	
  	/*
  	 * Constraints:
  	 * - record fields have unique names
  	 */				
  	@Check
	def checkDuplicatedRecordFields(RecordTypeDecl type) {
		checkForNameDuplications(type.fields, "record field", null)
	}
	
	/*
	 * Constraints:
	 * - record extension hierarchy has no cycles
	 */
	@Check
	def checkCircularHierarchy(RecordTypeDecl type){
		if(type.getAllParents.contains(type)){
			error("Cycle in the extension hierarchy", TypesPackage.Literals.RECORD_TYPE_DECL__PARENT)
		}
	}
	
	/*
	 * Constraints:
	 * - no overriding of inherited record fields
	 */
	@Check
	def checkOverridenRecordFields(RecordTypeDecl type){
		if(type.parent !== null){
			val inheritedFields = type.parent.getAllFields
			for(f : type.fields){
				if(!inheritedFields.filter(ff | ff.name.equals(f.name)).empty){
					error("Local field overrides inherited field", f, TypesPackage.Literals.NAMED_ELEMENT__NAME)
				}
			}
		}
	}

	/*
	 * Constraints:
	 * - A record type cannot be used as a type of its fields : the types of fields cannot form a cyclic graph
	 */
	@Check
	def CheckIfRecordFieldsFormCyclicGraph(RecordTypeDecl type) {
		var visited = new  HashSet<RecordTypeDecl>()
		if(containsCycleGraph(type,visited)){
			error("found cyclic graph   ",TypesPackage.Literals.RECORD_TYPE_DECL__FIELDS)
		}
	}
	
	def boolean containsCycleGraph(RecordTypeDecl record, Set<RecordTypeDecl> visited){
		if(visited.contains(record)) 
			return true
		visited.add(record)
		var childRecords = record.fields.filter[f | f.type.recordType].map[recordType].toSet
		for(childRecord : childRecords) {
		    val visitedCopy = visited.clone.toSet
			if(containsCycleGraph(childRecord, visitedCopy)){
				return true
			}
		}
		return false
	}
	
	def getRecordType(RecordField recordField) {
		recordField.type.typeObject as RecordTypeDecl
	}
	
	/*
	 * Constraints:
	 * - enum literals are unique
	 */
	@Check
	def checkDuplicatedEnumElements(EnumTypeDecl type) {
		checkForNameDuplications(type.literals, "enumeration literal", null)
	}
	
	/*
	 * Constraints:
	 * - enum literal values (when given) are increasing
	 */
	@Check
	def checkEnumLiteralValues(EnumTypeDecl type){
		var currentValue = -1
		for(l : type.literals){
			if(l.value === null){
				currentValue++
			}
			else{
				if(l.value.value <= currentValue){
					error("Enum value has to be greater than the previous value", l, TypesPackage.Literals.NAMED_ELEMENT__NAME);
					return
				}
				else {
					currentValue = l.value.value
				}
			}
		}
	}
	
	/*
	 * Constraints:
	 * - type definitions in a given types model have unique names 
	 */
	@Check
	def checkDuplicatedTypeDeclarations(TypesModel typedecl) {
		checkForNameDuplications(typedecl.types, "type declaration", null)
	}
	
	/*
	 * Constrains:
	 * - imports are valid URIs
	 * - imported resources are type models
	 */
	@Check
	def checkImportForValidity(FileImport imp){
		
		if(! EcoreUtil2.isValidUri(imp, URI.createURI(imp.importURI)))
			error("Invalid resource", imp, TypesPackage.Literals.FILE_IMPORT__IMPORT_URI)
		else{
			val Resource r = EcoreUtil2.getResource(imp.eResource, imp.importURI)
			if(! (r.allContents.head.eClass == TypesPackage.eINSTANCE.typesModel))
				error("Only imports for type definitions are allowed", imp, TypesPackage.Literals.FILE_IMPORT__IMPORT_URI)
		}
	}
	
	/*
	 * Constraints:
	 * - any, void and id cannot be directly used in type models
	 */
	 //TODO consider removing any type from the definitions types.types
	@Check
	def checkForAnyType(Type t){
		if(t.type instanceof SimpleTypeDecl && (t.type.name.equals("any"))){
			error("Usage of type any is not allowed", TypesPackage.Literals.TYPE__TYPE)
		}
	}
	
	@Check
	def checkForVoidType(Type t){
		if(t.type instanceof SimpleTypeDecl && (t.type.name.equals("void"))){
			error("Usage of type void is not allowed", TypesPackage.Literals.TYPE__TYPE)
		}
	}
	
	@Check
	def checkForIdType(Type t){
		if(t.type instanceof SimpleTypeDecl && (t.type.name.equals("id"))){
			error("Usage of type id is not allowed", TypesPackage.Literals.TYPE__TYPE)
		}
	}
	
	/*
	 * Constraints:
	 * - key type of maps is an enum or a simple type
	 * Rationale: semantics of equality of structured types needs further investigation
	 */
	 @Check
	 def checkKeyType(MapTypeConstructor mtc){
	 	if(!(mtc.keyType instanceof EnumTypeDecl) && !(mtc.keyType instanceof SimpleTypeDecl)){
	 		error("The type of map keys has to be enumeration or simple type", TypesPackage.Literals.TYPE__TYPE)
	 	}
	 }
	
	/*
	 * Constraints:
	 * - The following is implemented in the scope provider:
	 *   only int, string and real can be used as base types of user-defined simple types.
	 *   Note: it can be implemented alternatively: allow any simple type to be used as a base and
	 *   make then a validation check in this class.
	 * - TODO non-implemented constraints:
	 *   + the graph of transitive imports does not contain cycles. Note: the importing algorithm detects cycles
	 *     and does not traverse them more than once. This constraint is needed to avoid potential cyclic imports/
	 *     includes when code (e.g. C++) is generated from type models
	 *   + the chain of dependencies induced by record fields contains no cycles.
	 *     Clarification: record R1 with field A of type R2, R2 with field b of type R1 is not allowed.
	 *     Fields of vector types with base another record type should also be considered.
	 *     In CommaSuite, records are treated as values. The structure mentioned above can never be created.
	 */
	 
	 /*
	  * This methods implements the mechanism for registering third-party extensions to the 
	  * validators of the CommaSuite languages.
	  * All languages that directly or indirectly extend Types language inherit it.
	  */
	  
	 @Inject
	 override register(EValidatorRegistrar registrar) {
	 	super.register(registrar)
    	val VALIDATOR_ID = "org.eclipse.comma.types.commaValidator"
    	val reg = Platform.getExtensionRegistry()
    	if(reg !== null){
    		val extensions = reg.getConfigurationElementsFor(VALIDATOR_ID)
        	for(e : extensions){
        		val o = e.createExecutableExtension("class")
        		if(this.class.simpleName.startsWith(e.getAttribute("language"))){
        			for (ePackage : this.EPackages) {
    					registrar.register(ePackage, o as EValidator)
  					}
        		}
        	}	
    	}
    }
    
    @Inject extension CommaUtilities
    @Inject extension IQualifiedNameProvider
    
    /*
     * Constraints:
     * - imported type definitions have unique names
     * - local type definitions do not duplicate the imported ones
     */
    @Check
    def checkGlobalUniquenessOfTypes(TypesModel typesModel){
        if(typesModel.types.empty) return
        val multiMap = getImportedTypes(typesModel)
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported type " + entry.key.toString, typesModel, TypesPackage.Literals.TYPES_MODEL__TYPES)
            }
        }
        for(t : typesModel.types){
            val fqn = t.fullyQualifiedName
            if(multiMap.containsKey(fqn)){
                error("Local declaration duplicates imported type", t, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
    }
    
    def getImportedTypes(ModelContainer context) {
        val types = new HashSet<IEObjectDescription>
        var Multimap<QualifiedName, IEObjectDescription> multiMap = HashMultimap.create()
        if(context.name === null && !context.imports.exists[it instanceof NamespaceImport]) {
            val knownURIs = new LinkedHashSet<URI>(3);
            knownURIs.add(context.eResource.URI)
            val importedUris = TypesImportUriGlobalScopeProvider.traverseImportedURIs(context.eResource, knownURIs)
            for(uri : importedUris) {
                types.addAll(EcoreUtil2.getResource(context.eResource, uri.toString).resourceDescription.getExportedObjectsByType(TypesPackage.eINSTANCE.typeDecl))
            }
            for(t : types) {
                multiMap.put(t.qualifiedName, t)   
            }
        } else {
            multiMap = getGlobalDeclarations(context, TypesPackage.eINSTANCE.typeDecl)
        }
        multiMap.placePredefinedTypes
        multiMap
    }
    
    def Multimap<QualifiedName, IEObjectDescription> getGlobalDeclarations(EObject context, EClass type) {
        val allVisibleDeclarations = context.getVisibleEObjectDescriptions(type)
        val allExportedDeclarations = context.getResourceDescription.getExportedObjectsByType(type)
        val difference = allVisibleDeclarations.toSet
        difference.removeAll(allExportedDeclarations.toSet)
        val multiMap = HashMultimap.create()
        difference.forEach[multiMap.put(qualifiedName, it)]
        multiMap
    }
    
    def boolean usesOnlyFileImports(ModelContainer root, Set<URI> knownURIs) {
        if(!root.imports.filter(NamespaceImport).empty) return false
        for(fileImport : root.imports.filter(FileImport)) {
            val Resource importedResource = EcoreUtil2.getResource(root.eResource, fileImport.importURI)
            if(importedResource !== null){
                if(importedResource.allContents.head instanceof ModelContainer && !knownURIs.contains(importedResource.URI)){
                    knownURIs.add(importedResource.URI)
                    if(! usesOnlyFileImports(importedResource.allContents.head as ModelContainer, knownURIs)){
                        return false
                    }
                }
            }
        }
        return true
    }
}
