/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.generator

import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.util.CancelIndicator
import java.util.Set
import java.util.Map
import java.util.HashMap

class CmdLineContext implements IGeneratorContext {

	final static String context = "CMD_LINE"
	Set<String> excludedTasks = null
	Set<String> requestedTasks = null
	boolean execResult = true
	String errorString = ""
	Map<String, String> args = new HashMap<String, String>

	def String getContextString() {
		context
	}

	override getCancelIndicator() {
		CancelIndicator.NullImpl
	}
	
	def getExcludedTasks(){
	    return excludedTasks
	}
	
	def setExcludedTasks(Set<String> excludedTasks) {
	    this.excludedTasks = excludedTasks
	}
	
	def getRequestedTasks(){
        return requestedTasks
    }
    
    def setRequestedTasks(Set<String> requestedTasks) {
        this.requestedTasks = requestedTasks
    }
    
    def setExecutionResult(boolean result) {
        execResult = result
    }
    
    def boolean isSuccess() {
        execResult
    }
    
    def getErrorString() {
        errorString
    }
    
    def setErrorString(String msg) {
        errorString = msg
    }
    
    def getArguments() {
        args
    }
    
    def addArgument(String name, String value) {
        args.put(name, value)
    }
}
