/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.generator

import com.google.gson.Gson
import com.google.inject.Inject
import com.google.inject.Provider
import java.io.File
import java.io.FilenameFilter
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.Paths
import java.text.MessageFormat
import java.util.ArrayList
import java.util.Comparator
import java.util.HashSet
import java.util.List
import java.util.Set
import net.sourceforge.plantuml.SourceFileReader
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Options
import org.eclipse.comma.java.JArtifactInfoBuilder
import org.eclipse.comma.java.JCmdBuilder
import org.eclipse.comma.java.JCmdExecutor
import org.eclipse.comma.java.JCompiler
import org.eclipse.comma.java.JDependencyInfos
import org.eclipse.comma.java.JSourceInfos
import org.eclipse.comma.monitoring.dashboard.DashboardHelper
import org.eclipse.comma.monitoring.lib.CTask
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.diagnostics.Severity
import org.eclipse.xtext.generator.GeneratorDelegate
import org.eclipse.xtext.generator.JavaIoFileSystemAccess
import org.eclipse.xtext.util.CancelIndicator
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator

class CommaMain {
	static final String ERR_LOCATION_MISSING = 'Location is not provided as argument'
	static final String ERR_LOCATION_WRONG = 'Location could not be found. '

	static final String INFO_GENERATION_FINISHED = 'Code generation finished.'
	static final String INFO_COMMA_FINISHED = 'All CommaSuite Tasks are finished.'
	static final String INFO_SEARCHING = 'Searching for {0} ({1}) files in {2}'
	static final String INFO_OUTPUT = 'Output set: '
	static final String INFO_STOP = 'Errors found, stopping generation.'
	static final String INFO_READING = '-> Reading '
	static final String INFO_EMPTY_LOCATION = 'Location contained no project files. '

	static final String OPT_HELP = 'help'
	static final String OPT_HELP_C = 'h'
	static final String OPT_HELP_DESCRIPTION = 'Show options.'

	static final String OPT_LOCATION = 'location'
	static final String OPT_LOCATION_C = 'l'
	static final String OPT_LOCATION_DESCRIPTION = 'Location of the file or directory (Required argument)'

	static final String OPT_VALIDATION = 'validation'
	static final String OPT_VALIDATION_C = 'v'
	static final String OPT_VALIDATION_DESCRIPTION = 'Turns OFF validation, validation is ON by default.'

	static final String OPT_OUTPUT = 'output'
	static final String OPT_OUTPUT_C = 'o'
	static final String OPT_OUTPUT_DESCRIPTION = 'Set output location of the generated files, the default is "src-gen" at the provided "location".'
	static final String DEFAULT_OUTPUT_DIR = 'src-gen'

	static final String OPT_CLEAN_C = 'c'
	static final String OPT_CLEAN_DESCRIPTION = 'Turns ON cleaning the output folder before generation, clean is OFF by default.'
	static final String OPT_CLEAN = 'clean'

	static final String OPT_MODELPATH_C = 'mp'
    static final String OPT_MODELPATH_DESCRIPTION = 'List of semicolon separated directories that contain the input models.'
    static final String OPT_MODELPATH = 'modelpath'
    
    static final String OPT_SKIPTASKS_C = 's'
    static final String OPT_SKIPTASKS_DESCRIPTION = 'List of semicolon separated project task names that will not be executed.'
    static final String OPT_SKIPTASKS = 'skip'
    
    static final String OPT_RUNTASKS_C = 'r'
    static final String OPT_RUNTASKS_DESCRIPTION = 'List of semicolon separated project task names that will be executed.'
    static final String OPT_RUNTASKS = 'run'
	
	static final String MONITOR_MAIN_CLASS = "org.eclipse.comma.monitoring.generated.ScenarioPlayer";
	static final String MONITOR_MAIN_CLASS_PATH = "java/org/eclipse/comma/monitoring/generated/ScenarioPlayer.java";
    static final String GENERATED_JAR_NAME = "Player.jar";

	@Inject Provider<ResourceSet> resourceSetProvider

	@Inject IResourceValidator validator

	@Inject GeneratorDelegate generator

	@Inject JavaIoFileSystemAccess fileAccess

	String[] args
	String description
	String language
	String ext
	
	Options options

	def configure(String[] args, String description, String language, String ext) {
		this.args = args
		this.description = description
		this.language = language
		this.ext = ext
		this.options = createOptions
	}

	def read() {

		if (args.empty) {
			System::err.println(ERR_LOCATION_MISSING)
			showInfo(description, options)
			System.exit(1);
			return
		}

		val parser = new DefaultParser;
		val cmdLine = parser.parse(options, args);
		if (cmdLine.hasOption(OPT_HELP)) {
			showInfo(description, options)
		} else {
            if (cmdLine.hasOption(OPT_RUNTASKS) && cmdLine.hasOption(OPT_SKIPTASKS)) {
                System::err.println("Skip and Run tasks options cannot be used at the same time.")
                System.exit(1)
                return
            }
			val locationPath = getLocation(cmdLine)
			if (locationPath === null) {
				System::err.println(ERR_LOCATION_MISSING)
				showInfo(description, options)
				System.exit(1)
				return
			}
			if (!Files.exists(locationPath, LinkOption.NOFOLLOW_LINKS)) {
				System.out.println(ERR_LOCATION_WRONG + locationPath.toString)
				System.exit(1)
				return
			}
			
			val validation = ! cmdLine.hasOption(OPT_VALIDATION)

			if (Files.isDirectory(locationPath, LinkOption.NOFOLLOW_LINKS)) {
				println(MessageFormat.format(INFO_SEARCHING, language, ext, locationPath.toString))
				val dir = new File(locationPath.toString)
				val prjFiles = dir.listFiles(createFileFilter(ext));
				val createOutputSubFolders = prjFiles.size > 1 
				for (file : prjFiles) {
				    val outputdir = getOutputdir(cmdLine, locationPath, createOutputSubFolders ? file.name.substring(0, file.name.lastIndexOf(".")) : "")
                    System.out.println(INFO_OUTPUT + outputdir)
                    cleanOutput(cmdLine, outputdir)
					runGeneration(file.absolutePath, outputdir.toString, validation, cmdLine)
					System.out.println(INFO_GENERATION_FINISHED)
                    runMonitoring(outputdir)
				}
				if (prjFiles.empty) {
					System.out.println(INFO_EMPTY_LOCATION + locationPath.toString)
				}

			} else {
			    val outputdir = getOutputdir(cmdLine, locationPath, "")
                System.out.println(INFO_OUTPUT + outputdir)
                cleanOutput(cmdLine, outputdir)
				runGeneration(locationPath.toString, outputdir.toString, validation, cmdLine)
				System.out.println(INFO_GENERATION_FINISHED)
                runMonitoring(outputdir)
			}

			System.out.println("")
			System.out.println(INFO_COMMA_FINISHED)
			System.exit(0)
		}
	}

	def Options createOptions() {
		val options = new Options
		options.addOption(OPT_LOCATION_C, OPT_LOCATION, true, OPT_LOCATION_DESCRIPTION);
		options.addOption(OPT_OUTPUT_C, OPT_OUTPUT, true, OPT_OUTPUT_DESCRIPTION)
		options.addOption(OPT_HELP_C, OPT_HELP, false, OPT_HELP_DESCRIPTION)
		options.addOption(OPT_VALIDATION_C, OPT_VALIDATION, false, OPT_VALIDATION_DESCRIPTION)
		options.addOption(OPT_CLEAN_C, OPT_CLEAN, false, OPT_CLEAN_DESCRIPTION)
		options.addOption(OPT_MODELPATH_C, OPT_MODELPATH, true, OPT_MODELPATH_DESCRIPTION)
		options.addOption(OPT_SKIPTASKS_C, OPT_SKIPTASKS, true, OPT_SKIPTASKS_DESCRIPTION)
		options.addOption(OPT_RUNTASKS_C, OPT_RUNTASKS, true, OPT_RUNTASKS_DESCRIPTION)
	}

	def showInfo(String description, Options options) {
		val formatter = new HelpFormatter();
		formatter.printHelp(description, options);
	}

	def createFileFilter(String ext) {
		return new FilenameFilter() {
			override accept(File dir, String name) {
				(name.endsWith(ext))
			}
		}
	}

	def Path getLocation(CommandLine cmdLine) {
		val location = cmdLine.getOptionValue(OPT_LOCATION);
		if (location !== null) {
			val locationPath = Paths.get(location)
			if (locationPath.isAbsolute) {
				return locationPath
			} else {
				val current = Paths.get("").toAbsolutePath()
				return current.resolve(locationPath)
			}
		}
		return null
	}

	def getOutputdir(CommandLine cmdLine, Path location, String prjDir) {
		if (cmdLine.hasOption(OPT_OUTPUT)) {
			val outputPath = Paths.get(cmdLine.getOptionValue(OPT_OUTPUT)).resolve(prjDir).resolve(DEFAULT_OUTPUT_DIR)
			if (outputPath.isAbsolute) {
				return outputPath
			} else {
				if (Files.isDirectory(location, LinkOption.NOFOLLOW_LINKS)) {
					return location.resolve(outputPath).normalize
				} else {
					val dirLocation = Paths.get(
						location.toString.substring(0, location.toString.lastIndexOf(File.separator)))
					return dirLocation.resolve(outputPath)
				}
			}
		} else {
			if (Files.isDirectory(location, LinkOption.NOFOLLOW_LINKS)) {
				return location.resolve(prjDir).resolve(DEFAULT_OUTPUT_DIR).normalize
			} else {
				val dirLocation = Paths.get(
					location.toString.substring(0, location.toString.lastIndexOf(File.separator)))
				return dirLocation.resolve(prjDir).resolve(DEFAULT_OUTPUT_DIR)
			}
		}
	}
	
	def Set<String> getResourceExtensions(){
	    val resourceExtensions = new HashSet<String>(Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().keySet)
	    //skip loading the following resource types
	    resourceExtensions.remove("events")
	    resourceExtensions.remove("prj")
	    resourceExtensions.remove("traces")
	    resourceExtensions
	}
	
	def loadResources(ResourceSet set, String[] modelDirs) {
	    val resExtensions = resourceExtensions
	    for(modelDirStr : modelDirs){
            val modelDir = new File(modelDirStr)
            if(modelDir.directory){ //iterate over the content of the directory
                val files = Files.walk(Paths.get(modelDir.absolutePath)).filter[it !== null && it.toFile.file]
                files.forEach[
                    val fileAbsolutePath = it.toAbsolutePath.toString
                    val fileExt = fileAbsolutePath.substring(fileAbsolutePath.lastIndexOf(".") + 1)
                    if(resExtensions.contains(fileExt)) {
                        System.out.println("Loading: " + fileAbsolutePath)
                        set.getResource(URI.createFileURI(fileAbsolutePath), true)
                    }
                ]
            } else {
                System.out.println("Skipping " + modelDirStr + ". Not a directory.")
            }
        }
	}

	def runGeneration(String string, String outputdir, Boolean validation, CommandLine cmdLine) {
		// Load the resources
		System.out.println(INFO_READING + string)
		val set = resourceSetProvider.get
		
		if (cmdLine.hasOption(OPT_MODELPATH)) {
            val modelDirs = cmdLine.getOptionValue(OPT_MODELPATH).split(";");
            loadResources(set, modelDirs)
        }
		val resource = set.getResource(URI.createFileURI(string), true)

		// Validate the resources
		if(validation) {
		  var errorsFound = false
		  //first validate the project to force loading possible transitive imports
		  validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl)
		  for(res : set.resources.filter[!URI.toString.endsWith("types.types")]) {
		      //warnings are ignored
		      val issues = validator.validate(res, CheckMode.ALL, CancelIndicator.NullImpl).filter[it.severity == Severity.ERROR]
              if (!issues.empty) {
                 issues.forEach[System.err.println(it)]
                 errorsFound = true
              }
          }
          if (errorsFound) {
              System.out.println(INFO_STOP)
              System.exit(1)
              return;
          }
        }
		
		// Configure and start the generator
		fileAccess.outputPath = outputdir
		setCommaGen(fileAccess, outputdir)
		
		//prepare generator context
		val context = new CmdLineContext
		initializeContext(context, cmdLine)
		generator.generate(resource, fileAccess, context)
		if(!context.success) {
		    System.err.println(context.errorString)
            System.exit(1)
            return
		}
	}

	def cleanOutput(CommandLine cmdLine, Path outputdir) {
		if (cmdLine.hasOption(OPT_CLEAN)) {
			println("Start cleaning output directory.")
			val commagendir = outputdir.parent.resolve(CommaFileSystemAccess.COMMA_OUTPUT_CONF.outputDirectory).normalize
			//clean src-gen
			if(new File(outputdir.toString).exists){
				Files.walk(outputdir)
				.sorted(Comparator.reverseOrder())
				.forEach[Files::delete(it)]
			}
			//clean comma-gen
			if(new File(commagendir.toString).exists){
				Files.walk(commagendir)
				.sorted(Comparator.reverseOrder())
				.forEach[Files::delete(it)]
			}
			println("Clean finished.")
		}
	}

	def setCommaGen(JavaIoFileSystemAccess fileAccess, String outputdir) {
		val commaConf = CommaFileSystemAccess.COMMA_OUTPUT_CONF
		if(!commaConf.outputDirectory.contains(Paths.get(outputdir).parent.toString))
			commaConf.outputDirectory = Paths.get(outputdir).parent.toString + CommaFileSystemAccess.COMMA_OUTPUT_FOLDER
		fileAccess.outputConfigurations.put(CommaFileSystemAccess.COMMA_OUTPUT_ID, commaConf)
	}

	def getCommaGen() {
		val commaGen = fileAccess.outputConfigurations.get(CommaFileSystemAccess.COMMA_OUTPUT_ID)
		commaGen.outputDirectory
	}

	/**
	 * Runs the monitoring for the provided tasks.
	 */
	def runMonitoring(Path outputPath) {
	    val javaSourcePath = outputPath.resolve("java/").toString
	    val scenarioPlayerPath = outputPath.resolve(MONITOR_MAIN_CLASS_PATH)
	    if(!scenarioPlayerPath.toFile.exists) return
	    val generatedSourceInfos = JSourceInfos.getJavaSourcesFromSrcDirectory(javaSourcePath);
        val monitorRuntimeJar = new File(CTask.getProtectionDomain().getCodeSource().getLocation().toURI())
        val dashboardJar = new File(DashboardHelper.getProtectionDomain().getCodeSource().getLocation().toURI())
        val gsonJar = new File(Gson.getProtectionDomain().getCodeSource().getLocation().toURI())
        val plantumlJar = new File(SourceFileReader.getProtectionDomain().getCodeSource().getLocation().toURI())
        val monitorRuntimeDependency = JDependencyInfos.create(monitorRuntimeJar);
        val gsonDependency = JDependencyInfos.create(gsonJar);
        val dashboardDependency = JDependencyInfos.create(dashboardJar);
        val plantumlDependency = JDependencyInfos.create(plantumlJar);

        // alternative to the above that works only from the CLI but not in run/debug mode from the IDE
        // val commaJar = new File(this.class.getProtectionDomain().getCodeSource().getLocation().toURI())
        // val commaDependency = JDependencyInfos.create(commaJar);

	    val compiler = new JCompiler()
                        .withSourceInfos(generatedSourceInfos)
                        .withDependencyInfos(monitorRuntimeDependency)
                        .withDependencyInfos(gsonDependency)
                        .withDependencyInfos(dashboardDependency)
                        .withDependencyInfos(plantumlDependency)

        System.out.println("Compiling and building generated monitoring code ...");
        val classInfos = compiler.compile();

        // create JArtifactBuilder and build JArtifact
        val artifactInfo = new JArtifactInfoBuilder()
                            .withArchiveFile(new File(javaSourcePath + File.separator + GENERATED_JAR_NAME))
                            .withClassInfos(classInfos)
                            .withDependencyInfos(monitorRuntimeDependency)
                            .withDependencyInfos(gsonDependency)
                            .withDependencyInfos(dashboardDependency)
                            .withDependencyInfos(plantumlDependency)
                            .withMainClass(MONITOR_MAIN_CLASS)
                            .build();

        // create the cmd with options and arguments
        val cmd = new JCmdBuilder()
                   .withArtifactInfo(artifactInfo)
                   .build();

        // execute the command with the project directory as working directory
        System.out.println("Executing monitoring tasks...");
        
        var monitor = new JCmdExecutor(cmd)
			.withWorkingDirectory(outputPath.resolve("../").normalize.toFile)
			.withOnStdOut([text | System.out.print(text)])
			.execute();
		monitor.process.waitFor(); 
		
		if (!monitor.stdErr.isEmpty) {
			System.out.println("Error occurred during compilation and execution:");
			System.out.print(monitor.stdErr);
			System.exit(1);
		}
		
		System.out.println("Finished executing monitoring tasks in " + monitor.executionTime  + " secs");
	    convertPlantUml()
	}

	def convertPlantUml() {
		val plantumlFiles = new ArrayList<File>
		findPlantUmlFiles(new File(getCommaGen()), plantumlFiles)
		if (!plantumlFiles.isEmpty) {
			println("Monitoring errors found, converting plantuml files to images.")

			for (plantFile : plantumlFiles) {
				println("--> " + plantFile.name)

				val reader = new SourceFileReader(plantFile);
				if (!reader.generatedImages.isEmpty) {
					reader.getGeneratedImages().get(0);
				}
			}
		}
	}

	def void findPlantUmlFiles(File dir, List<File> plantuml) {
		for (file : dir.listFiles) {
			if(file.name.endsWith(".plantuml")) {
				plantuml.add(file)
			}
			if(file.isDirectory) {
				findPlantUmlFiles(file, plantuml)
			}
		}
	}
	
	def Options getOptions() {
	    options
	}
	
	def initializeContext(CmdLineContext context, CommandLine cmdLine) {
	    val iter = cmdLine.iterator
	    while(iter.hasNext) {
	        val o = iter.next
	        context.addArgument(o.opt, o.value)
	    }
        if(cmdLine.hasOption(OPT_RUNTASKS)) {
            context.setRequestedTasks(cmdLine.getOptionValue(OPT_RUNTASKS).split(";").toSet)
        } else if(cmdLine.hasOption(OPT_SKIPTASKS)) {
            context.setExcludedTasks(cmdLine.getOptionValue(OPT_SKIPTASKS).split(";").toSet)
        }
	}
}