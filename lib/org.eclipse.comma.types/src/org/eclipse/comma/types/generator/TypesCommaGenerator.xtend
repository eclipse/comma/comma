/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.generator

import org.eclipse.comma.types.types.TypeReference
import org.eclipse.comma.types.types.TypeDecl
import org.eclipse.comma.types.types.VectorTypeConstructor
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.EnumTypeDecl
import org.eclipse.comma.types.types.RecordTypeDecl
import org.eclipse.comma.types.utilities.TypeUtilities
import org.eclipse.comma.types.types.VectorTypeDecl
import org.eclipse.comma.types.types.MapTypeConstructor
import org.eclipse.comma.types.types.MapTypeDecl

/*
 * This class provides methods that map CommaSuite types to their CommaSuite text representation
 * In principle, the corresponding text nodes can be used but sometimes fully qualified names 
 * should be given in places where short names are used in a source model.
 * 
 * This class is mostly used when .traces models are generated from scenarios
 */
 
 //TODO check if this functionality can be used in the capture to trace transformation
class TypesCommaGenerator {
	
	def dispatch CharSequence typeToCommaSyntax(TypeReference t){
		typeToCommaSyntax(t.type)
	}
	
	def dispatch CharSequence typeToCommaSyntax(TypeDecl t){
		generateTypeName(t)
	}
	
	def dispatch CharSequence typeToCommaSyntax(VectorTypeConstructor t){
		'''«typeToCommaSyntax(t.type)»«FOR d : t.dimensions»[]«ENDFOR»'''
	}
	
	def dispatch CharSequence typeToCommaSyntax(MapTypeConstructor t){
		'''map<«typeToCommaSyntax(t.type)», «typeToCommaSyntax(t.valueType)»>'''
	}
	
	//classes can override this method to prefix with, for example, interface name
	def CharSequence generateTypeName(TypeDecl t){
		t.name
	}
	
	def dispatch CharSequence generateDefaultValue(TypeReference t){
		generateDefaultValue(t.type)
	}
	
	def dispatch CharSequence generateDefaultValue(SimpleTypeDecl t){
		if(t.name.equals("int")) return '''0'''
		if(t.name.equals("real")) return '''0.0'''
		if(t.name.equals("bool")) return '''true'''
		if(t.name.equals("string")) return '''""'''
		
		""
	}
	
	def dispatch CharSequence generateDefaultValue(EnumTypeDecl t){
		typeToCommaSyntax(t) + "::" + t.literals.get(0).name
	}
	
	def dispatch CharSequence generateDefaultValue(RecordTypeDecl  t){
		'''«typeToCommaSyntax(t)»{«FOR f : TypeUtilities::getAllFields(t) SEPARATOR ', '»«f.name» = «generateDefaultValue(f.type)»«ENDFOR»}'''
	}
		
	def dispatch CharSequence generateDefaultValue(VectorTypeDecl t){
		'''<«typeToCommaSyntax(t)»>[]'''
	}
	
	def dispatch CharSequence generateDefaultValue(VectorTypeConstructor t){
		'''<«typeToCommaSyntax(t)»>[]'''
	}
	
	def dispatch CharSequence generateDefaultValue(MapTypeDecl t){
		'''<«typeToCommaSyntax(t)»>{}'''
	}
	
	def dispatch CharSequence generateDefaultValue(MapTypeConstructor t){
		'''<«typeToCommaSyntax(t)»>{}'''
	}
}