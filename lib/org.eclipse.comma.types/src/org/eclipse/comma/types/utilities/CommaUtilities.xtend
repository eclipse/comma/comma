/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.utilities

import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import org.eclipse.comma.types.types.ModelContainer
import org.eclipse.comma.types.types.NamedElement
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.resource.IContainer
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.emf.ecore.resource.Resource

class CommaUtilities {
    public static final String NAMESPACE_SEPARATOR = "."
    
	static def <T extends EObject> List<T> resolveProxy(EObject context, Iterable<IEObjectDescription> elements) {
		var List<T> result = new ArrayList<T>
		for (descr : elements) {
			var object = descr.EObjectOrProxy
			if (object.eIsProxy) {
				object = EcoreUtil2.resolve(object, context)
			}
			result.add(object as T)
		}
		result
	}
	
	def static QualifiedName getNamespace(EObject o) {
	    var result = QualifiedName.EMPTY
	    val containers = EcoreUtil2::getAllContainers(o)
	    if(containers.empty) return result
	    val top = containers.last 
        if(top instanceof ModelContainer) {
           if(top.name !== null) {
               for(segment : top.name.split("\\.")) {
                    result = result.append(segment.replace("^", ""))   
               }
           }
        }
	    for(container : containers.filter(NamedElement).toList.reverseView){
	        result = result.append(container.name.replace("^", ""))
	    }
	    result
	}
	
	def static String getNamespaceAsString(EObject o) {
	    o.namespace.toString(NAMESPACE_SEPARATOR)
	}
	
	def static QualifiedName getFullyQualifiedName(NamedElement o) {
	    o.getNamespace.append(o.name.replace("^", ""))
    }
    
    def static String getFullyQualifiedNameAsString(NamedElement o) {
        o.fullyQualifiedName.toString(NAMESPACE_SEPARATOR)
    }
	
	@Inject ResourceDescriptionsProvider rdp
	@Inject IContainer.Manager cm
    
    def getVisibleContainers(EObject o) {
        val index = rdp.getResourceDescriptions(o.eResource)
        val rd = index.getResourceDescription(o.eResource.URI)
        cm.getVisibleContainers(rd, index)
    }
    
    def getVisibleEObjectDescriptions(EObject o, EClass type) {
        o.getVisibleContainers.map[ container |
        container.getExportedObjectsByType(type)
        ].flatten
    }
    
    def getResourceDescription(EObject o) {
        o.eResource.resourceDescription
    }
    
    def getResourceDescription(Resource r) {
        val index = rdp.getResourceDescriptions(r)
        index.getResourceDescription(r.URI)
    }
    
    def getExportedEObjectDescriptions(EObject o) {
        o.getResourceDescription.getExportedObjects
    }
	
	static def commaVersion(){
		"2.0.0"
	}
	
}
