/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.scoping

import com.google.common.base.Predicate
import java.util.LinkedHashSet
import org.eclipse.comma.types.types.FileImport
import org.eclipse.comma.types.types.ModelContainer
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider

class TypesImportUriGlobalScopeProvider extends ImportUriGlobalScopeProvider {

	static final String BASIC_TYPES_JAR_PATH = "org/eclipse/comma/types/types.types";
	static final URI BASIC_TYPES_PATH = URI.createURI(
		Thread.currentThread().contextClassLoader.getResource(BASIC_TYPES_JAR_PATH).toString)
	
	override getImportedUris(Resource resource) {
		var LinkedHashSet<URI> importedURIs = new LinkedHashSet<URI>(5);
				
		if(resource.allContents.head instanceof ModelContainer){
			val knownURIs = new LinkedHashSet<URI>(5);
			knownURIs.add(resource.URI)
			importedURIs =  traverseImportedURIs(resource, knownURIs);
		}else{
			importedURIs = super.getImportedUris(resource)
		}
		//implicit import of the built-in types in types.types
		importedURIs.add(BASIC_TYPES_PATH);
		
		return importedURIs
    }
    
    override getScope(Resource context, boolean ignoreCase, EClass type, Predicate<IEObjectDescription> filter) {
        super.getScope(context, ignoreCase, type, filter)
    }
    	
	static def LinkedHashSet<URI> traverseImportedURIs(Resource resource, LinkedHashSet<URI> knownURIs){
		val LinkedHashSet<URI> result = new LinkedHashSet<URI>(5);
		
		val root = resource.allContents.head as ModelContainer
		for(import : root.imports.filter(FileImport)){
			val Resource importedResource = EcoreUtil2.getResource(resource, import.importURI)
			if(importedResource !== null){
				if(importedResource.allContents.head instanceof ModelContainer){
					if(! knownURIs.contains(importedResource.URI)){
						knownURIs.add(importedResource.URI);
						result.add(importedResource.URI)
						result.addAll(traverseImportedURIs(importedResource, knownURIs))
					}
				}
			}
		}
		return result
	}
}
