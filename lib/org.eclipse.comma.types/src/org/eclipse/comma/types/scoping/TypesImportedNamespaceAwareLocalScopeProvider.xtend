/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.scoping

import com.google.inject.Binder
import com.google.inject.Inject
import com.google.inject.name.Names
import java.util.List
import org.eclipse.comma.types.types.ModelContainer
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider
import org.eclipse.xtext.scoping.impl.ImportNormalizer
import org.eclipse.xtext.scoping.impl.ImportedNamespaceAwareLocalScopeProvider

/*
 * This class implements the functionality for implicit import of the namespace
 * declared in the model that contains the context object.
 * 
 * It should be bound in the TypesRuntimeModule and also in all other Comma
 * languages that support the implicit namespace import by calling the static method bindIScopeProviderDelegate
 */
class TypesImportedNamespaceAwareLocalScopeProvider extends ImportedNamespaceAwareLocalScopeProvider {
    @Inject extension IQualifiedNameProvider
    
    override protected List<ImportNormalizer> internalGetImportedNamespaceResolvers(EObject context, boolean ignoreCase) {
        val resolvers = super.internalGetImportedNamespaceResolvers(context, ignoreCase)
        if (context instanceof ModelContainer) {
            val fqn = context.fullyQualifiedName
            // fqn is the namespace of this model
            if (fqn !== null) {
                // all the external definitions with the same namespace of this model
                // will be automatically visible in this model, without an import
                resolvers += new ImportNormalizer(fqn, true, ignoreCase) // use wildcards
            }
        }
        return resolvers
    }
    
    static def bindIScopeProviderDelegate(Binder binder) {
        binder.bind(IScopeProvider)
              .annotatedWith(Names.named(AbstractDeclarativeScopeProvider.NAMED_DELEGATE))
               .to(TypesImportedNamespaceAwareLocalScopeProvider)
    }
}