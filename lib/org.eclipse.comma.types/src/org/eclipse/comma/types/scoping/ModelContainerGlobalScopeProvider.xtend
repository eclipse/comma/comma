/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.scoping

import com.google.common.base.Predicate
import com.google.inject.Inject
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.scoping.impl.DefaultGlobalScopeProvider
import org.eclipse.comma.types.types.ModelContainer
import org.eclipse.comma.types.types.NamespaceImport

/*
 * Reuses the functionality of superclass getScope and passes a parent scope that implements
 * the file transitive import Comma functionality. The latter one loads the types.types library
 * with predefined Comma primitive types.
 * 
 * The inherited DefaultGlobalScopeProvider implements the import of namespaces.
 */
class ModelContainerGlobalScopeProvider extends DefaultGlobalScopeProvider {
    @Inject
    TypesImportUriGlobalScopeProvider uriGlobalScopeProvider
    
    override getScope(Resource context, boolean ignoreCase, EClass type, Predicate<IEObjectDescription> filter) {
        val parentScope = uriGlobalScopeProvider.getScope(context, ignoreCase, type, filter)
        val root = context.allContents.head
        if(root instanceof ModelContainer){
            if(root.name !== null || ! root.imports.filter(NamespaceImport).empty){
                return getScope(parentScope, context, ignoreCase, type, filter);
            }
        }
        return parentScope
    }
}
