/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.scoping

import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.impl.FilteringScope

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class TypesScopeProvider extends AbstractTypesScopeProvider {
	
	override getScope(EObject context, EReference reference){
		if(reference == TypesPackage.Literals.SIMPLE_TYPE_DECL__BASE){
			return new FilteringScope(super.getScope(context, reference), [val n = name.toString n.equals("int") || n.equals("string") || n.equals("real")])
		}
		
		return super.getScope(context, reference);
	}
	
}
