/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.validation

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.actions.actions.ActionList
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.VariableDeclBlock
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.scoping.BehaviorScopeProvider
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.Variable
import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.validation.Check
import org.eclipse.comma.actions.actions.Reply
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.ParallelComposition
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.behavior.behavior.NonTriggeredTransition
import org.eclipse.comma.behavior.behavior.BehaviorPackage

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class BehaviorValidator extends DataConstraintsValidator {
	
	public static final String STATEMACHINE_DUPLICATE_INTERFACE = "statemachine_duplicate_interface"
	public static final String STATEMACHINE_UNUSED_INTERFACE = "statemachine_unused_interface"
	public static final String STATEMACHINE_DUPLICATE_STATE = "statemachine_duplicate_state"
	public static final String STATEMACHINE_DUPLICATE_VAR = "statemachine_duplicate_var"
	public static final String STATEMACHINE_UNITIALIZED_VAR = "statemachine_uninitialized_var"
	public static final String STATEMACHINE_UNUSED_VAR = "statemachine_unused_var"
	
	/*
	 * Constraints:
	 * - warning on uninitialized variables used in state machines.
	 * 
	 * Note: variables in constraint blocks cannot be initialized
	 */
	@Check
	override checkNotInitializedVariables(VariableDeclBlock block){
		if(block instanceof AbstractBehavior) super.checkNotInitializedVariables(block)
	}
	
	/*
	 * Constraints:
	 * - state machine names are unique
	 */
	@Check
	def checkDuplicatedMachineNames(AbstractBehavior spec) {
		checkForNameDuplications(spec.machines, "state machine", null)
	}
	
	/*
	 * Constraints:
	 * - state names are unique across all machines
	 */
	@Check
	def checkDuplicateStateNames(AbstractBehavior spec){
		checkForNameDuplications(spec.machines.map[states].flatten, "state", null)
	}
	
	/*
	 * Constraints:
	 * - fragment names are unique
	 */
	@Check
	def checkDuplicatePCFragmentNames(AbstractBehavior spec){
		checkForNameDuplications(spec.fragments, "event fragment", null)
	}
	
	/*
	 * Constraints:
	 * - warning on unused global variables. Used means referred to in an expression
	 */
	@Check
	def checkUnusedGlobalVariables(AbstractBehavior spec){
		val List<Variable> variables = new ArrayList<Variable>()
		variables.addAll(spec.vars)
		variables.removeAll(EcoreUtil2.getAllContentsOfType(spec, ExpressionVariable).map[variable]) //Potentially expensive...
		for (v : variables) {
			warning('Unused variable.', ActionsPackage.Literals.VARIABLE_DECL_BLOCK__VARS,
				spec.vars.indexOf(v))
		}
	}
	
	@Check
    def checkVariableShadowing(ActionWithVars act) {
        var pred = act.eContainer
        if(pred instanceof ActionList) {
            val index = pred.actions.indexOf(act)
            if(index > 0) {
                pred = pred.actions.get(index-1)
            }
        }
        val visibleVars = (scopeProvider as BehaviorScopeProvider).scope_variable(pred)
        for(v : act.parameters) { 
            if(!visibleVars.getElements(QualifiedName.create(v.name)).empty) {
                error("Variable with this name is already defined", v, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
    }
    
    /*
     * Constraints:
     * - silent transitions are not allowed
     * 
     * Note: a silent transition is a non-triggered transition whose body does not 
     * send any event.
     */
    @Check
    def checkSilentTransitions(NonTriggeredTransition t) {
        for(c : t.clauses) {
            if(!hasEvent(c.actions)) {
                error("The clause has an execution path without any event", t, BehaviorPackage.Literals.TRANSITION__CLAUSES, t.clauses.indexOf(c))
            }
        }
    }
    
    def boolean hasEvent(ActionList actions) {
        if(actions === null) {
            return false
        }
        for(a : actions.actions) {
            switch(a) {
                EventWithVars |
                Reply |
                EventCall |
                ParallelComposition : {
                    return true
                }
                IfAction : {
                    if(hasEvent(a.thenList) && hasEvent(a.elseList)) {
                        return true
                    }
                }
            }
        }
        return false
    }
}
