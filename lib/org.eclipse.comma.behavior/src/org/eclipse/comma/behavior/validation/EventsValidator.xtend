/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.validation

import java.util.HashSet
import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.comma.behavior.behavior.EventInState
import org.eclipse.xtext.validation.Check
import org.eclipse.comma.actions.actions.EventPattern
import org.eclipse.comma.actions.actions.ParameterizedEvent
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.expressions.expression.ExpressionPackage

class EventsValidator extends StateMachineValidator {
	
	@Check
	def checkEventForDuplicatedStates(EventInState ev){
		var states = new HashSet<String>
		for(s : ev.state){
			if(!states.add(s.name)){
				warning("Duplicated state", BehaviorPackage.Literals.EVENT_IN_STATE__STATE, ev.state.indexOf(s))
			}
		}	
	}
	
	@Check 
    def checkEventPreConditionType(EventInState ev){
        if(ev.preCondition !== null){
            if( !identical(ev.preCondition.typeOf, boolType)){
                error('Condition has to be of boolean type.', BehaviorPackage.Literals.EVENT_IN_STATE__PRE_CONDITION)
            }
        }
    }
    
    /*
     * Only variables defined in an interface can be used in this condition expression.
     * Mixing interface and non-interface variables can only happen in data and generic
     * constraints. In theory, it is possible to allow both kinds of variables
     * to appear but this requires more changes in the constraints language semantics to be
     * made. In addition, there is no compelling use case yet.
     */
    @Check
    def checkEventPreConditionVariables(EventInState ev){
        if(ev.preCondition !== null){
            val nonInterfaceVars = EcoreUtil2.getAllContentsOfType(ev.preCondition, ExpressionVariable).
                filter[variable !== null && !(variable.eContainer instanceof AbstractBehavior)]
            for(v : nonInterfaceVars){
                error('Only interface variables are allowed.', v, ExpressionPackage.Literals.EXPRESSION_VARIABLE__VARIABLE)
            }
        }
    }
	
	/*
	 * Constraints:
	 * - when used in constraints, the actual parameters in event patterns are either variables or if not,
	 *   the expression cannot contain variables
	 * Rationale: variable binding cannot happen if this constraint does not hold
	 */
	@Check
	def checkParametersForVariables(EventPattern event){
		if(! (event.eContainer instanceof EventInState)) {return}
		if(event instanceof ParameterizedEvent)
			for(parameter : event.parameters.filter[it | ! (it instanceof ExpressionVariable)]){
				if (! EcoreUtil2.getAllContentsOfType(parameter, ExpressionVariable).empty) {
						error('Parameter cannot be an expression containing variables.',
							ActionsPackage.Literals.PARAMETERIZED_EVENT__PARAMETERS, event.parameters.indexOf(parameter))
				}
			}
	}
}