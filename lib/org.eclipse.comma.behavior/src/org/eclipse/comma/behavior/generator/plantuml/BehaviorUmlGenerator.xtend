/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.generator.plantuml

import java.io.File
import net.sourceforge.plantuml.SourceFileReader
import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.generator.plantuml.ActionsUmlGenerator
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.behavior.Clause
import org.eclipse.comma.behavior.behavior.ConditionedAbsenceOfEvent
import org.eclipse.comma.behavior.behavior.ConditionedEvent
import org.eclipse.comma.behavior.behavior.DataConstraint
import org.eclipse.comma.behavior.behavior.DataConstraintEvent
import org.eclipse.comma.behavior.behavior.DataConstraintUntilOperator
import org.eclipse.comma.behavior.behavior.EventInterval
import org.eclipse.comma.behavior.behavior.GroupTimeConstraint
import org.eclipse.comma.behavior.behavior.PeriodicEvent
import org.eclipse.comma.behavior.behavior.SingleTimeConstraint
import org.eclipse.comma.behavior.behavior.StateMachine
import org.eclipse.comma.behavior.behavior.TimeInterval
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.comma.types.types.TypeReference
import org.eclipse.comma.types.types.VectorTypeConstructor
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.generator.AbstractFileSystemAccess
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.actions.actions.CommandReplyWithVars
import org.eclipse.comma.actions.actions.ActionWithVars

class BehaviorUmlGenerator extends ActionsUmlGenerator {
	
	boolean generateImages
	final String filePrefix
	final AbstractBehavior behavior

	static final String EXT_UML = ".plantuml"
	static final String EXT_PNG = ".png"
	static final String COMPLETE_UML = "complete"
	static final String NOACTION_UML = "noactions"
	static final String NOSELF_UML = "noselftrans"
	static final String NOSELF_ACTION_UML = "noactions_noselftrans"
	static final String SEPERATOR = "_"
	
	URI outputURI	

	new(AbstractBehavior behavior, IFileSystemAccess fsa) {
		super("", fsa)		
		this.behavior = behavior
		this.filePrefix = behavior.eResource.getURI().trimFileExtension.lastSegment + SEPERATOR			
	}
	
	def setPrjURIForImages(URI prjURI) {
		generateImages = true
		val projectPath = getOutputURI(prjURI)
		val gen = URI.createURI(CommaFileSystemAccess.getGenerationFolder(fsa))
		outputURI = gen.resolve(projectPath)
	}

	def doGenerate() {		
		for (machine : behavior.machines) {
			val machineFile = filePrefix + machine.name + SEPERATOR
			generateUML(machineFile + COMPLETE_UML, toUMLStateMachine(machine, true, true))			
			generateUML(machineFile + NOACTION_UML, toUMLStateMachine(machine, true, false));
			generateUML(machineFile + NOSELF_UML, toUMLStateMachine(machine, false, true));
			generateUML(machineFile + NOSELF_ACTION_UML, toUMLStateMachine(machine, false, false));
		}

		if (behavior.timeConstraintsBlock !== null) {
			for (tc : behavior.timeConstraintsBlock.timeConstraints) {
				generateUML(filePrefix + tc.name, toUMLSequenceDiagram(tc));
			}
		}

		if (behavior.dataConstraintsBlock !== null) {
			for (dc : behavior.dataConstraintsBlock.dataConstraints) {
				generateUML(filePrefix + dc.name, toUMLSequenceDiagram(dc));
			}
		}
	}
	
	/* Generate Plant UML text and PNG image */
	def generateUML(String file, CharSequence contents) {
		fsa.generateFile(file + EXT_UML, contents)		
		
		if(generateImages) {
			fsa.generateFile(file + EXT_PNG, "")
			val umlURI = URI.createURI(file + EXT_UML).resolve(outputURI) //TODO check this
			val umlFile = new File(umlURI.toFileString)		
			val reader = new SourceFileReader(umlFile);
			if(!reader.generatedImages.isEmpty) {
				reader.getGeneratedImages().get(0);	
			} 
		}
	}

	def dispatch toUMLSequenceDiagram(DataConstraint dc) '''
		@startuml
		title Data Rule «dc.name»
		«FOR step : dc.steps»
			«dataConstraintStepToUML(step)»
		«ENDFOR»
		... where globally «generateExpression(dc.condition)»...
		@enduml
	'''

	def dispatch CharSequence dataConstraintStepToUML(DataConstraintEvent st) '''
		«IF st.negated !== null»«eventToUML(st.event.event, false)»«ELSE»«eventToUML(st.event.event, true)»«ENDIF»
	'''

	def dispatch CharSequence dataConstraintStepToUML(DataConstraintUntilOperator st) '''
		loop «IF st.body.negated !== null»not observed«ELSE»observe«ENDIF»
		«eventToUML(st.body.event.event, true)»
		end
		... until ...
		«dataConstraintStepToUML(st.stop)»
	'''

	def dispatch toUMLSequenceDiagram(SingleTimeConstraint tc) '''
		@startuml
		title Timing Rule «tc.name»
		«simpleTimeConstraintToUML(tc.constraint)»
		@enduml
	'''

	def dispatch toUMLSequenceDiagram(GroupTimeConstraint tc) '''
		@startuml
		title Scenario «tc.name»
		«simpleTimeConstraintToUML(tc.first)»
		«FOR c : tc.followups»
			... within «intervalToString(c.interval)» ...
			«eventToUML(c.event.event, true)»
		«ENDFOR»
		@enduml
	'''

	def dispatch simpleTimeConstraintToUML(ConditionedEvent r) '''
		«eventToUML(r.condition.event, true)»
		... within «intervalToString(r.interval)» ...
		«eventToUML(r.event.event, true)»	
	'''

	def dispatch simpleTimeConstraintToUML(ConditionedAbsenceOfEvent r) '''
		«eventToUML(r.condition.event, true)»
		... within «intervalToString(r.interval)» ...
		«eventToUML(r.event.event, false)»	
	'''

	def dispatch simpleTimeConstraintToUML(EventInterval r) '''
		«eventToUML(r.condition.event, true)»
		alt the following event is observed
		...within «intervalToString(r.interval)» ...
		«eventToUML(r.event.event, true)»
		else event is not observed -> rule is not triggered
		...other events...
		end
	'''

	def dispatch simpleTimeConstraintToUML(PeriodicEvent r) '''
		«eventToUML(r.condition.event, true)»
		loop period «generateExpression(r.period)» ms and jitter «generateExpression(r.jitter)» ms
		«eventToUML(r.event, true)»
		end
		«IF r.stopEvent !== null»
			...until...
			«eventToUML(r.stopEvent.event, true)»
		«ENDIF»
	'''

	def intervalToString(TimeInterval i) '''
	[«IF i.begin !== null»«generateExpression(i.begin)» ms«ENDIF» .. «IF i.end !== null»«generateExpression(i.end)» ms«ENDIF» ]'''

	def toUMLStateMachine(StateMachine m, boolean withSelfTransitions, boolean withActions) '''
		@startuml
		
		[*] -> «m.states.filter(s | s.initial).get(0).name»«IF withActions»: /-«ENDIF»
		«FOR s : m.states»
			«FOR t : StateMachineUtilities::transitionsForState(m, s)»
				«FOR c : t.clauses»
					«IF ((c.target !== null) && (s == c.target)) || (c.target === null)»
						«IF withSelfTransitions»
						«s.name» --> «s.name»«IF (t instanceof TriggeredTransition) || (t.guard !== null) || hasActionPart(c) || withActions»: «ENDIF»«IF t instanceof TriggeredTransition»«(t.trigger.eContainer as Signature).name»::«t.trigger.name»«IF t.parameters.size() > 0»(«FOR p : t.parameters SEPARATOR ', '»«typeToString(p.type)» «p.name»«ENDFOR»)«ENDIF»«ENDIF»«IF t.guard !== null»[«generateExpression(t.guard)»]«ENDIF»«IF withActions»«allActionsToUML(c)»«ELSE»«actionToUML(c)»«ENDIF»
					«ENDIF»
				«ELSE»
				«s.name» --> «c.target.name»«IF (t instanceof TriggeredTransition) || (t.guard !== null) || hasActionPart(c) || withActions»: «ENDIF»«IF t instanceof TriggeredTransition»«(t.trigger.eContainer as Signature).name»::«t.trigger.name»«IF t.parameters.size() > 0»(«FOR p : t.parameters SEPARATOR ', '»«typeToString(p.type)» «p.name»«ENDFOR»)«ENDIF»«ENDIF»«IF t.guard !== null»[«generateExpression(t.guard)»]«ENDIF»«IF withActions»«allActionsToUML(c)»«ELSE»«actionToUML(c)»«ENDIF»
			«ENDIF»
		«ENDFOR»
		«ENDFOR»
		«ENDFOR»
		
		@enduml
	'''

	def static dispatch typeToString(TypeReference tr) '''«tr.type.name»'''

	// TODO shall we print the dimension value?
	def static dispatch typeToString(VectorTypeConstructor tr) '''«tr.type.name»«FOR d : tr.dimensions»[]«ENDFOR»'''

	def allActionsToUML(
		Clause c) '''/«IF c.actions !== null»«FOR a : c.actions.actions»«generateAction(a)»«ENDFOR»«ELSE»-«ENDIF»'''

	def actionToUML(Clause c) {
		var CharSequence result
		var TreeIterator<EObject> iter;
		var EObject o;
		var boolean found
		var EventCall n
		var CommandReply r

		found = false;
		result = '''''';
		if (c.actions !== null) {
			iter = c.actions.eAllContents();
			while (iter.hasNext() && ! found) {
				o = iter.next();
				if (o instanceof EventCall) {
					n = o as EventCall
					result = '''/''' + (n.event.eContainer as Signature).name + '''::''' + n.event.name;
					if (n.parameters.size() > 0) {
						result = result + '''(«FOR p : n.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)'''
					}
					found = true;
				}
				if (o instanceof CommandReply) {
					r = o as CommandReply
					result = '''/reply''';
					if (r.parameters.size() > 0) {
						result = result + '''(«FOR p : r.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)'''
					}
					found = true;
				}
				if (o instanceof EventWithVars) {
                    result = '''/''' + (o.event.eContainer as Signature).name + '''::''' + o.event.name;
                    if (o.parameters.size() > 0) {
                        result = result + '''(«FOR p : o.parameters SEPARATOR ', '»«p.name»«ENDFOR»)«IF o.condition !== null» where («generateExpression(o.condition)»)«ENDIF»'''
                    }
                    found = true;
				}
				if (o instanceof CommandReplyWithVars) {
				    result = '''/reply«IF o.parameters.size() > 0»(«FOR p : o.parameters SEPARATOR ', '»«p.name»«ENDFOR»)«ENDIF»«IF o.condition !== null» where («generateExpression(o.condition)»)«ENDIF»'''
				}
			}
		} else {
			result = '''/-'''
		}
		result
	}

	def boolean hasActionPart(Clause c) {
		var boolean result = false
		var TreeIterator<EObject> iter;
		var EObject o;

		if (c.actions !== null) {
			iter = c.actions.eAllContents();
			while (iter.hasNext() && !result) {
				o = iter.next();
				if ((o instanceof EventCall) || (o instanceof CommandReply) || (o instanceof ActionWithVars)) {
					result = true;
				}
			}
		}
		result
	}
	
	def private getOutputURI(URI prjURI) {
		var rURI = prjURI
		if (rURI.isPlatform) {
			val IResource eclipseResource = ResourcesPlugin.workspace.root.findMember(
				rURI.toPlatformString(true))
			rURI =  URI.createFileURI(eclipseResource.rawLocation.toOSString);
		}
		
		var outputDir = getOutputConfiguration;
		if (outputDir === null) {
			throw new IllegalArgumentException("Output directory was not set.")
		}

		if (outputDir.isRelative) {
			outputDir = outputDir.resolve(rURI)
		}
		if (!outputDir.hasTrailingPathSeparator) {
			outputDir.appendSegment("")
		}
		
		return outputDir
	}

	def private getOutputConfiguration() {
		var access = fsa
		if (access instanceof CommaFileSystemAccess) {
			access = access.IFileSystemAccess
		}
		var tempPath = new Path(IFileSystemAccess.DEFAULT_OUTPUT)
		if (access instanceof AbstractFileSystemAccess) {
			val configs = access.outputConfigurations			
			if (!configs.isEmpty) {
				tempPath = new Path(configs.get(IFileSystemAccess.DEFAULT_OUTPUT).outputDirectory)
			}
		}
		val path = if (!tempPath.hasTrailingSeparator) {
				tempPath.addTrailingSeparator
			} else {
				tempPath
			}
		return URI.createFileURI(path.toString)
	}
}
