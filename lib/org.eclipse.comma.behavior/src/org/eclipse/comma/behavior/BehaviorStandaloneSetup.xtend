/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior

import org.eclipse.comma.actions.ActionsStandaloneSetup
import org.eclipse.comma.expressions.ExpressionStandaloneSetup
import org.eclipse.comma.signature.InterfaceSignatureStandaloneSetup
import org.eclipse.comma.types.TypesStandaloneSetup

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class BehaviorStandaloneSetup extends BehaviorStandaloneSetupGenerated {

	def static void doSetup() {
		new TypesStandaloneSetup().createInjectorAndDoEMFRegistration()
		new ExpressionStandaloneSetup().createInjectorAndDoEMFRegistration()
		new ActionsStandaloneSetup().createInjectorAndDoEMFRegistration()
		new InterfaceSignatureStandaloneSetup().createInjectorAndDoEMFRegistration()
		new BehaviorStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
