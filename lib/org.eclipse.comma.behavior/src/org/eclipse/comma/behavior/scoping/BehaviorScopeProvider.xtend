/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.scoping

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.actions.actions.Action
import org.eclipse.comma.actions.actions.ActionList
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.behavior.behavior.AbstractBehavior
import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.comma.behavior.behavior.DataConstraintsBlock
import org.eclipse.comma.behavior.behavior.EventInState
import org.eclipse.comma.behavior.behavior.GenericConstraintsBlock
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.behavior.behavior.TimeConstraintsBlock
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.comma.expressions.expression.ExpressionPackage
import org.eclipse.comma.expressions.expression.ExpressionQuantifier
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.signature.interfaceSignature.InterfaceEvent
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.signature.utilities.InterfaceUtilities
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScope

import static org.eclipse.xtext.scoping.Scopes.*
import org.eclipse.comma.expressions.expression.ExpressionMapRW

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class BehaviorScopeProvider extends AbstractBehaviorScopeProvider {
	override getScope(EObject context, EReference reference){
		if(context instanceof TriggeredTransition && reference == BehaviorPackage.Literals.TRIGGERED_TRANSITION__TRIGGER) {
			return scope_Transition_trigger(context as TriggeredTransition, reference)
		}
		if(context instanceof EventInState && reference == BehaviorPackage.Literals.EVENT_IN_STATE__STATE)
			return scope_Event_state(context as EventInState, reference)
		
		if(context instanceof ExpressionVariable && reference == ExpressionPackage.Literals.EXPRESSION_VARIABLE__VARIABLE)
			return scope_variable(context.eContainer)
			
		if(context instanceof AssignmentAction && reference == ActionsPackage.Literals.ASSIGNMENT_ACTION__ASSIGNMENT)
			return scope_variable(context.eContainer)
			
	    if(context instanceof EventWithVars && reference == ActionsPackage.Literals.EVENT_WITH_VARS__EVENT)
	       return scope_forEvent(context, null)
			
		return super.getScope(context, reference);
	}
	
	def IScope scope_variable(EObject context){
	    switch(context) {
	        ExpressionMapRW: {
	            if(context.context !== null) {
	                return scopeFor(List.of(context.context.iterator), scope_variable(context.eContainer))
	            }
	            else {
	                return scope_variable(context.eContainer)
	            }
	        }
	        ExpressionQuantifier: {
                return scopeFor(List.of(context.iterator), scope_variable(context.eContainer))
	        }
	        TriggeredTransition: {
                if(! context.parameters.empty){
                    return scopeFor(context.parameters, scope_variable(context.eContainer))
                }
                else
                    return scope_variable(context.eContainer)
	        }
	        Action: {
	            val container = context.eContainer
                var IScope parentScope = scope_variable(container)
                if(container instanceof ActionList) {
                    val index = container.actions.indexOf(context)
                    if(index > 0) {
                        parentScope = scope_variable(container.actions.get(index-1))
                    }
                }
                if(context instanceof ActionWithVars) {
	               return scopeFor(context.parameters, parentScope)        
	            }
	            return parentScope 
	        }
	        AbstractBehavior: return scopeFor(context.vars)
	        DataConstraintsBlock |
	        GenericConstraintsBlock: {
	            val IScope parentScope = scope_variable(context.eContainer)
	            return scopeFor(context.vars, parentScope)
	        }
	        TimeConstraintsBlock: return scope_variable(context.eContainer)
	        default: return scope_variable(context.eContainer) 
	    }
	}
	
	def scope_Transition_trigger(TriggeredTransition context, EReference ref){ 
		scope_forEvent(context, null)		
  	}
  	
	def scope_Event_state(EventInState context, EReference ref){
		var List<State> states = new ArrayList<State>();
		for(sm : (EcoreUtil2::getContainerOfType(context, AbstractBehavior) as AbstractBehavior).machines){
			states.addAll(sm.states)
		}
		return scopeFor(states)
	}
	
	def scope_forEvent(EObject context, Signature i){
		if(i !== null) return scopeFor(InterfaceUtilities::getAllInterfaceEvents(i))
		var List<Signature> interfaces = findVisibleInterfaces(context)
		var List<InterfaceEvent> events = new ArrayList<InterfaceEvent>
		for(i1 : interfaces) 
			events.addAll(InterfaceUtilities::getAllInterfaceEvents(i1));
		return scopeFor(events)
	}
}
