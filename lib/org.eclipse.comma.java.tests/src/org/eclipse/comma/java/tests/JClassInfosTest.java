/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.jar.JarEntry;

import org.junit.jupiter.api.Test;

import org.eclipse.comma.java.JClassInfo;
import org.eclipse.comma.java.JClassInfos;

public class JClassInfosTest {
	
	@Test
	public void testGetJarFileNameWithClassOnly() {
		MockFile f = new MockFile("D:/my-proj/src/MyClass.java");
		JClassInfo clsInfo = new MockClassInfo(f, "MyClass");
		
		String r = JClassInfos.getJarFileName(clsInfo);
		
		assertEquals("MyClass.class", r);
	}

	@Test
	public void testGetJarFileNameWith1FolderPackageAndClass() {
		MockFile f = new MockFile("D:/my-proj/src/my1/MyClass.java");
		JClassInfo clsInfo = new MockClassInfo(f, "my1.MyClass");
		
		String r = JClassInfos.getJarFileName(clsInfo);
		
		assertEquals("my1/MyClass.class", r);
	}

	@Test
	public void testGetJarFileNameWith2FolderAndClass() {
		MockFile f = new MockFile("D:/my-proj/src/my1/my2/MyClass.java");
		JClassInfo clsInfo = new MockClassInfo(f, "my1.m2.MyClass");
		
		String r = JClassInfos.getJarFileName(clsInfo);
		
		assertEquals("my1/m2/MyClass.class", r);
	}
	
	@Test
	public void testToJarEntryWithClassOnly() {
		MockFile f = new MockFile("D:/my-proj/src/MyClass.java").setLastModification(1234567);
		JClassInfo clsInfo = new MockClassInfo(f, "MyClass");
		
		JarEntry r = JClassInfos.toJarEntry(clsInfo);
		
		assertEquals("MyClass.class", r.getName());
		assertEquals(1234567, r.getTime());
	}
	

	@Test
	public void  testToJarEntryWith1FolderPackageAndClass() {
		MockFile f = new MockFile("D:/my-proj/src/my1/MyClass.java").setLastModification(1234567);
		JClassInfo clsInfo = new MockClassInfo(f, "my1.MyClass");
		
		JarEntry r = JClassInfos.toJarEntry(clsInfo);
		
		assertEquals("my1/MyClass.class", r.getName());
		assertEquals(1234567, r.getTime());
	}

	@Test
	public void  testToJarEntryNameWith2FolderAndClass() {
		MockFile f = new MockFile("D:/my-proj/src/my1/my2/MyClass.java").setLastModification(1234567);
		JClassInfo clsInfo = new MockClassInfo(f, "my1.m2.MyClass");
		
		JarEntry r = JClassInfos.toJarEntry(clsInfo);
		
		assertEquals("my1/m2/MyClass.class", r.getName());
		assertEquals(1234567, r.getTime());
	}
}
