/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import java.io.File;

import org.eclipse.comma.java.JSourceInfo;

/**
 * For testing purpose only and could be replaced if concrete mocking framework
 * is used.
 */
public class MockSourceInfo implements JSourceInfo {

	private File location;
	private String name;

	@Override
	public File getLocation() {
		return location;
	}

	@Override
	public String getName() {
		return name;
	}

	MockSourceInfo setLocation(File location) {
		this.location = location;
		return this;
	}

	MockSourceInfo setName(String name) {
		this.name = name;
		return this;
	}

}
