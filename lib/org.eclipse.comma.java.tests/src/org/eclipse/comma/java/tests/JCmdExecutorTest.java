/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.eclipse.comma.java.JArtifactInfo;
import org.eclipse.comma.java.JArtifactInfoBuilder;
import org.eclipse.comma.java.JCmd;
import org.eclipse.comma.java.JCmdBuilder;
import org.eclipse.comma.java.JCmdExecutor;
import org.eclipse.comma.java.JCompiler;
import org.eclipse.comma.java.JSourceInfos;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

public class JCmdExecutorTest {

	@Test
	public void testWith4120CharactersInStdout(@TempDir Path tmpDir) throws IOException, InterruptedException {
		// prepare test
		Path srcPath = Files.createDirectories(tmpDir.resolve("my"));
		Path srcMain = tmpDir.resolve("my/TestApp.java");
		Files.write(srcMain, Arrays.asList(
				"package my;",
				"  class TestApp {",
				"    static public void main(String[] args) {",
				"      for(int i=0;i<4120;i++) {System.out.print('x');}",
				"      System.out.print(\"\\nabc\");",
				"    }",
				"  }"
				));
		
		
		
		JCompiler c = new JCompiler()
						.withSourceInfos(JSourceInfos.getJavaSourcesFromSrcDirectory(srcPath.toFile()));
		
		JArtifactInfo artifactInfo = new JArtifactInfoBuilder()
				.withClassInfos(c.compile())
				.withArchiveFile(Files.createTempFile("JCmdExecutorTest4120Stdout", ".jar").toFile())
				.withMainClass("my.TestApp")
				.build();
		
		JCmd cmd = new JCmdBuilder().withArtifactInfo(artifactInfo).build();
		
		// execute test case
		var monitor = new JCmdExecutor(cmd).execute();
		monitor.process.waitFor();
		
		assertEquals(0, monitor.getStdErr().length());
		assertEquals("x".repeat(4120) + "\nabc", monitor.getStdOut());
	}
	
	@Test
	public void testWith4120CharactersInStderr(@TempDir Path tmpDir) throws IOException, InterruptedException {
		// prepare test
		Path srcPath = Files.createDirectories(tmpDir.resolve("my"));
		Path srcMain = tmpDir.resolve("my/TestApp.java");
		Files.write(srcMain, Arrays.asList(
				"package my;",
				"  class TestApp {",
				"    static public void main(String[] args) {",
				"      for(int i=0;i<4120;i++) System.err.print('x');",
				"    }",
				"  }"
				));
		
		JCompiler c = new JCompiler()
						.withSourceInfos(JSourceInfos.getJavaSourcesFromSrcDirectory(srcPath.toFile()));
		
		JArtifactInfo artifactInfo = new JArtifactInfoBuilder()
				.withClassInfos(c.compile())
				.withArchiveFile(Files.createTempFile("JCmdExecutorTest4120Stderr", ".jar").toFile())
				.withMainClass("my.TestApp")
				.build();
		
		JCmd cmd = new JCmdBuilder().withArtifactInfo(artifactInfo).build();
		
		// execute test case
		var monitor = new JCmdExecutor(cmd).execute();
		monitor.process.waitFor();
		
		assertEquals(0, monitor.getStdOut().length());
		assertEquals("x".repeat(4120), monitor.getStdErr());
	}
}
