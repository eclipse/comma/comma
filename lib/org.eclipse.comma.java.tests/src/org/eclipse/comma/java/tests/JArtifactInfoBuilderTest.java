/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.comma.java.JArtifactInfoBuilder;
import org.eclipse.comma.java.JCompiler;
import org.eclipse.comma.java.JDependencyInfos;
import org.eclipse.comma.java.JSourceInfos;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

public class JArtifactInfoBuilderTest {
	
	private HashMap<String, byte[]> readZip(File archive) {
		try {
			HashMap<String, byte[]> entries = new HashMap<>();
			ZipFile zipFile = new ZipFile(archive);
			Enumeration<? extends ZipEntry> enumerator = zipFile.entries();
	        while(enumerator.hasMoreElements()) {
	        	ZipEntry entry = enumerator.nextElement();
	        	byte[] stream = zipFile.getInputStream(entry).readAllBytes();
	        	entries.put(entry.getName(), stream);
	        }
	        zipFile.close();
	        return entries;
		} catch (Exception ex) {
			return null;
		}
	}

	@Test
	public void testJarCreation(@TempDir Path tmpDir) throws IOException, InterruptedException {
		Path srcPath = Files.createDirectories(tmpDir.resolve("my"));
		Path srcMain = tmpDir.resolve("my/TestApp.java");
		Files.write(srcMain, Arrays.asList(
				"package my;",
				"  class TestApp {",
				"    static public void main(String[] args) {",
				"    }",
				"  }"
				));
		
		JCompiler c = new JCompiler()
						.withSourceInfos(JSourceInfos.getJavaSourcesFromSrcDirectory(srcPath.toFile()));
		
		File archive = Files.createTempFile("JArtifactInfoBuilderTest", ".jar").toFile();
		var artifact = new JArtifactInfoBuilder()
				.withClassInfos(c.compile())
				.withArchiveFile(archive)
				.withMainClass("my.TestApp")
				.build();
		
		assertTrue(archive.exists());
		
		HashMap<String, byte[]> zipEntries = readZip(archive);
		assertEquals(new HashSet<>(Arrays.asList("my/TestApp.class", "META-INF/MANIFEST.MF")), zipEntries.keySet());
		String manifest = new String(zipEntries.get("META-INF/MANIFEST.MF")).replace("\r\n", "\n").trim();
		assertEquals("Manifest-Version: 1.0\nMain-Class: my.TestApp", manifest);
		assertEquals(artifact.getBytes().length, 534);
	}
	
	@Test
	public void testJarCreationWithIncludeDependencies(@TempDir Path tmpDir) throws IOException, InterruptedException {
		// Create the dependency JAR
		Path depPath = Files.createDirectories(tmpDir.resolve("mydependency"));
		Path depCode = tmpDir.resolve("mydependency/Adependency.java");
		Files.write(depCode, Arrays.asList(
				"package mydependency;",
				"  class Adependency {",
				"  }"
				));
		
		
		JCompiler cDep = new JCompiler()
				.withSourceInfos(JSourceInfos.getJavaSourcesFromSrcDirectory(depPath.toFile()));

		File dependencyArchive = Files.createTempFile("JArtifactInfoBuilderTestDependencies", ".jar").toFile();
		new JArtifactInfoBuilder()
				.withClassInfos(cDep.compile())
				.withIncludeDependencies()
				.withArchiveFile(dependencyArchive)
				.withMainClass("dummy")
				.build();
		
		// Create the src jar
		Path srcPath = Files.createDirectories(tmpDir.resolve("my"));
		Path srcMain = tmpDir.resolve("my/TestApp.java");
		Files.write(srcMain, Arrays.asList(
				"package my;",
				"  class TestApp {",
				"    static public void main(String[] args) {",
				"    }",
				"  }"
				));
		

		JCompiler c = new JCompiler()
						.withSourceInfos(JSourceInfos.getJavaSourcesFromSrcDirectory(srcPath.toFile()));
		
		File archive = Files.createTempFile("JArtifactInfoBuilderTestWithDependencies", ".jar").toFile();
		new JArtifactInfoBuilder()
				.withClassInfos(c.compile())
				.withDependencyInfos(JDependencyInfos.create(dependencyArchive))
				.withIncludeDependencies()
				.withArchiveFile(archive)
				.withMainClass("my.TestApp")
				.build();
		
		assertTrue(archive.exists());
		
		HashMap<String, byte[]> zipEntries = readZip(archive);
		assertEquals(new HashSet<>(Arrays.asList("my/TestApp.class", "mydependency/Adependency.class", "META-INF/MANIFEST.MF")), zipEntries.keySet());
		String manifest = new String(zipEntries.get("META-INF/MANIFEST.MF")).replace("\r\n", "\n").trim();
		assertEquals("Manifest-Version: 1.0\nMain-Class: my.TestApp", manifest);
	}
}
