/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.eclipse.comma.java.JCmd;
import org.eclipse.comma.java.JCmdBuilder;

public class JCmdBuilderTest {
	
	class MockJCmdBuilder extends JCmdBuilder {
		MockJCmdBuilder(List<String>options, List<String>arguments) {
			super(options != null ? options : new ArrayList<>(), arguments != null ? arguments : new ArrayList<>());
		}
	}

	@Test
	public void testWithOptionsWithOneEntry() {
		List<String>options = new ArrayList<>();
		JCmdBuilder builder = new MockJCmdBuilder(options, null);
		
		JCmdBuilder r = builder.withOptions("-Dtest1");
		
		assertEquals(builder, r);
		assertEquals(1, options.size());
		assertEquals("-Dtest1", options.get(0));
	}

	@Test
	public void testWithOptionsWithTwoEntries() {
		List<String>options = new ArrayList<>();
		JCmdBuilder builder = new MockJCmdBuilder(options, null);
		
		JCmdBuilder r = builder.withOptions("-Dtest1", "-Dtest2");
		
		assertEquals(builder, r);
		assertEquals(2, options.size());
		assertEquals("-Dtest1", options.get(0));
		assertEquals("-Dtest2", options.get(1));
	}

	@Test
	public void testWithOptionsWithThreeEntryiesAndOneNull() {
		List<String>options = new ArrayList<>();
		JCmdBuilder builder = new MockJCmdBuilder(options, null);
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			builder.withOptions("-Dtest1", "-Dtest2", null);
		});
	}
	
	@Test
	public void testWithArgumentsWithOneEntry() {
		List<String>arguments = new ArrayList<>();
		JCmdBuilder builder = new MockJCmdBuilder(null, arguments);

		JCmdBuilder r = builder.withArguments("arg1");
		
		assertEquals(builder, r);
		assertEquals(1, arguments.size());
		assertEquals("arg1", arguments.get(0));
	}

	@Test
	public void testWithArgumentsWithTwoEntries() {
		List<String>arguments = new ArrayList<>();
		JCmdBuilder builder = new MockJCmdBuilder(null, arguments);

		JCmdBuilder r = builder.withArguments("arg1", "arg2");
		
		assertEquals(builder, r);
		assertEquals(2, arguments.size());
		assertEquals("arg1", arguments.get(0));
		assertEquals("arg2", arguments.get(1));
	}

	@Test
	public void testWithArgumentsWithThreeEntryiesAndOneNull() {
		List<String>arguments = new ArrayList<>();
		JCmdBuilder builder = new MockJCmdBuilder(null, arguments);
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			builder.withArguments("arg1", "arg2", null);
		});
	}
	
	@Test
	public void testBuildWith0OptionAnd0Argument() {
		MockArtifactInfo artifactInfo = new MockArtifactInfo()
				.setLocation(new File("/tmp/my.jar"))
				.setMainClass("my.main.App")
				.setDependencyInfos(Collections.emptyList());
		
		JCmd r = new JCmdBuilder()
				.withArtifactInfo(artifactInfo)
				.withJavaExec("javaExec")
				.build();
		
		assertEquals(4, r.getItems().size());
		assertEquals("javaExec", r.getItems().get(0));
		assertEquals("-classpath", r.getItems().get(1));
		assertEquals(new File("/tmp/my.jar").getAbsolutePath(), r.getItems().get(2));
		assertEquals("my.main.App",  r.getItems().get(3));
	}
	
	@Test
	public void testBuildWith1OptionAnd1Argument() {
		MockArtifactInfo artifactInfo = new MockArtifactInfo()
				.setLocation(new File("/tmp/my.jar"))
				.setMainClass("my.main.App")
				.setDependencyInfos(Collections.emptyList());
		
		JCmd r = new JCmdBuilder()
				.withArguments("arg1")
				.withOptions("-Do1")
				.withArtifactInfo(artifactInfo)
				.withJavaExec("javaExec")
				.build();
		
		assertEquals(6, r.getItems().size());
		assertEquals("javaExec", r.getItems().get(0));
		assertEquals("-classpath", r.getItems().get(1));
		assertEquals(new File("/tmp/my.jar").getAbsolutePath(), r.getItems().get(2));
		assertEquals("-Do1",  r.getItems().get(3));
		assertEquals("my.main.App",  r.getItems().get(4));
		assertEquals("arg1",  r.getItems().get(5));		
	}
	
	@Test
	public void testBuildWith2OptionAnd3Argument() {
		MockArtifactInfo artifactInfo = new MockArtifactInfo()
				.setLocation(new File("/tmp/my.jar"))
				.setMainClass("my.main.App")
				.setDependencyInfos(Collections.emptyList());
		
		JCmd r = new JCmdBuilder()
				.withArguments("arg1")
				.withArguments("arg2")
				.withArguments("arg3")
				.withOptions("-Do1")
				.withOptions("-Do2")
				.withArtifactInfo(artifactInfo)
				.withJavaExec("javaExec")
				.build();
		
		assertEquals(9, r.getItems().size());
		assertEquals("javaExec", r.getItems().get(0));
		assertEquals("-classpath", r.getItems().get(1));
		assertEquals(new File("/tmp/my.jar").getAbsolutePath(), r.getItems().get(2));
		assertEquals("-Do1", r.getItems().get(3));
		assertEquals("-Do2", r.getItems().get(4));
		assertEquals("my.main.App", r.getItems().get(5));
		assertEquals("arg1", r.getItems().get(6));
		assertEquals("arg2", r.getItems().get(7));
		assertEquals("arg3", r.getItems().get(8));
	}
	
	@Test
	public void testBuildWithNoJavaExec() {
		MockArtifactInfo artifactInfo = new MockArtifactInfo()
				.setLocation(new File("/tmp/my.jar"))
				.setMainClass("my.main.App")
				.setDependencyInfos(Collections.emptyList());
		String javaExec = new File(System.getProperty("java.home"), "bin/java").getAbsolutePath();

		JCmd r = new JCmdBuilder()
				.withArtifactInfo(artifactInfo)
				.build();
		
		assertEquals(4, r.getItems().size());
		assertEquals(javaExec, r.getItems().get(0));
		assertEquals("-classpath", r.getItems().get(1));
		assertEquals(new File("/tmp/my.jar").getAbsolutePath(), r.getItems().get(2));
		assertEquals("my.main.App", r.getItems().get(3));
	}
	
	@Test
	public void testBuildWith1Dependency() {
		MockArtifactInfo artifactInfo = new MockArtifactInfo()
				.setLocation(new File("/tmp/my.jar"))
				.setMainClass("my.main.App")
				.setDependencyInfos(Arrays.asList(new MockDependencyInfo("/tmp/my.lib.jar")));
		String pathSeparator = System.getProperty("path.separator");
		
		JCmd r = new JCmdBuilder()
				.withArguments("arg1")
				.withOptions("-Do1")
				.withArtifactInfo(artifactInfo)
				.withJavaExec("javaExec")
				.build();
		
		assertEquals(6, r.getItems().size());
		assertEquals("javaExec", r.getItems().get(0));
		assertEquals("-classpath", r.getItems().get(1));
		String clsPath = String.format("%s%s%s", new File("/tmp/my.jar").getAbsolutePath(), pathSeparator,
				new File("/tmp/my.lib.jar").getAbsolutePath());
		assertEquals(clsPath, r.getItems().get(2));
		assertEquals("-Do1",  r.getItems().get(3));
		assertEquals("my.main.App",  r.getItems().get(4));
		assertEquals("arg1",  r.getItems().get(5));		
	}
}
