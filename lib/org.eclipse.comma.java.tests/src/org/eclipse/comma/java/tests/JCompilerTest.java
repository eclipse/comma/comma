/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.eclipse.comma.java.JClassInfo;
import org.eclipse.comma.java.JCompiler;
import org.eclipse.comma.java.JDependencyInfo;
import org.eclipse.comma.java.JSourceInfo;

public class JCompilerTest {

	class MockCompilerBuilder extends JCompiler {
		MockCompilerBuilder(List<JSourceInfo> sourceInfos, List<JDependencyInfo> dependencyInfos,
				List<String> options) {
			super(sourceInfos != null ? sourceInfos : new ArrayList<>(),
					dependencyInfos != null ? dependencyInfos : new ArrayList<>(),
					options != null ? options : new ArrayList<>());
		}
	}

	@Test
	public void testWithSourceInfosWith1Entry() {
		List<JSourceInfo> mockSourceInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(mockSourceInfos, null, null);
		List<JSourceInfo> sourceInfos = Arrays
				.asList(new MockSourceInfo().setLocation(new MockFile("/tmp/my/MyApp.java").setIsFile(true)));

		JCompiler r = c.withSourceInfos(sourceInfos);

		assertEquals(c, r);
		assertEquals(1, mockSourceInfos.size());
		assertEquals(new MockFile("/tmp/my/MyApp.java").setIsFile(true), mockSourceInfos.get(0).getLocation());
	}

	@Test
	public void testWithSourceInfosWith2Entry() {
		List<JSourceInfo> mockSourceInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(mockSourceInfos, null, null);
		List<JSourceInfo> sourceInfos = Arrays.asList(
				new MockSourceInfo().setLocation(new MockFile("/tmp/my/MyApp.java").setIsFile(true)),
				new MockSourceInfo().setLocation(new MockFile("/tmp/my/printer/MyPrinter.java").setIsFile(true)));

		JCompiler r = c.withSourceInfos(sourceInfos);

		assertEquals(c, r);
		assertEquals(2, mockSourceInfos.size());
		assertEquals(new MockFile("/tmp/my/MyApp.java").setIsFile(true), mockSourceInfos.get(0).getLocation());
		assertEquals(new MockFile("/tmp/my/printer/MyPrinter.java").setIsFile(true),
				mockSourceInfos.get(1).getLocation());
	}

	@Test
	public void testWithSourceInfosWithNoJavaSource() {
		List<JSourceInfo> mockSourceInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(mockSourceInfos, null, null);
		List<JSourceInfo> sourceInfos = Arrays
				.asList(new MockSourceInfo().setLocation(new MockFile("/tmp/my/logo.png").setIsFile(true)));

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			c.withSourceInfos(sourceInfos);
		});
	}
	
	@Test
	public void testWithSourceInfosWithDirectory() {
		List<JSourceInfo> mockSourceInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(mockSourceInfos, null, null);
		List<JSourceInfo> sourceInfos = Arrays
				.asList(new MockSourceInfo().setLocation(new MockFile("/tmp/my").setIsFile(false)));

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			c.withSourceInfos(sourceInfos);
		});
	}

	@Test
	public void testWithSourceInfosWithNull() {
		List<JSourceInfo>mockSourceInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(mockSourceInfos, null, null);
		List<JSourceInfo>sourceInfos = null;;
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			c.withSourceInfos(sourceInfos);	
		});
		
	}
	
	@Test
	public void testWithDependencyInfosWith1Entry() {
		List<JDependencyInfo> mockDependencyInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(null, mockDependencyInfos, null);
		List<JDependencyInfo> DependencyInfos = Arrays
				.asList(new MockDependencyInfo(new MockFile("/tmp/my/lib/lib1.jar").setIsFile(true)));

		JCompiler r = c.withDependencyInfos(DependencyInfos);

		assertEquals(c, r);
		assertEquals(1, mockDependencyInfos.size());
		assertEquals(new MockFile("/tmp/my/lib/lib1.jar").setIsFile(true), mockDependencyInfos.get(0).getLocation());
	}

	@Test
	public void testWithDependencyInfosWith2Entry() {
		List<JDependencyInfo> mockDependencyInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(null, mockDependencyInfos, null);
		List<JDependencyInfo> DependencyInfos = Arrays.asList(
				new MockDependencyInfo(new MockFile("/tmp/my/lib/lib1.jar").setIsFile(true)),
				new MockDependencyInfo(new MockFile("/tmp/my/lib/lib2.jar").setIsFile(true)));

		JCompiler r = c.withDependencyInfos(DependencyInfos);

		assertEquals(c, r);
		assertEquals(2, mockDependencyInfos.size());
		assertEquals(new MockFile("/tmp/my/lib/lib1.jar").setIsFile(true), mockDependencyInfos.get(0).getLocation());
		assertEquals(new MockFile("/tmp/my/lib/lib2.jar").setIsFile(true), mockDependencyInfos.get(1).getLocation());
	}
	
	@Test
	public void testWithDependencyInfosWithDirectory() {
		List<JDependencyInfo> mockDependencyInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(null, mockDependencyInfos, null);
		List<JDependencyInfo> DependencyInfos = Arrays
				.asList(new MockDependencyInfo(new MockFile("/tmp/my").setIsFile(false)));

		JCompiler r = c.withDependencyInfos(DependencyInfos);
		assertEquals(c, r);
		assertEquals(1, mockDependencyInfos.size());
		assertEquals(new MockFile("/tmp/my/").setIsFile(false), mockDependencyInfos.get(0).getLocation());
	}

	@Test
	public void testWithDependencyInfosWithNull() {
		List<JDependencyInfo>mockDependencyInfos = new ArrayList<>();
		JCompiler c = new MockCompilerBuilder(null, mockDependencyInfos, null);
		List<JDependencyInfo>DependencyInfos = null;;
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			c.withDependencyInfos(DependencyInfos);	
		});
		
	}
	
	@Test
	public void testBuildWithOneClass() throws IOException {
		Path tmpDir = Files.createTempDirectory("JCompilerTest-");
		Files.createDirectories(tmpDir.resolve("my"));
		Path srcApp = tmpDir.resolve("my/App.java");
		Files.write(srcApp, Arrays.asList(
				"package my;",
				"  class App {",
				"  }"
				));
		
		List<JClassInfo>classInfos= new JCompiler()
				.withSourceInfos(new MockSourceInfo().setLocation(srcApp.toFile()).setName("my.App"))
				.compile();
		
		assertEquals(1, classInfos.size());
		assertEquals("App.class", classInfos.get(0).getLocation().getName());
	}
	
	@Test
	public void testBuildWithOneInnerClass() throws IOException {
		Path tmpDir = Files.createTempDirectory("JCompilerTest-");
		Files.createDirectories(tmpDir.resolve("my"));
		Path srcApp = tmpDir.resolve("my/App.java");
		Files.write(srcApp, Arrays.asList(
				"package my;",
				"  class App {",
				"    class MyInner1 {}",
				"  }"
				));
		
		List<JClassInfo>classInfos= new JCompiler()
				.withSourceInfos(new MockSourceInfo().setLocation(srcApp.toFile()).setName("my.App"))
				.compile();
		
		sortByLocation(classInfos);
		assertEquals(2, classInfos.size());
		assertEquals("App$MyInner1.class", classInfos.get(0).getLocation().getName());
		assertEquals("App.class", classInfos.get(1).getLocation().getName());
	}
	
	@Test
	public void testBuildWith2Classes() throws IOException {
		Path tmpDir = Files.createTempDirectory("JCompilerTest-");
		Files.createDirectories(tmpDir.resolve("my"));
		Path srcApp = tmpDir.resolve("my/App.java");
		Files.write(srcApp, Arrays.asList(
				"package my;",
				"import my.Printer;",
				"  class App {",
				"    public void init() {",
				"      Printer p = new Printer();",
				"      p.print(\"Hallo World!\");",
				"    }",
				"  }"
				));
		Path srcPrinter = tmpDir.resolve("my/Printer.java");
		Files.write(srcPrinter, Arrays.asList(
				"package my;",
				"  class Printer {",
				"    public void print(String val) {",
				"      System.out.println(val);",
				"    }",
				"  }"
				));
		
		List<JClassInfo>classInfos= new JCompiler()
				.withSourceInfos(new MockSourceInfo().setLocation(srcApp.toFile()).setName("my.App"))
				.withSourceInfos(new MockSourceInfo().setLocation(srcPrinter.toFile()).setName("my.Printer"))
				.compile();
		
		sortByLocation(classInfos);
		assertEquals(2, classInfos.size());
		assertEquals("App.class", classInfos.get(0).getLocation().getName());
		assertEquals("Printer.class", classInfos.get(1).getLocation().getName());
	}	
	static void sortByLocation(List<JClassInfo>classInfos) {
		Collections.sort(classInfos, new Comparator<JClassInfo>() {

			@Override
			public int compare(JClassInfo ci1, JClassInfo ci2) {
				return ci1.getLocation().getAbsoluteFile().compareTo(ci2.getLocation().getAbsoluteFile());
			}

		});
	}
}
