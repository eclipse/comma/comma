/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import java.io.File;

import org.eclipse.comma.java.JClassInfo;

/**
 * For testing purpose only and could be replaced if concrete mocking framework
 * is used.
 */
public class MockClassInfo implements JClassInfo {

	private final File location;
	private final String name;

	MockClassInfo(File location, String name) {
		this.location = location;
		this.name = name;
	}

	@Override
	public File getLocation() {
		return location;
	}

	@Override
	public String getName() {
		return name;
	}

}
