/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import java.io.File;

/**
 * For testing purpose only and could be replaced if concrete mocking framework
 * is used.
 */
@SuppressWarnings("serial")
public class MockFile extends File {

	private boolean isFile;
	private long lastModification;

	public MockFile(String pathname) {
		super(pathname);
		this.lastModification = super.lastModified();
	}

	@Override
	public boolean isFile() {
		return isFile;
	}

	@Override
	public boolean isDirectory() {
		return !isFile();
	}

	@Override
	public long lastModified() {
		return lastModification;
	}

	public MockFile setIsFile(boolean isFile) {
		this.isFile = isFile;
		return this;
	}

	public MockFile setLastModification(long lastModification) {
		this.lastModification = lastModification;
		return this;
	}
}
