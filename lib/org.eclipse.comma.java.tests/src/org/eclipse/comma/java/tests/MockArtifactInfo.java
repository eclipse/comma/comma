/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.java.tests;

import java.io.File;
import java.util.List;

import org.eclipse.comma.java.JArtifactInfo;
import org.eclipse.comma.java.JDependencyInfo;

/**
 * For testing purpose only and could be replaced if concrete mocking framework
 * is used.
 */
public class MockArtifactInfo implements JArtifactInfo {

	private byte[] bytes;
	private File location;
	private List<JDependencyInfo> dependencyInfos;
	private String mainClass;
	private boolean includesDependencies;

	@Override
	public File getLocation() {
		return location;
	}

	@Override
	public List<JDependencyInfo> getDependencyInfos() {
		return dependencyInfos;
	}

	@Override
	public String getMainClass() {
		return mainClass;
	}
	
	@Override
	public byte[] getBytes() {
		return bytes;
	}

	public MockArtifactInfo setLocation(File location) {
		this.location = location;
		return this;
	}

	public MockArtifactInfo setDependencyInfos(List<JDependencyInfo> dependencyInfos) {
		this.dependencyInfos = dependencyInfos;
		return this;
	}

	public MockArtifactInfo setMainClass(String mainClass) {
		this.mainClass = mainClass;
		return this;
	}
	
	public MockArtifactInfo setIncludesDependencies(boolean includesDependencies) {
		this.includesDependencies = includesDependencies;
		return this;
	}

	@Override
	public boolean getIncludesDependencies() {
		return this.includesDependencies;
	}

}
