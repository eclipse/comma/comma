/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.validation.tests

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignatureDefinition
import org.eclipse.comma.signature.tests.InterfaceSignatureInjectorProvider
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.util.StringInputStream

import static org.eclipse.comma.signature.interfaceSignature.InterfaceSignaturePackage.Literals.*
import static org.eclipse.comma.types.types.TypesPackage.Literals.*
import org.eclipse.emf.ecore.resource.Resource
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceSignatureInjectorProvider)

class ValidationTest {

	@Inject extension ParseHelper<InterfaceSignatureDefinition> parseHelper
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider

	@Test
	def testVoidOk() {
		val model = parseHelper.parse('''
			signature ICamera
				types
				enum Status {OnOK OnFailed}

				commands
				void command

				notifications
				dummy(Status s)
			''')
		assertNoIssues(model)
	}

	@Test
	def testNoIssues() {
		val model = parseHelper.parse('''
			signature ICamera
							types
								enum Status {OnOK OnFailed}
							commands

								Status command

							notifications

								dummy(Status s)
		''')
		assertNoIssues(model)
	}

	@Test
	def unknownCommandReference() {
		val model = '''
			signature ICamera
				types
					enum Status {OnOK OnFailed}
				commands
					c   Click

				notifications
					CameraStatus(Status s)
		'''.parse

		assertError(model, TYPE_REFERENCE, "org.eclipse.xtext.diagnostics.Diagnostic.Linking","Couldn't resolve reference to TypeDecl")
		}

	@Test
	def checkParameterDirection(){
		val set = resourceSetProvider.get()
		val model = parse('''
			signature IVendingMachineTest
				commands
					void randomCommand(inout int x, in int y, out bool z)
				signals
					randomSignal(out int a)
		''',set)
		assertError(model,INTERFACE_EVENT, null, "Direction of parameters in notifications and signals must be IN")
	}

	@Test
	def checkUniqueParameterNames(){
		val set = resourceSetProvider.get()
		val model = parse('''
			signature IVendingMachineTest
				commands
					void randomCommand(inout int x, in int x, out bool z)
				signals
					randomSignal(int a)
		''',set)
		assertError(model,PARAMETER, null, "Duplicate parameter name")
	}

	@Test
	def checkUniqueEventNames(){
		val set = resourceSetProvider.get()
		val model = parse('''
			signature IVendingMachineTest
				commands
					void randomEvent(inout int x, in int y, out bool z)
				signals
					randomEvent(int a)
		''',set)
		assertError(model,INTERFACE_EVENT, null, "Duplicate interface event name")
	}

	@Test
	def checkUniqueLocalTypesNames(){
		val set = resourceSetProvider.get()
		val model = parse('''
			signature IVendingMachineTest
				types
				type myType
				enum myType{A B}
				commands
					void randomEvent(inout int x, in int y, out bool z)
				signals
					randomSignal(int a)
		''',set)
		assertError(model,NAMED_ELEMENT, null, "Duplicate type declaration name")
	}

	@Test
	def checkDuplicationOfNamesOfTypesEvents(){
		val set = resourceSetProvider.get()
		val model = parse('''
			signature IVendingMachineTest
				commands
					void randomEvent(inout int x, in int y, out bool z)
				signals
					real(int a)
		''',set)
		assertError(model,INTERFACE_EVENT, null, "Interface event duplicates type name")
	}

	@Test
	def checkLocalTypeOverridesImportedType(){
		val set = resourceSetProvider.get()
		importTypes(set)
		val model = parse('''
			import "shared.types"

			signature IVendingMachineTest
				types
							type myType
							commands
								void randomEvent(inout int x, in int y, out bool z)
							signals
								randomSignal(int a)
		''',set)
		assertNoIssues(model)
	}

	@Test
	def checkInheritedValidationFromTypes(){
		val set = resourceSetProvider.get()
		importTypes1(set)
		importTypes2(set)
		val model = parse('''
			import "shared1.types"
			import "shared2.types"

			signature IVendingMachineTest
							commands
								void randomEvent(inout int x, in int y, out bool z)
							signals
								randomSignal(int a)
		''',set)
		assertError(model,SIGNATURE, null, "Duplicate imported type myType")
	}

	@Test
	def checkSignatureFileNameSameAsImportedTypeFile(){
		val set = resourceSetProvider.get()
		importTypes1(set)
		val model = importSignature(set)
		assertError(model,IMPORT, null, "Imported type file should have a different name than the current signature file.")
	}

	@Test
	def checkNotAllowedUsageOfVoidAny(){
		val set = resourceSetProvider.get()
		val model = parse('''
			signature IVendingMachineTest
				commands
					void randomEvent(inout void x, in any y, out bool z)
				signals
					s(int a)
		''',set)
		assertError(model,TYPE, null, "Usage of type void is not allowed")
		assertError(model,TYPE, null, "Usage of type any is not allowed")
	}

	def importTypes(ResourceSet set){
		val res = set.createResource(URI.createURI("shared.types"));
		res.load(new StringInputStream("type myType
				"), emptyMap);
	}

	def importTypes1(ResourceSet set){
		val res = set.createResource(URI.createURI("shared1.types"));
		res.load(new StringInputStream("type myType
				"), emptyMap);
	}

	def importTypes2(ResourceSet set){
		val res = set.createResource(URI.createURI("shared2.types"));
		res.load(new StringInputStream("type myType
				"), emptyMap);
	}

	def Resource importSignature(ResourceSet set){
		val res = set.createResource(URI.createURI("shared1.signature"));
		res.load(new StringInputStream("import \"shared1.types\"
			                signature IVendingMachineTest
							commands
								void randomEvent(inout int x, in int y, out bool z)
							signals
								randomSignal(int a)
				"), emptyMap);
		res
	}
}
