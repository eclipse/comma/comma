/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.eclipse.comma.signature.tests.InterfaceSignatureInjectorProvider
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceSignatureInjectorProvider)
class SignatureFormatterTest {

	@Inject extension FormatterTestHelper

	@Test def void formatExpressions() {
		assertFormatted[
			expectation = '''
				signature ISuSd
					types
					enum State {
						SystemOn
						VideoOn
						SystemOff
					}

					commands
					State GetState

					notifications
					StateUpdate(State state)
			'''
			toBeFormatted = '''

					signature	 ISuSd								types

					enum   State   {				SystemOn			VideoOn

						SystemOff


					}

					commands		State    GetState			notifications


					StateUpdate ( State  state  )
			'''
		]
	}
}
