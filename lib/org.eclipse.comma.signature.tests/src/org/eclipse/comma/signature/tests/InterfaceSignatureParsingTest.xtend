/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.tests

import com.google.inject.Inject
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignatureDefinition
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions

@ExtendWith(InjectionExtension)
@InjectWith(InterfaceSignatureInjectorProvider)
class InterfaceSignatureParsingTest {

	@Inject	ParseHelper<InterfaceSignatureDefinition> parseHelper

	@Test
	def void loadModel() {
		val result = '''
			signature ICamera
				types
				/* Users can define their own enumeration types. The language provides a set of predefined primitive types:
				 * int, bool, string, real, void.
				 * User defined enumeration type Status defines two literals that indicate the success or failure of an operation.
				 */
					enum Status {OnOK OnFailed}

				commands
					/*Turns on the camera. Returns a value of type Status.*/
					Status PowerOn

					/*Turns off the camera. */
					void   PowerOff

					/*Takes a picture */
					void   Click

					/*Returns the number of the last taken picture. */
					int    GetPictureNumber

				notifications
					/*Indicates change of status of the camera. */
					CameraStatus(Status s)

					/*Indicates that the camera is running on a low battery. */
					LowBattery

					/*Indicates that the battery is depleted and the camera will turn off. */
					EmptyBattery

					/*Indicates that a picture has been successfully taken. */
					PictureTaken
		'''.parse

		Assertions.assertNotNull(result)
		Assertions.assertTrue(result.eResource.errors.isEmpty)
	}

		@Test
	def void loadModelContents() {
		val result = '''
			signature ICamera
				types
					enum Status {OnOK OnFailed}

				commands

					Status PowerOn

				notifications

					PictureTaken
		'''.parse

		val iCamera = result.signature;

		Assertions.assertEquals("Status",iCamera.types.get(0).name)
		Assertions.assertEquals("PowerOn",iCamera.commands.get(0).name)
		Assertions.assertEquals("PictureTaken",iCamera.notifications.get(0).name)
	}

	@Test
	def void doubleCommand() {
		val model = '''
			signature ICamera
				types
					enum Status {OnOK OnFailed}
				commands
					void   Click
					void   Click
				notifications
					CameraStatus(Status s)
		'''.parse

		val commands = model.signature.commands
	  	Assertions.assertNotNull(commands.get(0))
		Assertions.assertNotNull(commands.get(1))
		Assertions.assertEquals("Click",commands.get(0).name)
		Assertions.assertEquals("Click",commands.get(1).name)
	}

	def protected parse(CharSequence modelAsText) {
		parseHelper.parse(modelAsText)
	}
}
