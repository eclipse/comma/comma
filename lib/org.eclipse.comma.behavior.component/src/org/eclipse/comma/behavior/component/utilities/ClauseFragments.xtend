/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.comma.expressions.expression.Variable

class ClauseFragments {
    TransitionFragment root = null
    List<FunctionFragment> functionFragments = new ArrayList<FunctionFragment>
    List<TransitionFragment> subTransitions = new ArrayList<TransitionFragment>
    Map<Variable, String> triggerVariables = new HashMap<Variable, String>
    String machineName
    
    def addRootFragment(TransitionFragment fragment) {
        root = fragment
    }
    
    def getRootFragment() {
        return root
    }
    
    def addFunctionFragment(FunctionFragment f) {
        functionFragments.add(f)
    }
    
    def List<FunctionFragment> getFunctionFragments() {
        return functionFragments
    }
    
    def addSubTransitionFragment(TransitionFragment f) {
        subTransitions.add(f)
    }
    
    def List<TransitionFragment> getSubTransitionFragments() {
        return subTransitions
    }
    
    def setMachineName(String name) {
        machineName = name
    }
    
    def setMachineInLastAction() {
        for(fragment : all){
            val lastAction = fragment.getActions.last
            if(lastAction instanceof StateTransitionAction) {
                lastAction.machineName =  machineName
            }
        }
    }
    
    def getMachineName() {
        return machineName
    }
    
    def setMethodNames(String rootName) {
        val fragments = all
        for(i : 0..< fragments.size) {
            if(i == 0)
                fragments.get(i).methodName = rootName
            else
                fragments.get(i).methodName = rootName + "_" + i
        }
    }
    
    def addVariable(Variable v) {
        triggerVariables.put(v, "")
    }
    
    def getTriggerVariables() {
        triggerVariables
    }
    
    def all() { //TODO think of better solution
        val result = new ArrayList<BehaviorFragment>
        result.add(root)
        result.addAll(functionFragments)
        result.addAll(subTransitions)
        result
    }
}
