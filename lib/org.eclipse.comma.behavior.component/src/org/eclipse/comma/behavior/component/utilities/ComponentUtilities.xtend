/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

import java.util.ArrayList
import java.util.HashSet
import java.util.List
import org.eclipse.comma.actions.actions.EVENT_KIND
import org.eclipse.comma.actions.actions.EventPattern
import org.eclipse.comma.actions.actions.InterfaceEventInstance
import org.eclipse.comma.actions.actions.ParameterizedEvent
import org.eclipse.comma.behavior.behavior.Port
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.behavior.component.component.AnyEvent
import org.eclipse.comma.behavior.component.component.CommandEvent
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.ComponentFactory
import org.eclipse.comma.behavior.component.component.ComponentPackage
import org.eclipse.comma.behavior.component.component.ComponentPart
import org.eclipse.comma.behavior.component.component.Connection
import org.eclipse.comma.behavior.component.component.NotificationEvent
import org.eclipse.comma.behavior.component.component.PortReference
import org.eclipse.comma.behavior.component.component.PortSelector
import org.eclipse.comma.behavior.component.component.SignalEvent
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.expressions.expression.Expression
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.signature.interfaceSignature.InterfaceEvent
import org.eclipse.comma.signature.interfaceSignature.Notification
import org.eclipse.comma.signature.interfaceSignature.Signal
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.utilities.CommaUtilities
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScopeProvider

class ComponentUtilities {
	
	def static List<Signature> getAllUsedSignatures(Component c){
		val result = new HashSet<Signature>
		for(p : c.ports){
			result.add(p.interface)
		}
		result.toList
	}
	
	def static List<State> getInterfaceStatesForPort(Port p, IScopeProvider scopeProvider){
        val signature = p?.interface
        val signatureName = signature?.name
        if(signatureName === null) return new ArrayList<State>()
        val cInterfaces = p.getAllInterfaces(scopeProvider)
        val interface = cInterfaces.findFirst[name == signatureName]
        return interface.machines.map[states].flatten.toList
    }
	
	def static List<Interface> getAllInterfaces(EObject context, IScopeProvider scopeProvider){
		val cInterfaces = scopeProvider.getScope(context, ComponentPackage.Literals.DUMMY__INTERFACE).allElements
		CommaUtilities.resolveProxy(context, cInterfaces)
	}
	
	def static List<Component> getAllComponents(EObject context, IScopeProvider scopeProvider){
		val cComponents = scopeProvider.getScope(context, ComponentPackage.Literals.COMPONENT_MODEL__COMPONENT).allElements
		CommaUtilities.resolveProxy(context, cComponents)
	}
	
	def static dispatch EVENT_KIND getKind(AnyEvent ev){
		ev.kind
	}
	
	def static dispatch EVENT_KIND getKind(CommandEvent ev){
		EVENT_KIND::CALL
	}
	
	def static dispatch EVENT_KIND getKind(SignalEvent ev){
		EVENT_KIND::SIGNAL
	}
	
	def static dispatch EVENT_KIND getKind(NotificationEvent ev){
		EVENT_KIND::NOTIFICATION
	}
	
	def static dispatch EVENT_KIND getKind(CommandReply ev){
		null
	}
	
	def static boolean signatureProvidedByManyPorts(Signature s, EObject o){
		val parent = EcoreUtil2::getContainerOfType(o, Component)
		(parent as Component).ports.filter(p | p.interface == s).size > 1
	}
	
	def static Port determinePort(EventPattern ev){
		(ev as PortSelector).port
	}
	
	def static ComponentPart determinePart(EventPattern ev){
        (ev as PortSelector).part
    }
	
	//Checks if the set of events matched by ev1 is a subset of events matched by ev2
	def static boolean isSubsumedBy(EventPattern ev1, EventPattern ev2){
	    if(ev1.determinePart != ev2.determinePart) return false
		if(ev1.determinePort != ev2.determinePort) return false
		if(ev2 instanceof AnyEvent){
			return ev2.kind == EVENT_KIND::EVENT || ev1.kind == ev2.kind
		}
		//ev2 is a concrete event
		if(ev1 instanceof AnyEvent){
			return false
		}
		if(ev2 instanceof CommandReply){
			if(ev1 instanceof CommandReply){
				return (ev2.command === null) ||
					   (ev2.command.event == ev1.command?.event)
			}
			return false
		}
		if(ev1 instanceof InterfaceEventInstance)
			return ev1.event == (ev2 as InterfaceEventInstance).event
			
		return false
	}
	
	def static getParameters(EventPattern e){
		if(e instanceof ParameterizedEvent){
			return e.parameters
		}
		else{
			new ArrayList<Expression>
		}
	}
	
	def static State getInitialState(StateBasedFunctionalConstraint fc){
		val initialStates = fc.states.filter[initial]
		if(!initialStates.empty) return initialStates.get(0)
		return null
	}
	
	def static List<Connection> getConnectionsForEnd(Component c, ComponentPart part, Port port) {
	    c.connections.filter[(firstEnd.part === part && firstEnd.port === port) || (secondEnd.part === part && secondEnd.port === port)].toList
	}
	
	def static boolean sameEnds(Connection c1, Connection c2) {
	    c1.firstEnd.part === c2.firstEnd.part &&
	    c1.firstEnd.port === c2.firstEnd.port &&
	    c1.secondEnd.part === c2.secondEnd.part &&
	    c1.secondEnd.port === c2.secondEnd.port
	}
	
	def static PortReference otherEnd(Connection c, ComponentPart part, Port port) {
	    if(c.firstEnd.part === part && c.firstEnd.port === port) {
	        c.secondEnd
	    } else {
	        c.firstEnd
	    }
	}
	
	def static EventPattern makeEvent(InterfaceEvent event, Command repliedCommand, PortSelector selector, List<Expression> parameters) {
        if(event === null) { //it is a reply to command
           var CommandEvent command = null
           if(repliedCommand !== null) {
                command = ComponentFactory.eINSTANCE.createCommandEvent
                command.event = repliedCommand
                command.part = selector.part
                command.port = selector.port   
           }
           val reply = ComponentFactory.eINSTANCE.createCommandReply
           reply.command = command
           reply.part = selector.part
           reply.port = selector.port
           for(p : parameters){
               reply.parameters.add(p)
           }
           return reply
        }
        var InterfaceEventInstance result
        switch(event) {
            Command: {
               result = ComponentFactory.eINSTANCE.createCommandEvent
               result.event = event
            }
            Signal: {
               result = ComponentFactory.eINSTANCE.createSignalEvent
               result.event = event   
            }
            Notification: {
               result = ComponentFactory.eINSTANCE.createNotificationEvent
               result.event = event   
            }
            default: return null
        }
        for(p : parameters){
            result.parameters.add(p)
        }
        val portAware = result as PortSelector
        portAware.part = selector.part
        portAware.port = selector.port
        
        result as EventPattern
    }
}