/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

class StateTransitionAction {
    String nextTopLevelState = null
    TransitionFragment subTransition = null
    String machineName = ""
    boolean inNonTriggeredRoot = false
    
    new(String state) {
        this.nextTopLevelState = state
    }
    
    new(TransitionFragment fragment) {
        subTransition = fragment
    }
    
    def getTransitionFragment() {
        return subTransition
    }
    
     def getNextState() {
         if(subTransition !== null) return subTransition.subState
         return nextTopLevelState
     }
     
     def toSubState() {
         subTransition !== null
     }
     
     def getMachineName() {
         return machineName
     }
     
     def setMachineName(String name) {
         machineName = name
     }
     
     def setInNonTriggeredRoot() {
         inNonTriggeredRoot = true
     }
     
     def isInNonTriggeredRoot() {
         inNonTriggeredRoot
     }
}
