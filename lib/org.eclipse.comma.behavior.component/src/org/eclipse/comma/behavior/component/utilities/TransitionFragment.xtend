/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

import java.util.HashSet
import java.util.Iterator
import java.util.Set
import org.eclipse.comma.behavior.behavior.Transition
import org.eclipse.comma.signature.interfaceSignature.Notification
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.ParallelComposition

import static extension org.eclipse.comma.actions.utilities.ActionsUtilities.*
import org.eclipse.comma.actions.actions.CommandReplyWithVars

class TransitionFragment extends BehaviorFragment {
    boolean isRoot = false
    Transition transition = null
    Set<Notification> headNotifications = null
    
    String subState = ""
    
    new(){
        this.isRoot = false
    }
    
    new(Transition t) {
        this.transition = t
        this.isRoot = true
    }
    
    def getTransition() {
        return transition
    }
    
    def boolean isRoot() {
        isRoot
    }
    
    def setSubState(String name) {
        this.subState = name
    }
    
    def getSubState() {
        return this.subState
    }
    
    def getHeadNotifications() {
        if(headNotifications === null) {
            headNotifications = calculateHeadNotifications(actions.iterator)
        }
        headNotifications
    }
    
    private def Set<Notification> calculateHeadNotifications(Iterator<? extends Object> iterator) {
        val result = new HashSet<Notification>
        
        while(iterator.hasNext) {
            val action = iterator.next
            if(action instanceof CommandReplyWithVars) {
                return result
            }
            if(action instanceof EventCall) {
                result.add(action.event as Notification) //in the context of interfaces this is always a notification
                return result
            }
            if(action instanceof EventWithVars) {
                result.add(action.event as Notification)
                return result
            }
            if(action instanceof ParallelComposition) {
                for(a : action.flatten) {
                    if(a instanceof EventCall) {
                        result.add(a.event as Notification)
                    } else if(a instanceof EventWithVars) {
                        result.add(a.event as Notification)
                    }
                }
                if(!result.empty) {
                    return result
                }
            }
            if(action instanceof StateTransitionAction) {
                if(action.toSubState) {
                    val firstNextAction = action.transitionFragment.actions.get(0)
                    if(firstNextAction instanceof EventWithVars) {
                        result.add(firstNextAction.event as Notification)   
                    }
                    return result
                }
            }
            if(action instanceof FunctionFragmentCall) {
                result.addAll(calculateHeadNotifications(action.functionFragment.actions.iterator))
                return result
            }
            if(action instanceof IfAction) {
                val thenBranch = calculateHeadNotifications(action.thenList.actions.iterator)
                var elseBranch = action.elseList === null ? new HashSet<Notification> : calculateHeadNotifications(action.elseList.actions.iterator)
                result.addAll(thenBranch)
                result.addAll(elseBranch)
                if(thenBranch.empty || elseBranch.empty) {
                    result.addAll(calculateHeadNotifications(iterator))
                }
                return result
            }
            if(action instanceof IfHelperAction) {
                val thenBranch = calculateHeadNotifications(action.thenActions.iterator)
                var elseBranch = action.elseActions === null ? new HashSet<Notification> : calculateHeadNotifications(action.elseActions.iterator)
                result.addAll(thenBranch)
                result.addAll(elseBranch)
                if(thenBranch.empty || elseBranch.empty) {
                    result.addAll(calculateHeadNotifications(iterator))
                }
                return result
            }
        }
        result
    }
}
