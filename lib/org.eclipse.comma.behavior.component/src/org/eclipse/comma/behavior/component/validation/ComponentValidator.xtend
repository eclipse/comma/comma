/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.validation

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.actions.actions.Action
import org.eclipse.comma.actions.actions.ActionList
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.EVENT_KIND
import org.eclipse.comma.actions.actions.EventPattern
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction
import org.eclipse.comma.actions.actions.Reply
import org.eclipse.comma.actions.actions.VariableDeclBlock
import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.behavior.RequiredPort
import org.eclipse.comma.behavior.component.component.AnyEvent
import org.eclipse.comma.behavior.component.component.CommandEvent
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.CommandReplyWithVars
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.ComponentModel
import org.eclipse.comma.behavior.component.component.ComponentPackage
import org.eclipse.comma.behavior.component.component.Connection
import org.eclipse.comma.behavior.component.component.ConnectionValue
import org.eclipse.comma.behavior.component.component.ConnectionVariable
import org.eclipse.comma.behavior.component.component.EventCall
import org.eclipse.comma.behavior.component.component.EventWithVars
import org.eclipse.comma.behavior.component.component.ExpressionConnectionState
import org.eclipse.comma.behavior.component.component.ExpressionInterfaceState
import org.eclipse.comma.behavior.component.component.FunctionalConstraint
import org.eclipse.comma.behavior.component.component.FunctionalConstraintsBlock
import org.eclipse.comma.behavior.component.component.PortSelector
import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint
import org.eclipse.comma.behavior.component.component.TriggeredTransition
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinitionPackage
import org.eclipse.comma.behavior.scoping.BehaviorScopeProvider
import org.eclipse.comma.expressions.expression.Expression
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.Variable
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.signature.interfaceSignature.Signal
import org.eclipse.comma.types.types.FileImport
import org.eclipse.comma.types.types.NamespaceImport
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.Type
import org.eclipse.comma.types.types.TypeObject
import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.validation.Check

import static extension org.eclipse.comma.behavior.component.utilities.ComponentUtilities.*
import static extension org.eclipse.comma.types.utilities.TypeUtilities.*
import org.eclipse.comma.behavior.behavior.Transition

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation 
 */
class ComponentValidator extends AbstractComponentValidator {
	/*  
	 * Constraints:
	 * - component specifications may only import interface or component definitions
	 */
	@Check
	override checkImportForValidity(FileImport imp){
		if(! EcoreUtil2.isValidUri(imp, URI.createURI(imp.importURI))){
			error("Invalid resource", imp, TypesPackage.eINSTANCE.fileImport_ImportURI)
		}
		else{
			val r = EcoreUtil2.getResource(imp.eResource, imp.importURI)
			if(! (r.allContents.head instanceof InterfaceDefinition) && !(r.allContents.head instanceof ComponentModel))
				error("The imported resource has to be interface or component model.", imp, TypesPackage.eINSTANCE.fileImport_ImportURI)
		}
	}
	
	/* 
     * Constraints:
     * - interfaces of connected ports are the same
     * - connection connects different component instances
     * - connection can not connect two boundary ports (follows from above)
     * - boundary port is connected to a port of the same direction
     * - connected part ports have different direction
     * - connections are distinct
     * - a boundary port is connected at most once
     */
	@Check
    def checkConnectionPortsInterfaces(Connection c){
        if(c.firstEnd.port === null || c.secondEnd.port === null) {
            return
        }
        if(c.firstEnd.port.interface !== c.secondEnd.port.interface) {
             error("A connection can not connect ports of different interfaces.", c, ComponentPackage.Literals.CONNECTION__FIRST_END)
        }
    }
    
    @Check
    def checkConnectionDistinctEnds(Connection c){
        if(c.firstEnd.part === c.secondEnd.part) {
            error("A connection can not connect ports from the same component instance", c, ComponentPackage.Literals.CONNECTION__FIRST_END)
        }
    }
    
    @Check
    def checkConnectionPortDirections(Connection c){
        if(c.firstEnd.port === null || c.secondEnd.port === null) {
            return
        }
        if(c.firstEnd.part === null || c.secondEnd.part === null) {
            //Connection with boundary port. Direction is the same
            if((c.firstEnd.port instanceof ProvidedPort) !== (c.secondEnd.port instanceof ProvidedPort)) {
                error("Ports must be of the same direction", c.firstEnd, ComponentPackage.Literals.PORT_REFERENCE__PORT)
                error("Ports must be of the same direction", c.secondEnd, ComponentPackage.Literals.PORT_REFERENCE__PORT)
            }
        } else { //part ports
            //port direction must be different
            if((c.firstEnd.port instanceof ProvidedPort) == (c.secondEnd.port instanceof ProvidedPort)) {
                error("Ports must be of different direction", c.firstEnd, ComponentPackage.Literals.PORT_REFERENCE__PORT)
                error("Ports must be of different direction", c.secondEnd, ComponentPackage.Literals.PORT_REFERENCE__PORT)
            }
        }
    }
    
    @Check
    def checkDistinctConnections(Component c) {
        for(conn1 : c.connections){
            for(conn2 : c.connections) {
                if(conn1 !== conn2 && conn1.sameEnds(conn2)){
                    error("There is another connection with the same ends", c, ComponentPackage.Literals.COMPONENT__CONNECTIONS, c.connections.indexOf(conn2))
                }
            }
        }
    }
    
    @Check
    def checkBoundaryPortsConnections(Component c) {
        for(p : c.ports) {
            if(c.getConnectionsForEnd(null, p).size > 1) {
                error("Boundary port may have at most one connection", p, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
    }
    
    @Check
    def checkPartPortsConnections(Component c) {
        for(part : c.parts) {
            for(port : part.componentType.ports) {
                val connections = c.getConnectionsForEnd(part, port)
                if(port instanceof RequiredPort && connections.size > 1) {
                    error("Required part port " + part.name + "::" + port.name + " can be connected at most once", c, ComponentPackage.Literals.COMPONENT__CONNECTIONS, c.connections.indexOf(connections.get(0)))
                }
                if(port instanceof ProvidedPort && connections.size > 1) {
                    //all connections are from required part ports
                    for(conn : connections) {
                        val otherEnd = conn.otherEnd(part, port)
                        if(otherEnd.part === null || otherEnd.port instanceof ProvidedPort) {
                            error("Provided part port " + part.name + "::" + port.name + " can be connected to a single boundary port or to required part ports", c, ComponentPackage.Literals.COMPONENT__CONNECTIONS, c.connections.indexOf(conn))
                        }
                    }
                }
            }
        }
    }
	
	/*
	 * Constraints:
	 * - component ports have unique names
	 */
	@Check
	def checkDuplicatedPortNames(Component c){
		checkForNameDuplications(c.ports, "port", null, "port")
	}
	
	/*
     * Constraints:
     * - component parts have unique names
     */
    @Check
    def checkDuplicatedPartNames(Component c){
        checkForNameDuplications(c.parts, "part", null, "part")
    }
	
	/*
	 * Constraints:
	 * - functional constraints have unique names
	 */
	@Check
	def checkDuplicatedFuncConstraintNames(FunctionalConstraintsBlock fcb){
		checkForNameDuplications(fcb.functionalConstraints, "constraint", null, "constraint")
	}
	
	/*
	 * Constraints:
	 * - states in state-based functional constraints have unique names
	 */
	@Check
	def checkDuplicatedStateNamesFunctionalConstraint(StateBasedFunctionalConstraint fc){
		checkForNameDuplications(fc.states, "state", null, "state")
	}
		
	/*
	 * Constraints:
	 * - state-based functional constraints have exactly one initial state
	 */
	@Check
	def checkInitialState(StateBasedFunctionalConstraint fc){
		val initialStates = fc.states.filter(s | s.initial)
		if(initialStates.size > 1){
			error("More than one initial state", fc, TypesPackage.Literals.NAMED_ELEMENT__NAME)
			return
		}
		if(initialStates.size == 0){
			error("Missing initial state", fc, TypesPackage.Literals.NAMED_ELEMENT__NAME)
		}
	}
	
	/*
	 * Constraints:
	 * - warning is given if an event pattern is subsumed by another event pattern in the 
	 *   'use events' section.
	 *   Example: notification a::N is subsumed by any a::event and is thus obsolete
	 */
	@Check
	def checkUsedEventsList(StateBasedFunctionalConstraint fc){
		for(ev1 : fc.usedEvents)
		for(ev2 : fc.usedEvents){
			if(ev1 != ev2){
				if(ev1.isSubsumedBy(ev2)){
					warning("The event is duplicated or subsumed by another event", fc, ComponentPackage.Literals.STATE_BASED_FUNCTIONAL_CONSTRAINT__USED_EVENTS, fc.usedEvents.indexOf(ev1))
				}
			}
		}
	}
	
	@Check
	def checkCommandReplyPairs(StateBasedFunctionalConstraint fc) {
	    for(ev : fc.usedEvents) {
	        if(ev instanceof CommandEvent) {
	           if(fc.usedEvents.filter(e | matchForCommand(ev, e)).empty) {
	               error("The command is missing a matching reply", fc, ComponentPackage.Literals.STATE_BASED_FUNCTIONAL_CONSTRAINT__USED_EVENTS, fc.usedEvents.indexOf(ev))
	           }
	        }
	        if(ev instanceof CommandReply) {
	           if(fc.usedEvents.filter(e | matchForReply(ev, e)).empty) {
                   error("The reply is missing a matching command", fc, ComponentPackage.Literals.STATE_BASED_FUNCTIONAL_CONSTRAINT__USED_EVENTS, fc.usedEvents.indexOf(ev))
               }
            }
            if(ev instanceof AnyEvent) {
                if(ev.kind == EVENT_KIND::CALL) {
                    if(fc.usedEvents.filter(e | matchForCommand(ev, e)).empty) {
                        error("The any command pattern is missing a matching reply pattern", fc, ComponentPackage.Literals.STATE_BASED_FUNCTIONAL_CONSTRAINT__USED_EVENTS, fc.usedEvents.indexOf(ev))
                    }   
                }
            }
	    }
	}
	
	def boolean matchForReply(CommandReply r, EventPattern ev) {
	    if(ev instanceof CommandEvent) {
	        return (r.part === ev.part) && (r.port === ev.port) && (r.command?.event === ev.event)
	    }
	    if(ev instanceof AnyEvent) {
	        return (ev.kind === EVENT_KIND::CALL) && (r.part === ev.part) && (r.port === ev.port)
	    }
	    false
	}
	
	def boolean matchForCommand(CommandEvent c, EventPattern ev) {
        if(ev instanceof CommandReply) {
            return (c.part === ev.part) && (c.port === ev.port) && (c.event === ev.command?.event || ev.command === null)
        }
        false
    }
    
    def boolean matchForCommand(AnyEvent c, EventPattern ev) {
        if(ev instanceof CommandReply) {
            return (c.part === ev.part) && (c.port === ev.port) && (ev.command === null)
        }
        false
    }
    
    @Check
    def checkTriggeredTransition(TriggeredTransition t) {
        if(t.idVarDef !== null) {
            if(!t.idVarDef.type.typeObject.subTypeOf(idType)) {
                error("Variable has to be of type id", ComponentPackage.Literals.CONNECTION_VARIABLE__ID_VAR_DEF)
            }
        }
        if(t.trigger !== null) {
            val isCall = t.trigger instanceof Command || t.trigger instanceof Signal
            if(isCall && t.port instanceof RequiredPort) {
                error("Trigger has to be notification or reply to the required port " + t.port.name, BehaviorPackage.Literals.TRIGGERED_TRANSITION__TRIGGER)
                return
            }
            if(!isCall && t.port instanceof ProvidedPort) {
                error("Trigger has to be command or signal to the provided port " + t.port.name, BehaviorPackage.Literals.TRIGGERED_TRANSITION__TRIGGER)
                return
            }
            val evPattern = makeEvent(t.trigger, null, t, new ArrayList<Expression>)
            if(! (t.eContainer.eContainer as StateBasedFunctionalConstraint).usedEvents.exists[e | evPattern.isSubsumedBy(e)]){
                error("Event is not declared in used events list", BehaviorPackage.Literals.TRIGGERED_TRANSITION__TRIGGER)
            }
        } else { //the trigger is a reply
            //reply as trigger can be used only for required ports
            if(t.port instanceof ProvidedPort) {
                error("Reply can be a trigger only for commands on required ports", ComponentPackage.Literals.TRIGGERED_TRANSITION__REPLY_TO)    
            } else {
                val evPattern = makeEvent(null, t.replyTo, t, new ArrayList<Expression>)
                if(! (t.eContainer.eContainer as StateBasedFunctionalConstraint).usedEvents.exists[e | evPattern.isSubsumedBy(e)]){
                    error("Reply is not declared in used events list", ComponentPackage.Literals.TRIGGERED_TRANSITION__REPLY_TO)
                    return
                }
                var expectedTypes = new ArrayList<TypeObject>
                expectedTypes.addAll(t.replyTo.parameters.filter(p | p.direction != DIRECTION::IN).map[type.typeObject])
                if(!t.replyTo.type.isVoid) {expectedTypes.add(t.replyTo.type.typeObject)}
                //Check if the number of actual params in reply are equal to the size of expectedTypes
                if(expectedTypes.size() != t.parameters.size()){
                    error("Wrong number of parameters in reply", BehaviorPackage.Literals.TRIGGERED_TRANSITION__PARAMETERS)
                    return
                }
                //Number of values Ok, check the types
                for(i : 0..< expectedTypes.size){
                    if(!t.parameters.get(i).type.typeObject.subTypeOf(expectedTypes.get(i))) {
                        error('The type of the variable must be the same as the return type of the command or inout/out parameter type.', t,
                            BehaviorPackage.Literals.TRIGGERED_TRANSITION__PARAMETERS, i)   
                    }
                }
            }
        }
    }
    
    /*
     * Constraints:
     * - used events must be used at least once in constraint 
     */
	@Check
	def checkUsedEvent(StateBasedFunctionalConstraint fc){
	    var List<EventPattern> eventPatterns = new ArrayList<EventPattern>
	    for(ec : EcoreUtil2.getAllContentsOfType(fc, EventCall)){
	        eventPatterns.add(makeEvent(ec.event, null, ec, new ArrayList<Expression>))
	    }
	    for(ec : EcoreUtil2.getAllContentsOfType(fc, EventWithVars)){
	        eventPatterns.add(makeEvent(ec.event, null, ec, new ArrayList<Expression>))
	    }
	    for(state : fc.states){
	        for(ec : EcoreUtil2.getAllContentsOfType(state, Reply)){
	            var Command command = null
                if(ec.command !== null) {
                    command = ec.command.event as Command
                } else {
                    val t = EcoreUtil2.getContainerOfType(ec, TriggeredTransition)
                    if(t?.trigger !== null && t.trigger instanceof Command) {
                        command = t.trigger as Command
                    }
                }
                if(command !== null) {
                    eventPatterns.add(makeEvent(null, command, ec as PortSelector, new ArrayList<Expression>))
                }
	        }
	    }
	    for(t : EcoreUtil2.getAllContentsOfType(fc, TriggeredTransition)){
	        if(t.trigger === null) { //the trigger of the transition is reply
	           eventPatterns.add(makeEvent(t.trigger, t.replyTo, t, new ArrayList<Expression>))
	        } else {
	           eventPatterns.add(makeEvent(t.trigger, null, t, new ArrayList<Expression>))   
	        }
	    }
	    for(e : fc.usedEvents){
	        if (! eventPatterns.exists[evPattern | evPattern.isSubsumedBy(e)]){
	            error("Event is not mentioned in the constraint " + fc.name, fc, ComponentPackage.Literals.STATE_BASED_FUNCTIONAL_CONSTRAINT__USED_EVENTS, fc.usedEvents.indexOf(e))
	        }
	    }
	}
	/*
	 * Constraints:
	 * - events used in event receptions must belong to the list of defined used events
	 */
	@Check
	def checkEventReceptions(StateBasedFunctionalConstraint fc){
		for(ec : EcoreUtil2.getAllContentsOfType(fc, EventCall)){
		    val evPattern = makeEvent(ec.event, null, ec, new ArrayList<Expression>)
			if(! fc.usedEvents.exists[e | evPattern.isSubsumedBy(e)]){
				error("Event is not declared in used events list", ec, ActionsPackage.Literals.INTERFACE_EVENT_INSTANCE__EVENT)
			}
		}
		for(ec : EcoreUtil2.getAllContentsOfType(fc, EventWithVars)){
            val evPattern = makeEvent(ec.event, null, ec, new ArrayList<Expression>)
            if(! fc.usedEvents.exists[e | evPattern.isSubsumedBy(e)]){
                error("Event is not declared in used events list", ec, ActionsPackage.Literals.EVENT_WITH_VARS__EVENT)
            }
        }
        for(ec : EcoreUtil2.getAllContentsOfType(fc, Reply)){
            var Command command = null
            if(ec.command !== null) {
                command = ec.command.event as Command
            } else {
                val t = EcoreUtil2.getContainerOfType(ec, TriggeredTransition)
                if(t?.trigger !== null && t.trigger instanceof Command) {
                    command = t.trigger as Command
                }
            }
            if(command !== null) {
                val evPattern = makeEvent(null, command, ec as PortSelector, new ArrayList<Expression>)   
                if(! fc.usedEvents.exists[e | evPattern.isSubsumedBy(e)]){
                    error("Reply to this command is not declared in used events list", ec, ActionsPackage.Literals.REPLY__COMMAND)
                } else {
                    //reply is Ok. Check for params
                    if(ec instanceof CommandReply && EcoreUtil2.getContainerOfType(ec, Transition) !== null) {
                        checkReplyAgainstCommand(command, ec)   
                    }
                }  
            }
        }
	}
	
	/* 
	 * Constraints:
	 * - warning is given if a variable is declared but never used 
	 */
	@Check
	def checkUnusedVarsFunctionalConstraint(FunctionalConstraint fc){
		val List<Variable> variables = new ArrayList<Variable>()
		variables.addAll(fc.vars)
		variables.removeAll(EcoreUtil2.getAllContentsOfType(fc, ExpressionVariable).map[variable])
		variables.forEach[warning('Unused variable.', ActionsPackage.Literals.VARIABLE_DECL_BLOCK__VARS,
									fc.vars.indexOf(it))]
	}
		
	/*
	 * Constraints:
	 * - the expression in predicate functional constraint is of type bool
	 */
	@Check
	def checkTypingPredicateFunctionalConstraint(PredicateFunctionalConstraint fc){
		val t = fc.expression.typeOf
		if(!t.subTypeOf(boolType)){
			error("Type mismatch: the type of the expression must be boolean", ComponentPackage.Literals.PREDICATE_FUNCTIONAL_CONSTRAINT__EXPRESSION)
		}
	}
	
	/*
	 * Constraints:
	 * - if a variable is used in an event reception, it must be of type id
	 */
	@Check
	def checkConnectionValue(ConnectionValue cv){
		if(cv.idVar !== null){
			val t = cv.idVar.typeOf
			if(!t.subTypeOf(idType))
				error("The type of the variable must be id", ComponentPackage.Literals.CONNECTION_VALUE__ID_VAR)
		}
	}
	
	@Check
    def checkConnectionVar(ConnectionVariable cv){
        if(cv.idVarDef !== null){
            val t = cv.idVarDef.type.typeObject
            if(!t.subTypeOf(idType))
                error("The type of the variable must be id", ComponentPackage.Literals.CONNECTION_VARIABLE__ID_VAR_DEF)
        }
    }
    
    @Check
    def checkVarTypes(CommandReplyWithVars r) {
        if(r.command !== null) {
            checkReplyAgainstCommand(r.command.event as Command, r)   
        } else {
            val t = EcoreUtil2.getContainerOfType(r, TriggeredTransition)
            if(t?.trigger !== null && t.trigger instanceof Command) {
                checkReplyAgainstCommand(t.trigger as Command, r)
            }
        }
    }
    
	/*
	 * Constraints:
	 * - if a variable is used in an event reception, it must be of type id
	 */
	@Check
	def checkConnectionVariableInExpr(ExpressionConnectionState expr){
		if(expr.idVar !== null){
			val t = expr.idVar.typeOf
			if(!t.subTypeOf(idType))
				error("The type of the variable must be id", ComponentPackage.Literals.EXPRESSION_CONNECTION_STATE__ID_VAR)
		}
	}
	
	//Using id type is allowed for certain variables
	@Check
	override checkForIdType(Type t){
		if(t.type instanceof SimpleTypeDecl && (t.type.name.equals("id"))){
			if(!(t.eContainer.eContainer instanceof VariableDeclBlock) && !(t.eContainer.eContainer instanceof ConnectionVariable))
				error("Usage of type id is not allowed", TypesPackage.Literals.TYPE__TYPE)
		}
	}
		
	//Type checker extension: the expected type of the two new expressions is boolean
	override typeOf(Expression exp){
		if(exp !== null && (exp instanceof ExpressionInterfaceState || exp instanceof ExpressionConnectionState)) boolType
		else super.typeOf(exp)
	}
	
	@Check
    def checkDuplications(ComponentModel compDef){
        if(compDef.name === null && compDef.imports.filter(NamespaceImport).empty) {
            return
        }
        var multiMap = compDef.importedTypes
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported type " + entry.key.toString, compDef.component, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
        multiMap = getGlobalDeclarations(compDef, InterfaceDefinitionPackage.Literals.INTERFACE)
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported interface " + entry.key.toString, compDef.component, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
    }

    @Check
    def checkEventPattern(PortSelector ev) {
        //get connections
        if(ev.part === null) return //do nothing
        val parent = EcoreUtil2::getContainerOfType(ev, Component)
        val connections = parent.getConnectionsForEnd(ev.part, ev.port)
        if(connections.size == 1) {
            val conn = connections.get(0)
            val otherEnd = conn.otherEnd(ev.part, ev.port)
            if(otherEnd.part === null) {
                error("Use an event pattern that refers to boundary port " + otherEnd.port.name, ev, ComponentPackage.Literals.PORT_SELECTOR__PORT)
            }
        }
    }
    
     /*
     * Give a warning if a component with parts have unconnected ports
     */
    @Check
    def checkUnconnectedPorts(Component c) {
        if(c.parts.empty) {return}
        //first check component ports
        for(port : c.ports) {
            if(c.getConnectionsForEnd(null, port).empty) {
                warning("The port is unconnected.", c, BehaviorPackage.Literals.BLOCK__PORTS, c.ports.indexOf(port))
            }
        }
        //check ports of the parts
        for(part : c.parts) {
            for(p : part.componentType.ports) {
                if(c.getConnectionsForEnd(part, p).empty) {
                    warning("Port " + p.name + " is unconnected.", c, ComponentPackage.Literals.COMPONENT__PARTS, c.parts.indexOf(part))
                }
            }
        }
    }
    
    @Check
    def checkVariableName(ConnectionVariable cv) {
        if(cv.idVarDef?.name === null) return
        var pred = cv.eContainer
        if(pred instanceof ActionList) {
            val index = pred.actions.indexOf(cv)
            if(index > 0) {
                pred = pred.actions.get(index-1)
            }
        }
        val visibleVars = (scopeProvider as BehaviorScopeProvider).scope_variable(pred)
        if(!visibleVars.getElements(QualifiedName.create(cv.idVarDef.name)).empty) {
            error("Variable with this name is already defined", cv, ComponentPackage.Literals.CONNECTION_VARIABLE__ID_VAR_DEF)
        }
        var EList<Variable> varsToCheck = null
        if(cv instanceof ActionWithVars) {
            varsToCheck = cv.parameters    
        }
        if(cv instanceof TriggeredTransition) {
            varsToCheck = cv.parameters
        }
        if(varsToCheck === null) {return}
        for(v : varsToCheck) {
            if(v.name.equals(cv.idVarDef.name)) {
                error("Variable with this name is already defined", v, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }     
    }
    
    @Check
    def checkAllowedActionsInTransition(Action act) {
        if(act instanceof AssignmentAction || act instanceof IfAction || act instanceof RecordFieldAssignmentAction) {
            return
        }
        if(act instanceof CommandReply && act.eContainer instanceof StateBasedFunctionalConstraint) {
            return
        }
        val fc = EcoreUtil2::getContainerOfType(act, StateBasedFunctionalConstraint)
        if(fc === null) {return}
        val PortSelector ps = act as PortSelector
        var isCall = false
        if(act instanceof EventCall) {
            isCall = act.event instanceof Command || act.event instanceof Signal
        } else if(act instanceof EventWithVars) {
            isCall = act.event instanceof Command || act.event instanceof Signal
        }
        if(ps.port instanceof RequiredPort && !isCall) {
            error("Action not allowed in transition body for this required port", ps, ComponentPackage.Literals.PORT_SELECTOR__PORT)
        }
        if(ps.port instanceof ProvidedPort && isCall) {
            error("Action not allowed in transition body for this provided port", ps, ComponentPackage.Literals.PORT_SELECTOR__PORT)    
        }
    }
    
    @Check
    def checkRepliesMissingCommand(Reply r) {
        if(r.command === null) {
            val t = EcoreUtil2.getContainerOfType(r, Transition)
            if(t === null) {
                return
            }
            val parent = r.eContainer as ActionList
            val index = parent.actions.indexOf(r)
            if(t instanceof TriggeredTransition) {
               //the reply without command is allowed but the command must be the trigger of the transition
               if(t.trigger === null) {
                   error("The reply has to specify its command when the transition trigger is not a command", parent, ActionsPackage.Literals.ACTION_LIST__ACTIONS, index)
               } else if(t.trigger instanceof Command) {
                   if(t.part !== (r as PortSelector).part || t.port !== (r as PortSelector).port) {
                        error("The reply has to be on the same port as the transition trigger", parent, ActionsPackage.Literals.ACTION_LIST__ACTIONS, index)        
                   }
               } else { //trigger not a command
                   error("The reply has to specify its command when the transition trigger is not a command", parent, ActionsPackage.Literals.ACTION_LIST__ACTIONS, index)
               }
            } else {
                //the reply without command is not allowed
                error("The reply has to specify its command", parent, ActionsPackage.Literals.ACTION_LIST__ACTIONS, index)
            }
        }
    }
}
