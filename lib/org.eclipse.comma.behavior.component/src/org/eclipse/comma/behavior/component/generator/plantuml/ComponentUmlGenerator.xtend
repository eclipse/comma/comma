/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.generator.plantuml

import org.eclipse.comma.types.generator.CommaGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.component.component.PortReference
import org.eclipse.comma.behavior.component.component.Connection
import org.eclipse.comma.behavior.behavior.RequiredPort
import org.eclipse.emf.common.util.URI
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.xtext.generator.AbstractFileSystemAccess
import org.eclipse.core.runtime.Path
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.resources.IResource
import java.io.File
import net.sourceforge.plantuml.SourceFileReader

class ComponentUmlGenerator extends CommaGenerator {
    final Component component
    static final String EXT_UML = ".plantuml"
    
    URI outputURI
    
    new(Component component, IFileSystemAccess fsa) {
        super(component.name+EXT_UML, fsa)      
        this.component = component        
    }
    
    override getContent() {
        toUmlComponentDiagram(component, null)
    }
    
    override generate() {
        super.generate()
        fsa.generateFile(component.name + ".png", "")
            val umlURI = URI.createURI(component.name + EXT_UML).resolve(outputURI)
            val umlFile = new File(umlURI.toFileString)     
            val reader = new SourceFileReader(umlFile);
            if(!reader.generatedImages.isEmpty) {
                reader.getGeneratedImages().get(0); 
            }
    }
    
    def setPrjURIForImages(URI prjURI) {
        val projectPath = getOutputURI(prjURI)
        val gen = URI.createURI(CommaFileSystemAccess.getGenerationFolder(fsa))
        outputURI = gen.resolve(projectPath)
    }
    
    def private getOutputURI(URI prjURI) {
        var rURI = prjURI
        if (rURI.isPlatform) {
            val IResource eclipseResource = ResourcesPlugin.workspace.root.findMember(
                rURI.toPlatformString(true))
            rURI =  URI.createFileURI(eclipseResource.rawLocation.toOSString);
        }
        
        var outputDir = getOutputConfiguration;
        if (outputDir === null) {
            throw new IllegalArgumentException("Output directory was not set.")
        }

        if (outputDir.isRelative) {
            outputDir = outputDir.resolve(rURI)
        }
        if (!outputDir.hasTrailingPathSeparator) {
            outputDir.appendSegment("")
        }
        
        return outputDir
    }
    
    def private getOutputConfiguration() {
        var access = fsa
        if (access instanceof CommaFileSystemAccess) {
            access = access.IFileSystemAccess
        }
        var tempPath = new Path(IFileSystemAccess.DEFAULT_OUTPUT)
        if (access instanceof AbstractFileSystemAccess) {
            val configs = access.outputConfigurations           
            if (!configs.isEmpty) {
                tempPath = new Path(configs.get(IFileSystemAccess.DEFAULT_OUTPUT).outputDirectory)
            }
        }
        val path = if (!tempPath.hasTrailingSeparator) {
                tempPath.addTrailingSeparator
            } else {
                tempPath
            }
        return URI.createFileURI(path.toString)
    }
    
    def CharSequence toUmlComponentDiagram(Component c, String instanceName) {
        val isTopLevel = instanceName === null
        '''
        «IF isTopLevel»
        @startuml
        «ENDIF»
        component "«IF !isTopLevel»«instanceName» : «ENDIF»«c.name»" {
            «FOR port : c.ports»
            «IF port instanceof ProvidedPort»portin«ELSE»portout«ENDIF» "«port.name» : «port.interface.name»" as «port.name»_«c.name»
            «ENDFOR»
            «IF !c.parts.empty»
            
            «FOR part : c.parts»
                «toUmlComponentDiagram(part.componentType, part.name)»

            «ENDFOR»
            «ENDIF»
            «FOR connection : c.connections»
            «portId(connection.firstEnd)»«connectionDirection(connection)»«portId(connection.secondEnd)»
            «ENDFOR»
        }
        «IF isTopLevel»
        «FOR port : component.ports»
        interface «port.interface.name»
        «IF port instanceof ProvidedPort»
        «port.interface.name» -- «port.name»_«component.name»
        «ELSE»
        «port.name»_«component.name» ..> «port.interface.name» : use
        «ENDIF»
        «ENDFOR»
        @enduml
        «ENDIF»
        
        '''
    }
    
    def portId(PortReference pRef)
    '''«pRef.port.name»_«IF pRef.part !== null»«pRef.part.componentType.name»«ELSE»«(pRef.eContainer.eContainer as Component).name»«ENDIF»'''
    
    def connectionDirection(Connection c) {
        if(c.firstEnd.port instanceof ProvidedPort) {
            if(c.secondEnd.port instanceof ProvidedPort) {
                if(c.firstEnd.part === null) ''' --> ''' else ''' <-- '''
            } else {
                ''' <-- '''
            }
        } else {
            if(c.secondEnd.port instanceof RequiredPort) {
                if(c.firstEnd.part === null) ''' <-- ''' else ''' --> '''
            } else {
                ''' --> '''
            }
        }
    }
}
