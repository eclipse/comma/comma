/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

import java.util.List
import org.eclipse.comma.expressions.expression.Expression

class IfHelperAction {
    Expression condition
    List<Object> thenActions
    List<Object> elseActions = null
    
    new(Expression condition) {
        this.condition = condition
    }
    
    def Expression getCondition() {
        return condition
    }
    
    def setThenActions(List<Object> thenActions) {
        this.thenActions = thenActions
    }
    
    def getThenActions() {
        return thenActions
    }
    
    def setElseActions(List<Object> elseActions) {
        this.elseActions = elseActions
    }
    
    def getElseActions() {
        return elseActions
    }
}
