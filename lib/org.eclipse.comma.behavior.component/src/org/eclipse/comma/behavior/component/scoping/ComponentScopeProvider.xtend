/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.scoping

import com.google.common.collect.Lists
import java.util.ArrayList
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.CommandEvent
import org.eclipse.comma.behavior.behavior.BehaviorPackage
import org.eclipse.comma.behavior.behavior.EventInState
import org.eclipse.comma.behavior.behavior.Port
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.CommandReplyWithVars
import org.eclipse.comma.behavior.component.component.ComponentPackage
import org.eclipse.comma.behavior.component.component.ComponentPart
import org.eclipse.comma.behavior.component.component.ConnectionVariable
import org.eclipse.comma.behavior.component.component.ExpressionConnectionState
import org.eclipse.comma.behavior.component.component.ExpressionInterfaceState
import org.eclipse.comma.behavior.component.component.FunctionalConstraint
import org.eclipse.comma.behavior.component.component.PortReference
import org.eclipse.comma.behavior.component.component.PortSelector
import org.eclipse.comma.behavior.component.component.TriggeredTransition
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.types.ModelContainer
import org.eclipse.comma.types.types.NamespaceImport
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScope

import static org.eclipse.comma.behavior.component.utilities.ComponentUtilities.*
import static org.eclipse.xtext.scoping.Scopes.*

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class ComponentScopeProvider extends AbstractComponentScopeProvider {
	
	override getScope(EObject context, EReference reference){
		if(reference == ComponentPackage.Literals.EXPRESSION_INTERFACE_STATE__STATE){
			return stateScopeForPort(context, reference, (context as ExpressionInterfaceState).port)
		}
		
		if(reference == ComponentPackage.Literals.EXPRESSION_CONNECTION_STATE__STATES){
			return stateScopeForPort(context, reference, (context as ExpressionConnectionState).port)
		}
		
		if(context instanceof EventInState && reference == BehaviorPackage.Literals.EVENT_IN_STATE__STATE){
			return stateScopeForPort(context, reference, ((context as EventInState).event as PortSelector)?.port)
		}

		if(reference == ComponentPackage.Literals.PORT_REFERENCE__PORT) {
		    val portRef = context as PortReference
		    var part = portRef.part
		    if(part !== null) {
		      if(part.eIsProxy) {
		        part = EcoreUtil2.resolve(part, portRef) as ComponentPart
		      }
		      if(part.componentType !== null) return scopeFor(part.componentType.ports)
		    }
		    return super.getScope(context, reference)
		}
		
		if(reference == ComponentPackage.Literals.PORT_SELECTOR__PORT) {
            val portRef = context as PortSelector
            var part = portRef.part
            if(part !== null) {
              if(part.eIsProxy) {
                part = EcoreUtil2.resolve(part, portRef) as ComponentPart
              }
              if(part.componentType !== null) return scopeFor(part.componentType.ports)
            }
            return super.getScope(context, reference)
        }

		if(context instanceof CommandEvent
		   && reference == ActionsPackage.Literals.INTERFACE_EVENT_INSTANCE__EVENT
		   && (context.eContainer instanceof CommandReply || context.eContainer instanceof CommandReplyWithVars)) {
		      val port = (context.eContainer as PortSelector).port
		      val sig = port?.interface
		      if(sig !== null) {
		          return scopeFor(sig.commands)
		      }
		}
		
		if(context instanceof TriggeredTransition 
		   && reference == ComponentPackage.Literals.TRIGGERED_TRANSITION__REPLY_TO) {
		      val port = (context as PortSelector).port
		      val sig = port?.interface
              if(sig !== null) {
                  return scopeFor(sig.commands)
              }       
		}
		
		return super.getScope(context, reference)
	}
	
	def stateScopeForPort(EObject context, EReference reference, Port port){
		val states = getInterfaceStatesForPort(port, delegate)
		if(states !== null){
			return scopeFor(states)
		}
		return super.getScope(context, reference);
	}
	
	override findVisibleInterfaces(EObject context) {
	    var result = new ArrayList<Signature>()
		if(context instanceof PortSelector){
			if(context.port !== null && context.port.interface !== null){
				result.add(context.port.interface)
				return result
			}
		}
		if(EcoreUtil2::getContainerOfType(context, ModelContainer).imports.filter(NamespaceImport).empty) {
		    return super.findVisibleInterfaces(context)
		}
		result
	}
	
	override IScope scope_variable(EObject context){
		if(context instanceof FunctionalConstraint){
			return scopeFor(context.vars)
		}
		if(context instanceof TriggeredTransition) {
		    val vars = Lists.newArrayList(context.parameters)
		    if(context.idVarDef !== null) {
		        vars.add(context.idVarDef)
		    }
		    if(! vars.empty){
                return scopeFor(vars, scope_variable(context.eContainer))
            }
            else
                return scope_variable(context.eContainer)
		}
		if(context instanceof ConnectionVariable){
		    if(context.idVarDef !== null) {
                return scopeFor(Lists.newArrayList(context.idVarDef), super.scope_variable(context))
            }
		}
		return super.scope_variable(context);
	}	
}
