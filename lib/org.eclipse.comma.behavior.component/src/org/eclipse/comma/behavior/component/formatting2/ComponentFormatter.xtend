/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.formatting2

import com.google.inject.Inject
import java.util.List
import org.eclipse.comma.actions.actions.Action
import org.eclipse.comma.behavior.component.component.AnyEvent
import org.eclipse.comma.behavior.component.component.CommandEvent
import org.eclipse.comma.behavior.component.component.CommandReply
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.ComponentModel
import org.eclipse.comma.behavior.component.component.FunctionalConstraintsBlock
import org.eclipse.comma.behavior.component.component.NotificationEvent
import org.eclipse.comma.behavior.component.component.PortSelector
import org.eclipse.comma.behavior.component.component.PredicateFunctionalConstraint
import org.eclipse.comma.behavior.component.component.SignalEvent
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint
import org.eclipse.comma.behavior.component.services.ComponentGrammarAccess
import org.eclipse.comma.behavior.formatting2.BehaviorFormatter
import org.eclipse.comma.expressions.expression.Variable
import org.eclipse.comma.types.types.Import
import org.eclipse.emf.common.util.EList
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion
import org.eclipse.comma.behavior.component.component.EventCall

class ComponentFormatter extends BehaviorFormatter {

    @Inject extension ComponentGrammarAccess

    def dispatch void format(ComponentModel componentModel, extension IFormattableDocument document) {
        for (Import _import : componentModel.getImports()) {
            _import.format;
        }
        componentModel.component.format;
    }

    def dispatch void format(Component component, extension IFormattableDocument document) {
        val rFinder = component.regionFor

        rFinder.keyword(componentAccess.componentKeyword_0).prepend(emptyLine).append(oneSpace)
        rFinder.assignment(componentAccess.nameAssignment_1).append(emptyLine)

        component.ports.forEach [ port, idx |
            if(idx != 0) port.prepend(newLine)
            port.format
        ]

        component.functionalConstraintsBlock?.format
        component.timeConstraintsBlock?.format
        component.dataConstraintsBlock?.format
        component.genericConstraintsBlock?.format
    }

    def dispatch format(FunctionalConstraintsBlock functionalConstraintsBlock,
        extension IFormattableDocument document) {
        val regionFor = functionalConstraintsBlock.regionFor

        regionFor.keyword(functionalConstraintsBlockAccess.functionalKeyword_0).prepend(emptyLine).append(oneSpace)

        functionalConstraintsBlock.functionalConstraints.forEach [ functionalConstraint |
            functionalConstraint.prepend(emptyLine)
            functionalConstraint.format
        ]
    }

    def dispatch format(StateBasedFunctionalConstraint functionalConstraint, extension IFormattableDocument document) {
        val regionFor = functionalConstraint.regionFor

        val brackets = regionFor.keywordPairs(stateBasedFunctionalConstraintAccess.leftCurlyBracketKeyword_1,
            stateBasedFunctionalConstraintAccess.rightCurlyBracketKeyword_8)?.get(0)
        brackets.interior(indent)

        regionFor.keyword(stateBasedFunctionalConstraintAccess.leftCurlyBracketKeyword_1).prepend(oneSpace).append(
            emptyLine)

        regionFor.keyword(stateBasedFunctionalConstraintAccess.useKeyword_2).append(oneSpace)
        functionalConstraint.usedEvents.forEach [ usedEvent, idx |
            usedEvent.prepend(newLine)
            usedEvent.format
        ]

        regionFor.keyword(stateBasedFunctionalConstraintAccess.variablesKeyword_5_0)?.prepend(emptyLine)
        functionalConstraint.vars.forEach [ v |
            v.prepend(newLine)
            v.format
        ]

        regionFor.keyword(stateBasedFunctionalConstraintAccess.initKeyword_6_0)?.prepend(emptyLine)
        functionalConstraint.initActions.forEach [ initAction |
            initAction.prepend(newLine)
            initAction.format
        ]

        functionalConstraint.states.forEach [ state |
            state.prepend(emptyLine)
            state.format
        ]

        regionFor.keyword(stateBasedFunctionalConstraintAccess.rightCurlyBracketKeyword_8).prepend(newLine)
    }

    def dispatch format(PredicateFunctionalConstraint functionalConstraint, extension IFormattableDocument document) {
        // TODO
    }

    def dispatch void format(CommandEvent commandEvent, extension IFormattableDocument document) {
        commandEvent.regionFor.keyword("command").append(oneSpace)
        formatPortAwareEvent(commandEvent, document)
        formatInterfaceEventInstance(commandEvent, document)
    }

    def dispatch void format(NotificationEvent notificationEvent, extension IFormattableDocument document) {
        notificationEvent.regionFor.keyword("notification").append(oneSpace)
        formatPortAwareEvent(notificationEvent, document)
        formatInterfaceEventInstance(notificationEvent, document)
    }

    def dispatch void format(SignalEvent signalEvent, extension IFormattableDocument document) {
        signalEvent.regionFor.keyword("signal").append(oneSpace)
        formatPortAwareEvent(signalEvent, document)
        formatInterfaceEventInstance(signalEvent, document)
    }

    def dispatch void format(AnyEvent anyEvent, extension IFormattableDocument document) {
        anyEvent.regionFor.keyword("any").append(oneSpace)
        formatPortAwareEvent(anyEvent, document)
    }

    def dispatch void format(CommandReply commandReply, extension IFormattableDocument document) {
        formatPortAwareEvent(commandReply, document)
        formatParameterizedEvent(commandReply, document)
        commandReply.regionFor.keyword("<").surround(noSpace)
        commandReply.regionFor.keyword(">").surround(noSpace)
        commandReply.regionFor.keyword("to").surround(oneSpace)
        commandReply.command.format
    }
    
    def dispatch void format(EventCall eventCall, extension IFormattableDocument document) {
        formatPortAwareEvent(eventCall, document)
        formatInterfaceEventInstance(eventCall, document)
        eventCall.regionFor.keyword("<").surround(noSpace)
        eventCall.regionFor.keyword(">").surround(noSpace)
    }

    def formatPortAwareEvent(PortSelector portAwareEvent, extension IFormattableDocument document) {
        portAwareEvent.regionFor.keyword("::").surround(noSpace)
    }

    /* Helper Functions */
    def firstComponentRegion(List<ISemanticRegion> provided, List<ISemanticRegion> required) {
        if (provided.empty) {
            if (required.empty) {
                null
            } else {
                required.head
            }
        } else if (required.empty) {
            provided.head
        } else {
            if(required.head.offset < provided.head.offset) required.head else provided.head
        }
    }

    def lastComponentRegion(List<ISemanticRegion> provided, List<ISemanticRegion> required, EList<Variable> vars,
        EList<Action> actions) {
        if (!actions.empty) {
            actions.last.semanticRegions.last
        } else if (!vars.empty) {
            vars.last.semanticRegions.last
        } else if (provided.empty) {
            if (required.empty) {
                null
            } else {
                required.last
            }
        } else if (required.empty) {
            provided.last
        } else {
            if(required.last.offset > provided.last.offset) required.last else provided.last
        }
    }

}
