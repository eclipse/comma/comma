/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

import java.util.ArrayList
import java.util.List

abstract class BehaviorFragment {
    protected List<Object> actions = new ArrayList<Object>
    protected String methodName = ""
    protected String machineName = ""
    
    def addAction(Object action) {
        actions.add(action)
    }
    
    def addActions(List<Object> subActions) {
        actions.addAll(subActions)
    }
    
    def getActions() {
        return actions
    }
    
    def getMethodName() {
        return methodName
    }
    
    def setMethodName(String name) {
        methodName = name
    }
    
    def setMachineName(String name) {
        machineName = name
    }
    
    def getMachineName() {
        return machineName
    }
}
