/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.behavior.component.utilities

import java.util.ArrayList
import java.util.Iterator
import java.util.List
import org.eclipse.comma.actions.actions.Action
import org.eclipse.comma.actions.actions.ActionWithVars
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.ParallelComposition
import org.eclipse.comma.behavior.behavior.Clause
import org.eclipse.comma.behavior.behavior.StateMachine
import org.eclipse.comma.behavior.behavior.Transition
import org.eclipse.comma.behavior.behavior.TriggeredTransition
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.comma.actions.actions.Reply
import org.eclipse.comma.behavior.component.component.EventCall
import org.eclipse.comma.behavior.component.component.StateBasedFunctionalConstraint
import org.eclipse.comma.behavior.component.component.ConnectionVariable

class TransitionDecomposer {
    
    static (Object)=>Boolean isPointOfSplit
    static (Object)=>Boolean isPointOfSplitInterface = [action | action instanceof ActionWithVars]
    static (Object)=>Boolean isPointOfSplitComponent = [action | action instanceof ActionWithVars || action instanceof Reply || action instanceof EventCall]
    
    static (IfAction)=>Boolean isSplittable
    static (IfAction)=>Boolean isSplittableInterface = [ifAction | !EcoreUtil2.getAllContentsOfType(ifAction, ActionWithVars).
                                                                   filter[a | !(a.eContainer instanceof ParallelComposition)].empty]
    static (IfAction)=>Boolean isSplittableComponent = [ifAction | val iter = EcoreUtil2.getAllContents(ifAction, true)
                                                                   while(iter.hasNext) {
                                                                     if(isPointOfSplitComponent.apply(iter.next))
                                                                        return true
                                                                   }     
                                                                   return false]
    
    static def ClauseFragments decomposeTransition(StateMachine m, Transition t, Clause c) {
        isPointOfSplit = isPointOfSplitInterface
        isSplittable = isSplittableInterface
        val result = new ClauseFragments
        val main = new TransitionFragment(t)
        result.machineName = m.name
        val Object nextAction = new StateTransitionAction(c.target?.name)
        result.addRootFragment(main)
        if(t instanceof TriggeredTransition) {
            for(p : t.parameters) {result.addVariable(p)}
            main.addActions(behaviorBody(c.actions !== null ? c.actions.actions.iterator : (new ArrayList<Action>).iterator, false, result, nextAction))    
        } else {
            main.addActions(behaviorBody(c.actions.actions.iterator, true, result, nextAction))
        }
        result.setMachineInLastAction
        result
    }
    
    static def ClauseFragments decomposeTransition(StateBasedFunctionalConstraint fc, Transition t, Clause c) {
        isPointOfSplit = isPointOfSplitComponent
        isSplittable = isSplittableComponent
        val result = new ClauseFragments
        val main = new TransitionFragment(t)
        result.machineName = fc.name
        val Object nextAction = new StateTransitionAction(c.target.name)
        result.addRootFragment(main)
        if(t instanceof org.eclipse.comma.behavior.component.component.TriggeredTransition) {
            for(p : t.parameters) {result.addVariable(p)}
            if(t.idVarDef !== null) result.addVariable(t.idVarDef)
            main.addActions(behaviorBody(c.actions !== null ? c.actions.actions.iterator : (new ArrayList<Action>).iterator, false, result, nextAction))    
        } else {
            main.addActions(behaviorBody(c.actions.actions.iterator, true, result, nextAction))
        }
        result.setMachineInLastAction
        result
    }
    
    static def List<Object> behaviorBody(Iterator<Action> iterator, boolean inNonTriggeredRoot, ClauseFragments knownFragments, Object nextAction) {
        val result = new ArrayList<Object>
        while(iterator.hasNext) {
            val action = iterator.next
            switch(action) {
                IfAction : {
                    if(isSplittable.apply(action)) {
                       //action has receptions on some branches
                       val ifAction = new IfHelperAction(action.guard)
                       result.add(ifAction)
                       //get the remaining part
                       val trail = behaviorBody(iterator, inNonTriggeredRoot, knownFragments, nextAction)
                       var Object afterIfAction
                       if(!(trail.get(0) instanceof StateTransitionAction)) {
                           //make a function fragment
                           val functionFragment = new FunctionFragment
                           functionFragment.machineName = knownFragments.machineName
                           functionFragment.addActions(trail)
                           knownFragments.addFunctionFragment(functionFragment)
                           afterIfAction = new FunctionFragmentCall(functionFragment)
                       } else {
                           afterIfAction = trail.get(0)
                       }
                       val thenBody = behaviorBody(action.thenList.actions.iterator, inNonTriggeredRoot, knownFragments, afterIfAction)
                       ifAction.thenActions =  thenBody
                       if(action.elseList !== null) {
                          val elseBody = behaviorBody(action.elseList.actions.iterator, inNonTriggeredRoot, knownFragments, afterIfAction)
                          ifAction.elseActions = elseBody   
                       }
                       return result
                    } else {
                        //normal if
                        result.add(action)
                    }
                }
                default : {
                    if(isPointOfSplit.apply(action)) {
                        if(action instanceof ActionWithVars) {
                            for(p : action.parameters) {knownFragments.addVariable(p)}
                            if(action instanceof ConnectionVariable) {
                                if(action.idVarDef !== null) knownFragments.addVariable(action.idVarDef)
                            }   
                        }
                        // make a new transition
                        val subTransition = new TransitionFragment
                        subTransition.machineName = knownFragments.machineName
                        knownFragments.addSubTransitionFragment(subTransition)
                        subTransition.addAction(action)
                        subTransition.addActions(behaviorBody(iterator, false, knownFragments, nextAction))
                        val callToSubTransition = new StateTransitionAction(subTransition)
                        if(inNonTriggeredRoot) {
                            callToSubTransition.setInNonTriggeredRoot
                        }
                        result.add(callToSubTransition)
                        return result
                    } else {
                        result.add(action)
                    }
                }
            }
        }
        result.add(nextAction)
        return result
    }    
}
