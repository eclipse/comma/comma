/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {
	public static Object deepCopy(Object source) {
        try
        {
            //Saving the object in a byte stream
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(byteOutStream); 
            out.writeObject(source); 
            out.close(); 
            ByteArrayInputStream inputStream = new ByteArrayInputStream(byteOutStream.toByteArray());
            ObjectInputStream in = new ObjectInputStream(inputStream) {
            	@Override
            	protected Class<?> resolveClass(ObjectStreamClass classDesc) throws IOException, ClassNotFoundException {
            		try {
            			return Class.forName(classDesc.getName());
            		} catch (Exception ex) {
            			// Load from the context class loader instead (used in e.g. GeneratorTest)
            			return Thread.currentThread().getContextClassLoader().loadClass(classDesc.getName());
            		}
            	}
            };
            Object o = in.readObject();
            in.close();
            return o;
        }
        catch(Exception ex) {
        	ex.printStackTrace();
            return null;
        }
	}

	public static double roundDouble(double value, int nrDigits) {
		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(nrDigits, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
