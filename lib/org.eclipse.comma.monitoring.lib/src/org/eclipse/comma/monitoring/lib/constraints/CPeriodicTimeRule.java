/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CEnvironment;
import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;

@SuppressWarnings("serial")
public class CPeriodicTimeRule extends CTimeRule {
	private CPeriodicTimeRuleEnvironment env;
	
	public CPeriodicTimeRule(CMessagePattern triggeringEvent, CMessagePattern periodicEvent, CEventSelector stopEventSelector) {
		errorMessage = "Event not observed in the expected period.";
		env = new CPeriodicTimeRuleEnvironment();
		
		formula = new CConditionalFollow().
		           setLeft(new CSequence(). 
		                   addElement(new CEventSelector().setEvent(triggeringEvent).setTimeVariable("t1").setEnvironment(env)).
		                   setEnvironment(env)).
		           setRight(new CSequence(). 
		                    addElement(new CUntil().
		                                 setBody(new CORStep().
		                                		 addSelector((CEventSelector) new CEventSelector().setEvent(periodicEvent).setTimeVariable("t2").setOccurenceVariable("c").setConditionIndex(1).setEnvironment(env)).
		                                		 addSelector((CEventSelector) new CEventSelector().setEvent(periodicEvent).setTimeVariable("t3").setConditionIndex(2).setNegated().setEnvironment(env))).
		                                 setStop((CStep) stopEventSelector.setTimeVariable("t4").setConditionIndex(3).setEnvironment(env)).
		                                 setWeak().
		                                 setEnvironment(env)).
		                    setEnvironment(env));
	}
	
	public CEnvironment getEnvironment() {
		return env;
	}
}
