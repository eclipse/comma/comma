/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.Optional;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.CMonitorFeedbackSender;

import com.google.gson.JsonStreamParser;

public class CTracePlayer {
	private String tracePath;
	private CFileTraceReader traceReader = null;
	private CFileJsonTraceReader jsonTraceReader = null;
	private CDispatcher dispatcher;
	private CTraceResults result;
	private CFactory factory;
	private CRecordsTracker recordsTracker;
	private JsonStreamParser parser;
	private CMonitorFeedbackSender feedbackSender;
	
	public CTracePlayer(CMonitoringContext context, String tracePath, JsonStreamParser parser) {
		this(context, tracePath, parser, null);
	}
	
	public CTracePlayer(CMonitoringContext context, String tracePath, JsonStreamParser parser, CMonitorFeedbackSender feedbackSender) {
		this.tracePath = tracePath;
		this.factory = context.getFactory();
		this.feedbackSender = feedbackSender;
		dispatcher = factory.createDispatcher(context);
		result = new CTraceResults(tracePath);
		recordsTracker = factory.getRecordsTracker();
		this.parser = parser;
	}
	
	public void run() {
		String formatOfEventFile = getEventFormatOf(tracePath);
		if(!formatOfEventFile.equals("events")) { //monitoring traces from .jsonl file
			try {
				jsonTraceReader = new CFileJsonTraceReader(tracePath,parser, feedbackSender);
				jsonTraceReader.processEvents(dispatcher, result, recordsTracker);
				return;
			} catch(Exception e) {
				handleTraceError(e.getMessage());
			}
			return;
		}
		
		try {
			traceReader = new CFileTraceReader(tracePath, recordsTracker);
			if(traceReader.traceInWrongFormat()) {
				handleTraceError("Trace file format error: missing events section");
				return;
			}
			if(traceReader.traceHasErrors()) {
				handleTraceError("Trace file contains errors: check the error section");
				return;
			}
		}catch (Exception e) {
			handleTraceError(e.getMessage());
			return;
		}
		
		CObservedMessage msg;
		try {
			Optional<CObservedMessage> optionalMessage = traceReader.readMessage();
			while(optionalMessage.isPresent()) {
				msg = optionalMessage.get();
				dispatcher.consume(msg);
				optionalMessage = traceReader.readMessage();
			}
			//end of trace
			dispatcher.traceEnded();
			//get the result
			result.setMonitorResults(dispatcher.getResults());
		} catch (Exception e) {
			//malformed trace
			String errorMsg = e.getMessage();
			if(errorMsg == null) {
				errorMsg = "Error reading from the trace. Line " + traceReader.getCurrentLine();
			}
			handleTraceError(errorMsg);
			try {
				dispatcher.traceEnded();
			} catch (Exception e1) {
				//do nothing, already in error situation
			}
			//get the result
			result.setMonitorResults(dispatcher.getResults());
		}
	}	
	
	public CTraceResults getResults() {
		return result;
	}
	
	private void handleTraceError(String msg) {
		result.markTraceError(msg);
	}
	
	private String getEventFormatOf(String eventFile) {
		return eventFile.substring(eventFile.lastIndexOf(".") + 1);
	}
}
