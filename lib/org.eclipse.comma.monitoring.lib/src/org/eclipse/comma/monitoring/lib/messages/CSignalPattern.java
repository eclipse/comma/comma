/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

@SuppressWarnings("serial")
public class CSignalPattern extends CMessagePattern {
	public CSignalPattern() {
		super();
	}

	public CSignalPattern(String name) {
		super(name);
	}

	public CSignalPattern(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}

	public boolean match(CObservedMessage observedMessage) {
		return observedMessage instanceof CObservedSignal && super.match(observedMessage);
	}

	public String toString() {
		String result = "Signal " + name + ". ";
		return result + printParameters() + ((preCondition != null)? " with a condition." : "");
	}
}
