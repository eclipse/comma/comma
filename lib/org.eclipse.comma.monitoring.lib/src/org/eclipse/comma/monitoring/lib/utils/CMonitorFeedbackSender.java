/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class CMonitorFeedbackSender implements Runnable{
	private int port;
	private ServerSocket serverSocket;
	private Socket clientSocket;
	private boolean connected = false;
	private PrintWriter out;
	private InputStream in;
	private Thread worker;
	private boolean exit = false;
	
	public CMonitorFeedbackSender(int port) {
		this.port = port;
	}
	
	public void start() {
		worker = new Thread(this);
		worker.start();
	}
	
	public void run() {
		try {
			serverSocket = new ServerSocket(port);
			while (!exit) {
				clientSocket = serverSocket.accept();
				out = new PrintWriter(clientSocket.getOutputStream(), true);
				in = clientSocket.getInputStream();
				connected = true;
				try {
					while(!exit) {
						in.read();
						Thread.sleep(100);
					}
				} catch (Exception e) {
				}
				finally {
					connected = false;
				}
				clientSocket.close();
			}
			serverSocket.close();
		}catch (IOException e) {
			connected = false;
		}
	}
	
	public synchronized void stop() {
		exit = true;
		try {
			if(connected) {
				out.println("stop");
				clientSocket.close();
			}
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void write(String msg) {
		if(connected) {
			out.println(msg);
		}
	}
}
