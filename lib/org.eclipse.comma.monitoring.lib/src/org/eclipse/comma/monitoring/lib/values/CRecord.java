/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;


@SuppressWarnings("serial")
public class CRecord implements Serializable {
	private String name; //fullyQualified name of record	
	private Map<String, Object> fieldsValue; //<name of field, the value of field>

	public CRecord(String recordName) {
		this.name = recordName;
		fieldsValue = new LinkedHashMap<String, Object>();
	}

	public Object getValueOfField(String field) {
		return fieldsValue.get(field);
	}

	public CRecord setValueOfField(String field, Object value) {
		fieldsValue.put(field, value);
		return this;
	}

	public String getRecord() {
		return name;
	}

	public boolean equals(Object o) {
		if(o == null || !(o instanceof CRecord)) 
			return false;

		CRecord obj = (CRecord) o;
		if(this.fieldsValue.size() != obj.fieldsValue.size() || !this.name.equals(obj.name))
			return false;

		//compare the fields name
		if (!this.fieldsValue.keySet().equals(obj.fieldsValue.keySet()))
			   return false;

		//compare value of the fields
		for(String key : this.fieldsValue.keySet()) {
			Object field = this.fieldsValue.get(key);
			if(!(field instanceof CAny) && !field.equals(obj.getValueOfField(key))) {
				return false;
			}
		}
		return true;
	}
	
	public String toString() {
		String result = "Record "+name + "{";
		for(String field : fieldsValue.keySet()) {
			result += field + "=" + fieldsValue.get(field).toString() + ", ";
		}
		return result.substring(0, result.length()-2) + "}";
	}
}
