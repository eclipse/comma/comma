/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("serial")
public class CMap<K, V> implements Serializable {
	private Map<K, V> map;

	public CMap() {
		map = new HashMap<K, V>();
	}

	private CMap(Map<K, V> map) {
		this.map = map;
	}

	public CMap<K, V> put(K key, V value){
		map.put(key, value);
		return this;
	}

	//TODO consider deepCopy instead
	public CMap<K, V> update(K key, V value){
		Map<K, V> clone = new HashMap<K, V>();
		clone.putAll(map);
		clone.put(key, value);
		return new CMap<K, V>(clone);
	}
	
	public <T> CMap<K, V> update(CVector<T> collection, Function<T, K> key, Function<T, V> value){
		Map<K, V> clone = new HashMap<K, V>();
		clone.putAll(map);
		for(T i : collection.getElements()) {
			clone.put(key.apply(i), value.apply(i));
		}
		return new CMap<K, V>(clone);
	}

	public boolean hasKey(K key) {
		return map.containsKey(key);
	}

	//TODO deepCopy?
	public CMap<K, V> deleteKey(Object key) {
		Map<K, V> clone = new HashMap<K, V>();
		clone.putAll(map);
		if(key instanceof CVector) {
			for(K k : ((CVector<K>) key).getElements()) {
				clone.remove(k);
			}
		}
		else {
			clone.remove(key);
		}
		return new CMap<K, V>(clone);
	}

	public <T> CVector<V> readSafe(CVector<T> collection, Function<T, K> key, V defaultValue) {
		CVector<V> result = new CVector<V>();
		for(T i : collection.getElements()) {
			K k = key.apply(i);
			if(map.containsKey(k)) {
				result.getElements().add(map.get(k));
			} else {
				result.getElements().add(defaultValue);
			}
		}
		return result;
	}
	
	public V readSafe(K key, V defaultValue) {
		if(map.containsKey(key)) return map.get(key);
		return defaultValue;
	}

	public Map<K, V> getMap(){
		return map;
	}

	public boolean equals(Object o) {
		if(o == null || !(o instanceof CMap<?, ?>))
			return false;
		@SuppressWarnings("unchecked")
		CMap<K, V> obj_ = (CMap<K, V>) o;
		return map.equals(obj_.getMap());
	}

	@Override
	public String toString() {
		return "Map";
	}
}
