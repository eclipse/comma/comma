/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

@SuppressWarnings("serial")
public class CReplyPattern extends CMessagePattern {
	private CCommandPattern commandPattern = null;

	public CReplyPattern() {
		super();
	}

	public CReplyPattern(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}

	public CReplyPattern setCommand(CCommandPattern command) {
		this.commandPattern = command;
		return this;
	}

	public CCommandPattern getCommand() {
		return commandPattern;
	}

	public boolean match(CObservedMessage observedMessage) {
		if(!(observedMessage instanceof CObservedReply)) 
			return false;
		CObservedReply observedReply = (CObservedReply) observedMessage;
		return super.match(observedMessage) && (commandPattern == null || commandPattern.match(observedReply.getCommand()));
	}

	public boolean match(CObservedMessage observedMessage, CEnvironment env) {
		if(match(observedMessage)) {
			bindVariables(env, observedMessage);
			if(commandPattern != null) {
				commandPattern.bindVariables(env, ((CObservedReply)observedMessage).getCommand());
			}
			return true;
		} else {
			return false;
		}
	}

	public String toString() {
		String result = "Reply to command " + name + ". ";
		return result + printParameters() + ((preCondition != null)? " with a condition." : "");
	}
}
