/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public abstract class CFunctionalConstraint {
	protected String componentInstanceName;
	protected String name;
	protected CFunctionalConstraintContext portsContext;
	
	public CFunctionalConstraint(String name, String componentInstanceName) {
		this.name = name;
		this.componentInstanceName = componentInstanceName;
	}
	
	public String getName() {
		return name;
	}
	
	public void setPortsContext(CFunctionalConstraintContext context) {
	    this.portsContext = context;
	}
	
	protected String determineContextPort(CObservedMessage msg) {
		if(componentInstanceName.equals(msg.getSource())) return msg.getSourcePort();
		if(componentInstanceName.equals(msg.getDestination())) return msg.getDestinationPort();
		return msg.getDestination().substring(componentInstanceName.length() + 1) + "." + msg.getDestinationPort();
	}
	
	protected String determineContextConnection(CObservedMessage msg) {
		if(componentInstanceName.equals(msg.getSource())) return msg.getDestination();
		if(componentInstanceName.equals(msg.getDestination())) return msg.getSource();
		return msg.getSource();
	}
	
	public abstract void setState(CFunctionalConstraintState state);
	public abstract CFunctionalConstraintResult consume(CObservedMessage message);
	public abstract CFunctionalConstraintState getInitialState();
}
