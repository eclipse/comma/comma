/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public abstract class CComponentConstraintMonitor {
	public abstract void consume(CObservedMessage message, List<CPathDescription> pathDescriptions, String port) throws Exception;
	public abstract CComponentConstraintResult getResults();
	public abstract void traceEnded();
	public abstract CComponentConstraintResult getResultForLastEvent();
}
