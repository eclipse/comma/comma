/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("serial")
public abstract class CMessage implements Serializable {
	protected String name;
	protected String source;
	protected String destination;
	protected String sourcePort;
	protected String destinationPort;
	protected List<Object> parameters;
	protected List<String> states;

	public CMessage() {
		this("*", "*", "*", "*", "*");
	}

	public CMessage(String name) {
		this(name, "*", "*", "*", "*");
	}

	public CMessage(String name, String source, String destination, String sourcePort, String destinationPort) {
		this.name = name;
		this.source = source;
		this.destination = destination;
		this.sourcePort = sourcePort;
		this.destinationPort = destinationPort;
		this.parameters = new ArrayList<Object>();
		this.states = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourcePort() {
		return sourcePort;
	}

	public void setSourcePort(String sourcePort) {
		this.sourcePort = sourcePort;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationPort() {
		return destinationPort;
	}

	public void setDestinationPort(String destinationPort) {
		this.destinationPort = destinationPort;
	}

	public CMessage addParameter(Object param) {
		parameters.add(param);
		return this;
	}

	public List<Object> getParameters() {
		return parameters;
	}

	public CMessage addState(String state) {
		states.add(state);
		return this;
	}

	public List<String> getStates(){
		return states;
	}

	//serializes parameter values as text in order to be used in trace log files 
	//created from monitoring and in error messages
	public String printParameters() {
		Iterator<Object> itr = parameters.iterator();
		String result = parameters.isEmpty() ? "" : "Parameters:";
		
		while(itr.hasNext()) {
			String parameter = itr.next().toString();
			result +=" " + parameter;
		}
		return result;
	}
}
