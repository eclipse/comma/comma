/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public abstract class CFactory implements Serializable {
	public abstract CRecordsTracker getRecordsTracker();
	public abstract CInterfaceDecisionClass createDecisionClass(String interfaceName);
	public abstract List<CComponentConstraintMonitor> createComponentConstraintMonitors(CComponentMonitoringContext context);
	
	//TODO consider moving this to context instead
	public CDispatcher createDispatcher(CMonitoringContext context) {
		if(context instanceof CInterfaceMonitoringContext) {
			if(context.reorderingRequired())
				return new CReorderDispatcher(new CInterfaceDispatcher((CInterfaceMonitoringContext) context, this));
			else
				return new CInterfaceDispatcher((CInterfaceMonitoringContext) context, this);
		} else {
			if(context.reorderingRequired())
				return new CReorderDispatcher(new CComponentDispatcher((CComponentMonitoringTaskContext) context, this));
			else
				return new CComponentDispatcher((CComponentMonitoringTaskContext) context, this);
		}
	}
}
