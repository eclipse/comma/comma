/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

@SuppressWarnings("serial")
public class CObservedSignal extends CObservedMessage {
	public CObservedSignal() {
		super();
	}
	
	public CObservedSignal(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}

	public String toString() {
		String result = super.toString() + "Signal " + name + ". " + "Time: " + timestamp;
		return result + " " + printParameters();
	}

	public String printUML() {
		String result = source + " ->> " + destination + ": " + "signal " + name;
		return result + printUMLParameters();
	}
}
