/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.util.List;
import java.util.function.BiFunction;

import org.eclipse.comma.monitoring.lib.CState;
import org.eclipse.comma.monitoring.lib.values.CVariable;

@SuppressWarnings("serial")
public class CMessagePattern extends CMessage{
	protected BiFunction<CState, List<Object>, Boolean> preCondition = null;
	protected String eventNameVar = null;

	public CMessagePattern() {
		super();
	}

	public CMessagePattern(String name) {
		super(name);
	}

	public CMessagePattern(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}
	
	public CMessagePattern setPrecondition(BiFunction<CState, List<Object>, Boolean> preCondition) {
		this.preCondition = preCondition;
		return this;
	}
	
	public CMessagePattern setEventNameVar(String vName) {
		eventNameVar = vName;
		return this;
	}
	
	private boolean checkStates(List<String> observedStates) {
		if(!states.isEmpty()) {
			if(observedStates.isEmpty()) {
				return false;
			} else {
				return states.contains(observedStates.get(0));
			}
		}
		return true;
	}

	private boolean checkOrigin(CObservedMessage observedMessage) {
		if(!source.equals("*") && !destination.equals("*")) {
			return (sourcePort.equals(observedMessage.getSourcePort()) && source.equals(observedMessage.getSource())) ||
				   (destinationPort.equals(observedMessage.getDestinationPort()) && destination.equals(observedMessage.getDestination()));

		}
		return (sourcePort.equals("*") || sourcePort.equals(observedMessage.getSourcePort())) &&
				   (destinationPort.equals("*") || destinationPort.equals(observedMessage.getDestinationPort())) &&
				   (source.equals("*") || source.equals(observedMessage.getSource())) &&
				   (destination.equals("*") || destination.equals(observedMessage.getDestination()));
	}

	private boolean checkParameters(List<Object> observedParameters) {
		if(parameters.isEmpty()) 
			return true;
		if(parameters.size() != observedParameters.size()) 
			return false;
		for(int i = 0; i < parameters.size(); i++) {
			if(!parameters.get(i).equals(observedParameters.get(i))) 
				return false;
		}
		return true;
	}

	private boolean checkPrecondition(CState observedInterfaceState, List<Object> params) {
		if(preCondition != null && observedInterfaceState != null) {
			return preCondition.apply(observedInterfaceState, params);
		}
		return true;
	}

	public boolean match(CObservedMessage observedMessage) {
		return (name.equals("*") || name.equals(observedMessage.getName())) &&
			   checkStates(observedMessage.getStates()) &&
			   checkOrigin(observedMessage) &&
			   checkParameters(observedMessage.getParameters()) &&
			   checkPrecondition(observedMessage.getInterfaceState(), observedMessage.getParameters());
	}

	public boolean match(CObservedMessage observedMessage, CEnvironment env) {
		if (match(observedMessage)) {
			bindVariables(env, observedMessage);
			return true;
		} else {
			return false;
		}
	}

	public void bindVariables(CEnvironment env, CObservedMessage observedMessage) {
		for(int i = 0; i < parameters.size(); i++) {
			if(parameters.get(i) instanceof CVariable) {
				env.setVariableValue(((CVariable)parameters.get(i)).getName(), observedMessage.getParameters().get(i));
			}
		}
		if(eventNameVar != null) {
			env.setVariableValue(eventNameVar, observedMessage.getName());
		}
	}
}
