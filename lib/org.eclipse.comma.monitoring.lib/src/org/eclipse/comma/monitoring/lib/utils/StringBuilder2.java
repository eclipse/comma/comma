/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.utils;

public class StringBuilder2 {
	private StringBuilder builder;
	private static final String LINE_DELIMITER = System.getProperty("line.separator");
	
	public StringBuilder2() {
		builder = new StringBuilder();
	}
	
	public void append(String content) {
		builder.append(content);
	}
	
	public String toString() {
		return builder.toString();
	}
	
	public void newLine() {
		builder.append(LINE_DELIMITER);
	}
}
