/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.Set;

@SuppressWarnings("serial")
public class CComponentMonitoringContext extends CMonitoringContext{
	protected String componentType;
	protected String componentInstanceName;
	protected Set<String> singletonPorts;
	protected CComponentTypeDescriptor componentDescriptor;
	
	public CComponentMonitoringContext(CFactory factory, String resultsDir) {
		super(factory, resultsDir);
	}
	
	public boolean isSingletonPort(String port) {
		return componentDescriptor.singletonPorts.contains(port);
	}
	
	public String getComponentInstanceName() {
		return componentInstanceName;
	}
	
	public CComponentMonitoringContext setComponentInstanceName(String name) {
		this.componentInstanceName = name;
		return this;
	}
	
	public String getComponentType() {
		return componentType;
	}
	
	public CComponentMonitoringContext setComponentDescriptor(CComponentTypeDescriptor d) {
		this.componentType = d.componentType;
		this.componentDescriptor = d;
		return this;
	}
	
	public CInterfaceMonitoringContext createComponentTimeDataContext() {
		CInterfaceMonitoringContext context = new CInterfaceMonitoringContext("", false, null, resultsDir).setConnectionName(componentInstanceName).setLocalDir(componentInstanceName + "/" + "time_data/");
		context.skipTimeConstraints(this.timeConstraintsSkipped);
		context.skipDataConstraints(this.dataConstraintsSkipped);
		return context;
	}
	
	public String getResultsFile(String constraintName) {
		return resultsDir + componentInstanceName + "/" + constraintName + "/" + componentInstanceName + "_" + constraintName + "_results.txt";
	}
	
	public String getTraceFile(String constraintName) {
		return resultsDir + componentInstanceName + "/" + constraintName + "/" + componentInstanceName + "_" + constraintName + "_trace.txt";
	}
	
	public String getErrorUMLFile(String constraintName) {
		return resultsDir + componentInstanceName + "/" + constraintName + "/" + componentInstanceName + "_" + constraintName + "_error_sequence_diagram.plantuml";
	}
	
}
