/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.comma.monitoring.lib.constraints.CRuleError;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CComponentResults extends CMonitorResults {
	@Expose
	private String componentInstanceName;
	@Expose
	@SerializedName("warnings")
	private List<CRuleError> constraintErrors = new ArrayList<CRuleError>();
	@Expose
	private List<CFunctionalConstraintMonitorResult> functionalConstraintResults = new ArrayList<CFunctionalConstraintMonitorResult>();	
	//in the diagram this is encoded as a class CComponentPortResults. Here it is a map from portName to a list of connection results
	@Expose
	private Map<String, List<CConnectionResults>> portResults = new HashMap<String, List<CConnectionResults>>();
	
	public CComponentResults(String componentInstanceName) {
		this.componentInstanceName = componentInstanceName;
	}
	
	public void addConnectionResult(CConnectionResults connectionResult) {
		coverageInfo.addAll(connectionResult.getCoverageInfo());
		String port = ((CPortConnectionMonitoringContext)connectionResult.getContext()).getPort();
		if(portResults.containsKey(port)) {
			portResults.get(port).add(connectionResult);
		} else {
			List<CConnectionResults> conResults = new ArrayList<CConnectionResults>();
			conResults.add(connectionResult);
			portResults.put(port, conResults);
		}
	}
	
	public void addConstraintErrors(List<CRuleError> constraintErrors) {
		this.constraintErrors.addAll(constraintErrors);
	}
	
	public void addFunctionalConstraintResults(List<CFunctionalConstraintMonitorResult> functionalConstraintResults) {
		this.functionalConstraintResults.addAll(functionalConstraintResults);
	}

	@Override
	public boolean hasIssues() {
		return !constraintErrors.isEmpty() || !functionalConstraintResults.isEmpty();
	}
}
