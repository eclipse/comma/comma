/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

@SuppressWarnings("serial")
public class CObservedNotification extends CObservedMessage {
	public CObservedNotification() {
		super();
	}
	public CObservedNotification(String name, String source, String destination, String sourcePort,
			String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}

	public String toString() {
		String result = super.toString() + "Notification " + name + ". " + "Time: " + timestamp;
		return result + " " + printParameters();
	}

	public String printUML() {
		String result = destination + " //- " + source + ": " + "notification " + name;		
		return result + printUMLParameters();
	}
}
