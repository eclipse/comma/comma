/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

@SuppressWarnings("serial")
public class CCommandPattern extends CMessagePattern {

	public CCommandPattern() {
		super();
	}

	public CCommandPattern(String name) {
		super(name);
	}

	public CCommandPattern(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}

	@Override
	public boolean match(CObservedMessage observedMessage) {
		return observedMessage instanceof CObservedCommand && super.match(observedMessage);
	}

	public String toString() {
		String result = "Command " + name + ". ";
		return result + printParameters() + ((preCondition != null)? " with a condition." : "");
	}
}
