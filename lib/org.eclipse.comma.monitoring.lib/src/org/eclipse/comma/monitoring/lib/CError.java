/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

import com.google.gson.annotations.Expose;

//root of the monitoring errors hierarchy
public abstract class CError {
	protected List<CObservedMessage> context = new ArrayList<CObservedMessage>();
	@Expose
	protected String errorDescription;
	@Expose
	protected List<String> contextError;
	@Expose
	protected String lastEventId = "";
	@Expose
	protected String umlFile = null;
	
	public List<CObservedMessage> getErrorContext(){
		return context;
	}
	
	public void setErrorContext(List<CObservedMessage> errorContext) {
		this.context = errorContext;
		if(!context.isEmpty()) {
			CObservedMessage last = context.get(context.size() - 1);
			lastEventId = last.getMessageId();
			
			//Used to export as a (JSON) result  
			contextError = new ArrayList<>();
			for(int i = 0; i < context.size(); i++) {
				contextError.add(context.get(i).toString());
			}
		}
	}
	
	public String getUMLFile() {
		return umlFile;
	}
	
	public void setUMLFile(String umlFile) {
		this.umlFile = umlFile;
	}
	
	protected String traceContextToText() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("Trace context that leads to error:"); builder.newLine(); builder.newLine();
		for(int i = 0; i < context.size(); i++) {
			if(i == 0) {
				builder.append(context.get(i).printStringWithState());
			} else {
				builder.append(context.get(i).toString());
			}
			builder.newLine();
		}
		return builder.toString();
	}
	
	protected String traceContextToUML() {
		StringBuilder2 builder = new StringBuilder2();
		for(int i = 0; i < context.size(); i++) {
			if (i == (context.size() - 1)) {
				builder.append(context.get(i).printUMLWithTime());
			} else if(i == 0) {
				builder.append(context.get(i).printUMLWithState());
			} else {
				builder.append(context.get(i).printUML());
			}
			builder.newLine();
		}
		return builder.toString();
	}
	
	abstract public String toString();
	
	abstract public String toUML();
}
