/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.List;

import org.eclipse.comma.monitoring.lib.constraints.CRule;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.Utils;

public abstract class CInterfaceDecisionClass {
	protected static CInterfaceDecisionResult noTransitions;
	protected static CInterfaceDecisionResult unknownEvent;
	static {
		noTransitions = new CInterfaceDecisionResult();
		noTransitions.resutKind = CInterfaceDecisionResultKind.NO_TRANSITIONS;
		
		unknownEvent = new CInterfaceDecisionResult();
		unknownEvent.resutKind = CInterfaceDecisionResultKind.RUNTIME_ERROR;
		unknownEvent.errorMessage = "Unknown interface event";
	}
	
	public abstract void setState(CState state);
	
	public abstract CState getState();
	
	public abstract CInterfaceDecisionResult consume(CObservedMessage message);
	
	public abstract List<CRule> getRules();
	
	//To be called when we leave a transition
	protected void updateState(CState state, String currentState, String targetState, CExecutionContext context) {
		state.setExecutionState(currentState);
		state.setActiveState(targetState);
		state.stateEnacted(targetState);
		context.setState((CState) Utils.deepCopy(state));
	}
	
	//On entry of a transition
	protected void enactClause(CState state, String clauseKey) {
		state.clauseEnacted(clauseKey);
	}
	
	//To be called when we move to a sub-transition
	protected void updateState(CState state, String currentState, CExecutionContext context) {
		state.setExecutionState(currentState);
		state.setActiveState(currentState);
		context.setState((CState) Utils.deepCopy(state));
	}
	
	protected CInterfaceDecisionResult checkTransitionsResult(List<CExecutionContext> contexts) {
		if (!contexts.isEmpty()) {
			CInterfaceDecisionResult result = new CInterfaceDecisionResult();
			result.expectedObservations = contexts;
			return result;
		} else {
			return noTransitions;
		}
	}
}
