/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public class CComponentMonitor {
	private List<CMessageInfo> eventQueue = new ArrayList<CMessageInfo>();
	private String componentInstance;
	private boolean inErrorState = false;
	private List<CComponentConstraintMonitor> monitors;
	private CComponentResults result;
	private CComponentResults resultPerEvent;
	
	public CComponentMonitor(CComponentMonitoringContext context) {
		this.componentInstance = context.getComponentInstanceName();
		this.monitors = context.getFactory().createComponentConstraintMonitors(context);
		this.result = new CComponentResults(this.componentInstance);
	}
	
	//always received from the component dispatcher regardless of the state
	public void traceEnded() throws Exception {
		if(!inErrorState) { //resolve the unknown states and flush the queue
			for(CMessageInfo mInfo : eventQueue) {
				for(CPathDescription pd : mInfo.getPathDescriptions()) {
					if(!pd.postStatesKnown()) {
						pd.setActiveStatesAsPostStates();
						pd.setPostStatesKnown();
					}
				}
				for(CComponentConstraintMonitor monitor : monitors) {
					monitor.consume(mInfo.getMessage(), mInfo.getPathDescriptions(), determinePort(mInfo.getMessage()));
				}
			}
		}
		for(CComponentConstraintMonitor monitor : monitors) {
			monitor.traceEnded();
			CComponentConstraintResult monResult = monitor.getResults();
			result.addConstraintErrors(monResult.getConstraintErrors());
			result.addFunctionalConstraintResults(monResult.getFunctionaConstraintResults());
		}
	}
	
	public CComponentResults getResults() {
		return result;
	}
	
	public CComponentResults getResultForLastEvent() {
		return resultPerEvent;
	}
	
	public void consume(CObservedMessage message, List<CPathDescription> pathDescriptions, String connection) throws Exception {
		if(inErrorState) {
			resultPerEvent = null;
			return;
		}
		resultPerEvent = new CComponentResults(componentInstance);
		CMessageInfo messageInfo = new CMessageInfo(message, pathDescriptions, connection);
		if(messageInfo.isComplete() && eventQueue.isEmpty()) {
			//paths are complete and queue is empty
			//send
			for(CComponentConstraintMonitor monitor : monitors) {
				monitor.consume(message, pathDescriptions, determinePort(message));
				CComponentConstraintResult r =  monitor.getResultForLastEvent();
				if(r == null) continue;
				resultPerEvent.addConstraintErrors(r.getConstraintErrors());
				resultPerEvent.addFunctionalConstraintResults(r.getFunctionaConstraintResults());
			}
		} else {
			resolveIncompletePaths(messageInfo);
			//now flush the complete prefix in the queue
			flushQueue();
		}
	}
	
	
	//private AbstractMap.SimpleEntry<String, String> determinePort(CObservedMessage message){
	private String determinePort(CObservedMessage message){
		if (message.getSource().equals(componentInstance)){
			return message.getSourcePort();
		}
		else if(message.getDestination().equals(componentInstance)) {
			return message.getDestinationPort();
		}
		return null;
	}
	
	public void connectionError(CConnectionResults connectionResult) {
		inErrorState = true;
		result.addConnectionResult(connectionResult);
	}
	
	public void connectionError( ) {
		inErrorState = true;
	}
	
	public void connectionMonitoringDone(CConnectionResults connectionResult) {
		result.addConnectionResult(connectionResult);
	}
	
	public boolean inErrorState() {
		return inErrorState;
	}
	
	private void resolveIncompletePaths(CMessageInfo messageInfo) {
		for(CMessageInfo mInfo : eventQueue) {
			if(mInfo.isSameConnection(messageInfo)) {
				if(!mInfo.isComplete()) {
					mInfo.resolvePostEventStates(messageInfo.getPathDescriptions());
					break;
				}
			}
		}
		eventQueue.add(messageInfo);
	}
	
	private void flushQueue() throws Exception {
		while(!eventQueue.isEmpty() && eventQueue.get(0).isComplete()) {
			for(CComponentConstraintMonitor monitor : monitors) {
				monitor.consume(eventQueue.get(0).getMessage(), eventQueue.get(0).getPathDescriptions(), determinePort(eventQueue.get(0).getMessage()));
				CComponentConstraintResult r=  monitor.getResultForLastEvent();
				if(r == null) continue;
				resultPerEvent.addConstraintErrors(r.getConstraintErrors());
				resultPerEvent.addFunctionalConstraintResults(r.getFunctionaConstraintResults());
			}
			eventQueue.remove(0);
		}
	}
}
