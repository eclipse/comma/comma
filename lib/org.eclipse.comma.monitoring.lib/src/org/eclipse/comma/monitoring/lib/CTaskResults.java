/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class CTaskResults {
	@Expose
	private String taskName;
	@Expose
	private String taskKind; //indicate what is monitored: interface or component
	@Expose
	private String modelName; //the name of the interface or the component being monitored
	@Expose
	private List<CTraceResults> traceResults;
	@Expose
	private List<CCoverageInfo> coverageInfo = new ArrayList<CCoverageInfo>(); //one for each used interfce in the task
	
	public CTaskResults(String taskName, String taskKind, String modelName) {
		this.taskName = taskName;
		this.taskKind = taskKind;
		this.modelName = modelName;
		traceResults = new ArrayList<CTraceResults>();
	}
	
	public void addTraceResults(CTraceResults traceResult) {
		traceResults.add(traceResult);
		boolean interfaceFound;
		for(CCoverageInfo newInfo : traceResult.getCoverageInfo()) {
			interfaceFound = false;
			for(CCoverageInfo info : coverageInfo) {
				if(info.getInterfaceName().equals(newInfo.getInterfaceName())) {
					info.union(newInfo);
					interfaceFound = true;
					break;
				}
			}
			if(!interfaceFound) {
				CCoverageInfo clonedNewInfo = new CCoverageInfo();
				clonedNewInfo.union(newInfo);
				clonedNewInfo.setInterfaceName(newInfo.getInterfaceName());
				coverageInfo.add(clonedNewInfo);
			}
		}
	}
}
