/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

@SuppressWarnings("serial")
public abstract class CBinaryFormula extends CFormula {
	
	protected CFormula left;
	protected CFormula right;
	protected CConstraintValue statusLeft = CConstraintValue.UNKNOWN;
	protected CConstraintValue statusRight = CConstraintValue.UNKNOWN;

	public CFormula getLeft() {
		return left;
	}
	
	public CBinaryFormula setLeft(CFormula left) {
		this.left = left;
		return this;
	}
	
	public CFormula getRight() {
		return right;
	}
	
	public CBinaryFormula setRight(CFormula right) {
		this.right = right;
		return this;
	}
}
