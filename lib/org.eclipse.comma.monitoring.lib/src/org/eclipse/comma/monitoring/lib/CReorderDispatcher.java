/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedNotification;
import org.eclipse.comma.monitoring.lib.messages.CObservedReply;

public class CReorderDispatcher extends CDispatcher {

	private CDispatcher next;
	private Map<String, List<CObservedNotification>> queues = new HashMap<String, List<CObservedNotification>>();
	
	public CReorderDispatcher(CDispatcher dispatcher) {
		this.next = dispatcher;
	}
	
	@Override
	public void consume(CObservedMessage message) throws Exception {
		if(message instanceof CObservedCommand) {
			String key = makeKey(message, true);
			queues.put(key, new ArrayList<CObservedNotification>());
			next.consume(message);
			return;
		}
		if(message instanceof CObservedNotification) {
			String key = makeKey(message, false);
			if(queues.containsKey(key)) {
				queues.get(key).add((CObservedNotification)message);
			} else {
				next.consume(message);
			}
			return;
		}
		if(message instanceof CObservedReply) {
			String key = makeKey(message, false);
			double timestamp = message.getTimeDelta();
			next.consume(message);
			for(CObservedNotification n : queues.get(key)) {
				n.setTimeDelta(timestamp);
				next.consume(n);
			}
			queues.remove(key);
			return;
		}
		next.consume(message);
	}

	@Override
	public void traceEnded() throws Exception {
		next.traceEnded();
	}

	@Override
	public List<CMonitorResults> getResults() {
		return next.getResults();
	}
	
	private String makeKey(CObservedMessage message, boolean isCall) {
		if(isCall) {
			return message.getSource() + "_" + message.getSourcePort() + "_" + message.getDestination() + "_" + message.getDestinationPort();
		} else {
			return message.getDestination() + "_" + message.getDestinationPort() + "_" + message.getSource() + "_" + message.getSourcePort();
		}
	}

	@Override
	public List<CMonitorResults> getIssuesForLastEvent() {
		return next.getIssuesForLastEvent();
	}

}
