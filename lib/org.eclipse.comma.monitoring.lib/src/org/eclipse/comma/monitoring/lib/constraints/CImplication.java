/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CImplication extends CBinaryFormula {

	@Override
	public CConstraintValue consume(CObservedMessage message) {
		if(statusLeft == CConstraintValue.UNKNOWN) {
			if( (statusLeft = left.consume(message)) == CConstraintValue.FALSE ) return CConstraintValue.TRUE;
		}
		if(statusRight == CConstraintValue.UNKNOWN) {
			statusRight = right.consume(message);
		}
		if(statusLeft == CConstraintValue.TRUE && statusRight == CConstraintValue.TRUE) return CConstraintValue.TRUE;
		if(statusLeft == CConstraintValue.TRUE && statusRight == CConstraintValue.FALSE) return CConstraintValue.FALSE;
		return CConstraintValue.UNKNOWN;
	}

}
