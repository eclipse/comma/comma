/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CORStep extends CStep {
	protected List<CStep> selectors = new ArrayList<CStep>();
		
	public CORStep addSelector(CStep s) {
		selectors.add(s);
		return this;
	}
		
	public List<CStep> getSelectors() {
		return selectors;
	}
	
	public CConstraintValue consume(CObservedMessage message) {
		boolean result = false;
		for(CStep selector : selectors) {
			if(selector.consume(message) == CConstraintValue.TRUE) result = true;
		}
		if (result) return CConstraintValue.TRUE;
		else return CConstraintValue.FALSE;
	}
}
