/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class CCoverageInfo {
	@Expose
	private String interfaceName = "";
	@Expose
	private Map<String, Integer> transitionCoverage = new LinkedHashMap<String, Integer>();
	@Expose
	private Map<String, Integer> stateCoverage = new LinkedHashMap<String, Integer>();
	@Expose
	private long transitionCoveragePercentage = 0L;
	@Expose
	private long stateCoveragePercentage = 0L;
	
	public void setInterfaceName(String name) {
		interfaceName = name;
	}
	
	public String getInterfaceName() {
		return interfaceName;
	}
	
	public void setTransitionCoverage(Map<String, Integer> coverage) {
		this.transitionCoverage = coverage;
	}
	
	public Map<String, Integer> getTransitionCoverage() {
		return transitionCoverage;
	}
	
	public void setStateCoverage(Map<String, Integer> coverage) {
		this.stateCoverage = coverage;
	}
	
	public Map<String, Integer> getStateCoverage() {
		return stateCoverage;
	}
	
	public void setTransitionCoveragePercentage(int percentage) {
		this.transitionCoveragePercentage = percentage;
	}
	
	public long getTransitionCoveragePercentage() {
		return transitionCoveragePercentage;
	}
	
	public void setStateCoveragePercentage(int percentage) {
		this.stateCoveragePercentage = percentage;
	}
	
	public long getStateCoveragePercentage() {
		return stateCoveragePercentage;
	}
	
	public void calculateCoveragePercentage(){
		transitionCoveragePercentage = Math.round(transitionCoverage.values().stream().filter(v -> v > 0).count() * 100.0 / transitionCoverage.size());
		stateCoveragePercentage = Math.round(stateCoverage.values().stream().filter(v -> v > 0).count() * 100.0 / stateCoverage.size());
	}
	
	public void union(CCoverageInfo cInfo) {
		for(String key : cInfo.getTransitionCoverage().keySet()) {
			transitionCoverage.put(key, transitionCoverage.containsKey(key) ? transitionCoverage.get(key) + cInfo.getTransitionCoverage().get(key) : cInfo.getTransitionCoverage().get(key));
		}
		for(String key : cInfo.getStateCoverage().keySet()) {
			stateCoverage.put(key, stateCoverage.containsKey(key) ? stateCoverage.get(key) + cInfo.getStateCoverage().get(key) : cInfo.getStateCoverage().get(key));
		}
		calculateCoveragePercentage();
	}
}
