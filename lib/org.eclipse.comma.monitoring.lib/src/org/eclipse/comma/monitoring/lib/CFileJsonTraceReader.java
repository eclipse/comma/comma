/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedNotification;
import org.eclipse.comma.monitoring.lib.messages.CObservedReply;
import org.eclipse.comma.monitoring.lib.messages.CObservedSignal;
import org.eclipse.comma.monitoring.lib.utils.CMonitorFeedbackSender;
import org.eclipse.comma.monitoring.lib.utils.Utils;
import org.eclipse.comma.monitoring.lib.values.CBulkdata;
import org.eclipse.comma.monitoring.lib.values.CEnumValue;
import org.eclipse.comma.monitoring.lib.values.CRecord;
import org.eclipse.comma.monitoring.lib.values.CVector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;
import com.google.gson.JsonSyntaxException;


public class CFileJsonTraceReader extends CTraceReader {	
	private String timeStampPreviousEvent;
	private CRecordsTracker recordsTracker;
	private JsonStreamParser parser;
	private double timeDelta;
	private CMonitorFeedbackSender feedbackSender;
	private boolean runtimeMonitoring = false;

	public CFileJsonTraceReader(String tracePath, JsonStreamParser streamParser, CMonitorFeedbackSender feedbackSender) throws IOException {
		this.feedbackSender = feedbackSender;
		timeStampPreviousEvent = "";
		timeDelta = 0.0;
		
		if(streamParser == null) {
			InputStream inputStream;
			try {
				inputStream = Files.newInputStream(Path.of(tracePath));
				this.parser = new JsonStreamParser(new InputStreamReader(inputStream, "UTF-8"));
			} catch (IOException e) {
				throw new IOException("the trace "+tracePath+" could not be read");
			}
		} else { //runtime monitoring
			this.parser = streamParser;
			runtimeMonitoring = true;
		}
	}
	

	public void processEvents(CDispatcher dispatcher, CTraceResults traceResults, CRecordsTracker recordsTracker)throws Exception {
		Optional<CObservedMessage> eventMessage = null;
		int lineNumberOfEvent = 0;
		String monitorType = dispatcher.getClass().getSimpleName();
		this.recordsTracker = recordsTracker;
		
		try {
			if(monitorType.equals("CComponentDispatcher") && parser.hasNext()) {
				if(!runtimeMonitoring) { //reading a trace from a file
					parser.next(); //ignore component(s) instances
					lineNumberOfEvent++;
				}
			}
			while (parser.hasNext()) {
				lineNumberOfEvent++;
	        	JsonElement currentEvent = parser.next();
	        	//validate if the currentEvent contains the required attributes
	        	if(!validateEvent(currentEvent, traceResults, lineNumberOfEvent)){
	        		return;
	        	}
	        	eventMessage = readMessage(currentEvent.getAsJsonObject(), lineNumberOfEvent);
	        	if(eventMessage.isPresent()) {
	        		dispatcher.consume(eventMessage.get());
	        		
	        		//sending feedback
	        		List<CMonitorResults> resultLastEvent = dispatcher.getIssuesForLastEvent();
	        		if(feedbackSender != null && !resultLastEvent.isEmpty()) {
	        			Gson gson = new GsonBuilder()
	        						.excludeFieldsWithoutExposeAnnotation()
	        						.create();
	        			String resultsInJson = gson.toJson(resultLastEvent);
	        			feedbackSender.write(resultsInJson);
	        		}
	        	} else {
	        		throw new IllegalArgumentException("Events not present");
	        	}
	        }
			dispatcher.traceEnded();
			//get the result
			traceResults.setMonitorResults(dispatcher.getResults());
		} catch(Exception e) {
			//malformed trace
			String errorMsg = e.getMessage();
			if(errorMsg == null) {
				errorMsg = "Error reading from event on line number  :  "+ lineNumberOfEvent;
			}
			traceResults.markTraceError(errorMsg);
			if(feedbackSender != null) {
    			Gson gson = new GsonBuilder()
    						.excludeFieldsWithoutExposeAnnotation()
    						.create();
    			String resultsInJson = gson.toJson(traceResults);
    			feedbackSender.write(resultsInJson);
    		}
			
			try {
				dispatcher.traceEnded();
			} catch (Exception e1) {
				//do nothing, already in error situation
			}
			//get the result
			traceResults.setMonitorResults(dispatcher.getResults());
			}
	}
	
	
	public Optional<CObservedMessage> readMessage() throws Exception {
		return null;
	}

	public  Optional<CObservedMessage> readMessage(JsonObject event, int lineNumberOfEvent) throws Exception {
		String kind = event.get("kind").getAsString();
        String interfaceName = event.get("interface").getAsString();
        String method = event.get("method").getAsString();
        String timeStamp = event.get("timeStamp").getAsString();
        String messageId = "lineNr"+lineNumberOfEvent;

        //initialize timeStampPreviousEvent for the first time
        if(timeStampPreviousEvent.isEmpty()) {
        	timeStampPreviousEvent = timeStamp;
        }

        timeDelta += getTimeDeltaOfEvents(timeStampPreviousEvent, timeStamp);

        timeStampPreviousEvent = timeStamp;

        String source = event.get("source").getAsString();
        String destination = event.get("destination").getAsString();
        String destinationPort = event.get("destinationPort").getAsString();
        String sourcePort = event.get("sourcePort").getAsString();

        CObservedMessage eventMessage = null;
        switch(kind) {
        case "Command":
        	eventMessage = new CObservedCommand(method, source, destination, sourcePort, destinationPort);
        	break;
        case "Signal": eventMessage = new CObservedSignal(method, source, destination, sourcePort, destinationPort);
			break;
        case "Notification": eventMessage = new CObservedNotification(method, source, destination, sourcePort, destinationPort);
			break;
        case "Reply": eventMessage = new CObservedReply(method, source, destination, sourcePort, destinationPort);
			break;
        default:
        	throw new Exception("Unknown transition action " + kind);
        }

        eventMessage.setInterface(interfaceName);
        eventMessage.setMessageId(messageId);
        eventMessage.setTimestamp(timeStamp);
        eventMessage.setTimeDelta(Utils.roundDouble(timeDelta, 3));

        JsonArray parametersArray = null;
        if(event.has("parameters")) {
        	if(!event.get("parameters").isJsonArray()) {
        		throw new JsonSyntaxException("parameters did not contain a JSON Array");
        	}
        	parametersArray = event.getAsJsonArray("parameters");
        	for (JsonElement parameter : parametersArray) {
        		eventMessage.addParameter(getCommaTypeOf(parameter));
        	}
        }
        return Optional.ofNullable(eventMessage);
	}

	public Object getCommaTypeOf(JsonElement parameter) throws Exception {
		JsonObject element = parameter.getAsJsonObject();
		String parameterType = element.get("type").getAsString();
		switch(parameterType) {
		case "int":
			return Long.valueOf(element.get("value").getAsLong());
		case "bool":
			return Boolean.valueOf(element.get("value").getAsBoolean());
		case "real":
			return Double.valueOf(element.get("value").getAsDouble());
		case "string":
			return element.get("value").getAsString();
		case "bulkdata":
			int size = element.get("value").getAsInt();
			return new CBulkdata(size);
		case "enum":
			return new CEnumValue(element.get("value").getAsString());
		case "vector":
			CVector<Object> vectorValue = new CVector<Object>();
			String typeOfElements = element.get("typeElem").getAsString();

			if(!(typeOfElements.equals("vector") || typeOfElements.equals("record"))) {
				if(!element.get("value").isJsonArray()) {
	        		throw new JsonSyntaxException("Vector did not contain a vector Array");
	        	}
				element.get("value").getAsJsonArray().forEach(item -> {
					JsonObject value = new JsonObject();
					value.addProperty("type", typeOfElements);
					value.add("value", item);
					try {
						vectorValue.addNew(getCommaTypeOf(value));
					} catch (Exception e) {
						throw new IllegalArgumentException(e);
					}
				});
			} else {
				if(!element.get("value").isJsonArray()) {
	        		throw new JsonSyntaxException("Vector did not contain a vector Array");
	        	}
				element.get("value").getAsJsonArray().forEach(itemArray -> {
					try {
						vectorValue.addNew(getCommaTypeOf(itemArray));
					} catch (Exception e) {
						throw new IllegalArgumentException(e);
					}
				});
			}
			return vectorValue;
		case "record":
			String recordName = element.get("record").getAsString();
			JsonObject recordFieldsFromTraceFile = element.get("value").getAsJsonObject();
			
			//record must have a fullyQualified name: interface_RecordName
			recordName = recordName.replace(".","_");
			
			CRecord record = new CRecord(recordName);
			Map<String, String> fieldsOfRecordInterfaceModel = recordsTracker.getFieldsOfRecord(recordName);
			
			for(Map.Entry<String, String> field : fieldsOfRecordInterfaceModel.entrySet()) {
				if(!recordFieldsFromTraceFile.has(field.getKey())) {
					throw new IllegalArgumentException("The record "+recordName+" does not contain the field "+field.getKey());
				}
				String nameOfField = field.getKey();
				String typeOfField = field.getValue();
				
				if(typeOfField.equals("record") || typeOfField.equals("vector")) {
					record.setValueOfField(nameOfField, getCommaTypeOf(recordFieldsFromTraceFile.get(nameOfField).getAsJsonObject()));
				} else {
					JsonObject f = new JsonObject();
					String valueField = recordFieldsFromTraceFile.get(nameOfField).getAsString();
					f.addProperty("type", typeOfField);
					f.addProperty("value", valueField);
					record.setValueOfField(nameOfField, getCommaTypeOf(f));
				}
			}
			return record;
		default:
			throw new Exception("unsupported type");
		}
	}

	private double getTimeDeltaOfEvents(String previousEvent, String currentEvent) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		double timeDelta = 0.0;
		try {
			LocalDateTime currentEventDateTime = LocalDateTime.parse(currentEvent, formatter);
	 		LocalDateTime previousEventDateTime = LocalDateTime.parse(previousEvent, formatter);

	 		Duration duration = Duration.between(previousEventDateTime, currentEventDateTime);
	 		if(duration.isNegative()) {
	 			throw new IllegalArgumentException("The timestamp of current event "+currentEventDateTime+" cannot be earlier than the previous event "+previousEventDateTime);
	 		}
	 		timeDelta = duration.toMillis();
		} catch (DateTimeParseException e) {
			 throw new IllegalArgumentException(currentEvent + " is given in wrong  date format. Date format must be of iso 8601 : yyyy-MM-ddTHH:mm:ss.SSSXXX ");
		}
		return timeDelta;
	}

	protected Object processRecord(JsonObject recordObject, String nameOfInterface) throws Exception {
		throw new Exception("unexpected record value. Model does not use record types.");
	}

	private boolean validateEvent(JsonElement event, CTraceResults traceResults, int lineNumberOfEvent) {
		List<String> requiredKeys = Arrays.asList("interface","method","kind","source","sourcePort","destination","destinationPort","timeStamp");
		for(String key : requiredKeys) {
			JsonObject objectKey = event.getAsJsonObject();
			if(!objectKey.has(key)) {
				traceResults.markTraceError("Event on line "+lineNumberOfEvent+": misses or incorrectly typed required key " +key);
				return false;
			}
			if(objectKey.get(key).isJsonNull() || objectKey.get(key).getAsString().isBlank()) {
				traceResults.markTraceError("Event on line "+lineNumberOfEvent+ ": value of key "+key +" cannot be null or empty");
				return false;
			}
			
		}
		return true;
	}
}



