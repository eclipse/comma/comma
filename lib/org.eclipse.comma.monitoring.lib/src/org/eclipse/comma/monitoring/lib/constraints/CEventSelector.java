/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CEnvironment;
import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.Utils;

@SuppressWarnings("serial")
public class CEventSelector extends CStep {
	
	protected String timeVariable;
	protected String occurenceVariable;
	protected long occurenceCounter;
	protected boolean isNegated;
	protected int conditionIndex;
	protected CMessagePattern event;
	
	public CEventSelector() {
		occurenceVariable = null;
		occurenceCounter = 0L;
		isNegated = false;
		conditionIndex = 0;
		event = null;
	}
	
	public CEventSelector setTimeVariable(String vName) {
		timeVariable = vName;
		return this;
	}
	
	public CEventSelector setOccurenceVariable(String vName) {
		occurenceVariable = vName;
		return this;
	}
	
	public CEventSelector setNegated() {
		isNegated = true;
		return this;
	}
	
	public CEventSelector setConditionIndex(int i) {
		conditionIndex = i;
		return this;
	}
	
	public CEventSelector setEvent(CMessagePattern e) {
		event = e;
		return this;
	}
	
	CMessagePattern getEvent() {
		return event;
	}

	@Override
	public CConstraintValue consume(CObservedMessage message) {
		CEnvironment tempEnvironment = (CEnvironment) Utils.deepCopy(env);
		boolean eventsMatched = event.match(message, tempEnvironment);
		if (isNegated) eventsMatched = !eventsMatched;
		//check the condition
		if ( eventsMatched && conditionIndex > 0) { //there is additional condition
			//bind the time variable
			tempEnvironment.setVariableValue(timeVariable, message.getTimeDelta());
			eventsMatched = tempEnvironment.checkCondition(conditionIndex, message);
		}
		//if so far everything is Ok, update the environment
		if(eventsMatched) {
			env.setVariableValue(timeVariable, message.getTimeDelta());
			if(!isNegated) { //propagate positive occurrences
				event.bindVariables(env, message);
			}
			if(occurenceVariable != null) {
				env.setVariableValue(occurenceVariable, ++occurenceCounter);
			}
			return CConstraintValue.TRUE;
		}
		return CConstraintValue.FALSE;
	}
	
}
