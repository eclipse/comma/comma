/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public class CMessageInfo {
	private CObservedMessage message;
	private List<CPathDescription> pathDescriptions;
	private String connection;
	private boolean complete = false;
	
	public CMessageInfo(CObservedMessage message, List<CPathDescription> paths, String connection) {
		this.message = message;
		this.pathDescriptions = paths;
		this.connection = connection;
		checkCompleteness();
	}
	
	public List<CPathDescription> getPathDescriptions() {
		return pathDescriptions;
	}
	
	public CObservedMessage getMessage() {
		return message;
	}
	
	public String getConnection() {
		return connection;
	}
	
	public boolean isComplete() {
		return complete;
	}
	
	public boolean isSameConnection(CMessageInfo mInfo) {
		return mInfo.getConnection().equals(connection);
	}
	
	public void resolvePostEventStates(List<CPathDescription> newPathDescriptions) {
		complete = true;
		for(CPathDescription pd : pathDescriptions) {
			if(!pd.postStatesKnown()) {
				for(CPathDescription newPd : newPathDescriptions) {
					String currentPath = pd.getPathId();
					String newPath = newPd.getPathId();
					if(newPath.startsWith(currentPath)) {
						pd.setPostEventStates(newPd.getActiveStates());
						break;
					}
				}
				pd.setPostStatesKnown();
			}
		}
	}
	
	private void checkCompleteness() {
		complete = pathDescriptions.stream().allMatch(pd -> pd.postStatesKnown());
	}
}
