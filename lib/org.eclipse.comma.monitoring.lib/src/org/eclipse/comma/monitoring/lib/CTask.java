/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

public class CTask {
	private String name;
	private String kind;
	private String modelName;
	private List<CTracePlayer> tracePlayers;
	
	public CTask(String name, String kind, String modelName){
		this.name = name;
		this.kind = kind;
		this.modelName = modelName;
		tracePlayers = new ArrayList<CTracePlayer>();
	}
	
	public String getName() {
		return name;
	}
	
	public void addTracePlayer(CTracePlayer player) {
		tracePlayers.add(player);
	}
	
	public void run() {
		for(CTracePlayer player : tracePlayers) {
			player.run();
		}
	}
	
	public CTaskResults getResults() {
		CTaskResults result = new CTaskResults(this.name, this.kind, this.modelName);
		for(CTracePlayer player : tracePlayers) {
			result.addTraceResults(player.getResults());
		}
		return result;
	}
}
