/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

public class CEntryPoint {
	public String receivingComponentInstance;
	public String receivingPort;
	public String actualComponentInstance;
	public String actualPort;
	
	public CEntryPoint(String receivingComponentInstance, String receivingPort, String actualComponentInstance, String actualPort) {
		this.receivingComponentInstance = receivingComponentInstance;
		this.receivingPort = receivingPort;
		this.actualComponentInstance = actualComponentInstance;
		this.actualPort = actualPort;
	}
}
