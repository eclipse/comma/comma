/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CEnvironment;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.values.CInterval;

@SuppressWarnings("serial")
public class CSimpleTimeRuleEnvironment extends CEnvironment {
	private double t1;
	private double t2;
	private double t3;
	private CInterval i;
	private double intervalEnd;
	private CSimpleTimeRule rule;

	public CSimpleTimeRuleEnvironment setInterval(CInterval newInterval) {
		i = newInterval;
		return this;
	}

	public CInterval getInterval() {
		return i;
	}

	public CSimpleTimeRuleEnvironment setIntervalEnd(double iEnd) {
		intervalEnd = iEnd;
		return this;
	}

	public CEnvironment setVariableValue(String name, Object value) {
		if(name.equals("t1")) t1 = (double) value;
		if(name.equals("t2")) t2 = (double) value;
		if(name.equals("t3")) t3 = (double) value;
		return this;
	}

	public void setRule(CSimpleTimeRule r) {
		rule = r;
	}

	public boolean checkCondition(int index, CObservedMessage message) {
		switch(index) {
		case 1:
			rule.notifyOnIntervalCheck(t3 - t1);
			return i.isIn(t3-t1);
		//Required by ConditionedEvent, ConditionedAbsenseOfEvent
		case 2:
			if(intervalEnd == -1.0) return true;
			return (t2 - t1) <= intervalEnd;
		default: return false;
		}
	}
}
