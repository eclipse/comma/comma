/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class CRuleObserver implements Serializable {
	protected String fileName;
	protected String ruleType;

	public CRuleObserver(String fileName, String ruleType) {
		this.fileName = fileName;
		this.ruleType = ruleType;
	}

	public abstract void ruleFailed(CRule rule);
	
	public abstract void ruleSucceeded(CRule rule);
		
	public FileWriter getFile() throws IOException {
		return new FileWriter(fileName, true);
	}

	public String printValue(Object o) {
		return o.toString();
	}
}
