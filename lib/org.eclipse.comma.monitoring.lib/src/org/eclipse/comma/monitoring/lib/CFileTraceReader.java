/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.File;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedNotification;
import org.eclipse.comma.monitoring.lib.messages.CObservedReply;
import org.eclipse.comma.monitoring.lib.messages.CObservedSignal;
import org.eclipse.comma.monitoring.lib.utils.Utils;
import org.eclipse.comma.monitoring.lib.values.CBulkdata;
import org.eclipse.comma.monitoring.lib.values.CEnumValue;
import org.eclipse.comma.monitoring.lib.values.CRecord;
import org.eclipse.comma.monitoring.lib.values.CVector;

public class CFileTraceReader extends CTraceReader{
	
	protected Scanner scanner;
	private boolean traceHasErrors;
	private boolean traceInWrongFormat;
	private int currentLine = 0;
	private double timeDelta = 0.0;
	private CRecordsTracker recordsTracker;
	
	public CFileTraceReader(String tracePath, CRecordsTracker recordsTracker) throws Exception {
		traceHasErrors = false;
		traceInWrongFormat = false;
		scanner = new Scanner(new File(tracePath));
		this.recordsTracker = recordsTracker;
		moveToEvents();
	}
	
	public boolean traceHasErrors() {
		return traceHasErrors;
	}
	
	public boolean traceInWrongFormat() {
		return traceInWrongFormat;
	}
	
	public int getCurrentLine() {
		return currentLine;
	}
	
	//returns:
	// - the next message if any
	// - null if end of file
	//Throws exception if the file has errors or is in wrong format
	public Optional<CObservedMessage> readMessage() throws Exception {		
		if(traceHasErrors || traceInWrongFormat) {
			throw new Exception("Trace contains errors");
		}
		String messageId = "";
		CObservedMessage result = null;
		while(scanner.hasNextLine()) {
			String line = scanner.nextLine().trim();
			currentLine++;
			if(line.isEmpty()) 
				continue;
			if(line.startsWith("//")) 
				continue;
			String[] words = line.split("\\s+");
			if(words.length == 1) { //check for message id
				if(words[0].startsWith("id")) {
					messageId = words[0];
					continue;
				} else {
					throw new Exception("Line " + currentLine + ": expected message id.");
				}
			}
			if(words.length != 9) throw new Exception("Line " + currentLine + ": wrong message format.");
			switch(words[0]) {
			case "Command": result = new CObservedCommand(words[8], words[3], words[5], words[4], words[6]);
					break;
			case "Signal": result = new CObservedSignal(words[8], words[3], words[5], words[4], words[6]);
				break;
			case "Notification": result = new CObservedNotification(words[8], words[3], words[5], words[4], words[6]);
				break;
			case "Reply": result = new CObservedReply(words[8], words[3], words[5], words[4], words[6]);
				break;
			default:
				throw new Exception("Line " + currentLine + ": unknown message kind.");
			}
			result.setInterface(words[7]);
			result.setMessageId(messageId);
			result.setTimestamp(words[1]);
			timeDelta += Double.parseDouble(words[2]);
			result.setTimeDelta(Utils.roundDouble(timeDelta*1000.0, 3));
			processParameters(result);
			break;
		}
		return Optional.ofNullable(result);
	}
	
	private void processParameters(CObservedMessage message) throws Exception {
		while(scanner.hasNextLine()) {
			currentLine++;
			String line ="";
			//a string data data type might have a value written over multi lines
			if(scanner.hasNext("string")) {
				line += scanner.nextLine().trim(); //read the first line
				//handle possible multiline strings
				while(!scanner.hasNext("int|real|bool|string|bulkdata|enum|vector|record|End")) {
					line += scanner.nextLine().trim();
				}
			}
			
			if(line.isEmpty()) {
				line = scanner.nextLine().trim();
			}
			
			if(line.equals("End")) {
				return;
			}
			Scanner lineScanner = new Scanner(line);
			
			message.addParameter(processValue(lineScanner.next(), lineScanner));
			if(lineScanner.hasNext()) { //some tokens remaining while the parameter is already processed
				throw new Exception("Line " + currentLine + ": remaining tokens after parameter value is processed");
			}
		}
		throw new Exception("Missing closing End for a message");
	}
	
	protected Object processValue(String type, Scanner scanner) throws Exception {
		switch(type) {
		case "int":
			return scanner.nextLong();
		case "real":
			String val = scanner.next();
			if(val.equals("NaN")) {
				return Double.NaN;
			}else {
				return Double.parseDouble(val);
			}
		case "bool":
			return scanner.nextBoolean();
		case "string":
			scanner.useDelimiter("\"");
			scanner.next(); //read up to first double quote
			String value = scanner.next();
			while(value.endsWith("\\")) {
				value = value.substring(0, value.length()-1) + "\"" + scanner.next();
			}
			scanner.reset();
			scanner.next(); //consume the trailing quote			
			return value;
		case "bulkdata":
			return new CBulkdata(scanner.nextInt());
		case "enum":
			return new CEnumValue(scanner.next() + "::" + scanner.next());
		case "vector":
			String baseType = scanner.next();
			int size = scanner.nextInt();
			CVector<Object> vectorValue = new CVector<Object>();
			for(int i = 1; i <= size; i++) {
				vectorValue.addNew(processValue(baseType, scanner));
			}
			String endToken = scanner.next(); //read the END
			if(!"END".equals(endToken)) {
				throw new Exception("Line " + currentLine + ": missing closing END for a vector");
			}
			return vectorValue;
		case "record": 
			String interfaceNameOfCurrentEvent = null;
			String recordName = scanner.next();
			if(recordName.equals("_commaInterface")) {
				interfaceNameOfCurrentEvent = scanner.next();
				recordName = scanner.next();
				
				//record must have a fullyQualified name: interface_RecordName
				recordName = recordName.substring(recordName.lastIndexOf(".") + 1);
				recordName = interfaceNameOfCurrentEvent+"_"+recordName;
			}
			
			CRecord record = new CRecord(recordName);
			Map<String, String> fieldsOfRecordInterfaceModel = recordsTracker.getFieldsOfRecord(recordName); 
			
			for(Map.Entry<String, String> field : fieldsOfRecordInterfaceModel.entrySet()) {
				String nameOfField = field.getKey();
				String typeOfField = field.getValue();
				record.setValueOfField(nameOfField, processValue(typeOfField,scanner));
			}
			scanner.next(); //read END
			return record;
			//return processRecord(scanner);
		default:
			throw new Exception("Line " + currentLine + ": unsupported type");
		}
	}
	
	protected Object processRecord(Scanner scanner) throws Exception {
		throw new Exception("Line " + currentLine + ": unexpected record value. Model does not use record types.");
	}
	
	private void moveToEvents() {
		while(scanner.hasNextLine()) {
			String line = scanner.nextLine().trim();
			currentLine++;
			if(line.equals("events")) {
				return;
			}
			if(line.equals("errors")) {
				traceHasErrors = true;
				return;
			}
		}
		traceInWrongFormat = true;
	}
	
}
