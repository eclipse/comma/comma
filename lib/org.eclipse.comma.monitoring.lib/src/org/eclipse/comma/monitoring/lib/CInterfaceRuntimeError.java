/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

public class CInterfaceRuntimeError extends CError {
	private CState interfaceState;
	
	public CInterfaceRuntimeError(CObservedMessage triggerMessage, String errorMessage, CState interfaceState){
		this.errorDescription = errorMessage;
		this.interfaceState = interfaceState;
	}
	
	@Override
	public String toString() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("Interface runtime error"); builder.newLine();
		builder.append(errorDescription); builder.newLine();
		if(interfaceState != null) {
			builder.newLine();
			builder.append("Active state: " + interfaceState.getActiveState()); builder.newLine();
			builder.append(interfaceState.toString()); builder.newLine();
		}
		builder.newLine();
		builder.append(traceContextToText());
		return builder.toString();
	}

	@Override
	public String toUML() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("@startuml"); builder.newLine();
		builder.append("title Interface runtime error: " + errorDescription); builder.newLine();
		builder.append(traceContextToUML());
		if(interfaceState != null) {
			builder.append("note right"); builder.newLine();
			builder.append("Active state: " + interfaceState.getActiveState()); builder.newLine();
			builder.append(interfaceState.toString() + "end note"); builder.newLine();
		}
		builder.append("@enduml"); builder.newLine();
		return builder.toString();
	}
}
