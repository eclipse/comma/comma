/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.util.ArrayList;
import java.util.List;

public class CMessageMultiplicityPattern extends CMessageCompositePattern {
	private int lower;
	private int upper;
	private CMessagePattern pattern;
	
	public CMessageMultiplicityPattern() {
		this(null, 1, 1);
	}

	public CMessageMultiplicityPattern(CMessagePattern pattern, int lower, int upper) {
		this.pattern = pattern;
		this.lower = lower;
		this.upper = upper;
	}

	public CMessageMultiplicityPattern setMessagePattern(CMessagePattern mp) {
		pattern = mp;
		return this;
	}

	@Override
	public boolean isSkippable() {
		return lower == 0;
	}

	@Override
	public CMatchResult match(CObservedMessage observedMessage) {
		if(currentInterfaceState != null) {
			observedMessage.setInterfaceState(currentInterfaceState);
		}
		if(pattern.match(observedMessage)) {
			lower = Integer.max(0, lower - 1);
			upper = Integer.max(-1, upper - 1);
			if(upper == 0) {
				return CMatchResult.DROP;
			} else {
				return CMatchResult.MATCH;
			}
		} else {
			if(lower == 0) {
				return CMatchResult.SKIP;
			} else {
				return CMatchResult.FAIL;
			}
		}
	}

	@Override
	public List<CMessagePattern> getPossibleEvents() {
		List<CMessagePattern> result = new ArrayList<CMessagePattern>();
		result.add(pattern);
		return result;
	}
}
