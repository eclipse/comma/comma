/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CEnvironment;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CSequence extends CFormula {
	protected int conditionIndex;
	protected CEnvironment env;
	protected List<CSequenceElement> elements;
	protected int currentElement;
	
	public CSequence() {
		elements = new ArrayList<CSequenceElement>();
		conditionIndex = 0;
		currentElement = 0;
	}
	
	public CSequence addElement(CSequenceElement element) {
		elements.add(element);
		return this;
	}

	public List<CSequenceElement> getElements(){
		return elements;
	}

	public CSequence setEnvironment(CEnvironment env) {
		this.env = env;
		return this;
	}

	public CEnvironment getEnvironment() {
		return env;
	}

	public CSequence setConditionIndex(int i) {
		conditionIndex = i;
		return this;
	}

	@Override
	public CConstraintValue consume(CObservedMessage message) {
		CConstraintValue r = elements.get(currentElement).consume(message);
		if(r == CConstraintValue.TRUE) {
			currentElement++;
			if(currentElement == elements.size()) { //end of sequence
				if(conditionIndex > 0) { //there is a condition
					if (env.checkCondition(conditionIndex, message)) {
						return CConstraintValue.TRUE;
					} else {
						return CConstraintValue.FALSE;
					}
				}else {
					return CConstraintValue.TRUE;
				}
			}
		}
		if(r == CConstraintValue.FALSE) return CConstraintValue.FALSE;
		return CConstraintValue.UNKNOWN;
	}
}
