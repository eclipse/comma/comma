/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.constraints.CRuleError;

public class CComponentConstraintResult {
	private List<CRuleError> constraintErrors = new ArrayList<CRuleError>();
	private List<CFunctionalConstraintMonitorResult> functionalConstraintResults = new ArrayList<CFunctionalConstraintMonitorResult>();
	
	public List<CRuleError> getConstraintErrors(){
		return constraintErrors;
	}
	
	public void addConstraintErrors(List<CRuleError> errors) {
		constraintErrors.addAll(errors);
	}
	
	public List<CFunctionalConstraintMonitorResult> getFunctionaConstraintResults(){
		return functionalConstraintResults;
	}
	
	public void addFunctionaConstraintResult(CFunctionalConstraintMonitorResult result) {
		functionalConstraintResults.add(result);
	}
}
