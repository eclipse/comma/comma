/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CConstraintSequence extends CFormula {
	protected int conditionIndex = 0;
	protected CSequence seq;
	
	public CConstraintSequence setConditionIndex(int i) {
		conditionIndex = i;
		return this;
	}
	
	public CConstraintSequence setSequence(CSequence s) {
		this.seq = s;
		return this;
	}
	
	public CSequence getSequence() {
		return seq;
	}

	@Override
	public CConstraintValue consume(CObservedMessage message) {
		CConstraintValue r = seq.consume(message);
		if( r == CConstraintValue.FALSE) return CConstraintValue.TRUE; //precondition fails
		if(r == CConstraintValue.TRUE) { //precondition true, check the data constraint
			if (! seq.getEnvironment().checkCondition(conditionIndex, message)) return CConstraintValue.FALSE;
			else return CConstraintValue.TRUE;
		}
		return CConstraintValue.UNKNOWN;
	}
}
