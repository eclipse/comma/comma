/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

import com.google.gson.annotations.Expose;

public class CRuleError {
	@Expose
	private String ruleName;
	@Expose
	private String errorMessage;
	private CObservedMessage firstEvent;
	private CObservedMessage lastEvent;
	@Expose
	private String umlFile;

	public CRuleError(String ruleName, String message, CObservedMessage firstEvent, CObservedMessage lastEvent) {
		this.ruleName = ruleName;
		this.errorMessage = message;
		this.firstEvent = firstEvent;
		this.lastEvent = lastEvent;
	}

	public CRuleError setUMLFile(String file) {
		umlFile = file;
		return this;
	}

	public String errorSummary() {
		return "Time/data constraint violation.";
	}

	public String toString() {
		String result = "";
		
		result += "Warning: constraint violated. Rule: " + ruleName + "\n";
		result += "=================================\n";
		result += errorMessage + "\n\n";
		result += "Trace:\n";
		if(firstEvent != lastEvent) {
			result += firstEvent.printStringWithState() + "\n";
			result += "... messages...\n";
			result += lastEvent.toString();
		} else {
			result += firstEvent.printStringWithState();
		}
		return result;
	}
	
	public String toUML() {
		String result = "";
		result += "@startuml\n";
		result += "title Warning (rule " + ruleName + "): " + errorMessage + "\n";
		if(firstEvent != lastEvent) {
			result += "group Event sequence\n";
			result += firstEvent.printUMLWithTimeAndState() + "\n";
			result += "... messages ...\n";
			result += lastEvent.printUMLWithTime() + "\n";
			result += "end\n";
		} else {
			result += firstEvent.printUMLWithTimeAndState() + "\n";
		}
		result += "@enduml\n";	
		return result;
	}
}
