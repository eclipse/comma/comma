/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

public class CFunctionalConstraintError extends CError {
	
	public CFunctionalConstraintError() {
		errorDescription = "Functional constraint monitoring error";
	}

	@Override
	public String toString() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("Functional constraint monitoring error"); builder.newLine();
		builder.newLine();
		builder.append(traceContextToText());
		return builder.toString();
	}

	@Override
	public String toUML() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("@startuml"); builder.newLine();
		builder.append("title Functional constraint monitoring error"); builder.newLine();
		builder.append(traceContextToUML());
		builder.append("@enduml"); builder.newLine();
		return builder.toString();
	}

}
