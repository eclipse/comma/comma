/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

public class CInterfaceMonitoringError extends CError {
	private List<CExecutionContext> processedContexts;
	private List<CMessagePattern> possibleEvents;
	
	public CInterfaceMonitoringError(CObservedMessage triggerMessage, List<CExecutionContext> processedContexts, List<CMessagePattern> possibleEvents) {
		this.processedContexts = processedContexts;
		this.possibleEvents = possibleEvents;
		this.writeErrorDescription();
	}
	
	public void writeErrorDescription () {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("Interface monitoring error.");builder.newLine();
		if(possibleEvents.isEmpty()) {
			builder.append("There is no transition for the event in the current state of the model");builder.newLine();
		}else{
			builder.append("The event does not conform to the model."); builder.newLine();
			builder.append("Possible events in the current state of the model: "); builder.newLine();
			for(CMessagePattern e : possibleEvents) {
				builder.append(e.toString()); builder.newLine();
			}
		}
		errorDescription = builder.toString().trim();
	}

	@Override
	public String toString() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append(errorDescription);
		builder.newLine();
		if(!processedContexts.isEmpty()) {
			builder.append("Execution states of the model:"); builder.newLine();builder.newLine();
			for(CExecutionContext context : processedContexts) {
				if(context.isExecuted()){
					builder.append("Active state: " + context.getState().getActiveState()); builder.newLine();
					builder.append(context.getState().toString());
				} else {
					builder.append("Active state: " + context.getState().getExecutionState()); builder.newLine();
					builder.append(context.getState().getSnapshot());
				}
				builder.newLine();
			}
		}
		builder.append(traceContextToText());
		return builder.toString();
	}

	@Override
	public String toUML() {
		StringBuilder2 builder = new StringBuilder2();
		builder.append("@startuml"); builder.newLine();
		builder.append("title Interface monitoring error: ");
		if(possibleEvents.isEmpty()) {
			builder.append("there is no transition for the event in the current state of the model");
		} else {
			builder.append("the event does not conform to the model");
		}
		builder.newLine();
		builder.append(traceContextToUML());
		if(!processedContexts.isEmpty()) {
			builder.append("note right"); builder.newLine();
			for(CExecutionContext context : processedContexts) {
				if(context != processedContexts.get(0)) {
					builder.append("----------------"); builder.newLine();
				}
				if(context.isExecuted()) {
					builder.append("Active state: " + context.getState().getActiveState()); builder.newLine();
					builder.append(context.getState().toString()); builder.newLine();
				} else {
					builder.append("Active state: " + context.getState().getExecutionState()); builder.newLine();
					builder.append(context.getState().getSnapshot()); builder.newLine();
				}
			}
			builder.append("end note"); builder.newLine();
		}
		builder.append("@enduml"); builder.newLine();
		return builder.toString();
	}
}
