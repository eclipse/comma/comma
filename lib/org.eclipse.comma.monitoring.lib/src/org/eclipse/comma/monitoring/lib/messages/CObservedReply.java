/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

@SuppressWarnings("serial")
public class CObservedReply extends CObservedMessage {
	protected CObservedCommand command = null;

	public CObservedReply() {
		super();
	}

	public CObservedReply(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}
	
	public void setCommand(CObservedCommand command) {
		this.command = command;
	}

	public CObservedCommand getCommand() {
		return command;
	}

	public String toString() {
		String result = super.toString() + "Reply to command " + name + ". " + "Time: " + timestamp;
		return result + " " + printParameters();
	}

	public String printUML() {
		String result = source + " --> " + destination + ": " + "reply";
	    result += printUMLParameters() + " to " + name;
	    return result;
	}
}
