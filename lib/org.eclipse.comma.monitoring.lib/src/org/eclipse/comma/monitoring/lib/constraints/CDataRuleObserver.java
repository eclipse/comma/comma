/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

@SuppressWarnings("serial")
public class CDataRuleObserver extends CRuleObserver {
	
	public CDataRuleObserver(String fileName, String ruleType) {
		super(fileName, ruleType);
	}
	
	private void handleEvent(CDataRule rule) {
		Map<String, Object> values = rule.observedValues();
		if(values == null) return;
		try {
			FileWriter file = getFile();
			for(String key : values.keySet()) {
				file.write(key + "," + printValue(values.get(key)) + "\n");
			}
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void ruleFailed(CRule rule) {
		if(rule instanceof CDataRule) {
			handleEvent((CDataRule)rule);
		}
	}
	
	public void ruleSucceeded(CRule rule) {
		if(rule instanceof CDataRule) {
			handleEvent((CDataRule)rule);
		}
	}
}
