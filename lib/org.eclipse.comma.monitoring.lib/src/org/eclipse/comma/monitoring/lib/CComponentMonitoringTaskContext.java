/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class CComponentMonitoringTaskContext extends CMonitoringContext{
	protected List<CEntryPoint> entryPoints;
	protected List<List<CPortInstance>> portPaths;
	protected List<String> componentInstancesInTrace;
	protected String componentType;
	protected Map<String, CComponentTypeDescriptor> componentDescriptors = new HashMap<String, CComponentTypeDescriptor>();
	
	public CComponentMonitoringTaskContext(String componentType, List<String> componentInstancesInTrace, CFactory factory, String resultsDir) {
		super(factory, resultsDir);
		this.componentType = componentType;
		this.componentInstancesInTrace = componentInstancesInTrace;
	}

	public void setEntryPoints(List<CEntryPoint> entryPoints) {
		this.entryPoints = entryPoints;
	}
	
	public CComponentTypeDescriptor getMonitoredComponentDescriptor() {
		return componentDescriptors.get(componentType);
	}
	
	
	public CComponentTypeDescriptor getComponentDescriptor(String componentType) {
		return componentDescriptors.get(componentType);
	}
	
	public List<CEntryPoint> getEntryPoints() {
		return entryPoints;
	}
	
	public void setPortPaths(List<List<CPortInstance>> portPaths) {
		this.portPaths = portPaths;
	}
	
	public List<List<CPortInstance>> getPortPaths() {
		return portPaths;
	}
	
	public List<String> getComponentInstances() {
		return componentInstancesInTrace;
	}
	
	public void addComponentTypeDescriptor(CComponentTypeDescriptor d) {
		componentDescriptors.put(d.componentType, d);
	}
	
	public CComponentMonitoringContext createComponentMonitoringContext(String instance, String componentType) {
		CComponentMonitoringContext context = new CComponentMonitoringContext(factory, resultsDir);
		context.skipTimeConstraints(timeConstraintsSkipped);
		context.skipDataConstraints(dataConstraintsSkipped);
		return context.setComponentInstanceName(instance).setComponentDescriptor(componentDescriptors.get(componentType));
	}
	
	//TODO interface can be obtained via the context!
	//TODO try to reduce the number of params!
	public CPortConnectionMonitoringContext createPortConnectionContext(String interfaceName, String port, String componentInstance, String connectionKey) {
		CPortConnectionMonitoringContext result = new CPortConnectionMonitoringContext(interfaceName, false, factory, resultsDir);
		result.setConnectionName(connectionKey).setLocalDir(componentInstance + "/" + "port_" + port + "/");
		result.setPort(port);
		return result;
	}
	
	public CInterfaceMonitoringContext createComponentTimeDataContext(String componentInstanceName) {
		CInterfaceMonitoringContext context = new CInterfaceMonitoringContext("", false, null, resultsDir).setConnectionName(componentInstanceName).setLocalDir(componentInstanceName + "/" + "time_data/");
		context.skipTimeConstraints(this.timeConstraintsSkipped);
		context.skipDataConstraints(this.dataConstraintsSkipped);
		return context;
	}
}
