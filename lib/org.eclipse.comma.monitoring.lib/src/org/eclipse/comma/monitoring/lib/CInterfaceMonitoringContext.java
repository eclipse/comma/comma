/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@SuppressWarnings("serial")
public class CInterfaceMonitoringContext extends CMonitoringContext {
	protected String localDir;
	protected String connectionName;
	protected String interfaceName;
	protected String resultsFolderOfConnection;
	protected boolean isSingleton = false;
	
	public CInterfaceMonitoringContext(String interfaceName, boolean isSingleton, CFactory factory, String traceResultsDir) {
		super(factory, traceResultsDir);
		this.interfaceName = interfaceName;
		this.isSingleton = isSingleton;
	}
	
	public CInterfaceMonitoringContext clone() {
		CInterfaceMonitoringContext context = new CInterfaceMonitoringContext(interfaceName, isSingleton, factory, resultsDir);
		context.skipTimeConstraints(this.timeConstraintsSkipped);
		context.skipDataConstraints(this.dataConstraintsSkipped);
		return context; 
	}
	
	public CInterfaceMonitoringContext setConnectionName(String connectionName) {
		this.connectionName = connectionName;
		return this;
	}
	
	public String getConnectionName() {
		return connectionName;
	}
	
	public CInterfaceMonitoringContext setLocalDir(String localDir) {
		this.localDir = localDir;
		resultsFolderOfConnection = getPath(Paths.get(resultsDir, localDir));
		return this;
	}
	
	public String getInterfaceName() {
		return interfaceName;
	}
	
	public boolean isSingleton() {
		return isSingleton;
	}
	
	public String resultsDir() {
		return resultsFolderOfConnection;
	}
	
	public String getTraceFile() {
		return resultsFolderOfConnection + connectionName + "_trace.txt";
	}
	
	public String getResultsFile() {
		return resultsFolderOfConnection + connectionName + "_results.txt";
	}
	
	public String getErrorUMLFile()  {
		return resultsFolderOfConnection + connectionName + "_error_sequence_diagram.plantuml";
	}
	
	public String getRuleErrorUMLFile(int i) {
		return resultsFolderOfConnection + connectionName + "_warning_" + i + "_sequence_diagram.plantuml";
	}
	
	public String getTimeStatisticsFileName() {
		return resultsDir + "../" + "/statistics/statisticsTime" + connectionName + ".statistics";
	}
	
	public String getDataStatisticsFileName()  {
		return resultsDir + "../" + "/statistics/statisticsData" + connectionName + ".statistics";
	}
	
	private String getPath(Path resultsDir) {
		try {
			Files.createDirectories(resultsDir);
		} catch(IOException e) {
			System.err.println("Failed to create directory!" + e.getMessage());
		}
		//transform directory path to linux format when created
		return 	resultsDir.toString().replace("\\","/").concat("/");
	}
}
