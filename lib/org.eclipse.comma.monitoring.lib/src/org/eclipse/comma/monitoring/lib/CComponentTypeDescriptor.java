/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.List;
import java.util.Set;

public class CComponentTypeDescriptor {
	public String componentType;
	public Set<String> singletonPorts; 
	public List<CPartDescriptor> partDescriptors = null;
}
