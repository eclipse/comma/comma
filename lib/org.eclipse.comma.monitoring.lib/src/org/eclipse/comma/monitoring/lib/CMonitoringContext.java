/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CMonitoringContext implements Serializable {
	protected String resultsDir;
	protected CFactory factory;
	protected boolean timeConstraintsSkipped = false;
	protected boolean dataConstraintsSkipped = false;
	protected boolean applyReordering = false;
	
	public CMonitoringContext(CFactory factory, String resultsDir) {
		this.factory = factory;
		this.resultsDir = resultsDir;
	}
	
	public CFactory getFactory() {
		return factory;
	}
	
	public boolean timeConstraintsSkipped() {
		return timeConstraintsSkipped;
	}
		
	public void skipTimeConstraints(boolean skip) {
		this.timeConstraintsSkipped = skip;
	}
	
	public boolean dataConstraintsSkipped() {
		return dataConstraintsSkipped;
	}
	
	public void skipDataConstraints(boolean skip) {
		this.dataConstraintsSkipped = skip;
	}
	
	public boolean reorderingRequired() {
		return applyReordering;
	}
	
	public void applyReordering(boolean reorder) {
		this.applyReordering = reorder;
	}
}
