/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CParallelCompositionPattern extends CMessageCompositePattern {
	private List<CMessageCompositePattern> elements = new ArrayList<CMessageCompositePattern>();

	public CParallelCompositionPattern addElement(CMessageCompositePattern element) {
		elements.add(element);
		return this;
	}

	@Override
	public boolean isSkippable() {
		for(int i = 0; i < elements.size(); i++) {
			if(!elements.get(i).isSkippable())
				return false;
		}
		return true;
	}

	@Override
	public CMatchResult match(CObservedMessage observedMessage) {
		boolean failed = false;
		if(currentInterfaceState != null) {
			observedMessage.setInterfaceState(currentInterfaceState);
		}
		for(int i = 0; i < elements.size(); i++) {
			CMatchResult result = elements.get(i).match(observedMessage);
			switch(result) {
			case MATCH: 
				return result;
			case DROP:
				if(elements.size() > 1) {
					elements.remove(i);
					return CMatchResult.MATCH;
				} else {
					return result;
				}
			case FAIL:
				failed = true;
				break;
			default:
				break;
			}
		}
		if(failed) {
			return CMatchResult.FAIL;
		} else {
			return CMatchResult.SKIP;
		}
	}

	@Override
	public List<CMessagePattern> getPossibleEvents() {
		return elements.stream().map(x -> x.getPossibleEvents()).flatMap(Collection::stream).collect(Collectors.toList());
	}
}
