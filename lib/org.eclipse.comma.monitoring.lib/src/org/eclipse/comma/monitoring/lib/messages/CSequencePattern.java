/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.util.ArrayList;
import java.util.List;

public class CSequencePattern extends CMessageCompositePattern {
	private List<CMessageCompositePattern> elements = new ArrayList<CMessageCompositePattern>();
	private int current = 0;

	public CSequencePattern addElement(CMessageCompositePattern element) {
		elements.add(element);
		return this;
	}

	public boolean isEmpty() {
		return current == elements.size();
	}

	@Override
	public boolean isSkippable() {
		if(isEmpty())
			return true;
		for(int i = current; i < elements.size(); i++) {
			if(!elements.get(i).isSkippable())
				return false;
		}
		return true;
	}

	@Override
	public CMatchResult match(CObservedMessage observedMessage) {
		CMatchResult result = elements.get(current).match(observedMessage);
		switch(result) {
		case DROP:
			if(current < elements.size()-1) {
				result = CMatchResult.MATCH;
			}
			current++;
			break;
		case SKIP:
			if(current < elements.size()-1) {
				current++;
				result = match(observedMessage);
			} else {
				current++;
			}
			break;
		default:
			break;
		}
		return result;
	}

	@Override
	public List<CMessagePattern> getPossibleEvents() {
		return elements.get(current).getPossibleEvents();
	}

}
