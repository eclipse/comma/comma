/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

import com.google.gson.annotations.Expose;

public class CFunctionalConstraintMonitorResult {
	@Expose
	private String constraintName;
	@Expose
	private CFunctionalConstraintError error = null;
	
	public CFunctionalConstraintMonitorResult(String constraintName, String traceFile) {
		this.constraintName = constraintName;
	}
	
	public void setError(CFunctionalConstraintError error) {
		this.error = error;
	}
	
	public CFunctionalConstraintError getError() {
		return error;
	}
	
	public boolean isSuccess() {
		return error == null;
	}
	
	public String toString() {
		StringBuilder2 builder = new StringBuilder2();
		if(isSuccess()) {
			builder.append("Success: no errors found.");
        } else {//there is an error
        	builder.append(error.toString());
        }
		return builder.toString();
	}
}
