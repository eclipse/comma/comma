/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("serial")
public class CFunctionalConstraintContext implements Serializable {
	private Map<String, Map<String, CPathDescription>> portsStates = new HashMap<String, Map<String, CPathDescription>>();
	private Map<String, Set<String>> initialPortStates;
	private List<CFunctionalConstraintState> constraintExecutionStates;
	
	private Set<String> getPortStates(String port, String connection, boolean isContext) {
		CPathDescription pd = portsStates.get(port).get(connection);
		if(pd.forwardPort == null) {
			return isContext ? pd.getActiveStates() : pd.getPostEventStates();
		} else {
			CPathDescription forwardPd = portsStates.get(pd.forwardPort).get(pd.forwardConnection);
			return isContext ? forwardPd.getActiveStates() : forwardPd.getPostEventStates();
		}
	}
	
	public Map<String, Map<String, CPathDescription>> getPortsStates() {
		return portsStates;
	}
		
	public void setPortsStates(Map<String, Map<String, CPathDescription>> newPortsStates) {
		portsStates = newPortsStates;
	}
	
	public void setInitialPortsStates(Map<String, Set<String>> initialValues) {
		initialPortStates = initialValues;
	}
	
	public List<CFunctionalConstraintState> getConstraintExecutionStates() {
		return constraintExecutionStates;
	}
	
	public void setConstraintExecutionStates(List<CFunctionalConstraintState> newStates) {
		constraintExecutionStates = newStates;
	}
	
	public boolean portInState(String port, String state, String contextPort, String contextConnection) {
		if(portsStates.containsKey(port)) {
			return allAtPortInState(port, new HashSet<>(Arrays.asList(state)), contextPort, contextConnection);
		} else {
			return initialPortStates.get(port).contains(state);
		}
	}
	
	private boolean isContext(String port, String connection, String contextPort, String contextConnection) {
		if(contextPort == null) return false;
		if(port.equals(contextPort) && connection.equals(contextConnection)) return true;
		CPathDescription pd = portsStates.get(port).get(connection);
		if(pd.forwardPort != null) {
			if(pd.forwardPort.equals(contextPort) && pd.forwardConnection.equals(contextConnection)) return true;
		}
		pd = portsStates.get(contextPort).get(contextConnection);
		if(pd.forwardPort != null) {
			if(pd.forwardPort.equals(port) && pd.forwardConnection.equals(connection)) return true;
		}
		return false;
	}
	
	public boolean allAtPortInState(String port, Set<String> states, String contextPort, String contextConnection) {
		//if the port has no connections yet the check is vacuously true
		//we check if the port has some communication
		if(!portsStates.containsKey(port)) return true;
		for(String connection : portsStates.get(port).keySet()) {
			Set<String> actualStates = getPortStates(port, connection, isContext(port, connection, contextPort, contextConnection));
			Set<String> intersection = new HashSet<String>(actualStates);
			intersection.retainAll(states);
			if(intersection.isEmpty()) return false;
		}
		return true;
	}
	
	public boolean someAtPortInState(String port, Set<String> states, String contextPort, String contextConnection, int lower, int upper) {
		int count = 0;
		Map<String, CPathDescription> connectionsAtPort = portsStates.get(port);
		if(connectionsAtPort != null) {
			for(String connection : connectionsAtPort.keySet()) {
				Set<String> actualStates = getPortStates(port, connection, isContext(port, connection, contextPort, contextConnection));
				Set<String> intersection = new HashSet<String>(actualStates);
				intersection.retainAll(states);
				if(!intersection.isEmpty()) count++;
			}
		}
	    return upper != -1 ? (lower <= count) && (count <= upper) : lower <= count;
	}
	
	public boolean connectionAtPortInState(String connection, String port, Set<String> states, String contextPort, String contextConnection) {
	
		Map<String, CPathDescription> connectionsAtPort = portsStates.get(port);
		if(connectionsAtPort == null) return false;
		CPathDescription connectionInfo = connectionsAtPort.get(connection);
		if(connectionInfo == null) return false;
		Set<String> actualStates = getPortStates(port, connection, isContext(port, connection, contextPort, contextConnection));
		Set<String> intersection = new HashSet<String>(actualStates);
		intersection.retainAll(states);
		if(intersection.isEmpty()) return false;
		return !intersection.isEmpty();
	}
}
