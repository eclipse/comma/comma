/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public class CTraceSerializer {
	private Queue<CObservedMessage> latestTrace;
	private static final int latestTraceMaxSize = 10;
	private String fileName;
	
	public CTraceSerializer(String fileName) {
		latestTrace = new LinkedList<CObservedMessage>();
		this.fileName = fileName;
	}

	public void processEvent(CObservedMessage message) throws Exception {
		if (latestTrace.size() ==  latestTraceMaxSize) {
			latestTrace.remove();
		}
		latestTrace.add(message);
		try {
            FileWriter writer = new FileWriter(fileName, true);
            writer.write(message.toString());
            writer.write("\r\n");
            writer.close();
        } catch (IOException e) {
            throw new Exception("Cannot serialize event. Possible cause: event from undeclared connection");
        }
	}

	public List<CObservedMessage> getLatestTrace() {
		return latestTrace.stream().collect(Collectors.toList());
	}
}
