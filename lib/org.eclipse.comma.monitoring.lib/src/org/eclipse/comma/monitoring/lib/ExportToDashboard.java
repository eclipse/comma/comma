/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import org.eclipse.comma.monitoring.dashboard.DashboardHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;

public class ExportToDashboard implements ExportResults {
	private static final String RESULTS_PATH = ""+"comma-gen"+File.separator;

	/*
	 * Expects a collections of the (tasks)results from the performed
	 * monitoring tasks.
	 * The results will be saved as a dashboard.
	 */
	public void export(List<CTaskResults> taskResults) {
		File file = new File(RESULTS_PATH + "dashboard.html");
		Gson gson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation()
				.create();
		JsonElement element = gson.toJsonTree(taskResults);

		try (FileOutputStream stream = new FileOutputStream(file)) {
			// Add statisitics and render umlFiles as base64 png string
			addStatistics(element);
			replaceUmlFileWithBase64(element);
			
			// The dashboard.htm contains many characters which are escaped by a string file writer.
			// Therefore we write bytes instead.

			byte[] resultsInJson = gson.toJson(element).getBytes(StandardCharsets.UTF_8);
			byte[] dashboardHTML = DashboardHelper.getHTML();
			if (dashboardHTML != null) {
				// Find where the %MONITORING_TASKS% in dashboardHTML is
				byte[] placeholder = "\"%MONITORING_TASKS%\"".getBytes(StandardCharsets.UTF_8);
				int placeholderStart = 0;
				int placeholderIndex = 0;
				for (int i = 0; i < dashboardHTML.length; i++) {
					if (dashboardHTML[i] == placeholder[placeholderIndex]) {
						if (placeholderIndex == 0) {
							placeholderStart = i;
						} else if (placeholderIndex == placeholder.length - 1) {
							break;
						}
						
						placeholderIndex++;
					} else {
						placeholderIndex = 0;
					}
				}
				
				// Write dashboard
			    stream.write(Arrays.copyOfRange(dashboardHTML, 0, placeholderStart));
			    stream.write(resultsInJson);
			    stream.write(Arrays.copyOfRange(dashboardHTML, placeholderStart + placeholder.length, dashboardHTML.length));
			} else {
				String content = "Dashboard not present, likely you forgot to build Comma with the -Pdashboard flag, see developing.md";
				stream.write(content.getBytes(StandardCharsets.UTF_8));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addStatistics(JsonElement root) throws IOException {
		for (JsonElement task : root.getAsJsonArray()) {
			JsonArray statisticsJson = new JsonArray();
			String name = task.getAsJsonObject().get("taskName").getAsString();
			File statistics = new File(RESULTS_PATH + name + File.separator + "statistics");
			if (statistics.exists()) {
				for (String statistic : statistics.list()) {
					Path statisticPath = Paths.get(statistics.getPath(), statistic);
					JsonArray content = new JsonArray();
					for (String line : Files.readString(statisticPath).split("\n")) {
						content.add(line);
					}

					JsonObject statisticJson = new JsonObject();
					statisticJson.addProperty("file", statisticPath.getFileName().toString());
					statisticJson.add("content", content);
					statisticsJson.add(statisticJson);
				}
			}
			task.getAsJsonObject().add("statistics", statisticsJson);
		}
	}

	private String renderUmlFileAsBase64(String umlFile) throws IOException {
		String content = Files.readString(Paths.get(umlFile));
		SourceStringReader  reader = new SourceStringReader(content);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
        reader.outputImage(os, new FileFormatOption(FileFormat.PNG));
        return Base64.getEncoder().encodeToString(os.toByteArray());
	}

	private void replaceUmlFileWithBase64(Object object) throws IOException {
		if (object instanceof JsonArray) {
			JsonArray jsonArray = (JsonArray) object;
			for (int i = 0; i < jsonArray.size(); i++) {
				replaceUmlFileWithBase64(jsonArray.get(i));
			}
		} else if (object instanceof JsonObject) {
			JsonObject jsonObject = (JsonObject) object;
			for (String key : jsonObject.keySet()) {
				if (key.equals("umlFile")) {
					String umlFile = jsonObject.get(key).getAsString();
					String base64 = renderUmlFileAsBase64(umlFile);
					jsonObject.addProperty("umlBase64", base64);
					jsonObject.remove(key);
				} else {
					replaceUmlFileWithBase64(jsonObject.get(key));
				}
			}
		}
	}
}
