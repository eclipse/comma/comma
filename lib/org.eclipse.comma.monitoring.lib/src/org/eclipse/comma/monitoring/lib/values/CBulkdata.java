/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CBulkdata implements Serializable {
	private int size;

	public CBulkdata(int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof CBulkdata && ((CBulkdata) obj).getSize() == size;
	}

	@Override
	public String toString() {
		return "bulkdata";
	}
}
