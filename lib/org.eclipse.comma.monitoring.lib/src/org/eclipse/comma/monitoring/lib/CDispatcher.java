/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public abstract class CDispatcher {
	protected Map<String, CInterfaceMonitor> connection2monitor;
	
	public CDispatcher() {
		connection2monitor = new HashMap<String, CInterfaceMonitor>();
	}
	
	protected String makeMultitonConnectionKey(String destination, String source, String interfaceName) {
		return destination + "_" + source + "_" + interfaceName;
	}
	
	protected String makeSingletonConnectionKey(String destination, String port, String interfaceName) {
		return destination + "_" + port + "_" + interfaceName;
	}
	
	public abstract void consume(CObservedMessage message) throws Exception;
	
	public abstract List<CMonitorResults> getIssuesForLastEvent();
	
	//end of trace
	public abstract void traceEnded() throws Exception;
	
	public abstract List<CMonitorResults> getResults();
}
