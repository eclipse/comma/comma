/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.comma.monitoring.lib.constraints.CDataRuleObserver;
import org.eclipse.comma.monitoring.lib.constraints.CRule;
import org.eclipse.comma.monitoring.lib.constraints.CRuleError;
import org.eclipse.comma.monitoring.lib.constraints.CRulesExecutor;
import org.eclipse.comma.monitoring.lib.constraints.CTimeRuleObserver;
import org.eclipse.comma.monitoring.lib.messages.CMatchResult;
import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;
import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedNotification;
import org.eclipse.comma.monitoring.lib.messages.CObservedReply;
import org.eclipse.comma.monitoring.lib.messages.CObservedSignal;
import org.eclipse.comma.monitoring.lib.utils.CTraceSerializer;
import org.eclipse.comma.monitoring.lib.utils.Utils;

public class CInterfaceMonitor {
	private CInterfaceMonitoringContext context;
	private List<CExecutionContext> executionContexts;
	private CInterfaceDecisionClass decisionClass;
	private List<CPathDescription> pathDescriptions = null;
	private CObservedCommand lastSeenCommand;
	private CConnectionResults monitoringResult;
	private CConnectionResults resultPerEvent;
	private boolean inErrorState;
	private CTraceSerializer traceSerializer;
	private CRulesExecutor rulesRunner;

	public CInterfaceMonitor(CInterfaceMonitoringContext context, CFactory factory) {
		this.context = context;
		executionContexts = new ArrayList<CExecutionContext>();
		CExecutionContext initialContext = new CExecutionContext();
		initialContext.setExecutionPathId("p");
		executionContexts.add(initialContext);
		lastSeenCommand = null;
		monitoringResult = new CConnectionResults(context);
		inErrorState = false;
		traceSerializer = new CTraceSerializer(context.getTraceFile());
		decisionClass = factory.createDecisionClass(context.getInterfaceName());
		rulesRunner = new CRulesExecutor(context);
		List<CRule> rules = decisionClass.getRules();
		registerRuleObservers(rules);
		rulesRunner.setListOfRules(rules); 
		initialContext.setRulesExecutor(rulesRunner);
	}
	
	private void registerRuleObservers(List<CRule> rules) {
		CTimeRuleObserver observerTR = new CTimeRuleObserver(context.getTimeStatisticsFileName(), "time");
		CDataRuleObserver observerDR = new CDataRuleObserver(context.getDataStatisticsFileName(), "data");
		for(CRule r : rules) {
			r.registerObserver(observerTR);
			r.registerObserver(observerDR);
		}
	}
	
	public boolean inErrorState() {
		return inErrorState;
	}
	
	private void executeRules(CObservedMessage m, CExecutionContext context, String state) {
		CObservedMessage newObservation = m.clone();
		newObservation.addState(state);
		List<CRuleError> constraintErrors = context.consumeEvent(newObservation);
		monitoringResult.addConstraintErrors(constraintErrors);
		resultPerEvent.addConstraintErrors(constraintErrors);
	}
	
	private void setRuleExecutor(CExecutionContext currentContext, CExecutionContext newContext) {
		newContext.setRulesExecutor((CRulesExecutor)Utils.deepCopy(currentContext.getRulesExecutor()));
	}
	
	private void serializeResults() {
		try {
            FileWriter writer = new FileWriter(context.getResultsFile(), true); //append the content
            writer.write(monitoringResult.toString());
            writer.close();
            if(! monitoringResult.isSuccess()){ //serialize the errors in UML file
            	for(CError error : monitoringResult.getMonitoringErrors()) {
            		//write in plantUML file
            		String umlErrorFileName = context.getErrorUMLFile();
            		FileWriter umlWriter = new FileWriter(umlErrorFileName, false);
            		umlWriter.write(error.toUML());
            		umlWriter.close();
            		error.setUMLFile(umlErrorFileName);
            	}
            }
            List<CRuleError> ruleErrors = monitoringResult.getConstraintErrors();
            for(int i = 0; i < ruleErrors.size(); i++) {
        		//write in plantUML file
        		String umlRuleErrorFileName = context.getRuleErrorUMLFile(i);
        		FileWriter umlWriter = new FileWriter(umlRuleErrorFileName, false);
        		umlWriter.write(ruleErrors.get(i).toUML());
        		umlWriter.close();
        		ruleErrors.get(i).setUMLFile(umlRuleErrorFileName);
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public void traceEnded() {
		if(!inErrorState) {
			for(CExecutionContext context : executionContexts) {
				CState s = context.getState();
				monitoringResult.addExecutionState(s);
			}
		}
		serializeResults();
	}
	
	public CConnectionResults getResults() {
		return monitoringResult;
	}
	
	public CConnectionResults getResultForLastEvent() {
		return resultPerEvent;
	}
	
	private boolean isTraceMalformed(CObservedMessage message) {
		String errorMessage = null;
		if(message instanceof CObservedCommand) {
			if(lastSeenCommand == null) {
				lastSeenCommand = (CObservedCommand) message;
			} else {
				errorMessage = "Malformed trace: nested blocking calls.";
			}
		} else if(message instanceof CObservedReply) {
			if(lastSeenCommand != null) {
				((CObservedReply) message).setCommand(lastSeenCommand);
				lastSeenCommand = null;
			} else {
				errorMessage = "Malformed trace: reply without a call.";
			}
		} else if(message instanceof CObservedSignal && lastSeenCommand != null) {
			errorMessage = "Malformed trace: signal sent before reply to a blocking call.";
		}
		if(errorMessage != null) {
			CMalformedTraceError error = new CMalformedTraceError(errorMessage);
			error.setErrorContext(traceSerializer.getLatestTrace());
			monitoringResult.addMonitoringError(error);
			resultPerEvent.addMonitoringError(error);
			inErrorState = true;
			return true;
		}
		return false;
	}
	
	public boolean consume(CObservedMessage message) throws Exception {
		if(inErrorState) return false;
		traceSerializer.processEvent(message);
		resultPerEvent = new CConnectionResults(context);
		if(isTraceMalformed(message)) {
			serializeResults();
			return false;
		}
		List<CExecutionContext> newExecutionContexts = new ArrayList<CExecutionContext>();
		List<CExecutionContext> processedContexts = new ArrayList<CExecutionContext>();
		List<CMessagePattern> possibleEvents = new ArrayList<CMessagePattern>();
		pathDescriptions = new ArrayList<CPathDescription>();
		
		//Start actual processing
		
		//iterate over all the execution contexts
		int i = 0;
		while(i < executionContexts.size()) {
			CExecutionContext context = executionContexts.get(i);
			int oldNumberNewContexts = newExecutionContexts.size();
			if(context.expectedMessagesEmpty()) {
				//no pending expected messages. Send the new message
				CState state = context.getState();
				if(state != null) {//state may be nil only in the beginning
					context.setExecuted(true);
					processedContexts.add(context);
					decisionClass.setState(state);
				} else {
					//this is the first message
					//state is needed for the purpose of constraint rules
					state = decisionClass.getState();
				}
				if(message instanceof CObservedCommand || message instanceof CObservedSignal) {
					message.setInterfaceState(state);
				}
				CInterfaceDecisionResult result = decisionClass.consume(message);
				if(result.resutKind == CInterfaceDecisionResultKind.RUNTIME_ERROR) {
					CInterfaceRuntimeError error = new CInterfaceRuntimeError(message, result.errorMessage, state);
					error.setErrorContext(traceSerializer.getLatestTrace());
					monitoringResult.addMonitoringError(error);
					resultPerEvent.addMonitoringError(error);
					inErrorState = true;
					serializeResults();
					return false;
				}
				if(result.resutKind == CInterfaceDecisionResultKind.SUCCESS) {
					postProcessMessage(context, newExecutionContexts, result.expectedObservations, possibleEvents, message);
				}
			}else { //There are messages in the queue
				context.setExecuted(false);
				processedContexts.add(context);
				List<CMessagePattern> oldPossibleEvents = new ArrayList<CMessagePattern>(possibleEvents);
				possibleEvents.addAll(context.getPossibleEvents());
				CMatchResult matchResult = matchExpectedObservation(message, context, newExecutionContexts);
				if(matchResult == CMatchResult.MATCH || matchResult == CMatchResult.DROP) {
					executeRules(message, context, context.getState().getExecutionState());
				} else if(matchResult == CMatchResult.SKIP) {
					processedContexts.remove(processedContexts.size()-1);
					possibleEvents = oldPossibleEvents;
					i--;
				}
			}
			//form the execution paths
			//TODO rethink after understanding
			int j = 0; //was 1
			while(j < newExecutionContexts.size() - oldNumberNewContexts){  //was <=
				if (newExecutionContexts.size() - oldNumberNewContexts == 1) {
					newExecutionContexts.get(newExecutionContexts.size() - 1).setExecutionPathId(context.getExecutionPathId());
					pathDescriptions.get(pathDescriptions.size()-1).setPathId(context.getExecutionPathId());
				} else {
					newExecutionContexts.get(oldNumberNewContexts + j).setExecutionPathId(context.getExecutionPathId() + j);
					pathDescriptions.get(oldNumberNewContexts + j).setPathId(context.getExecutionPathId() + j);
				}
				j++;
			}
			i++;
		}
		if(newExecutionContexts.isEmpty()) {
			CInterfaceMonitoringError error = new CInterfaceMonitoringError(message, processedContexts, possibleEvents);
			error.setErrorContext(traceSerializer.getLatestTrace());
			monitoringResult.addMonitoringError(error);
			resultPerEvent.addMonitoringError(error);
			inErrorState = true;
			serializeResults();
			return false;
		}else {
			executionContexts = newExecutionContexts;
		}
		return true;
	}
	
	List<CPathDescription> getPathDescriptions() {
		return pathDescriptions;
	}
	
	private void postProcessMessage(CExecutionContext currentContext, List<CExecutionContext> newContexts, List<CExecutionContext> receivedContexts, List<CMessagePattern> possibleEvents, CObservedMessage message) {
		if(message instanceof CObservedNotification || message instanceof CObservedReply) {
			boolean executed = false;
			for(CExecutionContext receivedContext : receivedContexts) {
				if (!receivedContext.expectedMessagesEmpty()) {
					possibleEvents.addAll(receivedContext.getPossibleEvents());
				}
				CMatchResult matchResult = matchExpectedObservation(message, receivedContext, newContexts);
				if (matchResult == CMatchResult.MATCH || matchResult == CMatchResult.DROP) {
					if (! executed) {
						executeRules(message, currentContext, receivedContext.getState().getExecutionState());
						executed = true;
					}
				    setRuleExecutor(currentContext, receivedContext);
				}
			}
		}else {
			//no need for post processing
			if(receivedContexts.isEmpty()) return;
			executeRules(message, currentContext, receivedContexts.get(0).getState().getExecutionState()); 
			for(CExecutionContext context : receivedContexts) {
				CState receivedState = context.getState();
				CPathDescription pathDescription = new CPathDescription();
				context.setPathDescription(pathDescription);
				Set<String> activeStates = new HashSet<String>(receivedState.getActiveStates());
				activeStates.remove(receivedState.getActiveState());
				activeStates.add(receivedState.getExecutionState());
				pathDescription.setActiveStates(activeStates);
				if (context.expectedMessagesEmpty() || !context.expectedMessagesSkippable()){
					if (context.expectedMessagesEmpty()) {
						pathDescription.setPostEventStates(new HashSet<String>(receivedState.getActiveStates()));
					} else {
						pathDescription.setPostEventStates(new HashSet<String>(pathDescription.getActiveStates()));
					}
				}else {
					pathDescription.setPostEventStates(new HashSet<String>(receivedState.getActiveStates()));
					pathDescription.setPostStatesUnknown();
				}
				pathDescriptions.add(pathDescription);
				setRuleExecutor(currentContext, context);
				newContexts.add(context);
			}
		}
	}
	
	private CMatchResult matchExpectedObservation(CObservedMessage message, CExecutionContext context, List<CExecutionContext> newContexts) {
		if(context.expectedMessagesEmpty()) return CMatchResult.FAIL;
		
		CMatchResult result = context.getExpectedMessages().match(message);
		if (result == CMatchResult.MATCH || result == CMatchResult.DROP) {
			CPathDescription pathDescription = new CPathDescription();
			context.setPathDescription(pathDescription);
			CState receivedState = context.getState();
			Set<String> activeStates = new HashSet<String>(receivedState.getActiveStates());
			activeStates.remove(receivedState.getActiveState());
			activeStates.add(receivedState.getExecutionState());
			pathDescription.setActiveStates(activeStates);
			if (context.expectedMessagesEmpty() || !context.expectedMessagesSkippable()){
				if (context.expectedMessagesEmpty()) {
					pathDescription.setPostEventStates(new HashSet<String>(receivedState.getActiveStates()));
				} else {
					pathDescription.setPostEventStates(new HashSet<String>(pathDescription.getActiveStates()));
				}
			}else {
				pathDescription.setPostEventStates(new HashSet<String>(receivedState.getActiveStates()));
				pathDescription.setPostStatesUnknown();
			}
			pathDescriptions.add(pathDescription);
			newContexts.add(context);
		}
		return result;
	}
}
