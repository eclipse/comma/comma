/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

@SuppressWarnings("serial")
public class CSimpleTimeRule extends CTimeRule {
	protected CSimpleTimeRuleEnvironment env;

	public CSimpleTimeRule() {
		env = new CSimpleTimeRuleEnvironment();
		env.setRule(this);
	}

	public CSimpleTimeRuleEnvironment getEnvironment() {
		return env;
	}

	public void registerObserver(CRuleObserver observer) {
		if (observer instanceof CTimeRuleObserver) {
			super.registerObserver(observer);
		}
	}

	public void notifyOnIntervalCheck(double v) {
		for(CRuleObserver observer : ruleObservers) {
			((CTimeRuleObserver) observer).intervalChecked(this, v);
		}
	}
}
