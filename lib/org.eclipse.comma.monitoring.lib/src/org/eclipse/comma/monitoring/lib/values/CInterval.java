/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CInterval implements Serializable{
	private double begin = -1.0;
	private double end = -1.0;
	private double limit = -1.0;

	public double getBegin() {
		return begin;
	}

	public CInterval setBegin(double begin) {
		this.begin = begin;
		return this;
	}

	public double getEnd() {
		return end;
	}

	public CInterval setEnd(double end) {
		this.end = end;
		return this;
	}

	public boolean isIn(double t) {
		if(Double.compare(begin, limit) == 0) 
			return lessOrEqualThan(t, end); //t <= end;

		if(Double.compare(end, limit) == 0) 
			return lessOrEqualThan(begin, t); //begin <= t
		
		return lessOrEqualThan(begin, t) &&	//begin <= t
               lessOrEqualThan(t, end);	 //t <= end
	}

	/*
	 * Evaluates values value1 and value2 on : value1 <= value2, 
	 * where value1 and value2 are of type double
	 */
	private boolean lessOrEqualThan(double value1, double value2) {
		return (Double.compare(value1, value2) == 0) || (Double.compare(value1, value2) < 0);
	}

	public boolean isAfter(double t) {
		if(Double.compare(end, limit) == 0) 
			return false;
		return (end < t);
	}

	public String toString() {
		String result = "";

		if(Double.compare(begin, limit) == 0) {
			result += "[..";
		} else {
			result += "[" + begin + " ms ..";
		}

		if(Double.compare(end, limit) == 0) {
			result += "]";
		} else {
			result += " " + end + " ms]";
		}
		return result;
	}
}
