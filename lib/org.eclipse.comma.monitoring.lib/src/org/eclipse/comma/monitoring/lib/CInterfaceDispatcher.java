/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedSignal;

public class CInterfaceDispatcher extends CDispatcher{
	private CInterfaceMonitoringContext context;
	private final String interfaceName;
	private final boolean singleton;
	private CFactory factory;
	private CConnectionResults resultsLastEvent;
	
	public CInterfaceDispatcher(CInterfaceMonitoringContext context, CFactory factory) {
		this.context = context;
		this.interfaceName = context.getInterfaceName();
		this.singleton = context.isSingleton();
		this.factory = factory;
	}

	@Override
	public void consume(CObservedMessage message) throws Exception {
		resultsLastEvent = null;
		if(!message.getInterface().equals(interfaceName)) 
			return;

		String key = connectionKey(message);
		CInterfaceMonitor monitor = connection2monitor.get(key);

		if(monitor == null) {
			monitor = new CInterfaceMonitor(context.clone().setConnectionName(key).setLocalDir(key + "/"), factory);
			connection2monitor.put(key, monitor);
		}
		if(!monitor.inErrorState()) {
			monitor.consume(message);
			resultsLastEvent = monitor.getResultForLastEvent();
		}
	}
	
	private String connectionKey(CObservedMessage message) {
		if(message instanceof CObservedCommand || message instanceof CObservedSignal) {
			if(!singleton) {
				return makeMultitonConnectionKey(message.getDestination(), message.getSource(), interfaceName);
			}else {
				return makeSingletonConnectionKey(message.getDestination(), message.getDestinationPort(), interfaceName);
			}
		}else {
			if(!singleton) {
				return makeMultitonConnectionKey(message.getSource(), message.getDestination(), interfaceName);
			}else {
				return makeSingletonConnectionKey(message.getSource(), message.getSourcePort(), interfaceName);
			}
		}
	}

	@Override
	public void traceEnded() {
		for(CInterfaceMonitor mon : connection2monitor.values().stream().
				                          filter(h -> !h.inErrorState()).
				                          collect(Collectors.toList())) {
			mon.traceEnded();
		}
	}
	
	@Override
	public List<CMonitorResults> getResults() {
		return connection2monitor.values().stream().map(h -> h.getResults()).collect(Collectors.toList());
	}

	@Override
	public List<CMonitorResults> getIssuesForLastEvent() {
		if(resultsLastEvent != null && resultsLastEvent.hasIssues()) {
			return Arrays.asList(resultsLastEvent);
		} else {
			return Arrays.asList();
		}
	}
}


