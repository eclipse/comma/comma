/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class CEnvironment implements Serializable{
	public abstract CEnvironment setVariableValue(String name, Object value);
	public abstract boolean checkCondition(int index, CObservedMessage message); //TODO check if the message param is really needed
}
