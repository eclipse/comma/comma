/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.comma.monitoring.lib.utils.Utils;
import org.eclipse.comma.monitoring.lib.values.CInterval;

@SuppressWarnings("serial")
public class CTimeRuleObserver extends CRuleObserver {

	public CTimeRuleObserver(String fileName, String ruleType) {
		super(fileName, ruleType);
	}

	private void handleEvent(CRule rule) {
		try {
			FileWriter file = getFile();
			String toWrite = rule.getName() + ",";
			double diff = Utils.roundDouble(rule.getLastObservation().getTimeDelta() - rule.getTriggeringObservation().getTimeDelta(), 3);
			toWrite += diff + ",";
			CInterval i = ((CSimpleTimeRule)rule).getEnvironment().getInterval();
			toWrite += i.getBegin() + ",";
			toWrite += i.getEnd();
			file.write(toWrite + "\n");
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void ruleFailed(CRule rule) {
		if(rule instanceof CConditionedAbsenceOfEventTimeRule ||
		   rule instanceof CConditionedEventTimeRule) {
			handleEvent(rule);
		}
	}

	public void ruleSucceeded(CRule rule) {
		if(rule instanceof CConditionedEventTimeRule) {
			handleEvent(rule);
		}
	}

	public void intervalChecked(CSimpleTimeRule rule, double v) {
		if(!(rule instanceof CEventIntervalTimeRule)) return;
		try {
			FileWriter file = getFile();
			String toWrite = rule.getName() + "," + Utils.roundDouble(v, 3) + ",";
			CInterval i = ((CSimpleTimeRule)rule).getEnvironment().getInterval();
			toWrite += i.getBegin() + ",";
			toWrite += i.getEnd();
			file.write(toWrite + "\n");
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
