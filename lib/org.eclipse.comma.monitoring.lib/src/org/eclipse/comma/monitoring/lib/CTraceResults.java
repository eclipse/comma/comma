/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class CTraceResults {
	@Expose
	String traceFile;
	@Expose
	boolean traceError = false;
	@Expose
	String traceErrorMessage = "";
	@Expose
	List<CMonitorResults> monitorResults;
	private List<CCoverageInfo> coverageInfo = new ArrayList<CCoverageInfo>();
	
	public CTraceResults(String traceFile) {
		this.traceFile = traceFile;
		monitorResults = new ArrayList<CMonitorResults>();
	}
	
	public void setMonitorResults(List<CMonitorResults> monitorResults) {
		this.monitorResults = monitorResults;
		for(CMonitorResults mr : monitorResults) {
			coverageInfo.addAll(mr.getCoverageInfo());
		}
	}
	
	public void markTraceError(String msg) {
		traceError = true;
		traceErrorMessage = msg;
	}
	
	public List<CCoverageInfo> getCoverageInfo() {
		return coverageInfo;
	}
}
