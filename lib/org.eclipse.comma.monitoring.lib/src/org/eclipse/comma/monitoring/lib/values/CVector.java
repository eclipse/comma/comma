/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.utils.Utils;

@SuppressWarnings("serial")
public class CVector<T> implements Serializable {
	private List<T> elements;

	public CVector(){
		elements = new ArrayList<T>();
	}

	@SuppressWarnings("unchecked")
	public CVector<T> add(T element) {
		CVector<T> result = (CVector<T>) Utils.deepCopy(this);
		result.getElements().add(element);
		return result;
	}

	@SuppressWarnings("unchecked")
	public void addNew(Object element) {
		elements.add((T)element);
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public boolean contains(Object o) {
		return elements.contains(o);
	}
	
	public long size() {
		return Long.valueOf(elements.size());
	}

	public List<T> getElements() {
		return elements;
	}

	@SuppressWarnings("unchecked")
	public boolean equals(Object o) {
		if(o == null || !(o instanceof CVector)) return false;
		CVector<T> vector_ = (CVector<T>) o;
		if(vector_.getElements().size() != elements.size()) return false;
		for(int i = 0; i < elements.size(); i++) {
			if(!elements.get(i).equals(vector_.getElements().get(i))) return false;
 		}
		return true;
	}

	@Override
	public String toString() {
		return "Vector";
	}
}
