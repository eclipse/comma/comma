/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

public class CFunctionalConstraintResult {
	public List<CFunctionalConstraintState> states = new ArrayList<CFunctionalConstraintState>();
	public List<Object> expectedMessages = null; //TODO exact type/consider if needed
}
