/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.io.Serializable;

import org.eclipse.comma.monitoring.lib.messages.CEnvironment;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public abstract class CSequenceElement implements Serializable {
	protected CEnvironment env;

	public CSequenceElement setEnvironment(CEnvironment env) {
		this.env = env;
		return this;
	}

	public CEnvironment getEnvironment() {
		return env; 
	}

	public abstract CConstraintValue consume(CObservedMessage message);
}
