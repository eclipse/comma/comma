/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

@SuppressWarnings("serial")
public class CPortConnectionMonitoringContext extends CInterfaceMonitoringContext {
	protected String port = "ev";

	public CPortConnectionMonitoringContext(String interfaceName, boolean isSingleton, CFactory factory,
			String traceResultsDir) {
		super(interfaceName, isSingleton, factory, traceResultsDir);
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	public String getPort() {
		return port;
	}

}
