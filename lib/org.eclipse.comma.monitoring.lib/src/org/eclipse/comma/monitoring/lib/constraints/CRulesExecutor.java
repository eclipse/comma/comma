/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.CInterfaceMonitoringContext;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CRulesExecutor implements Serializable {
	private List<CRule> listOfRules;
	private List<CRule>listOfActivatedRules;
	private CInterfaceMonitoringContext context;

	public CRulesExecutor(CInterfaceMonitoringContext monitoringContext) {
		listOfActivatedRules = new ArrayList<CRule>();
		listOfRules = new ArrayList<CRule>();
		this.context = monitoringContext;
	}

	public void setListOfRules(List<CRule> rules) {
		listOfRules = rules;
	}

	public List<CRuleError> consumeEvent(CObservedMessage message) {
		List<CRuleError> errors = new ArrayList<CRuleError>();
		if (!listOfRules.isEmpty() && (!context.dataConstraintsSkipped() || !context.timeConstraintsSkipped())) {
			List<CRule> newlyActivatedRules = new ArrayList<CRule>();
			//first check if the event will trigger a rule
			for(CRule rule : listOfRules) {
				if ( (!context.dataConstraintsSkipped() && (rule instanceof CDataRule)) || 
				     (!context.timeConstraintsSkipped() && (rule instanceof CTimeRule)) || 
				     (rule instanceof CGenericRule)) {
					rule = rule.deepCopy();
					CConstraintValue ruleResult = rule.consume(message);
					if (ruleResult == CConstraintValue.UNKNOWN) { //the rule is activated
						newlyActivatedRules.add(rule);
					}
					if (ruleResult == CConstraintValue.FALSE) { //the rule immediately failed
						errors.add(rule.getError());
					}
				}
			}
			//till now the rules are iterated to see if the current event activated a rule
			//iterate over the activated rules to check if there will be rule resolution or error
			for(CRule rule : listOfActivatedRules) {
				CConstraintValue ruleResult = rule.consume(message);
				if (ruleResult == CConstraintValue.FALSE) { //error
					errors.add(rule.getError());
				}
				if (ruleResult == CConstraintValue.UNKNOWN) { //no impact
					newlyActivatedRules.add(rule);
				}
			}
			listOfActivatedRules = newlyActivatedRules;
		}
		return errors;
	}
}
