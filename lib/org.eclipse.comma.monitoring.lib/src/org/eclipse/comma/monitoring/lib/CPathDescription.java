/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("serial")
public class CPathDescription implements Serializable {
	private String pathId;
	private Set<String> activeStates;
	private Set<String> postEventStates;
	private boolean postStatesKnown;
	
	//TODO consider a better structure
	public String forwardPort = null;
	public String forwardConnection = null;
	
	public CPathDescription() {
		pathId = "";
		activeStates = new HashSet<String>();
		postEventStates = new HashSet<String>();
		postStatesKnown = true;
	}
	
	public void setPathId(String path) {
		pathId = path;
	}
	
	public String getPathId() {
		return pathId;
	}
	
	public void setActiveStates(Set<String> states) {
		activeStates = states;
	}
	
	public Set<String> getActiveStates(){
		return activeStates;
	}
	
	public void setPostEventStates(Set<String> states) {
		postEventStates = states;
	}
	
	public Set<String> getPostEventStates(){
		return postEventStates;
	}
	
	public void setActiveStatesAsPostStates() {
		postEventStates = activeStates;
	}
	
	public boolean postStatesKnown() {
		return postStatesKnown;
	}
	
	public void setPostStatesKnown() {
		postStatesKnown = true;
	}
	
	public void setPostStatesUnknown() {
		postStatesKnown = false;
	}
}
