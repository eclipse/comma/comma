/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.List;

import org.eclipse.comma.monitoring.lib.constraints.CRuleError;
import org.eclipse.comma.monitoring.lib.constraints.CRulesExecutor;
import org.eclipse.comma.monitoring.lib.messages.CMessageCompositePattern;
import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CSequencePattern;

public class CExecutionContext {
	private CSequencePattern expectedMessages = new CSequencePattern();
	private CState state = null;
	private boolean executed = true;
	private String executionPathId = "";
	private CRulesExecutor rulesExecutor = null;

	public boolean expectedMessagesEmpty() {
		return expectedMessages.isEmpty();
	}
	
	public boolean expectedMessagesSkippable() {
		return expectedMessages.isSkippable();
	}
	
	public List<CMessagePattern> getPossibleEvents(){
		return expectedMessages.getPossibleEvents();
	}
	
	public CState getState() {
		return state;
	}
	
	public void setState(CState s) {
		this.state = s;
	}
	
	public void setExecutionPathId(String p) {
		executionPathId = p;
	}
	
	public String getExecutionPathId() {
		return executionPathId;
	}
	
	public CExecutionContext addToExpectedMessages(CMessageCompositePattern o) {
		expectedMessages.addElement(o);
		return this;
	}
	
	public CSequencePattern getExpectedMessages() {
		return expectedMessages;
	}
	
	public void setPathDescription(CPathDescription pd) {
	}
	
	public void setExecuted(boolean status) {
		executed = status;
	}
	
	public boolean isExecuted() {
		return executed;
	}
	
	public void setRulesExecutor(CRulesExecutor e) {
		rulesExecutor = e;
	}
	
	public CRulesExecutor getRulesExecutor() {
		return rulesExecutor;
	}
	
	public List<CRuleError> consumeEvent(CObservedMessage message) {
		return rulesExecutor.consumeEvent(message);
	}
}
