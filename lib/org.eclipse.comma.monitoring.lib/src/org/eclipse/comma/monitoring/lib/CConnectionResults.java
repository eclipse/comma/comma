/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.constraints.CRuleError;
import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CConnectionResults extends CMonitorResults {	
	private List<CState> executionStates;	
	private CInterfaceMonitoringContext context;
	@Expose
	private String connectionName;
	@Expose
	private String status = "successful";
	@Expose
	private List<CError> monitoringErrors;
	@Expose
	@SerializedName("warnings")
	private List<CRuleError> constraintErrors;
	
	public CConnectionResults(CInterfaceMonitoringContext context) {
		this.context = context;
		connectionName = context.getConnectionName();
		monitoringErrors = new ArrayList<CError>();
		constraintErrors = new ArrayList<CRuleError>();
		executionStates = new ArrayList<CState>();
		resultsFolder = context.resultsDir();
	}
	
	public CInterfaceMonitoringContext getContext() {
		return context;
	}
	
	public void addMonitoringError(CError error) {
		monitoringErrors.add(error);
		status = "failed";
	}
	
	public List<CError> getMonitoringErrors(){
		return monitoringErrors;
	}
	
	public void addConstraintErrors(List<CRuleError> errors) {
		constraintErrors.addAll(errors);
	}
	
	public List<CRuleError> getConstraintErrors(){
		return constraintErrors;
	}
	
	public void addExecutionState(CState s) {
		executionStates.add(s);
		CCoverageInfo ci = s.getCoverageInfo();
		ci.setInterfaceName(context.getInterfaceName());
		ci.calculateCoveragePercentage();
		coverageInfo.add(ci);
	}
	
	public boolean isSuccess() {
		return monitoringErrors.isEmpty();
	}
	
	public String toString() {
		StringBuilder2 builder = new StringBuilder2();
		if(isSuccess()) {
			builder.append("Success: no errors found.");
			builder.newLine(); builder.newLine();
        } else {//there are errors
        	for(CError error : monitoringErrors) {
        		builder.append(error.toString());
        		builder.newLine();
        	}
        }
		for(CRuleError error : constraintErrors) {
			builder.append(error.toString());
			builder.newLine(); builder.newLine();
		}
		for(CState state : executionStates) {
			builder.append("Coverage Info"); builder.newLine();
			builder.append("================================="); builder.newLine();
			builder.append(state.coverageInfoToString());
		}
		return builder.toString();
	}

	@Override
	public boolean hasIssues() {
		return !monitoringErrors.isEmpty() || !constraintErrors.isEmpty(); 
	}
}
