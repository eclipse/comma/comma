/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CEnvironment;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CPeriodicTimeRuleEnvironment extends CEnvironment {
	private double t1;
	private double t2;
	private double t3;
	private double t4;
	private long c = 0L;
	private double period;
	private double jitter;
	
	public CEnvironment setVariableValue(String name, Object value) {
		if(name.equals("t1")) t1 = (double) value;
		if(name.equals("t2")) t2 = (double) value;
		if(name.equals("t3")) t3 = (double) value;
		if(name.equals("t4")) t4 = (double) value;
		if(name.equals("c")) c = (long) value;
		if(name.equals("period")) period = (double) value;
		if(name.equals("jitter")) jitter = (double) value;
		return this;
	}
				
	public boolean checkCondition(int index, CObservedMessage message) {
		switch(index) {
		case 1: //TODO comparison of doubles?
			return ((t2 - t1) <= (period * (c + 1) + jitter)) & ((period * (c + 1) - jitter) <= (t2 - t1));
		case 2:
			return (t3 - t1) <= (period * (c + 1) + jitter);
		case 3:
			return (t4 - t1) <= (period * (c + 1) + jitter);
		default: return false;
		}
	}
}
