/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CNegation extends CFormula {
	protected CFormula formula;
	
	public CNegation setFormula(CFormula f) {
		formula = f;
		return this;
	}
	
	public CFormula getFormula() {
		return formula;
	}
	
	@Override
	public CConstraintValue consume(CObservedMessage message) {
		CConstraintValue result = formula.consume(message);
		if(result == CConstraintValue.FALSE) return CConstraintValue.TRUE;
		if(result == CConstraintValue.TRUE) return CConstraintValue.FALSE;
		return result;
	}

}
