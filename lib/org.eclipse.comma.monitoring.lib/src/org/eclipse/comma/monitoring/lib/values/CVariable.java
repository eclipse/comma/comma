/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CVariable implements Serializable {
	private String name;
	private Class<?> clazz;

	public CVariable(String name, Class<?> clazz) {
		this.name = name;
		this.clazz = clazz;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) return false;
		return obj.getClass() == clazz;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "variable " + name; 
	}
}
