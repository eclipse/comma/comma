/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

public class CPartDescriptor {
	public String partId;
	public String type;
	
	public CPartDescriptor(String partId, String type) {
		this.partId = partId;
		this.type = type;
	}
}
