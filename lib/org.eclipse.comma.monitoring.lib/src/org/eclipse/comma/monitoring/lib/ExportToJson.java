/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ExportToJson implements ExportResults{
	private static final String RESULTS_PATH = ""+"comma-gen";

	/*
	 * Expects a collections of the (tasks)results from the performed
	 * monitoring tasks.
	 * The results will be saved as a Json structure.
	 */
	public void export(List<CTaskResults> taskResults) {

		Gson gson = new GsonBuilder()
						.excludeFieldsWithoutExposeAnnotation()
						.setPrettyPrinting()
						.create();

		String resultsInJson = gson.toJson(taskResults);
		File path = new File(RESULTS_PATH);
		
		//if output directory of results path does not exists create one
		path.mkdirs();
		File file = new File(RESULTS_PATH ,"tasksResults.json");		

		try {
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(resultsInJson);
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getPath() {
		return RESULTS_PATH + "tasksResults.json";
	}
}
