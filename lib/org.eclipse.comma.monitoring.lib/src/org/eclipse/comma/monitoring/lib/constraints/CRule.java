/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.utils.Utils;

@SuppressWarnings("serial")
public abstract class CRule implements Serializable {
	protected String name;
	protected CObservedMessage triggeringObservation;
	protected CObservedMessage lastObservation;
	protected String errorMessage;
	protected boolean activated = false;
	protected CFormula formula;
	protected List<CRuleObserver> ruleObservers = new ArrayList<CRuleObserver>();
	protected CRuleError error = null;
	
	public CRule setName(String n) {
		name = n;
		return this;
	}
	
	public String getName() {
		return name;
	}
	
	public CObservedMessage getTriggeringObservation() {
		return triggeringObservation;
	}
	
	public CObservedMessage getLastObservation() {
		return lastObservation;
	}
	
	public CRule setErrorMessage(String m) {
		errorMessage = m;
		return this;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public CRule setFormula(CFormula f) {
		formula = f;
		return this;
	}

    public CFormula getFormula() {
    	return formula;
    }
	
    public void registerObserver(CRuleObserver observer) {
		ruleObservers.add(observer);
    }
	
    public void unregisterObserver(CRuleObserver observer) {
    	ruleObservers.remove(observer);
    }
	
    public void clearObservers() {
    	ruleObservers.clear();
    }
	
    public void notifyOnSuccess() {
    	for(CRuleObserver observer : ruleObservers) {
    		observer.ruleSucceeded(this);
    	}
    }
	
    public void notifyOnFailure() {
    	for(CRuleObserver observer : ruleObservers) {
    		observer.ruleFailed(this);
    	}
    }
    
    public CRuleError getError() {
    	return error;
    }

    public void produceError(CObservedMessage failingObservation) {
    	error = new CRuleError(name, errorMessage, triggeringObservation, failingObservation);
    }
	
    public CConstraintValue consume(CObservedMessage message) {
    	lastObservation = message;
    	CConstraintValue r = formula.consume(message);
    	if(!activated && r != CConstraintValue.TRUE) {
			activated = true;
			triggeringObservation = message;
    	}
    	if(r == CConstraintValue.FALSE) {
			produceError(message);
			notifyOnFailure();
    	}
		return r;
    }
	
	public CRule deepCopy() { //copy everything but share the observers
		CRule result = (CRule) Utils.deepCopy(this);
		result.clearObservers();
		for(CRuleObserver observer : ruleObservers) {
    		result.registerObserver(observer);
    	}
		return result;
	}
}
