/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;

@SuppressWarnings("serial")
public class CEventIntervalTimeRule extends CSimpleTimeRule {
	
	public CEventIntervalTimeRule(CMessagePattern firstEvent, CMessagePattern secondEvent) {
		super();
		errorMessage = "The interval between events is different from the expected interval.";
		formula =
				new CConstraintSequence().
		           setSequence(new CSequence().
		           		addElement(new CEventSelector().setEvent(firstEvent).setTimeVariable("t1").setEnvironment(env)).
		                addElement(new CUntil().
		                             setBody((CStep) new CEventSelector().setEvent(firstEvent).setTimeVariable("t2").setNegated().setEnvironment(env)).
		                             setStop((CStep) new CEventSelector().setEvent(secondEvent).setTimeVariable("t3").setEnvironment(env)).
		                             setEnvironment(env)).
		                setEnvironment(env)).
		           setConditionIndex(1);
	}
}
