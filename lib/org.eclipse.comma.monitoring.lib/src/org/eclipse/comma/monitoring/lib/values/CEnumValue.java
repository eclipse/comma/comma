/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.values;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CEnumValue implements Serializable {
	private String literal;

	public CEnumValue(String literal) {
		this.literal = literal;
	}

	public String getLiteral() {
		return literal;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof CEnumValue && ((CEnumValue) obj).getLiteral().equals(literal);
	}

	public String toString() {
		return literal;
	}

	@Override
	public int hashCode() {
		return literal.hashCode();
	}
}
