/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import java.util.List;
import org.eclipse.comma.monitoring.lib.CState;

public abstract class CMessageCompositePattern {
	protected CState currentInterfaceState = null;
	public abstract CMatchResult match(CObservedMessage observedMessage);

	public CMessageCompositePattern setInterfaceState(CState state) {
		this.currentInterfaceState = state;
		return this;
	}

	public boolean isSkippable() {
		return false;
	}

	public abstract List<CMessagePattern> getPossibleEvents();
}