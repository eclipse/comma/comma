/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CUntil extends CSequenceElement{
	protected CStep body;
	protected CStep stop;
	protected boolean isWeak = false;

	public CUntil setBody(CStep b) {
		body = b;
		return this;
	}
	
	public CStep getBody() {
		return body;
	}

	public CUntil setStop(CStep s) {
		stop = s;
		return this;
	}

	public CStep getStop() {
		return stop;
	}

	public CUntil setWeak() {
		isWeak = true;
		return this;
	}

	@Override
	public CConstraintValue consume(CObservedMessage message) {
		if( (stop.consume(message)) == CConstraintValue.TRUE) return CConstraintValue.TRUE;
		if( (body.consume(message)) == CConstraintValue.FALSE) return CConstraintValue.FALSE;
		return CConstraintValue.UNKNOWN;
	}
}
