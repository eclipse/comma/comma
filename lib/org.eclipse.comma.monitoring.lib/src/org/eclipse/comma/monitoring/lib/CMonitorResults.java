/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public abstract class CMonitorResults {
	@Expose
	protected List<CCoverageInfo> coverageInfo = new ArrayList<CCoverageInfo>();
	@Expose
	protected String resultsFolder;

	public List<CCoverageInfo> getCoverageInfo() {
		return coverageInfo;
	}
	
	public abstract boolean hasIssues();
}
