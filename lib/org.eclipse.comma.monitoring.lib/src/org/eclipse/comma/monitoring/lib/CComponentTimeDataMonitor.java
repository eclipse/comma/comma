/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.comma.monitoring.lib.constraints.CDataRuleObserver;
import org.eclipse.comma.monitoring.lib.constraints.CRule;
import org.eclipse.comma.monitoring.lib.constraints.CRuleError;
import org.eclipse.comma.monitoring.lib.constraints.CRulesExecutor;
import org.eclipse.comma.monitoring.lib.constraints.CTimeRuleObserver;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

public class CComponentTimeDataMonitor extends CComponentConstraintMonitor {
	private CRulesExecutor rulesRunner;
	private CInterfaceMonitoringContext context;
	private CComponentConstraintResult result;
	private CComponentConstraintResult resultPerEvent;
	
	public CComponentTimeDataMonitor(CInterfaceMonitoringContext context, List<CRule> rules) {
		this.context = context;
		rulesRunner = new CRulesExecutor(context);
		registerRuleObservers(rules);
		rulesRunner.setListOfRules(rules); 
		result = new CComponentConstraintResult();
	}

	private void registerRuleObservers(List<CRule> rules) {
		CTimeRuleObserver observerTR = new CTimeRuleObserver(context.getTimeStatisticsFileName(), "time");
		CDataRuleObserver observerDR = new CDataRuleObserver(context.getDataStatisticsFileName(), "data");
		for(CRule r : rules) {
			r.registerObserver(observerTR);
			r.registerObserver(observerDR);
		}
	}
	
	@Override
	public void consume(CObservedMessage message, List<CPathDescription> pathDescriptions, String port) {
		resultPerEvent = new CComponentConstraintResult();
		List<CRuleError> warnings = rulesRunner.consumeEvent(message); 
		result.addConstraintErrors(warnings);
		resultPerEvent.addConstraintErrors(warnings);
		
	}

	@Override
	public CComponentConstraintResult getResults() {
		return result;
	}

	@Override
	public void traceEnded() {
		List<CRuleError> ruleErrors = result.getConstraintErrors();
        for(int i = 0; i < ruleErrors.size(); i++) {
    		//write in plantUML file
    		String umlRuleErrorFileName = context.getRuleErrorUMLFile(i);
    		try {
    			FileWriter umlWriter = new FileWriter(umlRuleErrorFileName, false);
				umlWriter.write(ruleErrors.get(i).toUML());
				umlWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    		ruleErrors.get(i).setUMLFile(umlRuleErrorFileName);
    	}
        //TODO consider keeping/removing the results file
	}

	@Override
	public CComponentConstraintResult getResultForLastEvent() {
		return resultPerEvent;
	}
	
}
