/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;

@SuppressWarnings("serial")
public class CConditionedAbsenceOfEventTimeRule extends CSimpleTimeRule {
	
	public CConditionedAbsenceOfEventTimeRule(CMessagePattern triggeringEvent, CMessagePattern missingEvent) {
		super();
		errorMessage = "Event is observed in the specified interval.";
		formula = new CNegation().
				   setFormula(new CSequence().
				              addElement(new CEventSelector().setEvent(triggeringEvent).setTimeVariable("t1").setEnvironment(env)).
				              addElement(new CUntil(). 
				                           setBody((CStep) new CEventSelector().setEvent(new CMessagePattern()).setTimeVariable("t2").setConditionIndex(2).setEnvironment(env)).
				                           setStop((CStep) new CEventSelector().setEvent(missingEvent).setTimeVariable("t3").setConditionIndex(1).setEnvironment(env)).
				                           setEnvironment(env)).
				              setEnvironment(env));
	}
}
