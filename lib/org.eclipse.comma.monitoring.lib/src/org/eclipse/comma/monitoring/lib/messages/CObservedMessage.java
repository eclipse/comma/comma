/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.messages;

import org.eclipse.comma.monitoring.lib.CState;
import org.eclipse.comma.monitoring.lib.utils.Utils;

@SuppressWarnings("serial")
public class CObservedMessage extends CMessage {
	protected String messageId = "";
	protected double timeDelta = 0.0;
	protected String timestamp = "";
	protected String interfaceName = "";
	protected CState interfaceState = null;

	public CObservedMessage() {
		super();
	}

	public CObservedMessage(String name, String source, String destination, String sourcePort, String destinationPort) {
		super(name, source, destination, sourcePort, destinationPort);
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setInterface(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getInterface() {
		return interfaceName;
	}

	public void setTimeDelta(double timeDelta) {
		this.timeDelta = timeDelta;
	}

	public double getTimeDelta() {
		return timeDelta;
	}

	public CObservedMessage setTimestamp(String timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	public CState getInterfaceState() {
		return interfaceState;
	}

	public CObservedMessage setInterfaceState(CState interfaceState) {
		this.interfaceState = interfaceState;
		return this;
	}

	public CObservedMessage clone() {
		return ((CObservedMessage) Utils.deepCopy(this)).clearState();
	}

	public CObservedMessage clearState() { 
		states.clear();
		return this;
	}

	public String toString() {
		return messageId.length() == 0 ? "" : "[" + messageId + "] ";
	}

	/* 
	 * utility methods for representing a message in text format
	 * used in error reporting and plantUML visualization
	*/	
	public String printStringWithState() {
		String result = this.toString();
		
		if(!states.isEmpty()) {
			result = "State: " + states.get(0)+ " " + result;
 		}
		return result;
	}

	public String printUMLWithState() {
		String result = printUML();
		
		if(!states.isEmpty()) {
			result = result + "\n" + "note left: State " + states.get(1);
 		}
		return result;
	}

	public String printUMLWithTime() {
		String result = printUML() + " at time " + timestamp;
		return result ;
	}
	
	public String printUMLWithTimeAndState() {
		String result = printUMLWithTime();
		
		if(!states.isEmpty()) {
			result = result + "\n" + "note left: State " + states.get(0);
 		}
		return result;
	}

	public String printUMLParameters() {
		String result = "(";
							
		for(int i = 0; i < getParameters().size(); i++) {
			if(i >= 1) {
				result = result + ", ";
			}

			String parameter = getParameters().get(i).toString();
			result = result + parameter;
		}
		return result + ")";
	}

	public String printUML() {
		return "";
	}
}
