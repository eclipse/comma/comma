/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedSignal;
import org.eclipse.comma.monitoring.lib.utils.CTraceSerializer;
import org.eclipse.comma.monitoring.lib.utils.Utils;

public class CFunctionalConstraintMonitor extends CComponentConstraintMonitor{
	private CComponentMonitoringContext context;
	private CFunctionalConstraint constraint;
	private Function<CObservedMessage, Boolean> filter;
	private List<CFunctionalConstraintContext> componentExecutionContexts = new ArrayList<CFunctionalConstraintContext>();
	private CTraceSerializer traceSerializer;
	private boolean inErrorState;
	private CFunctionalConstraintMonitorResult funcConstraintResult;
	private CComponentConstraintResult result;
	private CComponentConstraintResult resultPerEvent;
	
	public CFunctionalConstraintMonitor(CComponentMonitoringContext context, CFunctionalConstraint constraint, Function<CObservedMessage, Boolean> filter) {
		this.context = context;
		this.constraint = constraint;
		this.filter = filter;
		inErrorState = false;
		CFunctionalConstraintContext initialContext = new CFunctionalConstraintContext();
		initialContext.setConstraintExecutionStates(Arrays.asList(constraint.getInitialState()));
		initialContext.setInitialPortsStates(constraint.getInitialState().getInitialPortStates());
		componentExecutionContexts.add(initialContext);
		String traceFile = context.getTraceFile(constraint.getName());
		traceSerializer = new CTraceSerializer(traceFile);
		funcConstraintResult = new CFunctionalConstraintMonitorResult(constraint.getName(), traceFile);
		result = new CComponentConstraintResult();
		result.addFunctionaConstraintResult(funcConstraintResult);
	}
	
	@Override
	public void consume(CObservedMessage message, List<CPathDescription> pathDescriptions, String port) throws Exception {
		if(inErrorState) {
			resultPerEvent = null;
			return; //Do nothing
		}
		resultPerEvent = new CComponentConstraintResult();
		if(port == null) { //TODO simplify the one below
			int partIndex = context.getComponentInstanceName().length() + 1;
			if(message instanceof CObservedCommand || message instanceof CObservedSignal) { //a call
				updateExecutionStates(message.getSource(), message.getSource().substring(partIndex) + "." + message.getSourcePort(), message.getDestination(), message.getDestination().substring(partIndex) + "." + message.getDestinationPort(), pathDescriptions);
			} else { //notification or reply
				updateExecutionStates(message.getDestination(), message.getDestination().substring(partIndex) + "." + message.getDestinationPort(), message.getSource(), message.getSource().substring(partIndex) + "." + message.getSourcePort(), pathDescriptions);
			}
		} else {
			String instanceName = context.getComponentInstanceName();
			if(message.getDestination().equals(instanceName)) {
				updateExecutionStates(message.getSource(), pathDescriptions, port);
			} else {
				updateExecutionStates(message.getDestination(), pathDescriptions, port);
			}
		}
		if(componentExecutionContexts.isEmpty()) {
			//no valid combinations of interface execution paths that are also accepted by the functional constraint
			if(filter.apply(message)) {
				traceSerializer.processEvent(message);
			}
			handleError();
			return;
		}
		//update of execution contexts is done for every message
		//message is processed only if it is used by the func. constraint:
		if(filter.apply(message)) {
			traceSerializer.processEvent(message);
			List<CFunctionalConstraintContext> newComponentExecutionContexts = new ArrayList<CFunctionalConstraintContext>();
			for(CFunctionalConstraintContext context : componentExecutionContexts) {
				constraint.setPortsContext(context);
				List<CFunctionalConstraintState> newStates = new ArrayList<CFunctionalConstraintState>();
				for(CFunctionalConstraintState state : context.getConstraintExecutionStates()) {
					constraint.setState(state);
					CFunctionalConstraintResult result = constraint.consume(message);
					newStates.addAll(result.states);
				}
				if(!newStates.isEmpty()) {
					context.setConstraintExecutionStates(newStates);
					newComponentExecutionContexts.add(context);
				}
			}
			if(newComponentExecutionContexts.isEmpty()) {
				//Error: all possible paths lead to a violation of functional constraint
				handleError();
			} else {
				componentExecutionContexts = newComponentExecutionContexts;
			}
		}
	}
	
	private void handleError() {
		inErrorState = true;
		CFunctionalConstraintError error = new CFunctionalConstraintError();
		error.setErrorContext(traceSerializer.getLatestTrace());
		String umlErrorFileName = context.getErrorUMLFile(constraint.getName());
		error.setUMLFile(umlErrorFileName);
		funcConstraintResult.setError(error);
		resultPerEvent.addFunctionaConstraintResult(funcConstraintResult);
		try {
			FileWriter writer = new FileWriter(context.getResultsFile(constraint.getName()), false);
            writer.write(funcConstraintResult.toString());
            writer.close();
            FileWriter umlWriter = new FileWriter(umlErrorFileName, false);
			umlWriter.write(error.toUML());
			umlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void updateExecutionStates(String connection, List<CPathDescription> pathDescriptions, String port) {
		List<CFunctionalConstraintContext> newComponentExecutionContexts = new ArrayList<CFunctionalConstraintContext>();
		for(CFunctionalConstraintContext executionContext : componentExecutionContexts) {
			Map<String, CPathDescription> portConnections = executionContext.getPortsStates().get(port);
			if(portConnections == null) {
				//no connections at the port yet. For each pathDescription add a new state derived from the current
				for(CPathDescription pd : pathDescriptions) {
					CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
					Map<String, CPathDescription> connections = new HashMap<String, CPathDescription>();
					connections.put(connection, (CPathDescription)Utils.deepCopy(pd));
					newExecutionContext.getPortsStates().put(port, connections);
					newComponentExecutionContexts.add(newExecutionContext);
				}
			} else {
				//there is at least one connection for the port
				if(!portConnections.containsKey(connection)) {
					//current connection is not known yet
					for(CPathDescription pd : pathDescriptions) {
						if(!context.isSingletonPort(port)) {
							//this is multiton port
							CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
							newExecutionContext.getPortsStates().get(port).put(connection, (CPathDescription)Utils.deepCopy(pd));
							newComponentExecutionContexts.add(newExecutionContext);
						} else {
							//this is a singleton port
							CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
							newExecutionContext.getPortsStates().get(port).entrySet().forEach(p -> p.setValue((CPathDescription)Utils.deepCopy(pd)));
							newExecutionContext.getPortsStates().get(port).put(connection, (CPathDescription)Utils.deepCopy(pd));
							newComponentExecutionContexts.add(newExecutionContext);
						}
					}
				} else {
					//current connection is known
					for(CPathDescription pd : pathDescriptions) {
						String path = portConnections.get(connection).getPathId();
						if(pd.getPathId().startsWith(path)) {
							//the received path description is from the same path
							CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
							newExecutionContext.getPortsStates().get(port).put(connection, (CPathDescription)Utils.deepCopy(pd));
							newComponentExecutionContexts.add(newExecutionContext);
							if(context.isSingletonPort(port)) { //port singleton
								newExecutionContext.getPortsStates().get(port).entrySet().forEach(p -> p.setValue((CPathDescription)Utils.deepCopy(pd)));
							}
						}
					}
				}
			}
		}
		componentExecutionContexts = newComponentExecutionContexts;
	}
	
	private void updateExecutionStates(String client, String clientPort, String server, String serverPort, List<CPathDescription> pathDescriptions) {
		List<CFunctionalConstraintContext> newComponentExecutionContexts = new ArrayList<CFunctionalConstraintContext>();
		for(CFunctionalConstraintContext executionContext : componentExecutionContexts) {
			Map<String, CPathDescription> portConnections = executionContext.getPortsStates().get(serverPort);
			if(portConnections == null) {
				//no connections at the port yet. For each pathDescription add a new state derived from the current
				for(CPathDescription pd : pathDescriptions) {
					//handle the server side
					CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
					Map<String, CPathDescription> connections = new HashMap<String, CPathDescription>();
					connections.put(client, (CPathDescription)Utils.deepCopy(pd));
					newExecutionContext.getPortsStates().put(serverPort, connections);
					//handle client side
					connections = new HashMap<String, CPathDescription>();
					CPathDescription clientPd = (CPathDescription)Utils.deepCopy(pd);
					clientPd.forwardPort = serverPort; clientPd.forwardConnection = client;
					connections.put(server, clientPd);
					newExecutionContext.getPortsStates().put(clientPort, connections);
					newComponentExecutionContexts.add(newExecutionContext);
				}
			} else {
				//there is at least one connection for the port
				if(!portConnections.containsKey(client)) {
					//current connection is not known yet
					for(CPathDescription pd : pathDescriptions) {
						if(!context.isSingletonPort(serverPort)) {
							//this is multiton port
							//handle the server side
							CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
							newExecutionContext.getPortsStates().get(serverPort).put(client, (CPathDescription)Utils.deepCopy(pd));
							//handle client side
							CPathDescription clientPd = (CPathDescription)Utils.deepCopy(pd);
							clientPd.forwardPort = serverPort; clientPd.forwardConnection = client;
							Map<String, CPathDescription> connections = new HashMap<String, CPathDescription>();
							connections.put(server, clientPd);
							newExecutionContext.getPortsStates().put(clientPort, connections);
							newComponentExecutionContexts.add(newExecutionContext);
						} else {
							//this is a singleton port
							CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
							//handle server side
							newExecutionContext.getPortsStates().get(serverPort).entrySet().forEach(p -> p.setValue((CPathDescription)Utils.deepCopy(pd)));
							newExecutionContext.getPortsStates().get(serverPort).put(client, (CPathDescription)Utils.deepCopy(pd));
							//handle client side
							Map<String, CPathDescription> connections = new HashMap<String, CPathDescription>();
							CPathDescription clientPd = (CPathDescription)Utils.deepCopy(pd);
							clientPd.forwardPort = serverPort; clientPd.forwardConnection = client;
							connections.put(server, clientPd);
							newExecutionContext.getPortsStates().put(clientPort, connections);
							newComponentExecutionContexts.add(newExecutionContext);
						}
					}
				} else {
					//current connection is known
					for(CPathDescription pd : pathDescriptions) {
						String path = portConnections.get(client).getPathId();
						if(pd.getPathId().startsWith(path)) {
							//the received path description is from the same path
							CFunctionalConstraintContext newExecutionContext = (CFunctionalConstraintContext)Utils.deepCopy(executionContext);
							//handle the server side
							newExecutionContext.getPortsStates().get(serverPort).put(client, (CPathDescription)Utils.deepCopy(pd));
							//handle the client side
							//no need to handle
							newComponentExecutionContexts.add(newExecutionContext);
							if(context.isSingletonPort(serverPort)) { //port singleton
								newExecutionContext.getPortsStates().get(serverPort).entrySet().forEach(p -> p.setValue((CPathDescription)Utils.deepCopy(pd)));
							}
						}
					}
				}
			}
		}
		componentExecutionContexts = newComponentExecutionContexts;
	}

	@Override
	public CComponentConstraintResult getResults() {
		return result;
	}
	
	@Override
	public void traceEnded() {
		if(!inErrorState) {
			FileWriter writer;
			try {
				writer = new FileWriter(context.getResultsFile(constraint.getName()), false);
				writer.write(funcConstraintResult.toString());
		        writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public CComponentConstraintResult getResultForLastEvent() {
		return resultPerEvent;
	}
}
