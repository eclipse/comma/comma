/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import java.util.Map;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public abstract class CDataRule extends CRule{
	public void registerObserver(CRuleObserver observer) {
		if (observer instanceof CDataRuleObserver){
			super.registerObserver(observer);
		}
	}
	
	public CConstraintValue consume(CObservedMessage message) {
		CConstraintValue result = super.consume(message);
    	if (result == CConstraintValue.TRUE) {
			notifyOnSuccess();
		}
		return result;
    }
	
	public abstract Map<String, Object> observedValues();
}
