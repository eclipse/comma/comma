/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.comma.monitoring.lib.messages.CObservedCommand;
import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;
import org.eclipse.comma.monitoring.lib.messages.CObservedReply;
import org.eclipse.comma.monitoring.lib.messages.CObservedSignal;
import org.eclipse.comma.monitoring.lib.utils.Utils;

public class CComponentDispatcher extends CDispatcher{
	private CComponentMonitoringTaskContext context;
	private CFactory factory;
	private Map<String, CComponentMonitor> componentMonitors = new HashMap<String, CComponentMonitor>();
	private List<CEntryPoint> entryPoints; //pairs of instance/port that are source or target of messages
	private List<String> componentInstances ; //all instances of the component type to be monitored
	private List<List<CPortInstance>> portPaths;
	private List<CPartDescriptor> partDescriptors; //the direct and indirect parts of the component type to be monitored
	private Map<CInterfaceMonitor, CComponentMonitor> connection2component = new HashMap<CInterfaceMonitor, CComponentMonitor>();
	private List<CMonitorResults> resultsLastEvent;
	
	public CComponentDispatcher(CComponentMonitoringTaskContext context, CFactory factory) {
		this.context = context;
		this.factory = factory;
		this.entryPoints = context.entryPoints;
		this.componentInstances = context.getComponentInstances();
		this.portPaths = context.portPaths;
		this.partDescriptors = context.getMonitoredComponentDescriptor().partDescriptors;
	}

	@Override
	public void consume(CObservedMessage message) throws Exception {
		//start determining if this is a message of interest
		CEntryPoint entry = null; //which instance and port are the recipients of the message
		String instanceToMonitor = null;
		String port = null;
		String rootInstance = null;
		List<CMonitorCall> monitoringPath = new ArrayList<CMonitorCall>();
		String connectionKey = null;
		resultsLastEvent = new ArrayList<CMonitorResults>();
		
		//first check if received at boundary of an instance to be monitored (the alternative is at some of its parts)
		for(CEntryPoint ep : entryPoints) {
			boolean outgoing = false;
			if( (outgoing = (ep.receivingComponentInstance.equals(message.getSource()) && ep.receivingPort.equals(message.getSourcePort()))) || 
				(ep.receivingComponentInstance.equals(message.getDestination()) && ep.receivingPort.equals(message.getDestinationPort()))) {
				entry = ep;
				rootInstance = instanceToMonitor = entry.actualComponentInstance;
				port = entry.actualPort; //always get the outer port even if it is using forwarding connection
				for(List<CPortInstance> path : portPaths) {
					if(path.get(0).port.equals(port) && path.get(0).componentInstance.equals("")) {
						for(CPortInstance pi : path) {
							CMonitorCall call = new CMonitorCall( rootInstance + pi.componentInstance, pi.port);
							if(outgoing) {
								monitoringPath.add(call.renameSource());
							} else {
								monitoringPath.add(call.renameTarget());
							}
						}
						Collections.reverse(monitoringPath);
						break;
					}
				}
				if(message instanceof CObservedCommand || message instanceof CObservedSignal) {
					if(outgoing) {
						//call from a required port
						connectionKey = makeMultitonConnectionKey(message.getDestination(), rootInstance, message.getInterface());
					} else {
						//call to a provided port
						if(context.getMonitoredComponentDescriptor().singletonPorts.contains(port)) { //singleton port
							connectionKey = makeSingletonConnectionKey(rootInstance, port, message.getInterface());
						} else {
							connectionKey = makeMultitonConnectionKey(rootInstance, message.getSource(), message.getInterface());
						}
					}
				} else {
					if(outgoing) {
						//reply or notification from provided port
						if(context.getMonitoredComponentDescriptor().singletonPorts.contains(port)) { //singleton port
							connectionKey = makeSingletonConnectionKey(rootInstance, port, message.getInterface());
						} else {
							connectionKey = makeMultitonConnectionKey(rootInstance, message.getDestination(), message.getInterface());
						}
					} else {
						//reply or notification from required port
						connectionKey = makeMultitonConnectionKey(message.getSource(), rootInstance, message.getInterface());
					}
				}
				break;
			}
		}
		
		//if not yet determined, check if the message is sent to one of the parts
		if(rootInstance == null)
			for(String ci : componentInstances) {
				if(message.getSource().startsWith(ci + ".")) {
					rootInstance = ci;
					boolean isCall = message instanceof CObservedCommand || message instanceof CObservedSignal;
					String otherPort;
					String otherSide;
					if(isCall) {
						port = message.getDestinationPort();
						instanceToMonitor = message.getDestination();
						otherPort = message.getSourcePort();
						otherSide = message.getSource();
					} else {
						port = message.getSourcePort();
						instanceToMonitor = message.getSource();
						otherPort = message.getDestinationPort();
						otherSide = message.getDestination();
					}
					String partId  = instanceToMonitor.substring(ci.length());
					String partType = "";
					for(CPartDescriptor pd : partDescriptors) {
						if(pd.partId.equals(partId)) {
							partType = pd.type;
							break;
						}
					}
					if(context.getComponentDescriptor(partType).singletonPorts.contains(port)) {
						connectionKey = makeSingletonConnectionKey(instanceToMonitor, port, message.getInterface());
					} else {
						connectionKey = makeMultitonConnectionKey(instanceToMonitor,
								  otherSide,
								  message.getInterface());
					}
					List<CMonitorCall> part = new ArrayList<CMonitorCall>();
					for(List<CPortInstance> path : portPaths) {
						if(path.get(0).port.equals(port) && (rootInstance + path.get(0).componentInstance).equals(instanceToMonitor)) {
							for(CPortInstance pi : path) {
								CMonitorCall mc = new CMonitorCall( rootInstance + pi.componentInstance, pi.port);
								if(isCall) {
									part.add(mc.renameTarget());
								} else {
									part.add(mc.renameSource());
								}
							}
							Collections.reverse(part);
							monitoringPath.addAll(part);
							break;
						}
					}
					//second part of the path: on source side
					part = new ArrayList<CMonitorCall>();
					for(List<CPortInstance> path : portPaths) {
						if(path.get(0).port.equals(otherPort) && (rootInstance + path.get(0).componentInstance).equals(otherSide)) {
							for(CPortInstance pi : path) {
								CMonitorCall mc = new CMonitorCall(rootInstance + pi.componentInstance, pi.port);
								if(isCall) {
									part.add(mc.renameSource());
								} else {
									part.add(mc.renameTarget());
								}
							}
							Collections.reverse(part);
							monitoringPath.addAll(part);
							break;
						}
					}
					//Add the root
					int lastDot = message.getSource().lastIndexOf('.');
					String parent = message.getSource().substring(0, lastDot);
					monitoringPath.add(new CMonitorCall(parent, ""));
					break;
				}
		}
		
		if(rootInstance == null) return; // not a message of interest
		
		//check if the monitor exists
		if(!componentMonitors.containsKey(rootInstance)) {
			//instantiate the component monitor for this instance and all its parts (transitively)
			for(CPartDescriptor pd : partDescriptors) {
				CComponentMonitor componentMonitor = new CComponentMonitor(context.createComponentMonitoringContext(rootInstance + pd.partId, pd.type)); 
				componentMonitors.put(rootInstance + pd.partId, componentMonitor);
			}
			//and now for the root
			CComponentMonitor componentMonitor = new CComponentMonitor(context.createComponentMonitoringContext(rootInstance, context.componentType));
			componentMonitors.put(rootInstance, componentMonitor);
		}
		//check if the connection monitor exists
		CInterfaceMonitor interfaceMonitor = connection2monitor.get(connectionKey);
		if(interfaceMonitor == null) { //connection unknown
			interfaceMonitor = new CInterfaceMonitor(context.createPortConnectionContext(message.getInterface(), port, instanceToMonitor, connectionKey), factory);
			connection2monitor.put(connectionKey, interfaceMonitor);
			connection2component.put(interfaceMonitor, componentMonitors.get(instanceToMonitor));
		}
		if(! interfaceMonitor.inErrorState()) {
			if(!interfaceMonitor.consume(message)) {
				CConnectionResults interfaceResults = interfaceMonitor.getResults();
				for (CMonitorCall c : monitoringPath) {
					if(c.instance.equals(instanceToMonitor))
						componentMonitors.get(c.instance).connectionError(interfaceResults);
					else
						componentMonitors.get(c.instance).connectionError();
				}				
			} else {
				//execute the component monitoring path
				for (CMonitorCall c : monitoringPath) {
					CObservedMessage msgCopy = (CObservedMessage) Utils.deepCopy(message);
					if(c.renameSource) {
						msgCopy.setSource(c.instance);
						msgCopy.setSourcePort(c.port);
						if(msgCopy instanceof CObservedReply) {
							((CObservedReply) msgCopy).getCommand().setDestination(c.instance);
							((CObservedReply) msgCopy).getCommand().setDestinationPort(c.port);
						}
					}
					if(c.renameTarget) {
						msgCopy.setDestination(c.instance);
						msgCopy.setDestinationPort(c.port);
						if(msgCopy instanceof CObservedReply) {
							((CObservedReply) msgCopy).getCommand().setSource(c.instance);
							((CObservedReply) msgCopy).getCommand().setSourcePort(c.port);
						}
					}
					componentMonitors.get(c.instance).consume(msgCopy, interfaceMonitor.getPathDescriptions(), connectionKey);
					CComponentResults r = componentMonitors.get(c.instance).getResultForLastEvent();
					if(r != null && r.hasIssues()) {
						resultsLastEvent.add(r);
					}
				}
			}
			CConnectionResults interfaceResult = interfaceMonitor.getResultForLastEvent(); 
			if(interfaceResult!= null && interfaceResult.hasIssues()) {
				resultsLastEvent.add(interfaceResult);
			}
		}
	}

	@Override
	public void traceEnded() throws Exception { //sent only to those not in error state. Monitors in error state has reported and serialized results
		for(CInterfaceMonitor mon : connection2monitor.values().stream().
                filter(h -> !h.inErrorState()).
                collect(Collectors.toList())) {
			mon.traceEnded();
			connection2component.get(mon).connectionMonitoringDone(mon.getResults());
		}
		for(CComponentMonitor mon : componentMonitors.values()) {
			mon.traceEnded();
		}
	}

	@Override
	public List<CMonitorResults> getResults() {
		return componentMonitors.values().stream().map(m -> m.getResults()).collect(Collectors.toList());
	}

	@Override
	public List<CMonitorResults> getIssuesForLastEvent() {
		return resultsLastEvent;
	}
}
