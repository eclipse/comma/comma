/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.comma.monitoring.lib.utils.StringBuilder2;

@SuppressWarnings("serial")
public class CState implements Serializable {
	protected Set<String> activeStates;
	protected String activeState;
	protected String executionState;
	
	protected String snapshot;
	protected Map<String, Integer> clauseMap;
	protected Map<String, Integer> stateMap;
	
	protected String subState = null;
	
	public Set<String> getActiveStates(){
		return activeStates;
	}
	
	public String getActiveState() {
		return activeState;
	}
	
	public String getExecutionState() {
		return executionState;
	}
	
	public void setExecutionState(String state) {
		this.executionState = state;
	}
	
	public void setActiveState(String state) {
		this.activeState = state;
	}
	
	public String getSubState() {
		return subState;
	}
	
	public void setSubState(String state) {
		this.subState = state;
	}
	
	//TODO consider changing the name
	public String toString() {
		return "State Information: variable values and current machine states";
	}
	
	public void takeSnapshot() {
		snapshot = this.toString();
	}
	
	public String getSnapshot() {
		return snapshot;
	}
	
	protected void incMapEntry(String key, Map<String, Integer> map) {
		if(map.containsKey(key)) {
			map.put(key, map.get(key) + 1);
		}else {
			map.put(key, 1);
		}
	}
	
	public void clauseEnacted(String clause) {
		incMapEntry(clause, clauseMap);
	}
	
	public void stateEnacted(String state) {
		incMapEntry(state, stateMap);
	}
	
	public Map<String, Integer> getClauseMap(){
		return clauseMap;
	}
	
	public Map<String, Integer> getStateMap(){
		return stateMap;
	}
	
	protected void mapUnion(Map<String, Integer> m1, Map<String, Integer> m2) {
		for(String key : m2.keySet()) {
			m1.put(key, m1.containsKey(key) ? m1.get(key) + m2.get(key) : m2.get(key));
		}
	}
	
	public void union(CState state) {
		mapUnion(clauseMap, state.getClauseMap());
		mapUnion(stateMap, state.getStateMap());
	}
	
	public String coverageInfoToString() {
		StringBuilder2 builder = new StringBuilder2();
		for(String clause : clauseMap.keySet()) {
			builder.append("State " + clause + ": " + clauseMap.get(clause)); builder.newLine();
		}
		builder.newLine();
		long transitionCoverage = Math.round(clauseMap.values().stream().filter(v -> v > 0).count() * 100.0 / clauseMap.size());
		builder.append("Transition coverage: " + transitionCoverage + "%"); builder.newLine();builder.newLine();
		for(String state : stateMap.keySet()) {
			builder.append("State " + state + ": " + stateMap.get(state)); builder.newLine();
		}
		builder.newLine();
		long stateCoverage = Math.round(stateMap.values().stream().filter(v -> v > 0).count() * 100.0 / stateMap.size());
		builder.append("State coverage: " + stateCoverage + "%"); builder.newLine();builder.newLine();
		
		return builder.toString();
	}
	
	public CCoverageInfo getCoverageInfo() {
		CCoverageInfo result = new CCoverageInfo();
		Map<String, Integer> cm = new LinkedHashMap<String, Integer>();
		mapUnion(cm, clauseMap);
		Map<String, Integer> sm = new LinkedHashMap<String, Integer>();
		mapUnion(sm, stateMap);
		result.setStateCoverage(sm);
		result.setTransitionCoverage(cm);
		return result;
	}
}
