/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CObservedMessage;

@SuppressWarnings("serial")
public class CConjunction extends CBinaryFormula {

	@Override
	public CConstraintValue consume(CObservedMessage message) {
		if(statusLeft == CConstraintValue.UNKNOWN) {
			if( (statusLeft = left.consume(message))== CConstraintValue.FALSE) return CConstraintValue.FALSE ;
		}
		if(statusRight == CConstraintValue.UNKNOWN) {
			if( (statusRight = right.consume(message))== CConstraintValue.FALSE) return CConstraintValue.FALSE ;
		}
		if(statusRight == CConstraintValue.TRUE && statusLeft == CConstraintValue.TRUE) return CConstraintValue.TRUE;
		return CConstraintValue.UNKNOWN;
	}
}
