/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.monitoring.lib.constraints;

import org.eclipse.comma.monitoring.lib.messages.CMessagePattern;

@SuppressWarnings("serial")
public class CConditionedEventTimeRule extends CSimpleTimeRule{
	
	public CConditionedEventTimeRule(CMessagePattern triggeringEvent, CMessagePattern expectedEvent) {
		super();
		errorMessage = "Event is not observed in the expected interval.";
		formula = new CConditionalFollow().
		           setLeft(new CSequence(). 
		                   addElement(new CEventSelector().setEvent(triggeringEvent).setTimeVariable("t1").setEnvironment(env)).
		                   setEnvironment(env)).
		           setRight(new CSequence().
		                    addElement(new CUntil().
		                                 setBody((CStep) new CEventSelector().setEvent(expectedEvent).setTimeVariable("t2").setNegated().setConditionIndex(2).setEnvironment(env)).
		                                 setStop((CStep) new CEventSelector().setEvent(expectedEvent).setTimeVariable("t3").setConditionIndex(1).setEnvironment(env)).
		                                 setEnvironment(env)).
		                    setEnvironment(env));
	}
}
