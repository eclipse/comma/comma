/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.simulator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.comma.behavior.component.component.Component;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.petrinet.EnvConfig;
import org.eclipse.comma.petrinet.PetrinetBuilder;
import org.eclipse.xtext.scoping.IScopeProvider;

public class SimulatorGenerator {
	public static String generateForComponent(Component component, List<EnvConfig> environment, IScopeProvider scopeProvider, String plantumlJar) throws URISyntaxException {
		var petrinetCode = PetrinetBuilder.forComponent(component, environment, scopeProvider, PetrinetBuilder.Mode.NOT_INTERACTIVE);
		return generate(petrinetCode, plantumlJar);
	}
	
	public static String generateForInterface(Interface itf, Parameters parameters, IScopeProvider scopeProvider, String plantumlJar) throws URISyntaxException {
		var petrinetCode = PetrinetBuilder.forInterface(itf, parameters, scopeProvider, PetrinetBuilder.Mode.NOT_INTERACTIVE);
		return generate(petrinetCode, plantumlJar);
	}

	private static String generate(String petrinetCode, String plantumlJar) throws URISyntaxException {
		var code = "SELF_CONTAINED = True\n" + PetrinetBuilder.getModelCode() + PetrinetBuilder.getWalkerCode();
		code += petrinetCode + "\n\n";
		
        var javaExecutable = Paths.get(System.getProperty("java.home"), "bin", "java");

		code += "## Parameters\n";
		code += String.format("JAVA = r'%s'\n", javaExecutable);
		code += String.format("PLANTUML = r'%s'\n", plantumlJar);
		code += "\n";

		code += "## simulator.py\n" + getResourceText("/simulator.py") + "\n\n";
		return code;
	}
	
	private static String getResourceText(String resource) {
		var stream = SimulatorGenerator.class.getResourceAsStream(resource);
		return new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));
	}
}
