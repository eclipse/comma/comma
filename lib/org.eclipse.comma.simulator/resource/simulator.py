#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import threading, subprocess, tempfile, os, sys, ctypes, functools
import tkinter as tk
from typing import Callable, Optional, Dict, List, Any, Type, Set, Literal
from tkinter import ttk, filedialog, messagebox
from snakes.nets import PetriNet
if not 'SELF_CONTAINED' in globals():
    from model import Constraint, Variables, VariablesGL
    from walker import Walker, WalkerStep
    nets: Dict[str, Callable[[], PetriNet]]
    constraints: List[Type['Constraint']]

# From: https://gist.github.com/JackTheEngineer/81df334f3dcff09fd19e4169dd560c59
class VerticalScrolledFrame(ttk.Frame):
    def __init__(self, parent: tk.Widget):
        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            self.canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != self.canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                self.canvas.config(width=interior.winfo_reqwidth())

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != self.canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                self.canvas.itemconfigure(interior_id, width=self.canvas.winfo_width())

        if sys.platform == 'win32':
            def _on_mousewheel(event):
                self.canvas.yview_scroll(int(-1*(event.delta/120)), "units")
            def _bind_to_mousewheel(event):
                self.canvas.bind_all("<MouseWheel>", _on_mousewheel)
            def _unbind_from_mousewheel(event):
                self.canvas.unbind_all("<MouseWheel>")
        else:
            def _on_mousewheel(event, scroll):
                self.canvas.yview_scroll(int(scroll), "units")

            def _bind_to_mousewheel(event):
                self.canvas.bind_all("<Button-4>", functools.partial(_on_mousewheel, scroll=-1))
                self.canvas.bind_all("<Button-5>", functools.partial(_on_mousewheel, scroll=1))

            def _unbind_from_mousewheel(event):
                self.canvas.unbind_all("<Button-4>")
                self.canvas.unbind_all("<Button-5>")

        ttk.Frame.__init__(self, parent)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        vscrollbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        self.canvas = tk.Canvas(self, bd=0, highlightthickness=0, yscrollcommand=vscrollbar.set)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        vscrollbar.config(command=self.canvas.yview)

        # reset the view
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = tk.Frame(self.canvas)
        interior_id = self.canvas.create_window(0, 0, window=interior, anchor=tk.NW)

        interior.bind('<Configure>', _configure_interior)
        self.canvas.bind('<Configure>', _configure_canvas)
        self.canvas.bind('<Enter>', _bind_to_mousewheel)
        self.canvas.bind('<Leave>', _unbind_from_mousewheel)


class TextScrollCombo(ttk.Frame):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # ensure a consistent GUI size
        self.grid_propagate(False)
        # implement stretchability
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        # create a Text widget
        self.txt = tk.Text(self, wrap=tk.NONE)
        self.txt.grid(row=0, column=0, sticky="nsew", padx=2, pady=2)
        # create a Scrollbar and associate it with txt
        scrolly = ttk.Scrollbar(self, command=self.txt.yview)
        scrolly.grid(row=0, column=1, sticky='nsew')
        self.txt['yscrollcommand'] = scrolly.set
        scrollx = ttk.Scrollbar(self, command=self.txt.xview, orient=tk.HORIZONTAL)
        scrollx.grid(row=1, column=0, sticky='nsew')
        self.txt['xscrollcommand'] = scrollx.set

ORIENT = Union[Literal['vertical'], Literal['horizontal']]

class Simulator:
    LABEL_FONT = ("Arial", 11)
    BUTTON_FONT = font=("Arial", 10)
    GRID_PACK_TYPE = Optional[Dict[str, Any]]
    DIVIDER = '-----------'

    def __init__(self, nets: Dict[str, Callable[[], PetriNet]], constraints: List[Type['Constraint']], plantuml: str, java: str) -> None:
        self.walker = Walker(nets, constraints, self.log)
        self.selected_action = None
        self.steps = [(self.walker.get_state(), None)]
        self.saved_state_steps = None
        self.plantuml = plantuml
        self.java = java
        self.recording = []
        self.saved_state_recording = []

    def show_ui(self):
        if sys.platform == 'win32':
            ctypes.windll.shcore.SetProcessDpiAwareness(1)
        self.root = tk.Tk()
        self.root.title("Eclipse CommaSuite Simulator")
        geometry = (2000, 700)
        self.root.geometry(f"{geometry[0]}x{geometry[1]}")
        self.root.bind("<<render_sequence>>", self.render_sequence_render)
        self.sequence_image: Optional[tk.PhotoImage] = None
        self.sequence_image_counter = 0
        self.sequence_image_lock = threading.Lock()

        root_pane = self.add_paned_window(self.root, width=0, orient=tk.HORIZONTAL, pack={'fill': tk.BOTH, 'expand': True})

        client_pane = self.add_paned_window(root_pane, width=250)
        self.add_label(client_pane, 'Client')
        self.client_frame = self.add_scrolled_frame(client_pane).interior

        server_pane = self.add_paned_window(root_pane, width=250)
        self.add_label(server_pane, 'Server')
        self.server_frame = self.add_scrolled_frame(server_pane).interior
        
        actions_controls_pane = self.add_paned_window(root_pane, width=350)
        actions_controls_frame = self.add_frame(actions_controls_pane)
        self.add_label(actions_controls_frame, 'Actions', pack={})
        self.actions_frame = self.add_scrolled_frame(actions_controls_frame, pack={'fill': tk.BOTH, 'expand': True}).interior
        self.add_label(actions_controls_frame, 'Controls', pack={})
        controls_frame = self.add_frame(actions_controls_frame, pack={'fill': tk.X}, rowconfigure=[], columnconfigure=[1, 1])
        self.add_button(controls_frame, 'Log states', command=lambda: self.handle_control_button('log_states'), 
            grid={'column': 0, 'row': 0, 'sticky': tk.EW})
        self.add_button(controls_frame, 'Log variables', command=lambda: self.handle_control_button('log_variables'), 
            grid={'column': 1, 'row': 0, 'sticky': tk.EW})
        self.step_back_button = self.add_button(controls_frame, 'Step back', command=lambda: self.handle_control_button('step_back'), 
            grid={'column': 0, 'row': 1, 'sticky': tk.EW})
        self.reset_button = self.add_button(controls_frame, 'Reset', command=lambda: self.handle_control_button('reset'), 
            grid={'column': 1, 'row': 1, 'sticky': tk.EW})
        self.save_state_button = self.add_button(controls_frame, 'Save state', command=lambda: self.handle_control_button('save_state'), 
            grid={'column': 0, 'row': 2, 'sticky': tk.EW})
        self.restore_state_button = self.add_button(controls_frame, 'Restore state', command=lambda: self.handle_control_button('restore_state'), 
            grid={'column': 1, 'row': 2, 'sticky': tk.EW})

        log_sequence_pane = self.add_paned_window(root_pane, width=0)
        
        log_pane = self.add_paned_window(log_sequence_pane, height=int(geometry[1]/2), orient=tk.HORIZONTAL)
        log_frame = self.add_frame(log_pane, pack={'fill': tk.BOTH, 'expand': True})
        self.add_label(log_frame, 'Log', pack={})
        self.log_text = self.add_scrolled_text(log_frame, pack={'fill': tk.BOTH, 'expand': True})

        sequence_pane = self.add_paned_window(log_sequence_pane, orient=tk.HORIZONTAL)
        sequence_parent_frame = self.add_frame(sequence_pane, pack={'fill': tk.BOTH, 'expand': True})
        self.add_label(sequence_parent_frame, 'Sequence', pack={})
        self.sequence_frame = self.add_scrolled_frame(sequence_parent_frame, pack={'fill': tk.BOTH, 'expand': True})
        self.sequence_frame.canvas.config(background='white')
        self.sequence_frame.interior.config(background='white')

        menubar = tk.Menu(self.root)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Save log", command=lambda: self.handle_menu_action('save_log'))
        filemenu.add_command(label="Save log for testapplication", command=lambda: self.handle_menu_action('save_log_for_testapplication'))
        filemenu.add_command(label="Save sequence", command=lambda: self.handle_menu_action('save_sequence'))
        menubar.add_cascade(label="Save", menu=filemenu)
        self.root.config(menu=menubar)

        self.populate_actions()
        self.render_sequence_start()
        self.update_ui()
        self.root.mainloop()

    def add_grid_pack(self, parent:tk.Widget, child:tk.Widget, grid:GRID_PACK_TYPE=None, pack:GRID_PACK_TYPE=None):
        if isinstance(parent, ttk.PanedWindow): parent.add(child) # type: ignore
        elif grid != None: child.grid(**grid)
        elif pack != None: child.pack(**pack)

    def add_button(self, parent:tk.Widget, text:str, command: Callable[[], None], grid:GRID_PACK_TYPE=None, pack:GRID_PACK_TYPE=None):
        button = tk.Button(parent, text=text, font=self.BUTTON_FONT, command=command)
        self.add_grid_pack(parent, button, grid, pack)
        return button

    def add_paned_window(self, parent:tk.Widget, width:int=0, height:int=0, orient:ORIENT=tk.VERTICAL, pack:GRID_PACK_TYPE=None):
        pane = ttk.PanedWindow(parent, orient=orient)
        self.add_grid_pack(parent, pane, pack=pack)
        pane.config(width=width, height=height)
        return pane

    def add_label(self, parent:tk.Widget, text:str, grid:GRID_PACK_TYPE=None, pack:GRID_PACK_TYPE=None):
        label = tk.Label(parent, text=text, font=self.LABEL_FONT)
        self.add_grid_pack(parent, label, grid, pack)
        return label

    def add_frame(self, parent:tk.Widget, grid:GRID_PACK_TYPE=None, pack:GRID_PACK_TYPE=None, 
            rowconfigure:Optional[List[int]]=None, columnconfigure:Optional[List[int]]=None):
        frame = tk.Frame(parent)
        if rowconfigure != None:
            for i, w in enumerate(rowconfigure): frame.grid_rowconfigure(i, weight=w)
        if columnconfigure != None:
            for i, w in enumerate(columnconfigure): frame.grid_columnconfigure(i, weight=w)
        self.add_grid_pack(parent, frame, grid, pack)
        return frame

    def add_scrolled_frame(self, parent:tk.Widget, grid:GRID_PACK_TYPE=None, pack:GRID_PACK_TYPE=None):
        vsf = VerticalScrolledFrame(parent)
        self.add_grid_pack(parent, vsf, grid, pack)
        return vsf

    def add_scrolled_text(self, parent: tk.Widget, grid:GRID_PACK_TYPE=None, pack:GRID_PACK_TYPE=None):
        frame = self.add_frame(parent, grid, pack)
        txt = TextScrollCombo(frame)
        txt.pack({'fill': tk.BOTH, 'expand': True})
        txt.txt.config(borderwidth=3, relief="sunken")
        return txt.txt

    def populate_actions(self):
        seen: Set[str] = set()
        self.action_buttons: Dict[str, tk.Button] = {}
        for net in self.walker.nets.values():
            for transition in [t for t in net.transition() if t.meta['type'] == 'event']:
                event = transition.meta['event']
                name = event.__str__(True)
                if name in seen: continue
                seen.add(name)
                frame = self.client_frame if transition.meta['event'].is_client_event() else self.server_frame
                self.action_buttons[name] = self.add_button(frame, name, lambda name=name: self.handle_client_server_button(name), pack={'fill': tk.X})

    def handle_menu_action(self, action: str):
        if action == 'save_log':
            f = filedialog.asksaveasfile(title="Save log", filetypes=[("text files","*.txt")], initialfile='log.txt')
            if f is None:  return
            f.write(self.log_text.get("1.0",tk.END))
            f.close()
        elif action == "save_log_for_testapplication":
            f = filedialog.asksaveasfile(title="Save log for testapplication", defaultextension=".recording", filetypes=(("ComMA Test Application recording", "*.recording"),))
            if f is None:  return
            lineWritten = False
            for event in self.recording:
                if (event.is_provided_port() and event.is_client_event()) or (event.is_required_port() and event.is_server_event()):
                    f.write(f"{str(event)}\n")
                    lineWritten = True
            f.close()
            if not lineWritten:
                messagebox.showerror("Error", "No events stored. In case of a single interface, create a component which uses the interface.")
        elif action == 'save_sequence':
            f = filedialog.asksaveasfile(title="Save sequence", filetypes=[("png files","*.png")], initialfile='sequence.png')
            if f is None:  return
            f.close()
            self.sequence_image.write(f.name, format='png')

    def handle_control_button(self, button: str):
        if button == 'log_states':
            self.log('States:')
            for port, machine_states in self.walker.states.items():
                for machine, state in machine_states.items():
                    self.log(f"  {port}.{machine}: {state}")
        elif button == 'log_variables':
            self.log('Variables:')
            for port, net in self.walker.nets.items():
                for marking in net.get_marking().values():
                    variables = [t for t in marking if isinstance(t, Variables) or isinstance(t, VariablesGL)]
                    if len(variables) == 1:
                        if isinstance(variables[0], Variables):
                            vars = [('global', k, v) for k, v in variables[0].vars().items()]
                        else:
                            vars = [*[('global', k, v) for k, v in variables[0].g.vars().items()],
                                *[('local', k, v) for k, v in variables[0].l.vars().items()]]

                        if len(vars) > 0:
                            self.log(f"  {port}")
                            for scope, name, value in vars: self.log(f"    {name} ({scope}): {value}")
            for constraint in self.walker.constraints:
                if len(constraint.get_state()) != 0:
                    if len(constraint.get_state()[0].variables.vars()) != 0:
                        self.log(f"  {constraint.__class__.__name__}")
                        for state in constraint.get_state():
                            for name, value in state.variables.vars().items(): 
                                self.log(f"    {name}: {value}")
        elif button == 'step_back':
            self.steps = self.steps[:-1]
            self.walker.set_state(self.steps[-1][0])
            self.log(f"Stepped back to step #{len(self.steps) - 1}")
            self.log(self.DIVIDER)
            self.recording.pop()
            self.render_sequence_start()
            self.update_ui()
        elif button == 'reset':
            self.steps = self.steps[:1]
            self.walker.set_state(self.steps[0][0])
            self.log(f"Reset to step #0")
            self.log(self.DIVIDER)
            self.recording = []
            self.saved_state_recording = []
            self.render_sequence_start()
            self.update_ui()
        elif button == 'save_state':
            self.saved_state_steps = [*self.steps]
            self.log(f"Saved state")
            self.log(self.DIVIDER)
            self.saved_state_recording = []
            self.saved_state_recording.extend(self.recording)
            self.update_ui()
        elif button == 'restore_state':
            self.steps = self.saved_state_steps
            self.walker.set_state(self.steps[-1][0])
            self.log(f"Restored state")
            self.log(self.DIVIDER)
            self.recording = []
            self.recording.extend(self.saved_state_recording)
            self.render_sequence_start()
            self.update_ui()

    def handle_action_button(self, step: WalkerStep):
        prefix = '->' if step.event.is_client_event() else '<-'
        self.log(f"Step #{len(self.steps)}")
        self.log(f"{prefix} {step.event}")
        self.walker.take_step(step)
        self.log(self.DIVIDER)
        self.recordStep(step)
        self.selected_action = None
        self.steps.append((self.walker.get_state(), step.event))
        self.render_sequence_start()
        self.update_ui()

    def render_sequence_start(self):
        self.sequence_image_counter += 1
        counter = self.sequence_image_counter
        def render():
            with self.sequence_image_lock:
                if counter != self.sequence_image_counter: return
                text = ["@startuml"]
                for step in self.steps:
                    if step[1] != None:
                        if step[1].is_provided_port():
                            order = "provided -> component" if step[1].is_client_event() else "component -> provided"
                        elif step[1].is_required_port():
                           order = "component -> required" if step[1].is_client_event() else "required -> component"
                        else:
                            order = "client -> server" if step[1].is_client_event() else "server -> client"
                        text.append(f"{order}: {str(step[1])}")
                if len(self.steps) == 1:
                    text.append("actor \"push button\"")
                text.append("@enduml")
                try:
                    fd, filename = tempfile.mkstemp()
                    os.write(fd, '\n'.join(text).encode('utf-8'))
                    os.close(fd)
                    subprocess.run([self.java, "-jar", self.plantuml, filename], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                    os.remove(filename)
                    image = f"{filename}.png"
                    if counter == self.sequence_image_counter:
                        self.sequence_image = tk.PhotoImage(file=image)
                        self.root.event_generate("<<render_sequence>>", when="tail")
                    os.remove(image)
                except Exception as e:
                    if counter == self.sequence_image_counter:
                        self.sequence_image = None
                        self.root.event_generate("<<render_sequence>>", when="tail")

        threading.Thread(target=render).start()
    
    def render_sequence_render(self, evt):
        for child in self.sequence_frame.interior.winfo_children(): child.destroy()
        if self.sequence_image != None:
            label = tk.Label(self.sequence_frame.interior, image=self.sequence_image, background='white')
            label.image = self.sequence_image
        else:
            label = tk.Label(self.sequence_frame.interior, text='Render failed', background='white')
        label.pack()
        self.root.after(1, lambda: self.sequence_frame.canvas.yview_moveto(1))

    def handle_client_server_button(self, event: str):
        self.selected_action = event
        self.update_ui()

    def update_ui(self):
        for child in self.actions_frame.winfo_children(): child.destroy() 
        for button in self.action_buttons.values(): button['state'] = 'disabled'
        for port in self.walker.nets.keys():
            for step in self.walker.next_steps(port):
                self.action_buttons[step.event.__str__(True)]['state'] = 'normal'
                if self.selected_action == None or self.selected_action == step.event.__str__(True):
                    self.add_button(self.actions_frame, str(step.event), command=lambda step=step: self.handle_action_button(step), 
                        pack={'fill': tk.X})
        self.step_back_button['state'] = 'normal' if len(self.steps) > 1 else 'disabled'
        self.reset_button['state'] = 'normal' if len(self.steps) > 1 else 'disabled'
        self.restore_state_button['state'] = 'normal' if self.saved_state_steps != None else 'disabled'

    def log(self, txt: str):
        self.log_text.insert(tk.END, txt + "\n")
        self.log_text.see(tk.END)

    def recordStep(self, step: WalkerStep):
        self.recording.append(step.event)

if __name__ == "__main__":
    client = Simulator(nets, constraints, PLANTUML, JAVA)
    client.show_ui()
