#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

if __name__ == "__main__":
    import json, argparse, jinja2, os
    assert len(nets.values()) == 1

    parser = argparse.ArgumentParser()
    parser.add_argument('--output', default='file', choices=['file', 'print'])
    args = parser.parse_args()

    directory = os.path.dirname(__file__)
    template_file = os.path.join(directory, TEMPLATE)

    def read(file: str):
        with open(file) as f: return f.read()
    def write(file: str, content: str):
        with open(file, 'w') as f: f.write(content)

    
    rg = ReachabilityGraph(list(nets.values())[0](), False, MAX_DEPTH)
    s = Scenarios(rg)
    template = jinja2.Template(read(template_file))

    def error(e: str): raise Exception(e)
    code = template.render(scenarios=s, EventType=EventType, error=error, task_name=TASK_NAME)

    if args.output == 'file':
        write(os.path.join(directory, OUTPUT_FILE), code)
        write(os.path.join(directory, 'reachability_graph.json'), json.dumps(rg.to_json(), indent=4))
        write(os.path.join(directory, 'scenarios.json'), json.dumps(s.to_json(), indent=4))
    else:
        print(json.dumps({'reachability_graph': rg.to_json(), 'scenarios': s.to_json(), 'code': code}))
