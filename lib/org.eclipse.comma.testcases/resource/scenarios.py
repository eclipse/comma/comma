#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import itertools
from typing import List, Set
if not 'SELF_CONTAINED' in globals():
    from reachability_graph import ReachabilityGraph, Edge, Node


class Scenarios:
    def __init__(self, rg: 'ReachabilityGraph'):
        self.rg = rg
        self.scenarios: List[List[Edge]] = []
        self.__compute()

    def __compute(self):
        seen_nodes: Set[Node] = set()
        stack: List[List[Edge]] = [[]]
        while len(stack) != 0:
            scenario = stack.pop(0)
            node = self.rg.initial if len(scenario) == 0 else scenario[-1].target
            if node in seen_nodes:
                self.scenarios.append(scenario)
            else:
                seen_nodes.add(node)
                for edge in node.outgoing:
                    new_scenario = [*scenario, edge]
                    # Currently a BFS is done (insert as last), to do a DFS insert at first
                    stack.append(new_scenario)

    def get_scenarios_events(self) -> List[List[Event]]:
        return [[e.get_event() for e in s if e.has_event] for s in self.scenarios]

    def create_comments(self, events: List[Event], indent: str, comment_start: str, comment_intermediate: str, comment_end: str):
        lines: List[str] = []
        for idx, event in enumerate(events):
            next_server_events = list(itertools.takewhile(lambda e: not e.is_client_event(), events[idx + 1:]))
            line = ''
            if event.kind == EventType.Command or event.kind == EventType.Signal:
                if idx != 0: lines.append('')
                if len(next_server_events) > 0:
                    line = f"When calling {event.kind.name.lower()} {event.__str__(include_prefix=False, include_port=False)} we should get:"
                else:
                    line = f"Call {event.kind.name.lower()} {event.__str__(include_prefix=False, include_port=False)}"
            else:
                line = f"- {event.kind.name} {event.__str__(include_prefix=False, include_port=False)}"
            lines.append(line)
        
        lines.insert(0, comment_start)
        lines = [l if i == 0 else f"{indent}{comment_intermediate} {l}" for i, l in enumerate(lines)]
        lines.append(f"{indent}{comment_end}")
        return '\n'.join(lines)

    def get_command_index(self, event: Event, events: List[Event]):
        assert event.kind == EventType.Reply
        for i in range(events.index(event) - 1, -1, -1):
            if events[i].kind == EventType.Command:
                return i

    def get_reply_parameters(self, command_index: int, events: List[Event]):
        for i in range(command_index, len(events)):
            if events[i].kind == EventType.Reply:
                return events[i].parameters
        
    def to_json(self):
        return [[e.to_json() for e in s] for s in self.scenarios]
