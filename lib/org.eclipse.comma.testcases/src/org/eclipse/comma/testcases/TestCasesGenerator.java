/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.testcases;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.stream.Collectors;

import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.petrinet.PetrinetBuilder;
import org.eclipse.comma.petrinet.PetrinetBuilder.Mode;
import org.eclipse.xtext.scoping.IScopeProvider;

public class TestCasesGenerator {
	public static String generate(Interface itf, Parameters params, IScopeProvider scopeProvider, int maxDepth, String templatePath, String outputFile, String taskName) {
		var code = "SELF_CONTAINED = True\n" + PetrinetBuilder.getModelCode() + PetrinetBuilder.getReachabilityGraphCode();
		code += "## scenarios.py\n" + getResourceText("/scenarios.py") + "\n\n";
		code += PetrinetBuilder.forInterface(itf, params, scopeProvider, Mode.NOT_INTERACTIVE) + "\n\n";
		
		code += "## Parameters\n";
		code += String.format("MAX_DEPTH = %d\n", maxDepth);
		code += String.format("OUTPUT_FILE = '%s'\n", outputFile);
		code += String.format("TEMPLATE = '%s'\n\n", templatePath);
		code += String.format("TASK_NAME = '%s'\n\n", taskName);

		code += "## generator_tc.py\n" + getResourceText("/generator_tc.py") + "\n\n";
		return code;
	}
	
	public static TestCasesTemplate getTemplate(File projectFile, String template) {		   
		var resourcePath = String.format("/%s.j2", template);
		if (template == null) {
			return null;
		} else if (TestCasesGenerator.class.getResource(resourcePath) != null) {
			// Check if built-in resource
			var content = getResourceText(resourcePath);
			return new TestCasesTemplate(template + ".j2", content);
		} else {
		   // Resolve template file relative from project file.
		   var file = projectFile.getParentFile().toPath().resolve(template);
		   if (file.toFile().exists()) {
			   try {
				   var content = Files.readString(file);
				   var k =  new TestCasesTemplate(file.toFile().getName(), content);
				   return k;
			   } catch (IOException e) {}	   
		   }
		}
		
		return null;
	}
	
	private static String getResourceText(String resource) {
		var stream = TestCasesGenerator.class.getResourceAsStream(resource);
		return new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));
	}
}
