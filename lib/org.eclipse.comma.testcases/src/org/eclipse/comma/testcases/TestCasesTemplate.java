/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.testcases;

public class TestCasesTemplate {
	public final String content;
	public final String fileName;
	
	public TestCasesTemplate(String fileName, String content) {
		this.fileName = fileName;
		this.content = content;
	}
}
