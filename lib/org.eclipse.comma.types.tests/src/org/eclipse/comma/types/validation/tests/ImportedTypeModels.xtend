/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.validation.tests

import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.util.StringInputStream
import org.eclipse.emf.common.util.URI

class ImportedTypeModels {
	
	def importModel1(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes1.types"));
		res.load(new StringInputStream("
				type A"), emptyMap);
	}
	
	def importModel2(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes2.types"));
		res.load(new StringInputStream("
				type A"), emptyMap);
	}
	
	def importModel3(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes3.types"));
		res.load(new StringInputStream("
				import \"myTypes2.types\"
				type A"), emptyMap);
	}
	
	def importModel4(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes4.types"));
		res.load(new StringInputStream("
				import \"myTypes2.types\"
				type B"), emptyMap);
	}
	
	def importModel5(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes5.types"));
		res.load(new StringInputStream("
				import \"myTypes2.types\"
				type C"), emptyMap);
	}
	
	def importModel6(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes6.types"));
		res.load(new StringInputStream("
				import \"myTypes7.types\"
				type D"), emptyMap);
	}
	
	def importModel7(ResourceSet set){
		val res = set.createResource(URI.createURI("myTypes7.types"));
		res.load(new StringInputStream("
				import \"myTypes6.types\"
				type E"), emptyMap);
	}
}