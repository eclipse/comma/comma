/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.validation.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import com.google.inject.Provider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.comma.types.tests.TypesInjectorProvider
import static org.eclipse.comma.types.types.TypesPackage.Literals.*
import org.eclipse.comma.types.types.TypesModel
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension)
@InjectWith(TypesInjectorProvider)
class SingleModelValidatorTest {
	
	@Inject extension ParseHelper<TypesModel> parseHelper
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider
	
	@Test
	def noErrorsModel(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			type typeTest
			enum enumTest{A B}
			record recordTest{typeTest a, enumTest b, bool[] c}
			vector intArray = int[]
		''', set);
		assertNoIssues(model)
	}
	
	@Test
	def duplicatedLocalTypes(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			type typeTest
			type typeTest
			enum enumTest{A B}
			record recordTest{typeTest a, enumTest b, bool[] c}
			vector intArray = int[]
		''', set);
		assertError(model, TYPE_DECL, null, "Duplicate type declaration name")
	}
	
	@Test
	def overriddenPredefinedType(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			type int
		''', set);
		assertError(model, TYPE_DECL, null, "Local declaration duplicates imported type")
	}
	
	@Test
	def duplicatedFiledsEnumLiterals(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			enum enumTest {A A}
			record recordTest{int a bool a}
		''', set);
		assertError(model, RECORD_FIELD, null, "Duplicate record field name")
		assertError(model, ENUM_ELEMENT, null, "Duplicate enumeration literal name")
	}
	
	@Test
	def illegalUsageOfVoidAny(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			record recordTest{void a any b}
		''', set);
		assertError(model, TYPE, null, "Usage of type any is not allowed")
		assertError(model, TYPE, null, "Usage of type void is not allowed")
	}
	
	@Test
	def circularExtensionInRecords(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			record recordTest1 extends recordTest2{int a}
			record recordTest2 extends recordTest1{int b}
		''', set);
		assertError(model, RECORD_TYPE_DECL, null, "Cycle in the extension hierarchy")
	}
	
	@Test
	def overridingField(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			record recordTest1 {int a}
			record recordTest2 extends recordTest1{int b}
			record recordTest3 extends recordTest2{int a}
		''', set);
		assertError(model, RECORD_FIELD, null, "Local field overrides inherited field")
	}
	
	@Test
	def simpleTypeBaseOk(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			type uint based on int
		''', set);
		assertNoIssues(model)
	}
	
	@Test
	def simpleTypeBaseError(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			type uint based on void
		''', set);
		assertError(model, SIMPLE_TYPE_DECL, "org.eclipse.xtext.diagnostics.Diagnostic.Linking", "Couldn't resolve reference to SimpleTypeDecl 'void'.")
	}
	
	@Test
	def enumLiteralValueOk(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			enum enumTest{A = 0 B C = 3}
		''', set)
		assertNoIssues(model)
	}
	
	@Test
	def enumLiteralValueError(){
		val set = resourceSetProvider.get()
		val model = parse('''			
			enum enumTest{A = 0 B = 2 C = 1}
		''', set)
		assertError(model, ENUM_ELEMENT, null, "Enum value has to be greater than the previous value")
	}
	
	@Test
	def mapKeyTypeError(){
		val set = resourceSetProvider.get()
		val model = parse('''
			vector V  = int[]		
			M = map<V, string[]>
		''', set)
		assertError(model, MAP_TYPE_CONSTRUCTOR, null, "The type of map keys has to be enumeration or simple type")
	}
}