/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.validation.tests

import org.eclipse.xtext.testing.InjectWith
import org.eclipse.comma.types.tests.TypesInjectorProvider
import com.google.inject.Inject
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import com.google.inject.Provider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.util.ParseHelper
import static org.eclipse.comma.types.types.TypesPackage.Literals.*
import org.eclipse.comma.types.types.TypesModel
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension) 
@InjectWith(TypesInjectorProvider)
class ImportModelsValidatorTest {
	
	@Inject extension ValidationTestHelper validationTestHelper
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject extension ParseHelper<TypesModel> parseHelper
	
	var importedTypeModels = new ImportedTypeModels
	
	@Test
	def duplicationsInImportedModels1(){
		val set = resourceSetProvider.get()
		importedTypeModels.importModel1(set)
		importedTypeModels.importModel2(set)
		val model = parse('''	
			import "myTypes1.types"		
			import "myTypes2.types"
			
			type typeTest
			enum enumTest{A B}
			record recordTest{typeTest a, enumTest b, bool[] c}
			vector intArray = int[]
		''', set);
		assertError(model, TYPES_MODEL, null, "Duplicate imported type A")
	}
	
	@Test
	def duplicationsInImportedModels2(){
		val set = resourceSetProvider.get()
		importedTypeModels.importModel2(set)
		importedTypeModels.importModel3(set)
		val model = parse('''	
			import "myTypes3.types"		
			
			type typeTest
			enum enumTest{A B}
			record recordTest{typeTest a, enumTest b, bool[] c}
			vector intArray = int[]
		''', set);
		assertError(model, TYPES_MODEL, null, "Duplicate imported type A")
	}
	
	@Test
	def importDiamondShape(){
		val set = resourceSetProvider.get()
		importedTypeModels.importModel2(set)
		importedTypeModels.importModel4(set)
		importedTypeModels.importModel5(set)
		val model = parse('''	
			import "myTypes4.types"	
			import "myTypes5.types"	
			
			type typeTest
			enum enumTest{A B}
			record recordTest{typeTest a, enumTest b, bool[] c}
			vector intArray = int[]
		''', set);
		assertNoIssues(model)
	}
	
	@Test
	def overridenImport(){
		val set = resourceSetProvider.get()
		importedTypeModels.importModel2(set)
		importedTypeModels.importModel4(set)
		importedTypeModels.importModel5(set)
		val model = parse('''	
			import "myTypes4.types"	
			import "myTypes5.types"	
			
			type A
			enum enumTest{A B}
			record recordTest{typeTest a, enumTest b, bool[] c}
			vector intArray = int[]
		''', set);
		assertError(model, TYPE_DECL, null, "Local declaration duplicates imported type")
	}
	
	@Test
	def cyclesInImports(){
		val set = resourceSetProvider.get()
		importedTypeModels.importModel6(set)
		importedTypeModels.importModel7(set)
		val model = parse('''	
			import "myTypes6.types"		
			
			type A
			enum enumTest{A B}
			record recordTest{D a, E b, bool[] c}
			vector intArray = int[]
		''', set);
		assertNoIssues(model)
	}
	
	@Test
	def modelImportedTwice(){
		val set = resourceSetProvider.get()
		importedTypeModels.importModel6(set)
		importedTypeModels.importModel7(set)
		val model = parse('''	
			import "myTypes6.types"	
			import "myTypes6.types"	
			import "myTypes7.types"		
			
			type A
			enum enumTest{A B}
			record recordTest{D a, E b, bool[] c}
			vector intArray = int[]
		''', set);
		assertNoIssues(model)
	}
}