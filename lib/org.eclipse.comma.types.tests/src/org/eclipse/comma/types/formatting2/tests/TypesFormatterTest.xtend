/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.formatting2.tests

import com.google.inject.Inject
import org.eclipse.comma.types.tests.TypesInjectorProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test

@ExtendWith(InjectionExtension) 
@InjectWith(TypesInjectorProvider)
class TypesFormatterTest {
	@Inject extension FormatterTestHelper

	@Test
	def void testFormatImportWith2BlanksBetweenImportAndString() {
		assertFormatted[
			expectation = '''
				import  "MyOther.types"
				type dummy
			'''
			toBeFormatted = '''import  "MyOther.types"
				type dummy
			'''
		]
	}
	
	@Test
	def void testFormatImportWithNameAndStringInSeparatedLines() {
		assertFormatted[
			expectation = '''
				import
				"MyOther.types"
				type dummy
			'''
			toBeFormatted = '''
				import
				"MyOther.types"
				type dummy
			'''
		]
	}
	
	@Test
	def void testFormatImportWithTwoEntriesSeparatedBy2Lines() {
		assertFormatted[
			expectation = '''
				import "MyOther.types"


				import "MyOther2.types"
				type dummy
			'''
			toBeFormatted = '''
				import "MyOther.types"


				import "MyOther2.types"
				
				type dummy
			'''
		]
	}
	
	@Test
	def void testFormatEnumWith2NameValueAssignmentEntriesInOneLine() {
		assertFormatted[
			expectation = '''
			
			enum A {
				I1 = 1
				I2 = 2
			}'''
			toBeFormatted = '''enum A {I1 = 1 I2 = 2}'''
		]
	}
	
	@Test
	def void testFormatEnumWithNameValueAssignmentSeparatedInLines() {
		assertFormatted[
			expectation = '''
			
			enum A {
				I1 = 1
			}'''
			toBeFormatted = '''enum A {
			I1 
			= 
			1}'''
		]
	}
	
	@Test
	def void testFormatEnumWith2NameEntriesInOneLine() {
		assertFormatted[
			expectation = '''
			
			enum A {
				I1
				I2
			}'''
			toBeFormatted = '''enum A {I1 I2}'''
		]
	}
	
	@Test
	def void testFormatVectorTypeDeclWithNameAndConstructorInDifferentLines() {
		assertFormatted[
			expectation = '''
				vector MyV1 = int[]
			'''
			toBeFormatted = '''
				vector 
				MyV1 
				=
				int[]
			'''
		]
	}
	
	@Test
	def void testFormatVectorTypeDeclWithNameConstructorSurroundedWith2Blanks() {
		assertFormatted[
			expectation = '''vector MyV1 = int[][]
			'''		
			toBeFormatted = '''vector  MyV1  =  int[][]'''
		]
	}
	
	@Test
	def void testFormatVectorTypeConstructorlWithTypeAndBracketsInSeparatedLines() {
		assertFormatted[
			expectation = '''
				vector MyV1 = int[][]
			'''
			
			toBeFormatted = '''vector MyV1 = 
				int
				[
				]
				[
				]'''
		]
	}
	
	@Test
	def void testFormatVectorTypeConstructorlWithTypeAndBracketsAndSizeInSeparatedLines() {
		assertFormatted[
			expectation = '''
			vector MyV1 = int[5][4]
		'''
			toBeFormatted = '''vector MyV1 = 
			int
				[
				5
				]
				[
				4
				]'''
		]
	}
	
	@Test
	def void testFormatVectorTypeDeclWithTwoDeclaration() {
		assertFormatted[
			expectation = '''
			vector MyV1 = int[5][4]
			vector MyV2 = int[3]
		'''
			toBeFormatted = '''vector 
			MyV1 
			= 
			int[5][4]
			
			vector MyV2
			=
			int[3]'''
		]
	}
}
