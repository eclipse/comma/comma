/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.types.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.comma.types.types.TypesModel
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions

@ExtendWith(InjectionExtension) 
@InjectWith(TypesInjectorProvider)
class TypesParsingTest {
	@Inject
	ParseHelper<TypesModel> parseHelper
	
	@Test
	def void loadModel() {
		val result = parseHelper.parse('''
			type A
			type uint based on int
			enum enumTest {A B}
			vector ints = int[]
			record recordTest{A a enumTest b}
			M = map<A, int>
		''')
		Assertions.assertNotNull(result)
		Assertions.assertTrue(result.eResource.errors.isEmpty)
	}
}
