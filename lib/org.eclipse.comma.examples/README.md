# org.eclipse.comma.examples
This project contains examples.

## Adding an example
- Assuming the project with the example is already available, export the project to a zip file
  Note: do not use export in Eclipse, also don't zip the project folder, but select all files in project folder (outside Eclipse) and zip them.
- Place the zip file in project org.eclipse.comma.examples, folder ‘zips’
- Locate project org.eclipse.comma.standard.project. File plugin.xml has to be modified
- In file plugin.xml, new <wizard> element has to be added. An existing wizard element can be copied and then the ‘id’ and ‘name’ attributes have to be changed
- In plugin.xml, new <example> element has to be added. Again, a copy of an existing element will work. Attributes ‘id’ and ‘wizardID’ have to be changed. The wizardID must match the id attribute of the added wizard from the previous step. In the ‘projectDescriptor’ element, change the attribute contentURI to point to the zip with the example. Attribute ‘name’ also has to be changed

Note: also update the list of examples in the user guide.
