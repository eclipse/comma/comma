/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.modelqualitychecks.tests

import com.google.inject.Inject
import com.google.inject.Provider
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Stream
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.modelqualitychecks.ModelQualityChecksGenerator
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.util.StringInputStream
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.DynamicTest.dynamicTest

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class Tests {
    @Inject extension Provider<ResourceSet> resourceSet
    @Inject IScopeProvider scopeProvider
    
    static val resourcePath = "test_resources"
    
    def void test(String path) {
        var set = resourceSet.get()
        var Parameters parameters = null
        var testResourcePath = resourcePath + "/" + path
        var Interface itf = null
        var String expectedReport = null
        for (file : Paths.get(testResourcePath).toFile.listFiles) {
            var ext = file.name.substring(file.name.lastIndexOf("."));
            var contents = Files.readString(Paths.get(testResourcePath + "/" + file.name)).replaceAll("\r\n", "\n").trim()
            if (#[".interface", ".params", ".signature", ".types"].contains(ext)) {
                set.createResource(URI.createURI(file.name))
                    .load(new StringInputStream(contents), emptyMap)
                if (ext.equals(".params")) {
                    parameters = set.getResource(URI.createURI(file.name), true).contents.head as Parameters
                } else if (ext.equals(".interface")) {
                    itf = (set.getResource(URI.createURI(file.name), true).contents.head as InterfaceDefinition).interface
                }
            } 
            else if (file.name.equals("report.json")) { expectedReport = contents }
        }
        
        var code = ModelQualityChecksGenerator.generate(itf, parameters, scopeProvider, 1000, null, "");
        var actualReport = PythonInterpreter.execute(code, #["--output", "print"])
        Assertions.assertEquals(expectedReport, actualReport.replaceAll("\r\n", "\n").trim());
    }

    @TestFactory
    def Stream<DynamicTest> testFromResources() {
        return Paths.get(resourcePath).toFile.listFiles.map[f | dynamicTest(f.name, [| test(f.name)])].stream
    }
}
