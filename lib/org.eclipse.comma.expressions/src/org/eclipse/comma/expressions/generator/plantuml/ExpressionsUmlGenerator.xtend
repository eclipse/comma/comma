/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.expressions.generator.plantuml

import org.eclipse.comma.expressions.expression.ExpressionAddition
import org.eclipse.comma.expressions.expression.ExpressionAnd
import org.eclipse.comma.expressions.expression.ExpressionAny
import org.eclipse.comma.expressions.expression.ExpressionBracket
import org.eclipse.comma.expressions.expression.ExpressionConstantBool
import org.eclipse.comma.expressions.expression.ExpressionConstantInt
import org.eclipse.comma.expressions.expression.ExpressionConstantReal
import org.eclipse.comma.expressions.expression.ExpressionConstantString
import org.eclipse.comma.expressions.expression.ExpressionDivision
import org.eclipse.comma.expressions.expression.ExpressionEnumLiteral
import org.eclipse.comma.expressions.expression.ExpressionEqual
import org.eclipse.comma.expressions.expression.ExpressionFunctionCall
import org.eclipse.comma.expressions.expression.ExpressionGeq
import org.eclipse.comma.expressions.expression.ExpressionGreater
import org.eclipse.comma.expressions.expression.ExpressionLeq
import org.eclipse.comma.expressions.expression.ExpressionLess
import org.eclipse.comma.expressions.expression.ExpressionMaximum
import org.eclipse.comma.expressions.expression.ExpressionMinimum
import org.eclipse.comma.expressions.expression.ExpressionMinus
import org.eclipse.comma.expressions.expression.ExpressionModulo
import org.eclipse.comma.expressions.expression.ExpressionMultiply
import org.eclipse.comma.expressions.expression.ExpressionNEqual
import org.eclipse.comma.expressions.expression.ExpressionNot
import org.eclipse.comma.expressions.expression.ExpressionOr
import org.eclipse.comma.expressions.expression.ExpressionPlus
import org.eclipse.comma.expressions.expression.ExpressionPower
import org.eclipse.comma.expressions.expression.ExpressionQuantifier
import org.eclipse.comma.expressions.expression.ExpressionRecord
import org.eclipse.comma.expressions.expression.ExpressionRecordAccess
import org.eclipse.comma.expressions.expression.ExpressionSubtraction
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.ExpressionVector
import org.eclipse.comma.types.generator.CommaGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.comma.expressions.expression.ExpressionBulkData
import org.eclipse.comma.expressions.expression.ExpressionMap
import org.eclipse.comma.expressions.expression.ExpressionMapRW

class ExpressionsUmlGenerator extends CommaGenerator{	
	
	new(String fileName, IFileSystemAccess fsa) {
		super(fileName, fsa)
	}
	
	def dispatch CharSequence generateExpression(ExpressionAddition expr) 
 	'''«generateExpression(expr.left)» + «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionAnd expr) 
    '''«generateExpression(expr.left)» and «generateExpression(expr.right)»'''
    
    def dispatch CharSequence generateExpression(ExpressionBracket expr) 
    '''(«generateExpression(expr.sub)»)'''

    def dispatch CharSequence generateExpression(ExpressionConstantBool expr) 
    '''«expr.value»'''
    
    def dispatch CharSequence generateExpression(ExpressionConstantReal expr) 
    '''«expr.value»'''

    def dispatch CharSequence generateExpression(ExpressionConstantInt expr) 
    '''«expr.value»'''
    
     def dispatch CharSequence generateExpression(ExpressionConstantString expr) 
    ''''«expr.value»' '''
            
     def dispatch CharSequence generateExpression(ExpressionAny expr) 
    '''*'''
    
    def dispatch CharSequence generateExpression(ExpressionBulkData expr)
    '''bulkdata'''

	def dispatch CharSequence generateExpression(ExpressionDivision expr) 
    '''«generateExpression(expr.left)» / «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionEqual expr) 
    '''«generateExpression(expr.left)» = «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionGeq expr) 
    '''«generateExpression(expr.left)» >= «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionGreater expr) 
    '''«generateExpression(expr.left)» > «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionLeq expr) 
    '''«generateExpression(expr.left)» <= «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionLess expr) 
    '''«generateExpression(expr.left)» < «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionMaximum expr) 
    '''max(«generateExpression(expr.left)», «generateExpression(expr.right)»)'''

    def dispatch CharSequence generateExpression(ExpressionMinimum expr) 
    '''min(«generateExpression(expr.left)», «generateExpression(expr.right)»)'''
    
	 def dispatch CharSequence generateExpression(ExpressionModulo expr) 
    '''(«generateExpression(expr.left)» mod «generateExpression(expr.right)»)'''    
    
     def dispatch CharSequence generateExpression(ExpressionPower expr) 
    '''«generateExpression(expr.left)» ^ «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionMinus expr) 
    '''-«generateExpression(expr.sub)»'''

    def dispatch CharSequence generateExpression(ExpressionMultiply expr) 
    '''«generateExpression(expr.left)» * «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionNEqual expr) 
    '''«generateExpression(expr.left)» != «generateExpression(expr.right)»'''

    def dispatch CharSequence generateExpression(ExpressionNot expr) 
    '''not «generateExpression(expr.sub)»'''

    def dispatch CharSequence generateExpression(ExpressionOr expr) 
    '''«generateExpression(expr.left)» or «generateExpression(expr.right)»'''
    
    def dispatch CharSequence generateExpression(ExpressionPlus expr) 
    '''+«generateExpression(expr.sub)»'''

    def dispatch CharSequence generateExpression(ExpressionSubtraction expr) 
    '''«generateExpression(expr.left)» - «generateExpression(expr.right)»'''
    
	def dispatch CharSequence generateExpression(ExpressionVariable expr)
    '''«expr.variable.name»'''
    
    def dispatch CharSequence generateExpression(ExpressionEnumLiteral expr)
    '''«expr.type.name»::«expr.literal.name»'''
    
    def dispatch CharSequence generateExpression(ExpressionRecord expr)
    '''«expr.type.name»{«FOR f : expr.fields SEPARATOR ', '»«f.recordField.name» = «generateExpression(f.exp)»«ENDFOR»}'''
    
    def dispatch CharSequence generateExpression(ExpressionRecordAccess expr)
    '''«generateExpression(expr.record)».«expr.field.name»'''
    
    def dispatch CharSequence generateExpression(ExpressionVector expr)
    '''[«FOR e : expr.elements SEPARATOR ', '»«generateExpression(e)»«ENDFOR»]'''
    
    def dispatch CharSequence generateExpression(ExpressionMap expr)
    '''{«FOR e : expr.pairs SEPARATOR ', '»«generateExpression(e.key)» -> «generateExpression(e.value)»«ENDFOR»}'''
    
    def dispatch CharSequence generateExpression(ExpressionMapRW expr)
    '''«generateExpression(expr.map)»[«IF expr.context !== null»«expr.context.iterator.name» in «generateExpression(expr.context.collection)» : «ENDIF»«generateExpression(expr.key)»«IF expr.value !== null»->«generateExpression(expr.value)»«ENDIF»]'''
    
    def dispatch CharSequence generateExpression(ExpressionFunctionCall expr)
    '''«expr.functionName»(«FOR a : expr.args SEPARATOR ', '»«generateExpression(a)»«ENDFOR»)'''
    
    def dispatch CharSequence generateExpression(ExpressionQuantifier expr)
    '''«expr.quantifier»(«expr.iterator.name» in «generateExpression(expr.collection)» : «generateExpression(expr.condition)»)'''
}