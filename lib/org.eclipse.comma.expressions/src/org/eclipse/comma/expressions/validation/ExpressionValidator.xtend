/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.expressions.validation

import com.google.inject.Inject
import java.util.List
import org.eclipse.comma.expressions.expression.Expression
import org.eclipse.comma.expressions.expression.ExpressionAddition
import org.eclipse.comma.expressions.expression.ExpressionAnd
import org.eclipse.comma.expressions.expression.ExpressionAny
import org.eclipse.comma.expressions.expression.ExpressionBinary
import org.eclipse.comma.expressions.expression.ExpressionBracket
import org.eclipse.comma.expressions.expression.ExpressionBulkData
import org.eclipse.comma.expressions.expression.ExpressionConstantBool
import org.eclipse.comma.expressions.expression.ExpressionConstantInt
import org.eclipse.comma.expressions.expression.ExpressionConstantReal
import org.eclipse.comma.expressions.expression.ExpressionConstantString
import org.eclipse.comma.expressions.expression.ExpressionDivision
import org.eclipse.comma.expressions.expression.ExpressionEnumLiteral
import org.eclipse.comma.expressions.expression.ExpressionEqual
import org.eclipse.comma.expressions.expression.ExpressionFunctionCall
import org.eclipse.comma.expressions.expression.ExpressionGeq
import org.eclipse.comma.expressions.expression.ExpressionGreater
import org.eclipse.comma.expressions.expression.ExpressionLeq
import org.eclipse.comma.expressions.expression.ExpressionLess
import org.eclipse.comma.expressions.expression.ExpressionMap
import org.eclipse.comma.expressions.expression.ExpressionMapRW
import org.eclipse.comma.expressions.expression.ExpressionMaximum
import org.eclipse.comma.expressions.expression.ExpressionMinimum
import org.eclipse.comma.expressions.expression.ExpressionMinus
import org.eclipse.comma.expressions.expression.ExpressionModulo
import org.eclipse.comma.expressions.expression.ExpressionMultiply
import org.eclipse.comma.expressions.expression.ExpressionNEqual
import org.eclipse.comma.expressions.expression.ExpressionNot
import org.eclipse.comma.expressions.expression.ExpressionOr
import org.eclipse.comma.expressions.expression.ExpressionPackage
import org.eclipse.comma.expressions.expression.ExpressionPlus
import org.eclipse.comma.expressions.expression.ExpressionPower
import org.eclipse.comma.expressions.expression.ExpressionQuantifier
import org.eclipse.comma.expressions.expression.ExpressionRecord
import org.eclipse.comma.expressions.expression.ExpressionRecordAccess
import org.eclipse.comma.expressions.expression.ExpressionSubtraction
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.ExpressionVector
import org.eclipse.comma.expressions.expression.QUANTIFIER
import org.eclipse.comma.types.types.Dimension
import org.eclipse.comma.types.types.MapTypeConstructor
import org.eclipse.comma.types.types.RecordTypeDecl
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.TypeDecl
import org.eclipse.comma.types.types.TypeObject
import org.eclipse.comma.types.types.TypesFactory
import org.eclipse.comma.types.types.VectorTypeConstructor
import org.eclipse.comma.types.types.VectorTypeDecl
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.validation.Check

import static extension org.eclipse.comma.types.utilities.TypeUtilities.*

/*
 * This class mainly captures the CommaSuite type system for expressions. Constraints are not formulated
 * in text here. Consult the document with the formal definition of the CommaSuite type system.
 */
class ExpressionValidator extends AbstractExpressionValidator {
	
	@Inject protected IScopeProvider scopeProvider
	
	protected var SimpleTypeDecl boolType = null;
	protected var SimpleTypeDecl intType = null;
	protected var SimpleTypeDecl realType = null;
	protected var SimpleTypeDecl stringType = null;
	protected var SimpleTypeDecl voidType = null;
	protected var SimpleTypeDecl anyType = null
	protected var SimpleTypeDecl idType = null
	protected var SimpleTypeDecl bulkdataType = null;
	
	new(){
		initPredefinedTypes();
	}
	
	def initPredefinedTypes(){
			
		boolType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); boolType.name = 'bool'
		voidType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); voidType.name = 'void'
		intType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); intType.name = 'int'
		stringType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); stringType.name = 'string'
		realType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); realType.name = 'real'
		anyType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); anyType.name = 'any'
		idType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); idType.name = 'id'
		bulkdataType = TypesFactory.eINSTANCE.createSimpleTypeDecl(); bulkdataType.name = 'bulkdata'
	}
	def boolean identical(TypeObject t1, TypeObject t2) {
		if(t1 === null || t2 === null) return false
		
		if(t1 instanceof SimpleTypeDecl)
			if(t2 instanceof SimpleTypeDecl)
				return t1.name.equals(t2.name)
				
		if(t1 instanceof VectorTypeConstructor)
			if(t2 instanceof VectorTypeConstructor){
				if(!t1.type.identical(t2.type)) return false
				if(t1.dimensions.size != t2.dimensions.size) return false
				for(i : 0..< t1.dimensions.size){
					if(t1.dimensions.get(i).size != t2.dimensions.get(i).size) return false
				}
				return true
			}
			
		if(t1 instanceof MapTypeConstructor)
			if(t2 instanceof MapTypeConstructor){
				return t1.keyType.identical(t2.keyType) && t1.valueType.typeObject.identical(t2.valueType.typeObject)
			}
				
		t1 === t2	
	}
	
	def boolean subTypeOf(TypeObject t1, TypeObject t2){
		if(t1 === null || t2 === null) return false
		if(t1.synonym(t2)) return true //reflexive case
		if(t1.identical(anyType)) return true //any is subtype of all types
		if(t1 instanceof RecordTypeDecl  && t2 instanceof RecordTypeDecl) //record type subtyping
			return getAllParents(t1 as RecordTypeDecl).contains(t2)
			
		if(t1 instanceof VectorTypeConstructor){
			if(t2 instanceof VectorTypeConstructor){
				if(!t1.type.subTypeOf(t2.type)) return false
				if(t1.dimensions.size != t2.dimensions.size) return false
				for(i : 0..< t1.dimensions.size){
					if(t1.dimensions.get(i).size != t2.dimensions.get(i).size) return false
				}
				return true
			}
		}
		false
	}
	
	def boolean synonym(TypeObject t1, TypeObject t2){
		if(t1 === null || t2 === null) return false
		if(t1.identical(t2)) return true //reflexive case
		
		if(t1 instanceof SimpleTypeDecl)
			if(t2 instanceof SimpleTypeDecl){
				return (t1.base.identical(t2)) ||
				       (t2.base.identical(t1)) ||
				       (t1.base.identical(t2.base))
			}
		false
	}
	
	//Type computation. No checking is performed. If the type cannot be determined, null is returned
	def TypeObject typeOf(Expression e){
		if(e === null) return null
		switch(e){
			ExpressionConstantBool |
			ExpressionAnd |
			ExpressionOr |
			ExpressionNot |
			ExpressionEqual |
			ExpressionNEqual |
			ExpressionLess |
			ExpressionGreater |
			ExpressionLeq |
			ExpressionGeq : boolType
			ExpressionConstantInt |
			ExpressionModulo : intType
			ExpressionConstantReal : realType
			ExpressionAddition |
			ExpressionSubtraction |
			ExpressionDivision | 
			ExpressionMultiply |
			ExpressionPower |
			ExpressionMinimum |
			ExpressionMaximum : inferTypeBinaryArithmetic(e)
			ExpressionMinus |
			ExpressionPlus : {
				val t = e.sub.typeOf
				if(t.subTypeOf(intType) || t.subTypeOf(realType))
					t
				else
					null
			}
			ExpressionVariable : e.variable?.type.typeObject
			ExpressionConstantString : stringType
			ExpressionBracket : e.sub?.typeOf
			ExpressionEnumLiteral : e.type
			ExpressionRecord : e.type
			ExpressionRecordAccess : e.field?.type?.typeObject
			ExpressionBulkData : bulkdataType
			ExpressionAny : anyType
			ExpressionFunctionCall : inferTypeFunCall(e)
			ExpressionVector : e.typeAnnotation?.type?.typeObject
			ExpressionQuantifier : {
				if(e.quantifier == QUANTIFIER::DELETE)
					e.collection.typeOf
				else
					boolType
			}
			ExpressionMap : e.typeAnnotation?.type?.typeObject
			ExpressionMapRW : {
				val t = e.map.typeOf
				if(t !== null && t.mapType)
					if(e.value !== null){ // we have map update
						t
					} else {
					    if(e.context === null) { // map read with single key
					       t.valueType
					    } else { // map read of collection of keys
					       val result = TypesFactory.eINSTANCE.createVectorTypeConstructor()
					       result.dimensions.add(TypesFactory.eINSTANCE.createDimension)
					       result.type = t.valueType as TypeDecl
					       return result
					    }
					}
				else
					null
			}
			
		}
	}
		
	def TypeObject inferTypeBinaryArithmetic(ExpressionBinary e){
		val leftType = e.left.typeOf
		val rightType = e.right.typeOf
		switch(e){
			ExpressionAddition : {
				if(leftType.subTypeOf(intType) && rightType.subTypeOf(intType)) return intType
				if(leftType.subTypeOf(realType) && rightType.subTypeOf(realType)) return realType
				if(leftType.subTypeOf(stringType) && rightType.subTypeOf(stringType)) return stringType
				return null
			}
			ExpressionSubtraction |
			ExpressionDivision |
			ExpressionPower | 
			ExpressionMultiply |
			ExpressionMinimum |
			ExpressionMaximum : {
				if(leftType.subTypeOf(intType) && rightType.subTypeOf(intType)) return intType
				if(leftType.subTypeOf(realType) && rightType.subTypeOf(realType)) return realType
				return null
			}
			default : null
		}
	}
	
	def TypeObject inferTypeFunCall(ExpressionFunctionCall e){
		switch(e.functionName){
			case "isEmpty" : boolType
			case "contains" : boolType
			case "hasKey" : boolType
			case "matches" : boolType
			case "asReal" : realType
			case "length",
			case "size" : intType
			case "abs",
			case "add",
			case "deleteKey" : {
				if(! e.args.empty){
					val t = e.args.get(0).typeOf
					if(e.functionName.equals("abs")) {
						if(t.subTypeOf(intType) || t.subTypeOf(realType)) t
						else null
					}
					else e.args.get(0).typeOf
				}	
				else null
			}
			default: null
		}
	}
	
	//Type checking
	
	@Check
	def checkTypingExpression(Expression e){
		switch(e){
			ExpressionAnd |
			ExpressionOr : {
				val leftType = e.left.typeOf
				val rightType = e.right.typeOf
		
				if((leftType !== null) && !leftType.identical(boolType)){ //use subtype instead!
					error("Type mismatch: expected type bool", ExpressionPackage.Literals.EXPRESSION_BINARY__LEFT)
				}
				if((rightType !== null) && !rightType.identical(boolType)){
					error("Type mismatch: expected type bool", ExpressionPackage.Literals.EXPRESSION_BINARY__RIGHT)
				}
			}
			ExpressionNot: {
				val t = e.sub.typeOf
				if((t !== null) && !t.identical(boolType)){
					error("Type mismatch: expected type bool", ExpressionPackage.Literals.EXPRESSION_UNARY__SUB)
				}
			}
			ExpressionLess |
			ExpressionGreater |
			ExpressionLeq |
			ExpressionGeq : {
				val leftType = e.left.typeOf
				val rightType = e.right.typeOf
		
				if(leftType === null || rightType === null) {return}
				if(!leftType.synonym(rightType)){
					error("Arguments must be of compatible types", e.eContainer, e.eContainingFeature)
					return
				}
				if(!leftType.synonym(intType) && !leftType.synonym(realType)){
					error("Type mismatch: expected type int or real", ExpressionPackage.Literals.EXPRESSION_BINARY__LEFT)
				}
			}
			ExpressionAddition |
			ExpressionSubtraction | 
			ExpressionMultiply |
			ExpressionDivision |
			ExpressionModulo |
			ExpressionPower |
			ExpressionMinimum |
			ExpressionMaximum : {
				val leftType = e.left.typeOf
				val rightType = e.right.typeOf
				if(leftType === null || rightType === null) {return}
				if(!leftType.synonym(rightType)){
					error("Arguments must be of compatible types", e.eContainer, e.eContainingFeature)
					return
				}
				if(e instanceof ExpressionModulo){
					if(!leftType.synonym(intType)){
						error("Type mismatch: expected type int", ExpressionPackage.Literals.EXPRESSION_BINARY__LEFT)
					}
					return
				}
				if(e instanceof ExpressionAddition){
					if(!leftType.synonym(intType) && !leftType.synonym(realType) && !leftType.synonym(stringType)){
						error("Type mismatch: expected type int, real or string", ExpressionPackage.Literals.EXPRESSION_BINARY__LEFT)
				}
					return
				}
				if(!leftType.synonym(intType) && !leftType.synonym(realType)){
					error("Type mismatch: expected type int or real", ExpressionPackage.Literals.EXPRESSION_BINARY__LEFT)
				}
				
			}
			ExpressionMinus |
			ExpressionPlus : {
				val t = e.sub.typeOf
				if((t !== null) && !t.subTypeOf(intType) && !t.subTypeOf(realType)){
					error("Type mismatch: expected type int or real", ExpressionPackage.Literals.EXPRESSION_UNARY__SUB)
				}
			}
			ExpressionRecord : {
				if(e.fields.size()!=getAllFields(e.type).size()) {
					error('Wrong number of fields', ExpressionPackage.Literals.EXPRESSION_RECORD__FIELDS)
					return
				} 
				for(f : e.fields){
					if(!f.recordField.name.equals(getAllFields(e.type).get(e.fields.indexOf(f)).name))
						error('Wrong field name', ExpressionPackage.Literals.EXPRESSION_RECORD__FIELDS, e.fields.indexOf(f))
				}
				for(f : e.fields){
					//type checking
					if(! f.exp.typeOf.subTypeOf(getAllFields(e.type).get(e.fields.indexOf(f)).type.typeObject))
						error('Type mismatch', e, ExpressionPackage.Literals.EXPRESSION_RECORD__FIELDS, e.fields.indexOf(f))
				}
			}
			ExpressionMapRW : {
				val mapType = e.map.typeOf
				if(mapType === null) {return}
				if(!mapType.isMapType){
					error("Expression is not a map", ExpressionPackage.Literals.EXPRESSION_MAP_RW__MAP)
					return
				}
				val keyType = e.key.typeOf
				if(keyType !== null && !keyType.identical(mapType.keyType)){
					error("Type of expression does not conform to map key type", ExpressionPackage.Literals.EXPRESSION_MAP_RW__KEY)
				}
				if(e.value !== null){
					val valType = e.value.typeOf
					if(valType !== null && !valType.subTypeOf(mapType.valueType)){
						error("Type of expression does not conform to map value type", ExpressionPackage.Literals.EXPRESSION_MAP_RW__VALUE)
					}
				}
				if(e.context !== null) {
				    val collectionType = e.context.collection.typeOf
                    if(collectionType !== null && !isVectorType(collectionType))
                        error("Expression must be of type vector", e.context, ExpressionPackage.Literals.MAP_RW_CONTEXT__COLLECTION)
				}
			}
			ExpressionMap : {
				if(e.typeAnnotation?.type === null) {return}
				val mapType = e.typeAnnotation.type
				if(! mapType.isMapType){
					error("The type must be a map type", e.typeAnnotation, ExpressionPackage.Literals.TYPE_ANNOTATION__TYPE)
					return
				}
				//check the pairs
				val keyType = mapType.typeObject.keyType
				val valueType = mapType.typeObject.valueType
				for(p : e.pairs){
					val pairKeyType = p.key.typeOf
					val pairValueType = p.value.typeOf
					if(pairKeyType !== null && ! pairKeyType.identical(keyType)){
						error("Type of expression does not conform to map key type", p, ExpressionPackage.Literals.PAIR__KEY)
					}
					if(pairValueType !== null && ! pairValueType.subTypeOf(valueType)){
						error("Type of expression does not conform to map value type", p, ExpressionPackage.Literals.PAIR__VALUE)
					}
				}
			}
			ExpressionVector : {
				if(e.typeAnnotation?.type === null) {return}
				val vectorType = e.typeAnnotation.type
				if(! isVectorType(vectorType)){
					error("The type must be a vector type", e.typeAnnotation, ExpressionPackage.Literals.TYPE_ANNOTATION__TYPE)
					return
				}
				val s = getFirstDimension(vectorType.typeObject)
				if(s > 0 && (e.elements.size != s)){
					error("Expected size of the vector is " + s, ExpressionPackage.Literals.EXPRESSION_VECTOR__ELEMENTS)
					return
				}
				//check the elements
				val expectedType = getBaseTypeToCheck(vectorType.typeObject)
				for(el : e.elements){
					val t = el.typeOf
					if(t !== null && !t.subTypeOf(expectedType))
						error("The element does not conform to the base type", ExpressionPackage.Literals.EXPRESSION_VECTOR__ELEMENTS, e.elements.indexOf(el))
				}
			}
			ExpressionFunctionCall : {
				switch(e.functionName) {
					case "isEmpty" : checkFunIsEmpty(e)
					case "size"	: checkFunSize(e)
					case "contains" : checkFunContains(e)
					case "add" : checkFunAdd(e)
					case "asReal" : checkFunAsReal(e)
					case "abs" : checkFunAbs(e)
					case "length" : checkFunLength(e)
					case "hasKey" : checkFunHasOrDeleteKey(e)
					case "deleteKey" : checkFunHasOrDeleteKey(e)
					case "matches" : checkFunMatches(e)
					default : error("Unknown function", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
				}
			}
			//TODO consider adding a new check if the type of the iterator is compatible with the type of
			//the vector elements
			ExpressionQuantifier: {
				val collectionType = e.collection.typeOf
				if(collectionType !== null && !isVectorType(collectionType))
					error("Expression must be of type vector", ExpressionPackage.Literals.EXPRESSION_QUANTIFIER__COLLECTION)
				val condType = e.condition.typeOf
				if(condType !== null && !condType.subTypeOf(boolType))
					error("Condition expression must be of type boolean", ExpressionPackage.Literals.EXPRESSION_QUANTIFIER__CONDITION)
	
			}
		}
	}
	
	def checkFunIsEmpty(ExpressionFunctionCall e){
		if(e.args.size != 1){
			error("Function isEmpty expects one argument", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
			return
		}
		val t = e.args.get(0).typeOf
		if(t !== null && !isVectorType(t)){
			error("Function isEmpty expects argument of type vector", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
		}
	}
	
	def checkFunSize(ExpressionFunctionCall e){
		if(e.args.size != 1){
			error("Function size expects one argument", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
			return
		}
		val t = e.args.get(0).typeOf
		if(t !== null && !isVectorType(t)){
			error("Function size expects argument of type vector", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
		}
	}
	
	def checkFunContains(ExpressionFunctionCall e){
		if(e.args.size != 2){
			error("Function contains expects two arguments", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
			return
		}
		val t = e.args.get(0).typeOf
		if(t !== null && !isVectorType(t)){
			error("Function contains expects first argument of type vector", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
		}	
	}
	
	def checkFunAdd(ExpressionFunctionCall e){
		if(e.args.size != 2){
			error("Function add expects two arguments", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
			return
		}
		val t = e.args.get(0).typeOf
 		if(t === null || !isVectorType(t)){
 			error("Function add expects first argument of type vector", e, ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
 			return
 		}
		//Check if the second argument conforms to the base type of the vector
		val expectedType = getBaseTypeToCheck(t)
		val elType = e.args.get(1).typeOf
		if(elType !== null && !subTypeOf(elType, expectedType))
			error("Second argument does not conform to the base type of the vector", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 1)	
	}
	
	def checkFunAsReal(ExpressionFunctionCall e){
		if(e.args.size != 1)
			error("Function asReal expects one argument", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
		else{
			val t = e.args.get(0).typeOf
			if(t !== null && !t.subTypeOf(intType))
				error("Function asReal expects an argument of type int", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
		}
	}
	
	def checkFunAbs(ExpressionFunctionCall e){
		if(e.args.size != 1)
			error("Function abs expects one argument", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
		else{
			val t = e.args.get(0).typeOf
			if(t !== null && !(t.subTypeOf(realType) || t.subTypeOf(intType)))
				error("Function abs expects an argument of numeric type", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS)
		}
	}
	
	def checkFunLength(ExpressionFunctionCall e){
		if(e.args.size != 1)
			error("Function length expects one argument", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
		else{
			val t = e.args.get(0).typeOf
			if(t !== null && !t.subTypeOf(bulkdataType))
				error("Function length expects an argument of type bulkdata", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
		}
	}
	
	def checkFunHasOrDeleteKey(ExpressionFunctionCall e){
		if(e.args.size != 2){
			error("This function expects two arguments", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
			return
		}
		val t = e.args.get(0).typeOf
 		if(t === null || !t.isMapType) {
 			error("This function expects first argument of type map", e, ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
 			return
 		}
		//Check if the second argument conforms to the key type of the map
		val expectedType = t.keyType
		var keyExprType = e.args.get(1).typeOf
		if(e.functionName.equals("deleteKey") && keyExprType.isVectorType) {
		    keyExprType = keyExprType.baseType
		}
		if(keyExprType !== null && !identical(keyExprType, expectedType))
			error("Second argument does not conform to the key type of the map", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 1)	
	}
	
	def checkFunMatches(ExpressionFunctionCall e) {
	    if(e.args.size != 2){
            error("This function expects two arguments", ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__FUNCTION_NAME)
            return
        }
        var t = e.args.get(0).typeOf
        if(t === null || !t.subTypeOf(stringType)) {
            error("This function expects first argument of type string", e, ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
            return
        }
        t = e.args.get(1).typeOf
        if(t === null || !t.subTypeOf(stringType)) {
            error("This function expects second argument of type string", e, ExpressionPackage.Literals.EXPRESSION_FUNCTION_CALL__ARGS, 0)
            return
        }
	}
	
	private def getBaseTypeToCheck(TypeObject vt){
		var TypeDecl base
		var List<Dimension> dimensions
		if(vt instanceof VectorTypeDecl){
			base = vt.constructor.type
			dimensions = vt.constructor.dimensions
		}
		else{
			base = (vt as VectorTypeConstructor).type
			dimensions = (vt as VectorTypeConstructor).dimensions
		}
		
		if(dimensions.size == 1){
			base
		}
		else{
			var result = TypesFactory.eINSTANCE.createVectorTypeConstructor
			result.type = base
			for(i : 1..< dimensions.size){
				var newDimension = TypesFactory.eINSTANCE.createDimension
				newDimension.size = dimensions.get(i).size
				result.dimensions.add(newDimension)
			}
			result
		}
	}
}