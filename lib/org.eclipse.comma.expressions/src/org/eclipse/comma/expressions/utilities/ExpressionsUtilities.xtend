/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.expressions.utilities

import java.util.HashSet
import java.util.List
import org.eclipse.comma.expressions.expression.ExpressionQuantifier
import org.eclipse.comma.expressions.expression.ExpressionVariable
import org.eclipse.comma.expressions.expression.Variable
import org.eclipse.xtext.EcoreUtil2

class ExpressionsUtilities {
	/*
	 * Collects all variables used in a quantifier except the quantifier iterator 
	 * variable
	 */
	def static List<Variable> getReferredVariablesInQuantifier(ExpressionQuantifier exp){
		var result = new HashSet<Variable>
		val allExprVariables = EcoreUtil2::getAllContentsOfType(exp, ExpressionVariable)
		for(v : allExprVariables){
			if(!EcoreUtil2::getAllContainers(v.variable).exists(e | e == exp)){
				result.add(v.variable)
			}
		}
		result.toList
	}	
}