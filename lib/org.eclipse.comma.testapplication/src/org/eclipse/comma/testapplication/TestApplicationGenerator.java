/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.testapplication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.comma.behavior.component.component.Component;
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface;
import org.eclipse.comma.parameters.parameters.Parameters;
import org.eclipse.comma.petrinet.EnvConfig;
import org.eclipse.comma.petrinet.PetrinetBuilder;
import org.eclipse.xtext.scoping.IScopeProvider;

public class TestApplicationGenerator {
	public static String generateForComponent(Component component, List<EnvConfig> environment, String adapterPath, double noEventsTimeout, double defaultTimeout, IScopeProvider scopeProvider) {
		var petrinetCode = PetrinetBuilder.forComponent(component, environment, scopeProvider, PetrinetBuilder.Mode.INTERACTIVE);
		return generate(petrinetCode, adapterPath, noEventsTimeout, defaultTimeout);
	}
	
	public static String generateForInterface(Interface itf, Parameters parameters, String adapterPath, double noEventsTimeout, double defaultTimeout, IScopeProvider scopeProvider) {
		var petrinetCode = PetrinetBuilder.forInterface(itf, parameters, scopeProvider, PetrinetBuilder.Mode.INTERACTIVE);
		return generate(petrinetCode, adapterPath, noEventsTimeout, defaultTimeout);
	}

	private static String generate(String petrinetCode, String adapterPath, double noEventsTimeout, double defaultTimeout) {
		var code = "SELF_CONTAINED = True\n" + 
	               "ADAPTER_PATH = " + "\"" + (adapterPath != null ? adapterPath : "") + "\"" + "\n" +  
	               "NO_EVENTS_TIMEOUT = " + (noEventsTimeout != 0.0 ? noEventsTimeout : "5.0") + "\n" +
	               "DEFAULT_TIMEOUT = " + (defaultTimeout != 0.0 ? defaultTimeout : "0.1") + "\n" +
	               PetrinetBuilder.getModelCode() + PetrinetBuilder.getWalkerCode();
		code += petrinetCode + "\n\n";
		code += "## test_application.py\n" + getResourceText("/test_application.py") + "\n\n";
		return code;
	}
	
	private static String getResourceText(String resource) {
		var stream = TestApplicationGenerator.class.getResourceAsStream(resource);
		return new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));
	}
}
