#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import threading, random, asyncio, queue, ctypes, datetime, subprocess, os, io, json, traceback, sys, re, time
from typing import Dict, Callable, List, Optional, Union, Type
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from tkinter import filedialog as fd
from snakes.nets import PetriNet
if not 'SELF_CONTAINED' in globals():
    from model import Event, EventType, Parameters, Constraint
    from model import Event, Constraint
    from walker import Walker
    nets: Dict[str, Callable[[], PetriNet]]
    constraints: List[Type['Constraint']]


class WalkRecorder():
    def __init__(self) -> None:
        self.logger = None
        self.saveInFile = ""
        self.rerunFromFile = ""
        self.file = None
        self.extend = False
        self.continu = False

    def log(self, logger):
        self.logger = logger

    def saveAs(self, file: str):
        if file != None:
            self.saveInFile = file
            self.file = open(file, "w")
    
    def stop(self):
        self.saveInFile = ""
        self.rerunFromFile = ""
        if self.file != None and not self.file.closed:
            self.file.flush()
            self.file.close()
            self.file = None

    def record(self, step: WalkerStep):
        if self.saveInFile != "":
            self.file.write(f"{str(step.event)}\n")

    def playFrom(self, file: str, extend: bool, continu: bool):
        if file != None:
            self.extend = extend
            self.continu = continu
            self.rerunFromFile = file
            self.file = open(file, "r")

    def rerun(self, steps: List[WalkerStep]) -> List[WalkerStep]:
        line = ""
        if self.rerunFromFile == "":
            return steps
        else:
            line = self.file.readline().strip()
            newLine = line
        rStep = []
        for step in steps:
            parts = line.split(' ')
            if len(parts) > 2:
                step.breakStep = True
                newLine = parts[0] + ' ' + parts[1].strip()
            if str(step.event) == newLine:
                rStep.append(step)
        if rStep == [] and line != "":
            self.logger("Cannot select step in {}. Is the SUT non-deterministic?".format(line))
            rStep = None
        if rStep != None and rStep == [] and line == "":
            self.logger("All recorded lines have been executed successfully")
            if self.extend:
                self.logger("Continue testing and append to recorded file")
                self.saveInFile = self.rerunFromFile
                self.rerunFromFile = ""
                self.file = open(self.saveInFile, "a")
                rStep = steps
            else:
                if self.continu:
                    self.logger("Continue testing")
                    self.rerunFromFile = ""
                    rStep = steps
                else:
                    rStep = None
        return rStep


class TestStrategy():
    def __init__(self, strategy: str, recorder: Callable[['WalkRecorder'], None], log: Callable[[str], None], debugger: Callable[['Debugger'], None]) -> None:
        self.strategy = strategy
        self.recorder = recorder
        self.log = log
        self.debugger = debugger
        self.taken_transitions = []

    def next_step(self, walker: Dict[str, Callable[[], Walker]], take_reply_to_cmd: Optional[Event]):
        if self.strategy == "Random":
            return self.next_step_orig(walker, take_reply_to_cmd)
        else: 
            if self.strategy == "Prioritize non-selected":
                return self.next_step_improved(walker, take_reply_to_cmd)
            else:
                assert False

    def next_step_orig(self, walker, take_reply_to_cmd):
        steps = []
        if take_reply_to_cmd != None:
            steps = walker.next_steps((take_reply_to_cmd.port, take_reply_to_cmd.component))
            steps = [c for c in steps if c.event.port == take_reply_to_cmd.port and c.event.component == take_reply_to_cmd.component
                and c.event.kind == EventType.Reply and c.event.method == take_reply_to_cmd.method]
        else:
            connections = list(walker.nets.keys())
            for connection in connections:
                steps.extend(walker.next_steps(connection))
        step = None
        if len(steps) != 0:
            steps = self.recorder.rerun(steps)
            if steps == None:
                return None
            steps = self.debugger.debug_next_step(steps)
            step = random.choice(steps)
            assert step.event != None
        return step
    
    def next_step_improved(self, walker, take_reply_to_cmd):
        steps = []
        if take_reply_to_cmd != None:
            steps = walker.next_steps((take_reply_to_cmd.port, take_reply_to_cmd.component))
            steps = [c for c in steps if c.event.port == take_reply_to_cmd.port and c.event.component == take_reply_to_cmd.component
                and c.event.kind == EventType.Reply and c.event.method == take_reply_to_cmd.method]
        else:
            connections = list(walker.nets.keys())
            for connection in connections:
                steps.extend(walker.next_steps(connection))
        step = None
        if len(steps) != 0:
            steps = self.recorder.rerun(steps)
            if steps == None:
                return None
            rSteps = []
            for step in steps: # If possible, choose a step that has not been taken before
                net = walker.nets[step.event.connectionKey()]
                try:
                    if "_join" in step.clause:
                        step.clause = step.clause[0: (step.clause.rfind("_join"))]
                    if "_split" in step.clause:
                        step.clause = step.clause[0: (step.clause.rfind("_split"))]
                    if not step.clause.endswith("_0"):
                        step.clause = step.clause[0: (step.clause.rfind("_")+1)] + "0"
                    clause_str = step.event.interface + "." + step.event.port + "." + step.clause + "." + net._place[step.clause].meta['sourceline']
                except Exception as e:
                    # self.log(f"step: '{step}', clause: '{net._place[step.clause]}'")
                    # self.log(f"clause.meta: '{net._place[step.clause].meta['sourceline']}'")
                    raise Exception("New exception")
                if clause_str not in walker.seen_clauses:
                    rSteps.append(step)
            if rSteps == []:
                rSteps = steps
            rSteps = self.debugger.debug_next_step(rSteps)
            step = random.choice(rSteps)
        return step

class Debugger():
    def __init__(self):
        self.choice_func = None
        self.lock = threading.Lock()
        self.enable_lock = threading.Lock()
        self.wait = False
        self.enabled = False
        self.take_step = -1

    def debug_register_choice_func(self, func):
        self.choice_func = func

    def debug_set(self, state: bool):
        self.enable_lock.acquire()
        self.enabled = state
        self.enable_lock.release()

    def debug_next_step(self, steps: List[Type['Event']]):
        self.lock.acquire()
        for step in steps:
            self.enable_lock.acquire()
            enabled = self.enabled
            self.enable_lock.release()
            if self.enabled and step.event.has_breakpoint():
                self.wait = True
            if self.enabled and step.breakStep:
                self.wait = True
        has_to_wait = self.wait
        if has_to_wait and self.choice_func != None:
             self.choice_func(steps)
        self.lock.release()
        while has_to_wait:
            time.sleep(0.1)
            self.lock.acquire()
            has_to_wait = self.wait
            self.lock.release()
        self.lock.acquire()
        if self.take_step != -1:
            self.wait = True
            tmpSteps = []
            tmpSteps.extend(steps)
            steps = []
            steps.append(tmpSteps[self.take_step])
        else:
            self.wait = False
        self.lock.release()
        return steps

    def debug_pause(self):
        self.lock.acquire()
        self.wait = True
        self.lock.release()

    def debug_continue(self):
        self.lock.acquire()
        self.wait = False
        self.take_step = -1
        self.lock.release()       

    def debug_step(self, index: str):
        self.lock.acquire()
        self.wait = False
        self.take_step = int(index)
        self.lock.release()

    def debug_timeout(self, timeout: int):
        # self.lock.acquire()
        # if self.wait or self.take_step != -1:
        #     timeout = None # Infinite timeout during debugging
        # self.lock.release()
        return timeout


class TestApplicationWalker():
    def __init__(self, nets: Dict[str, Callable[[], PetriNet]], constraints: List[Type['Constraint']], send_event: Callable[['Event'], None], 
                 stopped: Callable[[Optional[str]], None], strategy: str, log: Callable[[str], None], recorder: Callable[['WalkRecorder'], None], 
                 debugger: Callable[[Optional['Debugger']], None]) -> None:
        self.send_event = send_event
        self.stopped = stopped
        self.walker = Walker(nets, constraints, log)
        self.event_queue: queue.Queue[Union[Event, None]] = queue.Queue()
        self.thread: Optional[threading.Thread] = None
        self.stop_requested = False
        self.recorder = recorder
        self.test_strategy = TestStrategy(strategy, self.recorder, log, debugger)
        self.debugger = debugger

    def start(self):
        assert self.thread == None, "Already running"
        self.thread = threading.Thread(target=self.__run_non_async)
        self.thread.start()
    
    def stop(self):
        if self.thread != None:
            self.stop_requested = True
            self.event_queue.put(None) # Force run to stop
            if threading.current_thread() != self.thread: self.thread.join()
            self.stop_requested = False

    def received_event(self, event: 'Event'):
        self.event_queue.put(event)

    def __run_non_async(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.__run())
        loop.close()

    async def __run(self):
        port_notification_during_command_transition: Dict[str, List[Event]] = {}
        take_reply_to_cmd: Optional[Event] = None
        stop_on_no_events = False
        error: Optional[str] = None

        self.walker.log("Initial states:")
        for (connection, states) in self.walker.states.items():
            for (machine, state) in states.items():
                self.walker.log(f"Connection ({connection[1]}, {connection[0]}) , machine '{machine}' is in state '{state}'")

        try:
            while not self.stop_requested and error == None:
                event: Optional[Event] = None
                try:
                    timeout = NO_EVENTS_TIMEOUT if stop_on_no_events else DEFAULT_TIMEOUT
                    timeout = self.debugger.debug_timeout(timeout)
                    if self.event_queue.qsize() == 0:
                        event = self.event_queue.get(True, timeout)
                    else:
                        event = self.event_queue.get()
                    if event == None: continue # None means we have to stop (added in stop())
                except queue.Empty:
                    pass

                if event != None:
                    stop_on_no_events = False
                    parameter_place_name = f"P_{event.method}{'_reply' if event.kind == EventType.Reply else ''}"
                    # self.walker.log(f"Process event: '{str(event)}'")
                    if not event.connectionKey() in self.walker.nets:
                        error = f"Received event '{str(event)}' from unknown port '{event.port}'"
                        continue
                    if not parameter_place_name in self.walker.nets[event.connectionKey()]._place:
                        error = f"Event '{event.method}' is unknown for port '{event.port}'"
                        continue
                    place = self.walker.nets[event.connectionKey()]._place[parameter_place_name]
                    # self.walker.log(f"Process place: '{str(place)}'")
                    place.add([Parameters([p.value for p in event.parameters])])
                    # self.walker.log(f"Process parameters: '{str(place.meta)}'")
                    steps = [e for e in self.walker.next_steps(event.connectionKey()) if e.event == event]
                    if len(steps) == 0:
                        if event.kind == EventType.Notification and event.port in port_notification_during_command_transition:
                            port_notification_during_command_transition[event.port].append(event)
                        else:
                            error = f"Event '{str(event)}' is not possible"
                    else:
                        step = random.choice(steps)
                        # self.walker.log(f"Process step: '{str(step)}'")
                        self.walker.take_step(step)
                        if event.kind == EventType.Reply:
                            for notification in port_notification_during_command_transition[event.port]:
                                steps = [e for e in self.walker.next_steps(event.connectionKey()) if e.event == notification]
                                if len(steps) == 0:
                                    error = f"Event '{str(notification)}' is not possible"
                                    break
                                else:
                                    self.walker.take_step(random.choice(steps))
                            del port_notification_during_command_transition[event.port]  
                        elif event.kind == EventType.Command:
                            take_reply_to_cmd = event
                else:
                    step = self.test_strategy.next_step(self.walker, take_reply_to_cmd)
                    # self.walker.log(f"Process step: '{str(step)}'")
                    if step == None:
                        if stop_on_no_events:
                            error = "No next steps possible from test application"
                        else:
                            stop_on_no_events = True
                    else:
                        if step.event.kind == EventType.Command:
                            port_notification_during_command_transition[step.event.port] = []
                        self.send_event(step.event)
                        self.recorder.record(step)
                        take_reply_to_cmd = None
                        self.walker.take_step(step)
        except Exception as e:
            error = f"Error while running: {repr(e)}, event: {str(event)}, place: {str(place)}"
            traceback.print_exc()

        if not self.stop_requested:
            self.stopped(error)

class TestApplication:
    running = False
    stopping_or_starting = False
    running_a_rerun = False
    start_time: datetime.datetime
    update_ui_timer: threading.Timer
    adapter: subprocess.Popen
    walker: 'TestApplicationWalker'
    recorder = WalkRecorder()
    debugger = Debugger()

    window = tk.Tk()
    cmd_entry: tk.Entry
    log_text: tk.Text
    start_button: ttk.Button
    save_coverage_button: ttk.Button
    running_time_label: tk.Label
    state_coverage_label: tk.Label
    event_coverage_label: tk.Label
    transition_coverage_label: tk.Label
    save_checkbutton: tk.Checkbutton
    save_var: tk.IntVar
    rerun_button: ttk.Button
    continu_checkbutton: tk.Checkbutton
    continu_var: tk.IntVar
    extend_checkbutton: tk.Checkbutton
    extend_var: tk.IntVar
    test_strategy = tk.StringVar
    dropdown = tk.OptionMenu
    lock = threading.Lock()

    def start_adapter(self):
        def reader(pipe: io.BytesIO, cb: Callable[[str], None]):
            with pipe:
                for line in iter(pipe.readline, b''):
                    cb(line.decode()[:-1])
            self.stop("Adapter stopped")

        self.adapter = subprocess.Popen(self.cmd_entry.get().strip(), stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        threading.Thread(target=reader, args=[self.adapter.stdout, self.on_stdout]).start()
        threading.Thread(target=reader, args=[self.adapter.stderr, self.on_stderr]).start()

    def log(self, line: str):
        if self.running:
            self.lock.acquire()
            numlines = int(self.log_text.index('end - 1 line').split('.')[0])
            while numlines >= 1000:
                self.log_text.delete(1.0, 2.0)
                numlines = int(self.log_text.index('end - 1 line').split('.')[0])
            time = datetime.datetime.now().replace(microsecond=0).isoformat()
            self.log_text.insert(tk.END, f"{time}: {line}\n")
            self.log_text.see(tk.END)
            self.lock.release()

    def send_to_adapter(self, event: 'Event'):
        if self.running:
            self.log(f"-> {str(event)}")
            assert self.adapter.stdin != None
            self.adapter.stdin.write((json.dumps(event.to_json()) + "\n").encode())
            self.adapter.stdin.flush()

    def on_stdout(self, line: str):
        try:
            jsn = json.loads(line)
            if jsn['kind'] == 'Adapter': 
                if jsn['type'] == 'started':
                    self.log("Adapter started")
                    self.walker.start()
            elif self.running:
                event = Event.from_json(jsn)
                self.log(f"<- {str(event)}")
                self.walker.received_event(event)
        except Exception as e:
            self.stop(f"Error while processing adapter stdout: {repr(e)}")
            traceback.print_exc()

    def on_stderr(self, line: str):
        self.log(f"Adapter: {line}")

    def on_closing(self):
        if messagebox.askokcancel("Exit", "Do you really want to exit?"):
            self.running = False
            self.stop(f"exit by user")
            self.window.destroy()
            sys.exit()

    def show_ui(self):
        if sys.platform == 'win32':
            ctypes.windll.shcore.SetProcessDpiAwareness(1)
        self.window.title("Eclipse CommaSuite Test Application")
        self.window.geometry('1200x600')
        self.window.grid_columnconfigure(0, weight=1)
        self.window.grid_rowconfigure(1, weight=1)
        self.window.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Command entry
        self.cmd_entry = tk.Entry(self.window)
        self.cmd_entry.insert(0, ADAPTER_PATH)
        self.cmd_entry.grid(row=0, column=0, sticky=tk.EW, pady=2, padx=2)

        # Log
        log_frame = tk.Frame()
        log_frame.grid_rowconfigure(0, weight=1)
        log_frame.grid_columnconfigure(0, weight=1)
        log_frame.grid(row=1, column=0, padx=2, pady=2, sticky=tk.NSEW)
        log_xscroll = tk.Scrollbar(log_frame, orient=tk.HORIZONTAL)
        log_xscroll.grid(row=1, column=0, sticky=tk.NSEW)
        log_yscroll = tk.Scrollbar(log_frame, orient=tk.VERTICAL)
        log_yscroll.grid(row=0, column=1, sticky=tk.NSEW)
        self.log_text = tk.Text(log_frame, wrap=tk.NONE, yscrollcommand=log_yscroll.set, xscrollcommand=log_xscroll.set, font=("TkFixedFont", 9))
        log_yscroll['command'] = self.log_text.yview # type: ignore
        log_xscroll['command'] = self.log_text.xview # type: ignore
        self.log_text.grid(row=0, column=0, sticky=tk.NSEW) 

        # Right frame
        right_frame = tk.Frame()
        right_frame.grid(row=0, column=1, sticky=tk.N, pady=2, rowspan=2)

        # Debugger frame
        debugger_frame = tk.Frame()
        debugger_frame.grid(row=2, column=0, padx=2, pady=2, sticky=tk.NSEW)

        # Start button
        self.start_button = ttk.Button(right_frame, text="Start", command=self.start_stop)
        self.start_button.grid(column=0, row=0, sticky=tk.EW, columnspan=2)

        # Recorder
        self.save_var = tk.IntVar()
        self.save_checkbutton = tk.Checkbutton(right_frame, text="Save to file", onvalue=1, offvalue=0, variable=self.save_var, command=self.save_clicked)
        self.save_checkbutton.grid(row=1, sticky=tk.W)

        self.rerun_button = ttk.Button(right_frame, text="Rerun from file", command=self.rerun)
        self.rerun_button.grid(column=0, row=2, sticky=tk.EW, columnspan=2)
        self.continu_var = tk.IntVar()
        self.continu_checkbutton = tk.Checkbutton(right_frame, text="Continue recorded run", onvalue=1, offvalue=0, variable=self.continu_var, command=self.continu_clicked)
        self.continu_checkbutton.grid(row=3, sticky=tk.W)
        self.extend_var = tk.IntVar()
        self.extend_checkbutton = tk.Checkbutton(right_frame, text="Continue and save recorded run", onvalue=1, offvalue=0, variable=self.extend_var, command=self.extend_clicked)
        self.extend_checkbutton.grid(row=4, sticky=tk.W)

        # Labels
        tk.Label(right_frame, text = "Running time:").grid(column=0, row=5, sticky=tk.W)
        tk.Label(right_frame, text = "State coverage:").grid(column=0, row=6, sticky=tk.W)
        tk.Label(right_frame, text = "Event coverage:").grid(column=0, row=7, sticky=tk.W)
        tk.Label(right_frame, text = "Transition clauses coverage:").grid(column=0, row=8, sticky=tk.W)
        self.running_time_label = tk.Label(right_frame, text = "200s")
        self.running_time_label.grid(column=2, row=5, sticky=tk.W)
        self.state_coverage_label = tk.Label(right_frame, text = "0/10")
        self.state_coverage_label.grid(column=2, row=6, sticky=tk.W)
        self.event_coverage_label = tk.Label(right_frame, text = "0/20")
        self.event_coverage_label.grid(column=2, row=7, sticky=tk.W)
        self.transition_coverage_label = tk.Label(right_frame, text = "0/20")
        self.transition_coverage_label.grid(column=2, row=8, sticky=tk.W)

        # Save Coverage button
        self.save_coverage_button = ttk.Button(right_frame, text="Save Coverage Info", command=self.save_coverage)
        self.save_coverage_button.grid(column=0, row=9, sticky=tk.EW, columnspan=2)

        # Choose test strategy
        test_strategies = [ 
            "Random", 
            "Prioritize non-selected"
        ]
        self.clicked = tk.StringVar() 
        self.clicked.set(test_strategies[0])
        tk.Label(right_frame, text = "Test strategy:").grid(column=0, row=10, sticky=tk.W)
        self.dropdown = tk.OptionMenu(right_frame, self.clicked, *test_strategies) 
        self.dropdown.grid(column=1, row=10, sticky=tk.W)

        # Debugger
        self.break_var = tk.IntVar()
        self.break_checkbutton = tk.Checkbutton(debugger_frame, text="Break", onvalue=1, offvalue=0, variable=self.break_var, command=self.debug_clicked)
        self.break_checkbutton.grid(row=0, sticky=tk.W)

        self.pause_button = ttk.Button(debugger_frame, text="Pause", command=self.debug_pause)
        self.pause_button.grid(column=0, row=1, sticky=tk.EW, columnspan=1)

        self.continue_button = ttk.Button(debugger_frame, text="Continue", command=self.debug_continue)
        self.continue_button.grid(column=0, row=2, sticky=tk.EW, columnspan=1)

        choice_frame = tk.Frame(debugger_frame)
        choice_frame.grid_rowconfigure(0, weight=1)
        choice_frame.grid_columnconfigure(0, weight=1)
        choice_frame.grid(column=2, row=0, sticky=tk.EW, rowspan=3)
        choice_xscroll = tk.Scrollbar(choice_frame, orient=tk.HORIZONTAL)
        choice_xscroll.grid(row=1, column=0, sticky=tk.NSEW)
        choice_yscroll = tk.Scrollbar(choice_frame, orient=tk.VERTICAL)
        choice_yscroll.grid(row=0, column=1, sticky=tk.NSEW)
        self.choice_text = tk.Text(choice_frame, wrap=tk.NONE, yscrollcommand=choice_yscroll.set, xscrollcommand=choice_xscroll.set, width=98, height=3, font=("TkFixedFont", 9))
        self.choice_text.tag_configure("bold", font=("TkFixedFont", 11, "bold"))
        choice_yscroll['command'] = self.choice_text.yview # type: ignore
        choice_xscroll['command'] = self.choice_text.xview # type: ignore
        self.choice_text.grid(row=0, column=0, sticky=tk.NSEW)
       
        self.choices = [ 
            "0"
        ]
        self.debug_choice_var = tk.StringVar() 
        self.debug_choice_var.set(self.choices[0])
        tk.Label(debugger_frame, text = "Select choice:").grid(column=3, row=0, sticky=tk.W)
        self.debug_choice = tk.OptionMenu(debugger_frame, self.debug_choice_var, *self.choices) 
        self.debug_choice.grid(column=3, row=1, sticky=tk.W)

        self.step_button = ttk.Button(debugger_frame, text="Step", command=self.debug_step)
        self.step_button.grid(column=3, row=2, sticky=tk.EW, columnspan=1)

        self.debugger.debug_register_choice_func(self.choice_func)

        # Disable debugger
        self.pause_button.config(state=tk.DISABLED)
        self.continue_button.config(state=tk.DISABLED)
        self.debug_choice.config(state=tk.DISABLED)
        self.step_button.config(state=tk.DISABLED)

        # Init UI
        self.running_time_label['text'] = '-'
        self.state_coverage_label['text'] = '-'
        self.event_coverage_label['text'] = '-'
        self.transition_coverage_label['text'] = '-'
        self.update_ui()
        self.window.mainloop()

    def update_ui(self):
        try:
            self.start_button['text'] = 'Stop' if self.running else 'Start'
            self.start_button['state'] = 'disabled' if self.stopping_or_starting else 'enabled'
            self.cmd_entry['state'] = 'disabled' if self.running else 'normal'
            if self.running:
                self.running_time_label['text'] = f"{round((datetime.datetime.now() - self.start_time).total_seconds())}s"
                self.state_coverage_label['text'] = f"{len(self.walker.walker.seen_states)}/{len(self.walker.walker.all_states)} ({round(len(self.walker.walker.seen_states)*100/len(self.walker.walker.all_states))}%)"
                self.event_coverage_label['text'] = f"{len(self.walker.walker.seen_events)}/{len(self.walker.walker.all_events)} ({round(len(self.walker.walker.seen_events)*100/len(self.walker.walker.all_events))}%)"
                self.transition_coverage_label['text'] = f"{len(self.walker.walker.seen_clauses)}/{len(self.walker.walker.all_clauses)} ({round(len(self.walker.walker.seen_clauses)*100/len(self.walker.walker.all_clauses))}%)"
        except:
            print("stopped")
            exit(0)

    def update_ui_timer_tick(self):
        self.update_ui()
        self.update_ui_timer = threading.Timer(1, self.update_ui_timer_tick)
        self.update_ui_timer.daemon = True
        self.update_ui_timer.start()

    def stop(self, reason: Optional[str], force: bool = False):
        if (self.stopping_or_starting and not force) or not self.running: return
        self.stopping_or_starting = True
        self.debug_continue()
        self.update_ui()
        self.log(f"Stopping{f': {reason}' if reason != None else ''}")
        self.walker.stop()
        self.adapter.kill()
        self.recorder.stop()
        self.update_ui_timer.cancel()
        self.stopping_or_starting = False
        self.running = False
        self.running_a_rerun = False
        self.log("Stopped")
        self.continu_checkbutton.config(state=tk.NORMAL)
        self.extend_checkbutton.config(state=tk.NORMAL)
        self.save_checkbutton.config(state=tk.NORMAL)
        self.rerun_button.config(state=tk.NORMAL)
        self.save_coverage_button.config(state=tk.NORMAL)
        self.dropdown.config(state=tk.NORMAL)
        self.pause_button.config(state=tk.DISABLED)
        self.continue_button.config(state=tk.DISABLED)
        self.debug_choice.config(state=tk.DISABLED)
        self.step_button.config(state=tk.DISABLED)
        self.update_ui()

    def start_stop(self):
        if self.stopping_or_starting: return
        self.stopping_or_starting = True
        
        if not self.running_a_rerun:
            self.continu_checkbutton.deselect()
            self.extend_checkbutton.deselect()
        
        if self.save_var.get() and self.running == False:
            file = fd.asksaveasfilename(defaultextension=".recording", filetypes=(("ComMA Test Application recording", "*.recording"),))
            if file != "":
                self.recorder.saveAs(file)
        
        self.update_ui()
        
        def clear_coverage_files():
            if os.path.exists(os.path.dirname(__file__) + "/state_coverage.txt"):
                os.remove(os.path.dirname(__file__) + "/state_coverage.txt")
            if os.path.exists(os.path.dirname(__file__) + "/event_coverage.txt"):
                os.remove(os.path.dirname(__file__) + "/event_coverage.txt")
            if os.path.exists(os.path.dirname(__file__) + "/transition_coverage.txt"):
                os.remove(os.path.dirname(__file__) + "/transition_coverage.txt") 
        
        def handle():
            if not self.running:
                self.log('Starting...')
                self.start_time = datetime.datetime.now()
                self.recorder.log(self.log)
                self.walker = TestApplicationWalker(nets, constraints, self.send_to_adapter, self.stop, self.clicked.get(), self.log, self.recorder, self.debugger)
                try:
                    self.start_adapter()
                except Exception as e:
                    self.log(f"Failed to start adapter: '{str(e)}'")
                    self.stopping_or_starting = False
                    self.update_ui()
                    return
                self.update_ui_timer_tick()
                self.running = True
                self.log('Started')
                clear_coverage_files()
                self.continu_checkbutton.config(state=tk.DISABLED)
                self.extend_checkbutton.config(state=tk.DISABLED)
                self.save_checkbutton.config(state=tk.DISABLED)
                self.rerun_button.config(state=tk.DISABLED)
                self.save_coverage_button.config(state=tk.DISABLED)
                self.dropdown.config(state=tk.DISABLED)
                self.pause_button.config(state=tk.NORMAL)
                self.continue_button.config(state=tk.NORMAL)
                self.debug_choice.config(state=tk.NORMAL)
                self.step_button.config(state=tk.NORMAL)
            else:
                self.stop(None, True)
            self.stopping_or_starting = False
            self.update_ui()
            
        hdl = threading.Thread(target=handle)
        hdl.daemon = True
        hdl.start()

    def save_coverage(self):
        if not hasattr(self, 'walker'): return
        f= open(os.path.dirname(__file__) + "/state_coverage.txt","w")
        unseen_states = self.walker.walker.all_states.difference(self.walker.walker.seen_states)
        if len(unseen_states) > 0:
            f.write("Uncovered states:\n\n")
            f.write("Component,Port,Interface,State\n")
            self.print_coverage_info(f, unseen_states, 4)
            f.write("\n")
        if len(self.walker.walker.seen_states) > 0:
            f.write("Covered states:\n\n")
            f.write("Component,Port,Interface,State\n")
            self.print_coverage_info(f, self.walker.walker.seen_states, 4)
        f= open(os.path.dirname(__file__) + "/event_coverage.txt","w")
        unseen_events = self.walker.walker.all_events.difference(self.walker.walker.seen_events)
        if len(unseen_events) > 0:
            f.write("Uncovered events:\n\n")
            f.write("Component,Port,Interface,Event\n")
            self.print_coverage_info(f, unseen_events, 4)
            f.write("\n")
        if len(self.walker.walker.seen_events) > 0:
            f.write("Covered events:\n\n")
            f.write("Component,Port,Interface,Event\n")
            self.print_coverage_info(f, self.walker.walker.seen_events, 4)
        f= open(os.path.dirname(__file__) + "/transition_coverage.txt","w")
        unseen_clauses = self.walker.walker.all_clauses.difference(self.walker.walker.seen_clauses)
        if len(unseen_clauses) > 0:
            f.write("Uncovered transition clauses:\n\n")
            f.write("Component,Port,Interface,Clause,SourceLine\n")
            self.print_coverage_info(f, unseen_clauses, 5)
            f.write("\n")
        if len(self.walker.walker.seen_clauses) > 0:
            f.write("Covered transition clauses:\n\n")
            f.write("Component,Port,Interface,Clause,SourceLine\n")
            self.print_coverage_info(f, self.walker.walker.seen_clauses, 5)
        f.close()
        messagebox.showinfo("Information", "Coverage info files saved.") 

    def print_coverage_info(self, f, info, nrColumns):
        ports = dict()
        for s in info:
            fragments = s.split(".")
            port = fragments[2] + "," + fragments[1] + "," + fragments[0]
            if nrColumns == 4:
                content = ".".join(fragments[3:])
            else:
                content = ".".join(fragments[3:len(fragments)-1]) + "," + fragments[-1]
            if ports.get(port) == None:
                ports.update({port: [content]})
            else:
                ports.get(port).append(content)
        keys_list = [k for k in ports.keys()]
        keys_list.sort()
        for k in keys_list:
            ports[k].sort()
            for v in ports[k]:
                f.write(k + "," + v + "\n")

    def rerun(self):
        self.save_checkbutton.deselect()
        file = fd.askopenfilename(defaultextension=".recording", filetypes=(("ComMA Test Application recording", "*.recording"),))
        if file != "" and os.path.exists(file):
            self.recorder.playFrom(file, self.extend_var.get(), self.continu_var.get())
            self.running_a_rerun = True
            self.start_stop()

    def save_clicked(self):
        self.extend_checkbutton.deselect()
        self.continu_checkbutton.deselect()

    def continu_clicked(self):
        self.extend_checkbutton.deselect()
        self.save_checkbutton.deselect()

    def extend_clicked(self):
        self.save_checkbutton.deselect()
        self.continu_checkbutton.deselect()

    def debug_clicked(self):
        self.debugger.debug_set(self.break_var.get())

    def debug_pause(self):
        self.debugger.debug_pause()

    def debug_continue(self):
        if self.running:
            self.choice_text.delete('1.0', tk.END)
            self.debugger.debug_continue()

    def debug_step(self):
        self.debugger.debug_step(self.debug_choice_var.get())
    
    def choice_func(self, steps: Callable[['Event'], None]):
        counter = 0
        self.debug_choice_var.set('')
        self.debug_choice['menu'].delete(0, 'end')
        self.choice_text.delete('1.0', tk.END)
        self.debug_choice_var.set(f"0")
        for step in steps:
            if step.event.has_breakpoint():
                self.choice_text.insert(tk.END, f"{counter}: {step.event} -> break\n", "bold")
                self.debug_choice_var.set(f"{counter}")
            else:
                self.choice_text.insert(tk.END, f"{counter}: {step.event}\n")
            choice = f"{counter}"
            self.debug_choice['menu'].add_command(label=choice, command=tk._setit(self.debug_choice_var, choice))
            counter += 1
        self.choice_text.see(tk.END)

if __name__ == "__main__":
    client = TestApplication()
    client.show_ui()
