#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

import argparse, re, sys, json
from typing import Dict, Set, Any, List

IGNORE = [
    "org.eclipse", "org.w3c", "org.junit", "javax", "com.sun", "org.apache.ant", "org.apache.lucene", "org.sat4j.pb",
    "com.jcraft.jsch", "org.opentest4j", "org.apiguardian", "org.apache.httpcomponents.httpclient", "org.sat4j.core",
    "org.apache.jasper.glassfish", "org.apache.httpcomponents", "types", "commasuite-monitoring-dashboard", "commasuite-modelqualitychecks-dashboard"
]
MAPPING: List[Dict[str, Any]] = [
    {"name": "ANTLR", "package": "org.antlr", "license": "BSD 3-clause license", "project": "https://www.antlr.org/", "source": "https://github.com/antlr/antlr4"},
    {"name": "ASM", "package": "org.objectweb.asm", "license": "3-Clause BSD License", "project": "https://asm.ow2.io/", "source": "https://gitlab.ow2.org/asm/asm"},
    {"name": "Apache Batik", "package": "org.apache.batik", "license": "Apache License 2.0", "project": "https://xmlgraphics.apache.org/batik/", "source": "https://github.com/apache/xmlgraphics-batik"},
    {"name": "Apache Commons", "package": "org.apache.commons", "license": "Apache License 2.0", "project": "https://ant.apache.org/", "source": "https://github.com/apache/commons-lang"},
    {"name": "Apache Felix", "package": "org.apache.felix", "license": "Apache License 2.0", "project": "https://felix.apache.org/", "source": "https://github.com/apache/felix-dev"},
    {"name": "Apache Log4j", "package": "org.apache.log4j", "license": "Apache License 2.0", "project": "https://logging.apache.org/log4j", "source": "https://github.com/apache/log4j"},
    {"name": "Apache XML Graphics", "package": "org.apache.xmlgraphics", "license": "Apache License 2.0", "project": "https://xmlgraphics.apache.org/commons/", "source": "https://xmlgraphics.apache.org/fop/download.html"},
    {"name": "Classgraph", "package": "io.github.classgraph", "license": "MIT", "project": "https://github.com/classgraph/classgraph", "source": "https://github.com/classgraph/classgraph"},
    {"name": "Google Guava", "package": "com.google.guava", "license": "Apache License 2.0", "project": "https://github.com/google/guava", "source": "https://github.com/google/guava"},
    {"name": "Google Guice", "package": "com.google.inject", "license": "Apache License 2.0", "project": "https://github.com/google/guice", "source": "https://github.com/google/guice"},
    {"name": "ICU", "package": "com.ibm.icu", "license": "IBM ICU License", "project": "http://site.icu-project.org/", "source": "https://github.com/unicode-org/icu"},
    {"name": "PlantUML", "package": "net.sourceforge.plantuml", "license": "GPL", "project": "https://plantuml.com/", "source": "https://github.com/plantuml/plantuml"},
    {"name": "XZ Utils", "package": "org.tukaani.xz", "license": "Public domain", "project": "https://tukaani.org/xz/java.html", "source": "https://git.tukaani.org/?p=xz-java.git;a=summary"},
    {"name": "M2Doc", "package": "org.obeonetwork.m2doc", "license": "Eclipse Public License 1.0", "project": "https://www.m2doc.org/", "source": "https://github.com/ObeoNetwork/M2Doc"},
    {"name": "Apache POI", "package": "org.apache.poi", "license": "Eclipse Public License 1.0", "project": "https://poi.apache.org/", "source": "https://github.com/apache/poi"},
    {"name": "Java Hamcrest", "package": "org.hamcrest.core", "license": "BSD License", "project": "http://hamcrest.org/JavaHamcrest/", "source": "https://github.com/hamcrest/JavaHamcrest"},
    {"name": "Gson", "package": "com.google.gson", "license": "Apache License 2.0", "project": "https://github.com/google/gson", "source": "https://github.com/google/gson"},
    {"name": "Bouncy Castle Crypto Package", "package": "org.bouncycastle.bcp", "license": "Custom", "project": "https://www.bouncycastle.org/java.html", "source": "https://github.com/bcgit/bc-java"},
]
# Some npm packages are missing a repository URL, map them here.
NPM_MISSING_URL = {
    'mapbox/geojson-types': 'https://github.com/mapbox/geojson-types', 
    'quickselect': 'https://github.com/mourner/quickselect'
}

def is_ignored(dependency: str):
    return len([d for d in IGNORE if re.match(d, dependency)]) > 0

def find(dependency: str):
    for m in MAPPING:
        if re.match(m['package'], dependency):
            m['used'] = True
            return m
    assert False

def add(package: str, version: str, obj: Dict[str, Set[str]]):
    if not package in obj: obj[package] = set()
    obj[package].add(version)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('build_log')
    parser.add_argument('dashboard_dependencies')
    parser.add_argument('output')
    args = parser.parse_args()

    # Maven (build.log)
    with open(args.build_log) as f:
        lines = f.readlines()

    dependencies_java: Dict[str, Set[str]] = {}
    pattern = re.compile(r"\[INFO\]\s*p2\.eclipse-plugin:(.*):system")
    for line in lines:
        match = pattern.match(line)
        if match:
            dependency = match[1].split(":")[0]
            version = match[1].split(":")[-1]

            if not is_ignored(dependency):
                try:
                    mapped = find(dependency)
                    add(mapped['package'], version, dependencies_java)
                except:
                    print("ERROR unampped dependency: %s" % dependency)
                    sys.exit(1)

    # Dashboard (dependencies.json)
    dependencies_dashboards: Dict[str, List[Dict[str, Any]]] = {}
    for file in args.dashboard_dependencies.split(','):
        with open(file) as f:
            dashboard_dependencies = json.load(f)

        for key, value in dashboard_dependencies.items():
            dependency = key.rsplit("@")[-2]
            version = key.rsplit("@")[-1]
            if not is_ignored(dependency):
                if not dependency in dependencies_dashboards: dependencies_dashboards[dependency] = []
                source = value['repository'] if 'repository' in value else NPM_MISSING_URL[dependency]
                dependencies_dashboards[dependency].append({'name': dependency, 'version': version, 'license': value['licenses'], 'source': source})

    output = open(args.output, "w")
    output.write("# Third-party Content\n\n")
    for dependency, version in sorted(dependencies_java.items(), key=lambda x: find(x[0])['name']):
        mapped = find(dependency)
        version = ', '.join([v.split('.v')[0] for v in sorted(version)])
        output.write("## %s (%s)\n\n" % (mapped['name'], version))
        output.write("- License: %s\n" % mapped['license'])
        output.write("- Project: %s\n" % mapped['project'])
        output.write("- Source: %s\n" % mapped['source'])
        output.write("\n")

    output.write("## Dashboards (NPM)\n\n")
    for name, dependency in sorted(dependencies_dashboards.items(), key=lambda x: x[0]):
        versions = ', '.join([d['version'] for d in dependency])
        d = dependency[0]
        output.write(f"### {d['name']} ({versions})\n")
        output.write(f"- License: {d['license']}\n")
        output.write(f"- Source: {d['source']}\n")
        output.write("\n")

    print("Generated to '%s'" % args.output)
    output.close()

    for package in MAPPING:
        if not 'used' in package:
            print('%s in MAPPING but not used' % package['package'])
            sys.exit(1)
