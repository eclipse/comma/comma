/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.ide

import com.google.inject.Guice
import org.eclipse.comma.signature.InterfaceSignatureRuntimeModule
import org.eclipse.comma.signature.InterfaceSignatureStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class InterfaceSignatureIdeSetup extends InterfaceSignatureStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new InterfaceSignatureRuntimeModule, new InterfaceSignatureIdeModule))
	}
	
}
