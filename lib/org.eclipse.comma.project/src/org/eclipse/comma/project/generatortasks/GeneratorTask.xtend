/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.io.File
import java.io.FilenameFilter
import java.util.ArrayList
import java.util.List
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.scoping.InterfaceUtilities
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.project.project.FilePath
import org.eclipse.comma.project.project.InterfaceReference
import org.eclipse.comma.project.project.Task
import org.eclipse.comma.project.project.TraceSource
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider

abstract class GeneratorTask {

	final protected List<String> errors
	final protected Task task
	final protected Resource resource
	final protected IScopeProvider scopeProvider
	final protected IFileSystemAccess2 fsa
	final protected String projectFileName
	final protected OutputLocator outputLocator

	new(Task task, IScopeProvider scopeProvider, OutputLocator outputLocator,
		IFileSystemAccess2 fsa) {
		this.task = task
		this.resource = task.eResource
		this.scopeProvider = scopeProvider
		this.fsa = fsa

		this.errors = new ArrayList<String>
		this.projectFileName = resource.URI.lastSegment
		this.outputLocator = outputLocator
	}

	def List<String> startGeneration() {
		try {
			doGenerate
		} catch (IllegalArgumentException e) {
			errors.add(e.message)
		}
		return errors
	}

	abstract protected def void doGenerate() throws IllegalArgumentException
	
	protected def IllegalArgumentException createException(String message) {
		new IllegalArgumentException(message + " - " + resource.URI.lastSegment + " (" + class.simpleName + ")")
	}

	protected def IllegalArgumentException createException(String message, Resource context, Class<?> cl) {
		new IllegalArgumentException(message + " - " + context.URI.lastSegment + " (" + cl.simpleName + ")")
	}

	def commaFSA() {
		new CommaFileSystemAccess(fsa)
	}

	def commaFSA(String root) {
		val path =  if(!root.endsWith("/")) root + "/" else root
		new CommaFileSystemAccess(path, fsa)
	}
	
	def getSignature(Interface cInterface) {
		InterfaceUtilities.getSignature(cInterface, scopeProvider)
	}

	/* outputLocator */
	def getGeneratedFile(Task task) {
		outputLocator.getGeneratedFile(task)
	}
	
	def getFileName(Task task) {
		outputLocator.getFileName(task)
	}

	def getOutputFolder(Task task) {
		outputLocator.getOutputFolder(task)
	}

	def getFileSystemAccess(Task task) {
		commaFSA(outputLocator.getOutputFolder(task))
	}
	
	def getTraceResource(TraceSource source) {
		outputLocator.getTraceResource(fsa, source)
	}
	
	def URI getTraceResourceURI(TraceSource source){
		outputLocator.getTraceResourceURI(fsa, source)
	}
	
	/*********************** ExecutableReference ******************************/
	
	def dispatch getInterface(InterfaceReference iRef) {
		iRef.interface
	}
	
	def dispatch getInterface (ComponentReference iRef) {
		return null
	}
	
	def getResourcesFromDirs(List<FilePath> directories) {
		val resources = new ArrayList<Resource>
		for (location : directories) {
			var uri = resource.resolveUri(location.path)	 		
			if(uri.isPlatform) {
				val platform = uri.toPlatformString(true)
				val IResource eclipseResource = ResourcesPlugin.workspace.root.findMember(platform)				
				uri =  URI.createFileURI(eclipseResource.rawLocation.toOSString);		
			}
			
			val traceFiler = new FilenameFilter() {
				override accept(File dir, String name) {
					(name.endsWith(".traces") || name.endsWith(".events") || name.endsWith(".jsonl"))
				}
			}
			
			val dir = new File(uri.toFileString)
			if (dir.exists && dir.isDirectory) {
				for (file : dir.listFiles(traceFiler)) {
					val res = resource.resourceSet.getResource(URI.createFileURI(file.path), true)				
					if(res !== null) {
						resources.add(res)
					} else { 
						errors.add("Trace resource could not be loaded: " + file.path +".")
					}					
				}
			} else {
				errors.add("Trace dir did not exist or is not a directory. " + dir.path)
			}
		}
		resources
	} 
	
	def static resolveUri(Resource context, String path) {
		val contextURI = context.getURI();
		var uri = URI.createFileURI(path)
		if (contextURI.isHierarchical() && !contextURI.isRelative() && (uri.isRelative() && !uri.isEmpty())) {
			uri = uri.resolve(contextURI);
		}		
		return uri;
	}
}
