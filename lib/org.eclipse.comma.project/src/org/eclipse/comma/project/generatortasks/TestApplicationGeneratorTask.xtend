/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.project.ProjectUtility
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.project.project.TestApplicationGenerationTask
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.comma.testapplication.TestApplicationGenerator
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider

class TestApplicationGeneratorTask extends GeneratorTask {
	
	final protected TestApplicationGenerationTask task
	final protected IScopeProvider scopeProvider
	final protected IFileSystemAccess2 fsa
	protected CommaFileSystemAccess libFileSystemAccess;
	
	new(TestApplicationGenerationTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
		super(task, scopeProvider, outputLocator, fsa)
		this.task = task
		this.scopeProvider = scopeProvider
		this.fsa = fsa
	}

	override protected doGenerate()
	{
	    var List<Parameters> parameters = new ArrayList<Parameters>();
        if (task.params !== null) {
            parameters = task.params.map[p | EcoreUtil2.getResource(task.eResource, p).contents.head as Parameters]
        }
        var String testApplication;
        val target = task.target;
        if (target instanceof ComponentReference) {
            val environment = ProjectUtility::getComponentEnvironmentConfig(task, target.component, task.envComponents, parameters, scopeProvider)
            testApplication = TestApplicationGenerator.generateForComponent(target.component, environment, task.adapterPath, task.noEventsTimeout, task.defaultTimeout, scopeProvider);
        } else {
            val parameter = !parameters.isEmpty ? parameters.get(0) : null;
            testApplication = TestApplicationGenerator.generateForInterface(target.interface, parameter, task.adapterPath, task.noEventsTimeout, task.defaultTimeout, scopeProvider);
        }

	    var path = "testapplication/" + task.name + "/";
	    var filePath = path + "testapplication.py"
	    fsa.generateFile(filePath, testApplication)
	    
	    var file = ProjectUtility.uriToFile(fsa.getURI(filePath));
        PythonInterpreter.createStartScript(file, "start");
	}
}