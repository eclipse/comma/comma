/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.util.ArrayList
import java.util.List
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.project.ProjectUtility
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.project.project.SimulatorGenerationTask
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.comma.simulator.SimulatorGenerator
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider

class SimulatorGeneratorTask extends GeneratorTask {
	
	final protected SimulatorGenerationTask task
	final protected IScopeProvider scopeProvider
	final protected IFileSystemAccess2 fsa
	protected CommaFileSystemAccess libFileSystemAccess;
		
	new(SimulatorGenerationTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
        super(task, scopeProvider, outputLocator, fsa)
        this.task = task
        this.scopeProvider = scopeProvider
        this.fsa = fsa
	}
	
	override protected doGenerate()
    {
        var List<Parameters> parameters = new ArrayList<Parameters>();
        if (task.params !== null) {
            parameters = task.params.map[p | EcoreUtil2.getResource(task.eResource, p).contents.head as Parameters]
        }
        
        var path = "simulator/" + task.name + "/";
        var plantumlJar = ProjectUtility.uriToFile(fsa.getURI(path + "plantuml.jar"));        
        var String simulator;
        val target = task.target;
        if (target instanceof ComponentReference) {
            val environment = ProjectUtility::getComponentEnvironmentConfig(task, target.component, task.envComponents, parameters, scopeProvider)
            simulator = SimulatorGenerator.generateForComponent(target.component, environment, scopeProvider, plantumlJar.toString);
        } else {
            val parameter = !parameters.isEmpty ? parameters.get(0) : null;
            simulator = SimulatorGenerator.generateForInterface(target.interface, parameter, scopeProvider, plantumlJar.toString);
        }
        
        var filePath = path + "simulator.py"
        fsa.generateFile(filePath, simulator)
        ProjectUtility.copyPlantUMLJar(plantumlJar);

        var file = ProjectUtility.uriToFile(fsa.getURI(filePath));
        PythonInterpreter.createStartScript(file, "start");
    }
}