/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.monitoring.generator.GeneratorUtility
import org.eclipse.comma.monitoring.generator.TaskClassGenerator
import org.eclipse.comma.monitoring.generator.TaskGeneratorParams
import org.eclipse.comma.monitoring.lib.CComponentTypeDescriptor
import org.eclipse.comma.monitoring.lib.CPartDescriptor
import org.eclipse.comma.project.ProjectUtility
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.project.project.FilePath
import org.eclipse.comma.project.project.MonitoringTask
import org.eclipse.comma.traces.events.traceEvents.Connection
import org.eclipse.comma.traces.events.traceEvents.TraceEvents
import org.eclipse.comma.traces.events.utilities.EventsUtilities
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IFileSystemAccessExtension2
import org.eclipse.xtext.scoping.IScopeProvider

import static org.eclipse.comma.types.generator.CommaFileSystemAccess.*

import static extension org.eclipse.comma.types.utilities.CommaUtilities.*


class MonitoringGeneratorTask extends GeneratorTask{
    static final String TEMP_CREATE_FOLDER = "temp"
    
    final protected MonitoringTask task
    final protected CommaFileSystemAccess javaFileSystemAccess //root of the Java code: src-gen/java
    final protected CommaFileSystemAccess convertedTracesFSA //for generating .events files from the legacy .traces (src-gen/traces)
    List<URI> traces =  new ArrayList<URI>
    TaskGeneratorParams params = new TaskGeneratorParams()
    
    new(MonitoringTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
        super(task, scopeProvider, outputLocator, fsa)
        this.task = task
        this.javaFileSystemAccess = commaFSA(OutputLocator.JAVA_FOLDER)
        this.convertedTracesFSA = commaFSA(OutputLocator.TRACE_FOLDER)
    }

    override protected doGenerate() throws IllegalArgumentException {
        //functionality common for both interface and component monitoring
        //generate part of the folder hierarchy in the comma-gen folder for the task
        createStatisticsFolder 
        //create a list of traces
        fetchTraces
        params.taskName = task.name
        params.skipTimeConstraints = task.skipTimeConstraints || task.skipConstraints
        params.skipDataConstraints = task.skipDataConstraints || task.skipConstraints
        params.applyReordering = task.applyReordering
        if(task.source instanceof ComponentReference){
            //generate component monitor. Component is supplied by referring to an existing component
            generateComponentMonitor()
        }else{
            //generate interface monitor. The interface is either an existing interface or a learned one (from a task)
            generateInterfaceMonitor()
        }
    }

    def generateInterfaceMonitor(){
        val interface = task.source.interface
        params.taskKind = "interface"
        params.interface_ = interface
		
		if(ProjectUtility::runtimeTask(task)){
            params.monitorPort = task.port
            params.feedbackPort = task.feedbackPort
            params.traceFileNames.add(task.port+"")
            params.traceFilePaths.add(task.port+".runtime")
            new TaskClassGenerator(javaFileSystemAccess, params).generateTask   
            return
        }
		
        //iterate over the traces
        if(traces.empty){
            errors.add("Warning: no valid traces were located for monitoring task " + task.name)
        }

        for (traceUri : traces) {
            try {
                val traceFileName = traceUri.trimFileExtension.lastSegment
                val tracePath = getTracePath(traceUri)
                if(traceUri.fileExtension.equals("events")) {
                	val eventsModel = EventsUtilities::readHeader(traceUri)
                		if(eventsModel === null){
                    	throw createException("Monitoring Task: File did not contain the Events syntax " + traceUri + "\n", resource, MonitoringGeneratorTask)                		}
                }
                params.traceFileNames.add(traceFileName)
                params.traceFilePaths.add(tracePath)
            } catch (Exception exception) {
                errors.add("Trace could not be read: " + traceUri + ".  " + exception.message)
            }
        }
        new TaskClassGenerator(javaFileSystemAccess, params).generateTask
    }

    def generateComponentMonitor(){
        val component = (task.source as ComponentReference).component
        val visibleInterfaces = ComponentUtilities::getAllInterfaces(task, scopeProvider)
        params.taskKind = "component"
        params.component = component
        val allComponents = ComponentUtilities::getAllComponents(task, scopeProvider)
        for(c : allComponents) {
            val td = new CComponentTypeDescriptor();
            td.componentType = c.fullyQualifiedNameAsString
            td.singletonPorts = c.ports.filter[it instanceof ProvidedPort].filter[p | visibleInterfaces.exists[i | i.fullyQualifiedNameAsString.equals(p.interface.fullyQualifiedNameAsString) && i.singleton]].map[name].toSet
            if(c == component) {
                td.partDescriptors = GeneratorUtility.getPartDescriptors(c)
            }
            params.componentDescriptors.add(td);
        }
        val validTraces = generateComponentResultsStructure(component, params)
        if(validTraces.empty){
            errors.add("Warning: no valid traces were located for monitoring task " + task.name)
        }
        new TaskClassGenerator(javaFileSystemAccess, params).generateTask
    }


    def List<URI> generateComponentResultsStructure(Component component, TaskGeneratorParams params){
        val validTraces = new ArrayList<URI>
        val allComponents = ComponentUtilities::getAllComponents(task, scopeProvider)
        val List<CPartDescriptor> partDescriptors = GeneratorUtility.getPartDescriptors(component)
        if(ProjectUtility::runtimeTask(task)){
        	addTrace(resource.resolveUri(task.instances.path))
        }
        for(traceUri : traces){
            try{
            	var traceFileName = traceUri.trimFileExtension.lastSegment
            	var traceFile = getTracePath(traceUri)
            	if(ProjectUtility::runtimeTask(task)){
        			params.monitorPort = task.port
                    params.feedbackPort = task.feedbackPort
        			traceFile = task.port+".runtime"
            	}
                val eventsModel = traceUri.fileExtension.toString.equals("jsonl") ? EventsUtilities::readHeaderFromJson(traceUri) : EventsUtilities::readHeader(traceUri) 
                if(eventsModel === null){
                    throw createException("Monitoring Task: File did not contain the Events syntax " + traceUri + "\n", resource, MonitoringGeneratorTask)
                }
                val componentInstances = eventsModel.components.filter[it.componentType.equals(component.fullyQualifiedNameAsString)]
                if(componentInstances.empty){
                    throw createException("Warning: trace file " + traceUri + " does not contain component instances of type " + component.fullyQualifiedNameAsString + "\n", resource, MonitoringGeneratorTask)
                }
                for(componentInstance : componentInstances){
                    //for each functional constraint create a directory for the monitor results
                    if(component.functionalConstraintsBlock !== null){
                        for(fc : component.functionalConstraintsBlock.functionalConstraints){
                            createMonitorResultsFolder(componentFuncConstraintResultsFolder(traceFileName, componentInstance.componentId, fc.name))
                        }
                    }
                    //create a folder for the results of time/data constraints for the component instance
                    createMonitorResultsFolder(componentTimeDataResultsFolder(traceFileName, componentInstance.componentId))
                    //handle all parts of the component instance
                    for(part : partDescriptors) {
                        val fcBlock = allComponents.findFirst[name.equals(part.type)].functionalConstraintsBlock
                        if(fcBlock !== null){
                             for(fc : fcBlock.functionalConstraints) {
                                 createMonitorResultsFolder(componentFuncConstraintResultsFolder(traceFileName, componentInstance.componentId + part.partId, fc.name))
                             }
                        }
                        createMonitorResultsFolder(componentTimeDataResultsFolder(traceFileName, componentInstance.componentId + part.partId))
                    }
                }
                params.traceFileNames.add(traceFileName)
                params.traceFilePaths.add(traceFile)
                params.componentInstances.add(componentInstances.map[componentId].toList)
                val Map<String, Component> allInstances = new HashMap<String, Component>()
                for(c : eventsModel.components){
                    allInstances.put(c.componentId, allComponents.findFirst[name.equals(c.componentType)])
                }
                params.allInstances.add(allInstances)
                validTraces.add(traceUri)
            } catch (Exception exception) {
                errors.add(exception.message)
            }
        }
        return validTraces
    }

    def fetchTraces(){
        if(!task.traces.empty){ //file names or a task
            val fileSources = task.traces.filter(FilePath).filter(p | p.path !== null).toList
            val taskSources = task.traces.filter(ts | !(ts instanceof FilePath))
            val resources = ProjectUtility::getTraceResourcesFromFiles(fileSources)
            for(traceUri : resources.validURIs){
                addTrace(traceUri)
            }
            for(traceSource : taskSources){
                addTrace(traceSource.traceResourceURI)
            }
            //serialize errors and ignore erroneous resources
            for(invalid : resources.invalidTraceSources){
                errors.add("Invalid resource " + invalid.path + ". Will be ignored.")
            }
            for(wrongNamed : resources.wrongnamedTraceSources){
                errors.add("Trace file name must start with a letter and may contain letters, digits and _. Resource " + wrongNamed.path + " will be ignored.")
            }
            for(duplicate : resources.duplicateTraceSources){
                errors.add("Resource with name " + duplicate.path + " is duplicated. Will be ignored.")
            }
        }
        else{//list of directories
            val resources = ProjectUtility::getTraceResourcesFromDirs(task.tracedirs)
            for(trace : resources.validURIs){
                addTrace(trace)
            }
            //serialize errors and ignore erroneous resources
            for(dir : resources.nonexistentDirectories){
                errors.add("Directory " + dir.path + " does not exist.")
            }
            for(dir : resources.duplicateDirectories){
                errors.add("Directory with name " + dir.path + " is duplicated.")
            }
            if(!resources.duplicateTraceFiles.empty){
                errors.add("Trace directories contain files with duplicated names. Only the first occurrence will be used")
            }
            if(!resources.wrongnamedTraceFiles.empty){
                errors.add("Trace directories contain files with names that contain '-' or white space. Files will be ignored.")
            }
        }
    }
    
    /*
     * Functionality for creating the monitor results folder hierarchy in comma-gen
     */
    
    def createStatisticsFolder() {
        val statsFSA = getStatisticsFSA(fsa, task.name)
        statsFSA.generateFile(TEMP_CREATE_FOLDER, "")
        statsFSA.deleteFile(TEMP_CREATE_FOLDER)
    }
    
    def createMonitorResultsFolder(String folderPath) {
        val monitorFSA = getMonitorResultFSA(fsa, task.name, folderPath) //formed as comma-gen/<taskName>/<folderPath>
        monitorFSA.generateFile(TEMP_CREATE_FOLDER, "")
        monitorFSA.deleteFile(TEMP_CREATE_FOLDER)
    }
    
    /*
     * Helper methods
     */
     
    //The method checks if the trace is in the old format (.traces) and converts to events file
    def addTrace(URI traceUri){
        val fileExtension = traceUri.fileExtension
        if(fileExtension.equals("events") || fileExtension.equals("jsonl")){
            traces.add(traceUri);
        } else 
        if(fileExtension.equals("traces")) {
            errors.add(traceUri + " file format is not supported.")
        }else {
            throw createException("Monitoring Task: File did not contain the Traces syntax " + traceUri + "\n", resource, MonitoringGeneratorTask)
        }
    }
    
    def getTracePath(URI traceUri){
        val fs = commaFSA("../").IFileSystemAccess as IFileSystemAccessExtension2
        val playerUri = fs.getURI("../f.java")
        val uri = traceUri.deresolve(playerUri, true, true, true)
        uri.toFileString.replace("\\", "\\\\")
    }
    
    def connections(TraceEvents trace, Interface i){
        var connections = trace.connections.filter[interface.equals(i.fullyQualifiedNameAsString)]
        if(i.singleton){
            val endPoints = new HashMap<String, Connection>
            connections.forEach[endPoints.put(target + targetPort, it)]
            connections = endPoints.values
        }
        connections
    }
    
    def reportFilePrefix(Connection c, Interface i)
    '''«IF i.singleton»«c.target»_«c.targetPort»_«i.fullyQualifiedNameAsString»«ELSE»«c.target»_«c.source»_«i.fullyQualifiedNameAsString»«ENDIF»'''
    
    def interfaceOutLocalPath(String traceFileName, Connection c, Interface i){
        '''«traceFileName»/«reportFilePrefix(c, i)»/'''
    }
        
    //TODO consider moving the following two methods to the corresponding Java generators
    //Rationale: the paths are also used in the generated Java code so their calculation
    //should be specified in a single location
    
    /*
     * Folder structure of the component monitoring results
     * For a task with name T, folder comma-gen/T is used. In this folder for each
     * trace file F, component instance C in the trace, ports P1, P2, ..., functional
     * constraints Fc1, Fc2, ...results are structured as:
     * 
     * <T>/
     *  <C>/
     *      port_<P1>/
     *      port_<P2>/
     *      ...
     *      <Fc1>/
     *      <Fc2>/
     *      ...
     *      time_data/
     * 
     */
    
    def componentFuncConstraintResultsFolder(String traceFileName, String componentId, String fcName){
        traceFileName + "/" + componentId + "/" + fcName
    }
    
    def componentTimeDataResultsFolder(String traceFileName, String componentId){
        traceFileName + "/" + componentId + "/time_data"    
    }
    
    def portOutLocalPath(String traceFileName, String componentInstance, String portName){
        '''«traceFileName»/«componentInstance»/port_«portName»/'''
    }
}