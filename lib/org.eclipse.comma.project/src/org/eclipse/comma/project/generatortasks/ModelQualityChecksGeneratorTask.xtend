/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.util.List
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.modelqualitychecks.ModelQualityChecksGenerator
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.project.ProjectUtility
import org.eclipse.comma.project.project.ModelQualityChecksGenerationTask
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider

class ModelQualityChecksGeneratorTask extends GeneratorTask {

	final protected ModelQualityChecksGenerationTask task
	final protected IScopeProvider scopeProvider
	final protected IFileSystemAccess2 fsa
		
	new(ModelQualityChecksGenerationTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
		super(task, scopeProvider, outputLocator, fsa)
		this.task = task
		this.scopeProvider = scopeProvider
		this.fsa = fsa
	}
	
	override protected doGenerate()
	{
	    var Parameters params = null;
        if (task.params !== null) {
            params = EcoreUtil2.getResource(task.eResource, task.params).contents.head as Parameters
        }
        
        var path = "model_quality_checks/" + task.name + "/";
        
        var plantumlJar = ProjectUtility.uriToFile(fsa.getURI(path + "plantuml.jar"));        
        var filePath = path + "generator.py"
        val maxDepth = task.rgMaxDepth == 0 ? 1000 : task.rgMaxDepth
        var List<List<State>> homeStates = null;
        if (task.homeStateSets !== null) {
            homeStates = task.homeStateSets.map[set | set.states.map[s | s.hs]]
        }
        var generator = ModelQualityChecksGenerator.generate(task.target.interface, params, scopeProvider, maxDepth, homeStates, plantumlJar.toString)
        fsa.generateFile(filePath, generator)
        fsa.generateFile(path + "dashboard.html", ModelQualityChecksGenerator.dashboardHTML);
        ProjectUtility.copyPlantUMLJar(plantumlJar);

        var file = ProjectUtility.uriToFile(fsa.getURI(filePath));
        PythonInterpreter.createStartScript(file, "generate");
        PythonInterpreter.execute(file)
	}
}