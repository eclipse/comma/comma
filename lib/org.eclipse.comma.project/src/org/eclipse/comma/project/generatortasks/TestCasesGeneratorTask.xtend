/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import org.eclipse.comma.project.project.TestCasesGenerationTask
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.comma.testcases.TestCasesGenerator
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.comma.project.ProjectUtility

class TestCasesGeneratorTask extends GeneratorTask {
	
	final protected TestCasesGenerationTask task
	final protected IScopeProvider scopeProvider
	final protected IFileSystemAccess2 fsa
	protected CommaFileSystemAccess libFileSystemAccess;
	
	new(TestCasesGenerationTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
		super(task, scopeProvider, outputLocator, fsa)
		this.task = task
		this.scopeProvider = scopeProvider
		this.fsa = fsa
	}

	override protected doGenerate()
	{
	    var Parameters params = null;
        if (task.params !== null) {
            params = EcoreUtil2.getResource(task.eResource, task.params).contents.head as Parameters
        }
        
        var path = "testcases/" + task.name + "/";
        var projectFile = ProjectUtility.uriToFile(task.eResource.URI);

        var template = TestCasesGenerator.getTemplate(projectFile, task.template)
        if (template === null) throw new RuntimeException("Template missing");
        fsa.generateFile(path + template.fileName, template.content)
        
        var filePath = path + "generator.py"
        val maxDepth = task.maxDepth == 0 ? 1000 : task.maxDepth
        var generator = TestCasesGenerator.generate(task.target.interface, params, scopeProvider, maxDepth, template.fileName, task.outputFile, task.name);
        fsa.generateFile(filePath, generator)

        var file = ProjectUtility.uriToFile(fsa.getURI(filePath));            
        PythonInterpreter.createStartScript(file, "generate");
        PythonInterpreter.execute(file)
	}
}