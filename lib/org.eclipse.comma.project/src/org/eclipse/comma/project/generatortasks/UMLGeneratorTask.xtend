/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import org.eclipse.comma.behavior.generator.plantuml.BehaviorUmlGenerator
import org.eclipse.comma.project.project.UMLTask
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.comma.project.project.InterfaceReference
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.behavior.component.generator.plantuml.ComponentUmlGenerator

class UMLGeneratorTask extends GeneratorTask {	
	
	final UMLTask task  
	
	new(UMLTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
		super(task, scopeProvider, outputLocator, fsa)
		this.task = task
	}
	
	override doGenerate() {
	    if(task.source instanceof InterfaceReference) {
	        val umlGen = new BehaviorUmlGenerator(task.source.interface, task.fileSystemAccess)
            umlGen.setPrjURIForImages(resource.URI) 
            umlGen.doGenerate    
	    } else {
	        val umlGen = new ComponentUmlGenerator((task.source as ComponentReference).component, task.fileSystemAccess)
	        umlGen.setPrjURIForImages(resource.URI)
	        umlGen.generate
	    }
	}
}