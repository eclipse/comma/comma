/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.util.ArrayList
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.project.project.DocumentationGenerationTask
import org.eclipse.comma.project.project.FilePath
import org.eclipse.comma.project.project.InterfaceReference
import org.eclipse.comma.project.project.MonitoringTask
import org.eclipse.comma.project.project.Task
import org.eclipse.comma.project.project.TraceSource
import org.eclipse.core.runtime.IPath
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.generator.AbstractFileSystemAccess2
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.comma.project.project.CodeGenerationSource
import org.eclipse.comma.project.project.DocumentationSource
import org.eclipse.comma.project.project.UMLTask
import org.eclipse.comma.behavior.interfaces.generator.InterfaceFileSystemAccess
import org.eclipse.emf.common.util.URI

class OutputLocator {

	public final static String DEFAULT_OUTPUT_FOLDER = "src-gen/"

	public final static String COMMA_GEN_FOLDER = "../../../comma-gen/"

	public final static String DOC_FOLDER = "doc/"
	public final static String DOC_EXT = ".docx"

	public final static String JAVA_FOLDER = "java/"
	public static final String TRACE_FOLDER = "traces"

	public final static String MONITORING_FILENAME = "ScenarioPlayer.java"

	def getGeneratedFile(Task task) {
		task.outputFolder + task.fileName
	}

	/*********************** getOutputFolder ******************************/
	dispatch def getOutputFolder(DocumentationGenerationTask task) {
		DOC_FOLDER
	}

	dispatch def getOutputFolder(UMLTask task) {
		InterfaceFileSystemAccess.UML + task.name + IPath.SEPARATOR
	}

	/*********************** getFileName ******************************/
	dispatch def getFileName(DocumentationGenerationTask task) {
		task.name + DOC_EXT
	}

	dispatch def getFileName(MonitoringTask task) {
		// TEMP: enable again once Java monitoring is ready
		"Dummy"
	}

	/*********************** Trace Output ******************************/

	def getTraceResource(IFileSystemAccess access, TraceSource source) {
		EcoreUtil2.getResource(source.eResource, (source as FilePath).path)
	}

	def URI getTraceResourceURI(IFileSystemAccess fsa, TraceSource source){
		URI.createURI((source as FilePath).path).resolve(source.eResource.URI)
	}

	def getDefaultOutputFolder(IFileSystemAccess fsa) {
		if(fsa instanceof AbstractFileSystemAccess2) {
			fsa.outputConfigurations.get(IFileSystemAccess2.DEFAULT_OUTPUT).outputDirectory
		}
		return DEFAULT_OUTPUT_FOLDER
	}

	/*********************** Code Generation and Documentation Source ******************************/

	def dispatch getInterfaces(CodeGenerationSource source){
		new ArrayList<Interface>
	}

	def dispatch getInterfaces(DocumentationSource source){
		new ArrayList<Interface>
	}

	def dispatch getInterfaces(InterfaceReference source){
		var result = new ArrayList<Interface>
		result.add(source.interface)
		result
	}
}
