/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generatortasks

import java.io.File
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import org.eclipse.comma.behavior.generator.plantuml.BehaviorUmlGenerator
import org.eclipse.comma.behavior.interfaces.generator.InterfaceFileSystemAccess
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.comma.project.generatortasks.GeneratorTask
import org.eclipse.comma.project.generatortasks.OutputLocator
import org.eclipse.comma.project.project.DocumentationGenerationTask
import org.eclipse.comma.project.service.DocService
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.acceleo.query.runtime.IQueryEnvironment
import org.eclipse.acceleo.query.runtime.Query
import org.eclipse.acceleo.query.runtime.ServiceUtils
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.BasicMonitor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EPackage
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.scoping.IScopeProvider
import org.obeonetwork.m2doc.generator.DocumentGenerationException
import org.obeonetwork.m2doc.parser.DocumentParserException
import org.obeonetwork.m2doc.util.ClassProvider
import org.obeonetwork.m2doc.util.M2DocUtils
import org.eclipse.emf.ecore.resource.ResourceSet

class DocumentationGeneratorTask extends GeneratorTask {
	
	final static String ERROR = "Documentation Generation error(s): "

	final protected DocumentationGenerationTask task
	final protected CommaFileSystemAccess umlFileSystemAccess
	final protected CommaFileSystemAccess docFileSystemAccess
	final protected ResourceSet resourceSet
	
	//interfaces and signatures selected by the task source
	protected List<Interface> sourceInterfaces
	protected List<Signature> sourceSignatures = new ArrayList<Signature> 
	

	new(DocumentationGenerationTask task, IScopeProvider scopeProvider, OutputLocator outputLocator, IFileSystemAccess2 fsa) {
		super(task, scopeProvider, outputLocator, fsa)
		this.task = task
		this.umlFileSystemAccess = InterfaceFileSystemAccess::getUMLFileSystemAccess(task.name, fsa)
		this.docFileSystemAccess = new CommaFileSystemAccess("doc" + Path.SEPARATOR, fsa)
		this.resourceSet = this.task.eResource.resourceSet
	}
	
	def URI createResultFileURI() {
		URI.createURI('''«task.targetFile»''')
	}

	override doGenerate() {
		sourceInterfaces = outputLocator.getInterfaces(task.source)
		sourceInterfaces.forEach[sourceSignatures.add(signature)]
		
		try {
			// initialize locations
			val behaviors = sourceInterfaces					
			val docURI = createDocDir()
			val templateURI = createTemplateURI			
			
			// Create uml images
			for(behavior : behaviors) {//				
				val umlGen = new BehaviorUmlGenerator(behavior, umlFileSystemAccess)
				umlGen.setPrjURIForImages(resource.URI)
				umlGen.doGenerate
				
			}
			
			// parse template
			val classProvider = new ClassProvider(this.getClass().getClassLoader());
			val queryEnvironment = createQueryEnvironment(templateURI);
			val template = M2DocUtils.parse(resourceSet.URIConverter, templateURI, queryEnvironment, classProvider, new BasicMonitor)

			// set variables
			val variables = new HashMap<String, Object>();
			setVariables(variables)
			
			// generate
			val genResult = M2DocUtils.generate(template, queryEnvironment, variables, resourceSet, docURI, false, new BasicMonitor);
			if(!genResult.messages.empty) {
				errors.add(ERROR)
			}
			for (message : genResult.messages) {
				errors.add(message.message)
			}
		} catch (DocumentParserException exception) {
			errors.add(ERROR + exception.message + "\n" + exception.cause)
		} catch (DocumentGenerationException exception) {
			errors.add(ERROR + exception.message + "\n" + exception.cause)
		}
	}
	
	def protected setVariables(HashMap<String, Object> variables){
		val outputUri = getOutputURI(umlFileSystemAccess)
		
		variables.put("task", task)
		variables.put("myModel", task.eContainer.eContainer.eContainer)
		variables.put("globalTypes", StateMachineUtilities::getGlobalTypes(task, scopeProvider))
		variables.put("outputPath", outputUri.toFileString())
		variables.put("interfaces", sourceInterfaces)
		variables.put("signatures", sourceSignatures)
	}

	def private createQueryEnvironment(URI templateURI) {
		val queryEnvironment = Query.newEnvironmentWithDefaultServices(null)

		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/project/Project")
		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/signature/InterfaceSignature")
		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/types/Types")
		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/behavior/Behavior")
		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/behavior/interfaces/InterfaceDefinition")
		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/expressions/Expression")
		registerPackage(queryEnvironment, "https://www.eclipse.org/comma/actions/Actions")

		val options = new HashMap<String, String>();
		M2DocUtils.prepareEnvironmentServices(queryEnvironment, resourceSet, templateURI, options);
		registerProjectPackageAndService(queryEnvironment)
		return queryEnvironment
	}

	protected def registerProjectPackageAndService(IQueryEnvironment queryEnvironment) {
		val services = ServiceUtils.getServices(queryEnvironment, DocService);
		ServiceUtils.registerServices(queryEnvironment, services);
	}

	def protected registerPackage(IQueryEnvironment env, String packageUri) {
		val ePackage = EPackage.Registry.INSTANCE.getEPackage(packageUri);
		env.registerEPackage(ePackage);
	}

	def private getOutputURI(CommaFileSystemAccess commaFsa) {		
		var rURI = resource.getURI()
		if (rURI.isPlatform) {
			val IResource eclipseResource = ResourcesPlugin.workspace.root.findMember(
				rURI.toPlatformString(true))
			rURI =  URI.createFileURI(eclipseResource.rawLocation.toOSString);
		}
		
		var outputDir = commaFsa.getOutputConfiguration;
		if (outputDir === null) {
			throw new IllegalArgumentException("Output directory was not set.")
		}

		if (outputDir.isRelative) {
			outputDir = outputDir.resolve(rURI)
		}
		
		if (!outputDir.hasTrailingPathSeparator) {
			outputDir.appendSegment("")
		}
		return outputDir
	}

	def private createDocDir() {
		val docURI = getOutputURI(docFileSystemAccess)//URI.createURI("doc" + Path.SEPARATOR).resolve(outputDir)
		val docFile = new File(docURI.path)
		if (!docFile.exists) {
			if (!docFile.mkdirs) {
				throw new IllegalArgumentException("Could not create doc output directory: " + docFile.path)
			}
		}
		return createResultFileURI.resolve(docURI)
	}

	def private createTemplateURI() {
		val template = task.template
		val nameURI = if (template !== null) {
				URI.createURI(template)
			} else {
				URI.createURI("Template.docx")
			}
		nameURI.resolve(task.eResource.URI)
	}

}
