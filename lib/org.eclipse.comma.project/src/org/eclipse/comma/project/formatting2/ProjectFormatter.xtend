/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.formatting2

import com.google.inject.Inject
import org.eclipse.comma.project.project.CompoundInterface
import org.eclipse.comma.project.project.CompoundInterfaceBlock
import org.eclipse.comma.project.project.DocumentationGenerationBlock
import org.eclipse.comma.project.project.DocumentationGenerationTask
import org.eclipse.comma.project.project.InterfaceMappings
import org.eclipse.comma.project.project.InterfaceReference
import org.eclipse.comma.project.project.MonitoringBlock
import org.eclipse.comma.project.project.MonitoringTask
import org.eclipse.comma.project.project.Policy
import org.eclipse.comma.project.project.Project
import org.eclipse.comma.project.project.ProjectDescription
import org.eclipse.comma.project.project.ResetCommand
import org.eclipse.comma.project.project.TypeMapping
import org.eclipse.comma.project.project.TypeMappings
import org.eclipse.comma.project.project.TypeMappingsBlock
import org.eclipse.comma.project.project.UMLBlock
import org.eclipse.comma.project.project.UMLTask
import org.eclipse.comma.project.services.ProjectGrammarAccess
import org.eclipse.comma.types.types.Import
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.AbstractRule
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.eclipse.xtext.formatting2.IHiddenRegionFormatter
import org.eclipse.xtext.formatting2.regionaccess.IComment
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegionsFinder
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1
import org.eclipse.comma.project.project.ReachabilityGraphGenerationTask
import org.eclipse.comma.project.project.TestCasesGenerationBlock
import org.eclipse.comma.project.project.TestCasesGenerationTask
import org.eclipse.comma.project.project.SimulatorGenerationBlock
import org.eclipse.comma.project.project.SimulatorGenerationTask
import org.eclipse.comma.project.project.ModelQualityChecksGenerationBlock
import org.eclipse.comma.project.project.ModelQualityChecksGenerationTask
import org.eclipse.comma.project.project.TestApplicationGenerationTask
import org.eclipse.comma.project.project.TestApplicationGenerationBlock
import org.eclipse.comma.project.project.ReachabilityGraphGenerationBlock

class ProjectFormatter extends AbstractFormatter2 {

	@Inject extension ProjectGrammarAccess

	// --------------------------------- Formatting Procedures
	static public final Procedure1<IHiddenRegionFormatter> oneSpaceWrap = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.oneSpace
			p.autowrap
		}
	};

	static public final Procedure1<IHiddenRegionFormatter> oneSpace = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.oneSpace
		}
	};

	static public final Procedure1<IHiddenRegionFormatter> noSpace = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.noSpace
		}
	};

	static public final Procedure1<IHiddenRegionFormatter> newLine = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.newLine
		}
	};

	static public final Procedure1<IHiddenRegionFormatter> emptyLine = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.newLines = 2
		}
	};

	static public final Procedure1<IHiddenRegionFormatter> indent = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.indent
		}
	};

	static public final Procedure1<IHiddenRegionFormatter> doubleIndent = new Procedure1<IHiddenRegionFormatter>() {
		override apply(IHiddenRegionFormatter p) {
			p.indent
			p.indent
		}
	};

	// Allow extra line before multinecomments
	override createCommentReplacer(IComment comment) {
		val grammarElement = comment.getGrammarElement();
		if (grammarElement instanceof AbstractRule) {
			val ruleName = grammarElement.name
			if (ruleName.startsWith("ML"))
				return new MultiLineFormatter(comment, '*');

		}
		super.createCommentReplacer(comment)
	}
	
	def prependBlockElements(EList<? extends EObject> blockelements, extension IFormattableDocument document) {
		for (i : 0 ..< blockelements.size) {
			val element = blockelements.get(i)
			if(i == 0) {
				element.prepend(newLine)
			} else {
				element.prepend(emptyLine)
			}
		}
	}

	// --------------------------------- Formatting 	
	
	def dispatch void format(ProjectDescription projectDescription, extension IFormattableDocument document) {		
		projectDescription.getImports.format;
		projectDescription.getProject.format;
	}

	def dispatch void format(Import _import, extension IFormattableDocument document) {
		if (_import.regionForEObject.previousSemanticRegion === null) {
			if (!this.textRegionExtensions.regionForEObject(_import).previousHiddenRegion.containsComment) {
				_import.prepend(noSpace)
			}
		} else {
			_import.prepend(newLine)
		}
		_import.regionFor.keyword(fileImportAccess.importKeyword_0).append(oneSpace)
	}

	def dispatch void format(Project project, extension IFormattableDocument document) {
		val rFinder = project.regionFor
		project.prepend(emptyLine)
		
		rFinder.keyword(projectAccess.projectKeyword_0).append(oneSpace)
		project.generatorBlocks.forEach[format]		
		formatSimpleBrackets(rFinder, projectAccess.leftCurlyBracketKeyword_2, projectAccess.rightCurlyBracketKeyword_4,
			document)		
	}
	
	// ------------------------------------ Blocks

	def dispatch void format(CompoundInterfaceBlock block, extension IFormattableDocument document) {
		val rFinder = block.regionFor
		block.prepend(emptyLine)

		rFinder.keyword(compoundInterfaceBlockAccess.compoundKeyword_1).prepend(oneSpace)
		rFinder.keyword(compoundInterfaceBlockAccess.interfacesKeyword_2).prepend(oneSpace)
		formatSimpleBrackets(rFinder, compoundInterfaceBlockAccess.leftCurlyBracketKeyword_3,
			compoundInterfaceBlockAccess.rightCurlyBracketKeyword_5, document)
		block.tasks.prependBlockElements(document)
		block.tasks.forEach[format]
	}
    
    def dispatch void format(TestApplicationGenerationBlock block, extension IFormattableDocument document) {
        val rFinder = block.regionFor
        block.prepend(emptyLine)

        rFinder.keyword(testApplicationGenerationBlockAccess.testApplicationKeyword_2).prepend(oneSpace)
        formatSimpleBrackets(rFinder, testApplicationGenerationBlockAccess.leftCurlyBracketKeyword_3, 
            testApplicationGenerationBlockAccess.rightCurlyBracketKeyword_5,
            document)
        block.tasks.prependBlockElements(document)
        block.tasks.forEach[format]
    }
	
	def dispatch void format(TestApplicationGenerationTask task, extension IFormattableDocument document) {
        val rFinder = task.regionFor
     
        rFinder.keyword(testApplicationGenerationTaskAccess.forKeyword_1).surround(oneSpaceWrap)
        
        formatSimpleBrackets(rFinder, 
            testApplicationGenerationTaskAccess.leftCurlyBracketKeyword_3,
            testApplicationGenerationTaskAccess.rightCurlyBracketKeyword_8, 
            document)
        
        rFinder.keyword(testApplicationGenerationTaskAccess.paramsKeyword_4_0)?.prepend(newLine).append(oneSpace)
    }
    
    def dispatch void format(ReachabilityGraphGenerationBlock block, extension IFormattableDocument document) {
        val rFinder = block.regionFor
        block.prepend(emptyLine)

        rFinder.keyword(reachabilityGraphGenerationBlockAccess.reachabilityGraphKeyword_2).prepend(oneSpace)
        formatSimpleBrackets(rFinder, reachabilityGraphGenerationBlockAccess.leftCurlyBracketKeyword_3, 
            reachabilityGraphGenerationBlockAccess.rightCurlyBracketKeyword_5,
            document)
        block.tasks.prependBlockElements(document)
        block.tasks.forEach[format]
    }
	
	def dispatch void format(ReachabilityGraphGenerationTask task, extension IFormattableDocument document) {
        val rFinder = task.regionFor
     
        rFinder.keyword(reachabilityGraphGenerationTaskAccess.forKeyword_1).surround(oneSpaceWrap)
        
        formatSimpleBrackets(rFinder, 
            reachabilityGraphGenerationTaskAccess.leftCurlyBracketKeyword_3,
            reachabilityGraphGenerationTaskAccess.rightCurlyBracketKeyword_5, 
            document)
        
        rFinder.keyword(reachabilityGraphGenerationTaskAccess.maxDepthKeyword_4_0_0)?.prepend(newLine).append(oneSpace)
        
        rFinder.keyword(reachabilityGraphGenerationTaskAccess.paramsKeyword_4_1_0)?.prepend(newLine).append(oneSpace)
    }

	def dispatch void format(CompoundInterface compound, extension IFormattableDocument document) {
		val rFinder = compound.regionFor

		rFinder.keyword(compoundInterfaceAccess.versionKeyword_2).prepend(newLine)
		rFinder.assignment(compoundInterfaceAccess.versionAssignment_3).prepend(newLine)

		rFinder.keyword(compoundInterfaceAccess.descriptionKeyword_4).prepend(emptyLine)
		rFinder.assignment(compoundInterfaceAccess.descriptionAssignment_5).prepend(newLine)

		rFinder.keyword(compoundInterfaceAccess.interfacesKeyword_6).prepend(emptyLine)

		formatSimpleBrackets(rFinder, compoundInterfaceAccess.leftCurlyBracketKeyword_1,
			compoundInterfaceAccess.rightCurlyBracketKeyword_8, document)

		compound.interfaces.forEach[it|it.prepend(newLine).format]
	}
	
	def dispatch void format(TestCasesGenerationBlock block, extension IFormattableDocument document) {
        val rFinder = block.regionFor
        block.prepend(emptyLine)
        
        rFinder.keyword(testCasesGenerationBlockAccess.testCasesKeyword_2).prepend(oneSpace)        
        formatSimpleBrackets(rFinder, 
            testCasesGenerationBlockAccess.leftCurlyBracketKeyword_3, 
            testCasesGenerationBlockAccess.rightCurlyBracketKeyword_5,
            document)
        block.tasks.prependBlockElements(document)
        block.tasks.forEach[format]
    }
    
    def dispatch void format(TestCasesGenerationTask task, extension IFormattableDocument document) {
        val rFinder = task.regionFor
     
        rFinder.keyword(testCasesGenerationTaskAccess.forKeyword_1).surround(oneSpaceWrap)
        
        formatSimpleBrackets(rFinder, 
            testCasesGenerationTaskAccess.leftCurlyBracketKeyword_3,
            testCasesGenerationTaskAccess.rightCurlyBracketKeyword_5, 
            document)
  
        rFinder.keyword(testCasesGenerationTaskAccess.templateKeyword_4_0_0)?.prepend(newLine).append(oneSpace)
        rFinder.keyword(testCasesGenerationTaskAccess.outputFileKeyword_4_1_0)?.prepend(newLine).append(oneSpace)
        rFinder.keyword(testCasesGenerationTaskAccess.maxDepthKeyword_4_2_0)?.prepend(newLine).append(oneSpace)
        rFinder.keyword(testCasesGenerationTaskAccess.paramsKeyword_4_3_0)?.prepend(newLine).append(oneSpace)
    }
	
	def dispatch void format(MonitoringBlock block, extension IFormattableDocument document) {
		val rFinder = block.regionFor
		block.prepend(emptyLine)
		
		rFinder.keyword(monitoringBlockAccess.monitorsKeyword_2).prepend(oneSpace)
		formatSimpleBrackets(rFinder, monitoringBlockAccess.leftCurlyBracketKeyword_3, monitoringBlockAccess.rightCurlyBracketKeyword_5,
			document)
		block.tasks.prependBlockElements(document)
		block.tasks.forEach[format]
	}
	
	def dispatch void format(DocumentationGenerationBlock block, extension IFormattableDocument document) {
		val rFinder = block.regionFor
		block.prepend(emptyLine)
		
		rFinder.keyword(documentationGenerationBlockAccess.documentationsKeyword_2).prepend(oneSpace)
		formatSimpleBrackets(rFinder, documentationGenerationBlockAccess.leftCurlyBracketKeyword_3, documentationGenerationBlockAccess.rightCurlyBracketKeyword_5,
			document)
		block.tasks.prependBlockElements(document)
		block.tasks.forEach[format]
	}
	
	def dispatch void format(UMLBlock block, extension IFormattableDocument document) {
		val rFinder = block.regionFor
		block.prepend(emptyLine)
		
		rFinder.keyword(UMLBlockAccess.UMLKeyword_2).prepend(oneSpace)		
		formatSimpleBrackets(rFinder, UMLBlockAccess.leftCurlyBracketKeyword_3, UMLBlockAccess.rightCurlyBracketKeyword_5,
			document)
		block.tasks.prependBlockElements(document)
		block.tasks.forEach[format]
	}
		
	def dispatch format(InterfaceReference iRef, extension IFormattableDocument document) {
		iRef.regionFor.keyword(interfaceReferenceAccess.interfaceKeyword_0).surround(oneSpace)
	}

	def dispatch void format(TypeMappingsBlock block, extension IFormattableDocument document) {
		val rFinder = block.regionFor
		block.prepend(emptyLine)

		rFinder.keyword(typeMappingsBlockAccess.typeKeyword_1).prepend(oneSpace)
		rFinder.keyword(typeMappingsBlockAccess.mappingsKeyword_2).prepend(oneSpace)
		formatSimpleBrackets(rFinder, typeMappingsBlockAccess.leftCurlyBracketKeyword_3,
			typeMappingsBlockAccess.rightCurlyBracketKeyword_5, document)
		block.tasks.prependBlockElements(document)
		block.tasks.forEach[format]
	}

	def dispatch void format(SimulatorGenerationBlock block, extension IFormattableDocument document) {
        val rFinder = block.regionFor
        block.prepend(emptyLine)
        
        formatSimpleBrackets(rFinder, 
            simulatorGenerationBlockAccess.leftCurlyBracketKeyword_3, 
            simulatorGenerationBlockAccess.rightCurlyBracketKeyword_5,
            document)
        block.tasks.prependBlockElements(document)
        block.tasks.forEach[format]
    }
    
    def dispatch void format(ModelQualityChecksGenerationBlock block, extension IFormattableDocument document) {
		val rFinder = block.regionFor
		block.prepend(emptyLine)

		rFinder.keyword(modelQualityChecksGenerationBlockAccess.modelQualityChecksKeyword_2).prepend(oneSpace)
		formatSimpleBrackets(rFinder, modelQualityChecksGenerationBlockAccess.leftCurlyBracketKeyword_3,
			modelQualityChecksGenerationBlockAccess.rightCurlyBracketKeyword_5, document)
		block.tasks.prependBlockElements(document)
		block.tasks.forEach[format]
	}

	def dispatch void format(ModelQualityChecksGenerationTask task, extension IFormattableDocument document) {
		val rFinder = task.regionFor

		rFinder.keyword(modelQualityChecksGenerationTaskAccess.forKeyword_1).surround(oneSpaceWrap)

		formatSimpleBrackets(rFinder, modelQualityChecksGenerationTaskAccess.leftCurlyBracketKeyword_3,
			modelQualityChecksGenerationTaskAccess.rightCurlyBracketKeyword_5, document)

		rFinder.keyword(modelQualityChecksGenerationTaskAccess.paramsKeyword_4_0_0)?.prepend(newLine).append(oneSpace)
		rFinder.keyword(modelQualityChecksGenerationTaskAccess.maxDepthKeyword_4_2_0)?.prepend(newLine).append(oneSpace)
		rFinder.keyword(modelQualityChecksGenerationTaskAccess.homeStatesKeyword_4_1_0)?.prepend(newLine).append(oneSpace)
	}

	// ------------------------------------ Tasks

	def dispatch void format(DocumentationGenerationTask task, extension IFormattableDocument document) {
		val rFinder = task.regionFor
		
		task.source.format
		rFinder.keyword(documentationGenerationTaskAccess.forKeyword_1).surround(oneSpaceWrap)
		
		formatSimpleBrackets(rFinder, documentationGenerationTaskAccess.leftCurlyBracketKeyword_3,
			documentationGenerationTaskAccess.rightCurlyBracketKeyword_16, document)	
		
		rFinder.keyword(documentationGenerationTaskAccess.templateKeyword_4).prepend(newLine)
		rFinder.keyword(documentationGenerationTaskAccess.equalsSignKeyword_5).surround(oneSpace)
		
		rFinder.keyword(documentationGenerationTaskAccess.targetFileKeyword_7).prepend(newLine)
		rFinder.keyword(documentationGenerationTaskAccess.equalsSignKeyword_8).surround(oneSpace)		
		
		rFinder.keyword(documentationGenerationTaskAccess.authorKeyword_10).prepend(newLine)
		rFinder.keyword(documentationGenerationTaskAccess.equalsSignKeyword_11).surround(oneSpace)		
		
		rFinder.keyword(documentationGenerationTaskAccess.roleKeyword_13).prepend(newLine)
		rFinder.keyword(documentationGenerationTaskAccess.equalsSignKeyword_14).surround(oneSpace)		
	}
	
	def dispatch void format(SimulatorGenerationTask task, extension IFormattableDocument document) {
        val rFinder = task.regionFor
     
        rFinder.keyword(simulatorGenerationTaskAccess.forKeyword_1).surround(oneSpaceWrap)
        
        formatSimpleBrackets(rFinder, 
            simulatorGenerationTaskAccess.leftCurlyBracketKeyword_3,
            simulatorGenerationTaskAccess.rightCurlyBracketKeyword_6, 
            document)
            
        rFinder.keyword(simulatorGenerationTaskAccess.paramsKeyword_4_0)?.prepend(newLine).append(oneSpace)
    }

	def dispatch void format(MonitoringTask task, extension IFormattableDocument document) {
		val rFinder = task.regionFor

		task.source.format
		rFinder.keyword(monitoringTaskAccess.forKeyword_1).surround(oneSpaceWrap)
		
		formatSimpleBrackets(rFinder, monitoringTaskAccess.leftCurlyBracketKeyword_3,
			monitoringTaskAccess.rightCurlyBracketKeyword_8, document)

		rFinder.keyword(monitoringTaskAccess.traceKeyword_6)?.prepend(newLine).append(oneSpace)
		if(task.traces.size == 1) {
			task.traces.get(0).prepend(oneSpace)
		} else {
			for(trace : task.traces) {
				trace.prepend(newLine)
			}
		}

		if(task.tracedirs.size == 1) {
			task.tracedirs.get(0).prepend(oneSpace)
		} else {
			for(trace : task.tracedirs) {
				trace.prepend(newLine)
			}
		}
	}

	def dispatch void format(Policy policy, extension IFormattableDocument document) {
		val rFinder = policy.regionFor
		rFinder.keyword(policyAccess.resetKeyword_1_1)?.prepend(oneSpace)
		rFinder.keyword(policyAccess.filterKeyword_1_2)?.prepend(oneSpace)		
		
		policy.resetCommands.forEach[format] 
	}
	
	def dispatch void format(ResetCommand reset, extension IFormattableDocument document) {
		val rFinder = reset.regionFor
		rFinder.keyword(resetCommandAccess.element1Keyword_0)?.surround(oneSpace)
		rFinder.keyword(resetCommandAccess.element2Keyword_2)?.surround(oneSpace)		
	}

	def void formatSimpleBrackets(ISemanticRegionsFinder rFinder, Keyword open, Keyword close,
		extension IFormattableDocument document) {
		val pairs = rFinder.keywordPairs(open, close)
		if (!pairs.empty) {
			val brackets = pairs.get(0)
			brackets.key.prepend(oneSpace)
			brackets.value.prepend(newLine)
			brackets.interior(indent)
		}
	}
	
	def dispatch void format(UMLTask task, extension IFormattableDocument document) {
		val rFinder = task.regionFor

		task.source.format
		rFinder.keyword(UMLTaskAccess.forKeyword_1).surround(oneSpaceWrap)		
	}
	
	// ------------------------------------ Mappings	
	def dispatch void format(TypeMappings typeMappings, extension IFormattableDocument document) {
		val rFinder = typeMappings.regionFor
				
		formatSimpleBrackets(rFinder, typeMappingsAccess.leftCurlyBracketKeyword_1,
			typeMappingsAccess.rightCurlyBracketKeyword_4, document)

		for (TypeMapping mapping : typeMappings.commonMappings) {
			mapping.prepend(newLine)
			mapping.format;
		}

		for (InterfaceMappings mapping : typeMappings.interfaceMappings) {
			mapping.prepend(emptyLine)
			mapping.format;
		}
	}

	def dispatch void format(InterfaceMappings typeMappings, extension IFormattableDocument document) {
		val rFinder = typeMappings.regionFor
		typeMappings.prepend(emptyLine)

		rFinder.keyword(interfaceMappingsAccess.interfaceKeyword_0).append(oneSpace)
		formatSimpleBrackets(rFinder, interfaceMappingsAccess.leftCurlyBracketKeyword_2,
			interfaceMappingsAccess.rightCurlyBracketKeyword_4, document)

		for (TypeMapping mapping : typeMappings.mappings) {
			mapping.prepend(newLine)
			mapping.format;
		}
	}

	def dispatch void format(TypeMapping mapping, extension IFormattableDocument document) {
		mapping.regionFor.keyword(typeMappingAccess.hyphenMinusGreaterThanSignKeyword_1).surround(oneSpace)
	}
}
