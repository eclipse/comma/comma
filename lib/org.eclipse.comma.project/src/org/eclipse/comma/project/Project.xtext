/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
grammar org.eclipse.comma.project.Project with org.eclipse.comma.types.Types

generate project "https://www.eclipse.org/comma/project/Project"
 
import "https://www.eclipse.org/comma/behavior/interfaces/InterfaceDefinition" as interface
import "https://www.eclipse.org/comma/behavior/component/Component" as component
import "https://www.eclipse.org/comma/signature/InterfaceSignature" as signature
import "https://www.eclipse.org/comma/behavior/Behavior" as behavior
import "https://www.eclipse.org/comma/types/Types" as types

ProjectDescription returns types::ModelContainer: {ProjectDescription}
	imports += Import*
	project = Project
;

CompoundInterfaceBlock: {CompoundInterfaceBlock}
	'Compound' 'Interfaces' '{'
		tasks += CompoundInterface*
	'}'
;

CompoundInterface:
	name = ID '{'
		'version'
		version = STRING
		'description' 
		description = STRING
		
		'interfaces'
		interfaces += InterfaceModel+
		
	'}'
;

//Constraints:
// - no duplications in used interfaces
InterfaceModel:
	interfaceModel = [interface::Interface | QN]
;

@Override
ModelContainer returns types::ModelContainer:
	super::ModelContainer | ProjectDescription
;

//Each kind of generator (e.g. Doc, C++ etc.) can appear exactly once
//Shall we allow type mappings and compound interfaces to appear multiple times ?

Project:
	'Project' name = ID '{'		
		generatorBlocks += GeneratorBlock* 
	'}'
;

TraceSource:
	FilePath
;

FilePath:
	path = STRING
;

GeneratorBlock:
	MonitoringBlock | 
	DocumentationGenerationBlock |
	UMLBlock |
	TypeMappingsBlock | 
	CompoundInterfaceBlock |
	ReachabilityGraphGenerationBlock |
	TestApplicationGenerationBlock |
	//TestCasesGenerationBlock |
	SimulatorGenerationBlock | 
	ModelQualityChecksGenerationBlock
;

fragment Task:
	name = ID
;

DocumentationSource:
	InterfaceReference
;

CodeGenerationSource:
	InterfaceReference
;

//Let's capture now the possibility to execute Components. We can restrict this in a validation
//and later allow it
ExecutableSource:
	InterfaceReference | ComponentReference
;

InterfaceReference:
	'interface' interface = [interface::Interface | QN]
;

ComponentReference:
	'component' component = [component::Component | QN]
;

//Constraints:
// - if the imported file contains transitively user defined simple types mappings should be present 

DocumentationGenerationBlock: {DocumentationGenerationBlock}
	'Generate' 'Documentations' '{'
		tasks += DocumentationGenerationTask*
	'}'
;

DocumentationGenerationTask:
	Task 'for' source = DocumentationSource '{'
		'template' '=' template = STRING
		'targetFile' '=' targetFile = STRING
		'author' '=' author = STRING
		'role' '=' role = STRING
	'}'
;

MonitoringBlock: {MonitoringBlock}
	'Generate' 'Monitors' '{'	
		tasks += MonitoringTask*
	'}'
;

ReachabilityGraphGenerationBlock: {ReachabilityGraphGenerationBlock}
    'Generate' 'ReachabilityGraph' '{'
        tasks += ReachabilityGraphGenerationTask*
    '}'
;

ReachabilityGraphGenerationTask:
    Task 'for' target = InterfaceReference '{'
    	(
	     	('max-depth:' maxDepth = Int)? &
	     	('params:' params = STRING)? 
	     )
    '}'
;

TestApplicationGenerationBlock: {TestApplicationGenerationBlock}
    'Generate' 'TestApplication' '{'
        tasks += TestApplicationGenerationTask*
    '}'
;

TestApplicationGenerationTask:
    Task 'for' target = (InterfaceReference | ComponentReference) '{'
        (('params:' params += STRING*)?)
        ('adapter:' adapterPath = STRING)?
        ('timeouts' '{' 'stop' 'when' 'no' 'events' 'observed:' noEventsTimeout = Double 'sec' 'default:' defaultTimeout = Double 'sec' '}')?
        envComponents += EnvironmentComponent*
    '}';

EnvironmentComponent:
    ((role = 'server') | (role = 'client')) id = ID 'ports' portSpecs += PortSpec+
;

PortSpec:
    port = [behavior::Port] ('default_params' | paramFile = STRING)
;

TestCasesGenerationBlock: {TestCasesGenerationBlock}
    'Generate' 'TestCases' '{'
		tasks += TestCasesGenerationTask*
	'}'
;

TestCasesGenerationTask:
	Task 'for' target=InterfaceReference '{'
        (
            ('template:' template=(STRING | 'java')) &
            ('output-file:' outputFile=STRING) &
            ('max-depth:' maxDepth = Int)? &
            ('params:' params = STRING)?
        )
	'}'
;

ModelQualityChecksGenerationBlock: {ModelQualityChecksGenerationBlock}
    'Generate' 'ModelQualityChecks' '{'
        tasks += ModelQualityChecksGenerationTask*
    '}'
;

HomeState:
	hs = [behavior::State]
;

HomeStateSet:
    '{' states += HomeState+ '}'
;

ModelQualityChecksGenerationTask:
    Task 'for' target = InterfaceReference '{'
        (
            ('params:' params = STRING)? &
            ('home-states:' homeStateSets += HomeStateSet+)? &
            ('max-depth:' rgMaxDepth = Int)?
         )
    '}'
;


SimulatorGenerationBlock: {SimulatorGenerationBlock}
    'Generate' 'Simulator' '{'
		tasks += SimulatorGenerationTask*
	'}'
;

SimulatorGenerationTask:
	Task 'for' target = (InterfaceReference | ComponentReference) '{'
    	(
	     	('params:' params += STRING*)?
	     )
	     envComponents += EnvironmentComponent*
    '}'
;

//Constraints:
//- trace files are .traces models
MonitoringTask:		
	Task 'for' source = ExecutableSource '{'
	    (applyReordering ?= 'reorder')?
		(('skip' skipTimeConstraints ?= 'time' 'constraints') |
		('skip' skipDataConstraints ?= 'data' 'constraints') |
		('skip' skipConstraints ?= 'constraints'))?
		'trace' ('files' traces += TraceSource+ | 'directories' tracedirs += FilePath+ | 'port' port = Int 'feedback' 'port' feedbackPort = Int ('instances' instances = FilePath)?)
	'}'
;

IncludedNI:
	ni = [signature::Notification]
;

IncludedCommand:
	co = [signature::Command]
;

IncludedSignal:
	si = [signature::Signal]
;

ExcludedNI:
	ni = [signature::Notification]
;

ExcludedCommand:
	co = [signature::Command]
;

ExcludedSignal:
	si = [signature::Signal]
;

UMLBlock: {UMLBlock}
	'Generate' 'UML' '{'
		tasks += UMLTask*
	'}'
;

UMLTask: 
	Task 'for' source = (InterfaceReference | ComponentReference)
;

ResetCommand: 
	'element1' element1 = STRING 'element2' element2 = STRING 
;
Policy: 
	split = 'NO_SPLIT' | 
	('SPLIT' 'reset' 'filter' resetCommands += ResetCommand+ )
;

//Constraints:
// - every global simple type is in the common mappings, defined once
// - type mappings have unique names 
// - every interface has only one mapping 

TypeMappingsBlock: {TypeMappingsBlock}
	'Type' 'Mappings' '{'
		tasks += TypeMappings*
	'}'
;

TypeMappings: 
	 name = ID '{'
		commonMappings += TypeMapping*
		interfaceMappings += InterfaceMappings*
	'}'
;

//Constraints:
// - there exist mappings for an interface if it defines simple types 
// - every defined simple type is mapped once 

InterfaceMappings :
	'interface' interface = [signature::Signature | QN]
	 '{' mappings += TypeMapping+ '}'
;

TypeMapping:
	type = [types::SimpleTypeDecl | QN] '->' targetType = STRING typedef?= 'typedef'? ('bytes:' bytes = Int)?
;
