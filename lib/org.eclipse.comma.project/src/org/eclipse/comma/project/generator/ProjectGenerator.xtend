/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.generator

import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import java.util.Set
import java.util.function.Consumer
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.scoping.InterfaceUtilities
import org.eclipse.comma.behavior.utilities.StateMachineUtilities
import org.eclipse.comma.monitoring.generator.ComponentTimeDataConstraintsGenerator
import org.eclipse.comma.monitoring.generator.FunctionalConstraintGenerator
import org.eclipse.comma.monitoring.generator.InfrastructureGenerator
import org.eclipse.comma.monitoring.generator.InterfaceDecisionGenerator
import org.eclipse.comma.project.generatortasks.DocumentationGeneratorTask
import org.eclipse.comma.project.generatortasks.ModelQualityChecksGeneratorTask
import org.eclipse.comma.project.generatortasks.MonitoringGeneratorTask
import org.eclipse.comma.project.generatortasks.OutputLocator
import org.eclipse.comma.project.generatortasks.SimulatorGeneratorTask
import org.eclipse.comma.project.generatortasks.TestCasesGeneratorTask
import org.eclipse.comma.project.generatortasks.TestApplicationGeneratorTask
import org.eclipse.comma.project.generatortasks.UMLGeneratorTask
import org.eclipse.comma.project.project.DocumentationGenerationTask
import org.eclipse.comma.project.project.ModelQualityChecksGenerationTask
import org.eclipse.comma.project.project.MonitoringTask
import org.eclipse.comma.project.project.Project
import org.eclipse.comma.project.project.ProjectDescription
import org.eclipse.comma.project.project.ProjectPackage
import org.eclipse.comma.project.project.SimulatorGenerationTask
import org.eclipse.comma.project.project.Task
import org.eclipse.comma.project.project.TestCasesGenerationTask
import org.eclipse.comma.project.project.TestApplicationGenerationTask
import org.eclipse.comma.project.project.UMLTask
import org.eclipse.comma.types.generator.CmdLineContext
import org.eclipse.comma.types.generator.CommaFileSystemAccess
import org.eclipse.comma.types.utilities.CommaUtilities
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.scoping.IScopeProvider

import static extension org.eclipse.comma.project.ProjectUtility.*

/**
 * Generates code from your model files on save.
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class ProjectGenerator extends AbstractGenerator {

	@Inject protected IScopeProvider scopeProvider
	
	protected OutputLocator outputLocator
	protected Project project
	protected Consumer<Task> processTaskConsumer
	protected List<String> errors = new ArrayList<String>()
	protected CmdLineContext cmdLineContext = null

    /**
     * This method is called by the command line tool.
     * Generation triggered by the IDE menus uses methods {@link #generateWithErrorReport}
     */
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		if(context instanceof CmdLineContext) {
		    cmdLineContext = context
		    project = (resource.allContents.head as ProjectDescription).project
		    val allTasks = taskNames.toSet
		    if(context.requestedTasks !== null) {
		        val dependencies = project.getTaskDependencyMap
		        val tasksToExecute = context.requestedTasks.requiredTasksClosure(dependencies)
		        generateWithErrorReport(tasksToExecute, fsa, context)
		        return
		    }
		    if(context.excludedTasks !== null) {
		        val dependencies = project.getTaskDependencyMap
		        val requiredTasks = context.excludedTasks.findReferredTasks(dependencies) 
		        if(!requiredTasks.empty){
		            context.executionResult = false
		            context.errorString = "Tasks " + requiredTasks.join(", ") + " cannot be skipped."
		            return
		        }else{
		            allTasks.removeAll(context.excludedTasks)
		            generateWithErrorReport(allTasks, fsa, context)
		            return
		        }
		    }
			generateWithErrorReport(allTasks, fsa, context)
		} else {
			//auto build disabled while editing prj files in UI
		}
	}

	def List<String> generateWithErrorReport(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
        project = (resource.allContents.head as ProjectDescription).project
        generateWithErrorReport(taskNames.toSet, fsa, context)
    }

	def List<String> generateWithErrorReport(Resource resource, Set<String> selectedTasks, IFileSystemAccess2 fsa, IGeneratorContext context) {
	    project = (resource.allContents.head as ProjectDescription).project
	    generateWithErrorReport(selectedTasks, fsa, context)
	}
	
	private def List<String> generateWithErrorReport(Set<String> tasksToExecute, IFileSystemAccess2 fsa, IGeneratorContext context) {
	    init(tasksToExecute, fsa)
        runTasks
        val monitorTasks = project.monitorTasks.filter[tasksToExecute.contains(name)].toList
        if(!monitorTasks.empty){
            try {
               generateJavaInfrastructure(monitorTasks, fsa)
            } catch (Exception e) {
               errors.add("Errors while generating monitoring code.")
               errors.add(e.message)   
            }
        }
        createErrorFile(fsa, errors)
        System.gc //workaround plantuml might have reference to outputWriter
        if(context instanceof CmdLineContext){
            if(!errors.empty) {
                context.executionResult = false
                context.errorString = "Errors encountered during generation"
            }
        }
        return errors
    }
    
    def Set<String> getTasksClosure(Resource resource, Set<String> selectedTasks) {
        project = (resource.allContents.head as ProjectDescription).project
        val dependencies = project.getTaskDependencyMap
        selectedTasks.requiredTasksClosure(dependencies)
    }

	def init(Set<String> tasksToExecute, IFileSystemAccess2 fsa) {
	    errors.clear
		processTaskConsumer = new Consumer<Task>() {
			override accept(Task task) {
			    if(tasksToExecute.contains(task.name)){
			         errors.addAll(processTask(task, fsa))   
			    }
			}
		}
		setOutputLocator
	}
	
	protected def taskNames() {
		project.getAllTasks.map[name]
	}
	
	def taskNames(Resource res) {
	    (res.allContents.head as ProjectDescription).project.getAllTasks.map[name]
    }

	def runTasks() {
		project.monitorTasks.forEach(processTaskConsumer)
        project.documentationTasks.forEach(processTaskConsumer)
        project.UMLTasks.forEach(processTaskConsumer)
        project.testCasesGenerationTasks.forEach(processTaskConsumer)
        project.testApplicationGeneratorTasks.forEach(processTaskConsumer)
        project.simulatorGenerationTasks.forEach(processTaskConsumer)
        project.modelQualityChecksTasks.forEach(processTaskConsumer)
	}

	def void setOutputLocator() {
		outputLocator = new OutputLocator
	}

	def createErrorFile(IFileSystemAccess2 fsa, List<String> errors) {
		if (!errors.empty) {
			// Errors present
			fsa.generateFile(
				"errorReport.txt",
				'''
					«FOR error : errors»
						«error»
					«ENDFOR»
				'''
			)
		}
	}

	def dispatch List<String> processTask(MonitoringTask task, IFileSystemAccess2 fsa) {
		val generatorTask = new MonitoringGeneratorTask(task, scopeProvider, outputLocator, fsa)		
		generatorTask.startGeneration
	}

	def dispatch List<String> processTask(UMLTask task, IFileSystemAccess2 fsa) {
		val generatorTask  = new UMLGeneratorTask(task, scopeProvider, outputLocator, fsa)
		generatorTask.startGeneration
	}

	def dispatch List<String> processTask(DocumentationGenerationTask task, IFileSystemAccess2 fsa) {
		val generatorTask = new DocumentationGeneratorTask(task, scopeProvider, outputLocator, fsa)
		generatorTask.startGeneration
	}
    
    def dispatch List<String> processTask(TestApplicationGenerationTask task, IFileSystemAccess2 fsa) {
        val generatorTask = new TestApplicationGeneratorTask(task, scopeProvider, outputLocator, fsa)
        generatorTask.startGeneration
    }

    def dispatch List<String> processTask(TestCasesGenerationTask task, IFileSystemAccess2 fsa) {
        val generatorTask = new TestCasesGeneratorTask(task, scopeProvider, outputLocator, fsa)
        generatorTask.startGeneration
    }
    
    def dispatch List<String> processTask(ModelQualityChecksGenerationTask task, IFileSystemAccess2 fsa) {
		val generatorTask = new ModelQualityChecksGeneratorTask(task, scopeProvider, outputLocator, fsa)
		generatorTask.startGeneration
	}

    def dispatch List<String> processTask(SimulatorGenerationTask task, IFileSystemAccess2 fsa) {
        val generatorTask = new SimulatorGeneratorTask(task, scopeProvider, outputLocator, fsa)
        generatorTask.startGeneration
    }

	def generateJavaInfrastructure(List<MonitoringTask> monitorTasks, IFileSystemAccess2 fsa){
		val javaFsa = new CommaFileSystemAccess(OutputLocator.JAVA_FOLDER, fsa)
		val recordTypes = StateMachineUtilities::getRecordTypes(project, scopeProvider).toSet
		val infrastructureGenerator = new InfrastructureGenerator(javaFsa, recordTypes)
		infrastructureGenerator.generateRecordsTracker
		val interfaces = CommaUtilities::resolveProxy(project, scopeProvider.getScope(project, ProjectPackage.Literals.INTERFACE_REFERENCE__INTERFACE).allElements).map[it as Interface].toSet
		val components = CommaUtilities::resolveProxy(project, scopeProvider.getScope(project, ProjectPackage.Literals.COMPONENT_REFERENCE__COMPONENT).allElements).map[it as Component].toSet
		//generate the factory
		infrastructureGenerator.generateFactory(interfaces, components)
		//generate classes from each interface
		for(i : interfaces){
			val signature = InterfaceUtilities::getSignature(i, scopeProvider) 
			val decisionClassGenerator = new InterfaceDecisionGenerator(signature, i, javaFsa);
			decisionClassGenerator.generateUtilityClass()
			decisionClassGenerator.generateConstraintClasses
			decisionClassGenerator.generateDecisionClass()
		}
		val fcGenerator = new FunctionalConstraintGenerator(interfaces, javaFsa)
		
		for(c : components) {
			if(c.functionalConstraintsBlock !== null){
				for(fc : c.functionalConstraintsBlock.functionalConstraints) {
					fcGenerator.generateUtilityClass(c, fc)
					fcGenerator.generateConstraintClass(c, fc)
				}
			}
			val tdGenerator = new ComponentTimeDataConstraintsGenerator(c, javaFsa)
			tdGenerator.generateConstraintClasses
		}
		infrastructureGenerator.generateScenarioPlayer(monitorTasks.map[name])
	}
}
