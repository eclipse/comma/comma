/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.validation

import com.google.inject.Inject
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.HashSet
import java.util.List
import org.eclipse.comma.behavior.behavior.ProvidedPort
import org.eclipse.comma.behavior.behavior.StateMachine
import org.eclipse.comma.behavior.component.component.ComponentPackage
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinitionPackage
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.project.ProjectUtility
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.project.project.CompoundInterface
import org.eclipse.comma.project.project.DocumentationGenerationTask
import org.eclipse.comma.project.project.FilePath
import org.eclipse.comma.project.project.GeneratorBlock
import org.eclipse.comma.project.project.InterfaceMappings
import org.eclipse.comma.project.project.InterfaceReference
import org.eclipse.comma.project.project.ModelQualityChecksGenerationTask
import org.eclipse.comma.project.project.MonitoringTask
import org.eclipse.comma.project.project.Project
import org.eclipse.comma.project.project.ProjectDescription
import org.eclipse.comma.project.project.ProjectPackage
import org.eclipse.comma.project.project.ReachabilityGraphGenerationTask
import org.eclipse.comma.project.project.SimulatorGenerationTask
import org.eclipse.comma.project.project.Task
import org.eclipse.comma.project.project.TestApplicationGenerationTask
import org.eclipse.comma.project.project.TestCasesGenerationTask
import org.eclipse.comma.project.project.TraceSource
import org.eclipse.comma.project.project.TypeMappings
import org.eclipse.comma.testcases.TestCasesGenerator
import org.eclipse.comma.types.types.FileImport
import org.eclipse.comma.types.types.NamespaceImport
import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator

import static org.eclipse.comma.project.project.ProjectPackage.Literals.*

import static extension org.eclipse.comma.behavior.utilities.StateMachineUtilities.*
import static extension org.eclipse.comma.project.ProjectUtility.*
import org.eclipse.comma.project.project.EnvironmentComponent
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.behavior.RequiredPort
import org.eclipse.xtext.diagnostics.Severity

/**
 * This class contains custom validation rules.
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class ProjectValidator extends AbstractProjectValidator {

	public final String WARNING_CODE_EMPTY_BLOCK = "warning_code_empty_block"
	public final String ERROR_CODE_DUPLICATE_BLOCK = "error_code_duplicate_block"

	@Inject IScopeProvider scopeProvider
    @Inject IResourceValidator validator

	@Check
	override checkImportForValidity(FileImport imp) {
		if (! EcoreUtil2.isValidUri(imp.eResource, URI.createURI(imp.importURI))) {
			error("Invalid resource", TypesPackage.Literals.FILE_IMPORT__IMPORT_URI)
		} else {
			var Resource res = EcoreUtil2.getResource(imp.eResource, imp.importURI)
			if (res === null) {
				error("Invalid resource", TypesPackage.Literals.FILE_IMPORT__IMPORT_URI)
			} else if(!res.errors.isEmpty) {
				error("Resource contains errors", TypesPackage.Literals.FILE_IMPORT__IMPORT_URI)
			}
		}
	}

	@Check
	def checkUniqueTaskName(Project project) {
		val duplicates = new HashSet<Task>()
		val knownTasks = new HashMap<String, Task>
		val tasks = EcoreUtil2.getAllContentsOfType(project, Task)
		for (task : tasks) {
			val duplicate = knownTasks.put(task.name, task)
			if (duplicate !== null) {
				duplicates.add(task)
				duplicates.add(duplicate)
			}
		}

		duplicates.forEach[error("Duplicate task name.", it, TASK__NAME)]
	}

	@Check
	def checkBlocks(Project project) {
		val duplicates = new HashSet<GeneratorBlock>()
		val knownBlocks = new HashMap<String, GeneratorBlock>
		for (block : project.generatorBlocks) {
			if (block.eAllContents.isEmpty) {
				warning("This block contains no tasks or elements.", block, null, WARNING_CODE_EMPTY_BLOCK)
			}

			val duplicate = knownBlocks.put(block.eClass.name, block)
			if (duplicate !== null) {
				duplicates.add(block)
				duplicates.add(duplicate)
			}
		}
		duplicates.forEach[error("Duplicate block.", it, null, ERROR_CODE_DUPLICATE_BLOCK)]
	}

	/****************************** MAPPING VALIDATION *********************************/
	
    @Check
    def checkDuplicatedTypeMappingsNames(Project prj) {
        val typeMappings = prj.typeMappings
        for (tm1 : typeMappings) {
            for (tm2 : typeMappings) {
                if ((tm1 != tm2 && tm1.name.equals(tm2.name))) {
                    error("Duplicate name of type mappings", tm1, null)
                }
            }
        }
    }
    
	@Check
	def checkDuplicatedCommonMappings(TypeMappings mappings) {
		for (m1 : mappings.commonMappings) {
			for (m2 : mappings.commonMappings) {
				if (m1 != m2 && m1.type == m2.type) {
					error(
						"Duplicate type mapping",
						TYPE_MAPPINGS__COMMON_MAPPINGS,
						mappings.commonMappings.indexOf(m1)
					)
				}
			}
		}
	}

	@Check
	def checkDuplicatedInterfaceMappings(TypeMappings mappings) {
		for (m1 : mappings.interfaceMappings) {
			for (m2 : mappings.interfaceMappings) {
				if (m1 != m2 && m1.interface == m2.interface) {
					error(
						"Duplicate interface mapping",
						TYPE_MAPPINGS__INTERFACE_MAPPINGS,
						mappings.interfaceMappings.indexOf(m1)
					)
				}
			}
		}
	}

	@Check
	def checkDuplicatedTypesInInterfaceMappings(InterfaceMappings mapping) {
		for (m1 : mapping.mappings) {
			for (m2 : mapping.mappings) {
				if (m1 != m2 && m1.type == m2.type) {
					error(
						"Duplicate type mapping",
						INTERFACE_MAPPINGS__MAPPINGS,
						mapping.mappings.indexOf(m1)
					)
				}
			}
		}
	}

	@Check
    def checkReachabilityGenerationTaskParameters(ReachabilityGraphGenerationTask task) {
        if (task.params !== null) {
            checkParamsResource(task, task.params, ProjectPackage.Literals.REACHABILITY_GRAPH_GENERATION_TASK__PARAMS, 0)
        }
    }
    
    @Check
    def checkTestCasesGenerationTaskParameters(TestCasesGenerationTask task) {
        if (task.params !== null) {
            checkParamsResource(task, task.params, ProjectPackage.Literals.TEST_CASES_GENERATION_TASK__PARAMS, 0)
        }
    }
    
    @Check
    def checkSimulatorGenerationTaskParameters(SimulatorGenerationTask task) {
        if (task.params !== null) {
            for (param : task.params) {
            	checkParamsResource(task, param, ProjectPackage.Literals.SIMULATOR_GENERATION_TASK__PARAMS, task.params.indexOf(param))
            }
        }
    }
    
    @Check
    def checkReachabilityGraphGenerationTaskParameters(ReachabilityGraphGenerationTask task) {
        if (task.params !== null) {
            checkParamsResource(task, task.params, ProjectPackage.Literals.REACHABILITY_GRAPH_GENERATION_TASK__PARAMS, 0)
        }
    }
    
    @Check
    def checkModelQualityChecksGenerationTaskParameters(ModelQualityChecksGenerationTask task) {
        if (task.params !== null) {
        	checkParamsResource(task, task.params, ProjectPackage.Literals.MODEL_QUALITY_CHECKS_GENERATION_TASK__PARAMS, 0)
        }
    }
    
    @Check
    def checkTestApplicationGenerationTaskParameters(TestApplicationGenerationTask task) {
        if (task.params !== null) {
            for (param : task.params) {
                checkParamsResource(task, param, ProjectPackage.Literals.TEST_APPLICATION_GENERATION_TASK__PARAMS, task.params.indexOf(param))
            }
        }
    }
    
    def checkParamsResource(EObject resource, String param, EStructuralFeature feature, int index) {
    	val ipres = EcoreUtil2.getResource(resource.eResource, param)
        if(ipres === null || !(ipres.contents.head instanceof Parameters)) {
            error("Invalid resource", feature, index)
        } else {
            if (!validator.validate(ipres, CheckMode.ALL, null).filter[issue | issue.severity == Severity.ERROR].isEmpty) {
               error("Errors found in .params file", feature, index)
            } 
        }
    }
    
    @Check
    def checkHomeStates(ModelQualityChecksGenerationTask task) {
    	if (task.homeStateSets !== null) {
    		task.homeStateSets.forEach[set | {
    		    val missingMachines = task.target.interface.machines.map[m | m.name].toSet
    			val machines = new ArrayList<String>();
    			set.states.forEach[state | {
    				var name = (state.hs.eContainer as StateMachine).name
    				if (machines.contains(name)) {
    					val error = String.format("Home state set contains multiple states from machine %s", String.join(", ", name));
    					error(error, state, HOME_STATE__HS);
    				}
    				machines.add(name);
    				missingMachines.remove(name);
    			}]
    			
    			if (!missingMachines.isEmpty) {
    			    val error = String.format("Home state set is missing state from machine(s): %s", String.join(", ", missingMachines));
                    error(error, set, HOME_STATE_SET__STATES);
    			}
    		}]
    	}
    }
    
    @Check
    def checkReachabilityGenerationTaskMissingParameters(ReachabilityGraphGenerationTask task) {
        checkParametersMissing(task.target, task.params !== null ? Arrays.asList(task.params) : null, task, false)
    }
    
    @Check
    def checkTestCasesGenerationTaskMissingParameters(TestCasesGenerationTask task) {
        checkParametersMissing(task.target, task.params !== null ? Arrays.asList(task.params) : null, task, false)
    }
    
    @Check
    def checkTestCasesGenerationTaskMissingTemplate(TestCasesGenerationTask task) {
        var projectFile = ProjectUtility.uriToFile(task.eResource.URI);
        if (task.template !== null && TestCasesGenerator.getTemplate(projectFile, task.template) === null) {
            val error = String.format("Template file '%s' does not exist", task.template);
            error(error, task, TASK__NAME);
        }
    }
    
    @Check
    def checkTestApplicationGenerationTaskMissingParameters(TestApplicationGenerationTask task) {
        checkParametersMissing(task.target, task.params, task, true)
    }
    
    @Check
    def checkSimulatorGenerationTaskMissingParameters(SimulatorGenerationTask task) {
        checkParametersMissing(task.target, task.params, task, true)
    }
    
    @Check
    def checkModelQualityChecksGenerationTaskMissingParameters(ModelQualityChecksGenerationTask task) {
    	checkParametersMissing(task.target, task.params !== null ? Arrays.asList(task.params) : null, task, false)
    }
    
    def checkParametersMissing(Object target, List<String> params, EObject task, boolean requiredPorts) {
    	val interfaces = new ArrayList<Interface>();
    	
    	if (target instanceof InterfaceReference) {
    		interfaces.add((target as InterfaceReference).interface)
    	} else if (target instanceof ComponentReference) {
    		val comp = (target as ComponentReference).component
    		val compInterfaces = ComponentUtilities::getAllInterfaces(comp, scopeProvider)
    		comp.ports.forEach[p | {
    			if (p instanceof ProvidedPort || requiredPorts) {
    				val itf = compInterfaces.findFirst[i | i.name.equals(p.interface.name)]
    				interfaces.add(itf)
    			}
    		}]
    	}
    	
    	var missing = interfaces.filter[itf | itf.hasOpenEvents].toList
    	var missingByName = new ArrayList<Interface>(missing).map[i | i.name].toList
    	
    	if (params !== null) {
    		for (file : params) {
    			val param = EcoreUtil2.getResource(task.eResource, file)
    			if(param !== null && param.contents.head instanceof Parameters) {
                    val p = param.contents.head as Parameters
                    missing.remove(p.interface)
                    missingByName.remove(p.interface.name)
                }
    		}
    	}
    	
    	if (!missingByName.isEmpty) {
    		val error = String.format("Missing parameters for %s", String.join(", ", missingByName));
    		error(error, task, TASK__NAME);
    	} else if (!missing.isEmpty) {
            val error = String.format("Provided parameters for %s are for different .interface", String.join(", ", missing.map[i | i.name]));
            error(error, task, TASK__NAME);
        }
    }
    
    def hasOpenEvents(Interface itf) {
        itf.machines.map[states].flatten.exists(s | !s.allOpenTriggers.empty || !s.allOpenNotifications.empty || !s.allCommandsWithOpenReply.empty)
    }

	/****************************** TASK VALIDATION *********************************/

	@Check
	def checkMonitoringTask(MonitoringTask task) {
		validateTraceSources(task.traces)
		validateDirectories(task.tracedirs, task)
	}

	def validateTraceSources(List<TraceSource> sources) {
		val filteredSources = sources.filter(FilePath).filter(s | s.path !== null).toList
		val resources = ProjectUtility::getTraceResourcesFromFiles(filteredSources)
		for(invalid : resources.invalidTraceSources){
			error("Invalid resource", invalid, FILE_PATH__PATH)
		}
		for(wrongNamed : resources.wrongnamedTraceSources){
			error("Trace file name must start with a letter and may contain letters, digits and _", wrongNamed, FILE_PATH__PATH)
		}
		for(duplicate : resources.duplicateTraceSources){
			error("File with the same name is already used", duplicate, FILE_PATH__PATH)
		}
	}

	@Check
	def checkDuplicatedInterfaces(CompoundInterface compInterface) {
		for (i1 : compInterface.interfaces) {
			for (i2 : compInterface.interfaces) {
				if (i1 != i2 && i1.interfaceModel == i2.interfaceModel) {
					error("Duplicate interface", i1, null)
				}
			}
		}
	}

	@Check
	def checkCompoundName(CompoundInterface compound) {
		for (interface : ComponentUtilities::getAllInterfaces(compound, scopeProvider)) {
			if (interface.name.equals(compound.name)) {
				error("Compound interface names are not allowed to be equal to an imported interface.",
					COMPOUND_INTERFACE__NAME)
			}
		}
	}

	def validateDirectories(List<FilePath> filePaths, Task task) {
		val resources = ProjectUtility::getTraceResourcesFromDirs(filePaths)
		for(dir : resources.nonexistentDirectories){
			error("Directory does not exist", dir, FILE_PATH__PATH)
		}
		for(dir : resources.duplicateDirectories){
			error("Directory is already listed", dir, FILE_PATH__PATH)
		}
		if(!resources.duplicateTraceFiles.empty){
			error("Trace directories contain files with duplicate names", task, TASK__NAME)
		}
		if(!resources.wrongnamedTraceFiles.empty){
			error("Trace directories contain files with wrong names", task, TASK__NAME)
		}
	}

	@Check
	def validateDocGenerationTask(DocumentationGenerationTask task){
		if (! EcoreUtil2.isValidUri(task.eResource, URI.createURI(task.template))) {
			error("Template not found", ProjectPackage.Literals.DOCUMENTATION_GENERATION_TASK__TEMPLATE)
		}
	}
	
	@Check
    def checkDuplications(ProjectDescription prj){
        if(prj.imports.filter(NamespaceImport).empty) {
            return
        }
        var multiMap = getGlobalDeclarations(prj, TypesPackage.eINSTANCE.typeDecl)
        multiMap.placePredefinedTypes
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported type " + entry.key.toString, prj.project, ProjectPackage.Literals.PROJECT__NAME)
            }
        }
        multiMap = getGlobalDeclarations(prj, InterfaceDefinitionPackage.Literals.INTERFACE)
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported interface " + entry.key.toString, prj.project, ProjectPackage.Literals.PROJECT__NAME)
            }
        }
        multiMap = getGlobalDeclarations(prj, ComponentPackage.Literals.COMPONENT)
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported component " + entry.key.toString, prj.project, ProjectPackage.Literals.PROJECT__NAME)
            }
        }
    }
    
    @Check
    def checkClientEnvironment(TestApplicationGenerationTask task) {
        if(task.target instanceof ComponentReference) {
            if(!task.envComponents.empty) {
                checkClientEnvironment(task.envComponents, (task.target as ComponentReference).component)
            }   
        }
        if(task.target instanceof InterfaceReference && !task.envComponents.empty) {
            warning("Component environment is not applicable to interface testing. It will be ignored", ProjectPackage.Literals.TEST_APPLICATION_GENERATION_TASK__ENV_COMPONENTS)
        }
    }
    
    @Check
    def checkClientEnvironment(SimulatorGenerationTask task) {
        if(task.target instanceof ComponentReference) {
            if(!task.envComponents.empty) {
                checkClientEnvironment(task.envComponents, (task.target as ComponentReference).component)
            }   
        }
        if(task.target instanceof InterfaceReference && !task.envComponents.empty) {
            warning("Component environment is not applicable to interface simulator. It will be ignored", ProjectPackage.Literals.SIMULATOR_GENERATION_TASK__ENV_COMPONENTS)
        }
    }
    
    def checkClientEnvironment(List<EnvironmentComponent> envComponents, Component component) {
        val components = new HashSet<String>
        for(envComponent : envComponents) {
            if(!components.add(envComponent.role + envComponent.id)) {
                error("Component is already defined", envComponent, ProjectPackage.Literals.ENVIRONMENT_COMPONENT__ID)
            }
            val ports = new HashSet<String>
            for(portSpec : envComponent.portSpecs) {
                if(portSpec.port !== null) {
                    if(!ports.add(portSpec.port.name)) {
                        error("Port is already used", portSpec, ProjectPackage.Literals.PORT_SPEC__PORT)
                    }
                }
                if(envComponent.role.equals("client") && portSpec.port instanceof RequiredPort) {
                    error("Clients can not be defined for required ports", portSpec, ProjectPackage.Literals.PORT_SPEC__PORT)
                }
                if(envComponent.role.equals("server") && portSpec.port instanceof ProvidedPort) {
                    error("Servers can not be defined for provided ports", portSpec, ProjectPackage.Literals.PORT_SPEC__PORT)
                }
                if(portSpec.paramFile !== null) {
                    val ipres = EcoreUtil2.getResource(portSpec.eResource, portSpec.paramFile)
                    if(ipres === null || !(ipres.contents.head instanceof Parameters)) {
                        error("Invalid resource", portSpec, ProjectPackage.Literals.PORT_SPEC__PARAM_FILE)
                    } else {
                        if (!validator.validate(ipres, CheckMode.ALL, null).filter[issue | issue.severity == Severity.ERROR].isEmpty) {
                            error("Errors found in .params file", portSpec, ProjectPackage.Literals.PORT_SPEC__PARAM_FILE)
                        } else {
                            if(!(ipres.contents.head as Parameters).interface.name.equals(portSpec.port.interface.name)) {
                                error(".params file and the port have different interfaces", portSpec, ProjectPackage.Literals.PORT_SPEC__PARAM_FILE)
                            }
                        }
                    }
                }
            }
        }
    }
}
