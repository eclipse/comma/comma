/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.scoping

import java.util.ArrayList
import org.eclipse.comma.behavior.behavior.State
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.project.project.ComponentReference
import org.eclipse.comma.project.project.HomeState
import org.eclipse.comma.project.project.InterfaceMappings
import org.eclipse.comma.project.project.ModelQualityChecksGenerationTask
import org.eclipse.comma.project.project.PortSpec
import org.eclipse.comma.project.project.ProjectPackage
import org.eclipse.comma.project.project.SimulatorGenerationTask
import org.eclipse.comma.project.project.TestApplicationGenerationTask
import org.eclipse.comma.project.project.TypeMapping
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference

import static org.eclipse.xtext.scoping.Scopes.*

/** 
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class ProjectScopeProvider extends AbstractProjectScopeProvider {
	
	override getScope(EObject context, EReference reference)
	{
		if (context instanceof TypeMapping && reference == ProjectPackage.Literals.TYPE_MAPPING__TYPE) {
			if (context.eContainer instanceof InterfaceMappings)
				return scopeFor((context.eContainer as InterfaceMappings).interface.types.filter(t | t instanceof SimpleTypeDecl), super.getScope(context, reference))
		} else if(context instanceof HomeState && reference == ProjectPackage.Literals.HOME_STATE__HS){
		    return scope_HomeState_hs(context)
		}
		
		if (context instanceof PortSpec && reference == ProjectPackage.Literals.PORT_SPEC__PORT) {
		    val task = context.eContainer.eContainer
		    var Component component = null
		    if(task instanceof TestApplicationGenerationTask) {
		        if(task.target instanceof ComponentReference) {
		            component = (task.target as ComponentReference).component
		        }
		    }
		    if(task instanceof SimulatorGenerationTask) {
                if(task.target instanceof ComponentReference) {
                    component = (task.target as ComponentReference).component
                }
            }
            if(component !== null) {
                return scopeFor(component.ports)
            }
		}

		return super.getScope(context, reference);
	}
	
	def scope_HomeState_hs(EObject context){
	    var Interface i = null;
	    if (context.eContainer.eContainer instanceof ModelQualityChecksGenerationTask) {
	        i = (context.eContainer.eContainer as ModelQualityChecksGenerationTask).target.interface;
	    }

		var result = new ArrayList<State>
		if(i.eResource !== null) 
		{
			val statemachines = i.machines
			for (sm : statemachines){
				result.addAll(sm.states)
			}
		} 
		return scopeFor(result)
	}
}
