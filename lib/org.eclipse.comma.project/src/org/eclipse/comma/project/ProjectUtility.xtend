/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project

import java.io.File
import java.io.FilenameFilter
import java.util.ArrayList
import java.util.Collections
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import java.util.regex.Pattern
import org.eclipse.comma.project.project.CompoundInterface
import org.eclipse.comma.project.project.CompoundInterfaceBlock
import org.eclipse.comma.project.project.DocumentationGenerationBlock
import org.eclipse.comma.project.project.DocumentationGenerationTask
import org.eclipse.comma.project.project.FilePath
import org.eclipse.comma.project.project.MonitoringBlock
import org.eclipse.comma.project.project.MonitoringTask
import org.eclipse.comma.project.project.Project
import org.eclipse.comma.project.project.ReachabilityGraphGenerationBlock
import org.eclipse.comma.project.project.ReachabilityGraphGenerationTask
import org.eclipse.comma.project.project.SimulatorGenerationBlock
import org.eclipse.comma.project.project.SimulatorGenerationTask
import org.eclipse.comma.project.project.Task
import org.eclipse.comma.project.project.TypeMappings
import org.eclipse.comma.project.project.TypeMappingsBlock
import org.eclipse.comma.project.project.UMLBlock
import org.eclipse.comma.project.project.UMLTask
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.comma.project.project.ModelQualityChecksGenerationBlock
import org.eclipse.comma.project.project.ModelQualityChecksGenerationTask
import org.eclipse.comma.project.project.TestApplicationGenerationBlock
import org.eclipse.comma.project.project.TestApplicationGenerationTask
import org.eclipse.comma.project.project.TestCasesGenerationBlock
import org.eclipse.comma.project.project.TestCasesGenerationTask
import org.eclipse.core.runtime.Path
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import org.eclipse.comma.petrinet.EnvConfig
import org.eclipse.emf.ecore.EObject
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.comma.project.project.EnvironmentComponent
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities
import org.eclipse.comma.behavior.interfaces.scoping.InterfaceUtilities

class ProjectUtility {
    
    def static getAllTasks(Project prj) {
        EcoreUtil2.getAllContentsOfType(prj, Task)
    }
	
	def static getTypeMappings(Project prj) {
		for(block : prj.generatorBlocks) {
			if(block instanceof TypeMappingsBlock) {
				return block.tasks
			}
		} 
		Collections.<TypeMappings>emptyList
	}
	
	def static getMonitorTasks(Project prj) {
		for(block : prj.generatorBlocks) {
			if(block instanceof MonitoringBlock) {
				return block.tasks
			}
		} 
		Collections.<MonitoringTask>emptyList
	}
	
	def static runtimeTask(MonitoringTask task){
	    task.tracedirs.empty && task.traces.empty
	}
	
	def static getCompoundInterfaces(Project prj) {
		for(block : prj.generatorBlocks) {
			if(block instanceof CompoundInterfaceBlock) {
				return block.tasks
			}
		} 
		Collections.<CompoundInterface>emptyList
	}
	
	def static getDocumentationTasks(Project prj) {
		for(block : prj.generatorBlocks) {
			if(block instanceof DocumentationGenerationBlock) {
				return block.tasks
			}
		} 
		Collections.<DocumentationGenerationTask>emptyList
	}
	
	def static getUMLTasks(Project prj) {
		for(block : prj.generatorBlocks) {
			if(block instanceof UMLBlock) {
				return block.tasks
			}
		} 
		Collections.<UMLTask>emptyList
	}
	
	def static getModelQualityChecksTasks(Project prj) {
		for(block : prj.generatorBlocks) {
			if(block instanceof ModelQualityChecksGenerationBlock) {
				return block.tasks
			}
		}
		Collections.<ModelQualityChecksGenerationTask>emptyList
	}
    
    def static getTestApplicationGeneratorTasks(Project prj) {
        for(block : prj.generatorBlocks) {
            if(block instanceof TestApplicationGenerationBlock) {
                return block.tasks
            }
        }
        Collections.<TestApplicationGenerationTask>emptyList
    }
	
	def static getReachabilityGraphGeneratorTasks(Project prj) {
        for(block : prj.generatorBlocks) {
            if(block instanceof ReachabilityGraphGenerationBlock) {
                return block.tasks
            }
        }
        Collections.<ReachabilityGraphGenerationTask>emptyList
    }
    
    def static getTestCasesGenerationTasks(Project prj) {
        for(block : prj.generatorBlocks) {
            if(block instanceof TestCasesGenerationBlock) {
                return block.tasks
            }
        }
        Collections.<TestCasesGenerationTask>emptyList
    }
	
    def static getSimulatorGenerationTasks(Project prj) {
        for(block : prj.generatorBlocks) {
            if(block instanceof SimulatorGenerationBlock) {
                return block.tasks
            }
        }
        Collections.<SimulatorGenerationTask>emptyList
    }
    
    /*
     * Methods for handling task dependencies
     */
     
    /**
     * Returns dependency map for the tasks in a given project.
     * An entry is a pair of task name and the set of names of tasks it depends on.
     * If a task does not depend on other tasks there is no entry for it.
     * The constraints on project files ensure that the dependency graph is acyclic.
     */
    def static Map<String, Set<String>> getTaskDependencyMap(Project prj){
        val result = new HashMap<String, Set<String>>
        val allTasks = prj.allTasks
        for(task : allTasks){
            //find all tasks that refer to task
            for(setting : EcoreUtil.UsageCrossReferencer.find(task, allTasks)){
                val containerTask = EcoreUtil2.getContainerOfType(setting.EObject, Task)
                if(containerTask !== null) { //containerTask refers/depends on task
                    if(result.containsKey(containerTask.name)){
                        result.get(containerTask.name).add(task.name)
                    }else{
                        val set = new HashSet<String>
                        set.add(task.name)
                        result.put(containerTask.name, set)
                    }                   
                }
            }
        }
        result
    }
    
    /**
     * Returns the reflective dependency transitive closure for a set of tasks based on
     * the provided task dependency map.
     */
    def static requiredTasksClosure(Set<String> tasks, Map<String, Set<String>> dependencies) {
        val result = new HashSet<String>
        tasks.forEach[result.addAll(getDependencyClosure(dependencies))]
        result
    }
    
    /**
     * For a given set of tasks, returns a subset with tasks that are required/referred to by other tasks
     * that are not in the given set.
     */
    def static findReferredTasks(Set<String> tasks, Map<String, Set<String>> dependencies) {
        tasks.filter[! dependencies.filter[k, v| v.contains(it) && !tasks.contains(k)].empty]
    }
    
    def private static Set<String> getDependencyClosure(String task, Map<String, Set<String>> dependencies) {
        val result = new HashSet<String>
        result.add(task)
        if(dependencies.containsKey(task)) {
            for(t : dependencies.get(task)) {
                result.addAll(t.getDependencyClosure(dependencies))
            }
        } 
        result
    }
    
    def static copyPlantUMLJar(File target) {
        var stream = ProjectUtility.getResourceAsStream("/plantuml.jar");
        Files.copy(stream, target.toPath, StandardCopyOption.REPLACE_EXISTING);
    }

	def static resolveUri(Resource context, String path) {
		val contextURI = context.getURI();
		var uri = URI.createFileURI(path)
		if (contextURI.isHierarchical() && !contextURI.isRelative() && (uri.isRelative() && !uri.isEmpty())) {
			uri = uri.resolve(contextURI);
		}		
		return uri;
	}
	
	def static TraceResources getTraceResourcesFromDirs(List<FilePath> directories){
		val result = new TraceResources
		val fileNames = new HashSet<String>
		val directoryNames = new HashSet<String>
		
		for (location : directories) {
			var uri = location.eResource.resolveUri(location.path)
			var uriOk = true 	
			var File dir
			if(uri.isPlatform) {
				val platform = uri.toPlatformString(true)
				val IResource eclipseResource = ResourcesPlugin.workspace.root.findMember(platform)		
				if(eclipseResource !== null){
					uri =  URI.createFileURI(eclipseResource.rawLocation.toOSString);
					dir = new File(uri.toFileString)
				}else{
					uriOk = false
				}
			}else{
				dir = new File(uri.toFileString)
			}
			
			val traceFilter = new FilenameFilter() {
				override accept(File dir, String name) {
					(name.endsWith(".traces") || name.endsWith(".events") || name.endsWith(".jsonl"))
				}
			}
			if (uriOk && dir.exists && dir.isDirectory) {
				if(!directoryNames.add(dir.toString)){
					result.duplicateDirectories.add(location)
				}else
					for (file : dir.listFiles(traceFilter)) {
						val fileUri = URI.createFileURI(file.path).resolve(location.eResource.URI)		
						if(EcoreUtil2.isValidUri(location.eResource, fileUri)){
							val fileName = fileUri.trimFileExtension.lastSegment
							if(! fileName.validFileName){
								result.wrongnamedTraceFiles.add(fileUri.toString)
							}else if(!fileNames.add(fileName)){
								result.duplicateTraceFiles.add(fileUri.toString)
							}else{
								result.validURIs.add(fileUri)
							}
						}				
					}
			} else {
				result.nonexistentDirectories.add(location)
			}
		}
		result
	}
	
	def static TraceResources getTraceResourcesFromFiles(List<FilePath> traceSources){
		val result = new TraceResources
		val names = new HashSet<String>
		for(traceSource : traceSources){
			val path = traceSource.path
			val uri = URI.createURI(path).resolve(traceSource.eResource.URI)
			if(! EcoreUtil2.isValidUri(traceSource.eResource, uri)){
				result.invalidTraceSources.add(traceSource)
			}else{
				val fileExtension = uri.fileExtension
				if(!(fileExtension.equals("traces") || fileExtension.equals("events") || fileExtension.equals("jsonl"))){
					result.invalidTraceSources.add(traceSource)
				}else{
					//resource is Ok. Check name
					val fileName = uri.trimFileExtension.lastSegment
					if(! fileName.validFileName){
						result.wrongnamedTraceSources.add(traceSource)
					}else if(!names.add(fileName)){
						result.duplicateTraceSources.add(traceSource)
					}else{
						result.validURIs.add(uri)
					}
				}
			}
		}
		result
	}
	
	def static List<EnvConfig> getComponentEnvironmentConfig(EObject task, Component component, List<EnvironmentComponent> envComponents, List<Parameters> parameters, IScopeProvider scopeProvider) {
	    val allInterfaces = ComponentUtilities.getAllInterfaces(component.eContainer(), scopeProvider);
        var List<EnvConfig> environment = new ArrayList<EnvConfig>();
        if(envComponents.isEmpty()) { // default case: for every port a single connection
            for(port : component.ports ){
                val itf = allInterfaces.stream().filter(ii | InterfaceUtilities.getSignature(ii, scopeProvider) == port.getInterface()).findFirst().get();
                val param = parameters.stream().filter(p | p.getInterface().equals(itf)).findFirst();
                environment.add(new EnvConfig("c", port, param.isPresent() ? param.get() : null))
            }
        } else { // components in the environment are given
            for(envComponent : envComponents) {
                for(portSpec : envComponent.portSpecs) {
                    var Parameters param = null;
                    val itf = allInterfaces.stream().filter(ii | InterfaceUtilities.getSignature(ii, scopeProvider) == portSpec.port.getInterface()).findFirst().get();
                    if(portSpec.paramFile === null) { // use default param file
                        var p = parameters.stream().filter(p | p.getInterface().equals(itf)).findFirst()
                        param = p.isPresent() ? p.get() : null;
                    } else {
                        param = EcoreUtil2.getResource(task.eResource, portSpec.paramFile).contents.head as Parameters
                    }
                    environment.add(new EnvConfig(envComponent.id, portSpec.port, param))
                }
            }
        }
        return environment
	}
	
	def static uriToFile(URI uri) {
	    if (uri.isPlatform) {
	        return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(uri.toPlatformString(true))).location.toFile;
	    } else {
	        return new File(uri.toFileString);
	    }
	}
	
	def static boolean isValidFileName(String fileName){
		Pattern.matches("[a-zA-Z][a-zA-Z0-9_]*", fileName)
	}
	
	static class TraceResources {
		public List<FilePath> invalidTraceSources
		public List<FilePath> wrongnamedTraceSources
		public List<FilePath> duplicateTraceSources
		public List<FilePath> nonexistentDirectories
		public List<FilePath> duplicateDirectories
		public List<String> wrongnamedTraceFiles
		public List<String> duplicateTraceFiles
		public List<String> invalidTraceFiles
		public List<URI> validURIs
		
		new(){
			invalidTraceSources = new ArrayList<FilePath>
			wrongnamedTraceSources = new ArrayList<FilePath>
			duplicateTraceSources = new ArrayList<FilePath>
			validURIs = new ArrayList<URI>
			wrongnamedTraceFiles = new ArrayList<String>
			duplicateTraceFiles = new ArrayList<String>
			invalidTraceFiles = new ArrayList<String>
			nonexistentDirectories = new ArrayList<FilePath>
			duplicateDirectories = new ArrayList<FilePath>
		}
	}
}
