/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.importing

import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionManager
import java.util.Collection
import org.eclipse.xtext.resource.IResourceDescription.Delta
import org.eclipse.xtext.resource.IResourceDescription
import org.eclipse.xtext.resource.IResourceDescriptions
import org.eclipse.xtext.resource.IResourceDescription.Manager.AllChangeAware

/**
 * ResourceDescriptinManager for the prj models which customizes the DefaultResourceDescriptionManager. 
 * It will trigger a revalidation of the prj model when a interface or signature model is changed. 
 * The prj validator needs to be triggered when these models are changed and their exported EObjects are not.
 * (Future solutions could be to change the exported objects or move the validation to the corresponding models)
 */
class ProjectResourceDescriptionManager extends DefaultResourceDescriptionManager implements AllChangeAware {

	override isAffectedByAny(Collection<Delta> deltas, IResourceDescription candidate,
		IResourceDescriptions context) throws IllegalArgumentException {
		if (!candidate.URI.fileExtension.equals("prj")) {
			return false
		}
		isInterested(deltas)
	}

	def isInterested(Collection<Delta> deltas) {
		for (delta : deltas) {
			val ext = delta.uri.fileExtension
			if (ext.equals("interface") || ext.equals("signature")) {
				return true
			}
		}
		return false
	}
}
