/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.ui.handler;

import org.eclipse.comma.project.ui.handler.ProjectUIGenerator.LaunchFile;

public class MonitorLaunchShortcut extends LaunchShortcut {

	@Override
	LaunchFile getLaunchFile() {
		return LaunchFile.MONITOR;
	}

	@Override
	String getMessage() {
		return "Generating and executing monitoring";
	}
}
