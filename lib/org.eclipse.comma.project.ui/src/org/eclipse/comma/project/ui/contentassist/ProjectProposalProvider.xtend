/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.ui.contentassist

import java.util.List
import java.util.Map
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

import static org.eclipse.comma.project.project.ProjectPackage.Literals.*

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class ProjectProposalProvider extends TaskProposalProvider {

	// UI TEXT
	public static String BLOCK_DESCRIPTION = "Block containing tasks"
	public static String PROJECT_TITLE = "Project "
	protected static String PROJECT_INFO = "The project contains information about the configuration of the model."

	public static String GENERATE_DOCUMENTATION_TITLE = "Generate Documentation Block"
	public static String GENERATE_MONITORING_TITLE = "Generate Monitors Block"
	public static String GENERATE_MAPPINGS_TITLE = "Type Mappings Block"
	public static String GENERATE_MAPPINGS_SIMPLE_TITLE = "Type Mappings Simple"
	public static String GENERATE_STUBS_TITLE = "Generate Standalone Stubs Block"

	// ---------============  Proposals  ============--------------

	override complete_Project(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_Project(model, ruleCall, context, acceptor)

		for (IEObjectDescription interface : scopeProvider.getScope(model,
			INTERFACE_REFERENCE__INTERFACE).allElements) {
			addProjectTemplate(interface.name.lastSegment, context, acceptor)
		}
		addProjectTemplate(getInterfaceName(model), context, acceptor)
	}

	def addProjectTemplate(String interfaceName, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val prop = '''
			import "<interface_name>.interface"

			Project «interfaceName»Project {

			}
		'''
		acceptor.accept(createTemplate(PROJECT_TITLE + interfaceName, prop, PROJECT_INFO, 0, context))
	}

	/********************* Blocks **********************/

	override complete_MonitoringBlock(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_MonitoringBlock(model, ruleCall, context, acceptor)
		if(model.containsBlock(MONITORING_BLOCK)) {
			return
		}

		val proposal = '''
		Generate Monitors {
			«String.format(TASK_MONITOR, getInterfaceName(model))»
		}'''

		acceptor.accept(createTemplate("Generate Monitors Block", proposal, BLOCK_DESCRIPTION, 1, context))
	}

	override complete_DocumentationGenerationBlock(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_DocumentationGenerationBlock(model, ruleCall, context, acceptor)
		if(model.containsBlock(DOCUMENTATION_GENERATION_BLOCK)) {
			return
		}

		val proposal = '''
		Generate Documentations {
			«String.format(documentationTask, getInterfaceName(model))»
		}'''

		acceptor.accept(createTemplate("Generate Documentations Block", proposal, BLOCK_DESCRIPTION, 1, context))
	}
	
	override complete_TestApplicationGenerationBlock(EObject model, RuleCall ruleCall, ContentAssistContext context,
        ICompletionProposalAcceptor acceptor) {

        super.complete_TestApplicationGenerationBlock(model, ruleCall, context, acceptor)
        if(model.containsBlock(TEST_APPLICATION_GENERATION_BLOCK)) {
            return
        }

        val proposal = '''
        Generate TestApplication {
            «complete_TestApplicationGenerationTaskBody(model)»
        }'''

        acceptor.accept(createTemplate("Generate Test Application Block", proposal, BLOCK_DESCRIPTION, 1, context))
    }
    
    override complete_SimulatorGenerationBlock(EObject model, RuleCall ruleCall, ContentAssistContext context,
        ICompletionProposalAcceptor acceptor) {

        super.complete_SimulatorGenerationBlock(model, ruleCall, context, acceptor)
        if(model.containsBlock(SIMULATOR_GENERATION_BLOCK)) {
            return
        }
        
        val proposal = '''
        Generate Simulator {
            «complete_SimulatorGenerationTaskBody(model)»
        }'''

        acceptor.accept(createTemplate("Generate Simulator Block", proposal, BLOCK_DESCRIPTION, 1, context))
    }
    
    override complete_TestCasesGenerationBlock(EObject model, RuleCall ruleCall, ContentAssistContext context, 
        ICompletionProposalAcceptor acceptor) {
            
        super.complete_TestCasesGenerationBlock(model, ruleCall, context, acceptor)
        if(model.containsBlock(TEST_CASES_GENERATION_BLOCK)) {
            return
        }

        val proposal = '''
        Generate TestCases {
        	«complete_TestCasesGenerationTaskBody(model)»
        }'''

        acceptor.accept(createTemplate("Generate Test Cases Generation Block", proposal, BLOCK_DESCRIPTION, 1, context))
    }
    
    override complete_ModelQualityChecksGenerationBlock(EObject model, RuleCall ruleCall, ContentAssistContext context, 
        ICompletionProposalAcceptor acceptor) {
        	
        super.complete_ModelQualityChecksGenerationBlock(model, ruleCall, context, acceptor)            
        if(model.containsBlock(MODEL_QUALITY_CHECKS_GENERATION_BLOCK)) {
            return
        }

        val proposal = '''
        Generate ModelQualityChecks {
        	«complete_ModelQualityChecksGenerationTaskBody(model)»
        }'''

        acceptor.accept(createTemplate("Generate Model Quality Checks Generation Block", proposal, BLOCK_DESCRIPTION, 1, context))
    }

	override complete_UMLBlock(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.complete_UMLBlock(model, ruleCall, context, acceptor)

		if(model.containsBlock(UML_BLOCK)) {
			return
		}

		val proposal = '''
		Generate UML {
			«String.format(TASK_UML, getInterfaceName(model))»
		}'''

		acceptor.accept(createTemplate("Generate UML Block", proposal, BLOCK_DESCRIPTION, 1, context))
	}

	/********************* Mappings **********************/

	override complete_InterfaceMappings(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_InterfaceMappings(model, ruleCall, context, acceptor)

		val emptyProposal = '''
			interface IName {
				any -> "int"
			}
		'''

		acceptor.accept(
			createTemplate("Interface Mapping", emptyProposal, "Interface mapping with dummy values", 3, context))
	}

	override complete_TypeMappings(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_TypeMappings(model, ruleCall, context, acceptor)
		addEmptyMapping(model, context, acceptor)
	}


	/********************* Mapping **********************/
	def addEmptyMapping(EObject model, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		acceptor.accept(createTemplate("Type Mappings Task", TASK_TYPE_MAPPINGS, GENERATE_MAPPINGS_INFO, 2, context))
	}

	def addSimpleMapping(EObject model, Iterable<String> globalSimpleTypes, Map<Signature, List<String>> ifSimpleTypes,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val proposal = '''
			Type Mappings typeMappings{
				«FOR globalType : globalSimpleTypes»
					«globalType» -> "int"
				«ENDFOR»

				«FOR ifSimpleType : ifSimpleTypes.entrySet»
					interface «ifSimpleType.key.name» {
						«FOR type : ifSimpleType.value»
							«type» -> "int"
						«ENDFOR»
					}
				«ENDFOR»
			}
		'''
		acceptor.accept(
			createTemplate(GENERATE_MAPPINGS_SIMPLE_TITLE, proposal, GENERATE_MAPPINGS_SIMPLE_INFO, 1, context))
	}

	def getInterfaceName(EObject context) {
		val interfaces = getInterfaces(context)
		if(interfaces.isEmpty) {
			return INAME
		} else {
			return interfaces.get(0).name
		}
	}
}
