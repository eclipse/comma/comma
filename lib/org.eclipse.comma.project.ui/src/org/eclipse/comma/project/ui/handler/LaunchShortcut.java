/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.ui.handler;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.comma.java.JArtifactInfo;
import org.eclipse.comma.java.JArtifactInfoBuilder;
import org.eclipse.comma.java.JClassInfo;
import org.eclipse.comma.java.JCmd;
import org.eclipse.comma.java.JCmdBuilder;
import org.eclipse.comma.java.JCmdExecutor;
import org.eclipse.comma.java.JCompiler;
import org.eclipse.comma.java.JDependencyInfo;
import org.eclipse.comma.java.JDependencyInfos;
import org.eclipse.comma.java.JSourceInfo;
import org.eclipse.comma.java.JSourceInfos;
import org.eclipse.comma.project.ui.handler.ProjectUIGenerator.LaunchFile;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.xtext.builder.EclipseOutputConfigurationProvider;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.validation.IResourceValidator;

import com.google.inject.Inject;
import com.google.inject.Provider;

import net.sourceforge.plantuml.SourceFileReader;

abstract class LaunchShortcut implements ILaunchShortcut {
	private static final Logger LOGGER = Logger.getLogger(LaunchShortcut.class.getName());
	private static final String COMMA_CONSOLE = "Comma Console";
	private static final String MONITORING_BUNDLE = "org.eclipse.comma.monitoring.lib";
	private static final String GSON_BUNDLE = "com.google.gson";
	private static final String DASHBOARD_MONITORING_BUNDLE = "org.eclipse.comma.monitoring.dashboard";
	private static final String DASHBOARD_MODELQUALITYCHECKS_BUNDLE = "org.eclipse.comma.modelqualitychecks";
	private static final String MONITOR_MAIN_CLASS = "org.eclipse.comma.monitoring.generated.ScenarioPlayer";
	private static final String GENERATED_JAR_NAME = "/Player.jar";

	@Inject
	private IGenerator2 generator;

	@Inject
	private Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;

	@Inject
	private IResourceSetProvider resourceSetProvider;

	@Inject
	public void setOutputConfigurationProvider(EclipseOutputConfigurationProvider outputConfigurationProvider) {
		this.outputConfigurationProvider = outputConfigurationProvider;
	}
	private EclipseOutputConfigurationProvider outputConfigurationProvider;

	@Inject
	private IResourceValidator validator;
	
	@Override
	public void launch(ISelection selection, String mode) {
		LOGGER.info("launching from selection: " + selection + " in mode: " + mode);
		IStructuredSelection sel = (IStructuredSelection) selection;
		for (Object file : sel.toArray()) {
			IFile f = (IFile) file;
			generateThenLaunch(f, mode);
		}
	}

	@Override
	public void launch(IEditorPart editor, String mode) {
		LOGGER.info("launching from editor: " + editor.getTitle() + " in mode: " + mode);
		generateThenLaunch(((IFileEditorInput) editor.getEditorInput()).getFile(), mode);
	}

	private void generateThenLaunch(IFile projectFile, String mode) {
		IWorkbenchWindow workbench = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		boolean taskSelection = getLaunchFile() == LaunchFile.GENERATORS_TASK_SELECTION;
		
		var job = new ProjectUIGeneratorJob(getMessage(), projectFile, generator, fileAccessProvider.get(), resourceSetProvider,
				outputConfigurationProvider, validator, workbench.getShell(), this, mode, getLaunchFile(), taskSelection);
		job.schedule();
	}

	abstract LaunchFile getLaunchFile();
	
	abstract String getMessage();

	protected void launchMonitor(IFile file, String mode) {
		try {
			File projectAbsolutePath = new File(file.getLocation().removeLastSegments(8).toString());
			String javaSourcePath = file.getLocation().removeLastSegments(6).toString();

			MessageConsole console = findConsole(COMMA_CONSOLE);
			MessageConsoleStream out = console.newMessageStream();

			Runnable compileAndRunJava =
					() -> {
						Set<JSourceInfo> generatedSourceInfos;
						try {
							generatedSourceInfos = JSourceInfos.getJavaSourcesFromSrcDirectory(javaSourcePath);
							JDependencyInfo runtimeLibDependency = JDependencyInfos.getDependencyFromBundleId(MONITORING_BUNDLE);
							JDependencyInfo gsonDependency = JDependencyInfos.getDependencyFromBundleId(GSON_BUNDLE);
							JDependencyInfo dashboardMonitoringDependency = JDependencyInfos.getDependencyFromBundleId(DASHBOARD_MONITORING_BUNDLE);
							JDependencyInfo dashboardModelQualityChecksDependency = JDependencyInfos.getDependencyFromBundleId(DASHBOARD_MODELQUALITYCHECKS_BUNDLE);
							File plantumlJar = new File(SourceFileReader.class.getProtectionDomain().getCodeSource().getLocation().toURI());
							JDependencyInfo plantumlDependency = JDependencyInfos.create(plantumlJar);

							JCompiler compiler = new JCompiler()
								.withSourceInfos(generatedSourceInfos)
								.withDependencyInfos(runtimeLibDependency)
								.withDependencyInfos(dashboardMonitoringDependency)
								.withDependencyInfos(dashboardModelQualityChecksDependency)
								.withDependencyInfos(plantumlDependency)
								.withDependencyInfos(gsonDependency);

							out.println("Compiling and building generated code ...");
							List<JClassInfo> classInfos = compiler.compile();

							// create JArtifactBuilder and build JArtifact
							JArtifactInfo artifactInfo = new JArtifactInfoBuilder()
								.withArchiveFile(new File(javaSourcePath + GENERATED_JAR_NAME))
								.withClassInfos(classInfos)
								.withDependencyInfos(runtimeLibDependency)
								.withDependencyInfos(gsonDependency)
								.withDependencyInfos(dashboardMonitoringDependency)
								.withDependencyInfos(dashboardModelQualityChecksDependency)
								.withDependencyInfos(plantumlDependency)
								.withMainClass(MONITOR_MAIN_CLASS)
								.build();
							// create the cmd with options and arguments
							JCmd cmd = new JCmdBuilder()
								.withArtifactInfo(artifactInfo)
								.build();

							// execute the command
							out.println("Executing monitoring tasks...");
							
							var monitor = new JCmdExecutor(cmd)
								.withWorkingDirectory(projectAbsolutePath)
								.withOnStdOut((String line) -> out.print(line))
								.execute();
							monitor.process.waitFor();
							
							if (!monitor.getStdErr().isEmpty()) {
								System.out.println("Error occurred during compilation and execution:");
								System.out.print(monitor.getStdErr());
							} else {
								out.println("Finished executing monitoring tasks in " + monitor.getExecutionTime()  + " secs");
							}
							
							file.getProject().refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
						} catch (InterruptedException | IOException | CoreException | URISyntaxException | IllegalArgumentException e) {
							e.printStackTrace(new PrintStream(out));
						}
					};
			Thread executor = new Thread(compileAndRunJava);
			executor.start();
		} catch (PartInitException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}
	
	protected void launchSimulator(IFile file) {
		try {
			MessageConsole console = findConsole(COMMA_CONSOLE);
			MessageConsoleStream out = console.newMessageStream();
			MessageConsoleStream err = console.newMessageStream();
			err.setColor(new Color(255, 0, 0));

			Runnable runSimulator =
					() -> {
						try {
							var process = Runtime.getRuntime().exec(file.getLocation().toFile().toString());
							var code = process.waitFor();
					    	if (code != 0) {
					    		err.println("Simulator failed with exit code " + code);
					    	}
						} catch (InterruptedException | IOException | IllegalArgumentException e) {
							e.printStackTrace(new PrintStream(out));
						}
					};
			Thread executor = new Thread(runSimulator);
			executor.start();
		} catch (PartInitException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}
	
	private MessageConsole findConsole(String name) throws PartInitException {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager manager = plugin.getConsoleManager();
		IConsole[] existing = manager.getConsoles();
		MessageConsole myConsole = null;
	    for (int i = 0; i < existing.length; i++)
	    	if (name.equals(existing[i].getName()))
	    		myConsole = (MessageConsole) existing[i];
	    //no console found, so create a new one
	    if(myConsole == null) {
	    	myConsole= new MessageConsole(name, null);
	    	manager.addConsoles(new IConsole[]{myConsole});
	    }
	    IWorkbenchWindow workbench = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	    IWorkbenchPage page = workbench.getActivePage();
	    String id = IConsoleConstants.ID_CONSOLE_VIEW;
	    IConsoleView view = (IConsoleView) page.showView(id);
	    view.display(myConsole);
	    myConsole.clearConsole();
	    return myConsole;
	}
}