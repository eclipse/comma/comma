/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.ui.handler;

import static com.google.common.collect.Maps.uniqueIndex;
import static org.eclipse.xtext.ui.util.ResourceUtil.getContainer;
import static org.eclipse.xtext.ui.util.ResourceUtil.sync;

import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.eclipse.comma.project.generator.ProjectGenerator;
import org.eclipse.comma.project.generatortasks.OutputLocator;
import org.eclipse.comma.project.project.ProjectDescription;
import org.eclipse.comma.project.ui.internal.ProjectActivator;
import org.eclipse.comma.types.generator.CommaFileSystemAccess;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.xtext.builder.EclipseOutputConfigurationProvider;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.generator.GeneratorContext;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.generator.OutputConfiguration;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

import com.google.common.base.Function;
import com.google.common.collect.Sets;

/**
 * CommaSuite Project Generator class used by UI implementations. (i.e.
 * {@link LaunchShortcut and @link GenerationHandler})
 * 
 */

public class ProjectUIGenerator implements IRunnableWithProgress {
	
	private static final HashMap<String, Object[]> taskSelectionCache = new HashMap<String, Object[]>();
	
	private static final Logger LOGGER = Logger.getLogger(ProjectUIGenerator.class.getName());

	protected static final String ERROR_FILE_NOT_FOUND = "Generated {0} was not found.";

	protected static final String ERROR_MESSAGE_TITLE = "CommaSuite generation error";

	protected static final String ERROR_MESSAGE_TEXT = "Errors during generation. Errors are stored in src-gen/errorReport.txt";

	protected static final String ERROR_MESSAGE_REASON = "CommaSuite encounter one or more errors during generation.";
	
	protected static final String ERROR_RESOURCE_TITLE = "CommaSuite workflow terminated";

	protected static final String ERROR_RESOURCE_TEXT = "The CommaSuite workflow was terminated because the file %s still contains errors.";

	protected static final String ERROR_REPORT = "errorReport.txt";

	protected static final String LOG_ERROR_CREATING_REPORT = "Error while deleting error report";

	protected static final String LOG_ERROR_GENERATION = "Error while generating documentation with error report";

	protected final IFile file;
	protected final IProject project;
	protected final Shell shell;
	protected final IGenerator2 generator;
	protected final EclipseResourceFileSystemAccess2 fsa;
	protected final IResourceSetProvider resourceSetProvider;
	protected final EclipseOutputConfigurationProvider outputConfigurationProvider;
	protected final IResourceValidator validator;
	
	public enum LaunchFile {
		MONITOR,
		GENERATORS,
		GENERATORS_TASK_SELECTION,
		SIMULATOR
	}
	
	final private IResource[] javaResource = new IResource[2];
	final private HashSet<IFile> simulators = new HashSet<>();
	final private LaunchShortcut launch;
	final String mode;
	final LaunchFile launchFile;
	
	private final boolean taskSelection;
	
	public ProjectUIGenerator(IFile file, IGenerator2 generator, EclipseResourceFileSystemAccess2 fileAccessProvider,
			IResourceSetProvider resourceSetProvider, EclipseOutputConfigurationProvider outputConfigurationProvider, IResourceValidator validator,
			Shell activeShell, LaunchShortcut launchShortcut, String mode, LaunchFile launchFile, boolean taskSelection) {		
		this.file = file;
		this.project = file.getProject();
		this.shell = activeShell;

		this.outputConfigurationProvider = outputConfigurationProvider;
		this.generator = generator;
		this.fsa = fileAccessProvider;
		this.resourceSetProvider = resourceSetProvider;
		this.validator = validator;
		this.taskSelection = taskSelection;
		
		this.mode = mode;
		this.launchFile = launchFile;
		this.launch = launchShortcut;
	}
	
	public String getMonitorFileName() {
		return OutputLocator.MONITORING_FILENAME;
	}

	public void launch() {
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				try {
					switch (launchFile) {
					case MONITOR:
						launch.launchMonitor(getMonitorJavaFile(), mode);
						break;
					case SIMULATOR:
						simulators.forEach(s -> launch.launchSimulator(s));
						break;
					default:
						break;
					}			
				} catch (FileNotFoundException e) {
					LOGGER.info("Generate then launch: " + e.getMessage());
				}
			}
		});
	}

	@Override
	public void run(IProgressMonitor monitor) {
		List<String> errors = runGeneration(monitor);
		if (shell != null && !errors.isEmpty()) {
			ErrorDialog.openError(shell, ERROR_MESSAGE_TITLE, ERROR_MESSAGE_TEXT, createErrorStatus(errors));
			
		}
		monitor.done();
		
	}
	
	public List<String> runGeneration(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 3);
		Map<String, OutputConfiguration> outputConfigurations = getOutputConfigurations(file.getProject());
		final List<String> errors = new ArrayList<>();
		
		try {
		
			URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
			Resource res = resourceSetProvider.get(project).getResource(uri, true);			
			
			if(containsErrors(res, subMonitor)) {				
				MessageDialog.openError(shell, ERROR_RESOURCE_TITLE , String.format(ERROR_RESOURCE_TEXT, res.getURI().lastSegment()));
				return errors;
			}
			
			refreshOutputFolders(file.getProject(), outputConfigurations, subMonitor.newChild(1));			

			fsa.setProject(project);
			fsa.setMonitor(subMonitor);
			fsa.setOutputConfigurations(outputConfigurations);

			if (!((IteratorExtensions.<EObject>head(res.getAllContents())) instanceof ProjectDescription)) {
				MessageDialog.openInformation(shell, "File not found", "CommaSuite project file was not found.");				
			}
			
			if (subMonitor.isCanceled()) {
				throw new OperationCanceledException();
			}
			
			var generator = (ProjectGenerator) this.generator;
			
			if (this.taskSelection) {
				Display.getDefault().syncExec(new Runnable() {
				    public void run() {
				    	var taskNames = generator.taskNames(res);
						@SuppressWarnings("deprecation")
						ListSelectionDialog dialog = new ListSelectionDialog(shell, taskNames, 
								ArrayContentProvider.getInstance(), new LabelProvider(), "Choose tasks to execute. Tasks required by the chosen tasks will be automatically selected.") {
							@Override
							public void create() {
								super.create();
								CheckboxTableViewer viewer = getViewer();
								viewer.addCheckStateListener(new ICheckStateListener() {
									@Override
									public void checkStateChanged(CheckStateChangedEvent event) {
										Object[] checkedElements = viewer.getCheckedElements();
										var selectedTasks = Arrays.stream(checkedElements).map(t->(String) t).collect(Collectors.toSet());
										for(String t : generator.getTasksClosure(res, selectedTasks)) { //when selection changes select also the transitively required tasks
											viewer.setChecked(t, true);
											if(!selectedTasks.contains(t)) {
												viewer.setGrayed(t, true);
											} else {
												viewer.setGrayed(t, false);
											}
										}
									}
								});
							}
						};
						dialog.setTitle("Run tasks");
						var cacheKey = file.getFullPath().toFile().toString();
						dialog.setInitialSelections(taskSelectionCache.getOrDefault(cacheKey, new Object[] {}));			
						
						dialog.open(); 

						Object[] result = dialog.getResult();
						if (result != null) {
							for (OutputConfiguration config : fsa.getOutputConfigurations().values()) {
								cleanOutputs(project, config, fsa, subMonitor);
							}
							taskSelectionCache.put(cacheKey, result);
							var selectedTasks = Arrays.stream(result).map(t->(String) t).collect(Collectors.toSet());
							errors.addAll(generator.generateWithErrorReport(res, selectedTasks, fsa, new GeneratorContext()));
						}					
				    }
				});
			}
			else {
				for (OutputConfiguration config : fsa.getOutputConfigurations().values()) {
					cleanOutputs(project, config, fsa, subMonitor);
				}
				errors.addAll(generator.generateWithErrorReport(res, fsa, new GeneratorContext()));
			}
			refreshOutputFolders(file.getProject(), outputConfigurations, subMonitor.newChild(1));
		} catch (CoreException e) {
			errors.add(this.getClass().getSimpleName() + e.getMessage() + e.getStackTrace());
		}
		return errors;
	}

	private boolean containsErrors(Resource resource, IProgressMonitor monitor) {
		monitor.beginTask("Validating", 1);
		List<Issue> issues = validator.validate(resource, CheckMode.ALL, getCancelIndicator(monitor));		
		for(Issue issue : issues) {
			if(issue.getSeverity().equals(Severity.ERROR)) {
				return true;
			}
		}
		return false;
	}

	protected void refreshOutputFolders(IProject project, Map<String, OutputConfiguration> outputConfigurations,
			IProgressMonitor monitor) throws CoreException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, outputConfigurations.size());
		simulators.clear();
		for (OutputConfiguration config : outputConfigurations.values()) {
			SubMonitor child = subMonitor.newChild(1);
			for (IContainer container : getOutputs(project, config)) {
				if (monitor.isCanceled()) {
					throw new OperationCanceledException();
				}
				if(container.exists()) {
					sync(container, IResource.DEPTH_INFINITE, child);
					findJavaResource(container);
				}
			}
		}
	}

	private IStatus createErrorStatus(List<String> errors) {
		List<Status> childStatuses = new ArrayList<>();
		for (String error : errors) {
			childStatuses.add(new Status(IStatus.ERROR, ProjectActivator.ORG_ECLIPSE_COMMA_PROJECT_PROJECT, error));
		}

		return new MultiStatus(ProjectActivator.ORG_ECLIPSE_COMMA_PROJECT_PROJECT, IStatus.ERROR,
				childStatuses.toArray(new Status[] {}), ERROR_MESSAGE_REASON, new Throwable(ERROR_MESSAGE_TITLE));
	}

	public boolean cleanOutputs(IProject project, OutputConfiguration config, EclipseResourceFileSystemAccess2 access,
			IProgressMonitor monitor) {

		for (IContainer container : getOutputs(project, config)) {
			if (container.exists()) {
				try {
					for (IResource resource : container.members()) {
						delete(resource, config, access, monitor);
					}
				} catch (CoreException e) {
					return false;
				}
			}
		}
		return true;
	}

	private void delete(IResource resource, OutputConfiguration config, EclipseResourceFileSystemAccess2 access,
			IProgressMonitor monitor) throws CoreException {
		if (monitor.isCanceled()) {
			throw new OperationCanceledException();
		}
		if (resource instanceof IContainer) {
			IContainer container = (IContainer) resource;
			for (IResource child : container.members()) {
				delete(child, config, access, monitor);
			}
			container.delete(IResource.FORCE | IResource.KEEP_HISTORY, monitor);
		} else if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			access.deleteFile(file, config.getName(), monitor);
		} else {
			resource.delete(IResource.FORCE | IResource.KEEP_HISTORY, monitor);
		}
	}

	protected Set<IContainer> getOutputs(IProject project, OutputConfiguration outputConfiguration) {
		Set<IContainer> outputs = Sets.newLinkedHashSet();
		for (String outputPath : outputConfiguration.getOutputDirectories()) {
			IContainer output = getContainer(project, outputPath);
			outputs.add(output);
		}
		return outputs;
	}

	protected Map<String, OutputConfiguration> getOutputConfigurations(IProject project) {
		Set<OutputConfiguration> configurations = outputConfigurationProvider.getOutputConfigurations(project);
		configurations.add(CommaFileSystemAccess.COMMA_OUTPUT_CONF);
		return uniqueIndex(configurations, new Function<OutputConfiguration, String>() {
			@Override
			public String apply(OutputConfiguration from) {
				return from.getName();
			}
		});
		
	}
	
	protected CancelIndicator getCancelIndicator(final IProgressMonitor monitor) {
		return new CancelIndicator() {
			@Override
			public boolean isCanceled() {
				return monitor.isCanceled();
			}
		};
	}
	
	public IFile getMonitorJavaFile() throws FileNotFoundException {
		if (javaResource[0] instanceof IFile) {
			return (IFile) javaResource[0];
		} else {
			throw new FileNotFoundException(MessageFormat.format(ERROR_FILE_NOT_FOUND, getMonitorFileName()));
		}
	}

	
	private void findJavaResource(IContainer container) throws CoreException {
		container.accept(new IResourceProxyVisitor() {
			@Override
			public boolean visit(IResourceProxy proxy) throws CoreException {
				var resource = proxy.requestResource();
				if (proxy.getName().equals(getMonitorFileName())) {
					javaResource[0] = resource;
				} else if (resource instanceof IFile) {
					var file = (IFile) resource;
					var path = file.getFullPath().toString();
					if (path.contains("/src-gen/simulator/") && (path.endsWith(".bat") || path.endsWith(".sh"))) {
						simulators.add(file);
					}
				}
				return true;
			}
		}, IResource.DEPTH_INFINITE);
	}
}
