/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.ui.contentassist

import com.google.inject.Inject
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities
import org.eclipse.comma.project.project.Project
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.jface.viewers.StyledString
import org.eclipse.swt.graphics.Image
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.core.runtime.Platform
import java.util.HashSet
import org.eclipse.comma.behavior.behavior.ProvidedPort

class TaskProposalProvider extends AbstractProjectProposalProvider {

	@Inject IScopeProvider scopeProvider

	final Image templateIcon;
	public static String INAME = "IName"

	final int TEMPLATE_DEFAULT_PRIORITY = 600;
	// private final int TEMPLATE_HIGH_PRIORITY = 700;
	// private final int TEMPLATE_LOW_PRIORITY = 599;
	//
	// ---------============  Tasks  ============--------------
	//
	// UI TEXT
	protected static String GENERATE_MONITORING_INFO = "Task for monitoring"
	protected static String GENERATE_MAPPINGS_INFO = "Type Mappings for generation."
	protected static String GENERATE_MAPPINGS_SIMPLE_INFO = "Type Mappings for generation with Simple Type suggestions"
	protected static String GENERATE_DOCUMENTATION_INFO = "Task for how to generate the documentation"
	protected static String GENERATE_STUB_INFO = "Task for generating Stub and UI CPP code."
	protected static String GENERATE_UML_INFO = "Task for generating Plant UML images."	
	
	protected final String TASK_SIMULATION = '''simulation for interface %s'''
	protected final String TASK_UML = '''umlTask for interface %s'''
	
	protected final String TASK_STUB_INTERFACE = '''
		stubTask for interface %s {
			// exclude command: <Provide command Names that you want to exclude>
			// exclude signal: <Provide signal Names that you want to exclude>
			// exclude notification: <Provide notification names that you want to exclude>
			latency: 10 seconds // Polling delay of the simulation | stub behavior
			parameters file: "file.params" // Provide the correct name of the input parameters file
			
			auto-launch-console: NO // Build and launch of the console simulation on Execute CommaSuite Workflow?
			auto-launch-gui: NO // Build and launch of the GUI simulation on Execute CommaSuite Workflow?
			
			// The following attribute is OPTIONAL. 
			// It is used to specify a different location for the BAT File: vcvarsall.bat [located in your Visual Studio Installation]
			// If not specified then the default path is assumed, i.e. "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Auxiliary\\Build\\"
			// msvc-install-path: "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Auxiliary\\Build\\"
			
			overwrite-server-stub-helper: YES // Overwrite the helper file generated by the server stub?
			overwrite-client-stub-adapter: YES // Overwrite the generated client stub adapter implementation?
		}
	'''

	protected final String TASK_MONITOR = '''
		monitoring for interface %s {
			trace files
			"file.events"
		}'''
	
	protected final String TASK_TYPE_MAPPINGS = '''
		typeMappings {
			
		}'''
	
	protected final String COMPOUND_INTERFACE = '''
		ICompound {
			version
			"0.1"
			
			description
			"Description"
			
			interfaces
			%s
		}
	'''
	
	protected final String TASK_INPUT_TEMPLATE = '''
		inputTemplateTask for interface %s
	'''
    
    protected final String TASK_TEST_APPLICATION_GEN_INFO = '''Task for generating a test application'''
    protected final String TASK_TEST_APPLICATION_GEN = '''
        TestApplication_%s for %s %s
        {
            params: %s
        }
    '''
    
    protected final String TASK_SIM_GEN_INFO = '''Task for generating a Simulator'''
    protected final String TASK_SIM_GEN = '''
        Simulator_%s for %s %s
        {
            params: %s
        }
    '''
    
    protected final String TASK_TEST_CASES_GEN_INFO = '''Task for generating test cases'''
    protected final String TASK_TEST_CASES_GEN = '''
        TestCases_%s for interface %s {
            template: java
            output-file: 'Test.java'
            max-depth: 1000
            params: '%s'
        }
    '''
    
    protected final String TASK_MODEL_QUALITY_CHECKS_GEN_INFO = '''Task for generating model quality checks'''
    protected final String TASK_MODEL_QUALITY_CHECKS_GEN = '''
        ModelQualityChecks_%s for interface %s {
            params: '%s.params'
            max-depth: 1000
            home-states: { %s }
        }
    '''
		

	/********************* Tasks **********************/
	/* Monitoring */
	override complete_MonitoringTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_MonitoringTask(model, ruleCall, context, acceptor)

		acceptMonitoringTask(INAME, context, acceptor)
		model.getInterfaces.forEach[acceptMonitoringTask(it.name, context, acceptor)]
	}

	def acceptMonitoringTask(String name, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val proposalSimple = String.format(TASK_MONITOR, name)
		acceptor.accept(createTemplate("Monitoring Task " + name, proposalSimple, GENERATE_MONITORING_INFO, 2, context))
	}

	/* Documentation */
	override complete_DocumentationGenerationTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		val proposal = String.format(documentationTask, INAME)
		acceptor.accept(createTemplate("Documentation Task Example", proposal, GENERATE_DOCUMENTATION_INFO, 1, context))
		model.interfaces.forEach[acceptDocumenationTask(it.name, context, acceptor)]
	}

	def acceptDocumenationTask(String iName, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val proposal = String.format(documentationTask, iName)
		acceptor.accept(createTemplate("Documentation Task for interface " + iName, proposal, "", 1, context))
	}
	
	/* UML */
	override complete_UMLTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		acceptUMLTask(INAME, context, acceptor)
		model.interfaces.forEach[acceptUMLTask(it.name, context, acceptor)]
	}
	
	def acceptUMLTask(String name, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val proposalSimple = String.format(TASK_UML, name)
		acceptor.accept(
			createTemplate("UML Task " + name, proposalSimple, GENERATE_UML_INFO, 2, context))
	}
	
	def acceptCPPStubAndUITask(String name, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val proposalSimple = String.format(TASK_STUB_INTERFACE, name)
		acceptor.accept(
			createTemplate("Stub and UI Task " + name, proposalSimple, GENERATE_STUB_INFO, 2, context))
	}
	
	/* Compound interface */
	override complete_CompoundInterface(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.complete_CompoundInterface(model, ruleCall, context, acceptor)
		
		getInterfaces(model).forEach[acceptCompoundInterface(it.name, context, acceptor)]		
	}
	
	def acceptCompoundInterface(String iName, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val proposal = String.format(COMPOUND_INTERFACE, iName)
		acceptor.accept(createTemplate("Compound Interface with interface" + iName, proposal, "", 2, context))
	}
	
	/* Model Quality Checks generation */
	override complete_ModelQualityChecksGenerationTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
        ICompletionProposalAcceptor acceptor) {
        super.complete_ModelQualityChecksGenerationTask(model, ruleCall, context, acceptor)
        val text = complete_ModelQualityChecksGenerationTaskBody(model)
        acceptor.accept(createTemplate("Model Quality Checks generation Task", text, TASK_MODEL_QUALITY_CHECKS_GEN_INFO, 2, context))
    }
    
    def complete_ModelQualityChecksGenerationTaskBody(EObject model) {
    	var text = '''
    	«FOR itf : getInterfaces(model)»
        «String.format(TASK_MODEL_QUALITY_CHECKS_GEN, itf.name, itf.name, itf.name, String.join(" ", itf.machines.map[m | m.states.findFirst[s | s.initial].name]))»
        «ENDFOR»
    	'''
    	
    	if (text.blank) {
    		text = String.format(TASK_MODEL_QUALITY_CHECKS_GEN, INAME, INAME, INAME, "InitialState")
    	}
    	text
    }
	
	/* Test cases generation */
	override complete_TestCasesGenerationTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
        ICompletionProposalAcceptor acceptor) {
        super.complete_TestCasesGenerationTask(model, ruleCall, context, acceptor)
        val text = complete_TestCasesGenerationTaskBody(model)
        acceptor.accept(createTemplate("Test cases generation Task", text, TASK_TEST_CASES_GEN_INFO, 2, context))
    }
    
    def complete_TestCasesGenerationTaskBody(EObject model) {
    	var text = '''
    	«FOR itf : getInterfaces(model)»
        «String.format(TASK_TEST_CASES_GEN, itf.name, itf.name, itf.name + ".params")»
        «ENDFOR»
    	'''
    	
    	if (text.blank) {
    		text = String.format(TASK_TEST_CASES_GEN, INAME, INAME)
    	}
    	text
    }
    
    /* Test application */
    override complete_TestApplicationGenerationTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
        ICompletionProposalAcceptor acceptor) {
        super.complete_TestApplicationGenerationTask(model, ruleCall, context, acceptor)
        val text = complete_TestApplicationGenerationTaskBody(model)
        acceptor.accept(createTemplate("Test application generation Task", text, TASK_TEST_APPLICATION_GEN_INFO, 2, context))
    }
    
    def complete_TestApplicationGenerationTaskBody(EObject model) {
        var text = '''
        «FOR itf : getInterfaces(model)»
        «String.format(TASK_TEST_APPLICATION_GEN, itf.name, "interface", itf.name, '''"«itf.name»/«itf.name».params"''')»
        «ENDFOR»
        «FOR comp : getComponents(model)»
        «String.format(TASK_TEST_APPLICATION_GEN, comp.name, "component", comp.name, 
            new HashSet(comp.ports
                .filter[p | p instanceof ProvidedPort]
                .map[p | '''"«p.interface.name»/«p.interface.name».params"'''.toString.trim].toList
            ).join(" ")
        )»
        «ENDFOR»
        '''
        
        if (text.blank) {
            text = String.format(TASK_TEST_APPLICATION_GEN, INAME, INAME, INAME, INAME)
        }
        text
    }
    
    /* Simulator */
    override complete_SimulatorGenerationTask(EObject model, RuleCall ruleCall, ContentAssistContext context,
        ICompletionProposalAcceptor acceptor) {
        super.complete_SimulatorGenerationTask(model, ruleCall, context, acceptor)
        val text = complete_SimulatorGenerationTaskBody(model)
        acceptor.accept(createTemplate("Simulator generation Task", text, TASK_SIM_GEN_INFO, 2, context))
    }
    
    def complete_SimulatorGenerationTaskBody(EObject model) {
    	var text = '''
    	«FOR itf : getInterfaces(model)»
        «String.format(TASK_SIM_GEN, itf.name, "interface", itf.name, '''"«itf.name»/«itf.name».params"''')»
        «ENDFOR»
    	«FOR comp : getComponents(model)»
        «String.format(TASK_SIM_GEN, comp.name, "component", comp.name, 
        	new HashSet(comp.ports
        		.filter[p | p instanceof ProvidedPort]
        		.map[p | '''"«p.interface.name»/«p.interface.name».params"'''.toString.trim].toList
        	).join(" ")
        )»
        «ENDFOR»
    	'''
    	
    	if (text.blank) {
    		text = String.format(TASK_SIM_GEN, INAME, INAME, INAME, INAME)
    	}
    	text
    }
    

	// ---------============  Helpers  ============--------------
	new() {
		templateIcon = ImageDescriptor.createFromURL(Platform.getBundle("org.eclipse.comma.icons").getResource("icons/template.png")).
			createImage();
	}

	protected def createTemplate(String name, String content, String additionalInfo, Integer nrIndents,
		ContentAssistContext context) {
		createTemplate(name, content, additionalInfo, nrIndents, context, TEMPLATE_DEFAULT_PRIORITY)
	}

	protected def addIndents(String content, Integer nrIndents, boolean newLine) {
		var indent = "";
		for (var i = 0; i < nrIndents; i++) {
			indent += "\t";
		}
		val indentedContent = content.replace(System.lineSeparator, System.lineSeparator + indent)
		if (newLine) {
			return System.lineSeparator + indent + indentedContent
		}
		indent + indentedContent
	}

	private def createTemplate(String name, String content, String additionalInfo, Integer nrIndents,
		ContentAssistContext context, int priority) {
		var indentedContent = addIndents(content, nrIndents, true)
		
		while (indentedContent.startsWith("\n") || indentedContent.startsWith("\r") ||
			indentedContent.startsWith("\t")) {
			indentedContent = indentedContent.substring(1);
		}

		var finalAdditionalInfo = content
		val proposal = createHtmlCompletionProposal(indentedContent, new StyledString(name), templateIcon,
			TEMPLATE_DEFAULT_PRIORITY, context);

		if (proposal instanceof ConfigurableCompletionProposal) {
			finalAdditionalInfo = "<html><body bgcolor=\"#FFFFE1\"><style> body { font-size:9pt; font-family:'Segoe UI' }</style><pre>" +
				finalAdditionalInfo + "</pre>";
			if (additionalInfo !== null) {
				finalAdditionalInfo = finalAdditionalInfo + "<p>" + additionalInfo + "</p>";
			}
			finalAdditionalInfo = finalAdditionalInfo + "</body></html>"
			proposal.additionalProposalInfo = finalAdditionalInfo
			proposal.proposalContextResource = context.resource
			proposal.priority = priority
		}
		proposal
	}

	private def createHtmlCompletionProposal(String proposal, StyledString displayString, Image image, int priority,
		ContentAssistContext context) {
		if (isValidProposal(proposal, context.getPrefix(), context)) {
			return doCreateHtmlCompletionProposal(proposal, displayString, image, priority, context);
		}
		return null;
	}

	private def doCreateHtmlCompletionProposal(String proposal, StyledString displayString, Image image, int priority,
		ContentAssistContext context) {
		val replacementOffset = context.getReplaceRegion().getOffset();
		val replacementLength = context.getReplaceRegion().getLength();
		val result = new HtmlConfigurableCompletionProposal(proposal, replacementOffset, replacementLength,
			proposal.length(), image, displayString, null, null);

		result.priority = priority
		result.matcher = context.matcher
		result.replaceContextLength = context.replaceContextLength
		result;
	}

	def containsBlock(EObject model, EClass blockClass) {
		if (model instanceof Project) {
			for (block : model.generatorBlocks) {
				if (block.eClass === blockClass) {
					return true
				}
			}
		}
		return false
	}

	def getDocumentationTask() {
		""
	}
	
	def getInterfaces(EObject model) {
		ComponentUtilities::getAllInterfaces(model, scopeProvider)
	}
	
	def getComponents(EObject model) {
		ComponentUtilities::getAllComponents(model, scopeProvider)
	}
}
