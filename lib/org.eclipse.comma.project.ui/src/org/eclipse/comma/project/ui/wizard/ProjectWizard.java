/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.project.ui.wizard;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResourceStatus;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

public abstract class ProjectWizard extends Wizard implements INewWizard {

	private static final String WIZARD_NAME = "New CommaSuite project";
	private static final Logger LOGGER = Logger.getLogger(ProjectWizard.class.getName());
	private static final String XTEXT_NATURE = "org.eclipse.xtext.ui.shared.xtextNature";
	
	private WizardNewProjectCreationPage pageOne;	

	public ProjectWizard() {
		super();
		setWindowTitle(WIZARD_NAME);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		//Parameters not required
	}

	@Override
	public boolean performFinish() {

		String name = pageOne.getProjectName();
		URI location = null;
		if (!pageOne.useDefaults()) {
			location = pageOne.getLocationURI();
		}

		try {
			createProject(name, location, new NullProgressMonitor());
		} catch (CoreException e) {
			if (e.getStatus().getCode() == IResourceStatus.CASE_VARIANT_EXISTS) {
				pageOne.setErrorMessage(
						"A project with that name but different capitalization already exists in the workspace.");
			}
			LOGGER.log(Level.WARNING, "Error trying to create project.", e);
			return false;
		}
		return true;
	}

	@Override
	public void addPages() {
		super.addPages();
		pageOne = new WizardNewProjectCreationPage(WIZARD_NAME);
		pageOne.setTitle(WIZARD_NAME);
		pageOne.setDescription("This wizard creates a new CommaSuite project");
		addPage(pageOne);
	}

	public IProject createProject(String projectName, URI location, IProgressMonitor monitor)
			throws CoreException {
		Assert.isNotNull(projectName);
		Assert.isTrue(projectName.trim().length() > 0);
		IProject project = createBaseProject(projectName, location, monitor);
		addProjectStructure(project);
		addNatures(project, monitor);
		return project;
	}

	/**
	 * Just do the basics: create a basic project.
	 * 
	 * @param location
	 * @param projectName
	 * @throws CoreException
	 */
	private static IProject createBaseProject(String projectName, URI location, IProgressMonitor monitor)
			throws CoreException {
		// it is acceptable to use the ResourcesPlugin class
		IProject newProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);

		if (!newProject.exists()) {
			URI projectLocation = location;
			IProjectDescription desc = newProject.getWorkspace().newProjectDescription(newProject.getName());
			if (location != null && ResourcesPlugin.getWorkspace().getRoot().getLocationURI().equals(location)) {
				projectLocation = null;
			}

			desc.setLocationURI(projectLocation);

			newProject.create(desc, monitor);
			if (!newProject.isOpen()) {
				newProject.open(monitor);
			}
		}

		return newProject;
	}

	/**
	 * Create a folder structure with a parent root, overlay, and a few child
	 * folders.
	 * 
	 * @param newProject
	 * @param paths
	 * @throws CoreException
	 */
	private void addProjectStructure(IProject newProject) throws CoreException {
		createFile(newProject.getFile("Example.prj"));
		createFile(newProject.getFile("Example.signature"));
		createFile(newProject.getFile("Example.interface"));
		createFile(newProject.getFile("Template.docx"));
	}

	private void createFile(IFile file) throws CoreException {
		URL url;
		InputStream inputStream = null;
		try {
			url = new URL(getTemplateLocation() + file.getName());
			inputStream = url.openConnection().getInputStream();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Error trying to create File.", e);
		}
		file.create(inputStream, true, null);
	}
 
	protected abstract String getTemplateLocation();
	
	public static void addNatures(IProject project, IProgressMonitor monitor) throws CoreException {
		if (!project.hasNature(XTEXT_NATURE)) {
			IProjectDescription description = project.getDescription();
			String[] prevNatures = description.getNatureIds();
			String[] newNatures = new String[prevNatures.length + 1];
			System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
			newNatures[prevNatures.length] = XTEXT_NATURE;
			description.setNatureIds(newNatures);
			project.setDescription(description, monitor);
		}
	}

}
