/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.ui.tests;

import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.xtext.ui.testing.ContentAssistProcessorTestBuilder;
import org.eclipse.xtext.ui.testing.util.LineDelimiters;
import org.eclipse.xtext.ui.testing.util.ResourceLoadHelper;

import com.google.inject.Injector;

public class InterfacesContentAssistBuilder extends ContentAssistProcessorTestBuilder {

	public InterfacesContentAssistBuilder(Injector injector, ResourceLoadHelper helper) throws Exception {
		super(injector, helper);		
	}
	
	/**
	 * Look for the proposal based on the name
	 */
	@Override
	protected ICompletionProposal findProposal(String proposalString, ICompletionProposal[] proposals) {
		if (proposalString != null) {
			String platformDelimitedProposal = LineDelimiters.toPlatform(proposalString);
			for (ICompletionProposal candidate : proposals) {
				if (platformDelimitedProposal.equals(candidate.getDisplayString())) {
					return candidate;
				}
			}
		}
		return proposals[0];
	}

}
