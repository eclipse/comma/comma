/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.ui.tests

import com.google.inject.Inject
import com.google.inject.Injector
import org.eclipse.comma.signature.ui.contentassist.InterfaceSignatureProposalProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.ui.testing.AbstractContentAssistTest
import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions

@ExtendWith(InjectionExtension)  
@InjectWith(InterfaceSignatureUiInjectorProvider)

class ContentAssistTest extends AbstractContentAssistTest {	
	
	@Inject Injector injector;

	override protected newBuilder() throws Exception {
		return new InterfacesContentAssistBuilder(this.injector, this);
	}
	
	def void assertBuilder(String setup, String proposal, Integer location, String expected) {
	    var builder = newBuilder;
	    if (setup !== null) {
	        builder = builder.append(setup.replaceAll("\\r\\n", "\n"))
	    }
	    
	    if (location !== null) {
	        builder = builder.applyProposal(location, proposal)
	    } else {
	        builder = builder.applyProposal(proposal)
	    }
	    
	    var actual = builder.toString
	    actual = actual.substring(0, actual.indexOf("length: "));
	    Assertions.assertEquals(expected.replaceAll("\\r\\n", "\n").trim, actual.replaceAll("\\r\\n", "\n").trim);
	}
			
	@Test def void testTemplateStatemachine() {
		
		val expected = '''
		
		signature Iinterface
			
			commands
			int CommandIn (in int x)
			
			signals
			Signal(int x)
			
			notifications
			Notification(int x)
		'''
		
		assertBuilder(null, InterfaceSignatureProposalProvider.INTERFACE_TITLE, null, expected)
	}
	
	@Test def void testTemplateType() {
		val setup = '''		
		signature Iinterface 
			
			types
		'''
		
		val expected = '''		
		signature Iinterface 
			
			types
		
			type newType
		'''
		
		assertBuilder(setup, InterfaceSignatureProposalProvider.TYPE_TITLE, 31, expected)
	}
	
	@Test def void testTemplateEnum() {
		val setup = '''		
		signature Iinterface 
			
			types
			
		'''
		
		val expected = '''		
		signature Iinterface 
			
			types
		
			enum Enum {
				First
			}
				
		'''
		
		assertBuilder(setup, InterfaceSignatureProposalProvider.ENUM_TITLE, 31, expected)		
	}
	
	@Test def void testTemplateRecord() {
		val setup = '''		
		signature Iinterface 
			
			types
			
		'''
		
		val expected = '''		
		signature Iinterface 
			
			types
		
			record Record {
				int key, 
				int value
			}
				
		'''
		
		assertBuilder(setup, InterfaceSignatureProposalProvider.RECORD_TITLE, 31, expected)
	}
	
	@Test def void testTemplateNotification() {
		val setup = '''		
		signature Iinterface
			
			notifications
			
		'''
		
		val expected = '''		
		signature Iinterface
			
			notifications
			
			Notification(int x)
		'''
		
		assertBuilder(setup, InterfaceSignatureProposalProvider.NOTIFICATION_TITLE, 39, expected)
	}
	
	@Test def void testTemplateCommand() {
		val setup = '''		
		signature Iinterface
			
			commands
			
		'''
		
		val expected = '''		
		signature Iinterface
			
			commands
			
			int Command
		'''
		
		assertBuilder(setup, InterfaceSignatureProposalProvider.COMMAND_TITLE, 34, expected)
	}
}
