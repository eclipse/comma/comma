= Activity 11: Check the specified interfaces by monitoring

[.normal]

* Extend the project file _VendingMachine.prj_ with an import of _IUser.interface_. 
Add a monitoring task for the IUser interface.
* Use the GUI to generate .event files using commands from all interfaces.
* Apply monitoring to check all .event files. 
Inspect the coverage, the monitoring results and improve models when needed.
Note that coverage is recorded at the level of transitions, details inside a transition (for instance, when there is an if-then-else statement) are not visible.