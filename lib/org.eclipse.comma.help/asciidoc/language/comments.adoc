= Using Comments in CommaSuite Models
:lang: en

[.normal]

Multiline and single line comments can be inserted in the CommaSuite models. Some of the comments will be included in the generated documentation and code.

. Multiline comments will be included in generated documentation and code if they are placed _before_ the following constructs:
* Type declarations
* Interface event declarations (commands, signals, and notifications)
* Variable declarations and initializations in behavioral models
* Interface definitions
* State machines
* States
* Transitions
* Time and data constraints
+
The following is an example of a multiline comment placed before a type declaration:
+
....
/*
 * Type for personal information
 */

record Person {
    string name
    string address
}
....
. Parameters and return values of interface events can be documented by using two Doxygen tags placed in a multiline comment:
+
....
/*
 * Simulates the occurrence of an internal event
 * \param ev the kind of the event, e.g. internal error, timeout, etc.
 * \return result that indicates if the execution of the command will have an effect
 */

commands
Status InjectEvent(EventKind ev)
....
. Record fields and enumeration literals can be documented by arranging them on separate lines and placing a single line comment _after_ them:
+
....
/*
 * Type for personal information
 */

record Person {
    string name //Person name
    string address //Person address
}

enum Status {
    OK //Successful execution
    Failed //Execution failed
}
....
