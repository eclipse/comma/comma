= Parameter specification
:lang: en

[.normal]

A number of generation tasks (e.g. link:../generators/simulator.html[simulator], link:../generators/modelqualitychecks.html[model quality checks], or link:../generators/testapplication.html[test application]) require the specification
of values for input parameters of commands and signals, and in some cases, the parameter values of replies and notifications.
Values for these parameters are provided for a given interface in a file with extension `.params`.

For a given interface, the following parameters have to be provided:

* parameters for commands and signals
* parameters for replies and notifications with variables
* parameters for replies and notifications that contain the _any value_ expression (*)

In order to create a parameters file, follow the steps:

* Next to a `INTERFACE_NAME.interface` file, create a file called `INTERFACE_NAME.params`.
* Insert, as the first line of the file, an import of the interface (i.e., `import INTERFACE_NAME.interface`).
* The parameters have to be given in the following order: triggers (commands and signals), notifications, replies to commands. Open the content assist (ctrl + space) and select one of "Autocomplete missing triggers",
 "Autocomplete missing notifications", and "Autocomplete missing replies". Do this for all required events.
* Fill in the desired parameters and the `.params` file is complete.

An example of a `.params` file:

[source,json]
----
// Imports
import "Camera.interface"

// The interface we want to provide parameters for
interface: Camera

//optional definition of variables
variables
int i

//optional initialization of variables
init
i := 5

// The trigger (command or signal)
trigger: SetZoom
// The state we are providing parameters for
state: PoweredOn
// The provided parameters, note that multiple parameters sets can be provided. Variables can be used as parameter values
params: ( 3, 9 )
params: ( 11, i )
// Next to SetZoom in the PoweredOn state, we also provided parameters for SetZoom in the PoweredOff state.
state: PoweredOff
params: ( 21, 5 )

trigger: GetCapacity
state: PoweredOn
params: ( CapacityType::Total )
params: ( CapacityType::Remaining )

// Similarly to triggers (commands and signals), parameters can be given for notifications if in the interface definition they are used in actions with variables or with expression '*' 
notification: BatteryLevel
state: PoweredOn
params: (35)

// Parameters for replies can be given if they are used in actions with variables or with expression '*'
reply to: GetCapacity
state: PoweredOn
params: (120)
----
== The usage of parameter files

The usage of the defined parameter values depends on the scenario (or generator).
For example, a simulator uses all the supplied parameters while a test application for a component uses information that depends on the role an interface plays
(in a provided versus required port). Regardless the scenario, the parameters file has to be complete and has to provide values for all relevant events.

The table below shows which parts of the parameters files are used for generating simulators and test application for interface or component respectively.

[cols="1,1,1"]
|===
| 
|Simulator
|Test Application
|Interface
a|
 * Commands and Signals
 * Replies and notifications with variables
 * Replies and notifications with Any (*) value
a|
  * Commands and Signals
 |Component, provided port
 a|
 * Commands and Signals
 * Replies and notifications with variables
 * Replies and notifications with Any (*) value
a|
  * Commands and Signals
|Component, required port
 a|
 * Commands and Signals
 * Replies and notifications with variables
 * Replies and notifications with Any (*) value
a|
  * Replies and notifications with variables
  * Replies and notifications with Any (*) value
|===

== Skip

When simulating or testing a component some transitions can be skipped using the `skip` keyword.
The `skip` statement can be applied to parameter values and to states of commands, signals of notifications.
A skip of a reply statement is not allowed, only its parameter values can be skipped.

An example of a `.params` file using `skip`:

[source,json]
----
import "Camera.interface"

trigger: SetZoom
// Skip the transition with this trigger in state PoweredOn
state: PoweredOn skip

trigger: GetCapacity
state: PoweredOn
params: ( CapacityType::Total )
// Skip the transition with parameter value CapacityType::Remaining
params: ( CapacityType::Remaining ) skip

reply to: GetCapacity
state: PoweredOn
params: (110)
// Skip the reply transition with parameter value 120
params: (120) skip

notification: BatteryLevel
// Skip the notification transition in state PoweredOn
state: PoweredOn skip
----

A few notes:

* No parameters should be specified for skipped actions.
* Skip is only effective for actions that are under control of the test application, so

** commands and signals of provided interfaces and 
** notifications and reply values of required interfaces.

* Also actions without parameters can be skipped; they first have to be added to the parameters file.


== Break

For the test application, a `break` statement can be added to states of actions, including reply statements. 
It can not be added to parameter values. 

== Import of parameter file with data

When long and complex data values are needed, it might be convenient to define them in a separate file. 
This can be done as follows. 
First define the data values in a parameter file with last line *"variables only instance"* to supress a number of validation rules.
As a example consider the following file "Imported.params":
....
import "Example.interface"
interface: Example

variables
Enum bla
string data

init
bla := Enum::Second
data := "long string with information"

variables only instance
....

Next this file can be imported in another parameters file, as follows:

....
import "Example.interface"
import "Imported.params"

interface: Example

trigger: start
    state: initialState 
        params: ( data )

trigger: cmd 
    state: On
        params: ( Enum::First )
        params: ( bla )
....
