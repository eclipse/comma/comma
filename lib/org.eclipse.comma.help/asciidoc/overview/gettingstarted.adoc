= Getting Started
:lang: en

[.normal]

The CommaSuite language allows specifying the signature and behaviour of interfaces, and components that use multiple interfaces. 
The typical steps are:

. Define the interface signature model.
+
The interface signature (definitions of commands, signals and notifications) is specified in a single model stored in a file with extension _.signature_. 
The user can define interface-specific types and can import types from type definition models (files with extension _.types_)
. Define the interface behaviour model.
+
Interface behaviour is defined in a single model stored in a file with extension _.interface_. This model uses the already defined interface signature model.
. Optionally, define components that use several interfaces.
+
Components are abstractions that provide a context to define how a group of interfaces work together.
. Define the desired generation tasks.
+
Generation tasks are defined in project files (files with extension _.prj_). 
A project typically imports a number of interfaces and components. 
Generation tasks may depend on each other, for example, 
one task produces artifacts used as an input for another task.


A new CommaSuite project can be easily created with the provided wizard. 
Select from menu _File/New/Project..._ and then choose _CommaSuite Project_. 
Give a name to the project.

//image:../img/CommaProjectWizard.png[image,scaledwidth=60.0%]

The wizard automatically creates interface signature, interface behaviour and project models with default minimal content.

//Consult the user guide sections for the syntax of the link:../language/language.html[CommaSuite language] and the supported generation tasks.

There are several illustrative examples on the use of the language. 
Examples can be loaded from menu _File/New/Example..._ and then 
selecting section _CommaSuite Examples_

Hands-on experience with the tooling can be obtained 
by the link:../tutorial/intro.html[tutorial].

== Editors

The CommaSuite text editors provide the following content assistance functionality.

* Auto completion and code templates +
    The key combination <CTRL>-<Space> shows the next possible syntax token in a given context. 
    In addition, predefined code templates can be chosen from a drop-down context menu. 
    The following screen shows a list of available code templates for commands in an interface signature model and the content of a template for a simple command with one input parameter: +
    image:../img/Templates.png[image]
* Quick fixes +
    In case of a syntax error, quick fixes may be suggested after hovering over the underlined error. +
    image:../img/QuickFix.png[image]
* Auto formatting +
    The key combination <CTRL>-<Shift>-<F> automatically formats the code being edited.
