= Publications

== Scientific Publications

* https://doi.org/10.1016/j.scico.2023.103067[Model Based Component Development and Analysis with ComMA] Science of Computer Programming, Special issue in Success Stories in Model-Driven Engineering, Volume 233, March, 2024.

* http://www.cs.ru.nl/J.Hooman/FestschriftFrits2022.html[Runtime Verification of Compound Components with ComMA] A Journey from Process Algebra via Timed Automata to Model Learning, LNCS 13560, pages 382-402, 2022.

* http://www.cs.ru.nl/J.Hooman/ReverseEngineeringModelsOfSoftwareInterfaces2021.html[Reverse Engineering Models of Software Interfaces], Computer Science and Information Systems, pages 657-686, 2021.

* http://www.cs.ru.nl/J.Hooman/Modelsward2017.html[Integrating Interface Modeling and Analysis in an Industrial Setting] Proc. 5th Int. Conf. on Model-Driven Engineering and Software Development (MODELSWARD 2017), pages 345-352, 2017
                      
* http://www.cs.ru.nl/J.Hooman/FestschriftEd2017.html[Runtime Monitoring based on Interface Specifications] ModelEd, TestEd, TrustEd, LNCS 10500, pages 335-356, 2017
 
== Other Publications

* https://resolver.tno.nl/uuid:28311baf-1c1d-4c86-a5b5-7732b20f9609[Modeling and analyzing hardware-software interfaces with ComMA] Jozef Hooman, Wouter Tabingh-Suermondt, TNO Report R10720, 2024

* https://esi.nl/news/blogs/the-dynamics-approach[Model-based Specification, Verification, and Adaptation of Software Interfaces] The DYNAMICS Approach, Benny Akesson

* https://bits-chips.nl/artikel/comma-interfaces-open-the-door-to-reliable-high-tech-systems/[ComMA interfaces open the door to reliable high-tech systems] Bits & Chips, September 2020
                     
* https://www.bits-chips.nl/artikel/improving-interface-specifications-with-comma/[Improving interface specifications with ComMA] Bits & Chips, September 2017
                         
* https://www.codemotion.com/magazine/backend/languages/domain-specific-languages-could-scale-up-your-code-with-comma/[Domain-specific languages could scale up your code with ComMA] Article by Philips Healthcare in Codemotion Magazine 
                      

              
