# org.eclipse.comma.help
This project contains the user guide and site in Asciidoc format.
These files can be found under the `asciidoc` directory and are rendered as `.html` to the `asciidoc-gen` folder.
To generate both the site and Eclipse help (user guide) execute `GenerateHelp.launch` from `org.eclipse.comma.launch`.
Once finished `asciidoc-gen` will contain the `site` and `eclipse-help`.

In the `asciidoc` folder; `index.adoc`, `user_guide.adoc`, `favicon.png`, `docinfo.html`, `docinfo-footer.html` and everything under `site/*` is only meant for the site.
The rest will be shown on both the site and Eclipse help (since the site also contains the Eclipse help).

## Adding a new page
- To add a new page to the Eclipse help (user guide), create a new Asciidoc file in a folder of the `asciidoc` folder and add it to the `toc.xml`.
- To add a new page to the site, create a new Asciidoc file in the `asciidoc/site` folder and add it to the `toc` variable in `toc.rb`.
Note that any added pages to the `toc.xml` (user guide) will automatically be added to the site.

## Site updates
The site, except the user guide on the site, is automatically updated on every commit to the `main` branch.
The user guide on the site is only updated on a new release (but not a `.RC` or `.M` release!).
