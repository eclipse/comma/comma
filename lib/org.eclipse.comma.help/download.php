<?php
/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/**
 * This php script is deployed on https://download.eclipse.org/comma/ in the Jenkinsfile
 * It provides a static url to each release via a mirror.
 * e.g. :
 * - http://download.eclipse.org/comma/download.php?type=product&version=latest&os=win32&direct
 * - http://download.eclipse.org/comma/download.php?type=cli&version=v0.1.0
 */

$baseURL = "http://www.eclipse.org/downloads/download.php?file=/comma/";
$version = $_GET["version"];
$type = $_GET["type"];
$os = $_GET["os"];
$direct = isset($_GET['direct']);
$path = $version . '/' . $type . '/';
$files = array_values(array_filter(scandir($path), function($k) use ($os) {
    if ($k == "." || $k == "..") return false;
    if ($os != null && strpos($k, $os) == false) return false;
    return true;
}));

if (count($files) == 1) {
    $url = $baseURL . $path . $files[0];
    if ($direct) $url = $url . '&r=1';
    header('Location: ' . $url);
} else {
    echo "Error: zero or multiple results";
}
?>