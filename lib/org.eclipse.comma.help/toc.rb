#
# Copyright (c) 2021 Contributors to the Eclipse Foundation
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#

require 'rexml/document'
require 'pathname'
include REXML

class MyHtml5Converter < (Asciidoctor::Converter.for 'html5')
  register_for 'html5'

  def render_toc(items, file, depth)
    file_path = Pathname.new file
    result = "<ul class=\"sectlevel#{depth}\">"
    items.each { |item|
      item_path = Pathname.new item[:file]
      relative_path = item_path.relative_path_from file_path.dirname
      style = item[:file] == file ? "font-weight: bold" : ""
      result += "<li><a style=\"#{style}\" href=\"javascript:navigatePage('#{relative_path}')\">#{item[:name]}</a>"
      if item.key?(:children)
        result += render_toc(item[:children], file, depth + 1)
      end
      result += "</li>"
    }
    result += "</ul>"
    result
  end

  def populate_user_guide(xml_entry, children)
    xml_entry.elements.each("topic") { |e|
      item = {name: e.attributes["label"], file: e.attributes["href"].split("/eclipse-help")[1], children: []}
      populate_user_guide(e, item[:children])
      children << item
    }
  end

  def convert_outline(node, opts = {})
    file = node.attributes['outfile'].split("/asciidoc-gen/site")[1]
    toc = [
      {name: "Home", file: "/index.html"},
      {name: "Example", file: "/site/example.html"},
      {name: "Download", file: "/site/download.html"},
      {name: "Developers", file: "/site/developers.html"},
      {name: "Publications", file: "/site/publications.html"},
    ]

    # Create user guide based on toc.xml
    user_guide = {name: "User guide", file: "/user_guide.html", children: []}
    tocfile = File.new("../toc.xml")
    tocdoc = Document.new(tocfile)
    if (file != "/index.html" and !file.start_with? "/site") or file == "/site/user_guide.html"
      populate_user_guide(tocdoc.root, user_guide[:children])
    end
    toc << user_guide

    render_toc(toc, file, 1)
  end
end
