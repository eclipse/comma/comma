/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.actions.utilities

import org.eclipse.comma.actions.actions.ActionList
import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.comma.actions.actions.CommandReplyWithVars
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.InterfaceEventInstance
import org.eclipse.comma.actions.actions.Multiplicity
import org.eclipse.comma.actions.actions.PCFragmentReference
import org.eclipse.comma.actions.actions.ParallelComposition
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction
import org.eclipse.comma.expressions.utilities.ExpressionsComparator

import static extension org.eclipse.comma.actions.utilities.ActionsUtilities.*

class ActionsComparator extends ExpressionsComparator{
	
	def dispatch boolean compare(AssignmentAction act1, AssignmentAction act2){
		act1.assignment.sameAs(act2.assignment) && act1.exp.sameAs(act2.exp)
	}
	
	def dispatch boolean compare(IfAction act1, IfAction act2){
		act1.guard.sameAs(act2.guard) &&
		act1.thenList.sameAs(act2.thenList) &&
		act1.elseList.sameAs(act2.elseList)
	}
	
	def dispatch boolean compare(RecordFieldAssignmentAction act1, RecordFieldAssignmentAction act2){
		act1.fieldAccess.sameAs(act2.fieldAccess) && act1.exp.sameAs(act2.exp)
	}
	
	def dispatch boolean compare(CommandReply act1, CommandReply act2){
		act1.command.sameAs(act2.command) &&
		compareLists(act1.parameters, act2.parameters)
	}
	
	def dispatch boolean compare(CommandReplyWithVars act1, CommandReplyWithVars act2){
	    act1.condition.sameAs(act2.condition) &&
	    compareLists(act1.parameters, act2.parameters)
	}
	
	def dispatch boolean compare(InterfaceEventInstance ev1, InterfaceEventInstance ev2){
		ev1.event === ev2.event && compareLists(ev1.parameters, ev2.parameters)
	}
	
	def dispatch boolean compare(EventCall act1, EventCall act2){
		if(act1.event !== act2.event) return false
		compareLists(act1.parameters, act2.parameters) &&
		act1.occurence == act1.occurence && act1.multiplicity.sameAs(act2.multiplicity)
	}
	
	def dispatch boolean compare(EventWithVars act1, EventWithVars act2) {
	    if(act1.event !== act2.event) return false
	    act1.condition.sameAs(act2.condition) &&
        compareLists(act1.parameters, act2.parameters)
	}
	
	def dispatch boolean compare(Multiplicity m1, Multiplicity m2){
		m1.lower == m2.lower && m1.upper == m2.upper && 
		m1.upperInf == m2.upperInf
	}
	
	def dispatch boolean compare(ParallelComposition act1, ParallelComposition act2){
		val act1Components = act1.flatten
		val act2Components = act2.flatten
		
		if(act1Components.size != act2Components.size) {
			return false
		}
		return act1Components.forall(c1 | act2Components.exists(c2 | c1.sameAs(c2))) &&
			   act2Components.forall(c2 | act1Components.exists(c1 | c2.sameAs(c1)))
	}
	
	def dispatch boolean compare(PCFragmentReference ref1, PCFragmentReference ref2){
		ref1.fragment === ref2.fragment
	}
	
	def dispatch boolean compare(ActionList list1, ActionList list2){
		compareLists(list1.actions, list2.actions)
	}
}