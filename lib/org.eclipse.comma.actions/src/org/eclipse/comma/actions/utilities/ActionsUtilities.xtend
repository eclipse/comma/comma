/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.actions.utilities

import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set
import org.eclipse.comma.actions.actions.Action
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.EventWithVars
import org.eclipse.comma.actions.actions.PCElement
import org.eclipse.comma.actions.actions.PCFragment
import org.eclipse.comma.actions.actions.PCFragmentDefinition
import org.eclipse.comma.actions.actions.PCFragmentReference
import org.eclipse.comma.actions.actions.Reply
import org.eclipse.comma.expressions.expression.ExpressionAny
import org.eclipse.comma.expressions.utilities.ExpressionsComparator

class ActionsUtilities {
	
	def static EventPatternMultiplicity getNormalizedMultiplicity(EventCall ec){
		var result = new EventPatternMultiplicity
		if(ec.occurence !== null){
			switch(ec.occurence){
		 		case "?" : {result.lower = 0}
		 		case "*" : {result.lower = 0 result.upper = -1}
		 		case "+" : {result.lower = 1 result.upper = -1}
		 	}
		}else if(ec.multiplicity !== null){
			val m = ec.multiplicity
			if(m.upperInf !== null){
				result.upper = -1
				result.lower = m.lower
			}else{
				result.lower = m.lower
				result.upper = m.upper
			}
		}
		result
	}
	
	def static List<Action> flatten(PCFragment fragment){
		fragment.flattenHelper(new HashSet<PCFragment>)
	}
	
	def static private List<Action> flattenHelper(PCFragment fragment, Set<PCFragment> knownFragments){
		val result = new ArrayList<Action>
		for(c : fragment.components){
			switch(c){
				EventCall |
				EventWithVars |
				Reply : result.add(c)
				PCFragmentReference : if (knownFragments.add(c.fragment)) result.addAll(c.fragment.flattenHelper(knownFragments))
			}
		}
		result
	}
	
	def static HashSet<PCFragmentDefinition> allReferencedFragments(PCFragmentDefinition fd, HashSet<PCFragmentDefinition> knownFragments){
		for(c : fd.components.filter(PCFragmentReference)){
			if(knownFragments.add(c.fragment)){
				knownFragments.addAll(allReferencedFragments(c.fragment, knownFragments))
			}
		}
		knownFragments
	}
	
	def static boolean overlaps(EventCall call, PCElement other){
		if(call === null || other === null) return false
		if(other instanceof EventCall){
			if(call.event !== other.event) return false
			if(call.parameters.size !== other.parameters.size) return false
			for(i : 0..< call.parameters.size){
				val p1 = call.parameters.get(i)
				val p2 = other.parameters.get(i)
				val comp = new ExpressionsComparator
				if(!(p1 instanceof ExpressionAny) &&
				   !(p2 instanceof ExpressionAny) &&
				   ! comp.compare(p1, p2)) return false
			}
			return true
		}
		return false
	}
}