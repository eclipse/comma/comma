/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.actions.scoping

import java.util.ArrayList
import java.util.Collection
import java.util.List
import org.eclipse.comma.actions.actions.ActionsPackage
import org.eclipse.comma.actions.actions.CommandEvent
import org.eclipse.comma.actions.actions.InterfaceEventInstance
import org.eclipse.comma.actions.actions.NotificationEvent
import org.eclipse.comma.actions.actions.SignalEvent
import org.eclipse.comma.signature.interfaceSignature.InterfaceEvent
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignaturePackage
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.signature.utilities.InterfaceUtilities
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference

import static org.eclipse.xtext.scoping.Scopes.*

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class ActionsScopeProvider extends AbstractActionsScopeProvider {
	
	override getScope(EObject context, EReference reference){
		if(context instanceof InterfaceEventInstance && reference == ActionsPackage.Literals.INTERFACE_EVENT_INSTANCE__EVENT)
			return scope_InterfaceEventInstance_event(context as InterfaceEventInstance)
			
		return super.getScope(context, reference);
	}
	
	def scope_InterfaceEventInstance_event(InterfaceEventInstance context){
		var List<Signature> sigs = findVisibleInterfaces(context)
		var result = new ArrayList<InterfaceEvent>
		
		var EReference filter = null
		
		if(context instanceof CommandEvent){
			filter = InterfaceSignaturePackage.Literals.SIGNATURE__COMMANDS
		}else if(context instanceof SignalEvent){
			filter = InterfaceSignaturePackage.Literals.SIGNATURE__SIGNALS
		}else if(context instanceof NotificationEvent){
			filter = InterfaceSignaturePackage.Literals.SIGNATURE__NOTIFICATIONS
		}
		
		for(sig : sigs){
			if(filter === null){
				result.addAll(InterfaceUtilities::getAllInterfaceEvents(sig))
			}
			else{
				result.addAll(sig.eGet(filter) as Collection<InterfaceEvent>)
			}
		}
		
		return scopeFor(result)
	}
}
