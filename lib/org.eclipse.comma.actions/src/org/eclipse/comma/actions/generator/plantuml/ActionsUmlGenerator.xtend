/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.actions.generator.plantuml

import org.eclipse.comma.actions.actions.AnyEvent
import org.eclipse.comma.actions.actions.AssignmentAction
import org.eclipse.comma.actions.actions.CommandEvent
import org.eclipse.comma.actions.actions.CommandReply
import org.eclipse.comma.actions.actions.EVENT_KIND
import org.eclipse.comma.actions.actions.EventCall
import org.eclipse.comma.actions.actions.IfAction
import org.eclipse.comma.actions.actions.NotificationEvent
import org.eclipse.comma.actions.actions.RecordFieldAssignmentAction
import org.eclipse.comma.actions.actions.SignalEvent
import org.eclipse.comma.expressions.generator.plantuml.ExpressionsUmlGenerator
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.comma.actions.actions.ParallelComposition
import org.eclipse.comma.actions.utilities.ActionsUtilities
import org.eclipse.comma.actions.utilities.EventPatternMultiplicity
import org.eclipse.comma.actions.actions.PCFragmentReference
import org.eclipse.comma.actions.actions.CommandReplyWithVars
import org.eclipse.comma.actions.actions.EventWithVars

class ActionsUmlGenerator extends ExpressionsUmlGenerator {
	
	new(String fileName, IFileSystemAccess fsa) {
		super(fileName, fsa)
	}
	
	def dispatch CharSequence generateAction(AssignmentAction a)
	'''«a.assignment.name» := «generateExpression(a.exp)»; '''
	
	def dispatch CharSequence generateAction(RecordFieldAssignmentAction a)
	'''«generateExpression(a.fieldAccess)» := «generateExpression(a.exp)»; '''
	
	def dispatch CharSequence generateAction(IfAction a)
	'''if «generateExpression(a.guard)» then «FOR act : a.thenList.actions»«generateAction(act)»«ENDFOR»«IF a.elseList !== null» else «FOR act : a.elseList.actions»«generateAction(act)» «ENDFOR»«ENDIF»fi '''
	
	def dispatch CharSequence generateAction(CommandReply a)
	'''reply«IF a.parameters.size() > 0»(«generateExpression(a.parameters.get(0))»)«ENDIF» '''
	
	def dispatch CharSequence generateAction(CommandReplyWithVars a)
	'''reply«IF a.parameters.size() > 0»(«a.parameters.get(0).name»)«ENDIF»«IF a.condition !== null» where («generateExpression(a.condition)»)«ENDIF»'''
	
	def dispatch CharSequence generateAction(EventWithVars a)
    '''«(a.event.eContainer as Signature).name»::«a.event.name»«IF a.parameters.size() > 0»(«FOR p : a.parameters SEPARATOR ', '»«p.name»«ENDFOR»)«ENDIF»«IF a.condition !== null» where («generateExpression(a.condition)»)«ENDIF»'''
	
	def dispatch CharSequence generateAction(EventCall a)
	'''«(a.event.eContainer as Signature).name»::«a.event.name»«IF a.parameters.size() > 0»(«FOR p : a.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)«printMultiplicity(ActionsUtilities::getNormalizedMultiplicity(a))»«ENDIF» '''
	
	def dispatch CharSequence generateAction(PCFragmentReference a)
	'''fragment «a.fragment.name»'''
	
	def dispatch CharSequence generateAction(ParallelComposition a)
	'''any order(«FOR c : a.components SEPARATOR ', '»«generateAction(c)»«ENDFOR») '''
	
	def printMultiplicity(EventPatternMultiplicity m){
		if(m.lower == m.upper) return (if (m.lower == 1)'''''' else'''[«m.lower»]''')
		if(m.upper == -1){
			if(m.lower == 0) return '''[*]'''
			if(m.lower == 1) return '''[+]'''
			else return '''[«m.lower»-*]'''
		}else{
			if(m.lower == 0 && m.upper == 1) return '''[?]'''
			else return '''[«m.lower»-«m.upper»]'''
		}
	}
	
	def dispatch eventToUML(CommandEvent e, boolean expected)'''
						Client ->«IF ! expected»x«ENDIF» Server: command «(e.event.eContainer as Signature).name»_«e.event.name»«IF e.parameters.size() >0 »(«FOR p : e.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)«ENDIF»
					'''
	def dispatch eventToUML(SignalEvent e, boolean expected)'''
						Client ->>«IF ! expected»x«ENDIF» Server: signal «(e.event.eContainer as Signature).name»_«e.event.name»«IF e.parameters.size() >0 »(«FOR p : e.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)«ENDIF»
					'''
	
	def dispatch eventToUML(CommandReply e, boolean expected)'''
						Server -->«IF ! expected»x«ENDIF» Client: reply «IF e.parameters.size() > 0»(«generateExpression(e.parameters.get(0))»)«ENDIF»«IF e.command !== null» to command «(e.command.event.eContainer as Signature).name»_«e.command.event.name»«IF e.command.parameters.size() >0 »(«FOR p : e.command.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)«ENDIF»«ENDIF»
					'''
	
	def dispatch eventToUML(NotificationEvent e, boolean expected)'''
						Client «IF ! expected»x«ENDIF»//- Server: notification «(e.event.eContainer as Signature).name»_«e.event.name»«IF e.parameters.size() >0 »(«FOR p : e.parameters SEPARATOR ', '»«generateExpression(p)»«ENDFOR»)«ENDIF»
					'''
	
	def dispatch eventToUML(AnyEvent e, boolean expected)'''
						«IF e.kind == EVENT_KIND::CALL»Client ->«IF ! expected»x«ENDIF» Server: any command«ENDIF»
						«IF e.kind == EVENT_KIND::SIGNAL»Client ->>«IF ! expected»x«ENDIF» Server: any signal«ENDIF»
						«IF e.kind == EVENT_KIND::NOTIFICATION»Client «IF ! expected»x«ENDIF»//- Server: any notification«ENDIF»
						«IF e.kind == EVENT_KIND::EVENT»Client <->«IF ! expected»x«ENDIF» Server: any event«ENDIF»
					'''
}
