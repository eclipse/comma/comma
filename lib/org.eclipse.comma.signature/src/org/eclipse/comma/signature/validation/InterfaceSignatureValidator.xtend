/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.signature.validation

import com.google.inject.Inject
import org.eclipse.comma.signature.comments.InterfaceEventComment
import org.eclipse.comma.signature.interfaceSignature.Command
import org.eclipse.comma.signature.interfaceSignature.DIRECTION
import org.eclipse.comma.signature.interfaceSignature.InterfaceEvent
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignatureDefinition
import org.eclipse.comma.signature.interfaceSignature.InterfaceSignaturePackage
import org.eclipse.comma.signature.interfaceSignature.Notification
import org.eclipse.comma.signature.interfaceSignature.Signal
import org.eclipse.comma.signature.interfaceSignature.Signature
import org.eclipse.comma.types.types.FileImport
import org.eclipse.comma.types.types.SimpleTypeDecl
import org.eclipse.comma.types.types.Type
import org.eclipse.comma.types.types.TypesPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider
import org.eclipse.xtext.validation.Check

import static extension org.eclipse.comma.signature.utilities.InterfaceUtilities.*

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */

class InterfaceSignatureValidator extends AbstractInterfaceSignatureValidator {
	
	@Inject IEObjectDocumentationProvider documentationProvider
	
	/*
	 * Constraints:
	 * - the file name of the signature is different from the file names of the imported type models
	 */
	@Check
	def checkNamesImportedTypeModels(InterfaceSignatureDefinition root){
		val sigFilename = root.eResource.URI.trimFileExtension.lastSegment
		for(import : root.imports.filter(FileImport)){
			val importedFilename = URI.createURI(import.importURI).trimFileExtension.lastSegment
			if(importedFilename.equals(sigFilename))
				error("Imported type file should have a different name than the current signature file.", import, TypesPackage.Literals.FILE_IMPORT__IMPORT_URI)
		}
	}
	
	/*
	 * Constraints:
	 * - type definitions in the signature have unique names
	 */	
	@Check
	def checkDuplicatedTypeNamesInInterface(Signature s){
			checkForNameDuplications(s.types, "type declaration", null)
	}
	
	/*
	 * Constraints:
	 * - imported type definitions have unique names
	 * - interface event names do not duplicate type names
	 */
	@Check
  	def checkTypesForDuplications(InterfaceSignatureDefinition decl){
        val multiMap = decl.importedTypes
        for (entry : multiMap.asMap.entrySet) {
            val duplicates = entry.value    
            if (duplicates.size > 1) {
                error("Duplicate imported type " + entry.key.toString, decl.signature, TypesPackage.Literals.NAMED_ELEMENT__NAME)
            }
        }
        val events = decl.signature.getAllInterfaceEvents
        val simpleTypeNames = multiMap.keySet.map[lastSegment].toSet
  		for(ev : events){
  			//Check if the interface event has a name that duplicates type name
  			if(simpleTypeNames.contains(ev.name))
  				error("Interface event duplicates type name", ev, TypesPackage.Literals.NAMED_ELEMENT__NAME)
  		}
  	}
	
	/*
	 * Constraints:
	 * - parameter names of interface events are unique
	 */
	@Check
	def checkDuplicatedParameters(InterfaceEvent e){
		checkForNameDuplications(e.parameters, "parameter", null)
	}
	
	/*
	 * Constraints:
	 * - only commands may have inout and out parameters
	 */
	@Check
	def checkParameterDirection(InterfaceEvent e){
		if(e instanceof Notification || e instanceof Signal){
			for(p : e.parameters){
				if(p.direction != DIRECTION::IN)
					error('Direction of parameters in notifications and signals must be IN', 
						InterfaceSignaturePackage.Literals.INTERFACE_EVENT__PARAMETERS, 
						e.parameters.indexOf(p)
					)
			}
		}
	}
	
	/*
	 * Constraints:
	 * - interface event names are unique in the context of the signature
	 */
	@Check
	def checkDuplicatedInterfaceEvents(Signature s) {
		checkForNameDuplications(s.getAllInterfaceEvents, "interface event", null)
	}
	
	/*
	 * Constraints:
	 * - interface events are properly documented by using comments:
	 *   descriptions of: the event, the parameters, the returned value if any
	 */
	@Check
	def checkComment(InterfaceEvent ev){
		val InterfaceEventComment evComment = new InterfaceEventComment(ev, documentationProvider.getDocumentation(ev))
		if(!evComment.valid){
			warning(evComment.errorMessage, ev, TypesPackage.Literals.NAMED_ELEMENT__NAME)
		}
	}
	
	/*
	 * Constraints:
	 * - void type can only be used as a return type of commands
	 */
	@Check
	override checkForVoidType(Type t){
		if(t.type instanceof SimpleTypeDecl && (t.type.name.equals("void"))){
			if( !(t.eContainer instanceof Command) )
				error("Usage of type void is not allowed", TypesPackage.Literals.TYPE__TYPE)
		}
	}
	
	/*
	 * Constraints.
	 * The following constraints are inherited from the language for type definitions:
	 * - any type is not allowed to be directly used
	 * - imports have valid URIs and are type models
	 */
}
