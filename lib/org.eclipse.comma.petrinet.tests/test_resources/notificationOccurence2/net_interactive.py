## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_Example():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables

    # Init

    # Places
    add_place(Place('S_O', ['']), {'type': 'state', 'interface': 'Example', 'machine': 'StateMachine', 'state': 'O'})
    add_place(Place('T_O_0_signalo()'), {'type': 'transition', 'interface': 'Example', 'machine': 'StateMachine', 'state': 'O'})
    add_place(Place('C_O_0_signalo()_0_0'), {'type': 'clause', 'interface': 'Example', 'machine': 'StateMachine', 'state': 'O', 'sourceline': '10'})
    add_place(Place('S_A'), {'type': 'state', 'interface': 'Example', 'machine': 'StateMachine', 'state': 'A'})
    add_place(Place('P_O_signalo', [Parameters([])]), {'type': 'parameters', 'interface': 'Example'})
    add_place(Place('P_OK'), {'type': 'parameters', 'interface': 'Example'})
    add_place(Place('V', [Variables({})]), {'type': 'variables', 'interface': 'Example'})

    # Transitions
    add_transition(Transition('T0_event_signalo'), {'type': 'event', 'machine': 'StateMachine','event': Event(EventType.Signal, 'Example', 'Example', 'c', 'signalo', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'StateMachine'})
    add_transition(Transition('T2'), {'type': 'none', 'machine': 'StateMachine'})
    add_transition(Transition('T3_event_OK'), {'type': 'event', 'machine': 'StateMachine','event': Event(EventType.Notification, 'Example', 'Example', 'c', 'OK', [], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_O', 'T0_event_signalo', Variable('t'))
    n.add_input('T_O_0_signalo()', 'T1', Variable('gl'))
    n.add_input('C_O_0_signalo()_0_0', 'T2', Variable('gl'))
    n.add_input('C_O_0_signalo()_0_0', 'T3_event_OK', Variable('gl'))
    n.add_input('P_O_signalo', 'T0_event_signalo', Variable('p'))
    n.add_input('P_OK', 'T3_event_OK', Variable('p'))
    n.add_input('V', 'T0_event_signalo', Variable('g'))

    # Outputs
    n.add_output('T_O_0_signalo()', 'T0_event_signalo', Expression('g.gl(p.v([]))'))
    n.add_output('C_O_0_signalo()_0_0', 'T1', Variable('gl'))
    n.add_output('S_A', 'T2', Expression("''"))
    n.add_output('C_O_0_signalo()_0_0', 'T3_event_OK', Variable('gl'))
    n.add_output('P_O_signalo', 'T0_event_signalo', Variable('p'))
    n.add_output('V', 'T2', Expression("gl.g"))
    return n


nets = {
    ('Example', 'c') : net_Example,
}
constraints = [
]