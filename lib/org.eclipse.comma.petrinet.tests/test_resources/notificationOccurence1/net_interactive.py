## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables

    # Init

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '10'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_Off_0_PowerOn()_0_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_1_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '15'})
    add_place(Place('C_Off_0_PowerOn()_1_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_2_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '20'})
    add_place(Place('C_Off_0_PowerOn()_2_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PoweredOn'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PowerOn_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T2'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T3_event_PoweredOn', Expression('gl.r < 1')), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PoweredOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T4_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T5'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T6', Expression('gl.r >= 1')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T7_event_PoweredOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PoweredOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T8_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T9'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T10'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T11_event_PoweredOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PoweredOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T12_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T2', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T3_event_PoweredOn', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_1', 'T4_event_PowerOn_reply', Variable('gl'))
    n.add_input('T_Off_0_PowerOn()', 'T5', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_1_0', 'T6', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_1_0', 'T7_event_PoweredOn', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_1_1', 'T8_event_PowerOn_reply', Variable('gl'))
    n.add_input('T_Off_0_PowerOn()', 'T9', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_2_0', 'T10', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_2_0', 'T11_event_PoweredOn', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_2_1', 'T12_event_PowerOn_reply', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_PoweredOn', 'T3_event_PoweredOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T4_event_PowerOn_reply', Variable('p'))
    n.add_input('P_PoweredOn', 'T7_event_PoweredOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T8_event_PowerOn_reply', Variable('p'))
    n.add_input('P_PoweredOn', 'T11_event_PoweredOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T12_event_PowerOn_reply', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T1', Expression("gl.er('= 0')"))
    n.add_output('C_Off_0_PowerOn()_0_1', 'T2', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T3_event_PoweredOn', Expression("gl.er('+= 1')"))
    n.add_output('S_On', 'T4_event_PowerOn_reply', Expression("''"))
    n.add_output('C_Off_0_PowerOn()_1_0', 'T5', Expression("gl.er('= 0')"))
    n.add_output('C_Off_0_PowerOn()_1_1', 'T6', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_1_0', 'T7_event_PoweredOn', Expression("gl.er('= 1')"))
    n.add_output('S_On', 'T8_event_PowerOn_reply', Expression("''"))
    n.add_output('C_Off_0_PowerOn()_2_0', 'T9', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_2_1', 'T10', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_2_0', 'T11_event_PoweredOn', Variable('gl'))
    n.add_output('S_On', 'T12_event_PowerOn_reply', Expression("''"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('V', 'T4_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T8_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T12_event_PowerOn_reply', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]