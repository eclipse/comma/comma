## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_c_dataTypePort():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables
    v_ret = {}
    p_p1 = {}
    p_p2 = {}

    # Init
    v_ret = {"1": [{"age": 3.0}]}
    p_p1 = {"1": 2, "3": 4}
    p_p2 = {"9": {"99": 100}}

    # Places
    add_place(Place('S_A', ['']), {'type': 'state', 'interface': 'DataType', 'machine': 'dataTypeMachine', 'state': 'A'})
    add_place(Place('T_A_0_GetAddress()'), {'type': 'transition', 'interface': 'DataType', 'machine': 'dataTypeMachine', 'state': 'A'})
    add_place(Place('C_A_0_GetAddress()_0_0'), {'type': 'clause', 'interface': 'DataType', 'machine': 'dataTypeMachine', 'state': 'A', 'sourceline': '17'})
    add_place(Place('P_A_GetAddress', [Parameters([p_p1,p_p2])]), {'type': 'parameters', 'interface': 'DataType'})
    add_place(Place('P_GetAddress_reply'), {'type': 'parameters', 'interface': 'DataType'})
    add_place(Place('V', [Variables({'ret': v_ret})]), {'type': 'variables', 'interface': 'DataType'})

    # Transitions
    add_transition(Transition('T0_event_GetAddress'), {'type': 'event', 'machine': 'dataTypeMachine','event': Event(EventType.Command, 'DataType', 'dataTypePort', 'c', 'GetAddress', [Parameter({'type':'map','typeKey':'string','typeValue':'int'}, 'p[0]'),Parameter({'type':'map','typeKey':'string','typeValue':{'type':'map','typeKey':'string','typeValue':'int'}}, 'p[1]')], PortDirection.Provided, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'dataTypeMachine'})
    add_transition(Transition('T2_event_GetAddress_reply'), {'type': 'event', 'machine': 'dataTypeMachine','event': Event(EventType.Reply, 'DataType', 'dataTypePort', 'c', 'GetAddress', [Parameter({'type':'map','typeKey':'string','typeValue':'int'}, 'gl.l.p1'),Parameter({'type':'map','typeKey':'string','typeValue':{'type':'map','typeKey':'string','typeValue':'int'}}, 'gl.l.p2'),Parameter({'type':'map','typeKey':'string','typeValue':{'type':'vector','dimensions':1,'typeElem':{'type':'record','record':'Age','fields':[{'name':'age','type':'real'}]}}}, 'gl.g.ret')], PortDirection.Provided, False)})

    # Inputs 
    n.add_input('S_A', 'T0_event_GetAddress', Variable('t'))
    n.add_input('T_A_0_GetAddress()', 'T1', Variable('gl'))
    n.add_input('C_A_0_GetAddress()_0_0', 'T2_event_GetAddress_reply', Variable('gl'))
    n.add_input('P_A_GetAddress', 'T0_event_GetAddress', Variable('p'))
    n.add_input('P_GetAddress_reply', 'T2_event_GetAddress_reply', Variable('p'))
    n.add_input('V', 'T0_event_GetAddress', Variable('g'))

    # Outputs
    n.add_output('T_A_0_GetAddress()', 'T0_event_GetAddress', Expression('g.gl(p.v(["p1","p2"]))'))
    n.add_output('C_A_0_GetAddress()_0_0', 'T1', Variable('gl'))
    n.add_output('S_A', 'T2_event_GetAddress_reply', Expression("''"))
    n.add_output('P_A_GetAddress', 'T0_event_GetAddress', Variable('p'))
    n.add_output('V', 'T2_event_GetAddress_reply', Expression("gl.g"))
    return n


@dataclass
class VectorSimpleConstraintConstraint(Constraint):
    states: List[ConstraintState]

    use_event = [
        Event(EventType.Command, 'DataType', 'dataTypePort', '', 'GetAddress', [], PortDirection.Unknown, False),
        Event(EventType.Reply, 'DataType', 'dataTypePort', '', 'GetAddress', [], PortDirection.Unknown, False),
    ]

    def __init__(self, states=None, init=True):
        if init:
            v = Variables({})
            initState = [ConstraintState('AA', None, v)]
            self.states = initState
        else:
            self.states = states

    def __str__(self) -> str:
        return f"{__class__.__name__} {self.states}"

    def take(self, event: Event, port_machine_state: Dict[str, Dict[str, str]]):
        if len([e for e in self.use_event if e.equals(event, True)]) == 0:
            return self

        states = []
        for cstate in self.states:
            index = cstate.index
            s = cstate.state
            v = cstate.variables
            if s == 'AA':
                # Transition 0
                if index == None and Event(EventType.Command, 'DataType', 'dataTypePort', '', 'GetAddress', [], PortDirection.Unknown, False).equals(event, True):
                    v = cstate.variables.copy()
                    v.p11 = event.parameters[0].value
                    v.p22 = event.parameters[1].value
                    states.append(ConstraintState('AA', (0,1), v))
                if index == (0,1) and Event(EventType.Reply, 'DataType', 'dataTypePort', '', 'GetAddress', [Parameter({'type':'map','typeKey':'string','typeValue':'int'}, str({"1": 2, "3": 4})).eval(), Parameter({'type':'map','typeKey':'string','typeValue':{'type':'map','typeKey':'string','typeValue':'int'}}, str({"9": {"99": 100}})).eval(), Parameter({'type':'map','typeKey':'string','typeValue':{'type':'vector','dimensions':1,'typeElem':{'type':'record','record':'Age','fields':[{'name':'age','type':'real'}]}}}, str({"1": [{"age": 3.0}]})).eval()], PortDirection.Unknown, False).equals(event, False):
                    v = cstate.variables.copy()
                    states.append(ConstraintState('AA', None, v))
        if len(states):
            return VectorSimpleConstraintConstraint(states, False)

        return None

    def get_state(self) -> List[ConstraintState]:
        return self.states

    def set_state(self, state: List[ConstraintState]):
        self.states = state


nets = {
    ('dataTypePort', 'c'): net_c_dataTypePort,
}
constraints = [
    VectorSimpleConstraintConstraint,
]