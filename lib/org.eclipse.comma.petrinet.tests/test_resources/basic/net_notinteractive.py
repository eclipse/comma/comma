## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables
    v_picturesTaken = 0
    v_battery = 0.0

    # Init
    v_battery = 100.0

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '22'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_Off_0_PowerOn()_0_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_1_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '27'})
    add_place(Place('C_Off_0_PowerOn()_1_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_1_2'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_On_0_TakePicture()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '37'})
    add_place(Place('C_On_0_TakePicture()_0_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_2'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_3'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('T_On_ias0.0_NonTriggered'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_ias0.0_NonTriggered_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '16'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_TakePicture', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({'picturesTaken': v_picturesTaken,'battery': v_battery})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T2'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T3_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [Parameter('enum', '"Status:OnOK"')], PortDirection.Unknown, False)})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T5_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [Parameter('enum', '"Status:OnOK"')], PortDirection.Unknown, False)})
    add_transition(Transition('T6'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T7'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T8_event_TakePicture', Expression('(lambda g, p:((g.battery) > (45.0)))(g, p)')), {'type': 'event', 'machine': 'camera','event': Event(EventType.Signal, 'ICamera', 'ICamera', 'c', 'TakePicture', [], PortDirection.Unknown, False)})
    add_transition(Transition('T9'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T10'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T11_event_PictureTaken'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PictureTaken', [], PortDirection.Unknown, False)})
    add_transition(Transition('T12'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T13'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T14'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T15'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T16_event_Autofocussed'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'Autofocussed', [], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T2', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_1', 'T3_event_PowerOn_reply', Variable('gl'))
    n.add_input('T_Off_0_PowerOn()', 'T4', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_1_0', 'T5_event_PowerOn_reply', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_1_1', 'T6', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_1_2', 'T7', Variable('gl'))
    n.add_input('S_On', 'T8_event_TakePicture', Variable('t'))
    n.add_input('T_On_0_TakePicture()', 'T9', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_0', 'T10', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1', 'T11_event_PictureTaken', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_2', 'T12', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_3', 'T13', Variable('gl'))
    n.add_input('S_On', 'T14', Variable('t'))
    n.add_input('T_On_ias0.0_NonTriggered', 'T15', Variable('gl'))
    n.add_input('C_On_ias0.0_NonTriggered_0_0', 'T16_event_Autofocussed', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_On_TakePicture', 'T8_event_TakePicture', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))
    n.add_input('V', 'T8_event_TakePicture', Variable('g'))
    n.add_input('V', 'T14', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T1', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_1', 'T2', Expression("gl.e('g.picturesTaken = (lambda g, l: (0))(g, l)')"))
    n.add_output('S_On', 'T3_event_PowerOn_reply', Expression("''"))
    n.add_output('C_Off_0_PowerOn()_1_0', 'T4', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_1_1', 'T5_event_PowerOn_reply', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_1_2', 'T6', Expression("gl.e('g.picturesTaken = (lambda g, l: (0))(g, l)')"))
    n.add_output('S_On', 'T7', Expression("''"))
    n.add_output('T_On_0_TakePicture()', 'T8_event_TakePicture', Expression('g.gl(p.v([]))'))
    n.add_output('C_On_0_TakePicture()_0_0', 'T9', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1', 'T10', Expression("gl.e('g.battery = (lambda g, l: ((g.battery) * (0.9)))(g, l)')"))
    n.add_output('C_On_0_TakePicture()_0_2', 'T11_event_PictureTaken', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_3', 'T12', Expression("gl.e('g.picturesTaken = (lambda g, l: ((g.picturesTaken) + (1)))(g, l)')"))
    n.add_output('S_On', 'T13', Expression("''"))
    n.add_output('T_On_ias0.0_NonTriggered', 'T14', Expression('g.gl()'))
    n.add_output('C_On_ias0.0_NonTriggered_0_0', 'T15', Variable('gl'))
    n.add_output('S_On', 'T16_event_Autofocussed', Expression("''"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('P_On_TakePicture', 'T8_event_TakePicture', Variable('p'))
    n.add_output('V', 'T3_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T7', Expression("gl.g"))
    n.add_output('V', 'T13', Expression("gl.g"))
    n.add_output('V', 'T16_event_Autofocussed', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]