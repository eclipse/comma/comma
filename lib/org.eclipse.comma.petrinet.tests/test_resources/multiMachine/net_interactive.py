## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables

    # Init

    # Places
    add_place(Place('S_onOff_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'onOff', 'state': 'Off'})
    add_place(Place('T_onOff_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'onOff', 'state': 'Off'})
    add_place(Place('C_onOff_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'onOff', 'state': 'Off', 'sourceline': '9'})
    add_place(Place('S_onOff_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'onOff', 'state': 'On'})
    add_place(Place('T_onOff_On_0_PowerOff()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'onOff', 'state': 'On'})
    add_place(Place('C_onOff_On_0_PowerOff()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'onOff', 'state': 'On', 'sourceline': '16'})
    add_place(Place('S_flashlight_FlashlightOff', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'flashlight', 'state': 'FlashlightOff'})
    add_place(Place('T_flashlight_FlashlightOff_0_TurnOnFlashlight()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'flashlight', 'state': 'FlashlightOff'})
    add_place(Place('C_flashlight_FlashlightOff_0_TurnOnFlashlight()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'flashlight', 'state': 'FlashlightOff', 'sourceline': '26'})
    add_place(Place('S_flashlight_FlashlightOn'), {'type': 'state', 'interface': 'ICamera', 'machine': 'flashlight', 'state': 'FlashlightOn'})
    add_place(Place('T_flashlight_FlashlightOn_0_TurnOffFlashlight()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'flashlight', 'state': 'FlashlightOn'})
    add_place(Place('C_flashlight_FlashlightOn_0_TurnOffFlashlight()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'flashlight', 'state': 'FlashlightOn', 'sourceline': '33'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PowerOn_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_PowerOff', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PowerOff_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_FlashlightOff_TurnOnFlashlight', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_TurnOnFlashlight_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_FlashlightOn_TurnOffFlashlight', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_TurnOffFlashlight_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'onOff','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'onOff'})
    add_transition(Transition('T2_event_PowerOn_reply'), {'type': 'event', 'machine': 'onOff','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T3_event_PowerOff'), {'type': 'event', 'machine': 'onOff','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOff', [], PortDirection.Unknown, False)})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'onOff'})
    add_transition(Transition('T5_event_PowerOff_reply'), {'type': 'event', 'machine': 'onOff','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOff', [], PortDirection.Unknown, False)})
    add_transition(Transition('T6_event_TurnOnFlashlight'), {'type': 'event', 'machine': 'flashlight','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'TurnOnFlashlight', [], PortDirection.Unknown, False)})
    add_transition(Transition('T7'), {'type': 'none', 'machine': 'flashlight'})
    add_transition(Transition('T8_event_TurnOnFlashlight_reply'), {'type': 'event', 'machine': 'flashlight','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'TurnOnFlashlight', [], PortDirection.Unknown, False)})
    add_transition(Transition('T9_event_TurnOffFlashlight'), {'type': 'event', 'machine': 'flashlight','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'TurnOffFlashlight', [], PortDirection.Unknown, False)})
    add_transition(Transition('T10'), {'type': 'none', 'machine': 'flashlight'})
    add_transition(Transition('T11_event_TurnOffFlashlight_reply'), {'type': 'event', 'machine': 'flashlight','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'TurnOffFlashlight', [], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_onOff_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_onOff_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_onOff_Off_0_PowerOn()_0_0', 'T2_event_PowerOn_reply', Variable('gl'))
    n.add_input('S_onOff_On', 'T3_event_PowerOff', Variable('t'))
    n.add_input('T_onOff_On_0_PowerOff()', 'T4', Variable('gl'))
    n.add_input('C_onOff_On_0_PowerOff()_0_0', 'T5_event_PowerOff_reply', Variable('gl'))
    n.add_input('S_flashlight_FlashlightOff', 'T6_event_TurnOnFlashlight', Variable('t'))
    n.add_input('T_flashlight_FlashlightOff_0_TurnOnFlashlight()', 'T7', Variable('gl'))
    n.add_input('C_flashlight_FlashlightOff_0_TurnOnFlashlight()_0_0', 'T8_event_TurnOnFlashlight_reply', Variable('gl'))
    n.add_input('S_flashlight_FlashlightOn', 'T9_event_TurnOffFlashlight', Variable('t'))
    n.add_input('T_flashlight_FlashlightOn_0_TurnOffFlashlight()', 'T10', Variable('gl'))
    n.add_input('C_flashlight_FlashlightOn_0_TurnOffFlashlight()_0_0', 'T11_event_TurnOffFlashlight_reply', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T2_event_PowerOn_reply', Variable('p'))
    n.add_input('P_On_PowerOff', 'T3_event_PowerOff', Variable('p'))
    n.add_input('P_PowerOff_reply', 'T5_event_PowerOff_reply', Variable('p'))
    n.add_input('P_FlashlightOff_TurnOnFlashlight', 'T6_event_TurnOnFlashlight', Variable('p'))
    n.add_input('P_TurnOnFlashlight_reply', 'T8_event_TurnOnFlashlight_reply', Variable('p'))
    n.add_input('P_FlashlightOn_TurnOffFlashlight', 'T9_event_TurnOffFlashlight', Variable('p'))
    n.add_input('P_TurnOffFlashlight_reply', 'T11_event_TurnOffFlashlight_reply', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))
    n.add_input('V', 'T3_event_PowerOff', Variable('g'))
    n.add_input('V', 'T6_event_TurnOnFlashlight', Variable('g'))
    n.add_input('V', 'T9_event_TurnOffFlashlight', Variable('g'))

    # Outputs
    n.add_output('T_onOff_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_onOff_Off_0_PowerOn()_0_0', 'T1', Variable('gl'))
    n.add_output('S_onOff_On', 'T2_event_PowerOn_reply', Expression("''"))
    n.add_output('T_onOff_On_0_PowerOff()', 'T3_event_PowerOff', Expression('g.gl(p.v([]))'))
    n.add_output('C_onOff_On_0_PowerOff()_0_0', 'T4', Variable('gl'))
    n.add_output('S_onOff_Off', 'T5_event_PowerOff_reply', Expression("''"))
    n.add_output('T_flashlight_FlashlightOff_0_TurnOnFlashlight()', 'T6_event_TurnOnFlashlight', Expression('g.gl(p.v([]))'))
    n.add_output('C_flashlight_FlashlightOff_0_TurnOnFlashlight()_0_0', 'T7', Variable('gl'))
    n.add_output('S_flashlight_FlashlightOn', 'T8_event_TurnOnFlashlight_reply', Expression("''"))
    n.add_output('T_flashlight_FlashlightOn_0_TurnOffFlashlight()', 'T9_event_TurnOffFlashlight', Expression('g.gl(p.v([]))'))
    n.add_output('C_flashlight_FlashlightOn_0_TurnOffFlashlight()_0_0', 'T10', Variable('gl'))
    n.add_output('S_flashlight_FlashlightOff', 'T11_event_TurnOffFlashlight_reply', Expression("''"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('P_On_PowerOff', 'T3_event_PowerOff', Variable('p'))
    n.add_output('P_FlashlightOff_TurnOnFlashlight', 'T6_event_TurnOnFlashlight', Variable('p'))
    n.add_output('P_FlashlightOn_TurnOffFlashlight', 'T9_event_TurnOffFlashlight', Variable('p'))
    n.add_output('V', 'T2_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T5_event_PowerOff_reply', Expression("gl.g"))
    n.add_output('V', 'T8_event_TurnOnFlashlight_reply', Expression("gl.g"))
    n.add_output('V', 'T11_event_TurnOffFlashlight_reply', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]