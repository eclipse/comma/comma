## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_IMotor():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables
    v_requestedSpeed = 0
    v_reportedSpeed = 0

    # Init
    v_reportedSpeed = 1

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('T_Off_0_start()'), {'type': 'transition', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_0'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off', 'sourceline': '14'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'IMotor', 'machine': 'Main', 'state': 'On'})
    add_place(Place('C_Off_0_start()_0_1_split'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_1_join'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_1_split_if_0'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_1_split_if_1'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_1_split_else_0'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_3'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_4'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_5'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('C_Off_0_start()_0_6'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'Off'})
    add_place(Place('T_On_0_temp()'), {'type': 'transition', 'interface': 'IMotor', 'machine': 'Main', 'state': 'On'})
    add_place(Place('C_On_0_temp()_0_0'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'On', 'sourceline': '30'})
    add_place(Place('T_On_1_NonTriggered'), {'type': 'transition', 'interface': 'IMotor', 'machine': 'Main', 'state': 'On'})
    add_place(Place('C_On_1_NonTriggered_0_0'), {'type': 'clause', 'interface': 'IMotor', 'machine': 'Main', 'state': 'On', 'sourceline': '34'})
    add_place(Place('P_Off_start', [Parameters([40])]), {'type': 'parameters', 'interface': 'IMotor'})
    add_place(Place('P_Off_running', [Parameters([41]), Parameters([43])]), {'type': 'parameters', 'interface': 'IMotor'})
    add_place(Place('P_On_temp', [Parameters([])]), {'type': 'parameters', 'interface': 'IMotor'})
    add_place(Place('P_On_temp_reply', [Parameters([99])]), {'type': 'parameters', 'interface': 'IMotor'})
    add_place(Place('P_On_speed', [Parameters([43])]), {'type': 'parameters', 'interface': 'IMotor'})
    add_place(Place('V', [Variables({'requestedSpeed': v_requestedSpeed,'reportedSpeed': v_reportedSpeed})]), {'type': 'variables', 'interface': 'IMotor'})

    # Transitions
    add_transition(Transition('T0_event_start', Expression('(lambda g, p:((p[0]) > (0)))(g, p)')), {'type': 'event', 'machine': 'Main','event': Event(EventType.Command, 'IMotor', 'IMotor', 'c', 'start', [Parameter('int', 'p[0]')], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T2'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T3', Expression('(lambda gl:((gl.g.requestedSpeed) == (gl.l.s)))(gl)')), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T5_event_start_reply'), {'type': 'event', 'machine': 'Main','event': Event(EventType.Reply, 'IMotor', 'IMotor', 'c', 'start', [], PortDirection.Unknown, False)})
    add_transition(Transition('T6', Expression('(lambda gl:(not ((gl.g.requestedSpeed) == (gl.l.s))))(gl)')), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T7_event_start_reply'), {'type': 'event', 'machine': 'Main','event': Event(EventType.Reply, 'IMotor', 'IMotor', 'c', 'start', [], PortDirection.Unknown, False)})
    add_transition(Transition('T8'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T9_event_running', Expression('(lambda gl, p:(((p[0]) - (gl.g.requestedSpeed)) <= (2)))(gl, p)')), {'type': 'event', 'machine': 'Main','event': Event(EventType.Notification, 'IMotor', 'IMotor', 'c', 'running', [Parameter('int', 'p[0]')], PortDirection.Unknown, False)})
    add_transition(Transition('T10_event_running', Expression('(lambda gl, p:(((p[0]) - (gl.l.s)) <= (3)))(gl, p)')), {'type': 'event', 'machine': 'Main','event': Event(EventType.Notification, 'IMotor', 'IMotor', 'c', 'running', [Parameter('int', 'p[0]')], PortDirection.Unknown, False)})
    add_transition(Transition('T11'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T12'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T13_event_temp'), {'type': 'event', 'machine': 'Main','event': Event(EventType.Command, 'IMotor', 'IMotor', 'c', 'temp', [], PortDirection.Unknown, False)})
    add_transition(Transition('T14'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T15_event_temp_reply', Expression('(lambda gl, p:((p[0]) < (100)))(gl, p)')), {'type': 'event', 'machine': 'Main','event': Event(EventType.Reply, 'IMotor', 'IMotor', 'c', 'temp', [Parameter('int', 'p[0]')], PortDirection.Unknown, False)})
    add_transition(Transition('T16'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T17'), {'type': 'none', 'machine': 'Main'})
    add_transition(Transition('T18_event_speed', Expression('(lambda gl, p:((p[0]) == (gl.g.reportedSpeed)))(gl, p)')), {'type': 'event', 'machine': 'Main','event': Event(EventType.Notification, 'IMotor', 'IMotor', 'c', 'speed', [Parameter('int', 'p[0]')], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_Off', 'T0_event_start', Variable('t'))
    n.add_input('T_Off_0_start()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_start()_0_0', 'T2', Variable('gl'))
    n.add_input('C_Off_0_start()_0_1_split', 'T3', Variable('gl'))
    n.add_input('C_Off_0_start()_0_1_split_if_0', 'T4', Variable('gl'))
    n.add_input('C_Off_0_start()_0_1_split_if_1', 'T5_event_start_reply', Variable('gl'))
    n.add_input('C_Off_0_start()_0_1_split', 'T6', Variable('gl'))
    n.add_input('C_Off_0_start()_0_1_split_else_0', 'T7_event_start_reply', Variable('gl'))
    n.add_input('C_Off_0_start()_0_1_join', 'T8', Variable('gl'))
    n.add_input('C_Off_0_start()_0_3', 'T9_event_running', Variable('gl'))
    n.add_input('C_Off_0_start()_0_4', 'T10_event_running', Variable('gl'))
    n.add_input('C_Off_0_start()_0_5', 'T11', Variable('gl'))
    n.add_input('C_Off_0_start()_0_6', 'T12', Variable('gl'))
    n.add_input('S_On', 'T13_event_temp', Variable('t'))
    n.add_input('T_On_0_temp()', 'T14', Variable('gl'))
    n.add_input('C_On_0_temp()_0_0', 'T15_event_temp_reply', Variable('gl'))
    n.add_input('S_On', 'T16', Variable('t'))
    n.add_input('T_On_1_NonTriggered', 'T17', Variable('gl'))
    n.add_input('C_On_1_NonTriggered_0_0', 'T18_event_speed', Variable('gl'))
    n.add_input('P_Off_start', 'T0_event_start', Variable('p'))
    n.add_input('P_Off_running', 'T9_event_running', Variable('p'))
    n.add_input('P_Off_running', 'T10_event_running', Variable('p'))
    n.add_input('P_On_temp', 'T13_event_temp', Variable('p'))
    n.add_input('P_On_temp_reply', 'T15_event_temp_reply', Variable('p'))
    n.add_input('P_On_speed', 'T18_event_speed', Variable('p'))
    n.add_input('V', 'T0_event_start', Variable('g'))
    n.add_input('V', 'T13_event_temp', Variable('g'))
    n.add_input('V', 'T16', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_start()', 'T0_event_start', Expression('g.gl(p.v(["s"]))'))
    n.add_output('C_Off_0_start()_0_0', 'T1', Variable('gl'))
    n.add_output('C_Off_0_start()_0_1_split', 'T2', Expression("gl.e('g.requestedSpeed = (lambda g, l: (l.s))(g, l)')"))
    n.add_output('C_Off_0_start()_0_1_split_if_0', 'T3', Variable('gl'))
    n.add_output('C_Off_0_start()_0_1_split_if_1', 'T4', Expression("gl.e('g.requestedSpeed = (lambda g, l: ((l.s) + (1)))(g, l)')"))
    n.add_output('C_Off_0_start()_0_1_join', 'T5_event_start_reply', Variable('gl'))
    n.add_output('C_Off_0_start()_0_1_split_else_0', 'T6', Variable('gl'))
    n.add_output('C_Off_0_start()_0_1_join', 'T7_event_start_reply', Variable('gl'))
    n.add_output('C_Off_0_start()_0_3', 'T8', Expression("gl.e('g.reportedSpeed = (lambda g, l: (0))(g, l)')"))
    n.add_output('C_Off_0_start()_0_4', 'T9_event_running', Expression('gl.add(p.vdict(["y"]))'))
    n.add_output('C_Off_0_start()_0_5', 'T10_event_running', Expression('gl.add(p.vdict(["z"]))'))
    n.add_output('C_Off_0_start()_0_6', 'T11', Expression("gl.e('g.reportedSpeed = (lambda g, l: (l.z))(g, l)')"))
    n.add_output('S_On', 'T12', Expression("''"))
    n.add_output('T_On_0_temp()', 'T13_event_temp', Expression('g.gl(p.v([]))'))
    n.add_output('C_On_0_temp()_0_0', 'T14', Variable('gl'))
    n.add_output('S_On', 'T15_event_temp_reply', Expression("''"))
    n.add_output('T_On_1_NonTriggered', 'T16', Expression('g.gl()'))
    n.add_output('C_On_1_NonTriggered_0_0', 'T17', Variable('gl'))
    n.add_output('S_On', 'T18_event_speed', Expression("''"))
    n.add_output('P_Off_start', 'T0_event_start', Variable('p'))
    n.add_output('P_Off_running', 'T9_event_running', Variable('p'))
    n.add_output('P_Off_running', 'T10_event_running', Variable('p'))
    n.add_output('P_On_temp', 'T13_event_temp', Variable('p'))
    n.add_output('P_On_temp_reply', 'T15_event_temp_reply', Variable('p'))
    n.add_output('P_On_speed', 'T18_event_speed', Variable('p'))
    n.add_output('V', 'T12', Expression("gl.g"))
    n.add_output('V', 'T15_event_temp_reply', Expression("gl.g"))
    n.add_output('V', 'T18_event_speed', Expression("gl.g"))
    return n


nets = {
    ('IMotor', 'c') : net_IMotor,
}
constraints = [
]
