## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_c_testPort():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables

    # Init

    # Places
    add_place(Place('S_A', ['']), {'type': 'state', 'interface': 'Test', 'machine': 'dataTypeMachine', 'state': 'A'})
    add_place(Place('T_A_0_MySignal()'), {'type': 'transition', 'interface': 'Test', 'machine': 'dataTypeMachine', 'state': 'A'})
    add_place(Place('C_A_0_MySignal()_0_0'), {'type': 'clause', 'interface': 'Test', 'machine': 'dataTypeMachine', 'state': 'A', 'sourceline': '10'})
    add_place(Place('P_A_MySignal', [Parameters([99])]), {'type': 'parameters', 'interface': 'Test'})
    add_place(Place('V', [Variables({})]), {'type': 'variables', 'interface': 'Test'})

    # Transitions
    add_transition(Transition('T0_event_MySignal'), {'type': 'event', 'machine': 'dataTypeMachine','event': Event(EventType.Signal, 'Test', 'testPort', 'c', 'MySignal', [Parameter('int', 'p[0]')], PortDirection.Provided, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'dataTypeMachine'})
    add_transition(Transition('T2_event_MyNotification'), {'type': 'event', 'machine': 'dataTypeMachine','event': Event(EventType.Notification, 'Test', 'testPort', 'c', 'MyNotification', [Parameter('int', 'gl.l.p11'),Parameter('string', '"yes!"')], PortDirection.Provided, False)})

    # Inputs 
    n.add_input('S_A', 'T0_event_MySignal', Variable('t'))
    n.add_input('T_A_0_MySignal()', 'T1', Variable('gl'))
    n.add_input('C_A_0_MySignal()_0_0', 'T2_event_MyNotification', Variable('gl'))
    n.add_input('P_A_MySignal', 'T0_event_MySignal', Variable('p'))
    n.add_input('V', 'T0_event_MySignal', Variable('g'))

    # Outputs
    n.add_output('T_A_0_MySignal()', 'T0_event_MySignal', Expression('g.gl(p.v(["p11"]))'))
    n.add_output('C_A_0_MySignal()_0_0', 'T1', Variable('gl'))
    n.add_output('S_A', 'T2_event_MyNotification', Expression("''"))
    n.add_output('P_A_MySignal', 'T0_event_MySignal', Variable('p'))
    n.add_output('V', 'T2_event_MyNotification', Expression("gl.g"))
    return n


@dataclass
class MyConstraintConstraint(Constraint):
    states: List[ConstraintState]

    use_event = [
        Event(EventType.Signal, 'Test', 'testPort', '', 'MySignal', [], PortDirection.Unknown, False),
        Event(EventType.Notification, 'Test', 'testPort', '', 'MyNotification', [], PortDirection.Unknown, False),
    ]

    def __init__(self, states=None, init=True):
        if init:
            v = Variables({})
            initState = [ConstraintState('A', None, v)]
            self.states = initState
        else:
            self.states = states

    def __str__(self) -> str:
        return f"{__class__.__name__} {self.states}"

    def take(self, event: Event, port_machine_state: Dict[str, Dict[str, str]]):
        if len([e for e in self.use_event if e.equals(event, True)]) == 0:
            return self

        states = []
        for cstate in self.states:
            index = cstate.index
            s = cstate.state
            v = cstate.variables
            if s == 'A':
                # Transition 0
                if index == None and Event(EventType.Signal, 'Test', 'testPort', '', 'MySignal', [], PortDirection.Unknown, False).equals(event, True):
                    v = cstate.variables.copy()
                    v.p = event.parameters[0].value
                    if (v.p) == (99):
                        states.append(ConstraintState('A', (0,1), v))
                if index == (0,1) and Event(EventType.Notification, 'Test', 'testPort', '', 'MyNotification', [Parameter('int', str(99)).eval(), Parameter('string', '"yes!"').eval()], PortDirection.Unknown, False).equals(event, False):
                    v = cstate.variables.copy()
                    states.append(ConstraintState('A', None, v))
        if len(states):
            return MyConstraintConstraint(states, False)

        return None

    def get_state(self) -> List[ConstraintState]:
        return self.states

    def set_state(self, state: List[ConstraintState]):
        self.states = state


nets = {
    ('testPort', 'c'): net_c_testPort,
}
constraints = [
    MyConstraintConstraint,
]