## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables
    v_picturesTaken = 0
    v_battery = 0.0
    v_ready = True

    # Init
    v_battery = 100.0
    v_ready = True

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '18'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('T_On_0_TakePicture()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '27'})
    add_place(Place('C_On_0_TakePicture()_0_1_split'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_join'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0_split'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0_join'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0_split_if_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0_split_if_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0_split_else_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_if_0_split_else_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_1_split_else_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_3'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('T_On_1_NonTriggered'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_1_NonTriggered_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '43'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_TakePicture', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({'picturesTaken': v_picturesTaken,'battery': v_battery,'ready': v_ready})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T2_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [Parameter('enum', '"Status:OnOK"')], PortDirection.Unknown, False)})
    add_transition(Transition('T3_event_TakePicture', Expression('(lambda g, p:((g.battery) > (45.0)))(g, p)')), {'type': 'event', 'machine': 'camera','event': Event(EventType.Signal, 'ICamera', 'ICamera', 'c', 'TakePicture', [], PortDirection.Unknown, False)})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T5'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T6', Expression('(lambda gl:((gl.g.battery) <= (45.0)))(gl)')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T7'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T8', Expression('(lambda gl:((gl.g.battery) <= (35.0)))(gl)')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T9'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T10_event_BatteryLow'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'BatteryLow', [], PortDirection.Unknown, False)})
    add_transition(Transition('T11', Expression('(lambda gl:(not ((gl.g.battery) <= (35.0))))(gl)')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T12'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T13'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T14'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T15', Expression('(lambda gl:(not ((gl.g.battery) <= (45.0))))(gl)')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T16'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T17'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T18_event_PictureTaken'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PictureTaken', [], PortDirection.Unknown, False)})
    add_transition(Transition('T19', Expression('(lambda g, p:(not (g.ready)))(g, p)')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T20'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T21_event_Dummy'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'Dummy', [], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T2_event_PowerOn_reply', Variable('gl'))
    n.add_input('S_On', 'T3_event_TakePicture', Variable('t'))
    n.add_input('T_On_0_TakePicture()', 'T4', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_0', 'T5', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split', 'T6', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0', 'T7', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_split', 'T8', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_split_if_0', 'T9', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_split_if_1', 'T10_event_BatteryLow', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_split', 'T11', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_split_else_0', 'T12', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_split_else_1', 'T13', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_if_0_join', 'T14', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split', 'T15', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_split_else_0', 'T16', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_1_join', 'T17', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_3', 'T18_event_PictureTaken', Variable('gl'))
    n.add_input('S_On', 'T19', Variable('t'))
    n.add_input('T_On_1_NonTriggered', 'T20', Variable('gl'))
    n.add_input('C_On_1_NonTriggered_0_0', 'T21_event_Dummy', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_On_TakePicture', 'T3_event_TakePicture', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))
    n.add_input('V', 'T3_event_TakePicture', Variable('g'))
    n.add_input('V', 'T19', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T1', Variable('gl'))
    n.add_output('S_On', 'T2_event_PowerOn_reply', Expression("''"))
    n.add_output('T_On_0_TakePicture()', 'T3_event_TakePicture', Expression('g.gl(p.v([]))'))
    n.add_output('C_On_0_TakePicture()_0_0', 'T4', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split', 'T5', Expression("gl.e('g.battery = (lambda g, l: ((g.battery) * (0.9)))(g, l)')"))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0', 'T6', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_split', 'T7', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_split_if_0', 'T8', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_split_if_1', 'T9', Expression("gl.e('g.battery = (lambda g, l: (30.0))(g, l)')"))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_join', 'T10_event_BatteryLow', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_split_else_0', 'T11', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_split_else_1', 'T12', Expression("gl.e('g.battery = (lambda g, l: (40.0))(g, l)')"))
    n.add_output('C_On_0_TakePicture()_0_1_split_if_0_join', 'T13', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_join', 'T14', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_split_else_0', 'T15', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_1_join', 'T16', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_3', 'T17', Expression("gl.e('g.picturesTaken = (lambda g, l: ((g.picturesTaken) + (1)))(g, l)')"))
    n.add_output('S_On', 'T18_event_PictureTaken', Expression("''"))
    n.add_output('T_On_1_NonTriggered', 'T19', Expression('g.gl()'))
    n.add_output('C_On_1_NonTriggered_0_0', 'T20', Variable('gl'))
    n.add_output('S_On', 'T21_event_Dummy', Expression("''"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('P_On_TakePicture', 'T3_event_TakePicture', Variable('p'))
    n.add_output('V', 'T2_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T18_event_PictureTaken', Expression("gl.g"))
    n.add_output('V', 'T21_event_Dummy', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]