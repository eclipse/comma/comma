## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables
    v_picturesTaken = 0
    v_parameter = True

    # Init
    v_picturesTaken = 0

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '17'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('T_On_0_TakePictureAsync()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePictureAsync()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '25'})
    add_place(Place('C_On_0_TakePictureAsync()_0_1'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('T_On_1_TakePictureSync()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_1_TakePictureSync()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '32'})
    add_place(Place('C_On_1_TakePictureSync()_0_3'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PowerOn_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_TakePictureAsync', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PictureTaken'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_TakePictureSync', [Parameters([True]), Parameters([False])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_TakePictureSync_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({'picturesTaken': v_picturesTaken,'parameter': v_parameter})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T2_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T3_event_TakePictureAsync'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Signal, 'ICamera', 'ICamera', 'c', 'TakePictureAsync', [], PortDirection.Unknown, False)})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T5'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T6_event_PictureTaken'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PictureTaken', [Parameter('int', 'gl.g.picturesTaken')], PortDirection.Unknown, False)})
    add_transition(Transition('T7_event_TakePictureSync', Expression('(lambda g, p:((p[0]) == (True)))(g, p)')), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'TakePictureSync', [Parameter('bool', 'p[0]')], PortDirection.Unknown, False)})
    add_transition(Transition('T8'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T9'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T10_event_TakePictureSync_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'TakePictureSync', [Parameter('bool', 'gl.l.autoFocus'),Parameter('int', 'gl.g.picturesTaken')], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T2_event_PowerOn_reply', Variable('gl'))
    n.add_input('S_On', 'T3_event_TakePictureAsync', Variable('t'))
    n.add_input('T_On_0_TakePictureAsync()', 'T4', Variable('gl'))
    n.add_input('C_On_0_TakePictureAsync()_0_0', 'T5', Variable('gl'))
    n.add_input('C_On_0_TakePictureAsync()_0_1', 'T6_event_PictureTaken', Variable('gl'))
    n.add_input('S_On', 'T7_event_TakePictureSync', Variable('t'))
    n.add_input('T_On_1_TakePictureSync()', 'T8', Variable('gl'))
    n.add_input('C_On_1_TakePictureSync()_0_0', 'T9', Variable('gl'))
    n.add_input('C_On_1_TakePictureSync()_0_3', 'T10_event_TakePictureSync_reply', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T2_event_PowerOn_reply', Variable('p'))
    n.add_input('P_On_TakePictureAsync', 'T3_event_TakePictureAsync', Variable('p'))
    n.add_input('P_PictureTaken', 'T6_event_PictureTaken', Variable('p'))
    n.add_input('P_On_TakePictureSync', 'T7_event_TakePictureSync', Variable('p'))
    n.add_input('P_TakePictureSync_reply', 'T10_event_TakePictureSync_reply', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))
    n.add_input('V', 'T3_event_TakePictureAsync', Variable('g'))
    n.add_input('V', 'T7_event_TakePictureSync', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T1', Variable('gl'))
    n.add_output('S_On', 'T2_event_PowerOn_reply', Expression("''"))
    n.add_output('T_On_0_TakePictureAsync()', 'T3_event_TakePictureAsync', Expression('g.gl(p.v([]))'))
    n.add_output('C_On_0_TakePictureAsync()_0_0', 'T4', Variable('gl'))
    n.add_output('C_On_0_TakePictureAsync()_0_1', 'T5', Expression("gl.e('g.picturesTaken = (lambda g, l: ((g.picturesTaken) + (1)))(g, l)')"))
    n.add_output('S_On', 'T6_event_PictureTaken', Expression("''"))
    n.add_output('T_On_1_TakePictureSync()', 'T7_event_TakePictureSync', Expression('g.gl(p.v(["autoFocus"]))'))
    n.add_output('C_On_1_TakePictureSync()_0_0', 'T8', Variable('gl'))
    n.add_output('C_On_1_TakePictureSync()_0_3', 'T9', Expression("gl.e('g.picturesTaken = (lambda g, l: ((g.picturesTaken) + (1)))(g, l)').e('l.autoFocus = (lambda g, l: (not (l.autoFocus)))(g, l)').e('g.parameter = (lambda g, l: (l.autoFocus))(g, l)')"))
    n.add_output('S_On', 'T10_event_TakePictureSync_reply', Expression("''"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('P_On_TakePictureAsync', 'T3_event_TakePictureAsync', Variable('p'))
    n.add_output('P_On_TakePictureSync', 'T7_event_TakePictureSync', Variable('p'))
    n.add_output('V', 'T2_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T6_event_PictureTaken', Expression("gl.g"))
    n.add_output('V', 'T10_event_TakePictureSync_reply', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]