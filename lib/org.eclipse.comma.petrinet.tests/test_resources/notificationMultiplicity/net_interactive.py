## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables

    # Init

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '9'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_Off_0_PowerOn()_0_0_anyorder'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0_join'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0_anyorder_0_start'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0_anyorder_0_end'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0_anyorder_1_start'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0_anyorder_1_end'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_On_0_TakePicture()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '19'})
    add_place(Place('S_End'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'End'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PoweredOn'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PowerOn_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_TakePicture', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PictureTaken'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T2'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T3'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T5', Expression('gl.r >= 2')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T6_event_PoweredOn', Expression('gl.r < 3')), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PoweredOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T7_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T8'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T9_event_TakePicture'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Signal, 'ICamera', 'ICamera', 'c', 'TakePicture', [], PortDirection.Unknown, False)})
    add_transition(Transition('T10'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T11', Expression('gl.r >= 3')), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T12_event_PictureTaken', Expression('gl.r < 6')), {'type': 'event', 'machine': 'camera','event': Event(EventType.Notification, 'ICamera', 'ICamera', 'c', 'PictureTaken', [], PortDirection.Unknown, False)})

    # Inputs 
    n.add_input('S_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T2', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_anyorder', 'T3', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_anyorder_0_end', 'T4', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_anyorder_0_start', 'T5', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_anyorder_0_start', 'T6_event_PoweredOn', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_anyorder_1_end', 'T4', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_anyorder_1_start', 'T7_event_PowerOn_reply', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0_join', 'T8', Variable('gl'))
    n.add_input('S_On', 'T9_event_TakePicture', Variable('t'))
    n.add_input('T_On_0_TakePicture()', 'T10', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_0', 'T11', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_0', 'T12_event_PictureTaken', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_PoweredOn', 'T6_event_PoweredOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T7_event_PowerOn_reply', Variable('p'))
    n.add_input('P_On_TakePicture', 'T9_event_TakePicture', Variable('p'))
    n.add_input('P_PictureTaken', 'T12_event_PictureTaken', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))
    n.add_input('V', 'T9_event_TakePicture', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T1', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_0_anyorder', 'T2', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_0_join', 'T4', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_0_anyorder_0_start', 'T3', Expression("gl.er('= 0')"))
    n.add_output('C_Off_0_PowerOn()_0_0_anyorder_0_end', 'T5', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_0_anyorder_0_start', 'T6_event_PoweredOn', Expression("gl.er('+= 1')"))
    n.add_output('C_Off_0_PowerOn()_0_0_anyorder_1_start', 'T3', Variable('gl'))
    n.add_output('C_Off_0_PowerOn()_0_0_anyorder_1_end', 'T7_event_PowerOn_reply', Variable('gl'))
    n.add_output('S_On', 'T8', Expression("''"))
    n.add_output('T_On_0_TakePicture()', 'T9_event_TakePicture', Expression('g.gl(p.v([]))'))
    n.add_output('C_On_0_TakePicture()_0_0', 'T10', Expression("gl.er('= 0')"))
    n.add_output('S_End', 'T11', Expression("''"))
    n.add_output('C_On_0_TakePicture()_0_0', 'T12_event_PictureTaken', Expression("gl.er('+= 1')"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('P_On_TakePicture', 'T9_event_TakePicture', Variable('p'))
    n.add_output('V', 'T8', Expression("gl.g"))
    n.add_output('V', 'T11', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]