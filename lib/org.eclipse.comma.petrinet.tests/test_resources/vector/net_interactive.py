## net
from snakes.nets import Transition, Place, Expression, Variable, PetriNet
from typing import Tuple

def net_ICamera():
    n = PetriNet("N")

    def add_place(place: Place, meta: Dict[str, Any]):
        n.add_place(place)
        place.meta = meta

    def add_transition(transition: Transition, meta: Dict[str, Any]):
        n.add_transition(transition)
        transition.meta = meta

    # Variables
    v_myVector = []
    v_notEmpty = True
    v_nestedVector = []
    v_containsNested = True

    # Init
    v_myVector = [1]
    v_nestedVector = [["test"]]
    v_containsNested = False

    # Places
    add_place(Place('S_Off', ['']), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('T_Off_0_PowerOn()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off'})
    add_place(Place('C_Off_0_PowerOn()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'Off', 'sourceline': '21'})
    add_place(Place('S_On'), {'type': 'state', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('T_On_0_TakePicture()'), {'type': 'transition', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('C_On_0_TakePicture()_0_0'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On', 'sourceline': '29'})
    add_place(Place('C_On_0_TakePicture()_0_3'), {'type': 'clause', 'interface': 'ICamera', 'machine': 'camera', 'state': 'On'})
    add_place(Place('P_Off_PowerOn', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_PowerOn_reply'), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('P_On_TakePicture', [Parameters([])]), {'type': 'parameters', 'interface': 'ICamera'})
    add_place(Place('V', [Variables({'myVector': v_myVector,'notEmpty': v_notEmpty,'nestedVector': v_nestedVector,'containsNested': v_containsNested})]), {'type': 'variables', 'interface': 'ICamera'})

    # Transitions
    add_transition(Transition('T0_event_PowerOn'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Command, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T1'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T2_event_PowerOn_reply'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Reply, 'ICamera', 'ICamera', 'c', 'PowerOn', [], PortDirection.Unknown, False)})
    add_transition(Transition('T3_event_TakePicture'), {'type': 'event', 'machine': 'camera','event': Event(EventType.Signal, 'ICamera', 'ICamera', 'c', 'TakePicture', [], PortDirection.Unknown, False)})
    add_transition(Transition('T4'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T5'), {'type': 'none', 'machine': 'camera'})
    add_transition(Transition('T6'), {'type': 'none', 'machine': 'camera'})

    # Inputs 
    n.add_input('S_Off', 'T0_event_PowerOn', Variable('t'))
    n.add_input('T_Off_0_PowerOn()', 'T1', Variable('gl'))
    n.add_input('C_Off_0_PowerOn()_0_0', 'T2_event_PowerOn_reply', Variable('gl'))
    n.add_input('S_On', 'T3_event_TakePicture', Variable('t'))
    n.add_input('T_On_0_TakePicture()', 'T4', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_0', 'T5', Variable('gl'))
    n.add_input('C_On_0_TakePicture()_0_3', 'T6', Variable('gl'))
    n.add_input('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_input('P_PowerOn_reply', 'T2_event_PowerOn_reply', Variable('p'))
    n.add_input('P_On_TakePicture', 'T3_event_TakePicture', Variable('p'))
    n.add_input('V', 'T0_event_PowerOn', Variable('g'))
    n.add_input('V', 'T3_event_TakePicture', Variable('g'))

    # Outputs
    n.add_output('T_Off_0_PowerOn()', 'T0_event_PowerOn', Expression('g.gl(p.v([]))'))
    n.add_output('C_Off_0_PowerOn()_0_0', 'T1', Variable('gl'))
    n.add_output('S_On', 'T2_event_PowerOn_reply', Expression("''"))
    n.add_output('T_On_0_TakePicture()', 'T3_event_TakePicture', Expression('g.gl(p.v([]))'))
    n.add_output('C_On_0_TakePicture()_0_0', 'T4', Variable('gl'))
    n.add_output('C_On_0_TakePicture()_0_3', 'T5', Expression("gl.e('g.notEmpty = (lambda g, l: (not (len(g.myVector) == 0)))(g, l)').e('g.myVector = (lambda g, l: (g.myVector + [len(g.myVector)]))(g, l)').e('g.containsNested = (lambda g, l: (len([k for k in g.nestedVector if \"test\" in k]) != 0))(g, l)')"))
    n.add_output('S_On', 'T6', Expression("''"))
    n.add_output('P_Off_PowerOn', 'T0_event_PowerOn', Variable('p'))
    n.add_output('P_On_TakePicture', 'T3_event_TakePicture', Variable('p'))
    n.add_output('V', 'T2_event_PowerOn_reply', Expression("gl.g"))
    n.add_output('V', 'T6', Expression("gl.g"))
    return n


nets = {
    ('ICamera', 'c') : net_ICamera,
}
constraints = [
]