/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.petrinet.tests

import com.google.inject.Inject
import com.google.inject.Provider
import java.nio.file.Files
import java.nio.file.Paths
import java.util.ArrayList
import java.util.List
import java.util.stream.Stream
import org.eclipse.comma.behavior.component.component.Component
import org.eclipse.comma.behavior.component.component.ComponentModel
import org.eclipse.comma.behavior.component.utilities.ComponentUtilities
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.Interface
import org.eclipse.comma.behavior.interfaces.interfaceDefinition.InterfaceDefinition
import org.eclipse.comma.behavior.interfaces.scoping.InterfaceUtilities
import org.eclipse.comma.parameters.parameters.Parameters
import org.eclipse.comma.petrinet.EnvConfig
import org.eclipse.comma.petrinet.PetrinetBuilder
import org.eclipse.comma.python.PythonInterpreter
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.scoping.IScopeProvider
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.util.StringInputStream
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.^extension.ExtendWith

import static org.junit.jupiter.api.DynamicTest.dynamicTest
import java.io.PrintWriter

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class Tests {
    @Inject extension Provider<ResourceSet> resourceSet
    @Inject IScopeProvider scopeProvider
    
    static val resourcePath = "test_resources"
    
    def void test(String path) {
        var set = resourceSet.get()
        var parameters = new ArrayList<Parameters>();
        var testResourcePath = resourcePath + "/" + path;
        var Component component = null
        var List<Interface> interfaces = new ArrayList();
        var String expectedNetInteractive = null
        var String expectedNetNotInteractive = null
        var String constraintTest = null
        var String constraintExpected = null
        for (file : Paths.get(testResourcePath).toFile.listFiles) {
            var ext = file.name.substring(file.name.lastIndexOf("."));
            var contents = Files.readString(Paths.get(testResourcePath + "/" + file.name)).replaceAll("\r\n", "\n").trim()
            if (#[".component", ".interface", ".params", ".signature", ".types"].contains(ext)) {
                set.createResource(URI.createURI(file.name))
                    .load(new StringInputStream(contents), emptyMap)
                if (ext.equals(".params")) {
                    parameters.add(set.getResource(URI.createURI(file.name), true).contents.head as Parameters)
                } else if (ext.equals(".component")) {
                    component = (set.getResource(URI.createURI(file.name), true).contents.head as ComponentModel).component
                } else if (ext.equals(".interface")) {
                    var itf = (set.getResource(URI.createURI(file.name), true).contents.head as InterfaceDefinition).interface
                    interfaces.add(itf)
                }
            } 
            else if (file.name.equals("net_interactive.py")) { expectedNetInteractive = contents }
            else if (file.name.equals("net_notinteractive.py")) { expectedNetNotInteractive = contents }
            else if (file.name.equals("constraint_test.py")) { constraintTest = contents }
            else if (file.name.equals("constraint_expected.txt")) { constraintExpected = contents }
        }
        
        var actualNetTestInteractive = "";
        var actualNetNotInteractive = "";
        
        if (component !== null) {
            val allInterfaces = ComponentUtilities.getAllInterfaces(component.eContainer(), scopeProvider);
            var List<EnvConfig> environment = new ArrayList<EnvConfig>();
            for(port : component.ports ){
                val itf = allInterfaces.stream().filter(ii | InterfaceUtilities.getSignature(ii, scopeProvider) == port.getInterface()).findFirst().get();
                val param = parameters.stream().filter(p | p.getInterface().equals(itf)).findFirst();
                environment.add(new EnvConfig("c", port, param.isPresent() ? param.get() : null))
            }       
            actualNetTestInteractive = PetrinetBuilder.forComponent(component, environment, scopeProvider, PetrinetBuilder.Mode.INTERACTIVE);
            actualNetNotInteractive = PetrinetBuilder.forComponent(component, environment, scopeProvider, PetrinetBuilder.Mode.NOT_INTERACTIVE);
        } else {
            Assertions.assertEquals(1, interfaces.length);
            Assertions.assertTrue(interfaces.length <= 1);
            var itf = interfaces.get(0)
            var params = parameters.length == 1 ? parameters.get(0) : null
            actualNetTestInteractive = PetrinetBuilder.forInterface(itf, params, scopeProvider, PetrinetBuilder.Mode.INTERACTIVE);
            actualNetNotInteractive = PetrinetBuilder.forInterface(itf, params, scopeProvider, PetrinetBuilder.Mode.NOT_INTERACTIVE);
        }
        
        // Uncomment for debugging failing regression test cases
//        var outInteractive = new PrintWriter(path + "INTERACTIVE.py");
//        outInteractive.print(actualNetTestInteractive.replaceAll("\r\n", "\n").trim());
//        outInteractive.close();
//        var outNonInteractive = new PrintWriter(path + "NOT_INTERACTIVE.py");
//        outNonInteractive.print(actualNetNotInteractive.replaceAll("\r\n", "\n").trim());
//        outNonInteractive.close();
        
        Assertions.assertEquals(expectedNetInteractive, actualNetTestInteractive.replaceAll("\r\n", "\n").trim(), "INTERACTIVE net mismatch");
        Assertions.assertEquals(expectedNetNotInteractive, actualNetNotInteractive.replaceAll("\r\n", "\n").trim(), "NOT_INTERACTIVE net mismatch");

        // Test contraints
        if (constraintTest !== null) {
            var code = "SELF_CONTAINED = True\n" + PetrinetBuilder.getModelCode() + actualNetTestInteractive + "\n\n" + constraintTest;
            var actualConstraint = PythonInterpreter.execute(code);
            Assertions.assertEquals(constraintExpected, actualConstraint);
        }
    }

    @TestFactory
    def Stream<DynamicTest> testFromResources() {
        return Paths.get(resourcePath).toFile.listFiles.map[f | dynamicTest(f.name, [| test(f.name)])].stream
    }
}
