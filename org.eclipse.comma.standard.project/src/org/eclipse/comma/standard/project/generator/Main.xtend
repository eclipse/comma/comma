/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.generator

import org.eclipse.comma.behavior.BehaviorStandaloneSetup
import org.eclipse.comma.behavior.component.ComponentStandaloneSetup
import org.eclipse.comma.behavior.interfaces.InterfaceDefinitionStandaloneSetup
import org.eclipse.comma.project.ProjectStandaloneSetup
import org.eclipse.comma.standard.project.StandardProjectStandaloneSetup
import org.eclipse.comma.signature.InterfaceSignatureStandaloneSetup
import org.eclipse.comma.traces.events.TraceEventsStandaloneSetup
import org.eclipse.comma.types.generator.CommaMain
import org.eclipse.comma.parameters.ParametersStandaloneSetup

class Main {

	def static main(String[] args) {
		InterfaceSignatureStandaloneSetup.doSetup
		BehaviorStandaloneSetup.doSetup
		InterfaceDefinitionStandaloneSetup.doSetup
		ComponentStandaloneSetup.doSetup
		ParametersStandaloneSetup.doSetup
		TraceEventsStandaloneSetup.doSetup
		ProjectStandaloneSetup.doSetup
			
		val injector = new StandardProjectStandaloneSetup().createInjectorAndDoEMFRegistration
		
		val main = injector.getInstance(CommaMain)
		main.configure(args, "CommaSuite project generator", "project", ".prj")
		main.read
	}

}
