/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project

import org.eclipse.comma.project.importing.ProjectResourceDescriptionManager
import org.eclipse.comma.types.scoping.ModelContainerGlobalScopeProvider
import org.eclipse.xtext.resource.IResourceDescription
import org.eclipse.xtext.scoping.IGlobalScopeProvider

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class StandardProjectRuntimeModule extends AbstractStandardProjectRuntimeModule {

	def Class<? extends IResourceDescription.Manager> bindIResourceDescription$Manager() {
		return ProjectResourceDescriptionManager
	}

	override Class<? extends IGlobalScopeProvider> bindIGlobalScopeProvider() {
		return ModelContainerGlobalScopeProvider
	}
}
