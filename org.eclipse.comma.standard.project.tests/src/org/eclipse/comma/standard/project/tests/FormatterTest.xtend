/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(StandardProjectInjectorProvider)
class FormatterTest {

	@Inject extension FormatterTestHelper

	@Test def void formatDocumentation() {
		assertFormatted[
			expectation = '''
				import "IUniversity.sm"

				Project IUniversity {

					Generate Documentations {
						doc for interface ICamera {
							template = "Template.docx"
							targetFile = "targetFile"
							author = "John Smith"
							role = "R&D: SW Designer"
						}
					}
				}
			'''
			toBeFormatted = '''
				import "IUniversity.sm"			Project IUniversity {		Generate Documentations {
										doc for interface ICamera {	template = "Template.docx"	targetFile = "targetFile"
				author = "John Smith"	role = "R&D: SW Designer"		}		}					}
			'''
		]
	}

	@Test def void formatAllTasks() {
		assertFormatted[
			expectation = '''
				import "IUniversity.sm"

				Project IUniversity {

					Generate Documentations {
						doc for interface ICamera {
							template = "Template.docx"
							targetFile = "targetFile"
							author = "John Smith"
							role = "R&D: SW Designer"
						}
					}

					Generate Monitors {
						monitor for interface ICamera {
							trace files "StudentTest.traces"
						}
					}
				}
			'''
			toBeFormatted = '''
				import "IUniversity.sm"		Project IUniversity {	Generate Documentations {
					doc for interface ICamera { template = "Template.docx"	targetFile = "targetFile"	author = "John Smith" role = "R&D: SW Designer"
					} }	Generate Monitors {				monitor for interface ICamera {		trace files
				"StudentTest.traces"}	}	}
			'''
		]
	}
}
