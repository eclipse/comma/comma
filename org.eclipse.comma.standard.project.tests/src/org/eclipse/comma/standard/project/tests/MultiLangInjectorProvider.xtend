/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.tests

import org.eclipse.comma.behavior.interfaces.tests.InterfaceDefinitionInjectorProvider
import org.eclipse.comma.project.tests.ProjectInjectorProvider
import org.eclipse.comma.signature.tests.InterfaceSignatureInjectorProvider
import org.eclipse.comma.traces.events.tests.TraceEventsInjectorProvider
import org.eclipse.comma.behavior.component.tests.ComponentInjectorProvider

class MultiLangInjectorProvider extends StandardProjectInjectorProvider {
		override protected internalCreateInjector() {
		new InterfaceSignatureInjectorProvider().getInjector
		new InterfaceDefinitionInjectorProvider().getInjector
		new ProjectInjectorProvider().getInjector
		new TraceEventsInjectorProvider().getInjector
		new ComponentInjectorProvider().getInjector
		return super.internalCreateInjector()
		
	}
}
