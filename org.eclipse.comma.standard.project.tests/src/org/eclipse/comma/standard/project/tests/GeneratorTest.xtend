/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.tests

import org.eclipse.xtext.testing.InjectWith
import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.generator.JavaIoFileSystemAccess
import org.eclipse.comma.standard.project.generator.StandardProjectGenerator
import org.eclipse.comma.java.JSourceInfo
import java.util.Set
import org.eclipse.comma.java.JCompiler

import java.io.File

import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.IOException
import org.eclipse.comma.java.JArtifactInfoBuilder
import org.eclipse.comma.java.JSourceInfos
import org.eclipse.comma.java.JDependencyInfos
import org.eclipse.comma.monitoring.lib.CTask
import com.google.gson.Gson
import org.eclipse.comma.monitoring.dashboard.DashboardHelper
import net.sourceforge.plantuml.SourceFileReader
import java.nio.file.Paths
import java.nio.file.Path
import java.nio.file.Files
import org.junit.jupiter.api.Assertions
import com.google.gson.JsonParser
import org.eclipse.emf.common.util.URI
import java.util.ArrayList
import org.eclipse.emf.ecore.resource.Resource
import java.nio.file.NoSuchFileException
import java.nio.file.DirectoryNotEmptyException
import java.net.URLClassLoader;

/*
 * This test is used to test the generator, whether it is generating correct monitor(s) 
 * in accordance with the defined model(s). For this purpose a set of test cases with different models
 * are used to test the generator. These test cases are stored in the test_resources directory.
 * 
 * Furthermore, this test also checks whether the generator also generating the appropriate documentation files
 * when defined in the task
 * such as : UML, dashboard and documentation description files.
 * see testTheGenerationOfDocumenation() method for further info.
 */

@ExtendWith(InjectionExtension)
@InjectWith(MultiLangInjectorProvider)
class GeneratorTest {
	@Inject extension Provider<ResourceSet> resourceSetProvider
	@Inject Provider<JavaIoFileSystemAccess> fileAccessProvider
	@Inject StandardProjectGenerator gen
	JavaIoFileSystemAccess fileAccess
	Set<JSourceInfo> generatedSourceInfos
	ResourceSet set
	String generatedJavaFiles
	String pathOfTestCases

	
	@BeforeEach
	def void setup() {
		set = resourceSetProvider.get()
		fileAccess = fileAccessProvider.get()
				
		//the (java) generated monitor will be be stored in this folder.
        //from here the monitor will be executed
		fileAccess.setOutputPath("java-gen/")
		
		generatedJavaFiles = "java/org/eclipse/comma/monitoring/generated/"
		
		//folder which contains the test cases
		pathOfTestCases = "test_resources/"
		
	}
	
	@Test
	def void testingTheGenerator() {
		val testCases = new ArrayList<String>()
		
		testCases.add("Image")
		testCases.add("DataRulesTest")
		testCases.add("TimeGenericRulesTest")
		testCases.add("EventPreconditionTest")
		testCases.add("ImageComponentTest")
		testCases.add("ComponentSemanticsTest")
		testCases.add("ExpressionsTest")
		testCases.add("FunctionalIDTest")
		testCases.add("jsonEventsTest")
    	testCases.add("JsonComponentTest")
    	testCases.add("EventsReorderTest")
    	testCases.add("EventNameMatchTest")
    	testCases.add("StringPatternTest")

		
		testCases.forEach[testCase |
					generateMonitorOf(testCase)
		]
	}
	
	def void generateMonitorOf(String testCase) {
		val prj_file = Paths.get(pathOfTestCases + testCase+ "/Project.prj").toFile
		val prj = createProjectResource(prj_file)
		
		//Generate monitor of the testCase that contains a specific model
		gen.generateWithErrorReport(prj, fileAccess, null)
		
		assertTrue(fileAccess.isFile(generatedJavaFiles.toString+"ScenarioPlayer.java"), "The file scenarioPlayer.java has not been created")
		
		//run the generated monitor
		runMonitor()
		
		val expectedResult = Files.readAllLines(Paths.get(pathOfTestCases + testCase + "/ExpectedtasksResults.json")).join("\n")
		                                                                                                             .replace("\\r","")
                                                                                                                     .replace("\\n","")
		val generatedResults = Files.readAllLines(Paths.get("comma-gen/tasksResults.json")).join("\n").removePathFromTraceFile
	   																								  .replace("\\r","")
																									  .replace("\\n","")
	   							
		Assertions.assertEquals(JsonParser.parseString(expectedResult), JsonParser.parseString(generatedResults),"Failed on test case "+testCase.toString)
	}
	
	@Test
	def void testTheGenerationOfDocumenation(){
		
		val projectFile = Paths.get(pathOfTestCases + "DocumentationTest/Project.prj")
		val targetFile = new File("Project.prj")
		val targetPath = Paths.get("./"+targetFile.toString)
		
		//if a Project.prj already exists in workspace, delete it
		// and then copy  the current Project.prj to workspace
		cleanUpFile(targetPath)
		Files.copy(projectFile, targetPath)
		
		val prj_file = Paths.get(targetFile.toString).toFile
		val prj = createProjectResource(prj_file)		
		gen.generateWithErrorReport(prj, fileAccess, null)

		//run monitor
		runMonitor()
		Assertions.assertTrue(Files.exists(Paths.get("comma-gen/dashboard.html")),"dashboard has not been generated !!")
		Assertions.assertTrue(Files.exists(Paths.get("java-gen/uml/umlTask/IDocu_StateMachine_complete.plantuml")),"plantuml has not been generated !!")
		Assertions.assertTrue(Files.exists(Paths.get("java-gen/uml/umlTask/IDocu_StateMachine_complete.png")),"png file of plantuml has not been generated !!")
		Assertions.assertTrue(Files.exists(Paths.get("java-gen/doc/documentation.docx")),"documentation has not been generated !!")
		
		//delete Project.prj
		cleanUpFile(targetPath)
		
	}
	
	/*
	 * ================= helping methods =============
	 */
	
	def cleanUpFile(Path targetFile) {
		try {
            Files.deleteIfExists(targetFile);
        }
        catch(NoSuchFileException e) {
            System.out.println(targetFile+" does not exists")
        }
        catch(DirectoryNotEmptyException e) {
            System.out.println("Directory is not empty.")
        }
        catch(IOException e) {
            System.out.println("Invalid permissions.")
        }
	}
	
	def Resource createProjectResource(File prj_file) {
		val prj_uri = URI.createFileURI(prj_file.absolutePath)
		val prj = set.getResource(prj_uri, true)
		return prj;
	}


	def void runMonitor() {
		try {
			val javaSourcePath = "java-gen/java/org/eclipse/comma/monitoring/generated/"
			
			//getting the generated java classes
			generatedSourceInfos = JSourceInfos.getJavaSourcesFromSrcDirectory(javaSourcePath);

			//setting up the dependencies
			val monitorRuntimeJar = new File(CTask.getProtectionDomain().getCodeSource().getLocation().toURI())
        	val dashboardJar = new File(DashboardHelper.getProtectionDomain().getCodeSource().getLocation().toURI())
        	val gsonJar = new File(Gson.getProtectionDomain().getCodeSource().getLocation().toURI())
        	val plantumlJar = new File(SourceFileReader.getProtectionDomain().getCodeSource().getLocation().toURI())
        	val monitorRuntimeDependency = JDependencyInfos.create(monitorRuntimeJar);
        	val gsonDependency = JDependencyInfos.create(gsonJar);
        	val dashboardDependency = JDependencyInfos.create(dashboardJar);
        	val plantumlDependency = JDependencyInfos.create(plantumlJar);

			//creating the compiler
			val compiler = new JCompiler() 
							.withSourceInfos(generatedSourceInfos)
							.withDependencyInfos(monitorRuntimeDependency)
							.withDependencyInfos(gsonDependency)
							.withDependencyInfos(dashboardDependency)
                            .withDependencyInfos(plantumlDependency)
						
			println("Compiling and building generated code ...");
			var classInfos = compiler.compile()

			// Build .jar file
			val jarFile = new File(javaSourcePath + "/Player.jar")
			new JArtifactInfoBuilder()
	    		.withArchiveFile(jarFile)
	    		.withClassInfos(classInfos)
	    		.withMainClass("org/eclipse/comma/monitoring/generated/ScenarioPlayer")
	    		.build();

	    	// Add jar file to class path
	    	val urls = #[jarFile.toURI.toURL, monitorRuntimeJar.toURI.toURL, gsonJar.toURI.toURL, plantumlJar.toURI.toURL, dashboardJar.toURI.toURL]
			val classLoader = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());
			Thread.currentThread().setContextClassLoader(classLoader);

			// Create scenario player
			val scenarioPlayerClass = classLoader.loadClass("org.eclipse.comma.monitoring.generated.ScenarioPlayer");
			val scenarioPlayer = scenarioPlayerClass.getDeclaredConstructor.newInstance

			// Execute scenario player
    		scenarioPlayerClass.getDeclaredMethod("init",null).invoke(scenarioPlayer, null)
			scenarioPlayerClass.getDeclaredMethod("run", null).invoke(scenarioPlayer, null)
		} catch (IOException | InterruptedException e) {
			println("IOException/InterruptedException : " +e);
		}
	}
	
	def String removePathFromTraceFile(String generatedResults) {
		var newGeneratedResults = new StringBuffer(generatedResults)
		var startIndex = newGeneratedResults.indexOf("traceFile",0)
		var endIndex = 0
		while(startIndex != -1) {			
			endIndex = newGeneratedResults.indexOf(",", startIndex )

			var tracePath = newGeneratedResults.substring(startIndex +  "traceFile".length() + 3, endIndex)

			newGeneratedResults.replace(startIndex  + "traceFile".length() + 4, endIndex, getNameOfEventFile(tracePath)).toString()
			startIndex = newGeneratedResults.indexOf("traceFile", endIndex)
		}
		return newGeneratedResults.toString
	}

	def String getNameOfEventFile(String tracePath) {
		return tracePath.substring(tracePath.lastIndexOf(File.separator) + 1, tracePath.length())
	}
}
