/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.tests


import org.junit.jupiter.api.Test
import java.nio.file.Paths
import java.io.File
import java.nio.file.Files

import static org.junit.jupiter.api.Assertions.assertTrue
import static org.junit.jupiter.api.Assertions.assertFalse
import java.nio.file.NoSuchFileException
import java.nio.file.DirectoryNotEmptyException
import java.io.IOException
import java.security.Permission
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import com.google.gson.JsonParser


class ToolCommandLineTest {
	var outputDirectory = new File("commandLine")
	SecurityManager securityManager = System.getSecurityManager()

	@BeforeEach
	def void setUp() {
		System.setSecurityManager(new InterruptExitSecurityManager)
	}

	@Test
	def void outPutLocationTest() {
		val model = "test_resources/jsonEventsTest/Project.prj"
		val String[] arguments = #["-l", model, "-o" , outputDirectory.absolutePath]
		  try {
			org.eclipse.comma.standard.project.generator.Main.main(arguments)
		  } catch (InterruptExit e) {
		  	val expectedResult = Files.readAllLines(Paths.get("test_resources/jsonEventsTest/ExpectedtasksResults.json")).join("\n")
			val generatedResults = Files.readAllLines(Paths.get(outputDirectory + "/comma-gen/tasksResults.json")).join("\n").removePathFromTraceFile
	   																								  .replace("\\r","")
	   																								  .replace("\\n","")
	   																							  
			assertTrue(Files.isDirectory(outputDirectory.toPath),"The output directory commmandLine is not created")
			Assertions.assertEquals(JsonParser.parseString(expectedResult), JsonParser.parseString(generatedResults),"Failed on test case ")
		  }
	}

	@Test
	def void skipTaskTest() {
		val model = "test_resources/jsonEventsTest/Project.prj"
		val String[] arguments = #["-l", model, "-o" , outputDirectory.absolutePath, "-s", "monitoring"]
		val monitoringFolder = Paths.get(outputDirectory.absolutePath +"/comma-gen/monitoring")
		try {
			org.eclipse.comma.standard.project.generator.Main.main(arguments)
		} catch (InterruptExit e){
			assertFalse(Files.exists(monitoringFolder))
		}
	}

	@Test
	def void skipAndRunTaskAtSameTimeTest() {
		val model = "test_resources/jsonEventsTest/Project.prj"
		val String[] arguments = #["-l", model, "-o" , outputDirectory.absolutePath, "-s", "monitoring",  "-r", "documentation"]
		try{
			org.eclipse.comma.standard.project.generator.Main.main(arguments)
		}catch(InterruptExit e) {
			assertFalse(Files.exists(outputDirectory.toPath))
		}
	}

	@Test
	def void modelSyntaxErrorTest() {
		val model = "test_resources/FailingModel/Project.prj"
		val String[] arguments = #["-l", model, "-o" , outputDirectory.absolutePath]
		try{
			org.eclipse.comma.standard.project.generator.Main.main(arguments)
		}catch(InterruptExit e) {
			assertFalse(Files.exists(outputDirectory.toPath))
		}
	}

	@Test
	def void externalNameSpaceTest() {
		val model = "test_resources/NameSpaceModel/Project.prj"
		val types = "test_resources/NameSpaceModel/colors;test_resources/NameSpaceModel/interfaces"
		val String[] arguments = #["-l", model, "-o" , outputDirectory.absolutePath, "-mp", types]
		try{
			org.eclipse.comma.standard.project.generator.Main.main(arguments)
		}catch(InterruptExit e) {
			assertTrue(Files.exists(outputDirectory.toPath))
		}
	}

	@AfterEach
	def void tearDown(){
		cleanUpFile(outputDirectory.absolutePath)
		System.setSecurityManager(securityManager)
	}
	
	/*
	 * ================= helping methods =============
	 */
	
	def cleanUpFile(String path) {
		val directory = new File(path)
		try {
			 if (directory.isDirectory()) {
            	for (File f : directory.listFiles()) {
                	cleanUpFile(f.absolutePath);
            	}
        	}
        	//call delete to delete files and empty directory
        	directory.delete
        }
        catch(NoSuchFileException e) {
            System.out.println(directory+" does not exists")
        }
        catch(DirectoryNotEmptyException e) {
            System.out.println("Directory is not empty.")
        }
        catch(IOException e) {
            System.out.println("Invalid permissions.")
        }
	}
	
	def String removePathFromTraceFile(String generatedResults) {
		var newGeneratedResults = new StringBuffer(generatedResults)
		var startIndex = newGeneratedResults.indexOf("traceFile",0)
		var endIndex = 0
		while(startIndex != -1) {
			endIndex = newGeneratedResults.indexOf(",", startIndex )

			var tracePath = newGeneratedResults.substring(startIndex +  "traceFile".length() + 3, endIndex)

			newGeneratedResults.replace(startIndex  + "traceFile".length() + 4, endIndex, getNameOfEventFile(tracePath)).toString()
			startIndex = newGeneratedResults.indexOf("traceFile", endIndex)
		}
		return newGeneratedResults.toString
	}

	def String getNameOfEventFile(String tracePath) {
		return tracePath.substring(tracePath.lastIndexOf(File.separator) + 1, tracePath.length())
	}
	

	//The CommaMain.xtend contains  system.exit calls. This system.exit makes the JVM  exist before the test code is finished. Which leads the test result unfinished.
	//These classes (InterruptExit and InterruptExitSecurityManager) are used to interrupt the system.exit call that is used in the CommaMain, so that the test code can finish and 
	//produces appropriate test result.
	protected static class InterruptExit extends SecurityException {
    	public int exitStatus = 0
		new(int status) {
			exitStatus = status
		}
	}

	private static class InterruptExitSecurityManager extends SecurityManager {
    	override void checkExit(int status) {
        	super.checkExit(status)
        	throw new InterruptExit(status)
    	}
    	override void checkPermission(Permission perm){}
	}
}