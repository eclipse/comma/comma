/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.tests

import com.google.inject.Inject
import org.eclipse.comma.project.project.ProjectDescription
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(StandardProjectInjectorProvider)
class StandardProjectParsingTest {

	@Inject
	ParseHelper<ProjectDescription> parseHelper

	// Just a basic test with the minimal required parts
	@Test
	def void loadSimpleModel() {
		val result = parseHelper.parse('''
			import "CameraSpec.sm"

			Project Camera {

			}
		''')
		Assertions.assertNotNull(result)
		Assertions.assertTrue(result.eResource.errors.isEmpty)
	}

	// Test with all syntactical constructs
	@Test
	def void loadModel1() {
		val result = parseHelper.parse('''
			import "Camera.interface"

			Project Camera {


				Generate Documentations {
				 	doc for interface ICamera {
					 	template = "Template.docx"
					 	targetFile = "Result.docx"
					 	author = "John Smith"
					 	role = "Architect"
					}
				}

				Generate Monitors {
					monitor for interface ICamera {
						trace files
						"trace.traces"
					}
				}
			}
		''')
		Assertions.assertNotNull(result)
	}

	@Test
	def void loadModel2() {
		val result = parseHelper.parse('''
			import "ICamera.interface"

			Project Camera {


				Generate Documentations {
					doc for interface ICamera {
						template = "Template.docx"
						DHF = 123456
						targetFile = "Result.docx"
						author = "John Smith"
						role = "Architect"

						mappings = cppMappings
					}
				}

				Generate Monitors {
					mon for interface ICamera {
						trace files
						"trace.traces"
					}
				}

				Generate CPP {
					cppTask for interface ICamera {
						mappings = cppMappings
					}

					cppTask2 for interface ICamera {
						mappings = cppMappings
					}
				}

				Type Mappings {
					cppMappings {
						int -> "long"
						interface ICamera {
							string -> "reftype(std::wstring)"
							myType -> "float"
						}
					}
				}
			}
		''')
		Assertions.assertNotNull(result)
		Assertions.assertFalse(result.eResource.errors.isEmpty)
	}

	@Test
	def void loadInterfaces() {
		val result = parseHelper.parse('''
			import "ICamera.if"

			Project Camera {
				Compound Interfaces {
					iCamera {
					version "1.0"
					description "Demo project for ICamera"
					interfaces
						ICamera
						ILogging
					}
				}
			}
		''')
		Assertions.assertNotNull(result)
		Assertions.assertTrue(result.eResource.errors.isEmpty)
	}

	@Test
	def void loadCPP() {
		val result = parseHelper.parse('''
			import "ICamera.interface"

			Project Camera {
				Compound Interfaces {
					iCamera {
						version "1.0"
						description "Demo project for ICamera"
					}
				}

				Generate CPP {
					cppTask for interface ICamera
					cppTask2 for interface ICamera {
						mappings = cppMappings
					}
				}

			}
		''')
		Assertions.assertNotNull(result)
		Assertions.assertFalse(result.eResource.errors.isEmpty)
	}

	@Test
	def void loadDocumentation() {
		val result = parseHelper.parse('''
			import "ICamera.interface"

			Project Camera {
				Compound Interfaces {
					iCamera {
						version "1.0"
						description "Demo project for ICamera"
					}
				}

				Generate Documentations {
					doc for interface iCamera {
						template = "Template.docx"
						DHF = 123456
						targetFile = "Result.docx"
						author = "John Smith"
						role = "Architect"
					}
				}

			}
		''')
		Assertions.assertNotNull(result)
		Assertions.assertFalse(result.eResource.errors.isEmpty)
	}
}
