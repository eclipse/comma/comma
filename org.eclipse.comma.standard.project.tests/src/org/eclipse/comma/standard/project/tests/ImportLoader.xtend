/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.tests

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.util.StringInputStream

class ImportLoader {

	def static events(ResourceSet set){
		val res = set.createResource(URI.createURI("trace.events"));
		res.load(new StringInputStream(
			'''
			connections
						(Client1, p, IWorker, Server, ev)
						
						events
						
						Command 0.0 0.0 Client1 p Server ev IWorker start
						End
						
						Reply 0.1 0.04 Server p Client1 ev IWorker start
						enum Status STARTED 0
						End
						
						Notification 0.2 1.96 Server p Client1 ev IWorker status
						enum Activity WORKING 0
						End
						
						Notification 0.3 2.0 Server p Client1 ev IWorker status
						enum Activity WORKING 0
						End
						
						Signal 0.4 1.0 Client1 p Server ev IWorker sleep
						End
						
						Notification 0.5 1.0 Server p Client1 ev IWorker status
						enum Activity SLEEPING 1
						End
						
						Notification 0.6 2.0 Server p Client1 ev IWorker status
						enum Activity SLEEPING 1
						End
						
						Signal 0.7 1.05 Client1 p Server ev IWorker wakeup
						End
						
						Notification 0.8 0.95 Server p Client1 ev IWorker status
						enum Activity WORKING 0
						End
						
						Notification 0.9 2.0 Server p Client1 ev IWorker status
						enum Activity WORKING 0
						End
						
						Notification 0.10 2.0 Server p Client1 ev IWorker status
						enum Activity WORKING 0
						End
						
						Signal 0.11 0.05 Client1 p Server ev IWorker sleep
						End
			
			'''), emptyMap)
	}

	def static iUniversitySm(ResourceSet set) {
		val res = set.createResource(URI.createURI("IUniversity.interface"));
		res.load(new StringInputStream(
			'''
				import "IUniversity.signature" 
				
				interface IUniversity
				
					variables  
					int number 
					bool notificationPending
					Person newStudent 
					 
					init 
					number := 0
					notificationPending := false
				
				machine main {	
					//In this state the user can initiate the enrollment process. Other commands are not allowed
					initial state Process {
						transition trigger: startEnrollment
							do: reply
							next state: Enrolling
					}
					
					state Enrolling {
						//A person is added and enrolled in an university
						//The confirmation is given by notifying the client for a new student
						//The new student has the same name and age as the person and is assigned with a student number
						//It is not possible to add a new person before receiving teh notification
						
						transition trigger: add(Person p) guard: NOT notificationPending
							do: 
								notificationPending := true
								newStudent := p
								reply
							next state: Enrolling
							
						transition guard: notificationPending 
							do: 
								number := number + 1
								newStudent(IUniversity::Student{name = newStudent.name, age = newStudent.age, number = number})
								notificationPending := false
							next state: Enrolling
							
						transition trigger: endEnrollment guard: NOT notificationPending
							do:
								reply
							next state: Process
					}
				}
				
				timing constraints
				TR1 in state Enrolling command endEnrollment - [ .. 32.0 ms ] -> reply
				TR2 in state Process command endEnrollment - [ 10.0 ms .. 100.0 ms ] -> notification newStudent
				
				data constraints 
				Name in state Enrolling command endEnrollment until command add where NOT false
			'''
		), emptyMap);
	}

	def static iUniversityIf(ResourceSet set) {
		val res = set.createResource(URI.createURI("IUniversity.signature"));
		res.load(new StringInputStream(
			'''
				signature IUniversity 
					types
					/* Simple interface that illustrates the usage of records.
					 * Record types can be defined at the global scope (type Person) of in an interface (Student)
					 */
					record Person {
						string name,
						int age
					}
					record Student {
						string name,
						int age,
						int number
					}
				
					commands
					void add(Person p)
					void startEnrollment
					void endEnrollment
				
					notifications
					newStudent(Student s)
			'''
		), emptyMap);
	}

	def static studentTestTraces(ResourceSet set) {
		val res = set.createResource(URI.createURI("StudentTest.traces"));
		res.load(new StringInputStream(
			'''
				import "IUniversity.signature"
				
				server Server on 192.68.32.12    
				client Client1 on 192.68.32.1 uses IUniversity   
				
				
				Time: 2016-11-02 10:56:00.0050
				Timestamp: 0.0000
				src address: 192.68.32.1 
				dest address: 192.68.32.12  
				Interface: IUniversity
				Command: startEnrollment
				
				Time: 2016-11-02 10:56:00.0055
				Timestamp: 0.0005
				src address: 192.68.32.12 
				dest address: 192.68.32.1  
				Interface: IUniversity
				Command: startEnrollment OK   
				
				Time: 2016-11-02 10:56:00.0070
				Timestamp: 0.0015
				src address: 192.68.32.1 
				dest address: 192.68.32.12  
				Interface: IUniversity 
				Command: add
				Parameter: IUniversity::Person : IUniversity::Person{name = "Jan Janssen", age = 19}       
				
				Time: 2016-11-02 10:56:00.0075 
				Timestamp: 0.0005
				src address: 192.68.32.12 
				dest address: 192.68.32.1  
				Interface: IUniversity
				Command: add OK    
				
				Time: 2016-11-02 10:56:00.0075 
				Timestamp: 0.0000
				src address: 192.68.32.12 
				dest address: 192.68.32.1  
				Interface: IUniversity
				Command: newStudent NOTIFY      
				Parameter: IUniversity::Student : IUniversity::Student{name = "Jan Janssen", age = 19, number = 1}   
				
				Time: 2016-11-02 10:56:00.0180
				Timestamp: 0.0005
				src address: 192.68.32.1 
				dest address: 192.68.32.12  
				Interface: IUniversity
				Command: endEnrollment
				
				Time: 2016-11-02 10:56:00.0190 
				Timestamp: 0.0010
				src address: 192.68.32.12 
				dest address: 192.68.32.1  
				Interface: IUniversity
				Command: endEnrollment OK       
				
				//Uncomment the following to see an example of error trace
				
				/*
				Time: 2016-11-02 10:56:00.0100
				Timestamp: 0.0025
				src address: 192.68.32.1 
				dest address: 192.68.32.12   
				Interface: IUniversity
				Command: add
				Parameter: IUniversity::Person : IUniversity::Person{name = "Jan Janssen", age = 19}     
				
				Time: 2016-11-02 10:56:00.0175 
				Timestamp: 0.0075
				src address: 192.68.32.12 
				dest address: 192.68.32.1  
				Interface: IUniversity
				Command: add OK      
				
				 */
				
				
				
				
			'''
		), emptyMap);
	}
	
	def static interfaceIf(ResourceSet set) {
		val res = set.createResource(URI.createURI("interface.signature"));
		res.load(new StringInputStream(
			'''
				signature IWorker
					types
					enum Status {STARTED STOPPED}
					enum Activity {WORKING SLEEPING}
					
					commands
					Status start
					
					signals
					sleep
					wakeup
					stop
					
					notifications
					status(Activity a)
					startedWorking
			'''
		), emptyMap);
	}
	
	
	def static interfaceWorker(ResourceSet set) {
		val res = set.createResource(URI.createURI("IWorker.interface"));
		res.load(new StringInputStream(
			'''
				import "interface.signature" 
				
				interface IWorker
				
				machine main {	
					//In this state the user can initiate the enrollment process. Other commands are not allowed
					initial state Process {
					}
				}
			'''
		), emptyMap);
	}
}
