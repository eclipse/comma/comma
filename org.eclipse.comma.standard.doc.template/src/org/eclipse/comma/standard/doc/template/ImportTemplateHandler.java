/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.doc.template;

import org.eclipse.comma.project.ui.handler.AbstractTemplateHandler;


public class ImportTemplateHandler extends AbstractTemplateHandler {

	private static final String OUTPUT_LOCATION = "Template.docx";
	private static final String GENERIC_TEMPLATE = "platform:/plugin/org.eclipse.comma.standard.doc.template/template/Template.docx";

	public ImportTemplateHandler() {
		super(GENERIC_TEMPLATE, OUTPUT_LOCATION);		
	}


}
