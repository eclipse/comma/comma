<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright (c) 2021 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<?eclipse version="3.0"?>
<plugin>
	<extension
		point="org.eclipse.ui.editors">
		<editor
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
			contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
			default="true"
			extensions="prj"
			icon="platform:/plugin/org.eclipse.comma.icons/icons/file_project.png"
			id="org.eclipse.comma.standard.project.StandardProject"
			name="StandardProject Editor">
		</editor>
	</extension>
	<extension
		point="org.eclipse.ui.handlers">
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
			commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
			<activeWhen>
				<reference
					definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
			commandId="org.eclipse.comma.standard.project.StandardProject.validate">
		<activeWhen>
			<reference
					definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
			</reference>
		</activeWhen>
		</handler>
		<!-- copy qualified name -->
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedNameHandler"
			commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName">
			<activeWhen>
				<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened" />
			</activeWhen>
		</handler>
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedNameHandler"
			commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName">
			<activeWhen>
				<and>
					<reference definitionId="org.eclipse.comma.standard.project.StandardProject.XtextEditor.opened" />
					<iterate>
						<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
					</iterate>
				</and>
			</activeWhen>
		</handler>
	</extension>
	<extension point="org.eclipse.core.expressions.definitions">
		<definition id="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
			<and>
				<reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
				<with variable="activeEditor">
					<test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName"
						value="org.eclipse.comma.standard.project.StandardProject"
						forcePluginActivation="true"/>
				</with>
			</and>
		</definition>
		<definition id="org.eclipse.comma.standard.project.StandardProject.XtextEditor.opened">
			<and>
				<reference definitionId="isXtextEditorActive"/>
				<with variable="activeEditor">
					<test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName"
						value="org.eclipse.comma.standard.project.StandardProject"
						forcePluginActivation="true"/>
				</with>
			</and>
		</definition>
	</extension>
	<extension
			point="org.eclipse.ui.preferencePages">
		<page
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject"
			name="StandardProject">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
		</page>
		<page
			category="org.eclipse.comma.standard.project.StandardProject"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject.coloring"
			name="Syntax Coloring">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
		</page>
		<page
			category="org.eclipse.comma.standard.project.StandardProject"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject.templates"
			name="Templates">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
		</page>
	</extension>
	<extension
			point="org.eclipse.ui.propertyPages">
		<page
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject"
			name="StandardProject">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
			<enabledWhen>
				<adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
			<filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
		</page>
	</extension>
	<extension
		point="org.eclipse.ui.keywords">
		<keyword
			id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"
			label="StandardProject"/>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
	<command
			description="Trigger expensive validation"
			id="org.eclipse.comma.standard.project.StandardProject.validate"
			name="Validate">
	</command>
	<!-- copy qualified name -->
	<command
			id="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
			categoryId="org.eclipse.ui.category.edit"
			description="Copy the qualified name for the selected element"
			name="Copy Qualified Name">
	</command>
	<command
			id="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
			categoryId="org.eclipse.ui.category.edit"
			description="Copy the qualified name for the selected element"
			name="Copy Qualified Name">
	</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.edit">
			 <command
				 commandId="org.eclipse.comma.standard.project.StandardProject.validate"
				 style="push"
				 tooltip="Trigger expensive validation">
			<visibleWhen checkEnabled="false">
				<reference
					definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
				</reference>
			</visibleWhen>
		</command>
		</menuContribution>
		<!-- copy qualified name -->
		<menuContribution locationURI="popup:#TextEditorContext?after=copy">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
				style="push" tooltip="Copy Qualified Name">
				<visibleWhen checkEnabled="false">
					<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened" />
				</visibleWhen>
			</command>
		</menuContribution>
		<menuContribution locationURI="menu:edit?after=copy">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.EditorCopyQualifiedName"
				style="push" tooltip="Copy Qualified Name">
				<visibleWhen checkEnabled="false">
					<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened" />
				</visibleWhen>
			</command>
		</menuContribution>
		<menuContribution locationURI="popup:org.eclipse.xtext.ui.outline?after=additions">
			<command commandId="org.eclipse.xtext.ui.editor.copyqualifiedname.OutlineCopyQualifiedName"
				style="push" tooltip="Copy Qualified Name">
				<visibleWhen checkEnabled="false">
					<and>
						<reference definitionId="org.eclipse.comma.standard.project.StandardProject.XtextEditor.opened" />
						<iterate>
							<adapt type="org.eclipse.xtext.ui.editor.outline.IOutlineNode" />
						</iterate>
					</and>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
					<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
					</reference>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
			commandId="org.eclipse.xtext.ui.editor.FindReferences">
			<activeWhen>
				<reference
					definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<!-- adding resource factories -->
	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="prj">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
		<resourceServiceProvider
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
			uriExtension="prj">
		</resourceServiceProvider>
	</extension>
	<!-- marker definitions for org.eclipse.comma.standard.project.StandardProject -->
	<extension
			id="standardproject.check.fast"
			name="StandardProject Problem"
			point="org.eclipse.core.resources.markers">
		<super type="org.eclipse.xtext.ui.check.fast"/>
		<persistent value="true"/>
	</extension>
	<extension
			id="standardproject.check.normal"
			name="StandardProject Problem"
			point="org.eclipse.core.resources.markers">
		<super type="org.eclipse.xtext.ui.check.normal"/>
		<persistent value="true"/>
	</extension>
	<extension
			id="standardproject.check.expensive"
			name="StandardProject Problem"
			point="org.eclipse.core.resources.markers">
		<super type="org.eclipse.xtext.ui.check.expensive"/>
		<persistent value="true"/>
	</extension>

	<extension point="org.eclipse.xtext.builder.participant">
		<participant
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.builder.IXtextBuilderParticipant"
			fileExtensions="prj"/>
	</extension>
	<extension point="org.eclipse.ui.preferencePages">
		<page
			category="org.eclipse.comma.standard.project.StandardProject"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject.compiler.preferencePage"
			name="Compiler">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
		</page>
	</extension>
	<extension point="org.eclipse.ui.propertyPages">
		<page
			category="org.eclipse.comma.standard.project.StandardProject"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.builder.preferences.BuilderPreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject.compiler.propertyPage"
			name="Compiler">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
			<enabledWhen>
				<adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
			<filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
		</page>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?after=xtext.ui.openDeclaration">
			<command
				commandId="org.eclipse.xtext.ui.OpenGeneratedFileCommand"
				id="org.eclipse.comma.standard.project.StandardProject.OpenGeneratedCode"
				style="push">
				<visibleWhen checkEnabled="false">
					<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened" />
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.generator.trace.OpenGeneratedFileHandler"
			commandId="org.eclipse.xtext.ui.OpenGeneratedFileCommand">
			<activeWhen>
				<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened" />
			</activeWhen>
		</handler>
	</extension>
	<!-- Quick Outline -->
	<extension
		point="org.eclipse.ui.handlers">
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.outline.quickoutline.ShowQuickOutlineActionHandler"
			commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline">
			<activeWhen>
				<reference
					definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
		<command
			description="Open the quick outline."
			id="org.eclipse.xtext.ui.editor.outline.QuickOutline"
			name="Quick Outline">
		</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.open">
			<command commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline"
				style="push"
				tooltip="Open Quick Outline">
				<visibleWhen checkEnabled="false">
					<reference definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened"/>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<!-- quickfix marker resolution generator for org.eclipse.comma.standard.project.StandardProject -->
	<extension
			point="org.eclipse.ui.ide.markerResolution">
		<markerResolutionGenerator
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
			markerType="org.eclipse.comma.standard.project.ui.standardproject.check.fast">
			<attribute
				name="FIXABLE_KEY"
				value="true">
			</attribute>
		</markerResolutionGenerator>
		<markerResolutionGenerator
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
			markerType="org.eclipse.comma.standard.project.ui.standardproject.check.normal">
			<attribute
				name="FIXABLE_KEY"
				value="true">
			</attribute>
		</markerResolutionGenerator>
		<markerResolutionGenerator
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.quickfix.MarkerResolutionGenerator"
			markerType="org.eclipse.comma.standard.project.ui.standardproject.check.expensive">
			<attribute
				name="FIXABLE_KEY"
				value="true">
			</attribute>
		</markerResolutionGenerator>
	</extension>
	<!-- Rename Refactoring -->
	<extension point="org.eclipse.ui.handlers">
		<handler
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.DefaultRenameElementHandler"
			commandId="org.eclipse.xtext.ui.refactoring.RenameElement">
			<activeWhen>
				<reference
					definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.edit">
			<command commandId="org.eclipse.xtext.ui.refactoring.RenameElement"
				style="push">
				<visibleWhen checkEnabled="false">
					<reference
						definitionId="org.eclipse.comma.standard.project.StandardProject.Editor.opened">
					</reference>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.preferencePages">
		<page
			category="org.eclipse.comma.standard.project.StandardProject"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.refactoring.ui.RefactoringPreferencePage"
			id="org.eclipse.comma.standard.project.StandardProject.refactoring"
			name="Refactoring">
			<keywordReference id="org.eclipse.comma.standard.project.ui.keyword_StandardProject"/>
		</page>
	</extension>
	<extension point="org.eclipse.compare.contentViewers">
		<viewer id="org.eclipse.comma.standard.project.StandardProject.compare.contentViewers"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
			extensions="prj">
		</viewer>
	</extension>
	<extension point="org.eclipse.compare.contentMergeViewers">
		<viewer id="org.eclipse.comma.standard.project.StandardProject.compare.contentMergeViewers"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.compare.InjectableViewerCreator"
			extensions="prj" label="StandardProject Compare">
		</viewer>
	</extension>
	<extension point="org.eclipse.ui.editors.documentProviders">
		<provider id="org.eclipse.comma.standard.project.StandardProject.editors.documentProviders"
			class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.xtext.ui.editor.model.XtextDocumentProvider"
			extensions="prj">
		</provider>
	</extension>
	<extension point="org.eclipse.team.core.fileTypes">
		<fileTypes
			extension="prj"
			type="text">
		</fileTypes>
	</extension>

	<extension point="org.eclipse.core.expressions.definitions">
		<definition id="org.eclipse.comma.project.ui.visiblewhenprj">
			<adapt type="org.eclipse.core.resources.IResource">
				<test forcePluginActivation="true"
                	property="org.eclipse.core.resources.extension"
                	value="prj">
				</test>
			</adapt>
		</definition>
	</extension>


	  

	<extension point="org.eclipse.debug.ui.launchShortcuts">
    <shortcut
          class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.comma.project.ui.handler.RunGeneratorsLaunchShortcut"
          icon="platform:/plugin/org.eclipse.comma.icons/icons/comma_runas.png"
          id="org.eclipse.comma.launchShortcut.generator"
          label="Run generators"
          modes="run"
          path="a">
       <contextualLaunch>
          <contextLabel
                label="Run generators"
                mode="run">
          </contextLabel>
          <enablement>
             <with
                   variable="selection">
             </with>
             <count
                   value="1">
             </count>
             <iterate>
                <adapt
                      type="org.eclipse.core.resources.IResource">
                   <test
                         property="org.eclipse.core.resources.name"
                         value="*prj">
                   </test>
                </adapt>
             </iterate>
          </enablement>
       </contextualLaunch>
    </shortcut>
    <shortcut
          class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.comma.project.ui.handler.RunGeneratorsTaskSelectionLaunchShortcut"
          icon="platform:/plugin/org.eclipse.comma.icons/icons/comma_runas.png"
          id="org.eclipse.comma.launchShortcut.generatorTaskSelection"
          label="Run generators (select tasks)"
          modes="run"
          path="b">
       <contextualLaunch>
          <contextLabel
                label="Run generators (select tasks)"
                mode="run">
          </contextLabel>
          <enablement>
             <with
                   variable="selection">
             </with>
             <count
                   value="1">
             </count>
             <iterate>
                <adapt
                      type="org.eclipse.core.resources.IResource">
                   <test
                         property="org.eclipse.core.resources.name"
                         value="*prj">
                   </test>
                </adapt>
             </iterate>
          </enablement>
       </contextualLaunch>
    </shortcut>
		  <shortcut
          class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.comma.project.ui.handler.MonitorLaunchShortcut"
          icon="platform:/plugin/org.eclipse.comma.icons/icons/comma_runas.png"
          id="org.eclipse.comma.launchShortcut.monitor"
          label="Generate and execute monitoring"
          modes="run"
          path="c">
		     <contextualLaunch>
		        <contextLabel
		              label="Generate and execute monitoring"
		              mode="run">
		        </contextLabel>
		        <enablement>
		           <with
		                 variable="selection">
		           </with>
		           <count
		                 value="1">
		           </count>
		           <iterate>
		              <adapt
		                    type="org.eclipse.core.resources.IResource">
		                 <test
		                       property="org.eclipse.core.resources.name"
		                       value="*prj">
		                 </test>
		              </adapt>
		           </iterate>
		        </enablement></contextualLaunch>
		  </shortcut>
		  <shortcut
          class="org.eclipse.comma.standard.project.ui.StandardProjectExecutableExtensionFactory:org.eclipse.comma.project.ui.handler.SimulatorLaunchShortcut"
          icon="platform:/plugin/org.eclipse.comma.icons/icons/comma_runas.png"
          id="org.eclipse.comma.launchShortcut.simulator"
          label="Generate and execute simulator"
          modes="run"
          path="d">
		     <contextualLaunch>
		        <contextLabel
		              label="Generate and execute simulator"
		              mode="run">
		        </contextLabel>
		        <enablement>
		           <with
		                 variable="selection">
		           </with>
		           <count
		                 value="1">
		           </count>
		           <iterate>
		              <adapt
		                    type="org.eclipse.core.resources.IResource">
		                 <test
		                       property="org.eclipse.core.resources.name"
		                       value="*prj">
		                 </test>
		              </adapt>
		           </iterate>
		        </enablement></contextualLaunch>
		  </shortcut>
	  </extension>

	  <extension
       point="org.eclipse.ui.menus">
		<menuContribution
          allPopups="false"
          locationURI="popup:org.eclipse.ui.popup.any?after=additions">
			<command
             commandId="org.eclipse.comma.doc.template.ImportTemplateCommand"
             label="Import Documentation Template"
             style="push">
				<visibleWhen checkEnabled = "false">
					<with variable = "activeMenuSelection">
						<iterate ifEmpty="false">
							<adapt type="org.eclipse.core.resources.IResource">
								<test property="org.eclipse.core.resources.extension" value="prj">
								</test>
							</adapt>
						</iterate>
					</with>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension
		point="org.eclipse.ui.handlers">
		<handler
			class="org.eclipse.comma.standard.doc.template.ImportTemplateHandler"
			commandId = "org.eclipse.comma.doc.template.ImportTemplateCommand">
		</handler>
	</extension>
	<extension
       point="org.eclipse.ui.commands">
	  <command
          id="org.eclipse.comma.doc.template.ImportTemplateCommand"
          name="Import Template">
    </command>
    </extension>

	  <extension
         point="org.eclipse.ui.newWizards">
		<category
            id="org.eclipse.comma"
            name="CommaSuite">
		</category>
		<wizard
            category="org.eclipse.comma"
            class="org.eclipse.comma.standard.project.ui.wizard.StandardProjectWizard"
            hasPages="true"
            icon="platform:/plugin/org.eclipse.comma.icons/icons/file_project.png"
            id="org.eclipse.comma.standard.project.ui.wizard.commaproject"
            name="CommaSuite Project"
            project="true">
		</wizard>
	</extension>
</plugin>
