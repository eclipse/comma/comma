/*
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.ui.wizard;

import org.eclipse.comma.project.ui.wizard.ProjectWizard;

public class StandardProjectWizard extends ProjectWizard {
	private static final String STANDARD_TEMPLATE_LOCATION = "platform:/plugin/org.eclipse.comma.standard.project.ui/templates/";
	
	public String getTemplateLocation() {
		return STANDARD_TEMPLATE_LOCATION;
	}
}
