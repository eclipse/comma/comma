/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.comma.standard.project.ui.contentassist

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class StandardProjectProposalProvider extends AbstractStandardProjectProposalProvider {
	
	final String TASK_DOCUMENTATION = '''
		documentation for interface %s {
			template = "Template.docx"				
			targetFile = "Documentation.docx"
			author = "John Smith"
			role = "R&D: SW Designer"
		}
	'''
	
	override getDocumentationTask() {
		TASK_DOCUMENTATION
	}
	
}
