# Eclipse CommaSuite

<img src="org.eclipse.comma.standard.branding/logo/banner.png" alt="logo" width="500"/>

Welcome to the Eclipse CommaSuite repository!

The domain-specific language of Eclipse CommaSuite allows the specification of the provided and required interfaces of a software component.

Handy links:
- [Eclipse project page](https://projects.eclipse.org/projects/technology.comma)
- [Contributing](CONTRIBUTING.md)
- [Developers guide](docs/developing.md)
- [Jenkins CI](https://ci.eclipse.org/comma/)
- [License](LICENSE.txt)