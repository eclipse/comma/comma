# Third-party Content

## ANTLR (3.2.0, 4.7.2)

- License: BSD 3-clause license
- Project: https://www.antlr.org/
- Source: https://github.com/antlr/antlr4

## ASM (9.1.0)

- License: 3-Clause BSD License
- Project: https://asm.ow2.io/
- Source: https://gitlab.ow2.org/asm/asm

## Apache Batik (1.14.0)

- License: Apache License 2.0
- Project: https://xmlgraphics.apache.org/batik/
- Source: https://github.com/apache/xmlgraphics-batik

## Apache Commons (1.2.0, 1.3.0, 1.4.0, 2.6.0, 2.8.0)

- License: Apache License 2.0
- Project: https://ant.apache.org/
- Source: https://github.com/apache/commons-lang

## Apache Felix (2.1.24)

- License: Apache License 2.0
- Project: https://felix.apache.org/
- Source: https://github.com/apache/felix-dev

## Apache Log4j (1.2.15)

- License: Apache License 2.0
- Project: https://logging.apache.org/log4j
- Source: https://github.com/apache/log4j

## Apache POI (4.1.0.201910021432)

- License: Eclipse Public License 1.0
- Project: https://poi.apache.org/
- Source: https://github.com/apache/poi

## Apache XML Graphics (2.6.0)

- License: Apache License 2.0
- Project: https://xmlgraphics.apache.org/commons/
- Source: https://xmlgraphics.apache.org/fop/download.html

## Bouncy Castle Crypto Package (1.69.0, 1.69.0)

- License: Custom
- Project: https://www.bouncycastle.org/java.html
- Source: https://github.com/bcgit/bc-java

## Classgraph (4.8.117)

- License: MIT
- Project: https://github.com/classgraph/classgraph
- Source: https://github.com/classgraph/classgraph

## Google Guava (30.1.0)

- License: Apache License 2.0
- Project: https://github.com/google/guava
- Source: https://github.com/google/guava

## Google Guice (3.0.0)

- License: Apache License 2.0
- Project: https://github.com/google/guice
- Source: https://github.com/google/guice

## Gson (2.8.8)

- License: Apache License 2.0
- Project: https://github.com/google/gson
- Source: https://github.com/google/gson

## ICU (67.1.0)

- License: IBM ICU License
- Project: http://site.icu-project.org/
- Source: https://github.com/unicode-org/icu

## Java Hamcrest (1.3.0)

- License: BSD License
- Project: http://hamcrest.org/JavaHamcrest/
- Source: https://github.com/hamcrest/JavaHamcrest

## M2Doc (3.1.1.202101060924)

- License: Eclipse Public License 1.0
- Project: https://www.m2doc.org/
- Source: https://github.com/ObeoNetwork/M2Doc

## PlantUML (1.2021.5)

- License: GPL
- Project: https://plantuml.com/
- Source: https://github.com/plantuml/plantuml

## XZ Utils (1.9.0)

- License: Public domain
- Project: https://tukaani.org/xz/java.html
- Source: https://git.tukaani.org/?p=xz-java.git;a=summary

## Dashboards (NPM)

### 3d-view (2.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/3d-view

### a-big-triangle (1.0.3)
- License: MIT
- Source: https://github.com/mikolalysenko/a-big-triangle

### abs-svg-path (0.1.1)
- License: MIT
- Source: https://github.com/jkroso/abs-svg-path

### acorn (7.4.1)
- License: MIT
- Source: https://github.com/acornjs/acorn

### add-line-numbers (1.0.1)
- License: MIT
- Source: https://github.com/Jam3/add-line-numbers

### affine-hull (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/affine-hull

### almost-equal (1.1.0)
- License: MIT
- Source: https://github.com/mikolalysenko/almost-equal

### alpha-complex (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/alpha-complex

### alpha-shape (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/alpha-shape

### arr-flatten (1.1.0)
- License: MIT
- Source: https://github.com/jonschlinkert/arr-flatten

### array-bounds (1.0.1)
- License: MIT
- Source: https://github.com/dfcreative/array-bounds

### array-find-index (1.0.2)
- License: MIT
- Source: https://github.com/sindresorhus/array-find-index

### array-normalize (1.1.4)
- License: MIT
- Source: https://github.com/dfcreative/array-normalize

### array-range (1.0.1)
- License: MIT
- Source: https://github.com/mattdesl/array-range

### array-rearrange (2.2.2)
- License: MIT
- Source: https://github.com/dfcreative/array-rearrange

### atob-lite (1.0.0, 2.0.0)
- License: MIT
- Source: https://github.com/hughsk/atob-lite

### babel/runtime (7.13.10, 7.16.3)
- License: MIT
- Source: https://github.com/babel/babel

### barycentric (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/barycentric

### big-rat (1.0.4)
- License: MIT
- Source: https://github.com/rat-nest/big-rat

### binary-search-bounds (1.0.0, 2.0.5)
- License: MIT
- Source: https://github.com/mikolalysenko/binary-search-bounds

### bit-twiddle (0.0.2, 1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/bit-twiddle

### bitmap-sdf (1.0.3)
- License: MIT
- Source: https://github.com/dfcreative/bitmap-sdf

### bl (2.2.1)
- License: MIT
- Source: https://github.com/rvagg/bl

### bn.js (4.12.0)
- License: MIT
- Source: https://github.com/indutny/bn.js

### boundary-cells (2.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/boundary-cells

### box-intersect (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/box-intersect

### buffer-from (1.1.1)
- License: MIT
- Source: https://github.com/LinusU/buffer-from

### canvas-fit (1.5.0)
- License: MIT
- Source: https://github.com/hughsk/canvas-fit

### cdt2d (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/cdt2d

### cell-orientation (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/cell-orientation

### choojs/findup (0.2.1)
- License: MIT
- Source: https://github.com/choojs/findup

### circumcenter (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/circumcenter

### circumradius (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/circumradius

### clamp (1.0.1)
- License: MIT
- Source: https://github.com/hughsk/clamp

### clean-pslg (1.1.2)
- License: MIT
- Source: https://github.com/mikolalysenko/clean-pslg

### clsx (1.1.1, 1.1.1)
- License: MIT
- Source: https://github.com/lukeed/clsx

### color-alpha (1.0.4)
- License: MIT
- Source: https://github.com/colorjs/color-alpha

### color-id (1.1.0)
- License: MIT
- Source: https://github.com/colorjs/color-id

### color-name (1.1.3)
- License: MIT
- Source: https://github.com/dfcreative/color-name

### color-normalize (1.5.0)
- License: MIT
- Source: https://github.com/colorjs/color-normalize

### color-parse (1.3.8)
- License: MIT
- Source: https://github.com/colorjs/color-parse

### color-rgba (2.1.1)
- License: MIT
- Source: https://github.com/colorjs/color-rgba

### color-space (1.16.0)
- License: MIT
- Source: https://github.com/colorjs/color-space

### colormap (2.3.2)
- License: MIT
- Source: https://github.com/bpostlethwaite/colormap

### commander (2.20.3)
- License: MIT
- Source: https://github.com/tj/commander.js

### compare-angle (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/compare-angle

### compare-cell (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/compare-cells

### compare-oriented-cell (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/compare-oriented-cell

### compute-dims (1.1.0)
- License: MIT
- Source: https://github.com/compute-io/dims

### concat-stream (1.6.2, 2.0.0)
- License: MIT
- Source: https://github.com/maxogden/concat-stream

### const-max-uint32 (1.0.2)
- License: MIT
- Source: https://github.com/const-io/max-uint32

### const-pinf-float64 (1.0.0)
- License: MIT
- Source: https://github.com/const-io/pinf-float64

### convex-hull (1.0.3)
- License: MIT
- Source: https://github.com/mikolalysenko/convex-hull

### core-js (3.13.0, 3.19.2)
- License: MIT
- Source: https://github.com/zloirock/core-js

### core-util-is (1.0.2)
- License: MIT
- Source: https://github.com/isaacs/core-util-is

### country-regex (1.1.0)
- License: MIT
- Source: https://github.com/etpinard/country-regex

### css-font (1.2.0)
- License: MIT
- Source: https://github.com/dy/css-font

### css-font-size-keywords (1.0.0)
- License: MIT
- Source: https://github.com/jedmao/css-font-size-keywords

### css-font-stretch-keywords (1.0.1)
- License: MIT
- Source: https://github.com/jedmao/css-font-stretch-keywords

### css-font-style-keywords (1.0.1)
- License: MIT
- Source: https://github.com/jedmao/css-font-style-keywords

### css-font-weight-keywords (1.0.0)
- License: MIT
- Source: https://github.com/jedmao/css-font-weight-keywords

### css-global-keywords (1.0.1)
- License: MIT
- Source: https://github.com/jedmao/css-global-keywords

### css-system-font-keywords (1.0.0)
- License: MIT
- Source: https://github.com/jedmao/css-system-font-keywords

### css-vendor (2.0.8, 2.0.8)
- License: MIT
- Source: https://github.com/cssinjs/css-vendor

### csscolorparser (1.0.3)
- License: MIT
- Source: https://github.com/deanm/css-color-parser-js

### csstype (2.6.17, 3.0.8, 2.6.19, 3.0.10)
- License: MIT
- Source: https://github.com/frenic/csstype

### cubic-hermite (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/cubic-hermite

### cwise-compiler (1.1.3)
- License: MIT
- Source: https://github.com/scijs/cwise-compiler

### d (1.0.1)
- License: ISC
- Source: https://github.com/medikoo/d

### d3 (3.5.17)
- License: BSD-3-Clause
- Source: https://github.com/mbostock/d3

### d3-array (1.2.4)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-array

### d3-collection (1.0.7)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-collection

### d3-color (1.4.1)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-color

### d3-dispatch (1.0.6)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-dispatch

### d3-force (1.2.1)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-force

### d3-hierarchy (1.1.9)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-hierarchy

### d3-interpolate (1.4.0)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-interpolate

### d3-path (1.0.9)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-path

### d3-quadtree (1.0.7)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-quadtree

### d3-shape (1.3.7)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-shape

### d3-time (1.1.0)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-time

### d3-time-format (2.3.0)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-time-format

### d3-timer (1.0.10)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-timer

### deep-is (0.1.3)
- License: MIT
- Source: https://github.com/thlorenz/deep-is

### defined (1.0.0)
- License: MIT
- Source: https://github.com/substack/defined

### delaunay-triangulate (1.1.6)
- License: MIT
- Source: https://github.com/mikolalysenko/delaunay-triangulate

### detect-kerning (2.1.2)
- License: MIT
- Source: https://github.com/dy/detect-kerning

### dom-helpers (5.2.1, 5.2.1)
- License: MIT
- Source: https://github.com/react-bootstrap/dom-helpers

### double-bits (1.1.1)
- License: MIT
- Source: https://github.com/mikolalysenko/double-bits

### draw-svg-path (1.0.0)
- License: MIT
- Source: https://github.com/michaelrhodes/draw-svg-path

### dtype (2.0.0)
- License: MIT
- Source: https://github.com/shama/dtype

### dup (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/dup

### duplexify (3.7.1)
- License: MIT
- Source: https://github.com/mafintosh/duplexify

### earcut (2.2.2)
- License: ISC
- Source: https://github.com/mapbox/earcut

### edges-to-adjacency-list (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/edges-to-adjacency-list

### element-size (1.1.1)
- License: MIT
- Source: https://github.com/hughsk/element-size

### elementary-circuits-directed-graph (1.3.1)
- License: MIT
- Source: https://github.com/antoinerg/elementary-circuits-directed-graph

### emotion/hash (0.8.0, 0.8.0)
- License: MIT
- Source: https://github.com/emotion-js/emotion/tree/master/packages/hash

### end-of-stream (1.4.4)
- License: MIT
- Source: https://github.com/mafintosh/end-of-stream

### es5-ext (0.10.53)
- License: ISC
- Source: https://github.com/medikoo/es5-ext

### es6-iterator (2.0.3)
- License: MIT
- Source: https://github.com/medikoo/es6-iterator

### es6-promise (4.2.8)
- License: MIT
- Source: https://github.com/stefanpenner/es6-promise

### es6-symbol (3.1.3)
- License: ISC
- Source: https://github.com/medikoo/es6-symbol

### es6-weak-map (2.0.3)
- License: ISC
- Source: https://github.com/medikoo/es6-weak-map

### escodegen (1.14.3)
- License: BSD-2-Clause
- Source: https://github.com/estools/escodegen

### esprima (4.0.1)
- License: BSD-2-Clause
- Source: https://github.com/jquery/esprima

### estraverse (4.3.0)
- License: BSD-2-Clause
- Source: https://github.com/estools/estraverse

### esutils (2.0.3)
- License: BSD-2-Clause
- Source: https://github.com/estools/esutils

### events (3.3.0)
- License: MIT
- Source: https://github.com/Gozala/events

### ext (1.4.0)
- License: ISC
- Source: https://github.com/medikoo/es5-ext/tree/ext

### extract-frustum-planes (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/extract-frustum-planes

### falafel (2.2.4)
- License: MIT
- Source: https://github.com/substack/node-falafel

### fast-isnumeric (1.1.4)
- License: MIT
- Source: https://github.com/plotly/fast-isnumeric

### fast-levenshtein (2.0.6)
- License: MIT
- Source: https://github.com/hiddentao/fast-levenshtein

### filtered-vector (1.2.4)
- License: MIT
- Source: https://github.com/mikolalysenko/filtered-vector

### flatten-vertex-data (1.0.2)
- License: MIT
- Source: https://github.com/glo-js/flatten-vertex-data

### flip-pixels (1.0.2)
- License: MIT
- Source: https://github.com/dy/flip-pixels

### font-atlas (2.1.0)
- License: MIT
- Source: https://github.com/hughsk/font-atlas

### font-measure (1.2.2)
- License: MIT
- Source: https://github.com/dy/font-measure

### foreach (2.0.5)
- License: MIT
- Source: https://github.com/manuelstofer/foreach

### from2 (2.3.0)
- License: MIT
- Source: https://github.com/hughsk/from2

### function-bind (1.1.1)
- License: MIT
- Source: https://github.com/Raynos/function-bind

### functional-red-black-tree (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/functional-red-black-tree

### gamma (0.1.0)
- License: MIT
- Source: https://github.com/substack/gamma.js

### geojson-vt (3.2.1)
- License: ISC
- Source: https://github.com/mapbox/geojson-vt

### get-canvas-context (1.0.2)
- License: MIT
- Source: https://github.com/Jam3/get-canvas-context

### gl-axes3d (1.5.3)
- License: MIT
- Source: https://github.com/gl-vis/gl-axes3d

### gl-buffer (2.1.2)
- License: MIT
- Source: https://github.com/stackgl/gl-buffer

### gl-cone3d (1.5.2)
- License: MIT
- Source: https://github.com/gl-vis/gl-cone3d

### gl-constants (1.0.0)
- License: MIT
- Source: https://github.com/mattdesl/gl-constants

### gl-contour2d (1.1.7)
- License: MIT
- Source: https://github.com/gl-vis/gl-contour2d

### gl-error3d (1.0.16)
- License: MIT
- Source: https://github.com/gl-vis/gl-error3d

### gl-fbo (2.0.5)
- License: MIT
- Source: https://github.com/stackgl/gl-fbo

### gl-format-compiler-error (1.0.3)
- License: Unlicense
- Source: https://github.com/wwwtyro/gl-format-compiler-error

### gl-heatmap2d (1.1.1)
- License: MIT
- Source: https://github.com/gl-vis/gl-heatmap2d

### gl-line3d (1.2.1)
- License: MIT
- Source: https://github.com/gl-vis/gl-line3d

### gl-mat3 (1.0.0)
- License: Custom: http://badges.github.io/stability-badges/dist/stable.svg
- Source: https://github.com/gl-modules/gl-mat3

### gl-mat4 (1.2.0)
- License: Zlib
- Source: https://github.com/stackgl/gl-mat4

### gl-matrix (3.3.0)
- License: MIT
- Source: https://github.com/toji/gl-matrix

### gl-mesh3d (2.3.1)
- License: MIT
- Source: https://github.com/gl-vis/gl-mesh3d

### gl-plot2d (1.4.5)
- License: MIT
- Source: https://github.com/gl-vis/gl-plot2d

### gl-plot3d (2.4.7)
- License: MIT
- Source: https://github.com/gl-vis/gl-plot3d

### gl-pointcloud2d (1.0.3)
- License: MIT
- Source: https://github.com/gl-vis/gl-pointcloud2d

### gl-quat (1.0.0)
- License: Zlib
- Source: https://github.com/stackgl/gl-quat

### gl-scatter3d (1.2.3)
- License: MIT
- Source: https://github.com/gl-vis/gl-scatter3d

### gl-select-box (1.0.4)
- License: MIT
- Source: https://github.com/gl-vis/gl-select-box

### gl-select-static (2.0.7)
- License: MIT
- Source: https://github.com/gl-vis/gl-select-static

### gl-shader (4.2.1)
- License: MIT
- Source: https://github.com/stackgl/gl-shader

### gl-spikes2d (1.0.2)
- License: MIT
- Source: https://github.com/gl-vis/gl-spikes2d

### gl-spikes3d (1.0.10)
- License: MIT
- Source: https://github.com/gl-vis/gl-spikes3d

### gl-state (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/gl-state

### gl-streamtube3d (1.4.1)
- License: MIT
- Source: https://github.com/gl-vis/gl-streamtube3d

### gl-surface3d (1.6.0)
- License: MIT
- Source: https://github.com/gl-vis/gl-surface3d

### gl-text (1.1.8)
- License: MIT
- Source: https://github.com/a-vis/gl-text

### gl-texture2d (2.1.0)
- License: MIT
- Source: https://github.com/stackgl/gl-texture2d

### gl-util (3.1.3)
- License: MIT
- Source: https://github.com/dy/gl-util

### gl-vao (1.3.0)
- License: MIT
- Source: https://github.com/stackgl/gl-vao

### gl-vec3 (1.1.3)
- License: Custom: http://badges.github.io/stability-badges/dist/stable.svg
- Source: https://github.com/stackgl/gl-vec3

### gl-vec4 (1.0.1)
- License: Zlib
- Source: https://github.com/stackgl/gl-vec4

### glsl-inject-defines (1.0.3)
- License: MIT
- Source: https://github.com/mattdesl/glsl-inject-defines

### glsl-inverse (1.0.0)
- License: MIT
- Source: https://github.com/stackgl/glsl-inverse

### glsl-out-of-range (1.0.4)
- License: MIT
- Source: https://github.com/plotly/glsl-out-of-range

### glsl-resolve (0.0.1)
- License: MIT
- Source: https://github.com/hughsk/glsl-resolve

### glsl-shader-name (1.0.0)
- License: MIT
- Source: https://github.com/stackgl/glsl-shader-name

### glsl-specular-beckmann (1.1.2)
- License: MIT
- Source: https://github.com/stackgl/glsl-specular-beckmann

### glsl-specular-cook-torrance (2.0.1)
- License: MIT
- Source: https://github.com/stackgl/glsl-specular-cook-torrance

### glsl-token-assignments (2.0.2)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-assignments

### glsl-token-defines (1.0.0)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-defines

### glsl-token-depth (1.1.2)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-depth

### glsl-token-descope (1.0.2)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-descope

### glsl-token-inject-block (1.1.0)
- License: MIT
- Source: https://github.com/Jam3/glsl-token-inject-block

### glsl-token-properties (1.0.1)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-properties

### glsl-token-scope (1.1.2)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-scope

### glsl-token-string (1.0.1)
- License: MIT
- Source: https://github.com/stackgl/glsl-token-string

### glsl-token-whitespace-trim (1.0.0)
- License: MIT
- Source: https://github.com/hughsk/glsl-token-whitespace-trim

### glsl-tokenizer (2.1.5)
- License: MIT
- Source: https://github.com/gl-modules/glsl-tokenizer

### glslify (7.1.1)
- License: MIT
- Source: https://github.com/stackgl/glslify

### glslify-bundle (5.1.1)
- License: MIT
- Source: https://github.com/stackgl/glslify-bundle

### glslify-deps (1.3.2)
- License: ISC
- Source: https://github.com/stackgl/glslify-deps

### graceful-fs (4.2.6)
- License: ISC
- Source: https://github.com/isaacs/node-graceful-fs

### grid-index (1.1.0)
- License: ISC
- Source: https://github.com/mapbox/grid-index

### has (1.0.3)
- License: MIT
- Source: https://github.com/tarruda/has

### has-hover (1.0.1)
- License: MIT
- Source: https://github.com/dfcreative/has-hover

### has-passive-events (1.0.0)
- License: MIT
- Source: https://github.com/dfcreative/has-passive-events

### hoist-non-react-statics (3.3.2, 3.3.2)
- License: BSD-3-Clause
- Source: https://github.com/mridgway/hoist-non-react-statics

### hsluv (0.0.3)
- License: MIT
- Source: https://github.com/hsluv/hsluv

### hyphenate-style-name (1.0.4, 1.0.4)
- License: BSD-3-Clause
- Source: https://github.com/rexxars/hyphenate-style-name

### ieee754 (1.2.1)
- License: BSD-3-Clause
- Source: https://github.com/feross/ieee754

### image-palette (2.1.0)
- License: MIT*
- Source: https://github.com/dy/image-palette

### image-size (0.7.5)
- License: MIT
- Source: https://github.com/image-size/image-size

### incremental-convex-hull (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/incremental-convex-hull

### indefinite-observable (2.0.1)
- License: Apache-2.0
- Source: https://github.com/material-motion/indefinite-observable-js

### inherits (2.0.4)
- License: ISC
- Source: https://github.com/isaacs/inherits

### interval-tree-1d (1.0.3)
- License: MIT
- Source: https://github.com/mikolalysenko/interval-tree-1d

### invert-permutation (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/invert-permutation

### iota-array (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/iota-array

### is-base64 (0.1.0)
- License: MIT
- Source: https://github.com/miguelmota/is-base64

### is-blob (2.1.0)
- License: MIT
- Source: https://github.com/sindresorhus/is-blob

### is-browser (2.1.0)
- License: MIT
- Source: https://github.com/ForbesLindesay/is-browser

### is-buffer (1.1.6, 2.0.5)
- License: MIT
- Source: https://github.com/feross/is-buffer

### is-core-module (2.2.0)
- License: MIT
- Source: https://github.com/inspect-js/is-core-module

### is-finite (1.1.0)
- License: MIT
- Source: https://github.com/sindresorhus/is-finite

### is-firefox (1.0.3)
- License: MIT
- Source: https://github.com/gillstrom/is-firefox

### is-float-array (1.0.0)
- License: MIT
- Source: https://github.com/dy/is-float-array

### is-iexplorer (1.0.0)
- License: MIT
- Source: https://github.com/kevva/is-iexplorer

### is-in-browser (1.1.3, 1.1.3)
- License: MIT
- Source: https://github.com/tuxsudo/is-in-browser

### is-mobile (2.2.2)
- License: MIT
- Source: https://github.com/juliangruber/is-mobile

### is-obj (1.0.1)
- License: MIT
- Source: https://github.com/sindresorhus/is-obj

### is-plain-obj (1.1.0)
- License: MIT
- Source: https://github.com/sindresorhus/is-plain-obj

### is-string-blank (1.0.1)
- License: MIT
- Source: https://github.com/plotly/is-string-blank

### is-svg-path (1.0.2)
- License: MIT
- Source: https://github.com/dfcreative/is-svg-path

### isarray (1.0.0, 2.0.5)
- License: MIT
- Source: https://github.com/juliangruber/isarray

### js-tokens (4.0.0, 4.0.0)
- License: MIT
- Source: https://github.com/lydell/js-tokens

### jss (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-camel-case (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-default-unit (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-global (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-nested (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-props-sort (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-rule-value-function (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### jss-plugin-vendor-prefixer (10.6.0, 10.8.2)
- License: MIT
- Source: https://github.com/cssinjs/jss

### kdbush (3.0.0)
- License: ISC
- Source: https://github.com/mourner/kdbush

### lerp (1.0.3)
- License: MIT
- Source: https://github.com/mattdesl/lerp

### levn (0.3.0)
- License: MIT
- Source: https://github.com/gkz/levn

### loose-envify (1.4.0, 1.4.0)
- License: MIT
- Source: https://github.com/zertosh/loose-envify

### map-limit (0.0.1)
- License: MIT
- Source: https://github.com/hughsk/map-limit

### mapbox-gl (1.10.1)
- License: MIT*
- Source: https://github.com/mapbox/mapbox-gl-js

### mapbox/geojson-rewind (0.5.0)
- License: ISC
- Source: https://github.com/mapbox/geojson-rewind

### mapbox/geojson-types (1.0.2)
- License: ISC
- Source: https://github.com/mapbox/geojson-types

### mapbox/jsonlint-lines-primitives (2.0.2)
- License: Custom: https://github.com/tmcw/jsonlint
- Source: https://github.com/mapbox/jsonlint

### mapbox/mapbox-gl-supported (1.5.0)
- License: BSD-3-Clause
- Source: https://github.com/mapbox/mapbox-gl-supported

### mapbox/point-geometry (0.1.0)
- License: ISC
- Source: https://github.com/mapbox/point-geometry

### mapbox/tiny-sdf (1.2.5)
- License: BSD-2-Clause
- Source: https://github.com/mapbox/tiny-sdf

### mapbox/unitbezier (0.0.0)
- License: BSD-2-Clause
- Source: https://github.com/mapbox/unitbezier

### mapbox/vector-tile (1.3.1)
- License: BSD-3-Clause
- Source: https://github.com/mapbox/vector-tile-js

### mapbox/whoots-js (3.1.0)
- License: ISC
- Source: https://github.com/mapbox/whoots-js

### marching-simplex-table (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/marching-simplex-table

### mat4-decompose (1.0.4)
- License: MIT
- Source: https://github.com/mattdesl/mat4-decompose

### mat4-interpolate (1.0.4)
- License: MIT
- Source: https://github.com/mattdesl/mat4-interpolate

### mat4-recompose (1.0.4)
- License: MIT
- Source: https://github.com/mattdesl/mat4-recompose

### material-ui/core (4.11.4, 4.12.3)
- License: MIT
- Source: https://github.com/mui-org/material-ui

### material-ui/icons (4.11.2, 4.11.2)
- License: MIT
- Source: https://github.com/mui-org/material-ui

### material-ui/styles (4.11.4, 4.11.4)
- License: MIT
- Source: https://github.com/mui-org/material-ui

### material-ui/system (4.11.3, 4.12.1)
- License: MIT
- Source: https://github.com/mui-org/material-ui

### material-ui/types (5.1.0, 5.1.0)
- License: MIT
- Source: https://github.com/mui-org/material-ui

### material-ui/utils (4.11.2, 4.11.2)
- License: MIT
- Source: https://github.com/mui-org/material-ui

### math-log2 (1.0.1)
- License: MIT
- Source: https://github.com/sindresorhus/math-log2

### matrix-camera-controller (2.1.3)
- License: MIT
- Source: https://github.com/mikolalysenko/matrix-camera-controller

### minimist (1.2.5)
- License: MIT
- Source: https://github.com/substack/minimist

### monotone-convex-hull-2d (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/monotone-convex-hull-2d

### mouse-change (1.4.0)
- License: MIT
- Source: https://github.com/mikolalysenko/mouse-change

### mouse-event (1.0.5)
- License: MIT
- Source: https://github.com/mikolalysenko/mouse-event

### mouse-event-offset (3.0.2)
- License: MIT
- Source: https://github.com/mattdesl/mouse-event-offset

### mouse-wheel (1.2.0)
- License: MIT
- Source: https://github.com/mikolalysenko/mouse-wheel

### mumath (3.3.4)
- License: Unlicense
- Source: https://github.com/dfcreative/mumath

### murmurhash-js (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/murmurhash-js

### ndarray (1.0.19)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray

### ndarray-extract-contour (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-extract-contour

### ndarray-gradient (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-gradient

### ndarray-linear-interpolate (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-linear-interpolate

### ndarray-ops (1.2.2)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-ops

### ndarray-pack (1.2.1)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-pack

### ndarray-scratch (1.2.0)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-scratch

### ndarray-sort (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/ndarray-sort

### next-tick (1.0.0)
- License: MIT
- Source: https://github.com/medikoo/next-tick

### nextafter (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/nextafter

### normalize-svg-path (0.1.0, 1.1.0)
- License: MIT
- Source: https://github.com/jkroso/normalize-svg-path

### normals (1.1.0)
- License: MIT
- Source: https://github.com/mikolalysenko/normals

### number-is-integer (1.0.1)
- License: MIT
- Source: https://github.com/sindresorhus/number-is-integer

### numeric (1.2.6)
- License: MIT*
- Source: https://github.com/sloisel/numeric

### object-assign (4.1.1, 4.1.1)
- License: MIT
- Source: https://github.com/sindresorhus/object-assign

### object-keys (1.1.1)
- License: MIT
- Source: https://github.com/ljharb/object-keys

### once (1.3.3, 1.4.0)
- License: ISC
- Source: https://github.com/isaacs/once

### optionator (0.8.3)
- License: MIT
- Source: https://github.com/gkz/optionator

### orbit-camera-controller (4.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/orbit-camera-controller

### pad-left (1.0.2)
- License: MIT
- Source: https://github.com/jonschlinkert/pad-left

### parenthesis (3.1.7)
- License: MIT
- Source: https://github.com/dy/parenthesis

### parse-rect (1.2.0)
- License: MIT
- Source: https://github.com/dfcreative/parse-rect

### parse-svg-path (0.1.2)
- License: MIT
- Source: https://github.com/jkroso/parse-svg-path

### parse-unit (1.0.1)
- License: MIT
- Source: https://github.com/mattdesl/parse-unit

### path-parse (1.0.6)
- License: MIT
- Source: https://github.com/jbgutierrez/path-parse

### pbf (3.2.1)
- License: BSD-3-Clause
- Source: https://github.com/mapbox/pbf

### performance-now (2.1.0)
- License: MIT
- Source: https://github.com/braveg1rl/performance-now

### permutation-parity (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/permutation-parity

### permutation-rank (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/permutation-rank

### pick-by-alias (1.2.0)
- License: MIT
- Source: https://github.com/dfcreative/pick-by-alias

### planar-dual (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/planar-dual

### planar-graph-to-polyline (1.0.5)
- License: MIT
- Source: https://github.com/mikolalysenko/planar-graph-to-polyline

### plotly.js (1.58.4)
- License: MIT
- Source: https://github.com/plotly/plotly.js

### plotly/d3-sankey (0.7.2)
- License: BSD-3-Clause
- Source: https://github.com/d3/d3-sankey

### plotly/d3-sankey-circular (0.33.1)
- License: MIT
- Source: https://github.com/plotly/d3-sankey-circular

### plotly/point-cluster (3.1.9)
- License: MIT
- Source: https://github.com/plotly/point-cluster

### point-in-big-polygon (2.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/point-in-big-polygon

### polybooljs (1.2.0)
- License: MIT
- Source: https://github.com/voidqk/polybooljs

### polytope-closest-point (1.0.0)
- License: MIT*
- Source: https://github.com/mikolalysenko/polytope-closest-point

### popper.js (1.16.1-lts, 1.16.1-lts)
- License: MIT
- Source: https://github.com/FezVrasta/popper.js

### potpack (1.0.1)
- License: ISC
- Source: https://github.com/mapbox/potpack

### prelude-ls (1.1.2)
- License: MIT
- Source: https://github.com/gkz/prelude-ls

### process-nextick-args (2.0.1)
- License: MIT
- Source: https://github.com/calvinmetcalf/process-nextick-args

### prop-types (15.7.2, 15.7.2)
- License: MIT
- Source: https://github.com/facebook/prop-types

### protocol-buffers-schema (3.5.1)
- License: MIT
- Source: https://github.com/mafintosh/protocol-buffers-schema

### pxls (2.3.2)
- License: MIT
- Source: https://github.com/dy/pxls

### quantize (1.0.2)
- License: MIT
- Source: https://github.com/olivierlesnicki/quantize

### quat-slerp (1.0.1)
- License: MIT
- Source: https://github.com/mattdesl/quat-slerp

### quickselect (2.0.0)
- License: ISC
- Source: https://github.com/mourner/quickselect

### raf (3.4.1)
- License: MIT
- Source: https://github.com/chrisdickinson/raf

### rat-vec (1.1.1)
- License: MIT
- Source: https://github.com/rat-nest/rat-vec

### react (17.0.2, 17.0.2)
- License: MIT
- Source: https://github.com/facebook/react

### react-dom (17.0.2, 17.0.2)
- License: MIT
- Source: https://github.com/facebook/react

### react-is (16.13.1, 17.0.2, 16.13.1, 17.0.2)
- License: MIT
- Source: https://github.com/facebook/react

### react-lifecycles-compat (3.0.4)
- License: MIT
- Source: https://github.com/reactjs/react-lifecycles-compat

### react-plotly.js (2.5.1)
- License: MIT
- Source: https://github.com/plotly/react-plotly.js

### react-split-pane (0.1.92)
- License: MIT
- Source: https://github.com/tomkp/react-split-pane

### react-style-proptype (3.2.2)
- License: MIT
- Source: https://github.com/brigand/react-style-proptype

### react-transition-group (4.4.1, 4.4.2)
- License: BSD-3-Clause
- Source: https://github.com/reactjs/react-transition-group

### readable-stream (2.3.7, 3.6.0)
- License: MIT
- Source: https://github.com/nodejs/readable-stream

### reduce-simplicial-complex (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/reduce-simplicial-complex

### regenerator-runtime (0.13.7, 0.13.9)
- License: MIT
- Source: https://github.com/facebook/regenerator/tree/master/packages/regenerator-runtime

### regex-regex (1.0.0)
- License: MIT
- Source: https://github.com/kgryte/regex-regex

### regl (1.7.0)
- License: MIT
- Source: https://github.com/regl-project/regl

### regl-error2d (2.0.11)
- License: MIT
- Source: https://github.com/dy/regl-error2d

### regl-line2d (3.1.0)
- License: MIT
- Source: https://github.com/dy/regl-line2d

### regl-scatter2d (3.2.3)
- License: MIT
- Source: https://github.com/dy/regl-scatter2d

### regl-splom (1.0.14)
- License: MIT
- Source: https://github.com/gl-vis/regl-splom

### repeat-string (1.6.1)
- License: MIT
- Source: https://github.com/jonschlinkert/repeat-string

### resolve (0.6.3, 1.20.0)
- License: MIT
- Source: https://github.com/substack/node-resolve

### resolve-protobuf-schema (2.1.0)
- License: MIT
- Source: https://github.com/mafintosh/resolve-protobuf-schema

### right-now (1.0.0)
- License: MIT
- Source: https://github.com/hughsk/right-now

### robust-compress (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-compress

### robust-determinant (1.1.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-determinant

### robust-dot-product (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-dot-product

### robust-in-sphere (1.1.3)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-in-sphere

### robust-linear-solve (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-linear-solve

### robust-orientation (1.1.3)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-orientation

### robust-product (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-product

### robust-scale (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-scale

### robust-segment-intersect (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-segment-intersect

### robust-subtract (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-subtract

### robust-sum (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/robust-sum

### rw (1.3.3)
- License: BSD-3-Clause
- Source: https://github.com/mbostock/rw

### safe-buffer (5.1.2)
- License: MIT
- Source: https://github.com/feross/safe-buffer

### sane-topojson (4.0.0)
- License: MIT
- Source: https://github.com/etpinard/sane-topojson

### scheduler (0.20.2, 0.20.2)
- License: MIT
- Source: https://github.com/facebook/react

### shallow-copy (0.0.1)
- License: MIT
- Source: https://github.com/substack/shallow-copy

### signum (0.0.0, 1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/signum

### simplicial-complex (0.3.3, 1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/simplicial-complex

### simplicial-complex-boundary (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/simplicial-complex-boundary

### simplicial-complex-contour (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/simplicial-complex-contour

### simplify-planar-graph (2.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/simplify-planar-graph

### slab-decomposition (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/slab-decomposition

### source-map (0.6.1)
- License: BSD-3-Clause
- Source: https://github.com/mozilla/source-map

### split-polygon (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/split-polygon

### sprintf-js (1.0.3)
- License: BSD-3-Clause
- Source: https://github.com/alexei/sprintf.js

### stack-trace (0.0.9)
- License: MIT*
- Source: https://github.com/felixge/node-stack-trace

### static-eval (2.1.0)
- License: MIT
- Source: https://github.com/browserify/static-eval

### stream-shift (1.0.1)
- License: MIT
- Source: https://github.com/mafintosh/stream-shift

### string-split-by (1.0.0)
- License: MIT
- Source: https://github.com/dy/string-split-by

### string-to-arraybuffer (1.0.2)
- License: MIT
- Source: https://github.com/dy/string-to-arraybuffer

### string_decoder (1.1.1)
- License: MIT
- Source: https://github.com/nodejs/string_decoder

### strongly-connected-components (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/strongly-connected-components

### supercluster (7.1.3)
- License: ISC
- Source: https://github.com/mapbox/supercluster

### superscript-text (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/superscript-text

### surface-nets (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/surface-nets

### svg-arc-to-cubic-bezier (3.2.0)
- License: ISC
- Source: https://github.com/colinmeinke/svg-arc-to-cubic-bezier

### svg-path-bounds (1.0.1)
- License: MIT
- Source: https://github.com/dfcreative/svg-path-bounds

### svg-path-sdf (1.1.3)
- License: MIT
- Source: https://github.com/dy/svg-path-sdf

### symbol-observable (1.2.0)
- License: MIT
- Source: https://github.com/blesh/symbol-observable

### text-cache (4.2.2)
- License: MIT
- Source: https://github.com/gl-vis/text-cache

### through2 (3.0.2)
- License: MIT
- Source: https://github.com/rvagg/through2

### tiny-warning (1.0.3, 1.0.3)
- License: MIT
- Source: https://github.com/alexreardon/tiny-warning

### tinycolor2 (1.4.2)
- License: MIT
- Source: https://github.com/bgrins/TinyColor

### tinyqueue (2.0.3)
- License: ISC
- Source: https://github.com/mourner/tinyqueue

### to-array-buffer (3.2.0)
- License: MIT
- Source: https://github.com/dy/to-array-buffer

### to-float32 (1.0.1)
- License: MIT
- Source: https://github.com/dy/to-float32

### to-px (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/to-px

### to-uint8 (1.4.1)
- License: MIT
- Source: https://github.com/dy/to-uint8

### topojson-client (3.1.0)
- License: ISC
- Source: https://github.com/topojson/topojson-client

### triangulate-hypercube (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/triangulate-hypercube

### triangulate-polyline (1.0.3)
- License: MIT
- Source: https://github.com/mikolalysenko/triangulate-polyline

### turf/area (6.3.0)
- License: MIT
- Source: https://github.com/Turfjs/turf

### turf/bbox (6.3.0)
- License: MIT
- Source: https://github.com/Turfjs/turf

### turf/centroid (6.3.0)
- License: MIT
- Source: https://github.com/Turfjs/turf

### turf/helpers (6.3.0)
- License: MIT
- Source: https://github.com/Turfjs/turf

### turf/meta (6.3.0)
- License: MIT
- Source: https://github.com/Turfjs/turf

### turntable-camera-controller (3.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/turntable-camera-controller

### two-product (1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/two-product

### two-sum (1.0.0)
- License: MIT
- Source: https://github.com/mikolalysenko/two-sum

### type (1.2.0, 2.5.0)
- License: ISC
- Source: https://github.com/medikoo/type

### type-check (0.3.2)
- License: MIT
- Source: https://github.com/gkz/type-check

### type-name (2.0.2)
- License: MIT
- Source: https://github.com/twada/type-name

### typedarray (0.0.6)
- License: MIT
- Source: https://github.com/substack/typedarray

### typedarray-pool (1.2.0)
- License: MIT
- Source: https://github.com/mikolalysenko/typedarray-pool

### union-find (0.0.4, 1.0.2)
- License: MIT
- Source: https://github.com/mikolalysenko/union-find

### uniq (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/uniq

### unquote (1.1.1)
- License: MIT
- Source: https://github.com/lakenen/node-unquote

### update-diff (1.1.0)
- License: MIT
- Source: https://github.com/dfcreative/update-diff

### util-deprecate (1.0.2)
- License: MIT
- Source: https://github.com/TooTallNate/util-deprecate

### utils-copy (1.1.1)
- License: MIT
- Source: https://github.com/kgryte/utils-copy

### utils-copy-error (1.0.1)
- License: MIT
- Source: https://github.com/kgryte/utils-copy-error

### utils-indexof (1.0.0)
- License: MIT
- Source: https://github.com/kgryte/utils-indexof

### utils-regex-from-string (1.0.0)
- License: MIT
- Source: https://github.com/kgryte/utils-regex-from-string

### validate.io-array (1.0.6)
- License: MIT
- Source: https://github.com/validate-io/array

### validate.io-array-like (1.0.2)
- License: MIT
- Source: https://github.com/validate-io/array-like

### validate.io-buffer (1.0.2)
- License: MIT
- Source: https://github.com/validate-io/buffer

### validate.io-integer (1.0.5)
- License: MIT
- Source: https://github.com/validate-io/integer

### validate.io-integer-primitive (1.0.0)
- License: MIT
- Source: https://github.com/validate-io/integer-primitive

### validate.io-matrix-like (1.0.2)
- License: MIT
- Source: https://github.com/validate-io/matrix-like

### validate.io-ndarray-like (1.0.0)
- License: MIT
- Source: https://github.com/validate-io/ndarray-like

### validate.io-nonnegative-integer (1.0.0)
- License: MIT
- Source: https://github.com/validate-io/nonnegative-integer

### validate.io-number (1.0.3)
- License: MIT
- Source: https://github.com/validate-io/number

### validate.io-number-primitive (1.0.0)
- License: MIT
- Source: https://github.com/validate-io/number-primitive

### validate.io-positive-integer (1.0.0)
- License: MIT
- Source: https://github.com/validate-io/positive-integer

### validate.io-string-primitive (1.0.1)
- License: MIT
- Source: https://github.com/validate-io/string-primitive

### vectorize-text (3.2.1)
- License: MIT
- Source: https://github.com/mikolalysenko/vectorize-text

### vt-pbf (3.1.1)
- License: MIT
- Source: https://github.com/mapbox/vt-pbf

### weak-map (1.0.5)
- License: Custom: https://github.com/substack/node-browserify
- Source: https://github.com/drses/weak-map

### weakmap-shim (1.1.1)
- License: MIT
- Source: https://github.com/Raynos/weakmap-shim

### webgl-context (2.2.0)
- License: MIT
- Source: https://github.com/mattdesl/webgl-context

### word-wrap (1.2.3)
- License: MIT
- Source: https://github.com/jonschlinkert/word-wrap

### world-calendars (1.0.3)
- License: MIT
- Source: https://github.com/alexcjohnson/world-calendars

### wrappy (1.0.2)
- License: ISC
- Source: https://github.com/npm/wrappy

### xtend (2.2.0, 4.0.2)
- License: MIT
- Source: https://github.com/Raynos/xtend

### zero-crossings (1.0.1)
- License: MIT
- Source: https://github.com/mikolalysenko/zero-crossings

