pipeline {
    agent {
        kubernetes {
            label 'centos-7'
        }
    }

    tools {
        jdk 'adoptopenjdk-hotspot-jdk11-latest'
        maven 'apache-maven-latest'
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '2'))
    }

    stages {
        stage('Build and test') {
            steps {
                wrap([$class: 'Xvnc', takeScreenshot: false, useXauthority: true]) {
                    sh '''
                    ls lib/org.eclipse.comma.parameters.tests/src/org/eclipse/comma/parameters/tests
                    # Git setup
                    git config user.name "comma-bot"
                    git config user.email "comma-bot@eclipse.org"
                    git config push.default simple

                    # Setup node.js
                    VERSION="v14.17.0"
                    URL="https://nodejs.org/dist/$VERSION/node-$VERSION-linux-x64.tar.gz"
                    echo "Downloading Node.js $VERSION"
                    curl -s $URL | tar xz -C /tmp
                    export PATH="/tmp/node-$VERSION-linux-x64/bin":$PATH
                    npm install -g license-checker

                    # When RELEASE_VERSION is set validate, the version number and set it.
                    if [[ ! -z "$RELEASE_VERSION" ]]; then
                        if ! [[ $RELEASE_VERSION =~ ^v[0-9]+\\.[0-9]+\\.[0-9]+((\\.M[0-9]+)|(\\.RC[0-9]+))?$ ]]; then
                            echo "'$RELEASE_VERSION' is not a valid version number, should be 'v0.0.0', 'v0.0.0.RC0' or 'v0.0.0.M0' where '0' can be any and multiple numbers."
                            exit 1
                        fi

                        echo "Creating a release version with number '$RELEASE_VERSION'"
                        mvn org.eclipse.tycho:tycho-versions-plugin:2.7.5:set-version -Dtycho.mode=maven -DnewVersion="${RELEASE_VERSION:1}"
                        git add -A
                        git commit -m "Version $RELEASE_VERSION"
                        git tag -a "$RELEASE_VERSION" -m "$RELEASE_VERSION"
                    fi

                    # Signing takes long, therefore only do it on the main branch and for a release.
                    SIGN=""
                    if [[ "$GIT_BRANCH" == "main" || ! -z "$RELEASE_VERSION" ]]; then
                        SIGN=",sign"
                    fi

                    mvn -e clean verify dependency:list -Pdashboard$SIGN 2>&1 | tee build.log

                    # Update NOTICE-THIRD-PARTY.md
                    git diff --exit-code # Make sure there are no changes
                    python3 lib/org.eclipse.comma.license/generate_notice.py build.log lib/org.eclipse.comma.monitoring.dashboard/app/dependencies.json,lib/org.eclipse.comma.modelqualitychecks/app/dependencies.json NOTICE-THIRD-PARTY.md
                    if [ ! -z "$(git status --porcelain)" ]; then
                        git add -A
                        git commit -m "Update NOTICE-THIRD-PARTY.md"
                    fi
                    '''
                }
            }

            post {
                success {
                    archiveArtifacts artifacts: 'org.eclipse.comma.standard.product/target/products/*win32*.zip'
                    archiveArtifacts artifacts: 'org.eclipse.comma.standard.product/target/products/*.dmg', allowEmptyArchive: true
                    archiveArtifacts artifacts: 'org.eclipse.comma.standard.product/target/products/*linux*.tar.gz'
                    archiveArtifacts artifacts: 'org.eclipse.comma.standard.product/target/*updatesite*.zip'
                    archiveArtifacts artifacts: 'org.eclipse.comma.standard.site/target/*site*.zip'
                    archiveArtifacts artifacts: 'org.eclipse.comma.standard.project/target/comma-*.jar'
                }
            }
        }

        stage('Deploy and push release') {
            when {
                expression { env.RELEASE_VERSION != null }
            }
            environment {
                DOWNLOADS_PATH = "/home/data/httpd/download.eclipse.org/comma"
                DOWNLOADS_URL = "genie.comma@projects-storage.eclipse.org:${DOWNLOADS_PATH}"
                WEBSITE_GIT_URL = "ssh://genie.comma@git.eclipse.org:29418/www.eclipse.org/comma.git"
            }
            steps {
                sshagent (['projects-storage.eclipse.org-bot-ssh']) {
                    // Remove any existing directory for this release.
                    sh 'ssh genie.comma@projects-storage.eclipse.org rm -rf ${DOWNLOADS_PATH}/${RELEASE_VERSION}/'

                    // Plugin
                    sh 'ssh genie.comma@projects-storage.eclipse.org mkdir -p ${DOWNLOADS_PATH}/${RELEASE_VERSION}/plugin/update-site/'
                    sh 'scp -r org.eclipse.comma.standard.site/target/repository/* ${DOWNLOADS_URL}/${RELEASE_VERSION}/plugin/update-site/'

                    // CLI
                    sh 'ssh genie.comma@projects-storage.eclipse.org mkdir -p ${DOWNLOADS_PATH}/${RELEASE_VERSION}/cli'
                    sh 'scp -r org.eclipse.comma.standard.project/target/comma-*.jar ${DOWNLOADS_URL}/${RELEASE_VERSION}/cli/'

                    // Product
                    sh 'ssh genie.comma@projects-storage.eclipse.org mkdir -p ${DOWNLOADS_PATH}/${RELEASE_VERSION}/product/update-site/'
                    sh 'scp -r org.eclipse.comma.standard.product/target/repository/* ${DOWNLOADS_URL}/${RELEASE_VERSION}/product/update-site/'
                    sh 'scp -r org.eclipse.comma.standard.product/target/products/*win32*.zip ${DOWNLOADS_URL}/${RELEASE_VERSION}/product/'
                    sh 'scp -r org.eclipse.comma.standard.product/target/products/*.dmg ${DOWNLOADS_URL}/${RELEASE_VERSION}/product/'
                    sh 'scp -r org.eclipse.comma.standard.product/target/products/*linux*.tar.gz ${DOWNLOADS_URL}/${RELEASE_VERSION}/product/'

                    // Installation manual
                    sh 'scp -r lib/org.eclipse.comma.help/asciidoc-gen/eclipse-help/installation/manual.html ${DOWNLOADS_URL}/${RELEASE_VERSION}/installation_manual.html'

                    // download.php
                    sh 'scp -r lib/org.eclipse.comma.help/download.php ${DOWNLOADS_URL}/download.php'

                    // Latest
                    sh '''
                    if [[ $RELEASE_VERSION =~ ^v[0-9]+\\.[0-9]+\\.[0-9]+$ ]]; then
                        ssh genie.comma@projects-storage.eclipse.org rm -rf ${DOWNLOADS_PATH}/latest/
                        ssh genie.comma@projects-storage.eclipse.org cp -R ${DOWNLOADS_PATH}/${RELEASE_VERSION} ${DOWNLOADS_PATH}/latest
                    fi
                    '''
                }
                sh '''
                [[ $RELEASE_VERSION =~ ^v([0-9]+\\.[0-9]+\\.[0-9]+) ]]
                mvn org.eclipse.tycho:tycho-versions-plugin:2.7.5:set-version -Dtycho.mode=maven -DnewVersion="${BASH_REMATCH[1]}-SNAPSHOT"
                git add -A
                git commit -m "Version ${BASH_REMATCH[1]}-SNAPSHOT"
                '''
            }
        }

        stage('Website') {
            when {
                anyOf {
                    branch 'main'
                    expression { env.RELEASE_VERSION != null }
                }
            }
            environment {
                WEBSITE_GIT_URL = "git@gitlab.eclipse.org:eclipse/comma/website.git"
            }
            steps {
                sshagent(credentials: ["gitlab-bot-ssh"]) {
                    sh '''
                    rm -rf /tmp/website
                    git clone ${WEBSITE_GIT_URL} /tmp/website

                    # On a release (but not .RC or .M release) update full docs, otherwise only site
                    if [[ $RELEASE_VERSION =~ ^v[0-9]+\\.[0-9]+\\.[0-9]+$ ]]; then
                        rm -rf /tmp/website/*
                        cp -r lib/org.eclipse.comma.help/asciidoc-gen/site/* /tmp/website
                    else
                        cp lib/org.eclipse.comma.help/asciidoc-gen/site/index.html /tmp/website
                        cp lib/org.eclipse.comma.help/asciidoc-gen/site/favicon.png /tmp/website
                        rm -rf /tmp/website/site
                        cp -r lib/org.eclipse.comma.help/asciidoc-gen/site/site /tmp/website
                    fi

                    cd /tmp/website
                    if [ ! -z "$(git status --porcelain)" ]; then
                        git config user.name "comma-bot"
                        git config user.email "comma-bot@eclipse.org"
                        git config push.default simple
                        git add -A
                        git commit -m "Website update"
                        git push origin
                    fi
                    '''
                }
            }
        }

        stage('Push to Git') {
            steps {
                sshagent(credentials: ["gitlab-bot-ssh"]) {
                    sh '''
                    git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
                    git fetch origin
                    git rebase "origin/${GIT_BRANCH#"origin/"}"
                    git push origin "HEAD:${GIT_BRANCH#"origin/"}" --follow-tags
                    '''
               }
            }
        }
    }

    post {
        // Send an e-mail on unsuccessful builds (unstable, failure, aborted).
        unsuccessful {
            emailext subject: 'Build $BUILD_STATUS $PROJECT_NAME #$BUILD_NUMBER!',
            body: '''Check console output at $BUILD_URL to view the results.''',
            recipientProviders: [culprits(), requestor()]
        }

        // Send an e-mail on fixed builds (back to normal).
        fixed {
            emailext subject: 'Build $BUILD_STATUS $PROJECT_NAME #$BUILD_NUMBER!',
            body: '''Check console output at $BUILD_URL to view the results.''',
            recipientProviders: [culprits(), requestor()]
        }
    }
}
